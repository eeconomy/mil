﻿
#Область ОбработчикиСобытий

Процедура ОбработкаПолученияДанныхВыбора(ДанныеВыбора, Параметры, СтандартнаяОбработка)
	
	Если Параметры.Отбор.Свойство("Договор") Тогда
		
		СтандартнаяОбработка = Ложь;
		
		ДанныеВыбора = Новый СписокЗначений;
		
		Запрос = Новый Запрос;
		Запрос.Текст = "
		|ВЫБРАТЬ
		|	ЗНАЧЕНИЕ(Перечисление.ТипыПлатежейПоЛизингу.ВыкупПредметаЛизинга) КАК Ссылка
		|ИЗ
		|	Справочник.ДоговорыЛизинга КАК Т
		|ГДЕ
		|	Т.Ссылка = &Договор
		|	И Т.ЕстьВыкупПредметаЛизинга
		|
		|ОБЪЕДИНИТЬ ВСЕ
		|
		|ВЫБРАТЬ
		|	ЗНАЧЕНИЕ(Перечисление.ТипыПлатежейПоЛизингу.ОбеспечительныйПлатеж)
		|ИЗ
		|	Справочник.ДоговорыЛизинга КАК Т
		|ГДЕ
		|	Т.Ссылка = &Договор
		|	И Т.ЕстьОбеспечительныйПлатеж
		|	И НЕ &КорректировкаЗадолженности
		|
		|ОБЪЕДИНИТЬ ВСЕ
		|
		|ВЫБРАТЬ
		|	ЗНАЧЕНИЕ(Перечисление.ТипыПлатежейПоЛизингу.АрендныеОбязательства)
		|ИЗ
		|	Справочник.ДоговорыЛизинга КАК Т
		|ГДЕ
		|	Т.Ссылка = &Договор
		|	И Т.ВариантУчетаИмущества = ЗНАЧЕНИЕ(Перечисление.ВариантыУчетаИмуществаПриЛизинге.НаБалансе)
		|	И НЕ &ЭтоПлатеж
		|";
		
		Запрос.УстановитьПараметр("Договор", Параметры.Отбор.Договор);
		Если Параметры.Отбор.Свойство("Платеж") Тогда
			Запрос.УстановитьПараметр("ЭтоПлатеж", Параметры.Отбор.Платеж);
		Иначе
			Запрос.УстановитьПараметр("ЭтоПлатеж", Ложь);
		КонецЕсли;
		
		Если Параметры.Свойство("КорректировкаЗадолженности") И Параметры.КорректировкаЗадолженности Тогда
			Запрос.УстановитьПараметр("КорректировкаЗадолженности", Истина);
		Иначе
			Запрос.УстановитьПараметр("КорректировкаЗадолженности", Ложь);
		КонецЕсли;
		
		ДанныеВыбора.ЗагрузитьЗначения(Запрос.Выполнить().Выгрузить().ВыгрузитьКолонку("Ссылка"));
		ДанныеВыбора.Вставить(0, Перечисления.ТипыПлатежейПоЛизингу.ЛизинговыйПлатеж);
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти