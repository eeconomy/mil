﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ОбработчикиСобытий

Процедура ПриКомпоновкеРезультата(ДокументРезультат, ДанныеРасшифровки, СтандартнаяОбработка)

	ЗарплатаКадрыОбщиеНаборыДанных.ЗаполнитьОбщиеИсточникиДанныхОтчета(ЭтотОбъект);

	СтандартнаяОбработка = Ложь;
	
	НастройкиОтчета = ЭтотОбъект.КомпоновщикНастроек.ПолучитьНастройки();				   

	УстановитьЗначенияПараметров(НастройкиОтчета);
	
	ДокументРезультат.Очистить();
	
	КлючВарианта = ЗарплатаКадрыОтчеты.КлючВарианта(КомпоновщикНастроек);
	Если КлючВарианта = "ЧисленностьРаботающихИЗабронированныхГражданЗапаса" Тогда
		
		// Параметры документа
		ДокументРезультат.ТолькоПросмотр = Истина;
		ДокументРезультат.ИмяПараметровПечати = "ПАРАМЕТРЫ_ПЕЧАТИ_ЧисленностьРаботающихИЗабронированныхГражданЗапаса";
		ДокументРезультат.ОриентацияСтраницы = ОриентацияСтраницы.Портрет;
		
		ДатаОтчета = '00010101';
		
		УстановитьДатуОтчета(НастройкиОтчета, ДатаОтчета);
		
		ДанныеОтчета = Новый ДеревоЗначений;
		
		КомпоновщикМакета = Новый КомпоновщикМакетаКомпоновкиДанных;
		МакетКомпоновки = КомпоновщикМакета.Выполнить(ЭтотОбъект.СхемаКомпоновкиДанных, НастройкиОтчета, ДанныеРасшифровки,, Тип("ГенераторМакетаКомпоновкиДанныхДляКоллекцииЗначений"));
		
		// Создадим и инициализируем процессор компоновки.
		ПроцессорКомпоновки = Новый ПроцессорКомпоновкиДанных;
		ПроцессорКомпоновки.Инициализировать(МакетКомпоновки, , ДанныеРасшифровки, Истина);
		
		ПроцессорВывода = Новый ПроцессорВыводаРезультатаКомпоновкиДанныхВКоллекциюЗначений;
		ПроцессорВывода.УстановитьОбъект(ДанныеОтчета);
		
		// Обозначим начало вывода
		ПроцессорВывода.Вывести(ПроцессорКомпоновки, Истина);
		
		ВывестиМакетЧисленностьРаботающихИЗабронированныхГражданЗапаса(ДокументРезультат, ДанныеОтчета, ДатаОтчета);
		
		
		
	ИначеЕсли КлючВарианта = "ДонесениеОКоличествеГражданВЗапасе" Тогда
		
		// Параметры документа
		ДокументРезультат.ТолькоПросмотр = Истина;
		ДокументРезультат.ИмяПараметровПечати = "ПАРАМЕТРЫ_ПЕЧАТИ_ДонесениеОКоличествеГражданВЗапасе";
		ДокументРезультат.ОриентацияСтраницы = ОриентацияСтраницы.Портрет;
		
		ДатаОтчета = '00010101';
		
		УстановитьДатуОтчета(НастройкиОтчета, ДатаОтчета);
		
		ДанныеОтчета = Новый ДеревоЗначений;
		
		КомпоновщикМакета = Новый КомпоновщикМакетаКомпоновкиДанных;
		МакетКомпоновки = КомпоновщикМакета.Выполнить(ЭтотОбъект.СхемаКомпоновкиДанных, НастройкиОтчета, ДанныеРасшифровки,, Тип("ГенераторМакетаКомпоновкиДанныхДляКоллекцииЗначений"));
		
		// Создадим и инициализируем процессор компоновки.
		ПроцессорКомпоновки = Новый ПроцессорКомпоновкиДанных;
		ПроцессорКомпоновки.Инициализировать(МакетКомпоновки, , ДанныеРасшифровки, Истина);
		
		ПроцессорВывода = Новый ПроцессорВыводаРезультатаКомпоновкиДанныхВКоллекциюЗначений;
		ПроцессорВывода.УстановитьОбъект(ДанныеОтчета);
		
		// Обозначим начало вывода
		ПроцессорВывода.Вывести(ПроцессорКомпоновки, Истина);
		
		ВывестиМакетДонесениеОКоличествеГражданВЗапасе(ДокументРезультат, ДанныеОтчета, ДатаОтчета);
		
	ИначеЕсли КлючВарианта = "АнализОбеспеченностиТрудовымиРесурсами" Тогда
		
		// Параметры документа
		ДокументРезультат.ТолькоПросмотр = Истина;
		ДокументРезультат.ИмяПараметровПечати = "ПАРАМЕТРЫ_ПЕЧАТИ_АнализОбеспеченностиТрудовымиРесурсами";
		ДокументРезультат.ОриентацияСтраницы = ОриентацияСтраницы.Портрет;

		ДатаОтчета = '00010101';
		
		УстановитьДатуОтчета(НастройкиОтчета, ДатаОтчета);
		
		ДанныеОтчета = Новый ДеревоЗначений;
		
		КомпоновщикМакета = Новый КомпоновщикМакетаКомпоновкиДанных;
		МакетКомпоновки = КомпоновщикМакета.Выполнить(ЭтотОбъект.СхемаКомпоновкиДанных, НастройкиОтчета, ДанныеРасшифровки,, Тип("ГенераторМакетаКомпоновкиДанныхДляКоллекцииЗначений"));
		
		// Создадим и инициализируем процессор компоновки.
		ПроцессорКомпоновки = Новый ПроцессорКомпоновкиДанных;
		ПроцессорКомпоновки.Инициализировать(МакетКомпоновки, , ДанныеРасшифровки, Истина);
		
		ПроцессорВывода = Новый ПроцессорВыводаРезультатаКомпоновкиДанныхВКоллекциюЗначений;
		ПроцессорВывода.УстановитьОбъект(ДанныеОтчета);
		
		// Обозначим начало вывода
		ПроцессорВывода.Вывести(ПроцессорКомпоновки, Истина);
		
		//ДополнитьДанныеОтчетаАнализОбеспеченностиТрудовымиРесурсами(ДанныеОтчета, ДатаОтчета);
		
		ВывестиМакетАнализОбеспеченностиТрудовымиРесурсами(ДокументРезультат, ДанныеОтчета, ДатаОтчета);
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти


#Область СлужебныеПроцедурыИФункции

// Для общей формы "Форма отчета" подсистемы "Варианты отчетов".
Процедура ОпределитьНастройкиФормы(Форма, КлючВарианта, Настройки) Экспорт
	
	ЗарплатаКадрыОбщиеНаборыДанных.ЗаполнитьОбщиеИсточникиДанныхОтчета(ЭтотОбъект);
	ЗначениеВДанныеФормы(ЭтотОбъект, Форма.Отчет);
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// Функции формирования отчета по макету ЧисленностьРаботающихИЗабронированныхГражданЗапаса.

Процедура ВывестиМакетЧисленностьРаботающихИЗабронированныхГражданЗапаса(ДокументРезультат, ДанныеОтчета, ДатаОтчета)
	
	ДокументРезультат.ОриентацияСтраницы = ОриентацияСтраницы.Ландшафт;
	
	Макет 		  = УправлениеПечатью.МакетПечатнойФормы("Отчет.ВоинскийУчетБронирование.ПФ_MXL_ОтчетОЧисленностиРаботающихИЗабронированныхВоеннообязанных_Локализация");
	Заголовок 	  = Макет.ПолучитьОбласть("Заголовок");
	Шапка 		  = Макет.ПолучитьОбласть("Шапка");
	СтрокаТаблицы = Макет.ПолучитьОбласть("СтрокаТаблицы");
	ИТОГИ 		  = Макет.ПолучитьОбласть("Итоги");
	Подвал 		  = Макет.ПолучитьОбласть("Подвал");
	
	ИспользуетсяТрудПлавсостава = ПолучитьФункциональнуюОпцию("ИспользуетсяТрудПлавсостава");
	ИспользуетсяТрудЛетноПодъемногоСостава = ПолучитьФункциональнуюОпцию("ИспользуетсяТрудЛетноПодъемногоСостава");
	
	Для Каждого ДанныеОрганизации Из ДанныеОтчета.Строки Цикл
		
		ПараметрыЗаголовка = ПараметрыЗаголовкаСтруктура(ДанныеОрганизации.Организация, ДатаОтчета);
		
		ЗаполнитьЗначенияСвойств(Заголовок.Параметры, ПараметрыЗаголовка);
		Заголовок.Параметры.ДатаОтчета = Формат(ДатаОтчета, "ДФ=гггг");
		ЗаполнитьЗначенияСвойств(Подвал.Параметры, ПараметрыЗаголовка);
		
		ДокументРезультат.Вывести(Заголовок);
		ДокументРезультат.Вывести(Шапка);
		
		СтрокиОтчета = СформироватьТаблицуСтрокиОтчета();
		
		ЗаполнитьЗначенияСвойств(Итоги.Параметры, ДанныеОрганизации);
		
		Для Каждого СтрокаГруппировки Из ДанныеОрганизации.Строки Цикл
			
			Если ЗначениеЗаполнено(СтрокаГруппировки.ФизическоеЛицо) Тогда 
				Прервать;
			КонецЕсли;

			
			Если ИспользуетсяТрудПлавсостава И СтрокаГруппировки.Плавсостав = Истина Тогда 
				
				ЗаполнитьЗначенияСвойств(СтрокиОтчета[21], СтрокаГруппировки);
				
			ИначеЕсли ИспользуетсяТрудЛетноПодъемногоСостава И СтрокаГруппировки.ЛетноПодъемныйСостав = Истина Тогда 
				
				ЗаполнитьЗначенияСвойств(СтрокиОтчета[20], СтрокаГруппировки);
				
			Иначе 
				
				ЗаполнитьДанныеГруппировки(СтрокаГруппировки, СтрокиОтчета);

			КонецЕсли;
			
		КонецЦикла;
		
		СтрокиОтчета[7].ВсегоРаботающих 						  = СтрокиОтчета[8].ВсегоРаботающих 						  + СтрокиОтчета[9].ВсегоРаботающих;
		СтрокиОтчета[7].ВсегоВЗапасе 							  = СтрокиОтчета[8].ВсегоВЗапасе 							  + СтрокиОтчета[9].ВсегоВЗапасе;
		СтрокиОтчета[7].ОфицеровВЗапасе 						  = СтрокиОтчета[8].ОфицеровВЗапасе 						  + СтрокиОтчета[9].ОфицеровВЗапасе;
		СтрокиОтчета[7].ПрапорщиковВЗапасе 						  = СтрокиОтчета[8].ПрапорщиковВЗапасе 						  + СтрокиОтчета[9].ПрапорщиковВЗапасе;
		СтрокиОтчета[7].СолдатВЗапасе 							  = СтрокиОтчета[8].СолдатВЗапасе 							  + СтрокиОтчета[9].СолдатВЗапасе;
		СтрокиОтчета[7].ПрапорщиковСолдатОграниченноГодныхВЗапасе = СтрокиОтчета[8].ПрапорщиковСолдатОграниченноГодныхВЗапасе + СтрокиОтчета[9].ПрапорщиковСолдатОграниченноГодныхВЗапасе;
		СтрокиОтчета[7].ВсегоВЗапасеЗабронировано 				  = СтрокиОтчета[8].ВсегоВЗапасеЗабронировано 				  + СтрокиОтчета[9].ВсегоВЗапасеЗабронировано;
		СтрокиОтчета[7].ОфицеровЗабронировано 					  = СтрокиОтчета[8].ОфицеровЗабронировано 					  + СтрокиОтчета[9].ОфицеровЗабронировано;
		СтрокиОтчета[7].ПрапорщиковСолдатЗабронировано			  = СтрокиОтчета[8].ПрапорщиковСолдатЗабронировано 			  + СтрокиОтчета[9].ПрапорщиковСолдатЗабронировано;
		СтрокиОтчета[7].НезабронированоБезМобпредписаний 		  = СтрокиОтчета[8].НезабронированоБезМобпредписаний 		  + СтрокиОтчета[9].НезабронированоБезМобпредписаний;
		СтрокиОтчета[7].Призывников 							  = СтрокиОтчета[8].Призывников 						      + СтрокиОтчета[9].Призывников; 		
		СтрокиОтчета[7].До30Лет 								  = СтрокиОтчета[8].До30Лет 						      	  + СтрокиОтчета[9].До30Лет;
		СтрокиОтчета[7].От31До35Лет 							  = СтрокиОтчета[8].От31До35Лет 						      + СтрокиОтчета[9].От31До35Лет;
		СтрокиОтчета[7].От36До40Лет 							  = СтрокиОтчета[8].От36До40Лет 						      + СтрокиОтчета[9].От36До40Лет;
		СтрокиОтчета[7].От41До45Лет 							  = СтрокиОтчета[8].От41До45Лет 						      + СтрокиОтчета[9].От41До45Лет;
		СтрокиОтчета[7].От46До50Лет 							  = СтрокиОтчета[8].От46До50Лет 						      + СтрокиОтчета[9].От46До50Лет;
		СтрокиОтчета[7].От51До55Лет 							  = СтрокиОтчета[8].От51До55Лет 						      + СтрокиОтчета[9].От51До55Лет;
				
		Для Каждого СтрокаОтчета Из СтрокиОтчета Цикл
			
			ЗаполнитьЗначенияСвойств(СтрокаТаблицы.Параметры, СтрокаОтчета);
			ДокументРезультат.Вывести(СтрокаТаблицы);
			
		КонецЦикла;
		
		ДокументРезультат.Вывести(Итоги);
		
		ЗаполнитьПодписантов(Подвал, ДанныеОрганизации.Организация, ДатаОтчета);
		ДокументРезультат.Вывести(Подвал);
		
		ДокументРезультат.ВывестиГоризонтальныйРазделительСтраниц();
		
	КонецЦикла;
	
КонецПроцедуры

Процедура ЗаполнитьДанныеГруппировки(СтрокаГруппировки, СтрокиОтчета)
	
	СтрокаОтчета = СтрокиОтчета.Найти(СтрокаГруппировки.КатегорияВоинскогоУчета, "Категория");
	
	Если СтрокаОтчета <> Неопределено Тогда 
		ЗаполнитьЗначенияСвойств(СтрокаОтчета, СтрокаГруппировки);
	КонецЕсли;
	
	Для Каждого ТекСтрока Из СтрокаГруппировки.Строки Цикл 
		
		СтрокаОтчета = СтрокиОтчета.Найти(ТекСтрока.КатегорияУчетаЗабронированных, "Категория");
		
		Если СтрокаОтчета <> Неопределено Тогда 
			ЗаполнитьЗначенияСвойств(СтрокаОтчета, ТекСтрока);
		КонецЕсли;
		
	КонецЦикла;
	
КонецПроцедуры

Процедура ОписаниеСтрокиДанных(ТДанных, НомерСтрокиОтчета, ИмяСтрокиОтчета, Категория)
	
	Строка = ТДанных.Добавить();
	Строка.НомерСтрокиОтчета = НомерСтрокиОтчета;
	Строка.ИмяСтрокиОтчета = ИмяСтрокиОтчета;
	Строка.Категория = Категория;

КонецПроцедуры

Функция СформироватьТаблицуСтрокиОтчета()
	
	ТДанных = Новый ТаблицаЗначений;
	
	ТДанных.Колонки.Добавить("НомерСтрокиОтчета", Новый ОписаниеТипов("Строка", , Новый КвалификаторыСтроки(10)));
	ТДанных.Колонки.Добавить("ИмяСтрокиОтчета",   Новый ОписаниеТипов("Строка", , Новый КвалификаторыСтроки(100)));
	ТДанных.Колонки.Добавить("Категория", 		  Новый ОписаниеТипов("ПеречислениеСсылка.КатегорииДолжностейДляВоинскогоУчета, ПеречислениеСсылка.КатегорииДолжностейДляУчетаЗабронированныхЛокализация"));
	
	ТипЧисло = Новый ОписаниеТипов("Число", Новый КвалификаторыЧисла(15));
	
	ТДанных.Колонки.Добавить("ВсегоРаботающих", 						  ТипЧисло);
	ТДанных.Колонки.Добавить("ВсегоВЗапасе", 							  ТипЧисло);
	ТДанных.Колонки.Добавить("ОфицеровВЗапасе", 						  ТипЧисло);
	ТДанных.Колонки.Добавить("ПрапорщиковВЗапасе", 				 		  ТипЧисло);
	ТДанных.Колонки.Добавить("СолдатВЗапасе", 				  			  ТипЧисло);
	ТДанных.Колонки.Добавить("ПрапорщиковСолдатОграниченноГодныхВЗапасе", ТипЧисло);
	ТДанных.Колонки.Добавить("ВсегоВЗапасеЗабронировано", 				  ТипЧисло);
	ТДанных.Колонки.Добавить("ОфицеровЗабронировано", 					  ТипЧисло);
	ТДанных.Колонки.Добавить("ПрапорщиковСолдатЗабронировано", 			  ТипЧисло);
	ТДанных.Колонки.Добавить("НезабронированоБезМобпредписаний", 		  ТипЧисло);
	ТДанных.Колонки.Добавить("Призывников", 							  ТипЧисло);	
	ТДанных.Колонки.Добавить("До30Лет", 							 	  ТипЧисло);
	ТДанных.Колонки.Добавить("От31До35Лет", 							  ТипЧисло);
	ТДанных.Колонки.Добавить("От36До40Лет", 							  ТипЧисло);
	ТДанных.Колонки.Добавить("От41До45Лет", 							  ТипЧисло);
	ТДанных.Колонки.Добавить("От46До50Лет", 							  ТипЧисло);
	ТДанных.Колонки.Добавить("От51До55Лет", 							  ТипЧисло);
	
	КатегорииВоинскогоУчета 	  = Перечисления.КатегорииДолжностейДляВоинскогоУчета;
	КатегорииУчетаЗабронированных = Перечисления.КатегорииДолжностейДляУчетаЗабронированныхЛокализация;
	
	ОписаниеСтрокиДанных(ТДанных, "1",  НСтр("ru='Руководители'"), 													КатегорииВоинскогоУчета.Руководители);  	
	
	ОписаниеСтрокиДанных(ТДанных, "2",  НСтр("ru='Специалисты - всего, из них: '"), 								КатегорииВоинскогоУчета.Специалисты);  	
	ОписаниеСтрокиДанных(ТДанных, "3",  НСтр("ru='сельского хозяйства'"),			    							КатегорииУчетаЗабронированных.СпециалистыСХ);
	ОписаниеСтрокиДанных(ТДанных, "4",  НСтр("ru='торговли и общественного питания'"), 								КатегорииУчетаЗабронированных.СпециалистыТорговли);
	ОписаниеСтрокиДанных(ТДанных, "5",  НСтр("ru='науки'"), 														КатегорииУчетаЗабронированных.СпециалистыНауки);
	ОписаниеСтрокиДанных(ТДанных, "6",  НСтр("ru='культуры и искусства'"), 											КатегорииУчетаЗабронированных.СпециалистыКультуры);
	ОписаниеСтрокиДанных(ТДанных, "7",  НСтр("ru='образования'"),													КатегорииУчетаЗабронированных.СпециалистыОбразования);
	ОписаниеСтрокиДанных(ТДанных, "8",  НСтр("ru='Здравоохранения - всего, в том числе: (9+10)'"),					Неопределено);
	ОписаниеСтрокиДанных(ТДанных, "9",  НСтр("ru='врачи'"), 														КатегорииУчетаЗабронированных.СпециалистыВрачи);
	ОписаниеСтрокиДанных(ТДанных, "10", НСтр("ru='средний медицинский персонал'"),									КатегорииУчетаЗабронированных.СпециалистыСреднийМедецинскийПерсонал);
	
	ОписаниеСтрокиДанных(ТДанных, "11", НСтр("ru='Другие служащие - всего'"),										КатегорииВоинскогоУчета.ДругиеСлужащие);
	
	ОписаниеСтрокиДанных(ТДанных, "12", НСтр("ru='Рабочие - всего, в том числе: (сумма с 13 по 20)'"), 				КатегорииВоинскогоУчета.Рабочие);
	ОписаниеСтрокиДанных(ТДанных, "13", НСтр("ru='1 - 2-го разряда'"), 												КатегорииУчетаЗабронированных.РабочиеПервогоВторогоРазрядов);
	ОписаниеСтрокиДанных(ТДанных, "14", НСтр("ru='3 - 4-го разряда'"),												КатегорииУчетаЗабронированных.РабочиеТретьегоЧетсвертогоРазрядов);
	ОписаниеСтрокиДанных(ТДанных, "15", НСтр("ru='5-го разряда и выше'"),											КатегорииУчетаЗабронированных.РабочиеПятогоРазряда);
	ОписаниеСтрокиДанных(ТДанных, "16",	НСтр("ru='не имеющие тарифных разрядов'"),									КатегорииУчетаЗабронированных.РабочиеНеИмеющиеТарифныхРазрядов);
	ОписаниеСтрокиДанных(ТДанных, "17",	НСтр("ru='сельскохозяйственного производства'"),							КатегорииУчетаЗабронированных.РабочиеСХ);
	ОписаниеСтрокиДанных(ТДанных, "18",	НСтр("ru='локомотивных бригад'"), 											КатегорииУчетаЗабронированных.РабочиеЛокомотивныхБригад);
	ОписаниеСтрокиДанных(ТДанных, "19", НСтр("ru='водители'"),														КатегорииУчетаЗабронированных.Водители);
	ОписаниеСтрокиДанных(ТДанных, "20",	НСтр("ru='трактористы'"), 													КатегорииУчетаЗабронированных.Трактористы);
	ОписаниеСтрокиДанных(ТДанных, "21",	НСтр("ru='Из численности руководителей,специалистов и рабочих - летно-подъемный состав'"),Неопределено);
	ОписаниеСтрокиДанных(ТДанных, "22",	НСтр("ru='плавающий состав'"),												Неопределено);
	ОписаниеСтрокиДанных(ТДанных, "23",	НСтр("ru='Учащиеся'"),														КатегорииВоинскогоУчета.Учащиеся);

	
	Возврат ТДанных;
	
КонецФункции

////////////////////////////////////////////////////////////////////////////////
// Функции формирования отчета по макету ДонесениеОКоличествеГражданВЗапасе.

Процедура ВывестиМакетДонесениеОКоличествеГражданВЗапасе(ДокументРезультат, ДанныеОтчета, ДатаОтчета)
	
	ДокументРезультат.ОриентацияСтраницы = ОриентацияСтраницы.Ландшафт;
	
	Макет 		  = УправлениеПечатью.МакетПечатнойФормы("Отчет.ВоинскийУчетБронирование.ПФ_MXL_ДонесениеОКоличествеГражданВЗапасе");
	Заголовок 	  = Макет.ПолучитьОбласть("Заголовок");
	Шапка 		  = Макет.ПолучитьОбласть("Шапка");
	СтрокаТаблицы = Макет.ПолучитьОбласть("СтрокаТаблицы");
	Подвал 		  = Макет.ПолучитьОбласть("Подвал");
	
	Для Каждого ДанныеОрганизации Из ДанныеОтчета.Строки Цикл
		
		ПараметрыЗаголовка = ПараметрыЗаголовкаСтруктура(ДанныеОрганизации.Организация, ДатаОтчета);
		
		ЗаполнитьЗначенияСвойств(Заголовок.Параметры, ПараметрыЗаголовка);
		
		ДокументРезультат.Вывести(Заголовок);
		ДокументРезультат.Вывести(Шапка);
		
		ЗаполнитьЗначенияСвойств(СтрокаТаблицы.Параметры, ДанныеОрганизации);
		
		//СтрокаТаблицы.Параметры.ЗабронированоПроцент 					= ?(ЗначениеЗаполнено(ДанныеОрганизации.ВсегоВЗапасе), ДанныеОрганизации.ВсегоВЗапасеЗабронировано * 100 / ДанныеОрганизации.ВсегоВЗапасе, 0);
		//СтрокаТаблицы.Параметры.НезабронированоСМобпредписаниемПроцент = ?(ЗначениеЗаполнено(ДанныеОрганизации.ВсегоВЗапасе), ДанныеОрганизации.НезабронированоСМобпредписанием * 100 / ДанныеОрганизации.ВсегоВЗапасе, 0);
		
		ДокументРезультат.Вывести(СтрокаТаблицы);
		
		ЗаполнитьЗначенияСвойств(Подвал.Параметры, ПараметрыЗаголовка);
		
		ЗаполнитьПодписантов(Подвал, ДанныеОрганизации.Организация, ДатаОтчета);
		ДокументРезультат.Вывести(Подвал);
		
		ДокументРезультат.ВывестиГоризонтальныйРазделительСтраниц();
			
	КонецЦикла;
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// Функции формирования отчета по макету АнализОбеспеченностиТрудовымиРесурсами.

Процедура ВывестиМакетАнализОбеспеченностиТрудовымиРесурсами(ДокументРезультат, ДанныеОтчета, ДатаОтчета)
	
	ДокументРезультат.ОриентацияСтраницы = ОриентацияСтраницы.Ландшафт;
	
	Макет 		  = УправлениеПечатью.МакетПечатнойФормы("Отчет.ВоинскийУчетБронирование.ПФ_MXL_АнализОбеспеченностиТрудовымиРесурсами");
	Заголовок 	  = Макет.ПолучитьОбласть("Заголовок");
	Шапка 		  = Макет.ПолучитьОбласть("Шапка");
	СтрокаТаблицы = Макет.ПолучитьОбласть("СтрокаТаблицы");
	Подвал 		  = Макет.ПолучитьОбласть("Подвал");
	
	Для Каждого ДанныеОрганизации Из ДанныеОтчета.Строки Цикл
		
		ПараметрыЗаголовка = ПараметрыЗаголовкаСтруктура(ДанныеОрганизации.Организация, ДатаОтчета);
		
		ЗаполнитьЗначенияСвойств(Заголовок.Параметры, ПараметрыЗаголовка);
		
		ДокументРезультат.Вывести(Заголовок);
		ДокументРезультат.Вывести(Шапка);
		
		СтрокиОтчета = СформироватьТаблицуКатегорийДолжностей();
		
		СтрокаИтог = СтрокиОтчета[5];
		
		ЗаполнитьПлановыеДанныеПоОрганизации(СтрокиОтчета, ДанныеОрганизации.Организация, ДатаОтчета);
		
		ЗаполнитьЗначенияСвойств(СтрокаИтог, ДанныеОрганизации);
		
		Для Каждого ТекСтрока Из ДанныеОрганизации.Строки Цикл 
			
			Если ТекСтрока.КатегорияУчетаЗабронированных = Перечисления.КатегорииДолжностейДляУчетаЗабронированных.Водители Тогда 
				СтрокаОтчета = СтрокиОтчета[4];
			Иначе 
				СтрокаОтчета = СтрокиОтчета.Найти(ТекСтрока.КатегорияВоинскогоУчета, "Категория");
			КонецЕсли;
			
			Если СтрокаОтчета <> Неопределено Тогда 
				ЗаполнитьЗначенияСвойств(СтрокаОтчета, ТекСтрока);
			КонецЕсли;
			
		КонецЦикла;
		
		Для Каждого СтрокаОтчета Из СтрокиОтчета Цикл
			
			СтрокаОтчета.ИзбытокНекомплект = СтрокаОтчета.ОстаетсяРаботать - СтрокаОтчета.Потребность;
			ЗаполнитьЗначенияСвойств(СтрокаТаблицы.Параметры, СтрокаОтчета);
			ДокументРезультат.Вывести(СтрокаТаблицы);
			
		КонецЦикла;
		
		Если ДанныеОрганизации.Строки.Количество() > 0 Тогда 
			Подвал.Параметры.ПроцентОбеспеченности = 100 + ?(ЗначениеЗаполнено(СтрокаИтог.Потребность), СтрокаИтог.ИзбытокНекомплект / СтрокаИтог.Потребность, 0) * 100;
		КонецЕсли;
		
		ЗаполнитьЗначенияСвойств(Подвал.Параметры, ПараметрыЗаголовка);
		
		ЗаполнитьПодписантов(Подвал, ДанныеОрганизации.Организация, ДатаОтчета);
		ДокументРезультат.Вывести(Подвал);
		
		ДокументРезультат.ВывестиГоризонтальныйРазделительСтраниц();
			
	КонецЦикла;

КонецПроцедуры

Процедура ЗаполнитьПлановыеДанныеПоОрганизации(СтрокиОтчета, Организация, ДатаОтчета)

	Запрос = Новый Запрос;
	
	Запрос.УстановитьПараметр("Организация", Организация);
	Запрос.УстановитьПараметр("Период", ДатаОтчета);
	
	Запрос.Текст = "ВЫБРАТЬ
	               |	ПотребностьВСпециалистахНаПериодМобилизации.КатегорияВоинскогоУчета КАК Категория,
	               |	ПотребностьВСпециалистахНаПериодМобилизации.Количество КАК Потребность
	               |ИЗ
	               |	РегистрСведений.ПотребностьВСпециалистахНаПериодМобилизации КАК ПотребностьВСпециалистахНаПериодМобилизации
	               |ГДЕ
	               |	ПотребностьВСпециалистахНаПериодМобилизации.Организация = &Организация
	               |	И ПотребностьВСпециалистахНаПериодМобилизации.Год = ГОД(&Период)";
				   
	Выборка = Запрос.Выполнить().Выбрать();
	
	СтрокаИтог = СтрокиОтчета[5];
	ПотребностьИтог = 0;
	Водители = Перечисления.КатегорииДолжностейДляУчетаЗабронированных.Водители;
	
	Пока Выборка.Следующий() Цикл 
		
		СтрокаОтчета = СтрокиОтчета.Найти(Выборка.Категория, "Категория");
		
		Если СтрокаОтчета <> Неопределено Тогда 
			ЗаполнитьЗначенияСвойств(СтрокаОтчета, Выборка);
			ПотребностьИтог = ?(Выборка.Категория = Водители, ПотребностьИтог, ПотребностьИтог + Выборка.Потребность);
		КонецЕсли;
		
	КонецЦикла;
	
	СтрокаИтог.Потребность = ПотребностьИтог;
	
КонецПроцедуры

Функция СформироватьТаблицуКатегорийДолжностей() 
	
	ТДанных = Новый ТаблицаЗначений;
	
	ТДанных.Колонки.Добавить("НомерСтрокиОтчета", Новый ОписаниеТипов("Строка", , Новый КвалификаторыСтроки(10)));
	ТДанных.Колонки.Добавить("ИмяСтрокиОтчета",   Новый ОписаниеТипов("Строка", , Новый КвалификаторыСтроки(100)));
	ТДанных.Колонки.Добавить("Категория", 		  Новый ОписаниеТипов("ПеречислениеСсылка.КатегорииДолжностейДляВоинскогоУчета, ПеречислениеСсылка.КатегорииДолжностейДляУчетаЗабронированных"));
	
	ТипЧисло = Новый ОписаниеТипов("Число", Новый КвалификаторыЧисла(15));
	
	ТДанных.Колонки.Добавить("ВсегоРаботающих", 	ТипЧисло);
	ТДанных.Колонки.Добавить("ВсегоВЗапасе", 		ТипЧисло);
	ТДанных.Колонки.Добавить("ПодлежитМобилизации", ТипЧисло);
	ТДанных.Колонки.Добавить("ОстаетсяРаботать", 	ТипЧисло);
	ТДанных.Колонки.Добавить("Потребность", 		ТипЧисло);
	ТДанных.Колонки.Добавить("ИзбытокНекомплект", 	ТипЧисло);
	
	Категории = Перечисления.КатегорииДолжностейДляВоинскогоУчета;
	
	ОписаниеСтрокиДанных(ТДанных, "1", НСтр("ru='Руководители'"), 			Категории.Руководители);
	ОписаниеСтрокиДанных(ТДанных, "2", НСтр("ru='Специалисты'"), 			Категории.Специалисты);
	ОписаниеСтрокиДанных(ТДанных, "3", НСтр("ru='Другие служащие'"), 		Категории.ДругиеСлужащие);
	ОписаниеСтрокиДанных(ТДанных, "4", НСтр("ru='Рабочие'"), 				Категории.Рабочие);
	ОписаниеСтрокиДанных(ТДанных, "5", НСтр("ru='       из них водители'"), Перечисления.КатегорииДолжностейДляУчетаЗабронированных.Водители);
	ОписаниеСтрокиДанных(ТДанных, "6", НСтр("ru='ВСЕГО'"), 					Неопределено);
	
	Возврат ТДанных;
	
КонецФункции	

////////////////////////////////////////////////////////////////////////////////
// Универсальные процедуры и Функции.

Процедура УстановитьДатуОтчета(НастройкиОтчета, ДатаОтчета)
	
	ЗначениеПараметраПериод = НастройкиОтчета.ПараметрыДанных.НайтиЗначениеПараметра(Новый ПараметрКомпоновкиДанных("Период"));
	
	Если ЗначениеПараметраПериод <> Неопределено Тогда
		
		УстановитьДатуОтчета = Ложь;
		
		Если ТипЗнч(ЗначениеПараметраПериод.Значение) = Тип("Неопределено") Тогда
			УстановитьДатуОтчета = Истина;
		КонецЕсли;
		
		Если ТипЗнч(ЗначениеПараметраПериод.Значение) = Тип("Дата")
			И ЗначениеПараметраПериод.Значение = '00010101' Тогда
			УстановитьДатуОтчета = Истина;
		КонецЕсли; 
		
		Если ТипЗнч(ЗначениеПараметраПериод.Значение) = Тип("СтандартнаяДатаНачала")
			И Дата(ЗначениеПараметраПериод.Значение) = '00010101' Тогда
			УстановитьДатуОтчета = Истина;
		КонецЕсли; 
		
		Если УстановитьДатуОтчета Тогда
			ЗначениеПараметраПериод.Значение = ТекущаяДатаСеанса();
		КонецЕсли; 
		
		ДатаОтчета = Дата(ЗначениеПараметраПериод.Значение);
		
	КонецЕсли;
	
КонецПроцедуры

Процедура УстановитьЗначенияПараметров(НастройкиОтчета)
	
	Специалисты = Новый Массив;
	Специалисты.Добавить(Перечисления.КатегорииДолжностейДляУчетаЗабронированных.СпециалистыЗдравоохранение);
	Специалисты.Добавить(Перечисления.КатегорииДолжностейДляУчетаЗабронированных.СпециалистыОбрабатывающиеПроизводства);
	Специалисты.Добавить(Перечисления.КатегорииДолжностейДляУчетаЗабронированных.СпециалистыОбразования);
	Специалисты.Добавить(Перечисления.КатегорииДолжностейДляУчетаЗабронированных.СпециалистыПолезныеИскопаемые);
	Специалисты.Добавить(Перечисления.КатегорииДолжностейДляУчетаЗабронированных.СпециалистыПроизводствоЭлектроэнергии);
	Специалисты.Добавить(Перечисления.КатегорииДолжностейДляУчетаЗабронированных.СпециалистыСтроительство);
	Специалисты.Добавить(Перечисления.КатегорииДолжностейДляУчетаЗабронированных.СпециалистыСХ);
	Специалисты.Добавить(Перечисления.КатегорииДолжностейДляУчетаЗабронированных.СпециалистыТранспорт);
	Специалисты.Добавить(Перечисления.КатегорииДолжностейДляУчетаЗабронированных.ПрочиеСпециалисты);
	
	Рабочие = Новый Массив;
	Рабочие.Добавить(Перечисления.КатегорииДолжностейДляУчетаЗабронированных.РабочиеИмеющиеТарифныйРазряд);
	Рабочие.Добавить(Перечисления.КатегорииДолжностейДляУчетаЗабронированных.РабочиеЛокомотивныхБригад);
	Рабочие.Добавить(Перечисления.КатегорииДолжностейДляУчетаЗабронированных.РабочиеНеИмеющиеТарифныхРазрядов);
	Рабочие.Добавить(Перечисления.КатегорииДолжностейДляУчетаЗабронированных.РабочиеСХ);
	Рабочие.Добавить(Перечисления.КатегорииДолжностейДляУчетаЗабронированных.Трактористы);
	Рабочие.Добавить(Перечисления.КатегорииДолжностейДляУчетаЗабронированных.Водители);
	
	ПараметрСпециалисты = НастройкиОтчета.ПараметрыДанных.НайтиЗначениеПараметра(Новый ПараметрКомпоновкиДанных("Специалисты"));	
	
	Если ПараметрСпециалисты <> Неопределено Тогда
		ПараметрСпециалисты.Значение = Специалисты;
		ПараметрСпециалисты.Использование = Истина;
	КонецЕсли;
	
	ПараметрРабочие = НастройкиОтчета.ПараметрыДанных.НайтиЗначениеПараметра(Новый ПараметрКомпоновкиДанных("Рабочие"));	
	
	Если ПараметрРабочие <> Неопределено Тогда
		ПараметрРабочие.Значение = Рабочие;
		ПараметрРабочие.Использование = Истина;
	КонецЕсли;
	
КонецПроцедуры

Функция ПараметрыЗаголовкаСтруктура(Организация, ДатаОтчета)
	
	ПараметрыЗаголовка = ОбщегоНазначения.ЗначенияРеквизитовОбъекта(Организация, "Наименование, НаименованиеПолное");
	
	ПолноеНаименованиеОрганизации = ?(ЗначениеЗаполнено(ПараметрыЗаголовка.НаименованиеПолное), ПараметрыЗаголовка.НаименованиеПолное, ПараметрыЗаголовка.Наименование);
	
	ПараметрыЗаголовка.Вставить("ДатаОтчета", ДатаОтчета);
	ПараметрыЗаголовка.Вставить("Организация", Организация);
	ПараметрыЗаголовка.Вставить("ПолноеНаименованиеОрганизации", ПолноеНаименованиеОрганизации);
	
	Возврат ПараметрыЗаголовка;
	
КонецФункции

Процедура ЗаполнитьПодписантов(Макет, Организация, ДатаОтчета)
	
	ПараметрыЗаполнения = Новый Структура("ДолжностьРуководителя,Руководитель,РуководительРасшифровкаПодписи");
	КлючиОтветственныхЛиц = "";
	
	НастройкиОтчета = ЭтотОбъект.КомпоновщикНастроек.ПолучитьНастройки();
	
	ПараметрРуководитель = НастройкиОтчета.ПараметрыДанных.НайтиЗначениеПараметра(Новый ПараметрКомпоновкиДанных("Руководитель"));	
	Если ПараметрРуководитель <> Неопределено И ПараметрРуководитель.Использование Тогда
		Если ЗначениеЗаполнено(ПараметрРуководитель.Значение) Тогда
			ПараметрыЗаполнения.Руководитель = ПараметрРуководитель.Значение;
		КонецЕсли; 
	Иначе
		КлючиОтветственныхЛиц = "Руководитель";
	КонецЕсли;
	
	ПараметрДолжностьРуководителя = НастройкиОтчета.ПараметрыДанных.НайтиЗначениеПараметра(Новый ПараметрКомпоновкиДанных("ДолжностьРуководителя"));	
	Если ПараметрДолжностьРуководителя <> Неопределено И ПараметрДолжностьРуководителя.Использование Тогда
		ПараметрыЗаполнения.ДолжностьРуководителя = ПараметрДолжностьРуководителя.Значение;
	Иначе
		КлючиОтветственныхЛиц = ?(ПустаяСтрока(КлючиОтветственныхЛиц), "", КлючиОтветственныхЛиц + ",") + "ДолжностьРуководителяСтрокой";
	КонецЕсли;
	
	Если Не ПустаяСтрока(КлючиОтветственныхЛиц) Тогда
		
		ОтветственныеЛица = Новый Структура("Организация," + КлючиОтветственныхЛиц, Организация);
		ЗарплатаКадры.ПолучитьЗначенияПоУмолчанию(ОтветственныеЛица, ДатаОтчета);
		
		ЗаполнитьЗначенияСвойств(ПараметрыЗаполнения, ОтветственныеЛица);
		Если ОтветственныеЛица.Свойство("ДолжностьРуководителяСтрокой") Тогда
			ПараметрыЗаполнения.ДолжностьРуководителя = ОтветственныеЛица.ДолжностьРуководителяСтрокой;
		КонецЕсли; 
		
	КонецЕсли; 
	
	МассивФизЛиц = Новый Массив;
	Если ЗначениеЗаполнено(ПараметрыЗаполнения.Руководитель) Тогда
		МассивФизЛиц.Добавить(ПараметрыЗаполнения.Руководитель);
	КонецЕсли; 
		
	Если МассивФизЛиц.Количество() > 0 Тогда
		
		ФИОФизЛиц = ЗарплатаКадры.СоответствиеФИОФизЛицСсылкам(ДатаОтчета, МассивФизЛиц);
		ПараметрыЗаполнения.РуководительРасшифровкаПодписи = ФизическиеЛицаЗарплатаКадры.РасшифровкаПодписи(ФИОФизЛиц[ПараметрыЗаполнения.Руководитель]);

	КонецЕсли; 
	
	Макет.Параметры.Заполнить(ПараметрыЗаполнения);
	
КонецПроцедуры

#КонецОбласти

#КонецЕсли

