﻿
Процедура НастроитьВариантыОтчета(Настройки, НастройкиОтчета) Экспорт
	НастройкиВарианта = ВариантыОтчетов.ОписаниеВарианта(Настройки, НастройкиОтчета, "ДетализацияЗатратПоЗаказам");
	НастройкиВарианта.Описание = НСтр("ru = 'Отчет по детализации затрат по заказам на производство'");
КонецПроцедуры

	
Функция ДобавитьКомандуОтчета(КомандыОтчетов) Экспорт
		
		КомандаОтчет = КомандыОтчетов.Добавить();
		
		КомандаОтчет.Идентификатор               = Метаданные.Отчеты.ЭЭ_ДетализацияЗатратПоЗаказамНаПроизводство.ПолноеИмя();
		КомандаОтчет.Представление               = "Детализация затрат по заказам";
		КомандаОтчет.Менеджер                    = "Отчет.ЭЭ_ДетализацияЗатратПоЗаказамНаПроизводство";
		КомандаОтчет.МножественныйВыбор = Ложь;
		КомандаОтчет.Важность      = "Обычное";
		
		Возврат КомандаОтчет;
			
КонецФункции	
