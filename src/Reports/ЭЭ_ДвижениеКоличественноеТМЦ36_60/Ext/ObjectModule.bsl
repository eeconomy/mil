﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ОбработчикиСобытий

Процедура ПриКомпоновкеРезультата(ДокументРезультат, ДанныеРасшифровки, СтандартнаяОбработка)

	КомпоновщикНастроекФормы = КомпоновщикНастроек.ПолучитьНастройки();
	
	МассивДопРеквизитов = Новый Массив;
	МассивДопРеквизитов.Добавить("Чд_Цвет(справочник)");
	
	СоответствиеДопРеквизитов = Чд_ОбщегоНазначенияСервер.ДопРеквизитыПоИмени(МассивДопРеквизитов);
	КомпоновщикНастроекФормы.ПараметрыДанных.УстановитьЗначениеПараметра("ДопРеквизитЦвет", СоответствиеДопРеквизитов["Чд_Цвет(справочник)"]);
	КомпоновщикНастроекФормы.ПараметрыДанных.УстановитьЗначениеПараметра("ПодразделениеШвейныйЦех",
		Справочники.ЭЭ_ПредопределенныеЗначения.ШвейныйЦехНомерПять.Значение);
	
	КомпоновщикНастроек.ЗагрузитьНастройки(КомпоновщикНастроекФормы);
	
КонецПроцедуры

#КонецОбласти 
	
#КонецЕсли