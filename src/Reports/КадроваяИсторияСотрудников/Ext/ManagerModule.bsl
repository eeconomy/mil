﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

Процедура НастроитьВариантыОтчета(Настройки, НастройкиОтчета) Экспорт
	
	//НастройкиОтчета.ОпределитьНастройкиФормы = Истина;
	
	НастройкиВарианта = ВариантыОтчетов.ОписаниеВарианта(Настройки, НастройкиОтчета, "КадровыеИзменения");
	НастройкиВарианта.Описание = НСтр("ru = 'Приемы на работу, увольнения, переводы и перемещения между подразделениями'");
	
	НастройкиВарианта = ВариантыОтчетов.ОписаниеВарианта(Настройки, НастройкиОтчета, "СводКадровыхИзменений");
	НастройкиВарианта.Описание = НСтр("ru = 'Количество приемов, увольнений, переводов с точностью до позиции штатного расписания'");
	
	НастройкиВарианта = ВариантыОтчетов.ОписаниеВарианта(Настройки, НастройкиОтчета, "КадроваяИсторияСотрудников");
	НастройкиВарианта.Описание = НСтр("ru = 'Кадровая история сотрудников'");
	
КонецПроцедуры

#Область ПрограммныйИнтерфейс

#Область ДляВызоваИзДругихПодсистем

// Конец СтандартныеПодсистемы.ВариантыОтчетов

#КонецОбласти

#КонецОбласти

#КонецЕсли