﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ОбработчикиСобытий


Процедура ПриКомпоновкеРезультата(ДокументРезультат, ДанныеРасшифровки, СтандартнаяОбработка)
		
	ЗарплатаКадрыОбщиеНаборыДанных.ЗаполнитьОбщиеИсточникиДанныхОтчета(ЭтотОбъект);

	ДокументРезультат.Очистить();
	
	НастройкиОтчета = ЭтотОбъект.КомпоновщикНастроек.ПолучитьНастройки();
	
	КлючВарианта = ЗарплатаКадрыОтчеты.КлючВарианта(КомпоновщикНастроек);
	ПараметрПериод = НастройкиОтчета.ПараметрыДанных.НайтиЗначениеПараметра(Новый ПараметрКомпоновкиДанных("ПериодОтчета"));
	
	Если ПараметрПериод <> Неопределено Тогда
		НачалоПериода = ПараметрПериод.Значение.ДатаНачала;
		КонецПериода = ПараметрПериод.Значение.ДатаОкончания;
		
		ПараметрПериод.Использование = Истина;
	Иначе
		НачалоПериода = НачалоМесяца(ТекущаяДатаСеанса());
		КонецПериода = КонецМесяца(ТекущаяДатаСеанса());
		
		ПараметрПериод = НастройкиОтчета.ПараметрыДанных.Элементы.Добавить();
		ПараметрПериод.Значение = Новый СтандартныйПериод;
		ПараметрПериод.Значение.ДатаНачала = НачалоПериода;
		ПараметрПериод.Значение.ДатаОкончания = КонецПериода;
		ПараметрПериод.Использование = Истина;
	КонецЕсли;
		
	ПараметрДатаОтчета = НастройкиОтчета.ПараметрыДанных.НайтиЗначениеПараметра(Новый ПараметрКомпоновкиДанных("ДатаОтчета"));
	Если ПараметрДатаОтчета = Неопределено Тогда
		ДатаОтчета = ТекущаяДатаСеанса();
		ПараметрДатаОтчета = НастройкиОтчета.ПараметрыДанных.Элементы.Добавить();
		ПараметрДатаОтчета.Использование = Истина;
		ПараметрДатаОтчета.Значение = ДатаОтчета; 
	Иначе
		ПараметрДатаОтчета.Использование = Истина;
		Если ЗначениеЗаполнено(ПараметрДатаОтчета.Значение) Тогда
			Если ТипЗнч(ПараметрДатаОтчета.Значение) = Тип("СтандартнаяДатаНачала") Тогда			
				ДатаОтчета = ПараметрДатаОтчета.Значение.Дата;
			Иначе
				ДатаОтчета = ПараметрДатаОтчета.Значение;
			КонецЕсли;	
		Иначе
			ДатаОтчета = ТекущаяДатаСеанса();
			ПараметрДатаОтчета.Значение = ДатаОтчета;
		КонецЕсли;	
	КонецЕсли;
	
	ПараметрПериодРегистрации = НастройкиОтчета.ПараметрыДанных.НайтиЗначениеПараметра(Новый ПараметрКомпоновкиДанных("ПериодРегистрации"));
	
	Если ПараметрПериодРегистрации <> Неопределено Тогда
		ПараметрПериодРегистрации.Использование = Истина;
		ПараметрПериодРегистрации.Значение = НачалоМесяца(ДатаОтчета);
	КонецЕсли;
		
	ПараметрВыводитьПодразделения = НастройкиОтчета.ПараметрыДанных.НайтиЗначениеПараметра(Новый ПараметрКомпоновкиДанных("ВыводитьВРазрезеПодразделений"));
		
	Если ПараметрВыводитьПодразделения <> Неопределено Тогда
		ПараметрВыводитьПодразделения.Использование = Истина;
	Иначе		
		ПараметрВыводитьПодразделения = НастройкиОтчета.ПараметрыДанных.Элементы.Добавить();
		ПараметрВыводитьПодразделения.Значение = Ложь;
	КонецЕсли;
	
	ОтборыПоПодразделению = ОбщегоНазначенияКлиентСервер.НайтиЭлементыИГруппыОтбора(НастройкиОтчета.Отбор, "Подразделение");
	
	Для Каждого ЭлементОтбора Из ОтборыПоПодразделению Цикл
		Если ЭлементОтбора.Использование Тогда
			ПараметрВыводитьПодразделения.Значение = Истина;
			Прервать;
		КонецЕсли;	
	КонецЦикла;	
	
	ЗаполнитьСписокВидовВремени(НастройкиОтчета);
	
	Если КлючВарианта = "УнифицированнаяФормаТ13" Тогда
		
		СтандартнаяОбработка = Ложь;
		
		// Параметры документа
		ДокументРезультат.ТолькоПросмотр = Истина;
		ДокументРезультат.ИмяПараметровПечати = "ПАРАМЕТРЫ_ПЕЧАТИ_УнифицированнаяФормаТ13";
		ДокументРезультат.ОриентацияСтраницы = ОриентацияСтраницы.Ландшафт;
		
		КомпоновщикМакета = Новый КомпоновщикМакетаКомпоновкиДанных;
		МакетКомпоновки = КомпоновщикМакета.Выполнить(ЭтотОбъект.СхемаКомпоновкиДанных, НастройкиОтчета,,, Тип("ГенераторМакетаКомпоновкиДанныхДляКоллекцииЗначений"));
		
		// Создадим и инициализируем процессор компоновки.
		ПроцессорКомпоновки = Новый ПроцессорКомпоновкиДанных;
		ПроцессорКомпоновки.Инициализировать(МакетКомпоновки, , , Истина);
		
		ДанныеОВремени = Новый ДеревоЗначений;
		
		ПроцессорВывода = Новый ПроцессорВыводаРезультатаКомпоновкиДанныхВКоллекциюЗначений;
		ПроцессорВывода.УстановитьОбъект(ДанныеОВремени);
		
		// Обозначим начало вывода
		ПроцессорВывода.Вывести(ПроцессорКомпоновки, Истина);
			
		ВывестиМакет(ДокументРезультат, ДанныеОВремени, НачалоПериода, КонецПериода, ДатаОтчета);
		
		
	Иначе
		
		УчетНачисленнойЗарплатыРасширенный.ПриКомпоновкеОтчетаУнифицированнаяФормаТ13(
			ЭтотОбъект, ДокументРезультат, ДанныеРасшифровки, СтандартнаяОбработка, КлючВарианта, НастройкиОтчета);
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти


#Область СлужебныеПроцедурыИФункции


Процедура ИнициализироватьОтчет() Экспорт
	
	ЗарплатаКадрыОбщиеНаборыДанных.ЗаполнитьОбщиеИсточникиДанныхОтчета(ЭтотОбъект);
	
КонецПроцедуры

// Для общей формы "Форма отчета" подсистемы "Варианты отчетов".
Процедура ОпределитьНастройкиФормы(Форма, КлючВарианта, Настройки) Экспорт
	
	Настройки.Печать.ОриентацияСтраницы = ОриентацияСтраницы.Ландшафт;
	Настройки.События.ПриСозданииНаСервере = Истина;
	
КонецПроцедуры

// Вызывается в обработчике одноименного события формы отчета после выполнения кода формы.
//
// Параметры:
//   Форма - УправляемаяФорма - Форма отчета.
//   Отказ - Передается из параметров обработчика "как есть".
//   СтандартнаяОбработка - Передается из параметров обработчика "как есть".
//
// См. также:
//   "УправляемаяФорма.ПриСозданииНаСервере" в синтакс-помощнике.
//
Процедура ПриСозданииНаСервере(Форма, Отказ, СтандартнаяОбработка) Экспорт
	
	ИнициализироватьОтчет();
	ЗначениеВДанныеФормы(ЭтотОбъект, Форма.Отчет);
	
КонецПроцедуры


Процедура ЗаполнитьСписокВидовВремени(НастройкиОтчета)
	ПараметрВидВремени = НастройкиОтчета.ПараметрыДанных.НайтиЗначениеПараметра(Новый ПараметрКомпоновкиДанных("Выходной"));	
	Если ПараметрВидВремени <> Неопределено Тогда
		ПараметрВидВремени.Значение = ОбщегоНазначенияКлиентСервер.ПредопределенныйЭлемент("Справочник.ВидыИспользованияРабочегоВремени.ВыходныеДни");
		ПараметрВидВремени.Использование = Истина;
	КонецЕсли;	
	
	ПараметрВидВремени = НастройкиОтчета.ПараметрыДанных.НайтиЗначениеПараметра(Новый ПараметрКомпоновкиДанных("ВечерниеЧасы"));	
	Если ПараметрВидВремени <> Неопределено Тогда
		ПараметрВидВремени.Значение = ОбщегоНазначенияКлиентСервер.ПредопределенныйЭлемент("Справочник.ВидыИспользованияРабочегоВремени.РаботаВечерниеЧасы");
		ПараметрВидВремени.Использование = Истина;
	КонецЕсли;	
	
	ПараметрВидВремени = НастройкиОтчета.ПараметрыДанных.НайтиЗначениеПараметра(Новый ПараметрКомпоновкиДанных("НочныеЧасы"));	
	Если ПараметрВидВремени <> Неопределено Тогда
		ПараметрВидВремени.Значение = ОбщегоНазначенияКлиентСервер.ПредопределенныйЭлемент("Справочник.ВидыИспользованияРабочегоВремени.РаботаНочныеЧасы");
		ПараметрВидВремени.Использование = Истина;
	КонецЕсли;
	
	ПараметрВидВремени = НастройкиОтчета.ПараметрыДанных.НайтиЗначениеПараметра(Новый ПараметрКомпоновкиДанных("НочныеЧасы"));	
	Если ПараметрВидВремени <> Неопределено Тогда
		ПараметрВидВремени.Значение = ОбщегоНазначенияКлиентСервер.ПредопределенныйЭлемент("Справочник.ВидыИспользованияРабочегоВремени.РаботаНочныеЧасы");
		ПараметрВидВремени.Использование = Истина;
	КонецЕсли;
	
	ПараметрВидВремени = НастройкиОтчета.ПараметрыДанных.НайтиЗначениеПараметра(Новый ПараметрКомпоновкиДанных("Сверхурочно"));	
	Если ПараметрВидВремени <> Неопределено Тогда
		ПараметрВидВремени.Значение = ОбщегоНазначенияКлиентСервер.ПредопределенныйЭлемент("Справочник.ВидыИспользованияРабочегоВремени.Сверхурочные");
		ПараметрВидВремени.Использование = Истина;
	КонецЕсли;
	
	ПараметрВидВремени = НастройкиОтчета.ПараметрыДанных.НайтиЗначениеПараметра(Новый ПараметрКомпоновкиДанных("Явка"));	
	Если ПараметрВидВремени <> Неопределено Тогда
		ПараметрВидВремени.Значение = ОбщегоНазначенияКлиентСервер.ПредопределенныйЭлемент("Справочник.ВидыИспользованияРабочегоВремени.Явка");
		ПараметрВидВремени.Использование = Истина;
	КонецЕсли;
	
	ПараметрВидВремени = НастройкиОтчета.ПараметрыДанных.НайтиЗначениеПараметра(Новый ПараметрКомпоновкиДанных("РабочееВремя"));	
	Если ПараметрВидВремени <> Неопределено Тогда
		ПараметрВидВремени.Значение = ОбщегоНазначенияКлиентСервер.ПредопределенныйЭлемент("Справочник.ВидыИспользованияРабочегоВремени.РабочееВремя");
		ПараметрВидВремени.Использование = Истина;
	КонецЕсли;
	
КонецПроцедуры	

Процедура ВывестиМакет(ДокументРезультат, ДанныеОВремени, ДатаНачала, ДатаОкончания, ДатаОтчета)
	ДокументРезультат.ОриентацияСтраницы = ОриентацияСтраницы.Ландшафт;
	ДокументРезультат.АвтоМасштаб = Истина;
	
	ТекущийЛист = Новый ТабличныйДокумент;
	
	//Макет = УправлениеПечатью.МакетПечатнойФормы("ОбщийМакет.ПФ_MXL_УнифицированнаяФормаТ13");
	Макет = УправлениеПечатью.МакетПечатнойФормы("ОбщийМакет.ПФ_MXL_УнифицированнаяФормаТ13_Локализация"); //1С-Минск
	
	ОбластьШапка = Макет.ПолучитьОбласть("Шапка");
	ОбластьШапкаТаблицы = Макет.ПолучитьОбласть("ШапкаТаблицы");
	ОбластьДанныеОВремени = Макет.ПолучитьОбласть("Строка");
	ОбластьПодвал = Макет.ПолучитьОбласть("Подвал");
	
	Для Каждого ДанныеПоОрганизации Из ДанныеОВремени.Строки Цикл
		ВывестиШапку(ДокументРезультат, ОбластьШапка, ДанныеПоОрганизации, ДатаНачала, ДатаОкончания, ДатаОтчета, ТекущийЛист);
		
		ПроверяемыеОбласти = Новый Массив;
		ПроверяемыеОбласти.Добавить(ОбластьШапкаТаблицы);
		ПроверяемыеОбласти.Добавить(ОбластьДанныеОВремени);
		
		ВывестиОбласть(ДокументРезультат, ТекущийЛист, ОбластьШапкаТаблицы, ПроверяемыеОбласти);		
		
		Для Каждого ДанныеПоСотруднику Из ДанныеПоОрганизации.Строки Цикл
			 ВывестиДанныеПоСотруднику(ДокументРезультат, ОбластьДанныеОВремени, ОбластьШапкаТаблицы, ДанныеПоСотруднику, ТекущийЛист);		
		КонецЦикла;	
		 
		ВывестиПодвал(ДокументРезультат, ОбластьПодвал, ДанныеПоОрганизации.Организация, ДатаОтчета, ДанныеПоОрганизации.ПараметрыДанныхОтветственный, ТекущийЛист) 
	КонецЦикла;
КонецПроцедуры	

Процедура ВывестиШапку(ДокументРезультат, ОбластьШапка, ДанныеШапки, ДатаНачала, ДатаОкончания, ДатаОтчета, ТекущийЛист)	
	
	НастройкиПечатныхФорм = ЗарплатаКадрыПовтИсп.НастройкиПечатныхФорм();
	
	ОбластьШапка.Параметры.Заполнить(ДанныеШапки);
	
	Если НастройкиПечатныхФорм.ВыводитьПолнуюИерархиюПодразделений И ЗначениеЗаполнено(ДанныеШапки.Подразделение) Тогда
		ОбластьШапка.Параметры.ПодразделениеНаименование = ДанныеШапки.Подразделение.ПолноеНаименование();
	КонецЕсли; 
	
	ОбластьШапка.Параметры.ОрганизацияНаименование = ДанныеШапки.ОрганизацияНаименованиеПолное;
	ОбластьШапка.Параметры.ДатаЗаполнения = ДатаОтчета;
	ОбластьШапка.Параметры.ДатаНачала = Макс(НачалоМесяца(ДанныеШапки.Месяц), ДатаНачала);
	ОбластьШапка.Параметры.ДатаОкончания = Мин(КонецМесяца(ДанныеШапки.Месяц), ДатаОкончания);
	
	ПроверяемыеОбласти = Новый Массив;
	ПроверяемыеОбласти.Добавить(ОбластьШапка);
	
	ВывестиОбласть(ДокументРезультат, ТекущийЛист, ОбластьШапка, ПроверяемыеОбласти);	
КонецПроцедуры	

Процедура ВывестиПодвал(ДокументРезультат, ОбластьПодвал, Организация, ДатаОтчета, Ответственный, ТекущийЛист)
	
	ОтветственныеЛица = Новый Структура("Организация,Руководитель,ДолжностьРуководителя,РуководительКадровойСлужбы,ДолжностьРуководителяКадровойСлужбы", Организация);
	ЗарплатаКадры.ПолучитьЗначенияПоУмолчанию(ОтветственныеЛица, ДатаОтчета);
	
	МассивФизЛиц = Новый Массив;
	МассивФизЛиц.Добавить(ОтветственныеЛица.Руководитель);
	МассивФизЛиц.Добавить(ОтветственныеЛица.РуководительКадровойСлужбы);
	Если ЗначениеЗаполнено(Ответственный) Тогда
		МассивФизЛиц.Добавить(Ответственный);
	КонецЕсли;
	
	ФИОФизЛиц = ЗарплатаКадры.СоответствиеФИОФизЛицСсылкам(ДатаОтчета, МассивФизЛиц);
	ФИОРуководителя = ФИОФизЛиц[ОтветственныеЛица.Руководитель];
	ФИОРуководителяКадровойСлужбы = ФИОФизЛиц[ОтветственныеЛица.РуководительКадровойСлужбы];
	ДолжностьРуководителя = ?(ОтветственныеЛица.ДолжностьРуководителя = Неопределено, "", ОбщегоНазначения.ЗначениеРеквизитаОбъекта(ОтветственныеЛица.ДолжностьРуководителя, "Наименование"));
	ДолжностьРуководителяКадровойСлужбы = ?(ОтветственныеЛица.ДолжностьРуководителяКадровойСлужбы = Неопределено, "", ОбщегоНазначения.ЗначениеРеквизитаОбъекта(ОтветственныеЛица.ДолжностьРуководителяКадровойСлужбы, "Наименование"));
	
	Если ФИОРуководителя <> Неопределено Тогда
		ОбластьПодвал.Параметры.ФИОРуководителя	= ФизическиеЛицаЗарплатаКадры.РасшифровкаПодписи(ФИОРуководителя.Фамилия + " " + ФИОРуководителя.Имя + " " + ФИОРуководителя.Отчество);
	КонецЕсли;	
	ОбластьПодвал.Параметры.ДолжностьРуководителя = ДолжностьРуководителя;
	Если ФИОРуководителяКадровойСлужбы <> Неопределено Тогда 
		ОбластьПодвал.Параметры.ФИОКадровика = ФизическиеЛицаЗарплатаКадры.РасшифровкаПодписи(ФИОРуководителяКадровойСлужбы.Фамилия + " " + ФИОРуководителяКадровойСлужбы.Имя + " " + ФИОРуководителяКадровойСлужбы.Отчество);
	КонецЕсли;	
	ОбластьПодвал.Параметры.ДолжностьКадровика    = ДолжностьРуководителяКадровойСлужбы;
	
	ДолжностьОтветственного	= "";
	ФИООтветственногоСтрокой = "";
	Если ЗначениеЗаполнено(Ответственный) Тогда
		СписокФизЛиц = Новый Массив;
		СписокФизЛиц.Добавить(Ответственный);
		ОсновныеСотрудникиФизическихЛиц = КадровыйУчет.ОсновныеСотрудникиФизическихЛиц(СписокФизЛиц, Истина, Организация, ДатаОтчета);
		
		ФИООтветственного = ФИОФизЛиц[Ответственный];
		ФИООтветственногоСтрокой = ФизическиеЛицаЗарплатаКадры.РасшифровкаПодписи(ФИООтветственного.Фамилия + " " + ФИООтветственного.Имя + " " + ФИООтветственного.Отчество); 
		Если ОсновныеСотрудникиФизическихЛиц.Количество() > 0  Тогда
			СписокСотрудников = Новый Массив;
			СписокСотрудников.Добавить(ОсновныеСотрудникиФизическихЛиц[0].Сотрудник);	
			
			КадровыеДанные = КадровыйУчет.КадровыеДанныеСотрудников(Истина, СписокСотрудников, "Должность", ДатаОтчета);
			
			Если КадровыеДанные.Количество() > 0 Тогда
				ДолжностьОтветственного = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(КадровыеДанные[0].Должность, "Наименование");	
			КонецЕсли;	
		КонецЕсли;	
	КонецЕсли;		
	
	ОбластьПодвал.Параметры.ФИООтветственного = ФИООтветственногоСтрокой;
	ОбластьПодвал.Параметры.ДолжностьОтветственного = ДолжностьОтветственного;
	
	ПроверяемыеОбласти = Новый Массив;
	ПроверяемыеОбласти.Добавить(ОбластьПодвал);
	
	ВывестиОбласть(ДокументРезультат, ТекущийЛист, ОбластьПодвал, ПроверяемыеОбласти);	

КонецПроцедуры	

Процедура ВывестиДанныеПоСотруднику(ДокументРезультат, ОбластьДанныеОВремени, ОбластьШапкаТаблицы, ДанныеПоСотруднику, ТекущийЛист)
	ИмяПоляБуквенныйКод = ИмяПоляБуквенныйКод();
	
	
	НастройкиПечатныхФорм = ЗарплатаКадрыПовтИсп.НастройкиПечатныхФорм();
	КоличествоПараметров = ОбластьДанныеОВремени.Параметры.Количество();
	
	Для ИндексПараметра = 0 По КоличествоПараметров - 1 Цикл  
		ОбластьДанныеОВремени.Параметры.Установить(ИндексПараметра, Неопределено);
	КонецЦикла;	
	
	ВидВремениВыходной = ОбщегоНазначенияКлиентСервер.ПредопределенныйЭлемент("Справочник.ВидыИспользованияРабочегоВремени.ВыходныеДни");
	
	ОтработаноДнейЗаПервуюПоловинуМесяца = 0;
	ОтработаноЧасовЗаВторуюПоловинуМесяца = 0;
	ОтработаноДнейЗаВторуюПоловинуМесяца = 0;
	ОтработаноЧасовЗаПервуюПоловинуМесяца = 0;
	ОтработаноДнейЗаМесяц = 0;
	ОтработаноЧасовЗаМесяц = 0;
			
	Фамилия = ДанныеПоСотруднику.Фамилия;
	Имя = ДанныеПоСотруднику.Имя;
	Отчество = ДанныеПоСотруднику.Отчество;
	
	Если НастройкиПечатныхФорм.ВыводитьПолныеФИОВСписочныхПечатныхФормах Тогда
		
		ФИО = Фамилия + ?(ПустаяСтрока(Имя), "", " " + Имя + ?(ПустаяСтрока(Отчество), "", " " + Отчество))
		
	Иначе
		
		ФИО = ФизическиеЛицаКлиентСервер.ФамилияИнициалыФизЛица(
			ДанныеПоСотруднику.СотрудникНаименование,
			Фамилия,
			Имя,
			Отчество);
			
	КонецЕсли;
	
	ОбластьДанныеОВремени.Параметры.Сотрудник = ФИО + "
											|(" + ДанныеПоСотруднику.ДолжностьНаименование + ")";	
											
	ОбластьДанныеОВремени.Параметры.ТабельныйНомер = ДанныеПоСотруднику.СотрудникКод;	
	
	ОбластьДанныеОВремени.Параметры.НомерПП = ДанныеПоСотруднику.СистемныеПоляНомерПоПорядкуВГруппировке;	
	
	ОтклоненияПоСотруднику = Новый ТаблицаЗначений;
	ОтклоненияПоСотруднику.Колонки.Добавить("ВидВремени");
	ОтклоненияПоСотруднику.Колонки.Добавить("БуквенныйКод");
	ОтклоненияПоСотруднику.Колонки.Добавить("Часов");
	ОтклоненияПоСотруднику.Колонки.Добавить("Дней");

	Для Каждого ДанныеОВремениНаДату Из ДанныеПоСотруднику.Строки Цикл 
		
		Если День(ДанныеОВремениНаДату.Дата) <= 15 Тогда
			ОтработаноДнейЗаПервуюПоловинуМесяца = ОтработаноДнейЗаПервуюПоловинуМесяца + ДанныеОВремениНаДату.РабочиеДни;
			ОтработаноЧасовЗаПервуюПоловинуМесяца = ОтработаноЧасовЗаПервуюПоловинуМесяца + ДанныеОВремениНаДату.РабочиеЧасы;
		Иначе
			ОтработаноЧасовЗаВторуюПоловинуМесяца = ОтработаноЧасовЗаВторуюПоловинуМесяца + ДанныеОВремениНаДату.РабочиеЧасы;
			ОтработаноДнейЗаВторуюПоловинуМесяца = ОтработаноДнейЗаВторуюПоловинуМесяца + ДанныеОВремениНаДату.РабочиеДни;
		КонецЕсли;
		
		ОтработаноДнейЗаМесяц = ОтработаноДнейЗаМесяц + ДанныеОВремениНаДату.РабочиеДни;
		ОтработаноЧасовЗаМесяц = ОтработаноЧасовЗаМесяц + ДанныеОВремениНаДату.РабочиеЧасы;

		ПредставлениеВидовВремени = "";
		ЧасыПоВидамВремениСтрока = "";
		
		Для Каждого ДетальныеЗаписиЗаДень Из ДанныеОВремениНаДату.Строки Цикл
			Если Не ДетальныеЗаписиЗаДень.ВидУчетаВремениРабочееВремя 
				И ДетальныеЗаписиЗаДень.ВидУчетаВремени <> ВидВремениВыходной
				И ДетальныеЗаписиЗаДень.ВидУчетаВремениОсновноеВремя <> Справочники.ВидыИспользованияРабочегоВремени.ПустаяСсылка() Тогда
				
				ОтклоненияПоВидуВремени = ОтклоненияПоСотруднику.Добавить();
				
				ОтклоненияПоВидуВремени.ВидВремени = ДетальныеЗаписиЗаДень.ВидУчетаВремени;
				ОтклоненияПоВидуВремени.БуквенныйКод = ДетальныеЗаписиЗаДень[ИмяПоляБуквенныйКод];
				ОтклоненияПоВидуВремени.Дней = 1;
				Если ДанныеОВремениНаДату.Строки.Количество() > 1 Тогда
					ОтклоненияПоВидуВремени.Часов = ДетальныеЗаписиЗаДень.Часы;		
				Иначе 	
					ОтклоненияПоВидуВремени.Часов = ДанныеОВремениНаДату.Часы;
				КонецЕсли;	
					
			КонецЕсли;
			
			ПредставлениеВидовВремени = ПредставлениеВидовВремени + "/"+  ДетальныеЗаписиЗаДень[ИмяПоляБуквенныйКод];
			
			Если Не ДетальныеЗаписиЗаДень.ВидУчетаВремениЦелосменное Тогда 
				ЧасыПоВидамВремениСтрока = ЧасыПоВидамВремениСтрока +  "/" + Формат(ДетальныеЗаписиЗаДень.Часы, "ЧГ=");
			КонецЕсли;			
			
		КонецЦикла;	
						
		НомерДня = День(ДетальныеЗаписиЗаДень.Дата);
		
		ОбластьДанныеОВремени.Параметры["Символ" + НомерДня] = Сред(ПредставлениеВидовВремени, 2);
		ОбластьДанныеОВремени.Параметры["ДополнительноеЗначение" + НомерДня] = Сред(ЧасыПоВидамВремениСтрока, 2);
		
		ОбластьДанныеОВремени.Параметры.ДниПерваяПоловина = ОтработаноДнейЗаПервуюПоловинуМесяца;
		ОбластьДанныеОВремени.Параметры.ЧасыПерваяПоловина = ОтработаноЧасовЗаПервуюПоловинуМесяца;
		ОбластьДанныеОВремени.Параметры.ДниВтораяПоловина = ОтработаноДнейЗаВторуюПоловинуМесяца;
		ОбластьДанныеОВремени.Параметры.ЧасыВтораяПоловина = ОтработаноЧасовЗаВторуюПоловинуМесяца;
		ОбластьДанныеОВремени.Параметры.ДниЗаМесяц = ОтработаноДнейЗаМесяц;
		ОбластьДанныеОВремени.Параметры.ЧасыЗаМесяц = ОтработаноЧасовЗаМесяц;
		
		
		ОтклоненияПоСотруднику.Свернуть("ВидВремени, БуквенныйКод", "Дней, Часов");
		
		СчетчикОтклонений = 1;
		Для Каждого ОтклонениеПоВидуВремени Из ОтклоненияПоСотруднику Цикл
			Если СчетчикОтклонений > 8 Тогда
				Прервать;
			КонецЕсли;
			
			ОбластьДанныеОВремени.Параметры["НеявкаКод" + СчетчикОтклонений] = ОтклонениеПоВидуВремени.БуквенныйКод;
			ОбластьДанныеОВремени.Параметры["НеявкаДниЧасы" + СчетчикОтклонений] = Формат(ОтклонениеПоВидуВремени.Дней, "ЧГ=") + 
				?(ОтклонениеПоВидуВремени.Часов > 0, "(" + Формат(ОтклонениеПоВидуВремени.Часов, "ЧГ=") + ")", "");
																	
			СчетчикОтклонений = СчетчикОтклонений + 1;															
		КонецЦикла;	

	КонецЦикла;
	
	ВывестиОбласть(ДокументРезультат, ТекущийЛист, ОбластьДанныеОВремени, ОбластьДанныеОВремени);	
КонецПроцедуры	

Процедура ВывестиОбласть(ДокументРезультат, ТекущийЛист, ВыводимаяОбласть, ПроверяемыеОбласти)
    ТекущийЛист.ОриентацияСтраницы = ДокументРезультат.ОриентацияСтраницы;
    ТекущийЛист.АвтоМасштаб = ДокументРезультат.АвтоМасштаб;
    
    Если Не ОбщегоНазначения.ПроверитьВыводТабличногоДокумента(ТекущийЛист, ПроверяемыеОбласти) Тогда
        ТекущийЛист.ВывестиГоризонтальныйРазделительСтраниц();
        
        ДокументРезультат.Вывести(ТекущийЛист);
        
        ТекущийЛист = Новый ТабличныйДокумент;
    КонецЕсли;
    
    ДокументРезультат.Вывести(ВыводимаяОбласть);    
КонецПроцедуры 

Функция ИмяПоляБуквенныйКод()
	Если ПолучитьФункциональнуюОпцию("РаботаВБюджетномУчреждении") Тогда
		Возврат "ВидУчетаВремениБуквенныйКодБюджетный";
	Иначе
		Возврат "ВидУчетаВремениБуквенныйКод";
	КонецЕсли;	
КонецФункции

#КонецОбласти

#КонецЕсли