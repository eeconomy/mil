﻿#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	// Пропускаем инициализацию, чтобы гарантировать получение формы при передаче параметра "АвтоТест".
	Если Параметры.Свойство("АвтоТест") Тогда
		Возврат;
	КонецЕсли;
	
	ВызватьИсключение НСтр("ru = 'Обработка не предназначена для интерактивного использования!';
							|en = 'Data processor is not for interactive use.'");
	
КонецПроцедуры

#КонецОбласти