﻿
#Область ОбработчикиСобытий

&НаКлиенте
Процедура ОбработкаКоманды(ПараметрКоманды, ПараметрыВыполненияКоманды)
	
	// &ЗамерПроизводительности
	ОценкаПроизводительностиКлиент.НачатьЗамерВремени(Истина,
		"Обработка.Чд_АРМКонтролер.Команда.ПервичныеПромеры");
	
	ОткрытьФорму("Обработка.Чд_АРМКонтролер.Форма.Форма",,,,ПараметрыВыполненияКоманды.Окно);
	
КонецПроцедуры

#КонецОбласти
