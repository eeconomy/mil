﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ПрограммныйИнтерфейс

// Определяет список команд отчетов.
//
// Параметры:
//   КомандыОтчетов - ТаблицаЗначений - Таблица с командами отчетов. Для изменения.
//       См. описание 1 параметра процедуры ВариантыОтчетовПереопределяемый.ПередДобавлениемКомандОтчетов().
//   Параметры - Структура - Вспомогательные параметры. Для чтения.
//       См. описание 2 параметра процедуры ВариантыОтчетовПереопределяемый.ПередДобавлениемКомандОтчетов().
//
Процедура ДобавитьКомандыОтчетов(КомандыОтчетов, Параметры) Экспорт
	
	// Список "Документы"
	КомандаОтчет = ВариантыОтчетовУТПереопределяемый.ДобавитьКомандуСтруктураПодчиненности(КомандыОтчетов);
	Если КомандаОтчет <> Неопределено Тогда
		КомандаОтчет.МестоРазмещения = "СписокПодменюОтчеты";
		КомандаОтчет.Важность = "СмТакже";
		КомандаОтчет.ИмяСписка = "Список";
		КомандаОтчет.ВидимостьВФормах = "ДокументыПоНМА";
	КонецЕсли;
	
	// Список "Документы с детализацией по НМА"
	КомандаОтчет = ВариантыОтчетовУТПереопределяемый.ДобавитьКомандуСтруктураПодчиненности(КомандыОтчетов);
	Если КомандаОтчет <> Неопределено Тогда
		КомандаОтчет.МестоРазмещения = "СписокДетальноПодменюОтчеты";
		КомандаОтчет.Важность = "ПодменюОтчетыСмТакжеДетально";
		КомандаОтчет.ИмяСписка = "СписокДетально";
		КомандаОтчет.ВидимостьВФормах = "ДокументыПоНМА";
	КонецЕсли;
	
	// Версия 2.2
	КомандаОтчет = ВариантыОтчетовУТПереопределяемый.ДобавитьКомандуСтруктураПодчиненности(КомандыОтчетов);
	Если КомандаОтчет <> Неопределено Тогда
		КомандаОтчет.ВидимостьВФормах = "ДокументыПоНМА2_2";
	КонецЕсли; 
	
КонецПроцедуры

#КонецОбласти

#КонецЕсли