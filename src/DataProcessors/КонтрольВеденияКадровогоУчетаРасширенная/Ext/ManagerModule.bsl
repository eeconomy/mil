﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область СлужебныйПрограммныйИнтерфейс

Процедура ПроверкаЗадвоенностиФизическихЛиц(Проверка, ПараметрыПроверки) Экспорт
	
	Если Не ОбщегоНазначения.ПодсистемаСуществует("СтандартныеПодсистемы.КонтрольВеденияУчета") Тогда
		Возврат;
	КонецЕсли;
	
	УстановитьПривилегированныйРежим(Истина);
	
	Результат = РезультатПроверкиЗадвоенностиФизическихЛиц();
	Если Не Результат.Пустой() Тогда
		
		МодульКонтрольВеденияУчета = ОбщегоНазначения.ОбщийМодуль("КонтрольВеденияУчета");
		
		Выборка = Результат.Выбрать();
		Пока Выборка.СледующийПоЗначениюПоля("ОбластьПоиска") Цикл
			
			Пока Выборка.СледующийПоЗначениюПоля("Значение") Цикл
				
				ДополнительнаяИнформация = СтрШаблон("%1: %2", Выборка.ОбластьПоиска, Выборка.Значение);
				Пока Выборка.СледующийПоЗначениюПоля("ФизическоеЛицо") Цикл
					
					Проблема = МодульКонтрольВеденияУчета.ОписаниеПроблемы(Выборка.Сотрудник, ПараметрыПроверки);
					Проблема.УточнениеПроблемы = НСтр("ru='Найдены люди с такими же данными'") + " (" + ДополнительнаяИнформация + ")";
					КонтрольВеденияУчетаБЗК.ЗаписатьПроблему(Проблема, ПараметрыПроверки);
					
				КонецЦикла;
				
			КонецЦикла;
			
		КонецЦикла;
		
	КонецЕсли;
	
	УстановитьПривилегированныйРежим(Ложь);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

Функция РезультатПроверкиЗадвоенностиФизическихЛиц()
	
	Запрос = Новый Запрос;
	Запрос.МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
	
	КадровыйУчетРасширенный.СоздатьВТЗадублированныеФизическиеЛица(Запрос.МенеджерВременныхТаблиц);
	
	Запрос.Текст =
		"ВЫБРАТЬ
		|	ЗадублированныеФизическиеЛица.ОбластьПоиска КАК ОбластьПоиска,
		|	ЗадублированныеФизическиеЛица.Значение КАК Значение,
		|	ЗадублированныеФизическиеЛица.ФизическоеЛицо КАК ФизическоеЛицо,
		|	Сотрудники.Ссылка КАК Сотрудник
		|ИЗ
		|	ВТЗадублированныеФизическиеЛица КАК ЗадублированныеФизическиеЛица
		|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Справочник.Сотрудники КАК Сотрудники
		|		ПО ЗадублированныеФизическиеЛица.ФизическоеЛицо = Сотрудники.ФизическоеЛицо
		|
		|УПОРЯДОЧИТЬ ПО
		|	ОбластьПоиска,
		|	Значение,
		|	ФизическоеЛицо,
		|	Сотрудник";
	
	РезультатЗапроса = Запрос.Выполнить();
	
	Возврат РезультатЗапроса;
	
КонецФункции

#КонецОбласти

#КонецЕсли