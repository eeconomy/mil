﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Свойство("АвтоТест") Тогда // Возврат при получении формы для анализа.
		Возврат;
	КонецЕсли;
	
	ДатаЗагрузки = ТекущаяДатаСеанса();
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ПутьКФайлуНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	//
	Диалог = Новый ДиалогВыбораФайла(РежимДиалогаВыбораФайла.Открытие);
	
	Диалог.Заголовок = "Выберите файл загрузки:";
	Диалог.ПредварительныйПросмотр = Ложь;
	Диалог.Фильтр = "Excel (*.xlsx)|*.xlsx|";
	
	Если Диалог.Выбрать() Тогда
		Каталог = СокрЛП(Диалог.Каталог);
		ПолноеИмяФайла = СокрЛП(Диалог.ПолноеИмяФайла);
	Иначе
		Возврат;
	КонецЕсли;	
	ПутьКФайлу = ПолноеИмяФайла;
	
КонецПроцедуры

&НаКлиенте
Процедура Загрузить(Команда)
	                             		
	ОчиститьСообщения();
	Если ЗначениеЗаполнено(ПутьКФайлу) Тогда
		ДанныеФайла = Новый ДвоичныеДанные(ПутьКФайлу);
		АдресФайла = "";
		АдресФайла = ПоместитьВоВременноеХранилище(ДанныеФайла, ЭтаФорма.УникальныйИдентификатор);
		ИмпортироватьНаСервере(АдресФайла);
	КонецЕсли;	
	
КонецПроцедуры

&НаСервере
Процедура ИмпортироватьНаСервере(АдресХранилищаФайла)
	
	ТаблицаЗначенийМоделей = ЗагрузитьЧерезТабДок(АдресХранилищаФайла);

	Если ТаблицаЗначенийМоделей.Количество() > 0  Тогда
		КоличествоЗагруженныхДокументов = 0;
		ЗагрузитьДанныеВДокПолуфабрикаты(ТаблицаЗначенийМоделей);
		Если КоличествоЗагруженныхДокументов > 0 Тогда
			ОбщегоНазначения.СообщитьПользователю("Создано " + КоличествоЗагруженныхДокументов + " документов.");
		КонецЕсли;
	КонецЕсли;  
	
	ТаблицаОшибок = Новый ТаблицаЗначений;
	ТаблицаОшибок.Колонки.Добавить("НаименованиеВладельца", Новый ОписаниеТипов("Строка"));
	ТаблицаОшибок.Колонки.Добавить("Ошибка", Новый ОписаниеТипов("Строка"));
	
	Для каждого СтрокаТаб Из ТаблицаОшибочныхСтрок Цикл
		НоваяСтрокаТаблицаОшибок = ТаблицаОшибок.Добавить();
		НоваяСтрокаТаблицаОшибок.НаименованиеВладельца = СтрокаТаб.НаименованиеВладельца;
		НоваяСтрокаТаблицаОшибок.Ошибка = СтрокаТаб.Ошибка;
	КонецЦикла;
	
	ТаблицаОшибок.Свернуть("НаименованиеВладельца, Ошибка"); 
	
	ТаблицаОшибочныхСтрок.Загрузить(ТаблицаОшибок);
КонецПроцедуры

&НаСервере
Функция КодГербераПоНаименованию(ЗначениеПоля)
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	ЗначенияСвойствОбъектов.Ссылка КАК Ссылка
	|ИЗ
	|	Справочник.ЗначенияСвойствОбъектов КАК ЗначенияСвойствОбъектов
	|ГДЕ
	|	НЕ ЗначенияСвойствОбъектов.ЭтоГруппа
	|	И НЕ ЗначенияСвойствОбъектов.ПометкаУдаления
	|	И ЗначенияСвойствОбъектов.Владелец = &Владелец
	|	И ЗначенияСвойствОбъектов.Наименование = &Наименование";
	
	Запрос.УстановитьПараметр("Владелец", ПланыВидовХарактеристик.ДополнительныеРеквизитыИСведения.НайтиПоРеквизиту("Имя", "Чд_КодГербера"));
	Запрос.УстановитьПараметр("Наименование", ЗначениеПоля);
	
	ВыборкаКодаГербера = Запрос.Выполнить().Выбрать();
	
	Пока ВыборкаКодаГербера.Следующий() Цикл
		Возврат ВыборкаКодаГербера.Ссылка;	
	КонецЦикла;
	
	Возврат Справочники.ЗначенияСвойствОбъектов.ПустаяСсылка();    	
	
КонецФункции

&НаСервере
Процедура ЗагрузитьДанныеВДокПолуфабрикаты(ТаблицаЗначенийМоделей)
	
	ТекстОшибкиЗаписи = НСтр("ru='%1 невозможно записать документ по причине:|%2'");
	
	ТекстОшибкиЗаполнения = НСтр("ru='%1 невозможно провести по причине некорректного заполнения.'");
	
	ТаблицаМоделей = Новый ТаблицаЗначений;
	
	ТаблицаМоделей.Колонки.Добавить("Модель", Новый ОписаниеТипов("СправочникСсылка.Чд_МоделиНоменклатуры"));   
	
	Для каждого СсылкаМодель Из ТаблицаЗначенийМоделей.ВыгрузитьКолонку("Модель") Цикл
		НоваяСтрокаТаблицаМоделей = ТаблицаМоделей.Добавить();
		НоваяСтрокаТаблицаМоделей.Модель = СсылкаМодель;
	КонецЦикла;	
	
	ТаблицаМоделей.Свернуть("Модель");
	
	ТаблицаПФ = Новый ТаблицаЗначений;
	ТаблицаПФ.Колонки.Добавить("КодГербера", Новый ОписаниеТипов("СправочникСсылка.ЗначенияСвойствОбъектов"));
	ТаблицаПФ.Колонки.Добавить("Модель", Новый ОписаниеТипов("СправочникСсылка.Чд_МоделиНоменклатуры"));
	
	Для каждого СтрокаТаб Из ТаблицаЗначенийМоделей Цикл
		НоваяСтрокаТаблицаПФ = ТаблицаПФ.Добавить();
		НоваяСтрокаТаблицаПФ.Модель = СтрокаТаб.Модель;
		НоваяСтрокаТаблицаПФ.КодГербера = СтрокаТаб.КодГербера;
	КонецЦикла;   	
	
	ТаблицаПФ.Свернуть("Модель, КодГербера");   	
	
	Для каждого СтрокаМодель Из ТаблицаМоделей Цикл
		
		Если Не ЗначениеЗаполнено(СтрокаМодель.Модель) Тогда
			Продолжить;
		КонецЕсли;
		
		ОбъектДокумент = Документы.Чд_ПолуфабрикатыИДеталиМодели.СоздатьДокумент();
		ОбъектДокумент.Дата = ДатаЗагрузки;
		ОбъектДокумент.Модель = СтрокаМодель.Модель;
		ОбъектДокумент.Ответственный = ПараметрыСеанса.ТекущийПользователь;
		ОбъектДокумент.Комментарий = "Создано обработкой " + ТекущаяДатаСеанса();
		КодСвязи = 0;
		
		ТаблицаЗначенийПФ = СсылкаНаПФ(СтрокаМодель); 
		
		Для каждого СтрокаПФ Из ТаблицаЗначенийПФ Цикл
			
			СсылкаНаПФ =  СтрокаПФ.СсылкаПФ;
			
			Если ЗначениеЗаполнено(СсылкаНаПФ) Тогда
				КодСвязи = КодСвязи + 10;  
				НоваяСтрокаПФ = ОбъектДокумент.Полуфабрикаты.Добавить(); 
				НоваяСтрокаПФ.Номенклатура = СсылкаНаПФ;
				НоваяСтрокаПФ.КодСвязи = КодСвязи;
				
				Для каждого СтрокаДетали Из ТаблицаЗначенийМоделей.НайтиСтроки(Новый Структура("Модель, КодГербера", СтрокаПФ.Модель, СтрокаПФ.КодГербера)) Цикл
					
					НоваяСтрокаДетали = ОбъектДокумент.ДеталиПолуфабриката.Добавить(); 
					НоваяСтрокаДетали.Номенклатура = НоваяСтрокаПФ.Номенклатура;
					НоваяСтрокаДетали.КодСвязиТЧ =  НоваяСтрокаПФ.КодСвязи;
					НоваяСтрокаДетали.КодДетали = СтрокаДетали.Деталь;
					НоваяСтрокаДетали.Количество = СтрокаДетали.КоличествоПереведено + СтрокаДетали.КоличествоВведено;
					НоваяСтрокаДетали.ПоложениеДетали = СтрокаДетали.ПоложениеДетали;
					
				КонецЦикла;
				
			КонецЕсли;
			
		КонецЦикла;
		
		Попытка
			
			Если ОбъектДокумент.ПроверитьЗаполнение() Тогда
				ОбъектДокумент.Записать(РежимЗаписиДокумента.Запись);
				ОбъектДокумент.Записать(РежимЗаписиДокумента.Проведение);
				ТекстСообщения = НСтр("ru='%1 создан и проведен.'");
				ОбщегоНазначения.СообщитьПользователю(СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
					ТекстСообщения, ОбъектДокумент.Ссылка), ОбъектДокумент.Ссылка);
				
			Иначе
				
				ОтказЗаписи = Ложь;
				Для каждого СтрокаПолуфабрикаты Из ОбъектДокумент.Полуфабрикаты Цикл
					ВыборкаРегистра = РегистрыСведений.Чд_ПолуфабрикатыИДеталиМодели.Выбрать(Новый Структура("Номенклатура", СтрокаПолуфабрикаты.Номенклатура));
					Пока ВыборкаРегистра.Следующий() Цикл
						Если ВыборкаРегистра.Регистратор <> ОбъектДокумент.Ссылка Тогда
							ТекстСообщениеПФ = "По модели " + ОбъектДокумент.Модель + " для полуфабриката " + Строка(ВыборкаРегистра.Номенклатура)
								+ " уже задан состав деталей в документе №" +  Строка(ВыборкаРегистра.Регистратор.Номер)
								+ " от " +  Строка(ВыборкаРегистра.Регистратор.Дата); 
							ОбщегоНазначения.СообщитьПользователю(ТекстСообщениеПФ);
							ОтказЗаписи = Истина;
							Прервать;
						КонецЕсли; 
					КонецЦикла; 
				КонецЦикла; 	
				
				Если Не ОтказЗаписи Тогда
					ОбъектДокумент.Записать(РежимЗаписиДокумента.Запись);
					ОбщегоНазначения.СообщитьПользователю(
						СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ТекстОшибкиЗаполнения, Строка(ОбъектДокумент.Ссылка)), 
						ОбъектДокумент); 
				КонецЕсли;
				
				Если НЕ ОбъектДокумент.Полуфабрикаты.Количество() Тогда
					ОшибкаСтроки = ТаблицаОшибочныхСтрок.Добавить();
					ОшибкаСтроки.Ошибка = "По модели " + СтрокаМодель.Модель + " не найден ни один полуфабрикат!";
				КонецЕсли; 
			КонецЕсли;
			
		Исключение
			
			ЗаписьЖурналаРегистрации(НСтр("ru = 'Формирование Полуфабрикатов и деталей модели '", ОбщегоНазначенияКлиентСервер.КодОсновногоЯзыка()),
			УровеньЖурналаРегистрации.Ошибка, , , ОбработкаОшибок.ПодробноеПредставлениеОшибки(ИнформацияОбОшибке()));
			
			ТекстОшибки = ОписаниеОшибки();
			ОбщегоНазначения.СообщитьПользователю(
			СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ТекстОшибкиЗаписи, Строка(ОбъектДокумент.Ссылка), ТекстОшибки),
			ОбъектДокумент);
			
			ОшибкаСтроки = ТаблицаОшибочныхСтрок.Добавить();
			ОшибкаСтроки.Ошибка = ТекстОшибки;
			
		КонецПопытки; 
		
	КонецЦикла;
	
КонецПроцедуры

&НаСервере
Функция СсылкаНаПФ(СтрокаПФ)
	
	ТЗ = Новый ТаблицаЗначений; 	
	Запрос = Новый Запрос;
	Запрос.Текст = 
		"ВЫБРАТЬ РАЗЛИЧНЫЕ
		|	НоменклатураДопКодГербер.Значение КАК КодГербера,
		|	НоменклатураДопКодГербер.Ссылка КАК СсылкаПФ,
		|	НоменклатураДопКодГербер.Ссылка.ЭЭ_Модель КАК Модель
		|ИЗ
		|	Справочник.Номенклатура.ДополнительныеРеквизиты КАК НоменклатураДопКодГербер
		|ГДЕ
		|	НоменклатураДопКодГербер.Свойство = &СвойствоКодГербера
		|	И НоменклатураДопКодГербер.Ссылка.ЭЭ_Модель = &Модель
		|	И НЕ НоменклатураДопКодГербер.Ссылка.ПометкаУдаления
		|	И НЕ НоменклатураДопКодГербер.Ссылка.ЭтоГруппа
		|	И НоменклатураДопКодГербер.Ссылка.ВидНоменклатуры = &ВидНоменклатуры";
	     	
	Запрос.УстановитьПараметр("СвойствоКодГербера", ПланыВидовХарактеристик.ДополнительныеРеквизитыИСведения.НайтиПоРеквизиту("Имя", "Чд_КодГербера"));
	Запрос.УстановитьПараметр("Модель", СтрокаПФ.Модель);
	Запрос.УстановитьПараметр("ВидНоменклатуры", Справочники.ЭЭ_ПредопределенныеЗначения.ВидНоменклатурыПолуфабрикатыКроя_8000.Значение);
	
	ТЗ = Запрос.Выполнить().Выгрузить();
	
	Возврат ТЗ;                	
	
КонецФункции

&НаСервере
Функция ЗагрузитьЧерезТабДок(АдресХранилища) Экспорт

	Данные = ПолучитьИзВременногоХранилища(АдресХранилища);	
	ИмяФайлаВременное = ПолучитьИмяВременногоФайла("xlsx");	
	Данные.Записать(ИмяФайлаВременное);

	ТабДок = Новый ТабличныйДокумент;
	ТабДок.Прочитать(ИмяФайлаВременное);
	УдалитьФайлы(ИмяФайлаВременное);

	втЗ = ТЗИзМакета(ТабДок);
	
	ТаблицаРезультат = Новый ТаблицаЗначений;
	
	ТаблицаРезультат.Колонки.Добавить("Модель", Новый ОписаниеТипов("СправочникСсылка.Чд_МоделиНоменклатуры"));
	ТаблицаРезультат.Колонки.Добавить("КоличествоПереведено", Новый ОписаниеТипов("Число", Новый КвалификаторыЧисла(15, 3)));
	ТаблицаРезультат.Колонки.Добавить("КоличествоВведено", Новый ОписаниеТипов("Число", Новый КвалификаторыЧисла(15, 3)));
	ТаблицаРезультат.Колонки.Добавить("Деталь", Новый ОписаниеТипов("СправочникСсылка.Чд_Детали"));
	ТаблицаРезультат.Колонки.Добавить("КодГербера", Новый ОписаниеТипов("СправочникСсылка.ЗначенияСвойствОбъектов"));
	ТаблицаРезультат.Колонки.Добавить("ПоложениеДетали", Новый ОписаниеТипов("СправочникСсылка.Чд_ПоложениеДетали"));
	
	Для Каждого СтрокавтЗ Из втЗ Цикл 
		
		ТекущееЗначениеМодель = СтрЗаменить(Формат(Число(СтрокавтЗ.model_name), "ЧРГ=."), ".", "");
		СсылкаНаМодель = Справочники.Чд_МоделиНоменклатуры.НайтиПоНаименованию(СокрЛП(ТекущееЗначениеМодель), Истина);
		ТекущаяМодель = СокрЛП(ТекущееЗначениеМодель);
		Если Не ЗначениеЗаполнено(СсылкаНаМодель) Тогда
			ОшибкаСтроки = ТаблицаОшибочныхСтрок.Добавить();
			ОшибкаСтроки.НаименованиеВладельца = ТекущееЗначениеМодель;
			ОшибкаСтроки.Ошибка = "Не найдена модель " + ТекущееЗначениеМодель + "!";
			Продолжить;	
		Иначе
			НоваяСтрока = ТаблицаРезультат.Добавить();
			НоваяСтрока.Модель = СсылкаНаМодель;
		КонецЕсли;  		
		
		НоваяСтрока.КоличествоПереведено = ?(ЗначениеЗаполнено(СтрокавтЗ.m_x), Число(СтрокавтЗ.m_x), 0);
 
		НоваяСтрока.КоличествоВведено = Число(СтрокавтЗ.asin);         
		
		СсылкаНаДеталь = Справочники.Чд_Детали.НайтиПоНаименованию(СокрЛП(СтрокавтЗ.piece_category), Истина);
		Если Не ЗначениеЗаполнено(СсылкаНаДеталь) Тогда
			ОшибкаСтроки = ТаблицаОшибочныхСтрок.Добавить();
			ОшибкаСтроки.НаименованиеВладельца = СтрокавтЗ.piece_category;
			ОшибкаСтроки.Ошибка = "Не найдена Деталь номенклатуры " +  СтрокавтЗ.piece_category + " по модели " + ТекущаяМодель + "!";
			Продолжить;	
		Иначе
			НоваяСтрока.Деталь =  СсылкаНаДеталь;
		КонецЕсли; 
		
		СсылкаНаКодГербера = КодГербераПоНаименованию(СокрЛП(СтрокавтЗ.fabric));
		Если Не ЗначениеЗаполнено(СсылкаНаКодГербера) Тогда
			Если ЗначениеЗаполнено(СтрокавтЗ.fabric) Тогда
				ОшибкаСтроки = ТаблицаОшибочныхСтрок.Добавить();
				ОшибкаСтроки.НаименованиеВладельца = СтрокавтЗ.fabric;
				ОшибкаСтроки.Ошибка = "Не найден Код Гербера номенклатуры " + СтрокавтЗ.fabric + " по модели " + ТекущаяМодель + "!";
			КонецЕсли;
			Продолжить;	
		Иначе
			НоваяСтрока.КодГербера = СсылкаНаКодГербера;
		КонецЕсли; 
		
		СсылкаНаПоложениеДетали = Справочники.Чд_ПоложениеДетали.НайтиПоНаименованию(НРег(СокрЛП(СтрокавтЗ.position)), Истина);
		Если Не ЗначениеЗаполнено(СсылкаНаПоложениеДетали) Тогда
			Если ЗначениеЗаполнено(СтрокавтЗ.position) Тогда
				ОшибкаСтроки = ТаблицаОшибочныхСтрок.Добавить();
				ОшибкаСтроки.НаименованиеВладельца = СтрокавтЗ.position;
				ОшибкаСтроки.Ошибка = "Не найдено Положение Детали номенклатуры " + СтрокавтЗ.position + " по модели " + ТекущаяМодель + "!";
			КонецЕсли;
			Продолжить;	
		Иначе
			НоваяСтрока.ПоложениеДетали  = СсылкаНаПоложениеДетали;
		КонецЕсли; 
		
	КонецЦикла;
	
	Возврат ТаблицаРезультат;
	
КонецФункции

// Метод "Microsoft ADODB"/
//
// Параметры:
//        ФайлEXCEL - Полное имя файла (путь к файлу с именем файла и расширением).
//        ИмяЛиста - Имя выбранного листа файла EXCEL.
//       В обработке 1-я строка анализируется для сопоставления колонок EXCEL с реквизитами 1С (справочники, документы, регистры).
//        НачСтрока (по-умолчанию = 0) - Номер начальной строки, начиная с которой считываются данные из EXCEL.
//        КонСтрока (по-умолчанию = 0) - Номер конечной строки, которой заканчиваются считываемые данные из EXCEL.
//            Если НачСтрока=0 и КонСтрока=0, то считывается вся таблица, находящаяся на листе EXCEL.
//        КолвоСтрокExcel - Количество строк на листе "ИмяЛиста" EXCEL. Возвращается в вызываемую процедуру.
//        ПодключениеADODB - тип драйвера ADODB для подключения к EXCEL.
//
// Возвращаемые значения:
//         ТаблицаРезультат - Результат считывания с листа "ИмяЛиста" EXCEL.
//
&НаСервере
Функция ЗагрузитьМетодом_MSADODB(АдресХранилищаФайла, Знач ФайлEXCEL, НачСтрока = 2, КонСтрока = 0, Знач ПодключениеADODB = "MicrosoftACEOLEDB12") Экспорт
	
	Перем ADODBConnection, ADODBRecordset, КолвоКолонокExcel, Поле, ИмяКолонки;
	Перем НоваяСтрока, НомерСтроки, ТаблицаРезультат;         	
	
	Данные = ПолучитьИзВременногоХранилища(АдресХранилищаФайла);
	ФайлEXCEL = ПолучитьИмяВременногоФайла();
	Данные.Записать(ФайлEXCEL);
		
	ТабДок = Новый ТабличныйДокумент;
	ТабДок.Прочитать(ПутьКФайлу, СпособЧтенияЗначенийТабличногоДокумента.Значение);
	
	// Создание результирующей таблицы, в которую будут записываться считанные из EXCEL данные.
	ТаблицаРезультат = Новый ТаблицаЗначений;
	
	// Формирование колонок результирующей таблицы.
	Для Сч = 1 По КолвоКолонокExcel Цикл
		
		Поле = ADODBRecordset.Fields.Item(Сч - 1);
		ИмяКолонки = Поле.Name;
	
		ИмяБезПробелов = "";
		Если ИмяКолонки = "model_name" Тогда
			ИмяБезПробелов = "Модель";	
			ТипКолонки = Новый ОписаниеТипов("СправочникСсылка.Чд_МоделиНоменклатуры");
			
		ИначеЕсли ИмяКолонки = "m_x" Тогда
			ИмяБезПробелов = "КоличествоПереведено";	
			ТипКолонки = Новый ОписаниеТипов("Число", Новый КвалификаторыЧисла(15, 3));
			
		ИначеЕсли ИмяКолонки = "asin" Тогда
			ИмяБезПробелов = "КоличествоВведено";	
			ТипКолонки = Новый ОписаниеТипов("Число", Новый КвалификаторыЧисла(15, 3));
			
		ИначеЕсли ИмяКолонки = "piece_category" Тогда
			ИмяБезПробелов = "Деталь";	
			ТипКолонки = Новый ОписаниеТипов("СправочникСсылка.Чд_Детали");
			
		ИначеЕсли ИмяКолонки = "fabric" Тогда
			ИмяБезПробелов = "КодГербера";	
			ТипКолонки = Новый ОписаниеТипов("СправочникСсылка.ЗначенияСвойствОбъектов");
			
		ИначеЕсли ИмяКолонки = "position" Тогда
			ИмяБезПробелов = "ПоложениеДетали";	
			ТипКолонки = Новый ОписаниеТипов("СправочникСсылка.Чд_ПоложениеДетали");
			
		КонецЕсли;
		
		Если ЗначениеЗаполнено(ИмяБезПробелов) Тогда
			ТаблицаРезультат.Колонки.Добавить(ИмяБезПробелов, ТипКолонки);
			НоваяСтрокаТаблицы = ТаблицаКолонок.Добавить();
			НоваяСтрокаТаблицы.НомерКолонки = Сч;
			НоваяСтрокаТаблицы.НаименованиеРеквизита = ИмяКолонки;
			НоваяСтрокаТаблицы.НаименованиеКолонки 	 = ИмяБезПробелов;
		КонецЕсли;
		
	КонецЦикла;
	
	// ТаблицаРезультат: Формирование строк по указанному диапазону: НачСтрока - КонСтрока.
	НомерСтроки = 1;
	Пока ADODBRecordset.EOF() = 0 Цикл
		
		НомерСтроки = НомерСтроки + 1;
		
		Если НомерСтроки < НачСтрока Тогда    // Номер строки вне диапазона считываемых строк.
			ADODBRecordset.MoveNext();        // Следующая строка.
			Продолжить;
		КонецЕсли;
				
		Если КонСтрока > 0 И НомерСтроки > КонСтрока Тогда    // Номер строки вне диапазона считываемых строк.
			Прервать;
		КонецЕсли;
		
		НоваяСтрока = ТаблицаРезультат.Добавить();
		
		ТекущаяМодель = "";
	
		Для НомерКолонки = 1 По КолвоКолонокExcel Цикл
			
			ИмяКолонки = "";
			Поле = ADODBRecordset.Fields.Item(НомерКолонки - 1);
			
			Если Поле.ActualSize = 0 Тогда        // Пустое поле EXCEL.
				Продолжить;
			КонецЕсли;
			
			МассивДанных = ТаблицаКолонок.НайтиСтроки(Новый Структура("НаименованиеРеквизита", Поле.Name));

			Если МассивДанных.Количество() = 0 Тогда
				Продолжить;
			Иначе
				ИмяКолонки = МассивДанных[0].НаименованиеКолонки;
			КонецЕсли;
			
			ТекущееЗначение = Поле.Value;        // Учитывая параметр HDR=YES в строке соединения, данные считываются в соответствии с их типом.
			
			Если ИмяКолонки = "Модель" Тогда
				ТекущееЗначение = СтрЗаменить(Формат(ТекущееЗначение, "ЧРГ=."), ".", "");
				СсылкаНаМодель = Справочники.Чд_МоделиНоменклатуры.НайтиПоНаименованию(СокрЛП(ТекущееЗначение), Истина);
				ТекущаяМодель = СокрЛП(ТекущееЗначение);
				Если Не ЗначениеЗаполнено(СсылкаНаМодель) Тогда
					ОшибкаСтроки = ТаблицаОшибочныхСтрок.Добавить();
					ОшибкаСтроки.НаименованиеВладельца = ТекущееЗначение;
					ОшибкаСтроки.Ошибка = "Не найдена модель " + ТекущееЗначение + "!";
					Продолжить;	
				Иначе
					ЗначениеДанных = СсылкаНаМодель;
				КонецЕсли; 
				
			ИначеЕсли ИмяКолонки = "КоличествоВведено" Или ИмяКолонки = "КоличествоПереведено" Тогда 
				ЗначениеДанных = Число(ТекущееЗначение);  				
				
			ИначеЕсли ИмяКолонки = "Деталь"  Тогда 
				СсылкаНаДеталь = Справочники.Чд_Детали.НайтиПоНаименованию(СокрЛП(ТекущееЗначение), Истина);
				Если Не ЗначениеЗаполнено(СсылкаНаДеталь) Тогда
					ОшибкаСтроки = ТаблицаОшибочныхСтрок.Добавить();
					ОшибкаСтроки.НаименованиеВладельца = ТекущееЗначение;
					ОшибкаСтроки.Ошибка = "Не найдена Деталь номенклатуры " + ТекущееЗначение + " по модели " + ТекущаяМодель + "!";
					Продолжить;	
				Иначе
					ЗначениеДанных = СсылкаНаДеталь;
				КонецЕсли; 
				
			ИначеЕсли ИмяКолонки = "КодГербера"  Тогда 		
				
				СсылкаНаКодГербера = КодГербераПоНаименованию(СокрЛП(ТекущееЗначение));
				Если Не ЗначениеЗаполнено(СсылкаНаКодГербера) Тогда
					Если ЗначениеЗаполнено(ТекущееЗначение) Тогда
						ОшибкаСтроки = ТаблицаОшибочныхСтрок.Добавить();
						ОшибкаСтроки.НаименованиеВладельца = ТекущееЗначение;
						ОшибкаСтроки.Ошибка = "Не найден Код Гербера номенклатуры " + ТекущееЗначение + " по модели " + ТекущаяМодель + "!";
					КонецЕсли;
					Продолжить;	
				Иначе
					ЗначениеДанных = СсылкаНаКодГербера;
				КонецЕсли; 
				
			ИначеЕсли ИмяКолонки = "ПоложениеДетали"  Тогда                     
				СсылкаНаПоложениеДетали = Справочники.Чд_ПоложениеДетали.НайтиПоНаименованию(НРег(СокрЛП(ТекущееЗначение)), Истина);
				Если Не ЗначениеЗаполнено(СсылкаНаПоложениеДетали) Тогда
					Если ЗначениеЗаполнено(ТекущееЗначение) Тогда
						ОшибкаСтроки = ТаблицаОшибочныхСтрок.Добавить();
						ОшибкаСтроки.НаименованиеВладельца = ТекущееЗначение;
						ОшибкаСтроки.Ошибка = "Не найдено Положение Детали номенклатуры " + ТекущееЗначение + " по модели " + ТекущаяМодель + "!";
					КонецЕсли;
					Продолжить;	
				Иначе
					ЗначениеДанных = СсылкаНаПоложениеДетали;
				КонецЕсли; 
				
			КонецЕсли; 
			
			НоваяСтрока[ИмяКолонки] = ЗначениеДанных;
			
		КонецЦикла;
		
		ADODBRecordset.MoveNext();   // Следующая строка.
		
	КонецЦикла;
	
	ADODBRecordset.Close();
	ADODBConnection.Close();
	ADODBRecordset  = Неопределено;
	ADODBConnection = Неопределено;
	
	Возврат ТаблицаРезультат;
	
КонецФункции

&НаСервере
Функция ТЗИзМакета(ТабДок, КолонкаИндекса = 1)
    
    ТаблицаДанных = Новый ТаблицаЗначений;
    НомерКолонки = 0;
    Пока Истина Цикл
        НомерКолонки = НомерКолонки + 1;
        ИмяКолонки = ТабДок.Область(1, НомерКолонки).Текст;
        Если ПустаяСтрока(ИмяКолонки) Тогда
            Прервать;
        КонецЕсли;        
        ТаблицаДанных.Колонки.Добавить(ИмяКолонки);
    КонецЦикла;
    
    СчетчикКолонок = НомерКолонки - 1;
    
    НомерСтроки = 1; 
	ФлагПрерывания = Ложь;
    Пока Истина Цикл
        НомерСтроки = НомерСтроки + 1;
        Стр = ТаблицаДанных.Добавить();
        
        Для А = 1 По СчетчикКолонок Цикл
            ТекстКолонки = ТабДок.Область(НомерСтроки, А).Текст;
            Если ПустаяСтрока(ТекстКолонки) Тогда
                Если А = КолонкаИндекса Тогда
                    ФлагПрерывания = Истина;
                    ТаблицаДанных.Удалить(Стр);
                КонецЕсли;
            Иначе
                Стр[А - 1] = ТекстКолонки;
            КонецЕсли;
            
            Если ФлагПрерывания Тогда
                Прервать;
            КонецЕсли;
        КонецЦикла;
        
        Если ФлагПрерывания Тогда
            Прервать;
        КонецЕсли;
    КонецЦикла;
    
    Возврат ТаблицаДанных;
	
КонецФункции

#КонецОбласти