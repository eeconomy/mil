﻿
#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ПередачаПродукцииИзПодразделенийНаЛЦ(Команда)
	
	ОписаниеОповещения = Новый ОписаниеОповещения("ВыполнитьПослеСозданияДокумента", ЭтотОбъект);

	ПараметрыФормы = Новый Структура("ВыбранныйРежим", "ПередачаПродукцииИзПодразделенийНаЛЦ");
	ОткрытьФорму("Обработка.ЭЭ_МРМСозданиеНакладныхНаОтгрузкуНаЛЦ.Форма.ФормаСканирования", ПараметрыФормы,
					ЭтотОбъект, Истина, , , ОписаниеОповещения);
					
КонецПроцедуры

&НаКлиенте
Процедура ПередачаПродукцииПервогоПредъявленияНаЛЦ(Команда)
	
	ОписаниеОповещения = Новый ОписаниеОповещения("ВыполнитьПослеСозданияДокумента", ЭтотОбъект);

	ПараметрыФормы = Новый Структура("ВыбранныйРежим", "ПередачаПродукцииПервогоПредъявленияНаЛЦ");
	ОткрытьФорму("Обработка.ЭЭ_МРМСозданиеНакладныхНаОтгрузкуНаЛЦ.Форма.ФормаСканирования", ПараметрыФормы,
					ЭтотОбъект, Истина, , , ОписаниеОповещения);
					
КонецПроцедуры

&НаКлиенте
Процедура ПередачаПродукцииСИсправленийНаЛЦ(Команда)
	
	ОписаниеОповещения = Новый ОписаниеОповещения("ВыполнитьПослеСозданияДокумента", ЭтотОбъект);

	ПараметрыФормы = Новый Структура("ВыбранныйРежим", "ПередачаПродукцииСИсправленийНаЛЦ");
	ОткрытьФорму("Обработка.ЭЭ_МРМСозданиеНакладныхНаОтгрузкуНаЛЦ.Форма.ФормаСканирования", ПараметрыФормы,
					ЭтотОбъект, Истина, , , ОписаниеОповещения);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаКлиенте
Процедура ВыполнитьПослеСозданияДокумента(ВыбранноеЗначение, ДополнительныеПараметры) Экспорт

	Если ЗначениеЗаполнено(ВыбранноеЗначение) Тогда
		ОбщегоНазначенияКлиент.СообщитьПользователю(ВыбранноеЗначение); 	
	КонецЕсли; 	
	
КонецПроцедуры

#КонецОбласти
