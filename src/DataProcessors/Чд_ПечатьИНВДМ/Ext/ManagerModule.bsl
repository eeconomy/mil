﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область СлужебныеПроцедурыИФункции

#Область Печать

Процедура Печать(МассивОбъектов, ПараметрыПечати, КоллекцияПечатныхФорм, ОбъектыПечати, ПараметрыВывода) Экспорт
	
	СтруктураТипов = ОбщегоНазначенияУТ.СоответствиеМассивовПоТипамОбъектов(МассивОбъектов);;
	
	Если УправлениеПечатью.НужноПечататьМакет(КоллекцияПечатныхФорм, "ИНВДМ") Тогда
		
		УправлениеПечатью.ВывестиТабличныйДокументВКоллекцию(
			КоллекцияПечатныхФорм,
			"ИНВДМ",
			НСтр("ru = 'Инвентаризационная опись драгоценных металлов (ИНВ-8а)'"),
		
			СформироватьПечатнуюФормуИНвентаризацииДМ(СтруктураТипов, ОбъектыПечати, ПараметрыПечати));
			КонецЕсли;
	
КонецПроцедуры

Функция СформироватьПечатнуюФормуИНвентаризацииДМ(СтруктураТипов, ОбъектыПечати, ПараметрыПечати) Экспорт
	
	ТабличныйДокумент = Новый ТабличныйДокумент;
	ТабличныйДокумент.ИмяПараметровПечати = "ПАРАМЕТРЫ_ПЕЧАТИ_ИНВДМ";
	
	УстановитьПривилегированныйРежим(Истина);
	
	НомерДокумента = 0;
	
	Для Каждого СтруктураОбъектов Из СтруктураТипов Цикл
			
		Если СтруктураОбъектов.Ключ <> "Документ.ИнвентаризационнаяОписьТМЦ" И СтруктураОбъектов.Ключ <> "Документ.ИнвентаризационнаяОпись" Тогда 
			ТекстСообщения = НСтр("ru = 'Формирование печатной формы ""ИНВ-3"" доступно только для документов ""%ТипДокумента%"".'");
			ТекстСообщения = СтрЗаменить(ТекстСообщения, "%ТипДокумента%", Метаданные.Документы.ИнвентаризационнаяОпись.Синоним);
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения);
			Продолжить;
		КонецЕсли;
		
		МенеджерОбъекта = ОбщегоНазначения.МенеджерОбъектаПоПолномуИмени(СтруктураОбъектов.Ключ);
			
		Для Каждого ДокументОснование Из СтруктураОбъектов.Значение Цикл
			
			НомерДокумента = НомерДокумента + 1;
			Если НомерДокумента > 1 Тогда
				ТабличныйДокумент.ВывестиГоризонтальныйРазделительСтраниц();
			КонецЕсли;
						
			ДанныеДляПечати = МенеджерОбъекта.ПолучитьДанныеДляПечатнойФормыИНВДМ(ПараметрыПечати, ДокументОснование);
			Если НЕ ДанныеДляПечати = Неопределено Тогда
				ЗаполнитьТабличныйДокументИНВ3(ТабличныйДокумент, ДанныеДляПечати, ОбъектыПечати, ДокументОснование);
			КонецЕсли;
			
		КонецЦикла;
	
	КонецЦикла;
	
	УстановитьПривилегированныйРежим(Ложь);
	
	Возврат ТабличныйДокумент;
	
КонецФункции

Процедура ЗаполнитьТабличныйДокументИНВ3(ТабличныйДокумент, ДанныеДляПечати, ОбъектыПечати, ИнвентаризационнаяОпись)
	
	ТабличныйДокумент.ПолеСверху = 10;
	ТабличныйДокумент.ПолеСлева = 10;
	ТабличныйДокумент.ПолеСнизу = 10;
	ТабличныйДокумент.ПолеСправа = 10;
	ТабличныйДокумент.РазмерКолонтитулаСверху = 0;
	ТабличныйДокумент.РазмерКолонтитулаСнизу = 0;
	ТабличныйДокумент.АвтоМасштаб = Истина;
	ТабличныйДокумент.ОриентацияСтраницы = ОриентацияСтраницы.Ландшафт;
	
	Шапка = ДанныеДляПечати.РезультатПоШапке.Выбрать();
	ТабличнаяЧасть = ДанныеДляПечати.РезультатПоТабличнойЧасти.Выбрать();
	
	Шапка.Следующий();
	СтруктураРеквизитов = Новый Структура("Дата, Склад");
	СтруктураРеквизитов = ОбщегоНазначения.ЗначенияРеквизитовОбъекта(Шапка.Ссылка, СтруктураРеквизитов);
			
	Если ДанныеДляПечати.РезультатКурсВалюты <> Неопределено Тогда
		КурсВалюты = ДанныеДляПечати.РезультатКурсВалюты.Выбрать();
		Если КурсВалюты.Следующий() Тогда
			КоэффициентПересчета = КурсВалюты.КоэффициентПересчета;
		Иначе
			КоэффициентПересчета = 1;
		КонецЕсли;
	Иначе  
		КоэффициентПересчета = 1;		
	КонецЕсли;
	
	ВалютаРеглУчета = Константы.ВалютаРегламентированногоУчета.Получить();
	
	Если ТабличнаяЧасть.Количество() = 0 Тогда
		ТекстСообщения = НСтр("ru='В рамках периода документа ""%Опись%"" не были оформлены складские акты или не было проведено ни одного пересчета товаров. Нет данных для формирования печатной формы ""ИНВ-3""'");
		ТекстСообщения = СтрЗаменить(ТекстСообщения, "%Опись%", ИнвентаризационнаяОпись);
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, ИнвентаризационнаяОпись);
		Возврат;
	КонецЕсли;
	
	НомерСтрокиНачало = ТабличныйДокумент.ВысотаТаблицы + 1;                                                                	
	Макет = УправлениеПечатью.МакетПечатнойФормы("Обработка.Чд_ПечатьИНВДМ.ПФ_MXL_ИНВДМ");
	
	ОбластьЗаголовок      = Макет.ПолучитьОбласть("Заголовок");
	ОбластьШапки          = Макет.ПолучитьОбласть("ШапкаТаблицы");     
	ОбластьСтроки         = Макет.ПолучитьОбласть("Строка");
	ОбластьПодвалСтраницы = Макет.ПолучитьОбласть("ПодвалСтраницы");
	ОбластьПодвалОписи    = Макет.ПолучитьОбласть("ПодвалОписи");
	ОбластьПодпись        = Макет.ПолучитьОбласть("Подпись");
	ОбластьПодвал         = Макет.ПолучитьОбласть("Подвал");
	
	ОбластьЗаголовок.Параметры.Заполнить(Шапка);
	ОбластьЗаголовок.Параметры.ДолжностьМОЛ1  = Шапка.ДолжностьКладовщика;
	ОбластьЗаголовок.Параметры.ФИОМОЛ1        = ФизическиеЛицаУТ.ФамилияИнициалыФизЛица(Шапка.Кладовщик, Шапка.ДатаДокумента);
	ОбластьЗаголовок.Параметры.НомерДокумента = ПрефиксацияОбъектовКлиентСервер.НомерНаПечать(Шапка.НомерДокумента);
	ОбластьЗаголовок.Параметры.ОснованиеНомер = ПрефиксацияОбъектовКлиентСервер.НомерНаПечать(Шапка.ОснованиеНомер);
	ОбластьЗаголовок.Параметры.ПредставлениеОрганизации = ЗначениеНастроекПовтИсп.ПолучитьОрганизациюПоУмолчанию();
	
	ШтрихкодированиеПечатныхФорм.ВывестиШтрихкодВТабличныйДокумент(ТабличныйДокумент, Макет, ОбластьЗаголовок, Шапка.Ссылка); 
	ТабличныйДокумент.Вывести(ОбластьЗаголовок);
	ТабличныйДокумент.ВывестиГоризонтальныйРазделительСтраниц();
	
	ИтоговыеСуммы = Новый Структура;
	
	ИтоговыеСуммы.Вставить("ИтогоПоСтраницеФактКоличество",0);
	ИтоговыеСуммы.Вставить("ИтогоПоСтраницеФактСумма",0);
	ИтоговыеСуммы.Вставить("ИтогоПоСтраницеБухКоличество",0);
	ИтоговыеСуммы.Вставить("ИтогоФактКоличество",0);
	ИтоговыеСуммы.Вставить("ИтогоБухКоличество",0);
	
	НомерСтроки = 0;
	НомерСтраницы = 2;
	ПервыйНомерСтроки = 1;
	// Создаем массив для проверки вывода
	МассивВыводимыхОбластей = Новый Массив;
	
	КоличествоСтрок = ТабличнаяЧасть.Количество();
	
	Пока ТабличнаяЧасть.Следующий() Цикл
		
		НомерСтроки = НомерСтроки + 1;
		
		ОбластьСтроки.Параметры.Заполнить(ТабличнаяЧасть);
		
		ОбластьСтроки.Параметры.ТоварНаименование = НоменклатураКлиентСервер.ПредставлениеНоменклатурыДляПечати(
			ТабличнаяЧасть.ТоварНаименование,
			ТабличнаяЧасть.ХарактеристикаНаименование,
			ТабличнаяЧасть.СерияНаименование);
		
		ОбластьСтроки.Параметры.НомерСтроки = НомерСтроки;
		Если НомерСтроки = 1 Тогда // первая строка
			
			ТекстСтраница = НСтр("ru='Страница %НомерСтраницы%'");
			ТекстСтраница = СтрЗаменить(ТекстСтраница,"%НомерСтраницы%",2);
			ОбластьШапки.Параметры.НомерСтраницы = ТекстСтраница;
			
			ТабличныйДокумент.Вывести(ОбластьШапки);
			
		Иначе
			
			МассивВыводимыхОбластей.Очистить();
			МассивВыводимыхОбластей.Добавить(ОбластьСтроки);
			МассивВыводимыхОбластей.Добавить(ОбластьПодвалСтраницы);
			
			Если НомерСтроки <> 1 И Не ТабличныйДокумент.ПроверитьВывод(МассивВыводимыхОбластей) Тогда
				
				ОбластьПодвалСтраницы.Параметры.Заполнить(ИтоговыеСуммы);
				ОбластьПодвалСтраницы.Параметры.КоличествоПорядковыхНомеровНаСтраницеПрописью = ЧислоПрописью(НомерСтроки - ПервыйНомерСтроки, ,",,,,,,,,0");
				ОбластьПодвалСтраницы.Параметры.ОбщееКоличествоЕдиницФактическиНаСтраницеПрописью = ФормированиеПечатныхФорм.КоличествоПрописью(ИтоговыеСуммы.ИтогоПоСтраницеФактКоличество);
				ПервыйНомерСтроки = НомерСтроки;
				
				ТабличныйДокумент.Вывести(ОбластьПодвалСтраницы);
				
				// Очистим итоги по странице.
				ИтоговыеСуммы.ИтогоПоСтраницеФактКоличество = 0;
				ИтоговыеСуммы.ИтогоПоСтраницеБухКоличество = 0;
				
				НомерСтраницы = НомерСтраницы + 1;
				ТабличныйДокумент.ВывестиГоризонтальныйРазделительСтраниц();
				
				ТекстСтраница = НСтр("ru='Страница %НомерСтраницы%'");
				ТекстСтраница = СтрЗаменить(ТекстСтраница,"%НомерСтраницы%",НомерСтраницы);
				ОбластьШапки.Параметры.НомерСтраницы = ТекстСтраница;
				ТабличныйДокумент.Вывести(ОбластьШапки);
				
			КонецЕсли;
			
		КонецЕсли;
		
		ТабличныйДокумент.Вывести(ОбластьСтроки);
		
		ИтоговыеСуммы.ИтогоПоСтраницеФактКоличество = ИтоговыеСуммы.ИтогоПоСтраницеФактКоличество + ТабличнаяЧасть.КоличествоФактическое;
		ИтоговыеСуммы.ИтогоПоСтраницеБухКоличество = ИтоговыеСуммы.ИтогоПоСтраницеБухКоличество + ТабличнаяЧасть.КоличествоПоУчету;
		ИтоговыеСуммы.ИтогоФактКоличество = ИтоговыеСуммы.ИтогоФактКоличество + ТабличнаяЧасть.КоличествоФактическое;
		ИтоговыеСуммы.ИтогоБухКоличество = ИтоговыеСуммы.ИтогоБухКоличество + ТабличнаяЧасть.КоличествоПоУчету;
	КонецЦикла;
	
	Если ПервыйНомерСтроки <> НомерСтроки ИЛИ ПервыйНомерСтроки = ТабличнаяЧасть.Количество() Тогда
		ОбластьПодвалСтраницы.Параметры.Заполнить(ИтоговыеСуммы);
		ОбластьПодвалСтраницы.Параметры.КоличествоПорядковыхНомеровНаСтраницеПрописью = ЧислоПрописью(НомерСтроки - ПервыйНомерСтроки + 1, ,",,,,,,,,0");
		ОбластьПодвалСтраницы.Параметры.ОбщееКоличествоЕдиницФактическиНаСтраницеПрописью = ФормированиеПечатныхФорм.КоличествоПрописью(ИтоговыеСуммы.ИтогоПоСтраницеФактКоличество);
		ТабличныйДокумент.Вывести(ОбластьПодвалСтраницы);
	КонецЕсли;
	
	ОбластьПодвалОписи.Параметры.КоличествоПорядковыхНомеровПрописью     = ЧислоПрописью(НомерСтроки, ,",,,,,,,,0");
	ОбластьПодвалОписи.Параметры.ОбщееКоличествоЕдиницФактическиПрописью = ФормированиеПечатныхФорм.КоличествоПрописью(ИтоговыеСуммы.ИтогоФактКоличество);
	
	ТекстСтраница = НСтр("ru='Страница %НомерСтраницы%'");
	ТекстСтраница = СтрЗаменить(ТекстСтраница,"%НомерСтраницы%",НомерСтраницы + 1);
	ОбластьПодвалОписи.Параметры.НомерСтраницы = ТекстСтраница;
	
	ТабличныйДокумент.ВывестиГоризонтальныйРазделительСтраниц();
	ТабличныйДокумент.Вывести(ОбластьПодвалОписи);
	
	Если ТипЗнч(Шапка.Ссылка) = Тип("ДокументСсылка.ИнвентаризационнаяОписьТМЦ") ИЛИ
	
		ТипЗнч(Шапка.Ссылка) = Тип("ДокументСсылка.ИнвентаризационнаяОпись") Тогда
		
		СтруктураКомиссии = Справочники.КомиссииДляПечатныхФорм.ПолучитьСтруктуруКомиссии(Шапка.Комиссия);
		
		РезультатПоКомиссии = СтруктураКомиссии.РезультатПоКомиссии; // выборка
		ТаблицаИнвентаризационнаяКомиссия = СтруктураКомиссии.ТаблицаИнвентаризационнаяКомиссия; // таблица значений
		
		Если  ТаблицаИнвентаризационнаяКомиссия.Количество()  <= 3 Тогда
			ОбластьПодпись.Параметры.Заполнить(РезультатПоКомиссии);
			ОбластьПодпись.Параметры.Председатель = ФизическиеЛицаУТ.ФамилияИнициалыФизЛица(РезультатПоКомиссии.Председатель);
			Количество = Мин(ТаблицаИнвентаризационнаяКомиссия.Количество(), 3);
			Для Сч = 1 По Количество Цикл
				ОбластьПодпись.Параметры["Сотрудник" + Сч] = ФизическиеЛицаУТ.ФамилияИнициалыФизЛица(ТаблицаИнвентаризационнаяКомиссия[Сч-1].Сотрудник);
				ОбластьПодпись.Параметры["Должность" + Сч] = ТаблицаИнвентаризационнаяКомиссия[Сч-1].Должность;
			КонецЦикла;
			
			ТабличныйДокумент.Вывести(ОбластьПодпись);
			
		ИначеЕсли ТаблицаИнвентаризационнаяКомиссия.Количество() > 3 Тогда 
			
			ПодписьПредседателя = Макет.ПолучитьОбласть("ПодписьПредседателя");
			ПодписьЧленовКомиссии  = Макет.ПолучитьОбласть("ПодписьЧленовКомиссии");
			ПодписьПредседателя.Параметры.Заполнить(РезультатПоКомиссии);
			ПодписьПредседателя.Параметры.Председатель = ФизическиеЛицаУТ.ФамилияИнициалыФизЛица(РезультатПоКомиссии.Председатель);
			ТабличныйДокумент.Вывести(ПодписьПредседателя);
			ПервыйЧлен = Истина;
			Для каждого Сотрудник Из ТаблицаИнвентаризационнаяКомиссия Цикл
				Если ПервыйЧлен Тогда
					ПервыйЧлен = Ложь;
					ПодписьЧленовКомиссии.Параметры.ЧленыКомиссии = "Члены комиссии:";
				Иначе
					ПодписьЧленовКомиссии.Параметры.ЧленыКомиссии = "";
				КонецЕсли; 
				
				ПодписьЧленовКомиссии.Параметры.Заполнить(Сотрудник);
				ПодписьЧленовКомиссии.Параметры.Сотрудник = ФизическиеЛицаУТ.ФамилияИнициалыФизЛица(Сотрудник.Сотрудник);
				ТабличныйДокумент.Вывести(ПодписьЧленовКомиссии);
			КонецЦикла;
			
		КонецЕсли;
		
	КонецЕсли;
	
	ОбластьПодвал.Параметры.НомерСтраницы = НомерСтраницы;
	ОбластьПодвал.Параметры.ФИОМОЛ1 = ФизическиеЛицаУТ.ФамилияИнициалыФизЛица(Шапка.Кладовщик, Шапка.ДатаДокумента);
	ТабличныйДокумент.Вывести(ОбластьПодвал);
	
	УправлениеПечатью.ЗадатьОбластьПечатиДокумента(ТабличныйДокумент, НомерСтрокиНачало, ОбъектыПечати, Шапка.Ссылка);
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#КонецЕсли
