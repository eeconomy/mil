﻿	
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
		
	ЭЭ_РабочееМестоРаботникаСкладаСервер.ПриСозданииНаСервере(ЭтотОбъект, Отказ, СтандартнаяОбработка);
	ЗвукСирены = ПолучитьОбщийМакет("ЭЭ_ЗвукСиреныМРМ");
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	#Если НЕ МобильныйКлиент Тогда
	    ТекстСообщения = НСтр("ru='Форма МРМ доступна только при запуске из мобильного клиента 1С!'");
		ОбщегоНазначенияКлиент.СообщитьПользователю(ТекстСообщения, , , , Отказ);
		Возврат;	
	#КонецЕсли

	// МеханизмВнешнегоОборудования
	МенеджерОборудованияКлиент.НачатьПодключениеОборудованиеПриОткрытииФормы(
		Новый ОписаниеОповещения("НачатьПодключениеОборудованиеПриОткрытииФормыЗавершение", ЭтотОбъект),
		ЭтотОбъект,
		"СканерШтрихкода");
	// Конец МеханизмВнешнегоОборудования
	
	ПодключитьОбработчикОжидания("Подключаемый_ОбновитьСписокЗаданийОбработчикОжидания", 60);
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	// ПодключаемоеОборудование
	Если Источник = "ПодключаемоеОборудование" И ЭтотОбъект.ВводДоступен() Тогда
		
		Если ИмяСобытия = "ScanData" И МенеджерОборудованияУТКлиент.ЕстьНеобработанноеСобытие() Тогда
			
			// Преобразуем предварительно к ожидаемому формату
			Если Параметр[1] = Неопределено Тогда
				Штрихкод = Параметр[0];
			Иначе
				Штрихкод = Параметр[1][1];
			КонецЕсли;
			
			Если ПараметрыРежима.Режим = "ЗапросИнформации" Тогда
				
				ВывестиИнформациюПоШтрихкодуНаСервере(Штрихкод);
				
			ИначеЕсли ПараметрыРежима.Режим = "Сканирование" Тогда
				
				ПриСканированииЗначенияНаСервере(Штрихкод);
				
				Если ЗначениеЗаполнено(СообщениеОбОшибке) Тогда
					#Если МобильныйКлиент Тогда
						// в некоторых ТСД не поддерживается тип данных .wav для мелодии
						Попытка
							СредстваМультимедиа.ВоспроизвестиАудио(ЗвукСирены);
						Исключение
							// не обрабатываем
						КонецПопытки;
					#КонецЕсли
				КонецЕсли;
				
			ИначеЕсли ПараметрыРежима.Режим = "ВыборЗадания"
				Или ПараметрыРежима.Режим = "ВыборОперации" Тогда
				
				ПриСканированииШтрихкодаЗаданияНаСервере(Штрихкод);
				
			Иначе
				#Если МобильныйКлиент Тогда
					Попытка
						СредстваМультимедиа.ВоспроизвестиАудио(ЗвукСирены);
					Исключение
						// не обрабатываем
					КонецПопытки;
				#КонецЕсли
			КонецЕсли;
			
			ПодключитьОбработчикОжидания("Подключаемый_ВывестиСостояниеВыполненияЗаданияОбработчикОжидания", 5, Истина);
			
		КонецЕсли;
	ИначеЕсли ИмяСобытия = "СозданоНовоеЗаданиеНаРазмещение"
		И Источник = "УправлениеПоступлением"
		И Элементы.СтраницыФормы.ТекущаяСтраница = "СтраницаВыборОперации" Тогда
		
		ОсновноеМенюОбновитьНаСервере();
	КонецЕсли;
	// Конец ПодключаемоеОборудование
	
КонецПроцедуры

&НаКлиенте
Процедура ПередЗакрытием(Отказ, ЗавершениеРаботы, ТекстПредупреждения, СтандартнаяОбработка)
	
	Если ЗавершениеРаботы Тогда
		РабочееМестоРаботникаСкладаКлиент.ПередЗакрытием(ЭтотОбъект, Отказ, ТекстПредупреждения, СтандартнаяОбработка);
		Возврат;
	КонецЕсли;
	
	ПередЗакрытиемНаСервере(Отказ, СтандартнаяОбработка);
	
КонецПроцедуры

&НаКлиенте
Процедура ПриЗакрытии(ЗавершениеРаботы)
	
	ЭЭ_РаботаМРМКлиент.ПриЗакрытии(ЭтотОбъект, ЗавершениеРаботы);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыЗадания

&НаКлиенте
Процедура СписокЗаданийВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	
	СписокЗаданийВыборНаСервере();
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыЗоныприемкиотгрузки

&НаКлиенте
Процедура ЗоныПриемкиРазмещенияПередНачаломДобавления(Элемент, Отказ, Копирование, Родитель, Группа)
	
	Отказ = Истина;
	
КонецПроцедуры

&НаКлиенте
Процедура ЗоныПриемкиРазмещенияПередНачаломИзменения(Элемент, Отказ)
	
	Отказ = Истина;
	
КонецПроцедуры

&НаКлиенте
Процедура ЗоныПриемкиРазмещенияПередУдалением(Элемент, Отказ)
	
	Отказ = Истина;
	
КонецПроцедуры

&НаКлиенте
Процедура ЗоныПриемкиРазмещенияВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	
	ПриВыбореЗоныПриемкиРазмещенияНаСервере();
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ОсновноеМенюОбновить(Команда)
	
	ОсновноеМенюОбновитьНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура СканированиеВвестиКоличество(Команда)
	
	СканированиеВвестиКоличествоНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ВводКоличестваОК(Команда)
	
	РабочееМестоРаботникаСкладаКлиент.ВводКоличестваОК(ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура ВводКоличестваОтмена(Команда)
	
	ВводКоличестваОтменаНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ОсновноеМенюИнформация(Команда)
	
	ОсновноеМенюИнформацияНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ЗаданияПоказатьВсе(Команда)
	
	ЗаданияПоказатьВсеНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ОсновноеМенюОтбор(Команда)
	
	ОсновноеМенюОтборНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ОсновноеМенюПроверкаОтбора(Команда)
	
	ОсновноеМенюПроверкаОтбораНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ОсновноеМенюРазмещение(Команда)
	
	ОсновноеМенюРазмещениеНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ОсновноеМенюПеремещение(Команда)
	
	ОсновноеМенюПеремещениеНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ОсновноеМенюПересчет(Команда)
	
	ОсновноеМенюПересчетНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ОсновноеМенюОтметитьЯчейкуКПересчету(Команда)
	
	ОсновноеМенюОтметитьЯчейкуКПересчетуНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ОсновноеМенюЗакрыть(Команда)
	
	ОсновноеМенюЗакрытьНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура СканированиеИнформация(Команда)
	
	СканированиеИнформацияНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура СканированиеЯчейка(Команда)
	
	РабочееМестоРаботникаСкладаКлиент.СканированиеЯчейка(ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура СканированиеСерия(Команда)
	
	ТекстСообщенияПользователю = "";
	
КонецПроцедуры

&НаКлиенте
Процедура СканированиеТовар(Команда)
	
	СканированиеТоварНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура СоздатьНовоеСкладскоеЗадание(Команда)
	
	СоздатьНовоеСкладскоеЗаданиеНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура СканированиеДалее(Команда)
	
	СканированиеДалееНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ЗакрытьФормуМобильногоРабочегоМеста(Команда)
	
	РабочееМестоРаботникаСкладаКлиент.ЗакрытьФормуМобильногоРабочегоМеста(ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура НеЗакрыватьФормуМобильногоРабочегоМеста(Команда)
	
	НеЗакрыватьФормуМобильногоРабочегоМестаНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ОтменитьВыполнениеЗадания(Команда)
	
	ОтменитьВыполнениеЗаданияНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ЗавершитьВыполнениеЗадания(Команда)
	
	ЗавершитьВыполнениеЗаданияНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ПерейтиКРазмещениюТоваров(Команда)
	
	ПерейтиКРазмещениюТоваровНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ПерейтиКСледующемуТоваруСканирования(Команда)
	
	ПерейтиКСледующейСтрокеСканированияНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ПерейтиКСледующейЯчейкеСканирования(Команда)
	
	ПерейтиКСледующейЯчейкеСканированияНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ПодобратьТоварыИзДругихЯчеекПриПереходеКСледующейЯчейке(Команда)
	
	ПодобратьТоварыИзДругихЯчеекПриПереходеКСледующейЯчейкеНаСервере();
	ПодключитьОбработчикОжидания("Подключаемый_ВывестиСостояниеВыполненияЗаданияОбработчикОжидания", 5, Истина);
	
КонецПроцедуры

&НаКлиенте
Процедура ОтменитьПодборТоваровИзДругихЯчеекПриПереходеКСледующейЯчейке(Команда)
	
	ОтменитьПодборТоваровИзДругихЯчеекПриПереходеКСледующейЯчейкеНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ПодобратьТоварыИзДругихЯчеек(Команда)
	
	ПодобратьТоварыИзДругихЯчеекНаСервере();
	ПодключитьОбработчикОжидания("Подключаемый_ВывестиСостояниеВыполненияЗаданияОбработчикОжидания", 5, Истина);
	
КонецПроцедуры

&НаКлиенте
Процедура ОтменитьПодборТоваровИзДругихЯчеек(Команда)
	
	ОтменитьПодборТоваровИзДругихЯчеекНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ДобратьТовары(Команда)
	
	ДобратьТоварыНаСервере();
	ПодключитьОбработчикОжидания("Подключаемый_ВывестиСостояниеВыполненияЗаданияОбработчикОжидания", 5, Истина);
	
КонецПроцедуры

&НаКлиенте
Процедура СканированиеВвестиЗначение(Команда)
	
	СканированиеВвестиЗначениеНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ВыборЗначенияОК(Команда)
	
	РабочееМестоРаботникаСкладаКлиент.ВыборЗначенияОК(ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура ВыборЗначенияОтмена(Команда)
	
	ВыборЗначенияОтменаНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ПерейтиКСканированию(Команда)
	
	ПерейтиКСканированиюНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ПерейтиКПриемкеТоваров(Команда)
	
	ПерейтиКПриемкеТоваровНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ПерейтиНаПредыдущуюСтраницу(Команда)
	
	ПерейтиНаПредыдущуюСтраницуНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ВернутьсяКВыполнениюЗадания(Команда)
	
	ВернутьсяКВыполнениюЗаданияНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ПерейтиКВыборуЗаданияНаОтбор(Команда)
	
	ПерейтиКВыборуЗаданияНаОтборНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ПерейтиКВыборуЗаданияНаРазмещение(Команда)
	
	ПерейтиКВыборуЗаданияНаРазмещениеНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ПерейтиКВыборуЗаданияНаПеремещение(Команда)
	
	ПерейтиКВыборуЗаданияНаПеремещениеНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ПерейтиКВыборуЗаданияНаПересчет(Команда)
	
	ПерейтиКВыборуЗаданияНаПересчетНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ПерейтиКВыборуЗоныПриемки(Команда)
	
	ПерейтиКВыборуЗоныПриемкиНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ЗаданияОбновить(Команда)
	
	ЗаданияОбновитьНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ПрерватьВыполнениеЗадания(Команда)
	
	ОтменитьВыполнениеЗаданияНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура СканированиеНазначение(Команда)
	
	СканированиеНазначениеНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура НазначенияВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	НазначенияВыбратьНаСервере();
КонецПроцедуры

&НаКлиенте
Процедура ПерейтиКВыборуСкладскойОперации(Команда)
	
	ПерейтиКВыборуСкладскойОперацииНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура СканированиеНеОтгружать(Команда)
	
	СканированиеНеОтгружатьНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура СканированиеСервис(Команда)
	
	СканированиеСервисНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура СканированиеОтметитьЯчейкуКПересчету(Команда)
	
	СканированиеОтметитьЯчейкуКПересчетуНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура СервисПорядокОбработкиЯчейкаТовар(Команда)
	
	СервисПорядокОбработкиЯчейкаТоварНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура СервисПорядокОбработкиТоварЯчейка(Команда)
	
	СервисПорядокОбработкиТоварЯчейкаНаСервере();
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область Подключаемые

&НаКлиенте
Процедура Подключаемый_ВводКоличестваОКОбработчикОжидания()
	
	ВводКоличестваОКОбработчикОжиданияНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ВыборЗначенияОКОбработчикОжидания()
	
	ВыборЗначенияОКОбработчикОжиданиянаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ВыборАдресаОКОбработчикОжидания()
	
	ВыборАдресаОКОбработчикОжиданияНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ОбновитьСписокЗаданийОбработчикОжидания()
	
	Если РабочееМестоРаботникаСкладаКлиент.НужноОбновитьСписокЗаданий(ЭтотОбъект) Тогда
		ОбновитьСписокЗаданийОбработчикОжиданияНаСервере();
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ВывестиСостояниеВыполненияЗаданияОбработчикОжидания()
	
	Если РабочееМестоРаботникаСкладаКлиент.НужноОбновитьСостояниеВыполненияЗадания(ЭтотОбъект) Тогда
		ВывестиСостояниеВыполненияЗаданияОбработчикОжиданияНаСервере();
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область Прочие

&НаКлиенте
Процедура ВводКоличестваКоличествоПриИзменении(Элемент)
	
	РабочееМестоРаботникаСкладаКлиент.ВводКоличестваКоличествоПриИзменении(ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура ВводКоличестваВесПриИзменении(Элемент)
	
	РабочееМестоРаботникаСкладаКлиент.ВводКоличестваВесПриИзменении(ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура ВводКоличестваОбъемПриИзменении(Элемент)
	
	РабочееМестоРаботникаСкладаКлиент.ВводКоличестваОбъемПриИзменении(ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура ШтрихкодПараметраСканированияПриИзменении(Элемент)
	
	РабочееМестоРаботникаСкладаКлиент.ШтрихкодПараметраСканированияПриИзменении(ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура АдресЯчейкиПриИзменении(Элемент)
	
	РабочееМестоРаботникаСкладаКлиент.АдресЯчейкиПриИзменении(ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура СрокГодностиСерииПриИзменении(Элемент)
	
	РабочееМестоРаботникаСкладаКлиент.СрокГодностиСерииПриИзменении(ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Процедура ОсновноеМенюОбновитьНаСервере()
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.ОбновитьОсновноеМеню(ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Процедура СоздатьНовоеСкладскоеЗаданиеНаСервере()
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.СоздатьНовоеСкладскоеЗадание(ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Процедура ОтменитьВыполнениеЗаданияНаСервере()
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.ОтменитьВыполнениеЗадания(ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Процедура ЗавершитьВыполнениеЗаданияНаСервере()
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.ЗавершитьВыполнениеЗадания(ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Процедура ПерейтиКРазмещениюТоваровНаСервере()
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.ПриЗавершенииСканированияТекущегоЗадания(ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Процедура ПерейтиКСледующейСтрокеСканированияНаСервере()
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.ПерейтиКСледующемуТоваруСканирования(ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Процедура ПерейтиКСледующейЯчейкеСканированияНаСервере()
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.ПерейтиКСледующейЯчейкеСканирования(ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Процедура СканированиеВвестиЗначениеНаСервере()
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.СканированиеВвестиЗначение(ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Процедура ПерейтиКСканированиюНаСервере()
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.ПерейтиКСканированию(ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Процедура ПерейтиКПриемкеТоваровНаСервере()
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.ПерейтиКПриемкеТоваров(ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Процедура ОсновноеМенюОтборНаСервере()
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.ПерейтиКВыборуЗадания(ЭтотОбъект, "Отбор");
	
КонецПроцедуры

&НаСервере
Процедура ЗаданияОбновитьНаСервере()
	
	ТипЗадания = ПараметрыРежима.ТипЗадания;
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.ПерейтиКВыборуЗадания(ЭтотОбъект, ТипЗадания);
	
КонецПроцедуры

&НаСервере
Процедура ОсновноеМенюЗакрытьНаСервере()
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.ОсновноеМенюЗакрыть(ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Процедура СканированиеДалееНаСервере()
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.СканированиеДалее(ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Процедура ОсновноеМенюИнформацияНаСервере()
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.ПерейтиКПолучениюИнформации(ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Процедура ОсновноеМенюОтметитьЯчейкуКПересчетуНаСервере()
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.ПерейтиКОтметкеЯчейкиКПересчету(ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Процедура СканированиеИнформацияНаСервере()
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.ПерейтиКПолучениюИнформации(ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Процедура ОбновитьСписокЗаданийОбработчикОжиданияНаСервере()
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.ОбновитьСписокЗаданийОбработчикОжидания(ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Процедура НеЗакрыватьФормуМобильногоРабочегоМестаНаСервере()
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.НеЗакрыватьФормуМобильногоРабочегоМеста(ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Процедура ПередЗакрытиемНаСервере(Отказ, СтандартнаяОбработка)
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.ПередЗакрытием(ЭтотОбъект, Отказ, СтандартнаяОбработка);
	
КонецПроцедуры

&НаСервере
Процедура ЗаданияПоказатьВсеНаСервере()
	
	ТипЗадания = ПараметрыРежима.ТипЗадания;
	Если ТипЗадания = "Отбор" Тогда
		Элементы.СтраницыФормы.ТекущаяСтраница = Элементы.СтраницаФильтрЗаданий;
		Элементы.КодПроцесса.Видимость = ОтборПоКодуПроцесса;
	Иначе
		ЭЭ_РабочееМестоРаботникаСкладаСервер.ОтобразитьВсеТолькоСвоиСкладскиеЗадания(ЭтотОбъект);
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ВыборЗначенияОтменаНаСервере()
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.ВыборЗначенияОтмена(ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Процедура ВернутьсяКВыполнениюЗаданияНаСервере()
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.ВернутьсяКВыполнениюЗадания(ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Процедура СканированиеТоварНаСервере()
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.ВывестиИнформациюПоТекущемуТовару(ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Процедура ВыборЗначенияОКОбработчикОжиданияНаСервере()
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.ВыборЗначенияОКОбработчикОжидания(ЭтотОбъект);
	
	ШтрихкодПараметраСканирования = "";
	СрокГодностиСерии = "";
	
КонецПроцедуры

&НаСервере
Процедура ВыборАдресаОКОбработчикОжиданияНаСервере()
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.ВыборАдресаОКОбработчикОжидания(ЭтотОбъект);
	
	ШтрихкодПараметраСканирования = "";
	
КонецПроцедуры

&НаСервере
Процедура ВывестиСостояниеВыполненияЗаданияОбработчикОжиданияНаСервере()
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.ВывестиСостояниеВыполненияЗадания(ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Процедура ОсновноеМенюРазмещениеНаСервере()
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.ПерейтиКВыборуЗадания(ЭтотОбъект, "Размещение");
	
КонецПроцедуры

&НаСервере
Процедура ОсновноеМенюПеремещениеНаСервере()
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.ПерейтиКВыборуЗадания(ЭтотОбъект, "Перемещение");
	
КонецПроцедуры

&НаСервере
Процедура ОсновноеМенюПроверкаОтбораНаСервере()
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.ПерейтиКВыборуЗадания(ЭтотОбъект, "ПроверкаОтбора");
	
КонецПроцедуры

&НаСервере
Процедура ОсновноеМенюПересчетНаСервере()
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.ПерейтиКВыборуЗадания(ЭтотОбъект, "Пересчет");
	
КонецПроцедуры

&НаСервере
Процедура ПриВыбореЗоныПриемкиРазмещенияНаСервере()
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.ПриВыбореЗоныПриемкиОтгрузки(ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Процедура СписокЗаданийВыборНаСервере()
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.ПриВыбореЗадания(ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Процедура СканированиеВвестиКоличествоНаСервере()
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.СканированиеВвестиКоличество(ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Процедура ВводКоличестваОКОбработчикОжиданияНаСервере()
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.ВводКоличестваОКОбработчикОжидания(ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Процедура ВводКоличестваОтменаНаСервере()
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.ВводКоличестваОтмена(ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Процедура ПерейтиКВыборуСкладскойОперацииНаСервере()
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.ПерейтиКВыборуСкладскойОперации(ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Процедура СканированиеНеОтгружатьНаСервере()
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.СканированиеНеОтгружать(ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Процедура ПерейтиНаПредыдущуюСтраницуНаСервере()
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.ПерейтиНаПредыдущуюСтраницу(ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Процедура СканированиеСервисНаСервере()
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.СканированиеСервис(ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Процедура СканированиеОтметитьЯчейкуКПересчетуНаСервере()
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.ПерейтиКОтметкеЯчейкиКПересчету(ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Процедура СервисПорядокОбработкиТоварЯчейкаНаСервере()
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.ПриУстановкеПорядкаОбработкиТоварЯчейка(ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Процедура СервисПорядокОбработкиЯчейкаТоварНаСервере()
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.ПриУстановкеПорядкаОбработкиЯчейкаТовар(ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Процедура ВывестиИнформациюПоШтрихкодуНаСервере(Штрихкод)
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.ВывестиИнформациюПоШтрихкоду(ЭтотОбъект, Штрихкод);
	
КонецПроцедуры

&НаСервере
Процедура ПриСканированииЗначенияНаСервере(Штрихкод)
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.ПриСканированииЗначения(ЭтотОбъект, Штрихкод, , Истина);
	
КонецПроцедуры

&НаСервере
Процедура ПриСканированииШтрихкодаЗаданияНаСервере(Штрихкод)
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.ПриСканированииШтрихкодаЗадания(ЭтотОбъект, Штрихкод);
	
КонецПроцедуры

&НаСервере
Процедура ПерейтиКВыборуЗаданияНаРазмещениеНаСервере()
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.ПерейтиКВыборуЗадания(ЭтотОбъект, "Размещение");
	
КонецПроцедуры

&НаСервере
Процедура ПерейтиКВыборуЗаданияНаПеремещениеНаСервере()
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.ПерейтиКВыборуЗадания(ЭтотОбъект, "Перемещение");
	
КонецПроцедуры

&НаСервере
Процедура ПерейтиКВыборуЗаданияНаПересчетНаСервере()
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.ПерейтиКВыборуЗадания(ЭтотОбъект, "Пересчет");
	
КонецПроцедуры

&НаСервере
Процедура ПерейтиКВыборуЗаданияНаОтборНаСервере()
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.ПерейтиКВыборуЗадания(ЭтотОбъект, "Отбор");
	
КонецПроцедуры

&НаСервере
Процедура ПерейтиКВыборуЗоныПриемкиНаСервере()
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.ПерейтиКВыборуЗоныПриемкиОтгрузки(ЭтотОбъект, "ЗонаПриемки");
	
КонецПроцедуры

&НаСервере
Процедура ПриОшибкеПодключенияОборудованияНаСервере(ОписаниеОшибки)
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.ПриОшибкеПодключенияОборудования(ЭтотОбъект, ОписаниеОшибки);
	
КонецПроцедуры

&НаСервере
Процедура СканированиеНазначениеНаСервере()
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.ПерейтиКВыборуНазначения(ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Процедура НазначенияВыбратьНаСервере()
	ЭЭ_РабочееМестоРаботникаСкладаСервер.ПриВыбореНазначения(ЭтотОбъект);
КонецПроцедуры

&НаСервере
Процедура ПодобратьТоварыИзДругихЯчеекПриПереходеКСледующейячейкеНаСервере()
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.ПодобратьТоварыИзДругихЯчеекПриПереходеКСледующейЯчейке(ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Процедура ОтменитьПодборТоваровИзДругихЯчеекПриПереходеКСледующейЯчейкеНаСервере()
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.ОтменитьПодборТоваровИзДругихЯчеекПриПереходеКСледующейЯчейке(ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Процедура ПодобратьТоварыИзДругихЯчеекНаСервере()
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.ПодобратьТоварыИзДругихЯчеек(ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Процедура ОтменитьПодборТоваровИзДругихЯчеекНаСервере()
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.ОтменитьПодборТоваровИзДругихЯчеек(ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Процедура ДобратьТоварыНаСервере()
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.ДобратьТовары(ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура ЗапускРабочегоМестаРаботникаСкладаНеВТаксиЗавершение(Ответ, ДополнительныеПараметры) Экспорт
	
	Если Ответ = КодВозвратаДиалога.ОК Тогда
		
		ЗапускРабочегоМестаРаботникаСкладаНеВТаксиЗавершениеНаСервере();
		ЗавершитьРаботуСистемы(Ложь, Истина);
		
	Иначе
		
		ЗавершитьРаботуСистемы(Ложь);
		
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ЗапускРабочегоМестаРаботникаСкладаНеВТаксиЗавершениеНаСервере()
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.УстановитьВариантИнтерфейсаКлиентскогоПриложенияВерсии8_2(
		ПользователиИнформационнойБазы.ТекущийПользователь().Имя);
	
КонецПроцедуры

&НаКлиенте
Процедура НачатьПодключениеОборудованиеПриОткрытииФормыЗавершение(РезультатВыполнения, ДополнительныеПараметры) Экспорт
	
	Если НЕ РезультатВыполнения.Результат Тогда
		
		ПриОшибкеПодключенияОборудованияНаСервере(РезультатВыполнения.ОписаниеОшибки);
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область Интек

&НаКлиенте
Процедура ФильтрПрименить(Команда)
	
	УстановитьФильтрНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ОтборПоКодуПроцессаПриИзменении(Элемент)
	
	Если ОтборПоКодуПроцесса Тогда
		Элементы.КодПроцесса.Видимость = Истина;
	Иначе
		Элементы.КодПроцесса.Видимость = Ложь;
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура УстановитьФильтрНаСервере()
	
	ЭЭ_РабочееМестоРаботникаСкладаСервер.Чд_УстановитьФильтрПоСкладскимЗаданиям(ЭтотОбъект);
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти