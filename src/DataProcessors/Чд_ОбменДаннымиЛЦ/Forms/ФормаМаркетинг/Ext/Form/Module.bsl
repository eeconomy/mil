﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	ЗаполнитьТаблицуЗаказов();

КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура ПутьКФайлуЗагрузкиНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	ВыбратьФайлДляВыгрузкиНаКлиенте();
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ВыполнитьОперацию(Команда)
	
	Если Не ЗначениеЗаполнено(ПутьККаталогуВыгрузки) Тогда
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(
			НСтр("ru = 'Заполните путь к каталогу выгрузки файлов.'"));
	  	Возврат;
	КонецЕсли;
	
	ИдентификаторЗадания = Неопределено;
	ПодключитьОбработчикОжидания("ВыгрузитьДанные", 1, Истина);
	
КонецПроцедуры

&НаКлиенте
Процедура ИсключитьСтроки(Команда)
	
	МассивДанных = Элементы.ТаблицаЗаказов.ВыделенныеСтроки;
	ОтметитьСтроки(Ложь, МассивДанных);
	
КонецПроцедуры

&НаКлиенте
Процедура ВыбратьСтроки(Команда)
	
	МассивДанных = Элементы.ТаблицаЗаказов.ВыделенныеСтроки;
	ОтметитьСтроки(Истина, МассивДанных);
	
КонецПроцедуры

&НаКлиенте
Процедура ОбновитьЗаказы(Команда)

	ЗаполнитьТаблицуЗаказов();
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаКлиенте
Процедура ВыгрузитьДанные()
	
	ОчиститьСообщения();
	Элементы.ДекорацияПоясняющийТекстДлительнойОперации.Заголовок = НСтр("ru='Выполняется выгрузка данных...'");

	МассивСообщений = Неопределено;
	ДлительнаяОперация = ВыгрузитьДанныеНаСервере();
	ИдентификаторЗадания = ДлительнаяОперация.ИдентификаторЗадания;
	Если ДлительнаяОперация.Статус = "Выполнено" Тогда
		АдресХраненияРезультата = ДлительнаяОперация.АдресРезультата;
		ДлительнаяОперация.Свойство("Сообщения", МассивСообщений);
		ОбработатьРезультатВыгрузки();
	Иначе
		МодульДлительныеОперацииКлиент = ОбщегоНазначенияКлиент.ОбщийМодуль("ДлительныеОперацииКлиент");
		ПараметрыОжидания = МодульДлительныеОперацииКлиент.ПараметрыОжидания(ЭтотОбъект);
		ПараметрыОжидания.ВыводитьОкноОжидания = Ложь;
		
		ОписаниеОповещения = Новый ОписаниеОповещения("ПриЗавершенииОперацииВыгрузки", ЭтотОбъект);
		МодульДлительныеОперацииКлиент.ОжидатьЗавершение(ДлительнаяОперация, ОписаниеОповещения, ПараметрыОжидания);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПриЗавершенииОперацииВыгрузки(Результат, ДополнительныеПараметры) Экспорт
	
	Если Результат = Неопределено Тогда
		ВызватьИсключение НСтр("ru='Выгрузка данных завершена неудачно. Отсутствует результат выгрузки.'");
	ИначеЕсли Результат.Статус = "Ошибка" Тогда
		ВызватьИсключение Результат.КраткоеПредставлениеОшибки;
	КонецЕсли;
	
	АдресХраненияРезультата = Результат.АдресРезультата;
	ОбработатьРезультатВыгрузки();
	
КонецПроцедуры

&НаСервере
Функция ВыгрузитьДанныеНаСервере()
	
	ОбработкаОбъект = РеквизитФормыВЗначение("Объект");	
	МетаОбработкаИмя = ОбработкаОбъект.Метаданные().Имя;
	ЧастиИмени = СтрРазделить(ИмяФормы, ".");	
	БазовоеИмяДляФормы = "Обработка." + МетаОбработкаИмя;
	ИмяОбработки = ЧастиИмени[1];
	
	МассивЗаказов = Новый Массив;
	Для Каждого СтрокаТаб Из ТаблицаЗаказов.НайтиСтроки(Новый Структура("Выбран", Истина)) Цикл
		МассивЗаказов.Добавить(СтрокаТаб.ДокументСсылка);
	КонецЦикла;
		
	АдресХраненияРезультата = "";
	СтруктураПараметров = Новый Структура;
	СтруктураПараметров.Вставить("МассивЗаказов", МассивЗаказов);
	СтруктураПараметров.Вставить("ПутьККаталогуВыгрузки", ПутьККаталогуВыгрузки);
	СтруктураПараметров.Вставить("ЭтоФоновоеЗадание", Истина);

	ПараметрыЗадания = Новый Структура;
	ПараметрыЗадания.Вставить("ИмяМетода", "ВыгрузкаДанныхМаркетинга");
	ПараметрыЗадания.Вставить("ПараметрыВыполнения", СтруктураПараметров);
	ПараметрыЗадания.Вставить("ЭтоВнешняяОбработка", Ложь);
	ПараметрыЗадания.Вставить("ИмяОбработки", ИмяОбработки);

	ВыполняемыйМетод = "ДлительныеОперации.ВыполнитьПроцедуруМодуляОбъектаОбработки";

	МодульДлительныеОперации = ОбщегоНазначения.ОбщийМодуль("ДлительныеОперации");
	ПараметрыВыполнения = МодульДлительныеОперации.ПараметрыВыполненияВФоне(УникальныйИдентификатор);
	ПараметрыВыполнения.НаименованиеФоновогоЗадания = НСтр("ru = 'Выгрузка заказов клиентам.'");
	РезультатФоновогоЗадания = МодульДлительныеОперации.ВыполнитьВФоне(ВыполняемыйМетод, ПараметрыЗадания, ПараметрыВыполнения);
	
	Возврат РезультатФоновогоЗадания;
	
КонецФункции

&НаКлиенте
Процедура ВыбратьФайлДляВыгрузкиНаКлиенте(ТипВыгрузки = "ТипВыгрузки", ЗагружатьПослеВыбора = Ложь)
	
	ДополнительныеПараметры = Новый Структура;
	ДополнительныеПараметры.Вставить("ЗагружатьПослеВыбора", ЗагружатьПослеВыбора);
	ДополнительныеПараметры.Вставить(ТипВыгрузки, ТипВыгрузки);	
	
	Оповещение = Новый ОписаниеОповещения("ПодключитьРасширениеРаботыСФайламиЗавершение", ЭтотОбъект, ДополнительныеПараметры);
	НачатьПодключениеРасширенияРаботыСФайлами(Оповещение);
	
КонецПроцедуры

&НаКлиенте
Процедура ПодключитьРасширениеРаботыСФайламиЗавершение(Подключено, ДополнительныеПараметры) Экспорт
	
	Если Не Подключено Тогда
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(
			НСтр("ru = 'Для продолжения необходимо установить расширение работы с файлами.'"));
		Возврат;
	КонецЕсли;
	
	ДиалогОткрытияФайла = Новый ДиалогВыбораФайла(РежимДиалогаВыбораФайла.ВыборКаталога);
	
	ПараметрыОповещения = Новый Структура;
	ПараметрыОповещения.Вставить(ДополнительныеПараметры.ТипВыгрузки);
	ПараметрыОповещения.Вставить("ЗагружатьПослеВыбора", ДополнительныеПараметры.ЗагружатьПослеВыбора);
	
	ОповещениеВыбора = Новый ОписаниеОповещения("ВыбранКаталог", ЭтотОбъект, ПараметрыОповещения);
	ДиалогОткрытияФайла.Показать(ОповещениеВыбора);
	
КонецПроцедуры

&НаКлиенте
Процедура ВыбранКаталог(ВыбранныйКаталог, ДополнительныеПараметры) Экспорт
	
	Если ВыбранныйКаталог = Неопределено Тогда
		Возврат;
	КонецЕсли;
	ПутьККаталогуВыгрузки = ВыбранныйКаталог[0];
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработатьРезультатВыгрузки()
	
	ВывестиСообщенияФоновогоЗадания();
	ИдентификаторЗадания = Неопределено;
	Сообщение = Новый СообщениеПользователю();
	Сообщение.Текст = НСтр("ru = 'Выгрузка данных завершена.'");
	Сообщение.Сообщить();
	
КонецПроцедуры

&НаКлиенте
Процедура ВывестиСообщенияФоновогоЗадания()
	
	Если НЕ ЗначениеЗаполнено(МассивСообщений) Тогда
		МассивСообщений = ПрочитатьСообщенияФоновогоЗадания(ИдентификаторЗадания);
	КонецЕсли;
	
	Если ЗначениеЗаполнено(МассивСообщений) Тогда
		Для Каждого ТекСообщение Из МассивСообщений Цикл
			Если СтрНачинаетсяС(ТекСообщение.Текст, "{") Тогда
				Продолжить;
			КонецЕсли;
			ТекСообщение.Сообщить();
		КонецЦикла;
	КонецЕсли;
	
КонецПроцедуры

&НаСервереБезКонтекста
Функция ПрочитатьСообщенияФоновогоЗадания(Идентификатор)
	
	Если НЕ ЗначениеЗаполнено(Идентификатор) Тогда
		Возврат Неопределено;
	КонецЕсли;
	
	Задание = ФоновыеЗадания.НайтиПоУникальномуИдентификатору(Идентификатор);
	Если Задание = Неопределено Тогда
		Возврат Неопределено;
	КонецЕсли;
	
	Возврат Задание.ПолучитьСообщенияПользователю(Истина);
	
КонецФункции

&НаСервере
Процедура ОтметитьСтроки(Значение, ВыделенныеСтроки)
	
	Для каждого ИдентификаторСтроки Из ВыделенныеСтроки Цикл
		ТекущаяСтрока = ТаблицаЗаказов.НайтиПоИдентификатору(ИдентификаторСтроки);
		ТекущаяСтрока.Выбран = Значение;
	КонецЦикла;
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьТаблицуЗаказов()
	
	ТаблицаЗаказов.Очистить();
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
		"ВЫБРАТЬ РАЗЛИЧНЫЕ
		|	ЛОЖЬ КАК Выбран,
		|	ЗаказКлиентаТовары.Ссылка КАК ДокументСсылка
		|ИЗ
		|	Документ.ЗаказКлиента КАК ЗаказКлиента
		|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Документ.ЗаказКлиента.Товары КАК ЗаказКлиентаТовары
		|		ПО ЗаказКлиента.Ссылка = ЗаказКлиентаТовары.Ссылка
		|ГДЕ
		|	ЗаказКлиента.Проведен
		|	И ЗаказКлиента.Склад = &СкладМилавицыЛЦ
		|	И ЗаказКлиентаТовары.Номенклатура.Чд_КлассификацияНоменклатуры = ЗНАЧЕНИЕ(Перечисление.Чд_КлассификацияНоменклатуры.РекламнаяПродукция)
		|	И НЕ ЗаказКлиентаТовары.Отменено";
	
	Запрос.УстановитьПараметр("СкладМилавицыЛЦ", Константы.ЭЭ_СкладОтгрузкиМилавицаЛЦ.Получить());
	ТаблицаЗаказов.Загрузить(Запрос.Выполнить().Выгрузить());
	
КонецПроцедуры

#КонецОбласти
