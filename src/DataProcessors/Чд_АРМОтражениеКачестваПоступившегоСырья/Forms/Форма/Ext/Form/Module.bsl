﻿#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	ДокументыПрихода.Параметры.УстановитьЗначениеПараметра("НачалоПериода", 
		?(ЗначениеЗаполнено(Период.ДатаНачала), Период.ДатаНачала, Дата(1, 1, 1)));
	ДокументыПрихода.Параметры.УстановитьЗначениеПараметра("КонецПериода",
		?(ЗначениеЗаполнено(Период.ДатаОкончания), Период.ДатаОкончания, Дата(1, 1, 1)));
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	Если ЗначениеЗаполнено(Период) Тогда
		УстановитьОтборПоПериоду();
	КонецЕсли;
	
	ПоказатьВсеПриходыПриИзмененииНаСервере();
	ФильтрАкты = "Все";
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура АктыПриемкиТМЦПоКачеству(Команда)
	
	ОткрытьФорму("Документ.Чд_АктПриемкиТМЦ.ФормаСписка");
	
КонецПроцедуры

&НаКлиенте
Процедура Отчет(Команда)
	
	ОткрытьФорму("Отчет.Чд_КачествоФурнитурыПоПоставщикам.ФормаОбъекта");
	
КонецПроцедуры

&НаКлиенте
Процедура ОтчетПоАктам(Команда)
	
	ОткрытьФорму("Отчет.Чд_ОтчетПоАктамПриемкиТМЦ.ФормаОбъекта");
	
КонецПроцедуры

&НаКлиенте
Процедура ОтчетВ2(Команда)
	
	ОткрытьФорму("Отчет.Чд_КачествоФурнитурыПоПровереннойФурнитуре.ФормаОбъекта");
	
КонецПроцедуры

&НаКлиенте
Процедура УстановитьИнтервал(Команда)
	
	Оповещение = Новый ОписаниеОповещения("УстановитьИнтервалЗавершение", ЭтотОбъект);
	ОбщегоНазначенияУтКлиент.РедактироватьПериод(Период, , Оповещение);
	
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьДокумент(Команда)
	
	ТекДанные = Элементы.ДокументыПрихода.ТекущиеДанные;
	СсылкаНаДокумент = ТекДанные.Ссылка;
	ПараметрыОткрытия = Новый Структура("Ключ", СсылкаНаДокумент);
	Попытка
		Если ТипЗнч(СсылкаНаДокумент) = Тип("ДокументСсылка.ПриобретениеТоваровУслуг") Тогда
			ОткрытьФорму("Документ.ПриобретениеТоваровУслуг.Форма.ФормаДокумента", ПараметрыОткрытия);
		ИначеЕсли ТипЗнч(СсылкаНаДокумент) = Тип("ДокументСсылка.ПоступлениеОтПереработчика") Тогда
			ОткрытьФорму("Документ.ПоступлениеОтПереработчика.Форма.ФормаДокумента", ПараметрыОткрытия);
		КонецЕсли;
	Исключение
		ОбщегоНазначенияКлиент.СообщитьПользователю("Документ не может быть открыт");
	КонецПопытки;
	
КонецПроцедуры

&НаКлиенте
Процедура СоздатьАктыПоКачеству(Команда)
	
	МассивОбъектов = Элементы.ДокументыПрихода.ВыделенныеСтроки;
	СсылкаМассивОбъектов = Новый Массив;
	Для Каждого ТекДанные Из МассивОбъектов Цикл
		
		СсылкаНаДокумент = Элементы.ДокументыПрихода.ДанныеСтроки(ТекДанные);
		
		ПараметрыЗаполнения = Новый Структура("ДокументОснование,ДокументыОснованиеПредставление", СсылкаНаДокумент, СсылкаНаДокумент);
		
		АктСсылка = СсылкаНаАкт(ПараметрыЗаполнения, СсылкаНаДокумент.Склад);
		
		СсылкаМассивОбъектов.Добавить(СсылкаНаДокумент.Ссылка);
		
	КонецЦикла;
	
	УстановитьОтборыВЗависимыхСписках(СсылкаМассивОбъектов);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура ДокументыПриходаВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	ОбщегоНазначенияУТКлиент.ИзменитьЭлемент(Элемент);
	
КонецПроцедуры

&НаКлиенте
Процедура АктыПриемкиПоКачествуВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	ТекДанные = Элементы.АктыПриемкиПоКачеству.ТекущиеДанные;
	СсылкаНаДокумент = ТекДанные.Ссылка;
	ПараметрыОткрытия = Новый Структура("Ключ", СсылкаНаДокумент);
	ОткрытьФорму("Документ.АктОРасхожденияхПослеПриемки.Форма.Чд_ФормаДокумента", ПараметрыОткрытия);
	
КонецПроцедуры

&НаКлиенте
Процедура ПоказатьВсеПриходыПриИзменении(Элемент)
	
	ПоказатьВсеПриходыПриИзмененииНаСервере();
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура УстановитьОтборыВЗависимыхСписках(ДокументОснование)
	
	Если ФильтрАкты = "Все" Тогда
		
		Запрос = Новый Запрос;
		Запрос.Текст =
			"ВЫБРАТЬ
			|	АктОРасхожденияхПослеПриемкиТовары.ДокументОснование КАК ДокументОснование,
			|	АктОРасхожденияхПослеПриемкиТовары.Ссылка КАК Ссылка
			|ИЗ
			|	Документ.АктОРасхожденияхПослеПриемки.Товары КАК АктОРасхожденияхПослеПриемкиТовары";
		
		АктыОРасхожденияхПослеПриемки = Запрос.Выполнить().Выгрузить().ВыгрузитьКолонку("Ссылка");
		
		ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(АктыПриемкиПоКачеству, "Ссылка",
			АктыОРасхожденияхПослеПриемки, ВидСравненияКомпоновкиДанных.ВСписке, , Истина);
		
	ИначеЕсли ФильтрАкты = "ПоДокументу" Тогда
		
		Запрос = Новый Запрос;
		Запрос.Текст =
			"ВЫБРАТЬ
			|	АктОРасхожденияхПослеПриемкиТовары.ДокументОснование КАК ДокументОснование,
			|	АктОРасхожденияхПослеПриемкиТовары.Ссылка КАК Ссылка
			|ИЗ
			|	Документ.АктОРасхожденияхПослеПриемки.Товары КАК АктОРасхожденияхПослеПриемкиТовары
			|ГДЕ
			|	АктОРасхожденияхПослеПриемкиТовары.ДокументОснование В(&ДокументОснование)";
		
		Запрос.УстановитьПараметр("ДокументОснование", ДокументОснование);
		
		АктыОРасхожденияхПослеПриемки = Запрос.Выполнить().Выгрузить().ВыгрузитьКолонку("Ссылка");
		
		ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(АктыПриемкиПоКачеству, "Ссылка",
			АктыОРасхожденияхПослеПриемки, ВидСравненияКомпоновкиДанных.ВСписке, , Истина);
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ДокументыПриходаПриАктивизацииСтроки(Элемент)
	ФильтрАктыПриИзменении(Элемент);
КонецПроцедуры

&НаСервере
Функция ТекстЗапросаДокументыПрихода()
	
	ТекстЗапроса =
		"ВЫБРАТЬ
		|	1 КАК ЕстьДокументАкт,
		|	АктОРасхожденияхПослеПриемкиТовары.ДокументОснование КАК ДокументОснование
		|ПОМЕСТИТЬ ВТ_ДокументОснование
		|ИЗ
		|	Документ.АктОРасхожденияхПослеПриемки.Товары КАК АктОРасхожденияхПослеПриемкиТовары
		|ГДЕ
		|	АктОРасхожденияхПослеПриемкиТовары.Ссылка.Проведен
		|	И (АктОРасхожденияхПослеПриемкиТовары.Ссылка.Дата >= &НачалоПериода
		|			ИЛИ &НачалоПериода = ДАТАВРЕМЯ(1, 1, 1))
		|	И (АктОРасхожденияхПослеПриемкиТовары.Ссылка.Чд_СтатусКачество = ЗНАЧЕНИЕ(Перечисление.Чд_СтатусКачество.Архив)
		|			ИЛИ АктОРасхожденияхПослеПриемкиТовары.Ссылка.Чд_СтатусКачество = ЗНАЧЕНИЕ(Перечисление.Чд_СтатусКачество.АрхивСПретензией))
		|
		|СГРУППИРОВАТЬ ПО
		|	АктОРасхожденияхПослеПриемкиТовары.ДокументОснование
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ РАЗЛИЧНЫЕ
		|	ПоступлениеОтПереработчикаТовары.Ссылка КАК Ссылка,
		|	ПоступлениеОтПереработчикаТовары.Склад КАК Склад,
		|	ПоступлениеОтПереработчикаТовары.Ссылка.Номер КАК Номер,
		|	ПоступлениеОтПереработчикаТовары.Ссылка.Дата КАК ДатаДок,
		|	ПоступлениеОтПереработчикаТовары.Ссылка.НомерВходящегоДокумента КАК НомерПервичногоДокумента,
		|	ПоступлениеОтПереработчикаТовары.Ссылка.ДатаВходящегоДокумента КАК ДатаПервичногоДокумента,
		|	ПоступлениеОтПереработчикаТовары.Ссылка.Контрагент КАК Контрагент,
		|	ПоступлениеОтПереработчикаТовары.Ссылка.Проведен КАК Проведен,
		|	ПоступлениеОтПереработчикаТовары.Ссылка.ПометкаУдаления КАК ПометкаУдаления,
		|	ВЫБОР
		|		КОГДА ПоступлениеОтПереработчикаТовары.Ссылка.Проведен
		|			ТОГДА 0
		|		КОГДА ПоступлениеОтПереработчикаТовары.Ссылка.ПометкаУдаления
		|			ТОГДА 1
		|		ИНАЧЕ 2
		|	КОНЕЦ КАК НестандартнаяКартинка
		|ИЗ
		|	Документ.ПоступлениеОтПереработчика.Товары КАК ПоступлениеОтПереработчикаТовары
		|		ЛЕВОЕ СОЕДИНЕНИЕ ВТ_ДокументОснование КАК ВТ_ДокументОснование
		|		ПО ПоступлениеОтПереработчикаТовары.Ссылка = ВТ_ДокументОснование.ДокументОснование
		|ГДЕ
		|	ПоступлениеОтПереработчикаТовары.Ссылка.Проведен
		|	И (ПоступлениеОтПереработчикаТовары.Ссылка.Дата >= &НачалоПериода
		|			ИЛИ &НачалоПериода = ДАТАВРЕМЯ(1, 1, 1))
		|	И (ПоступлениеОтПереработчикаТовары.Ссылка.Дата <= &КонецПериода
		|			ИЛИ &КонецПериода = ДАТАВРЕМЯ(1, 1, 1))
		|	И ВЫБОР
		|			КОГДА ВТ_ДокументОснование.ЕстьДокументАкт = 1
		|				ТОГДА ИСТИНА
		|			ИНАЧЕ ЛОЖЬ
		|		КОНЕЦ = ЛОЖЬ
		|
		|ОБЪЕДИНИТЬ ВСЕ
		|
		|ВЫБРАТЬ РАЗЛИЧНЫЕ
		|	ПриобретениеТоваровУслугТовары.Ссылка,
		|	ПриобретениеТоваровУслугТовары.Склад,
		|	ПриобретениеТоваровУслугТовары.Ссылка.Номер,
		|	ПриобретениеТоваровУслугТовары.Ссылка.Дата,
		|	ПриобретениеТоваровУслугТовары.Ссылка.НомерВходящегоДокумента,
		|	ПриобретениеТоваровУслугТовары.Ссылка.ДатаВходящегоДокумента,
		|	ПриобретениеТоваровУслугТовары.Ссылка.Контрагент,
		|	ПриобретениеТоваровУслугТовары.Ссылка.Проведен,
		|	ПриобретениеТоваровУслугТовары.Ссылка.ПометкаУдаления,
		|	ВЫБОР
		|		КОГДА ПриобретениеТоваровУслугТовары.Ссылка.Проведен
		|			ТОГДА 0
		|		КОГДА ПриобретениеТоваровУслугТовары.Ссылка.ПометкаУдаления
		|			ТОГДА 1
		|		ИНАЧЕ 2
		|	КОНЕЦ
		|ИЗ
		|	Документ.ПриобретениеТоваровУслуг.Товары КАК ПриобретениеТоваровУслугТовары
		|		ЛЕВОЕ СОЕДИНЕНИЕ ВТ_ДокументОснование КАК ВТ_ДокументОснование
		|		ПО ПриобретениеТоваровУслугТовары.Ссылка = ВТ_ДокументОснование.ДокументОснование
		|ГДЕ
		|	ПриобретениеТоваровУслугТовары.Ссылка.Проведен
		|	И (ПриобретениеТоваровУслугТовары.Ссылка.Дата >= &НачалоПериода
		|			ИЛИ &НачалоПериода = ДАТАВРЕМЯ(1, 1, 1))
		|	И (ПриобретениеТоваровУслугТовары.Ссылка.Дата <= &КонецПериода
		|			ИЛИ &КонецПериода = ДАТАВРЕМЯ(1, 1, 1))
		|	И ВЫБОР
		|			КОГДА ВТ_ДокументОснование.ЕстьДокументАкт = 1
		|				ТОГДА ИСТИНА
		|			ИНАЧЕ ЛОЖЬ
		|		КОНЕЦ = ЛОЖЬ
		|	И ПриобретениеТоваровУслугТовары.Ссылка.Чд_Статус = ЗНАЧЕНИЕ(Перечисление.Чд_СтатусыПриобретенияТоваровУслуг.Приобретено)";
	
	Возврат ТекстЗапроса;
	
КонецФункции

// отображение приходов проведенных и со свойством "Приобретено"
&НаСервере
Процедура ПоказатьВсеПриходыПриИзмененииНаСервере()
	
	ТекстЗапроса = ТекстЗапросаДокументыПрихода();
	Если ПоказатьВсеПриходы = Ложь Тогда
		СвойстваСписка = ОбщегоНазначения.СтруктураСвойствДинамическогоСписка();
		СвойстваСписка.ТекстЗапроса = ТекстЗапроса;
		ОбщегоНазначения.УстановитьСвойстваДинамическогоСписка(Элементы.ДокументыПрихода, СвойстваСписка);
	Иначе
		ТекстЗапроса2 =
		"ВЫБРАТЬ РАЗЛИЧНЫЕ
		|	ПоступлениеОтПереработчикаТовары.Ссылка КАК Ссылка,
		|	ПоступлениеОтПереработчикаТовары.Склад КАК Склад,
		|	ПоступлениеОтПереработчикаТовары.Ссылка.Номер КАК Номер,
		|	ПоступлениеОтПереработчикаТовары.Ссылка.Дата КАК ДатаДок,
		|	ПоступлениеОтПереработчикаТовары.Ссылка.НомерВходящегоДокумента КАК НомерПервичногоДокумента,
		|	ПоступлениеОтПереработчикаТовары.Ссылка.ДатаВходящегоДокумента КАК ДатаПервичногоДокумента,
		|	ПоступлениеОтПереработчикаТовары.Ссылка.Контрагент КАК Контрагент,
		|	ПоступлениеОтПереработчикаТовары.Ссылка.Проведен КАК Проведен,
		|	ПоступлениеОтПереработчикаТовары.Ссылка.ПометкаУдаления КАК ПометкаУдаления,
		|	ВЫБОР
		|		КОГДА ПоступлениеОтПереработчикаТовары.Ссылка.Проведен
		|			ТОГДА 0
		|		КОГДА ПоступлениеОтПереработчикаТовары.Ссылка.ПометкаУдаления
		|			ТОГДА 1
		|		ИНАЧЕ 2
		|	КОНЕЦ КАК НестандартнаяКартинка
		|ИЗ
		|	Документ.ПоступлениеОтПереработчика.Товары КАК ПоступлениеОтПереработчикаТовары
		|ГДЕ
		|	ПоступлениеОтПереработчикаТовары.Ссылка.Проведен
		|	И (ПоступлениеОтПереработчикаТовары.Ссылка.Дата >= &НачалоПериода
		|			ИЛИ &НачалоПериода = ДАТАВРЕМЯ(1, 1, 1))
		|	И (ПоступлениеОтПереработчикаТовары.Ссылка.Дата <= &КонецПериода
		|			ИЛИ &КонецПериода = ДАТАВРЕМЯ(1, 1, 1))
		|
		|ОБЪЕДИНИТЬ ВСЕ
		|
		|ВЫБРАТЬ РАЗЛИЧНЫЕ
		|	ПриобретениеТоваровУслугТовары.Ссылка,
		|	ПриобретениеТоваровУслугТовары.Склад,
		|	ПриобретениеТоваровУслугТовары.Ссылка.Номер,
		|	ПриобретениеТоваровУслугТовары.Ссылка.Дата,
		|	ПриобретениеТоваровУслугТовары.Ссылка.НомерВходящегоДокумента,
		|	ПриобретениеТоваровУслугТовары.Ссылка.ДатаВходящегоДокумента,
		|	ПриобретениеТоваровУслугТовары.Ссылка.Контрагент,
		|	ПриобретениеТоваровУслугТовары.Ссылка.Проведен,
		|	ПриобретениеТоваровУслугТовары.Ссылка.ПометкаУдаления,
		|	ВЫБОР
		|		КОГДА ПриобретениеТоваровУслугТовары.Ссылка.Проведен
		|			ТОГДА 0
		|		КОГДА ПриобретениеТоваровУслугТовары.Ссылка.ПометкаУдаления
		|			ТОГДА 1
		|		ИНАЧЕ 2
		|	КОНЕЦ
		|ИЗ
		|	Документ.ПриобретениеТоваровУслуг.Товары КАК ПриобретениеТоваровУслугТовары
		|ГДЕ
		|	ПриобретениеТоваровУслугТовары.Ссылка.Проведен
		|	И (ПриобретениеТоваровУслугТовары.Ссылка.Дата >= &НачалоПериода
		|			ИЛИ &НачалоПериода = ДАТАВРЕМЯ(1, 1, 1))
		|	И (ПриобретениеТоваровУслугТовары.Ссылка.Дата <= &КонецПериода
		|			ИЛИ &КонецПериода = ДАТАВРЕМЯ(1, 1, 1))
		|	И ПриобретениеТоваровУслугТовары.Ссылка.Чд_Статус = ЗНАЧЕНИЕ(Перечисление.Чд_СтатусыПриобретенияТоваровУслуг.Приобретено)";
		
		СвойстваСписка = ОбщегоНазначения.СтруктураСвойствДинамическогоСписка();
		СвойстваСписка.ТекстЗапроса = ТекстЗапроса2;
		ОбщегоНазначения.УстановитьСвойстваДинамическогоСписка(Элементы.ДокументыПрихода, СвойстваСписка);
	КонецЕсли;
	
КонецПроцедуры

// Заполнение документа АктОРасхожденияхПослеПриемки
&НаСервере
Функция СсылкаНаАкт(ПараметрыЗаполнения, Склад)
	
	ПараметрыЗаполнения = ПараметрыЗаполнения.ДокументОснование.Ссылка;
	
	ДокументОбъект = Документы.АктОРасхожденияхПослеПриемки.СоздатьДокумент();
	ДокументОбъект.Заполнить(ПараметрыЗаполнения);
	ДокументОбъект.Дата = ТекущаяДатаСеанса();
	ДокументОбъект.Чд_ОтборПоСкладу = Склад;
	ДокументОбъект.Контрагент = ПараметрыЗаполнения.Контрагент;
	ДокументОбъект.Партнер = ПараметрыЗаполнения.Партнер;
	ДокументОбъект.Договор = ПараметрыЗаполнения.Договор;
	ДокументОбъект.Чд_Контролер = Пользователи.ТекущийПользователь();
	ДокументОбъект.Чд_СтатусКачество = Перечисления.Чд_СтатусКачество.Подготовлено;
	ДокументОбъект.Чд_АктПоКачеству = Истина;
	
	ДокументОбъект.Товары.Очистить();
	
	// В документе устанавливается признак 1-е поступление
	МассивПервоеПоступление = Новый Массив(ПараметрыЗаполнения.Товары.Количество());
	МассивПервоеПоступление = ПервоеПоступлениеТовара(ПараметрыЗаполнения);
	
	// В документе, созданном на основании "ПоступлениеОтПереработчика" устанавливается цена "Залоговая стоимость"
	Если ТипЗнч(ПараметрыЗаполнения) = Тип("ДокументСсылка.ПоступлениеОтПереработчика") Тогда
		МассивЦенаЗалоговая = ПоступлениеТовараЦена(ПараметрыЗаполнения);
	КонецЕсли;
	
	Для Каждого СтрокаДанных Из ПараметрыЗаполнения.Товары Цикл
		
		Если СтрокаДанных.Склад = Склад Тогда
			ТекСтр = ДОкументОбъект.Товары.Добавить();
			ЗаполнитьЗначенияСвойств(ТекСтр, СтрокаДанных);
			ТекСтр.ДокументОснование = ПараметрыЗаполнения;
			ТекСтр.ЗаполненоПоОснованию = Истина;
			ТекСтр.Чд_ОбъемВыборки = 0;
			ТекСтр.Чд_ФактическиПроверено = 0;
			ТекСтр.Чд_КодДефекта = Неопределено;
			ТекСтр.Чд_КодРекомендации = Неопределено;
			ТекСтр.КоличествоПоДокументу = СтрокаДанных.Количество;
			ТекСтр.КоличествоУпаковокПоДокументу = СтрокаДанных.КоличествоУпаковок;
			ТекСтр.Чд_ОбъемВыборки = ТекСтр.Номенклатура.Чд_ПроцентВыборкиДляКачества * ТекСтр.КоличествоУпаковокПоДокументу / 100;
			ТекСтр.Чд_ДатаКонтроля = '00010101';
			ТекСтр.Чд_ВИзоляторБрака = Ложь;
			ТекСтр.Чд_КоличествоВИзоляторБрака = 0;
			ТекСтр.Чд_Уценка = Ложь;
			ТекСтр.Чд_ПроцентУценки = 0;
			ТекСтр.Чд_ПричинаУценки = Неопределено;
			ТекСтр.Чд_СогласованныйПроцентУценки = 0;
			ТекСтр.Чд_СогласованВозврат = Ложь;
			Если ТипЗнч(ПараметрыЗаполнения) = Тип("ДокументСсылка.ПоступлениеОтПереработчика") Тогда
				ТекСтр.Цена = МассивЦенаЗалоговая[СтрокаДанных.НомерСтроки - 1];
				ТекСтр.СтавкаНДС = Перечисления.СтавкиНДС.БезНДС;
			КонецЕсли;
			
			Если МассивПервоеПоступление[СтрокаДанных.НомерСтроки - 1] = 1 Тогда
				ТекСтр.Чд_ПервоеПоступление = Истина;
			Иначе
				ТекСтр.Чд_ПервоеПоступление = Ложь;
			КонецЕсли;
		КонецЕсли;
	КонецЦикла;
	
	ДокументОбъект.Записать();
	
	Возврат ДокументОбъект.Ссылка;
	
КонецФункции

// для документа "АктОРасхожденияхПослеПриемки", созданном на основании
// "ПоступлениеОтПереработчика" устанавливается цена "Залоговая стоимость"
&НаСервере
Функция ПоступлениеТовараЦена(Ссылка)
	
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
		|	ПоступлениеОтПереработчикаТовары.Ссылка КАК Ссылка,
		|	ПоступлениеОтПереработчикаТовары.Номенклатура КАК Номенклатура,
		|	ПоступлениеОтПереработчикаТовары.Характеристика КАК Характеристика,
		|	ЕСТЬNULL(ЦеныНоменклатурыСрезПоследних.Цена, 0) КАК Цена,
		|	ПоступлениеОтПереработчикаТовары.НомерСтроки КАК НомерСтроки
		|ИЗ
		|	Документ.ПоступлениеОтПереработчика.Товары КАК ПоступлениеОтПереработчикаТовары
		|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.ЦеныНоменклатуры.СрезПоследних(, ВидЦены.Наименование = ""Залоговая стоимость"") КАК ЦеныНоменклатурыСрезПоследних
		|		ПО ПоступлениеОтПереработчикаТовары.Номенклатура = ЦеныНоменклатурыСрезПоследних.Номенклатура
		|			И ПоступлениеОтПереработчикаТовары.Характеристика = ЦеныНоменклатурыСрезПоследних.Характеристика
		|ГДЕ
		|	ПоступлениеОтПереработчикаТовары.Ссылка = &Ссылка
		|
		|УПОРЯДОЧИТЬ ПО
		|	НомерСтроки";
	
	Запрос.УстановитьПараметр("Ссылка", Ссылка);
	
	РезультатЗапроса = Запрос.Выполнить();
	
	Выборка = РезультатЗапроса.Выбрать();
	МассивЦена = Новый Массив(Выборка.Количество());
	Пока Выборка.Следующий() Цикл
		МассивЦена.Вставить(Выборка.НомерСтроки - 1, Выборка.Цена);
	КонецЦикла;
	
	Возврат МассивЦена;
	
КонецФункции

// анализируется количество приходов номенклатура - характеристика от определенного поставщика
&НаСервере
Функция ПервоеПоступлениеТовара(Ссылка)
	
	Запрос = Новый Запрос;
	Запрос.Текст =
		"ВЫБРАТЬ
		|	ПриобретениеТоваровУслугТовары.Ссылка КАК Ссылка,
		|	ПриобретениеТоваровУслугТовары.Номенклатура КАК Номенклатура,
		|	ПриобретениеТоваровУслугТовары.Характеристика КАК Характеристика,
		|	ПриобретениеТоваровУслугТовары.Ссылка.Контрагент КАК Контрагент,
		|	ПриобретениеТоваровУслугТовары.НомерСтроки КАК НомерСтроки
		|ПОМЕСТИТЬ ВТ_Товары
		|ИЗ
		|	Документ.ПриобретениеТоваровУслуг.Товары КАК ПриобретениеТоваровУслугТовары
		|ГДЕ
		|	ПриобретениеТоваровУслугТовары.Ссылка = &Ссылка
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	КОЛИЧЕСТВО(РАЗЛИЧНЫЕ СебестоимостьТоваров.АналитикаУчетаНоменклатуры.Номенклатура) КАК АналитикаУчетаНоменклатурыНоменклатура,
		|	КОЛИЧЕСТВО(РАЗЛИЧНЫЕ СебестоимостьТоваров.АналитикаУчетаНоменклатуры.Характеристика) КАК АналитикаУчетаНоменклатурыХарактеристика,
		|	СебестоимостьТоваров.Партия.Контрагент КАК ПартияКонтрагент,
		|	ВТ_Товары.НомерСтроки КАК НомерСтроки
		|ИЗ
		|	ВТ_Товары КАК ВТ_Товары
		|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ РегистрНакопления.СебестоимостьТоваров КАК СебестоимостьТоваров
		|		ПО ВТ_Товары.Контрагент = СебестоимостьТоваров.Партия.Контрагент
		|			И ВТ_Товары.Номенклатура = СебестоимостьТоваров.АналитикаУчетаНоменклатуры.Номенклатура
		|			И ВТ_Товары.Характеристика = СебестоимостьТоваров.АналитикаУчетаНоменклатуры.Характеристика
		|
		|СГРУППИРОВАТЬ ПО
		|	СебестоимостьТоваров.Партия.Контрагент,
		|	ВТ_Товары.НомерСтроки
		|
		|УПОРЯДОЧИТЬ ПО
		|	НомерСтроки
		|АВТОУПОРЯДОЧИВАНИЕ";
	
	Запрос.УстановитьПараметр("Ссылка", Ссылка);
	Если ТипЗнч(Ссылка) = Тип("ДокументСсылка.ПоступлениеОтПереработчика") Тогда
		Запрос.Текст = СтрЗаменить(Запрос.Текст, "ПриобретениеТоваровУслуг", "ПоступлениеОтПереработчика");
	КонецЕсли;
	
	РезультатЗапроса = Запрос.Выполнить();
	Выборка = РезультатЗапроса.Выбрать();
	МассивН = Новый Массив;
	Пока Выборка.Следующий() Цикл
		МассивН.Добавить(Выборка.АналитикаУчетаНоменклатурыХарактеристика);
	КонецЦикла;
	
	Возврат МассивН;
	
КонецФункции

&НаКлиенте
Процедура УстановитьИнтервалЗавершение(ВыбранноеЗначение, ДополнительныеПараметры) Экспорт
	
	Если ВыбранноеЗначение = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	Период = ВыбранноеЗначение;
	УстановитьОтборПоПериоду();
	
КонецПроцедуры

&НаКлиенте
Процедура УстановитьОтборПоПериоду()
	
	ДокументыПрихода.Параметры.УстановитьЗначениеПараметра("НачалоПериода", 
		?(ЗначениеЗаполнено(Период.ДатаНачала), Период.ДатаНачала, Дата(1, 1, 1)));
	ДокументыПрихода.Параметры.УстановитьЗначениеПараметра("КонецПериода",
		?(ЗначениеЗаполнено(Период.ДатаОкончания), Период.ДатаОкончания, Дата(1, 1, 1)));

КонецПроцедуры

&НаКлиенте
Процедура ФильтрАктыПриИзменении(Элемент)
	
	МассивОбъектов = Элементы.ДокументыПрихода.ВыделенныеСтроки;
	СсылкаНаДокумент = Новый Массив;
	Для Каждого ТекДанные Из МассивОбъектов Цикл
		ТекДокументОснование = Элементы.ДокументыПрихода.ДанныеСтроки(ТекДанные);
		СсылкаНаДокумент.Добавить(ТекДокументОснование.Ссылка);
	КонецЦикла;
	
	УстановитьОтборыВЗависимыхСписках(СсылкаНаДокумент);
	
КонецПроцедуры

#КонецОбласти
