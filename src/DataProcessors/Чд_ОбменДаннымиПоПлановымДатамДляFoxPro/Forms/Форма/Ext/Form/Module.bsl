﻿#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПередЗакрытииНаСервере()
	
	Структура = Новый Структура("Путь1", Путь1);
	Константа = Константы.Чд_ПутиВыгрузкиВDBFПоПлановымДатам.СоздатьМенеджерЗначения();
	Константа.Значение = Новый ХранилищеЗначения(Структура);
	Константа.Записать();
	
КонецПроцедуры

&НаКлиенте
Процедура ПередЗакрытием(Отказ, ЗавершениеРаботы, ТекстПредупреждения, СтандартнаяОбработка)
	
	Если НЕ Отказ И НЕ ЗавершениеРаботы И СтандартнаяОбработка Тогда
		ПередЗакрытииНаСервере();
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ПриОткрытииНаСервере()
	
	ТЗпутей = Новый ТаблицаЗначений;
	ТЗпутей = Константы.Чд_ПутиВыгрузкиВDBFПоПлановымДатам.Получить().Получить();
	Если ЗначениеЗаполнено(ТЗпутей) Тогда
		ЭтотОбъект.Путь1 = ТЗпутей.Путь1;
	Иначе
		Структура = Новый Структура("Путь1", Путь1);
		
		Константа = Константы.Чд_ПутиВыгрузкиВDBFПоПлановымДатам.СоздатьМенеджерЗначения();
		Константа.Значение = Новый ХранилищеЗначения(Структура);
		Константа.Записать();
		ТЗпутей = Константы.Чд_ПутиВыгрузкиВDBFПоПлановымДатам.Получить().Получить();
		ЭтотОбъект.Путь1 = ТЗпутей.Путь1;
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	ПриОткрытииНаСервере();
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура Выгрузить(Команда)
	ВыгрузитьНаСервере();
КонецПроцедуры

&НаСервере
Процедура ВыгрузитьНаСервере()
	
	ДокументОбъект = РеквизитФормыВЗначение("Объект");
	НаборПутей = Новый Массив;
	Если НЕ ПустаяСтрока(Путь1) Тогда
		НаборПутей.Добавить(Путь1);
	КонецЕсли;
	
	Если НаборПутей.Количество() > 0 Тогда
		ДокументОбъект.ВыгрузитьДокумент(НаборПутей);
	Иначе
		Сообщить("Выберете папку для сохранения!");
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ВыборПутиДляСохраненияФаилов

&НаКлиенте
Процедура ПутьНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	ДиалогВыбораПапки = Новый ДиалогВыбораФайла(РежимДиалогаВыбораФайла.ВыборКаталога);
	Если НЕ ПустаяСтрока(Путь1) Тогда
		ДиалогВыбораПапки.Каталог = Путь1;
	КонецЕсли;
	Если ДиалогВыбораПапки.Выбрать() Тогда
		Путь1 = ДиалогВыбораПапки.Каталог;
		ОчиститьСообщения();
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ПутьОткрытие(Элемент, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	ЗапуститьПриложение(Путь1);
	
КонецПроцедуры

#КонецОбласти