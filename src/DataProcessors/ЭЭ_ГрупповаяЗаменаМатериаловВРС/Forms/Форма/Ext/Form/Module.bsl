﻿#Область ОбработчикиСобытийФормы

&НаКлиенте
Процедура СерияНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	Если Не ЗначениеЗаполнено(Объект.Коллекция) Тогда 
		Возврат;
	КонецЕсли;
	СтандартнаяОбработка = Ложь;
	
	НастройкиКомпоновки = Новый НастройкиКомпоновкиДанных;
	
	ГруппаОтбора = НастройкиКомпоновки.Отбор.Элементы.Добавить(Тип("ГруппаЭлементовОтбораКомпоновкиДанных"));
	ГруппаОтбора.ТипГруппы = ТипГруппыЭлементовОтбораКомпоновкиДанных.ГруппаИ;
	
	ЭлементОтбора = ГруппаОтбора.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных")); 
	ЭлементОтбора.ЛевоеЗначение  = Новый ПолеКомпоновкиДанных("Ссылка");
	ЭлементОтбора.ВидСравнения   = ВидСравненияКомпоновкиДанных.ВИерархии;
	ЭлементОтбора.Использование  = Истина;
	ЭлементОтбора.ПравоеЗначение = Объект.Коллекция;
	
	ПараметрыФормы = Новый Структура;
	ПараметрыФормы.Вставить("ФиксированныеНастройки", НастройкиКомпоновки);
	
	ОбработкаВыбора = Новый ОписаниеОповещения("ПриЗакрытииФормыВыбора", ЭтотОбъект, "Подбор");
	
	ОткрытьФорму("Справочник.КоллекцииНоменклатуры.ФормаВыбора", ПараметрыФормы,
																	ЭтотОбъект, , , , ОбработкаВыбора);
	
КонецПроцедуры  

&НаКлиенте
Процедура ПриЗакрытииФормыВыбора(Значение, ДопПараметры) Экспорт
	
	Если Значение = Неопределено Тогда
        Возврат;
    КонецЕсли;
     
    Объект.Серия = Значение; 
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура Отбор(Команда)
	
	ОчиститьСообщения();   
	Если Не ЗначениеЗаполнено(Объект.ЗаменяемыйМатериалНоменклатура) 
		Или Не ЗначениеЗаполнено(Объект.ДатаНачалаЗамены) Тогда 
		Текст = "Поле Заменяемый материал и Дата начала замены должны быть заполнены";
		ОбщегоНазначенияКлиент.СообщитьПользователю(Текст);	
	Иначе
		 ЗаполнитьТЧОсновное();
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ВыбратьВсеОсновное(Команда)
	
	ОбработатьТЧ(Объект.Основное, Истина);
	
КонецПроцедуры

&НаКлиенте
Процедура СнятьВыделениеОсновное(Команда)
	
	ОбработатьТЧ(Объект.Основное, Ложь);
	
КонецПроцедуры

&НаКлиенте
Процедура ВыбратьВсеСозданиеРС(Команда)
	
	ОбработатьТЧ(Объект.СозданныеРС, Истина);
	
КонецПроцедуры

&НаКлиенте
Процедура СнятьВыделениеСозданиеРС(Команда)
	
	ОбработатьТЧ(Объект.СозданныеРС, Ложь);
	
КонецПроцедуры

&НаКлиенте
Процедура КомандаВыполнить(Команда)

	ОчиститьСообщения();
	Отказ = Ложь;
	МассивОшибок = Новый Массив;
	Если Не ЗначениеЗаполнено(Объект.ЗаменяемыйМатериалНоменклатура) Тогда 
		МассивОшибок.Добавить("Необходимо	заполнить 'Заменяемый материал'");
		Отказ = Истина;
	КонецЕсли;
	
	Если Не ЗначениеЗаполнено(Объект.ЗаменяющийМатериалНоменклатура) Тогда 
		МассивОшибок.Добавить("Необходимо	заполнить 'Заменяющий материал'");
       	Отказ = Истина;
	КонецЕсли;
	
	Если Не ЗначениеЗаполнено(Объект.ЗаменяемыйМатериалХарактеристика) 
			И Не ЗначениеЗаполнено(Объект.ЗаменяющийМатериалХарактеристика) Тогда  
			Если ПроверитьКоличествоСвойствУХарактеристик(Объект.ЗаменяемыйМатериалНоменклатура, 
															Объект.ЗаменяющийМатериалНоменклатура) Тогда 
				МассивОшибок.Добавить("Различное количество свойств в характеристиках");
				Отказ = Истина;
		КонецЕсли;
	КонецЕсли;
	
	Если Не ЗначениеЗаполнено(Объект.ДатаНачалаЗамены) Тогда 
		МассивОшибок.Добавить("Необходимо	заполнить 'Начало замены'"); 
		Отказ = Истина;
	КонецЕсли;
	
	Если Объект.Основное.Количество() = 0 Тогда 
		МассивОшибок.Добавить("Нет элементов в табличной части");
		Отказ = Истина;
	КонецЕсли; 
	
	Если Отказ Тогда 
		Текст = СтрСоединить(МассивОшибок, ",  ");
		ОбщегоНазначенияКлиент.СообщитьПользователю(Текст);
		Возврат;
	КонецЕсли;
		
	ЗаполнитьТЧПодробностиЗамен();
	 
КонецПроцедуры

&НаКлиенте
Процедура СоздатьРС(Команда)
	
	ОчиститьСообщения();
	Оповещение = Новый ОписаниеОповещения("ПослеОтветаНаВопрос", ЭтотОбъект);
	ДатаЗамены = Формат(Объект.ДатаНачалаЗамены, "ДФ=dd.MM.yyyy");
	Текст = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку("По выбранным строкам на дату %1 
																	|будут закрыты старые РС и созданы новые. Продолжить?",
																	 ДатаЗамены);
	ПоказатьВопрос(Оповещение, Текст, РежимДиалогаВопрос.ДаНет, , КодВозвратаДиалога.Нет);
	
КонецПроцедуры 

&НаКлиенте
Процедура ПослеОтветаНаВопрос(РезультатВопроса, ДополнительныеПараметры) Экспорт  
	
	Если РезультатВопроса = КодВозвратаДиалога.Да Тогда 	
		ЗакрытьСтаруюИСоздатьНовуюРС();
	КонецЕсли; 
	
КонецПроцедуры

&НаКлиенте
Процедура Назад(Команда)
	
	Объект.ПодробностиЗамен.Очистить();
	ПерейтиНаНовуюВкладку("Основное");
	
 КонецПроцедуры

&НаКлиенте
Процедура ПерейтиКОтбору(Команда)
	
	Объект.Основное.Очистить();
	Объект.СозданныеРС.Очистить();
	ПерейтиНаНовуюВкладку("Основное");
	
КонецПроцедуры

&НаСервере
Процедура ПерейтиНаНовуюВкладку(НаименованиеЭлемента)
	
	ТекущийЭлемент = Элементы[НаименованиеЭлемента];
	
КонецПроцедуры 

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаКлиенте
Процедура ОбработатьТЧ(Таблица, ЗначениеФлага)
	
	Для Каждого СтрокаТЧ Из Таблица Цикл 
		СтрокаТЧ.ВыбраннаяСтрока = ЗначениеФлага;
	КонецЦикла;
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьТЧОсновное()
	
	СтруктураЗаполнения = ПолучитьСтруктуруЗаполнения();
	ЗаполнитьЗначенияСвойств(СтруктураЗаполнения, Объект);
	ДанныеПоРС = ПолучитьДанныеПоРС(СтруктураЗаполнения);
	
	Если ДанныеПоРС.Количество() = 0 Тогда 
		Текст = "Не обнаружено данных по указанным критериям отбора";
		ОбщегоНазначения.СообщитьПользователю(Текст);
		Объект.Основное.Очистить();
		Возврат;
	Иначе
		Объект.Основное.Загрузить(ДанныеПоРС);
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Функция ПолучитьСтруктуруЗаполнения()
	
	Структура = Новый Структура;
	Структура.Вставить("Коллекция");
	Структура.Вставить("Серия");
	Структура.Вставить("ЦветМодели");
	Структура.Вставить("ДатаНачалаЗамены");
	Структура.Вставить("ЗаменяемыйМатериалНоменклатура");
	Структура.Вставить("ЗаменяемыйМатериалХарактеристика");
	Возврат Структура;
	
КонецФункции

&НаСервере
Функция ПолучитьДанныеПоРС(СтруктураЗаполнения)
	
	Перем ДанныеРС;
	
	Запрос = Новый Запрос;
	Запрос.Текст = ПолучитьЗапросДанныеПоРС(); 
		
	Запрос.УстановитьПараметр("Дата", СтруктураЗаполнения.ДатаНачалаЗамены);
	Запрос.УстановитьПараметр("Номенклатура", СтруктураЗаполнения.ЗаменяемыйМатериалНоменклатура);

	ИсточникДанных = "";
	
	Если ЗначениеЗаполнено(СтруктураЗаполнения.ЗаменяемыйМатериалХарактеристика) Тогда 		
		Запрос.Текст = СтрЗаменить(Запрос.Текст, "&Характеристика", 
													"И РесурсныеСпецификацииМатериалыИУслуги.Характеристика = &Характеристика"); 
		Запрос.УстановитьПараметр("Характеристика", СтруктураЗаполнения.ЗаменяемыйМатериалХарактеристика);	
	Иначе
		Запрос.Текст = СтрЗаменить(Запрос.Текст, "&Характеристика", "");	
	КонецЕсли;
	
	Если ЗначениеЗаполнено(СтруктураЗаполнения.Коллекция) Или ЗначениеЗаполнено(СтруктураЗаполнения.Серия) Тогда 	
		
		КоллекцияМилавицы = ?(ЗначениеЗаполнено(СтруктураЗаполнения.Коллекция), 
												"И (НоменклатураСправочник.ЭЭ_КоллекцияМилавицы = &ЭЭ_КоллекцияМилавицы)", "");
		КоллекцияНоменклатуры = ?(ЗначениеЗаполнено(СтруктураЗаполнения.Серия), 
												"И (НоменклатураСправочник.КоллекцияНоменклатуры = &КоллекцияНоменклатуры)", "");
		
		ИсточникДанных = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку("ВНУТРЕННЕЕ СОЕДИНЕНИЕ Справочник.Номенклатура КАК НоменклатураСправочник
																				|		ПО ВТ_Спецификации.Номенклатура = НоменклатураСправочник.Ссылка %1 %2",
																				КоллекцияМилавицы,
																				КоллекцияНоменклатуры); 	
				
		Если ЗначениеЗаполнено(СтруктураЗаполнения.Коллекция) Тогда 
			Запрос.УстановитьПараметр("ЭЭ_КоллекцияМилавицы", СтруктураЗаполнения.Коллекция);	
		КонецЕсли;
		
		Если ЗначениеЗаполнено(СтруктураЗаполнения.Серия) Тогда 
			Запрос.УстановитьПараметр("КоллекцияНоменклатуры", СтруктураЗаполнения.Серия);
		КонецЕсли;
			
	КонецЕсли;
	
	Если ЗначениеЗаполнено(СтруктураЗаполнения.ЦветМодели) Тогда 
		
		ИсточникДанных = ИсточникДанных + "ВНУТРЕННЕЕ СОЕДИНЕНИЕ Справочник.Номенклатура.ДополнительныеРеквизиты КАК НоменклатураДополнительныеРеквизиты
											|		ПО ВТ_Спецификации.Номенклатура = НоменклатураДополнительныеРеквизиты.Ссылка
											|			И (НоменклатураДополнительныеРеквизиты.Свойство = &Цвет)
											|			И (НоменклатураДополнительныеРеквизиты.Значение = &ЗначениеЦвета)";
		СвойствоЦВЕТ = ПланыВидовХарактеристик.ДополнительныеРеквизитыИСведения.НайтиПоРеквизиту("Имя", 
																								 "Чд_Цвет(справочник)");
		Запрос.УстановитьПараметр("Цвет", СвойствоЦВЕТ);
		Запрос.УстановитьПараметр("ЗначениеЦвета", СтруктураЗаполнения.ЦветМодели);
		
	КонецЕсли; 

	Запрос.Текст = СтрЗаменить(Запрос.Текст, "&ИсточникДанных", ИсточникДанных); 
	
	РезультатЗапроса = Запрос.Выполнить();
	
	ДанныеРС = РезультатЗапроса.Выгрузить();
	
	Возврат  ДанныеРС; 
	
КонецФункции

&НаСервере
Функция ПолучитьЗапросДанныеПоРС() 
	
	Возврат "ВЫБРАТЬ РАЗРЕШЕННЫЕ РАЗЛИЧНЫЕ
	        |	РесурсныеСпецификацииМатериалыИУслуги.Ссылка КАК Спецификации,
	        |	РесурсныеСпецификации.Статус КАК Статус,
	        |	РесурсныеСпецификации.НачалоДействия КАК НачалоДействия,
	        |	РесурсныеСпецификации.КонецДействия КАК КонецДействия,
	        |	РесурсныеСпецификацииВыходныеИзделия.Номенклатура КАК Номенклатура
	        |ПОМЕСТИТЬ ВТ_Спецификации
	        |ИЗ
	        |	Справочник.РесурсныеСпецификации.МатериалыИУслуги КАК РесурсныеСпецификацииМатериалыИУслуги
	        |		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.РесурсныеСпецификации КАК РесурсныеСпецификации
	        |		ПО РесурсныеСпецификацииМатериалыИУслуги.Ссылка = РесурсныеСпецификации.Ссылка
	        |		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.РесурсныеСпецификации.ВыходныеИзделия КАК РесурсныеСпецификацииВыходныеИзделия
	        |		ПО (РесурсныеСпецификацииВыходныеИзделия.Ссылка = РесурсныеСпецификацииМатериалыИУслуги.Ссылка)
	        |ГДЕ
	        |	РесурсныеСпецификацииМатериалыИУслуги.Номенклатура = &Номенклатура
	        |	И НЕ РесурсныеСпецификации.ПометкаУдаления
	        |	И РесурсныеСпецификации.Статус В (ЗНАЧЕНИЕ(Перечисление.СтатусыСпецификаций.ВРазработке), ЗНАЧЕНИЕ(Перечисление.СтатусыСпецификаций.Действует))
			|	&Характеристика
	        |
	        |СГРУППИРОВАТЬ ПО
	        |	РесурсныеСпецификацииМатериалыИУслуги.Ссылка,
	        |	РесурсныеСпецификации.Статус,
	        |	РесурсныеСпецификации.НачалоДействия,
	        |	РесурсныеСпецификации.КонецДействия,
	        |	РесурсныеСпецификацииВыходныеИзделия.Номенклатура
	        |;
	        |
	        |////////////////////////////////////////////////////////////////////////////////
	        |ВЫБРАТЬ
	        |	ВТ_Спецификации.Спецификации КАК Спецификации,
	        |	ВТ_Спецификации.Статус КАК Статус,
	        |	ВТ_Спецификации.НачалоДействия КАК НачалоДействия,
	        |	ВТ_Спецификации.КонецДействия КАК КонецДействия,
	        |	ВТ_Спецификации.Номенклатура КАК Номенклатура
	        |ПОМЕСТИТЬ ВТ_СпецификацииДата
	        |ИЗ
	        |	ВТ_Спецификации КАК ВТ_Спецификации
	        |ГДЕ
	        |	ВТ_Спецификации.КонецДействия = ДАТАВРЕМЯ(1, 1, 1, 0, 0, 0)
	        |
	        |ОБЪЕДИНИТЬ ВСЕ
	        |
	        |ВЫБРАТЬ
	        |	ВТ_Спецификации.Спецификации,
	        |	ВТ_Спецификации.Статус,
	        |	ВТ_Спецификации.НачалоДействия,
	        |	ВТ_Спецификации.КонецДействия,
	        |	ВТ_Спецификации.Номенклатура
	        |ИЗ
	        |	ВТ_Спецификации КАК ВТ_Спецификации
	        |ГДЕ
	        |	ВТ_Спецификации.КонецДействия >= &Дата
	        |;
	        |
	        |////////////////////////////////////////////////////////////////////////////////
	        |ВЫБРАТЬ
	        |	ВТ_СпецификацииДата.Спецификации КАК Спецификации,
	        |	ВТ_СпецификацииДата.Статус КАК Статус,
	        |	ВТ_СпецификацииДата.НачалоДействия КАК НачалоДействия,
	        |	ВТ_СпецификацииДата.КонецДействия КАК КонецДействия,
	        |	ВТ_СпецификацииДата.Номенклатура КАК Номенклатура
	        |ИЗ
	        |	ВТ_СпецификацииДата КАК ВТ_СпецификацииДата
			|		&ИсточникДанных	
	        |
	        |СГРУППИРОВАТЬ ПО
	        |	ВТ_СпецификацииДата.КонецДействия,
	        |	ВТ_СпецификацииДата.Спецификации,
	        |	ВТ_СпецификацииДата.Номенклатура,
	        |	ВТ_СпецификацииДата.НачалоДействия,
	        |	ВТ_СпецификацииДата.Статус";
	
КонецФункции

&НаСервере
Процедура ЗаполнитьТЧПодробностиЗамен() 
	
	СтруктураЗаполнения = ПолучитьСтруктуруЗаполненияДляГрупповойЗаменыМатериаловВРС();
	
	ЗаполнитьЗначенияСвойств(СтруктураЗаполнения, Объект);
	
	Запрос = Новый Запрос;
	МВТ = Новый МенеджерВременныхТаблиц;
	Запрос.МенеджерВременныхТаблиц = МВТ;
	Запрос.Текст = ТекстЗапросаВыбранныхРС();
	Запрос.УстановитьПараметр("ТЗ", СтруктураЗаполнения.Основное.Выгрузить());
	РезультатЗапроса = Запрос.Выполнить();
	
	Если РезультатЗапроса.Пустой() Тогда
		Текст = "Нет выбранных элементов в табличной части";
		ОбщегоНазначения.СообщитьПользователю(Текст);
		Возврат;
	КонецЕсли;
	
	ПодобратьИЗаполнитьХарактеристикиДляТЧПодробностиЗамен(СтруктураЗаполнения, МВТ);
	
	ПерейтиНаНовуюВкладку("ПодробностиЗамен");
	
КонецПроцедуры

&НаСервере
Процедура ПодобратьИЗаполнитьХарактеристикиДляТЧПодробностиЗамен(СтруктураЗаполнения, МВТ)

	Запрос = Новый Запрос;
	Запрос.МенеджерВременныхТаблиц = МВТ;
	Если ЗначениеЗаполнено(СтруктураЗаполнения.ЗаменяющийМатериалХарактеристика) Тогда 
		Запрос.Текст = ЗаполнитьИзЗаменяющийМатериалХарактеристика(); 
		Запрос.УстановитьПараметр("ЗаменяющийМатериалХарактеристика", СтруктураЗаполнения.ЗаменяющийМатериалХарактеристика);
	Иначе
		Запрос.Текст = СопоставитьХарактеристикиНоменклатуры(); 
		СвойствоОписание = ПланыВидовХарактеристик.ДополнительныеРеквизитыИСведения.НайтиПоРеквизиту("Имя", 
																									 "Чд_ОписаниеХарактеристики");
		Запрос.УстановитьПараметр("Свойство", СвойствоОписание);		
	КонецЕсли;
    Запрос.УстановитьПараметр("Номенклатура", СтруктураЗаполнения.ЗаменяемыйМатериалНоменклатура);
	
	Запрос.УстановитьПараметр("ЗаменяющийМатериалНоменклатура", СтруктураЗаполнения.ЗаменяющийМатериалНоменклатура);
	
	Если ЗначениеЗаполнено(СтруктураЗаполнения.ЗаменяемыйМатериалХарактеристика) Тогда 
		Запрос.Текст = СтрЗаменить(Запрос.Текст, "&Характеристика", 
									"И (РесурсныеСпецификацииМатериалыИУслуги.Характеристика = &Характеристика)"); 
		Запрос.УстановитьПараметр("Характеристика", СтруктураЗаполнения.ЗаменяемыйМатериалХарактеристика);
	Иначе 
		Запрос.Текст = СтрЗаменить(Запрос.Текст, "&Характеристика", "");	
	КонецЕсли;
	
	РезультатЗапроса = Запрос.Выполнить();
	
	Объект.ПодробностиЗамен.Очистить();
	Объект.ПодробностиЗамен.Загрузить(РезультатЗапроса.Выгрузить());

КонецПроцедуры

&НаСервере
Функция ЗаполнитьИзЗаменяющийМатериалХарактеристика()
	
	Возврат "ВЫБРАТЬ
	        |	РесурсныеСпецификации.НаименованиеРС КАК НаименованиеРС,
	        |	РесурсныеСпецификацииМатериалыИУслуги.Номенклатура КАК ЗаменяемыйМатериалНоменклатура,
	        |	РесурсныеСпецификацииМатериалыИУслуги.Характеристика КАК ЗаменяемыйМатериалХарактеристика,
	        |	&ЗаменяющийМатериалНоменклатура КАК ЗаменяющийМатериалНоменклатура,
	        |	&ЗаменяющийМатериалХарактеристика КАК ЗаменяющийМатериалХарактеристика,
	        |	ИСТИНА КАК ВыбраннаяСтрока
	        |ИЗ
	        |	ВТ_ОтобранныеРС КАК РесурсныеСпецификации
	        |		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Справочник.РесурсныеСпецификации.МатериалыИУслуги КАК РесурсныеСпецификацииМатериалыИУслуги
	        |		ПО РесурсныеСпецификации.НаименованиеРС = РесурсныеСпецификацииМатериалыИУслуги.Ссылка
	        |			И (РесурсныеСпецификацииМатериалыИУслуги.Номенклатура = &Номенклатура)
			|			&Характеристика
	        |
	        |СГРУППИРОВАТЬ ПО
	        |	РесурсныеСпецификации.НаименованиеРС,
	        |	РесурсныеСпецификацииМатериалыИУслуги.Номенклатура,
	        |	РесурсныеСпецификацииМатериалыИУслуги.Характеристика";
	
КонецФункции

&НаСервере
Функция СопоставитьХарактеристикиНоменклатуры()
	
	Возврат "ВЫБРАТЬ
		|	РесурсныеСпецификации.НаименованиеРС КАК РесурснаяСпецификация,
		|	РесурсныеСпецификацииМатериалыИУслуги.Номенклатура КАК ЗаменяемыйМатериалНоменклатура,
		|	РесурсныеСпецификацииМатериалыИУслуги.Характеристика КАК ЗаменяемыйМатериалХарактеристика
		|ПОМЕСТИТЬ ВТ_ДанныеРС
		|ИЗ
		|	ВТ_ОтобранныеРС КАК РесурсныеСпецификации
		|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Справочник.РесурсныеСпецификации.МатериалыИУслуги КАК РесурсныеСпецификацииМатериалыИУслуги
		|		ПО РесурсныеСпецификации.НаименованиеРС = РесурсныеСпецификацииМатериалыИУслуги.Ссылка
		|			И (РесурсныеСпецификацииМатериалыИУслуги.Номенклатура = &Номенклатура)
		|			&Характеристика
		|
		|СГРУППИРОВАТЬ ПО
		|	РесурсныеСпецификации.НаименованиеРС,
		|	РесурсныеСпецификацииМатериалыИУслуги.Номенклатура,
		|	РесурсныеСпецификацииМатериалыИУслуги.Характеристика
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	ВТ_ДанныеРС.РесурснаяСпецификация КАК РесурснаяСпецификация,
		|	ВТ_ДанныеРС.ЗаменяемыйМатериалНоменклатура КАК ЗаменяемыйМатериалНоменклатура,
		|	ВТ_ДанныеРС.ЗаменяемыйМатериалХарактеристика КАК ЗаменяемыйМатериалХарактеристика,
		|	КОЛИЧЕСТВО(РАЗЛИЧНЫЕ ХарактеристикиНоменклатурыДополнительныеРеквизиты.Свойство) КАК КоличествоСвойств
		|ПОМЕСТИТЬ ВТ_КоличествоСвойстВХарактеристикахРС
		|ИЗ
		|	ВТ_ДанныеРС КАК ВТ_ДанныеРС
		|		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.ХарактеристикиНоменклатуры.ДополнительныеРеквизиты КАК ХарактеристикиНоменклатурыДополнительныеРеквизиты
		|		ПО ВТ_ДанныеРС.ЗаменяемыйМатериалХарактеристика = ХарактеристикиНоменклатурыДополнительныеРеквизиты.Ссылка
		|			И (НЕ ХарактеристикиНоменклатурыДополнительныеРеквизиты.Свойство = &Свойство)
		|
		|СГРУППИРОВАТЬ ПО
		|	ВТ_ДанныеРС.РесурснаяСпецификация,
		|	ВТ_ДанныеРС.ЗаменяемыйМатериалНоменклатура,
		|	ВТ_ДанныеРС.ЗаменяемыйМатериалХарактеристика
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	ХарактеристикиНоменклатуры.Ссылка КАК ЗаменяющийМатериалХарактеристика,
		|	ХарактеристикиНоменклатуры.Владелец КАК ЗаменяющийМатериалНоменклатура,
		|	КОЛИЧЕСТВО(ХарактеристикиНоменклатурыДополнительныеРеквизиты.Свойство) КАК КоличествоСвойств
		|ПОМЕСТИТЬ ВТ_КоличествоСвойствУЗамещающейНоменклатуры
		|ИЗ
		|	Справочник.ХарактеристикиНоменклатуры КАК ХарактеристикиНоменклатуры
		|		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.ХарактеристикиНоменклатуры.ДополнительныеРеквизиты КАК ХарактеристикиНоменклатурыДополнительныеРеквизиты
		|		ПО ХарактеристикиНоменклатуры.Ссылка = ХарактеристикиНоменклатурыДополнительныеРеквизиты.Ссылка
		|			И (НЕ ХарактеристикиНоменклатурыДополнительныеРеквизиты.Свойство = &Свойство)
		|ГДЕ
		|	ХарактеристикиНоменклатуры.Владелец = &ЗаменяющийМатериалНоменклатура
		|	И НЕ ХарактеристикиНоменклатуры.ПометкаУдаления
		|
		|СГРУППИРОВАТЬ ПО
		|	ХарактеристикиНоменклатуры.Ссылка,
		|	ХарактеристикиНоменклатуры.Владелец
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	ВТ_КоличествоСвойствУЗамещающейНоменклатуры.ЗаменяющийМатериалХарактеристика КАК ЗаменяющийМатериалХарактеристика,
		|	ВТ_КоличествоСвойствУЗамещающейНоменклатуры.ЗаменяющийМатериалНоменклатура КАК ЗаменяющийМатериалНоменклатура,
		|	ВТ_КоличествоСвойствУЗамещающейНоменклатуры.КоличествоСвойств КАК КоличествоСвойств
		|ПОМЕСТИТЬ ВТ_ОтборПоКоличествуСвойств
		|ИЗ
		|	ВТ_КоличествоСвойствУЗамещающейНоменклатуры КАК ВТ_КоличествоСвойствУЗамещающейНоменклатуры
		|ГДЕ
		|	ВТ_КоличествоСвойствУЗамещающейНоменклатуры.КоличествоСвойств В
		|			(ВЫБРАТЬ
		|				Т.КоличествоСвойств
		|			ИЗ
		|				ВТ_КоличествоСвойстВХарактеристикахРС КАК Т)
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	ВТ_КоличествоСвойстВХарактеристикахРС.РесурснаяСпецификация КАК РесурснаяСпецификация,
		|	ВТ_КоличествоСвойстВХарактеристикахРС.ЗаменяемыйМатериалНоменклатура КАК ЗаменяемыйМатериалНоменклатура,
		|	ВТ_КоличествоСвойстВХарактеристикахРС.ЗаменяемыйМатериалХарактеристика КАК ЗаменяемыйМатериалХарактеристика,
		|	ВТ_КоличествоСвойстВХарактеристикахРС.КоличествоСвойств КАК КоличествоСвойств,
		|	ХарактеристикиНоменклатурыДополнительныеРеквизиты.Свойство КАК Свойство,
		|	ХарактеристикиНоменклатурыДополнительныеРеквизиты.Значение КАК Значение
		|ПОМЕСТИТЬ ВТ_СвойстваВХарактеристикахРС
		|ИЗ
		|	ВТ_КоличествоСвойстВХарактеристикахРС КАК ВТ_КоличествоСвойстВХарактеристикахРС
		|		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.ХарактеристикиНоменклатуры.ДополнительныеРеквизиты КАК ХарактеристикиНоменклатурыДополнительныеРеквизиты
		|		ПО ВТ_КоличествоСвойстВХарактеристикахРС.ЗаменяемыйМатериалХарактеристика = ХарактеристикиНоменклатурыДополнительныеРеквизиты.Ссылка
		|			И (НЕ ХарактеристикиНоменклатурыДополнительныеРеквизиты.Свойство = &Свойство)
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	ВТ_ОтборПоКоличествуСвойств.ЗаменяющийМатериалХарактеристика КАК ЗаменяющийМатериалХарактеристика,
		|	ВТ_ОтборПоКоличествуСвойств.ЗаменяющийМатериалНоменклатура КАК ЗаменяющийМатериалНоменклатура,
		|	ВТ_ОтборПоКоличествуСвойств.КоличествоСвойств КАК КоличествоСвойств,
		|	ХарактеристикиНоменклатурыДополнительныеРеквизиты.Свойство КАК Свойство,
		|	ХарактеристикиНоменклатурыДополнительныеРеквизиты.Значение КАК Значение
		|ПОМЕСТИТЬ ВТ_ХарактеристикиЗамещающейНоменклатуры
		|ИЗ
		|	ВТ_ОтборПоКоличествуСвойств КАК ВТ_ОтборПоКоличествуСвойств
		|		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.ХарактеристикиНоменклатуры.ДополнительныеРеквизиты КАК ХарактеристикиНоменклатурыДополнительныеРеквизиты
		|		ПО ВТ_ОтборПоКоличествуСвойств.ЗаменяющийМатериалХарактеристика = ХарактеристикиНоменклатурыДополнительныеРеквизиты.Ссылка
		|			И (НЕ ХарактеристикиНоменклатурыДополнительныеРеквизиты.Свойство = &Свойство)
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	ВТ_СвойстваВХарактеристикахРС.РесурснаяСпецификация КАК НаименованиеРС,
		|	ВТ_СвойстваВХарактеристикахРС.ЗаменяемыйМатериалНоменклатура КАК ЗаменяемыйМатериалНоменклатура,
		|	ВТ_СвойстваВХарактеристикахРС.ЗаменяемыйМатериалХарактеристика КАК ЗаменяемыйМатериалХарактеристика,
		|	&ЗаменяющийМатериалНоменклатура КАК ЗаменяющийМатериалНоменклатура,
		|	ЕСТЬNULL(ВТ_ХарактеристикиЗамещающейНоменклатуры.ЗаменяющийМатериалХарактеристика, ЗНАЧЕНИЕ(Справочник.ХарактеристикиНоменклатуры.ПустаяСсылка)) КАК ЗаменяющийМатериалХарактеристика,
		|	ВЫБОР
		|		КОГДА ВТ_ХарактеристикиЗамещающейНоменклатуры.ЗаменяющийМатериалХарактеристика ЕСТЬ NULL
		|			ТОГДА ЛОЖЬ
		|		ИНАЧЕ ИСТИНА
		|	КОНЕЦ КАК ВыбраннаяСтрока
		|ИЗ
		|	ВТ_СвойстваВХарактеристикахРС КАК ВТ_СвойстваВХарактеристикахРС
		|		ЛЕВОЕ СОЕДИНЕНИЕ ВТ_ХарактеристикиЗамещающейНоменклатуры КАК ВТ_ХарактеристикиЗамещающейНоменклатуры
		|		ПО ВТ_СвойстваВХарактеристикахРС.КоличествоСвойств = ВТ_ХарактеристикиЗамещающейНоменклатуры.КоличествоСвойств
		|			И ВТ_СвойстваВХарактеристикахРС.Свойство = ВТ_ХарактеристикиЗамещающейНоменклатуры.Свойство
		|			И ВТ_СвойстваВХарактеристикахРС.Значение = ВТ_ХарактеристикиЗамещающейНоменклатуры.Значение
		|
		|СГРУППИРОВАТЬ ПО
		|	ВТ_СвойстваВХарактеристикахРС.РесурснаяСпецификация,
		|	ВТ_СвойстваВХарактеристикахРС.ЗаменяемыйМатериалНоменклатура,
		|	ВТ_СвойстваВХарактеристикахРС.ЗаменяемыйМатериалХарактеристика,
		|	ВТ_ХарактеристикиЗамещающейНоменклатуры.ЗаменяющийМатериалХарактеристика,
		|	ЕСТЬNULL(ВТ_ХарактеристикиЗамещающейНоменклатуры.ЗаменяющийМатериалХарактеристика, ЗНАЧЕНИЕ(Справочник.ХарактеристикиНоменклатуры.ПустаяСсылка)),
		|	ВЫБОР
		|		КОГДА ВТ_ХарактеристикиЗамещающейНоменклатуры.ЗаменяющийМатериалХарактеристика ЕСТЬ NULL
		|			ТОГДА ЛОЖЬ
		|		ИНАЧЕ ИСТИНА
		|	КОНЕЦ";
	
КонецФункции

&НаСервере
Функция ТекстЗапросаВыбранныхРС()
	
	Возврат "ВЫБРАТЬ
	        |	ТЗ.Статус КАК Статус,
	        |	ТЗ.ДействуетС КАК ДействуетС,
	        |	ТЗ.ДействуетПо КАК ДействуетПо,
	        |	ТЗ.Номенклатура КАК Номенклатура,
	        |	ТЗ.НаименованиеРС КАК НаименованиеРС,
	        |	ТЗ.ВыбраннаяСтрока КАК ВыбраннаяСтрока
	        |ПОМЕСТИТЬ ВТ_Основная
	        |ИЗ
	        |	&ТЗ КАК ТЗ
	        |;
	        |
	        |////////////////////////////////////////////////////////////////////////////////
	        |ВЫБРАТЬ
	        |	ВТ_Основная.НаименованиеРС КАК НаименованиеРС
	        |ПОМЕСТИТЬ ВТ_ОтобранныеРС
	        |ИЗ
	        |	ВТ_Основная КАК ВТ_Основная
	        |ГДЕ
	        |	ВТ_Основная.ВыбраннаяСтрока
	        |;
	        |
	        |////////////////////////////////////////////////////////////////////////////////
	        |ВЫБРАТЬ
	        |	ВТ_ОтобранныеРС.НаименованиеРС КАК НаименованиеРС
	        |ИЗ
	        |	ВТ_ОтобранныеРС КАК ВТ_ОтобранныеРС";	
КонецФункции

&НаСервере
Функция ПолучитьСтруктуруЗаполненияДляГрупповойЗаменыМатериаловВРС()

	Структура = Новый Структура();
	
	Структура.Вставить("Основное");
	Структура.Вставить("ЗаменяемыйМатериалНоменклатура");
	Структура.Вставить("ЗаменяющийМатериалНоменклатура");
	Структура.Вставить("ЗаменяемыйМатериалХарактеристика");
	Структура.Вставить("ЗаменяющийМатериалХарактеристика");
	Структура.Вставить("ДатаНачалаЗамены");		
	
	Возврат Структура;
	
КонецФункции 

&НаСервере
Процедура ЗакрытьСтаруюИСоздатьНовуюРС()
	
	Запрос = Новый Запрос;
	МВТ = Новый МенеджерВременныхТаблиц;
	Запрос.МенеджерВременныхТаблиц = МВТ;
	Запрос.Текст = ПолучитьТекстЗапросаДляПроверки(); 
	Запрос.УстановитьПараметр("ТЗ", Объект.ПодробностиЗамен.Выгрузить());
	РезультатЗапроса = Запрос.Выполнить();
	
	Если Не РезультатЗапроса.Пустой() Тогда
		Текст = "Заменяющий материал должен быть заполнен";
		ОбщегоНазначения.СообщитьПользователю(Текст); 
		Возврат;
	КонецЕсли;
	
	Запрос = Новый Запрос;
	Запрос.МенеджерВременныхТаблиц = МВТ;
	Запрос.Текст = ПолучитьТекстЗапросаПоРСДляЗамены();
	
	РезультатЗапроса = Запрос.Выполнить();
	
	ВыборкаСсылка = РезультатЗапроса.Выбрать(ОбходРезультатаЗапроса.ПоГруппировкам);
	
	Пока ВыборкаСсылка.Следующий() Цикл
		
		СоздатьНовуюРСИЗакрытьСтарую(ВыборкаСсылка);
	
	КонецЦикла;
	
	ПерейтиНаНовуюВкладку("СозданныеРС");
	
КонецПроцедуры 

&НаСервере
Функция ПолучитьТекстЗапросаДляПроверки()
	
	Возврат "ВЫБРАТЬ
	        |	ТЗ.ВыбраннаяСтрока КАК ВыбраннаяСтрока,
	        |	ТЗ.НаименованиеРС КАК НаименованиеРС,
	        |	ТЗ.ЗаменяемыйМатериалНоменклатура КАК ЗаменяемыйМатериалНоменклатура,
	        |	ТЗ.ЗаменяемыйМатериалХарактеристика КАК ЗаменяемыйМатериалХарактеристика,
	        |	ТЗ.ЗаменяющийМатериалНоменклатура КАК ЗаменяющийМатериалНоменклатура,
	        |	ТЗ.ЗаменяющийМатериалХарактеристика КАК ЗаменяющийМатериалХарактеристика
			|ПОМЕСТИТЬ ВТ_ТЗ
	        |ИЗ
	        |	&ТЗ КАК ТЗ
	        |;
	        |
	        |////////////////////////////////////////////////////////////////////////////////
	        |ВЫБРАТЬ
	        |	ВТ_ТЗ.ВыбраннаяСтрока КАК ВыбраннаяСтрока,
	        |	ВТ_ТЗ.НаименованиеРС КАК НаименованиеРС,
	        |	ВТ_ТЗ.ЗаменяемыйМатериалНоменклатура КАК ЗаменяемыйМатериалНоменклатура,
	        |	ВТ_ТЗ.ЗаменяемыйМатериалХарактеристика КАК ЗаменяемыйМатериалХарактеристика,
	        |	ВТ_ТЗ.ЗаменяющийМатериалНоменклатура КАК ЗаменяющийМатериалНоменклатура,
	        |	ВТ_ТЗ.ЗаменяющийМатериалХарактеристика КАК ЗаменяющийМатериалХарактеристика
			|ПОМЕСТИТЬ ВТ_ПодробностиЗамен
	        |ИЗ
	        |	ВТ_ТЗ КАК ВТ_ТЗ
	        |ГДЕ
	        |	ВТ_ТЗ.ВыбраннаяСтрока
	        |
	        |СГРУППИРОВАТЬ ПО
	        |	ВТ_ТЗ.ВыбраннаяСтрока,
	        |	ВТ_ТЗ.ЗаменяемыйМатериалХарактеристика,
	        |	ВТ_ТЗ.ЗаменяющийМатериалХарактеристика,
	        |	ВТ_ТЗ.НаименованиеРС,
	        |	ВТ_ТЗ.ЗаменяемыйМатериалНоменклатура,
	        |	ВТ_ТЗ.ЗаменяющийМатериалНоменклатура
	        |;
	        |
	        |////////////////////////////////////////////////////////////////////////////////
	        |ВЫБРАТЬ
	        |	ВТ_ПодробностиЗамен.ВыбраннаяСтрока КАК ВыбраннаяСтрока,
	        |	ВТ_ПодробностиЗамен.НаименованиеРС КАК НаименованиеРС,
	        |	ВТ_ПодробностиЗамен.ЗаменяемыйМатериалНоменклатура КАК ЗаменяемыйМатериалНоменклатура,
	        |	ВТ_ПодробностиЗамен.ЗаменяемыйМатериалХарактеристика КАК ЗаменяемыйМатериалХарактеристика,
	        |	ВТ_ПодробностиЗамен.ЗаменяющийМатериалНоменклатура КАК ЗаменяющийМатериалНоменклатура,
	        |	ВТ_ПодробностиЗамен.ЗаменяющийМатериалХарактеристика КАК ЗаменяющийМатериалХарактеристика
	        |ИЗ
	        |	ВТ_ПодробностиЗамен КАК ВТ_ПодробностиЗамен
	        |ГДЕ
	        |	ВТ_ПодробностиЗамен.ЗаменяющийМатериалНоменклатура = ЗНАЧЕНИЕ(Справочник.Номенклатура.ПустаяСсылка)
	        |
	        |ОБЪЕДИНИТЬ ВСЕ
	        |
	        |ВЫБРАТЬ
	        |	ВТ_ПодробностиЗамен.ВыбраннаяСтрока,
	        |	ВТ_ПодробностиЗамен.НаименованиеРС,
	        |	ВТ_ПодробностиЗамен.ЗаменяемыйМатериалНоменклатура,
	        |	ВТ_ПодробностиЗамен.ЗаменяемыйМатериалХарактеристика,
	        |	ВТ_ПодробностиЗамен.ЗаменяющийМатериалНоменклатура,
	        |	ВТ_ПодробностиЗамен.ЗаменяющийМатериалХарактеристика
	        |ИЗ
	        |	ВТ_ПодробностиЗамен КАК ВТ_ПодробностиЗамен
	        |ГДЕ
	        |	ВТ_ПодробностиЗамен.ЗаменяющийМатериалХарактеристика = ЗНАЧЕНИЕ(Справочник.ХарактеристикиНоменклатуры.ПустаяСсылка)";
	
КонецФункции

&НаСервере
Функция ПолучитьТекстЗапросаПоРСДляЗамены()
	
	Возврат "ВЫБРАТЬ
	        |	ВТ_ПодробностиЗамен.ВыбраннаяСтрока КАК ВыбраннаяСтрока,
	        |	ВТ_ПодробностиЗамен.НаименованиеРС КАК НаименованиеРС,
	        |	ВТ_ПодробностиЗамен.ЗаменяемыйМатериалНоменклатура КАК ЗаменяемыйМатериалНоменклатура,
	        |	ВТ_ПодробностиЗамен.ЗаменяемыйМатериалХарактеристика КАК ЗаменяемыйМатериалХарактеристика,
	        |	ВТ_ПодробностиЗамен.ЗаменяющийМатериалНоменклатура КАК ЗаменяющийМатериалНоменклатура,
	        |	ВТ_ПодробностиЗамен.ЗаменяющийМатериалХарактеристика КАК ЗаменяющийМатериалХарактеристика,
	        |	ЕСТЬNULL(РесурсныеСпецификации.НачалоДействия, ДАТАВРЕМЯ(1, 1, 1, 0, 0, 0)) КАК НачалоДействия,
	        |	ЕСТЬNULL(РесурсныеСпецификации.КонецДействия, ДАТАВРЕМЯ(1, 1, 1, 0, 0, 0)) КАК КонецДействия,
	        |	ЕСТЬNULL(РесурсныеСпецификации.Статус, ЗНАЧЕНИЕ(Перечисление.СтатусыСпецификаций.ВРазработке)) КАК Статус,
	        |	ПРЕДСТАВЛЕНИЕ(ВТ_ПодробностиЗамен.НаименованиеРС) КАК ПредставлениеРС,
	        |	ЕСТЬNULL(РесурсныеСпецификацииМатериалыИУслуги.НомерСтроки, 0) КАК НомерСтроки
	        |ИЗ
	        |	ВТ_ПодробностиЗамен КАК ВТ_ПодробностиЗамен
	        |		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Справочник.РесурсныеСпецификации КАК РесурсныеСпецификации
	        |		ПО ВТ_ПодробностиЗамен.НаименованиеРС = РесурсныеСпецификации.Ссылка
	        |		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Справочник.РесурсныеСпецификации.МатериалыИУслуги КАК РесурсныеСпецификацииМатериалыИУслуги
	        |		ПО ВТ_ПодробностиЗамен.НаименованиеРС = РесурсныеСпецификацииМатериалыИУслуги.Ссылка
	        |			И ВТ_ПодробностиЗамен.ЗаменяемыйМатериалНоменклатура = РесурсныеСпецификацииМатериалыИУслуги.Номенклатура
	        |			И ВТ_ПодробностиЗамен.ЗаменяемыйМатериалХарактеристика = РесурсныеСпецификацииМатериалыИУслуги.Характеристика
	        |ИТОГИ
	        |	МАКСИМУМ(ВыбраннаяСтрока),
	        |	МАКСИМУМ(ЗаменяемыйМатериалНоменклатура),
	        |	МАКСИМУМ(ЗаменяемыйМатериалХарактеристика),
	        |	МАКСИМУМ(ЗаменяющийМатериалНоменклатура),
	        |	МАКСИМУМ(ЗаменяющийМатериалХарактеристика),
	        |	МАКСИМУМ(НачалоДействия),
	        |	МАКСИМУМ(КонецДействия),
	        |	МАКСИМУМ(Статус)
	        |ПО
	        |	НаименованиеРС";
	
КонецФункции

&НаСервере
Процедура СоздатьНовуюРСИЗакрытьСтарую(ВыборкаДетальныеЗаписи)
	
	НовыйЭлементРС = ВыборкаДетальныеЗаписи.НаименованиеРС.Скопировать();
	НовыйЭлементРС.НачалоДействия = ВыборкаДетальныеЗаписи.НачалоДействия;
	НовыйЭлементРС.КонецДействия = ВыборкаДетальныеЗаписи.КонецДействия;
	НовыйЭлементРС.Статус = ВыборкаДетальныеЗаписи.Статус;
		
	ВыборкаСтроки = ВыборкаДетальныеЗаписи.Выбрать();
	Пока ВыборкаСтроки.Следующий() Цикл
		ПараметрыПоиска = Новый Структура;
		ПараметрыПоиска.Вставить("Номенклатура", ВыборкаСтроки.ЗаменяемыйМатериалНоменклатура);
		ПараметрыПоиска.Вставить("НомерСтроки", ВыборкаСтроки.НомерСтроки);
		МассивСтрокТЧ = НовыйЭлементРС.МатериалыИУслуги.НайтиСтроки(ПараметрыПоиска);
		Если МассивСтрокТЧ.Количество() > 0 Тогда 
			СтрокаТЧ = МассивСтрокТЧ[0];
			СтрокаТЧ.Номенклатура = ВыборкаСтроки.ЗаменяющийМатериалНоменклатура;
			СтрокаТЧ.Характеристика = ВыборкаСтроки.ЗаменяющийМатериалХарактеристика; 
		КонецЕсли;	
	КонецЦикла; 
	
	НачатьТранзакцию();
	Попытка
		НовыйЭлементРС.Записать();
		ЗафиксироватьТранзакцию();
	Исключение
		ОтменитьТранзакцию(); 
		ЗаписьЖурналаРегистрации("Данные.ОтражениеРС", УровеньЖурналаРегистрации.Ошибка, , ,
		ОбработкаОшибок.ПодробноеПредставлениеОшибки(ИнформацияОбОшибке()));
		Текст = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку("Ошибка при записи РС %1 (см. Журнал)", 
																					НовыйЭлементРС.Наименование);
		ОбщегоНазначения.СообщитьПользователю(Текст); 
		Возврат;
	КонецПопытки;
	
	СтрокаСозданныеРС = Объект.СозданныеРС.Добавить();
	СтрокаСозданныеРС.НаименованиеРС = НовыйЭлементРС.Ссылка;
	СтрокаСозданныеРС.ДействуетС = НовыйЭлементРС.НачалоДействия;
	СтрокаСозданныеРС.ДействуетПо = НовыйЭлементРС.КонецДействия;
	СтрокаСозданныеРС.Статус  = НовыйЭлементРС.Статус;
	СтрокаСозданныеРС.Номенклатура = НовыйЭлементРС.ВыходныеИзделия[0].Номенклатура; 
	
	ТекущийЭлементРС = ВыборкаДетальныеЗаписи.НаименованиеРС.ПолучитьОбъект();
	ТекущийЭлементРС.КонецДействия = Объект.ДатаНачалаЗамены;
	НачатьТранзакцию();
	Попытка
		ТекущийЭлементРС.Записать();
		ЗафиксироватьТранзакцию();
	Исключение
		ОтменитьТранзакцию(); 
		ЗаписьЖурналаРегистрации("Данные.ОтражениеРС", УровеньЖурналаРегистрации.Ошибка, , ,
		ОбработкаОшибок.ПодробноеПредставлениеОшибки(ИнформацияОбОшибке()));
		Текст = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку("Ошибка при записи РС %1 (см. Журнал)", 
																				ВыборкаДетальныеЗаписи.ПредставлениеРС);
		ОбщегоНазначения.СообщитьПользователю(Текст);
	КонецПопытки;
	
КонецПроцедуры

&НаСервере
Функция ПроверитьКоличествоСвойствУХарактеристик(ЗаменяемыйМатериалНоменклатура, ЗаменяющийМатериалНоменклатура)
	
	Если ЗначениеЗаполнено(ЗаменяемыйМатериалНоменклатура) И ЗначениеЗаполнено(ЗаменяющийМатериалНоменклатура) Тогда 	
		Запрос = Новый Запрос;
		Запрос.Текст = 
		"ВЫБРАТЬ
		|	ХарактеристикиНоменклатурыДополнительныеРеквизиты.Свойство КАК Свойство
		|ПОМЕСТИТЬ ВТ_ПерваяНоменклатура
		|ИЗ
		|	Справочник.ХарактеристикиНоменклатуры.ДополнительныеРеквизиты КАК ХарактеристикиНоменклатурыДополнительныеРеквизиты
		|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Справочник.ХарактеристикиНоменклатуры КАК ХарактеристикиНоменклатуры
		|		ПО ХарактеристикиНоменклатурыДополнительныеРеквизиты.Ссылка = ХарактеристикиНоменклатуры.Ссылка
		|ГДЕ
		|	НЕ ХарактеристикиНоменклатурыДополнительныеРеквизиты.Свойство = &Свойство
		|	И ХарактеристикиНоменклатуры.Владелец = &ЗаменяемыйМатериалНоменклатура
		|
		|СГРУППИРОВАТЬ ПО
		|	ХарактеристикиНоменклатурыДополнительныеРеквизиты.Свойство
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	ХарактеристикиНоменклатурыДополнительныеРеквизиты.Свойство КАК Свойство
		|ПОМЕСТИТЬ ВТ_ВтораяНоменклатура
		|ИЗ
		|	Справочник.ХарактеристикиНоменклатуры.ДополнительныеРеквизиты КАК ХарактеристикиНоменклатурыДополнительныеРеквизиты
		|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Справочник.ХарактеристикиНоменклатуры КАК ХарактеристикиНоменклатуры
		|		ПО ХарактеристикиНоменклатурыДополнительныеРеквизиты.Ссылка = ХарактеристикиНоменклатуры.Ссылка
		|ГДЕ
		|	ХарактеристикиНоменклатуры.Владелец = &ЗаменяющийМатериалНоменклатура
		|	И НЕ ХарактеристикиНоменклатурыДополнительныеРеквизиты.Свойство = &Свойство
		|	И ХарактеристикиНоменклатурыДополнительныеРеквизиты.Свойство В
		|			(ВЫБРАТЬ
		|				Т.Свойство КАК Свойство
		|			ИЗ
		|				ВТ_ПерваяНоменклатура КАК Т)
		|
		|СГРУППИРОВАТЬ ПО
		|	ХарактеристикиНоменклатурыДополнительныеРеквизиты.Свойство
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	КОЛИЧЕСТВО(ВТ_ПерваяНоменклатура.Свойство) КАК СвойствоПервойНоменклатуры,
		|	0 КАК СвойствВторойНоменклатуры
		|ПОМЕСТИТЬ ВТ_ОбъединениеСвойств
		|ИЗ
		|	ВТ_ПерваяНоменклатура КАК ВТ_ПерваяНоменклатура
		|
		|ОБЪЕДИНИТЬ ВСЕ
		|
		|ВЫБРАТЬ
		|	0,
		|	КОЛИЧЕСТВО(ВТ_ВтораяНоменклатура.Свойство)
		|ИЗ
		|	ВТ_ВтораяНоменклатура КАК ВТ_ВтораяНоменклатура
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	СУММА(ВТ_ОбъединениеСвойств.СвойствВторойНоменклатуры) КАК СвойствВторойНоменклатуры,
		|	СУММА(ВТ_ОбъединениеСвойств.СвойствоПервойНоменклатуры) КАК СвойствоПервойНоменклатуры
		|ПОМЕСТИТЬ ВТ_СуммаСвойств
		|ИЗ
		|	ВТ_ОбъединениеСвойств КАК ВТ_ОбъединениеСвойств
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	ВЫБОР
		|		КОГДА ВТ_СуммаСвойств.СвойствВторойНоменклатуры = ВТ_СуммаСвойств.СвойствоПервойНоменклатуры
		|			ТОГДА ЛОЖЬ
		|		ИНАЧЕ ИСТИНА
		|	КОНЕЦ КАК РезультатСравненияНоменклатуры
		|ИЗ
		|	ВТ_СуммаСвойств КАК ВТ_СуммаСвойств";
		
		СвойствоОписание = ПланыВидовХарактеристик.ДополнительныеРеквизитыИСведения.НайтиПоРеквизиту("Имя", 
		"Чд_ОписаниеХарактеристики");
		Запрос.УстановитьПараметр("Свойство", СвойствоОписание);
		Запрос.УстановитьПараметр("ЗаменяемыйМатериалНоменклатура", ЗаменяемыйМатериалНоменклатура);
		Запрос.УстановитьПараметр("ЗаменяющийМатериалНоменклатура", ЗаменяющийМатериалНоменклатура);
		
		РезультатЗапроса = Запрос.Выполнить();
		
		ВыборкаДетальныеЗаписи = РезультатЗапроса.Выбрать();
		
		ВыборкаДетальныеЗаписи.Следующий();	
		Возврат ВыборкаДетальныеЗаписи.РезультатСравненияНоменклатуры; 
	Иначе 
		Возврат Ложь;
	КонецЕсли;
	
КонецФункции
#КонецОбласти