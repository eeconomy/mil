﻿#Область ОписаниеПеременных

&НаКлиенте
Перем ВыбранныеЗаказы;

&НаКлиенте
Перем ВыбранныеЗаданияНаРаскрой;

&НаКлиенте
Перем ВыбранныеКарты;

#КонецОбласти

#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	ДатаНачалаОтбор = ДобавитьМесяц(ТекущаяДатаСеанса(), -4);
	
	УсловноеОформлениеФормы();
	
	ОбъектыПечати = Новый Массив;
	ОбъектыПечати.Добавить(Метаданные.Документы.Чд_КонтрольныйЛист);
	// СтандартныеПодсистемы.ПодключаемыеКоманды
	ПараметрыРазмещения = ПодключаемыеКоманды.ПараметрыРазмещения();
	ПараметрыРазмещения.Источники = ОбъектыПечати;
	ПодключаемыеКоманды.ПриСозданииНаСервере(ЭтотОбъект, ПараметрыРазмещения);
	// Конец СтандартныеПодсистемы.ПодключаемыеКоманды
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	ВыбранныеЗаказы = Новый Массив;
	ВыбранныеЗаданияНаРаскрой = Новый Массив;
	ВыбранныеКарты = Новый Массив;
	
	ЗаданияНаРаскрой.Очистить();
	КартыРаскроя.Очистить();
	КоличествоВыбранныхЗаданий = 0;
	КоличествоВыбранныхЗаказов = 0;
	КоличествоВыбранныхКарт = 0;
	
	СписокЗаказов.Параметры.УстановитьЗначениеПараметра("ВыбранныеЗаказы", ВыбранныеЗаказы);
	СписокЗаданийНаРаскрой.Параметры.УстановитьЗначениеПараметра("ВыбранныеЗаданияНаРаскрой", ВыбранныеЗаданияНаРаскрой);
	СписокКартРаскроя.Параметры.УстановитьЗначениеПараметра("ВыбранныеКарты", ВыбранныеКарты);
	СписокКонтрольныхЛистов.Параметры.УстановитьЗначениеПараметра("КартаРаскрояОтбор", КартыРаскроя);
	
	ОбновитьЗаказыНаКлиенте();
	ОбновитьЗаданияНаРаскройНаКлиенте();
	ОбновитьКартыНаКлиенте();
	ОбновитьКонтрольныеЛистыНаКлиенте();
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовФормы

&НаКлиенте
Процедура ДатаНачалаОтборПриИзменении(Элемент)
	ОбновитьЗаказыНаКлиенте();
КонецПроцедуры

&НаКлиенте
Процедура ДатаОкончанияОтборПриИзменении(Элемент)
	ОбновитьЗаказыНаКлиенте();
КонецПроцедуры

&НаКлиенте
Процедура БригадаПошиваОтборПриИзменении(Элемент)
	ОбновитьЗаказыНаКлиенте();
КонецПроцедуры

&НаКлиенте
Процедура БригадаРаскрояКружеваОтборПриИзменении(Элемент)
	ОбновитьЗаказыНаКлиенте();
КонецПроцедуры

&НаКлиенте
Процедура МодельЦветОтборПриИзменении(Элемент)
	ОбновитьЗаказыНаКлиенте();
КонецПроцедуры

&НаКлиенте
Процедура СписокЗаказовВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	
	Если Поле.Имя = "СписокЗаказовВыбран" Тогда
		СтандартнаяОбработка = Ложь;
		СсылкаЗаказа = Элементы.СписокЗаказов.ТекущиеДанные.Ссылка;
		ИдентификаторЗаказа = Элементы.СписокЗаказов.ТекущаяСтрока;
		
		мИндекс = ВыбранныеЗаказы.Найти(СсылкаЗаказа);
		Если КоличествоВыбранныхЗаказов <> 0 И мИндекс = Неопределено Тогда
			ВыбранныеЗаказы.Очистить();
		КонецЕсли;
		Если мИндекс = Неопределено Тогда
			ВыбранныеЗаказы.Добавить(СсылкаЗаказа);
		Иначе
			ВыбранныеЗаказы.Удалить(мИндекс);
		КонецЕсли;
		КоличествоВыбранныхЗаказов = ВыбранныеЗаказы.Количество();
		СписокЗаказов.Параметры.УстановитьЗначениеПараметра("ВыбранныеЗаказы", ВыбранныеЗаказы);
		ОбновитьЗаказНаПроизводствоОтбор(СсылкаЗаказа);
		ВыбранныеЗаданияНаРаскрой.Очистить();
		КоличествоВыбранныхЗаданий = 0;
		ВыбранныеКарты.Очистить();
		КоличествоВыбранныхКарт = 0;
		КонтрольныеЛистыОтбор = 0;
		ОбновитьЗаданияНаРаскройНаКлиенте();
		УсловноеОформлениеФормы();
		НайтиТекущуюСтрокуСписка(ИдентификаторЗаказа);
		
	ИначеЕсли Элемент.ТекущийЭлемент.Имя = "СписокЗаказовМодельЦвет" И Элементы.СписокЗаказов.ТекущиеДанные <> Неопределено Тогда
		ПараметрыФормы = Новый Структура("Ключ", Элементы.СписокЗаказов.ТекущиеДанные.МодельЦвет);
		ОткрытьФорму("Справочник.Номенклатура.Форма.ФормаЭлемента", ПараметрыФормы);
	ИначеЕсли Элемент.ТекущийЭлемент.Имя = "СписокЗаказовЗаказНаПроизводствоНомер" И Элементы.СписокЗаказов.ТекущиеДанные <> Неопределено Тогда
		ПараметрыФормы = Новый Структура("Ключ", Элементы.СписокЗаказов.ТекущиеДанные.Ссылка);
		ОткрытьФорму("Документ.ЗаказНаПроизводство2_2.Форма.ФормаДокумента", ПараметрыФормы);
	ИначеЕсли Элемент.ТекущийЭлемент.Имя = "СписокЗаказовРРШ" И Элементы.СписокЗаказов.ТекущиеДанные <> Неопределено Тогда
		ПараметрыФормы = Новый Структура("Ключ", Элементы.СписокЗаказов.ТекущиеДанные.РРШ);
		ОткрытьФорму("Справочник.Чд_РРШ.Форма.ФормаЭлемента", ПараметрыФормы);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура СписокЗаданийНаРаскройВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	
	Если Поле.Имя = "СписокЗаданийНаРаскройВыбран" Тогда
		СтандартнаяОбработка = Ложь;
		СсылкаЗадания = Элементы.СписокЗаданийНаРаскрой.ТекущиеДанные.Ссылка;
		мИндекс = ВыбранныеЗаданияНаРаскрой.Найти(СсылкаЗадания);
		мИндексЗадания = ЗаданияНаРаскрой.НайтиПоЗначению(СсылкаЗадания);
		
		Если мИндекс = Неопределено Тогда
			ВыбранныеЗаданияНаРаскрой.Добавить(СсылкаЗадания);
		Иначе
			ВыбранныеЗаданияНаРаскрой.Удалить(мИндекс);
		КонецЕсли;
		
		Если мИндексЗадания = Неопределено Тогда
			ЗаданияНаРаскрой.Добавить(СсылкаЗадания);
		Иначе
			ЗаданияНаРаскрой.Удалить(мИндексЗадания);
		КонецЕсли;
		
		КоличествоВыбранныхЗаданий = ВыбранныеЗаданияНаРаскрой.Количество();
		СписокЗаданийНаРаскрой.Параметры.УстановитьЗначениеПараметра("ВыбранныеЗаданияНаРаскрой", ВыбранныеЗаданияНаРаскрой);
		
		ВыбранныеКарты.Очистить();
		КоличествоВыбранныхКарт = 0;
		КонтрольныеЛистыОтбор = 0;
		
		ОбновитьКартыНаКлиенте();
		УсловноеОформлениеФормы();
		
	ИначеЕсли Элементы.СписокЗаданийНаРаскрой.ТекущиеДанные <> Неопределено Тогда
		ПараметрыФормы = Новый Структура("Ключ", Элементы.СписокЗаданийНаРаскрой.ТекущиеДанные.Ссылка);
		ОткрытьФорму("Документ.Чд_ЗаданиеНаРаскрой.Форма.ФормаДокумента", ПараметрыФормы);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура СписокКонтрольныхЛистовВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	
	Если Элементы.СписокКонтрольныхЛистов.ТекущиеДанные <> Неопределено
		И Элемент.ТекущийЭлемент.Имя = "СписокКонтрольныхЛистовКартаРаскроя" Тогда
		
		ПараметрыФормы = Новый Структура("Ключ", Элементы.СписокКонтрольныхЛистов.ТекущиеДанные.КартаРаскроя);
		ОткрытьФорму("Документ.Чд_КартаРаскроя.Форма.ФормаДокумента", ПараметрыФормы);
	ИначеЕсли Элементы.СписокКонтрольныхЛистов.ТекущиеДанные <> Неопределено Тогда
		ПараметрыФормы = Новый Структура("Ключ", Элементы.СписокКонтрольныхЛистов.ТекущиеДанные.КонтрольныйЛист);
		ОткрытьФорму("Документ.Чд_КонтрольныйЛист.Форма.ФормаДокумента", ПараметрыФормы);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура СписокКартРаскрояВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	
	Если Поле.Имя = "СписокКартРаскрояВыбран" Тогда
		СтандартнаяОбработка = Ложь;
		СсылкаКарта = Элементы.СписокКартРаскроя.ТекущиеДанные.Ссылка;
		мИндекс = ВыбранныеКарты.Найти(СсылкаКарта);
		мИндексКарты = КартыРаскроя.НайтиПоЗначению(СсылкаКарта);
		
		Если мИндекс = Неопределено Тогда
			ВыбранныеКарты.Добавить(СсылкаКарта);
		Иначе
			ВыбранныеКарты.Удалить(мИндекс);
		КонецЕсли;
		
		Если мИндексКарты = Неопределено Тогда
			КартыРаскроя.Добавить(СсылкаКарта);
		Иначе
			КартыРаскроя.Удалить(мИндексКарты);
		КонецЕсли;
		
		КоличествоВыбранныхКарт = ВыбранныеКарты.Количество();
		СписокКартРаскроя.Параметры.УстановитьЗначениеПараметра("ВыбранныеКарты", ВыбранныеКарты);
		КонтрольныеЛистыОтбор = 0;
		ОбновитьКонтрольныеЛистыНаСервере();
		УсловноеОформлениеФормы();
		
	ИначеЕсли Элемент.ТекущийЭлемент.Имя = "СписокКартРаскрояНомерЗадания"
		И Элементы.СписокКартРаскроя.ТекущиеДанные <> Неопределено Тогда
		
		ПараметрыФормы = Новый Структура("Ключ", Элементы.СписокКартРаскроя.ТекущиеДанные.ЗаданиеНаРаскрой);
		ОткрытьФорму("Документ.Чд_ЗаданиеНаРаскрой.Форма.ФормаДокумента", ПараметрыФормы);
		
	ИначеЕсли Элемент.ТекущийЭлемент.Имя = "СписокКартРаскрояНомер"
		И Элементы.СписокКартРаскроя.ТекущиеДанные <> Неопределено Тогда
		
		ПараметрыФормы = Новый Структура("Ключ", Элементы.СписокКартРаскроя.ТекущиеДанные.Ссылка);
		ОткрытьФорму("Документ.Чд_КартаРаскроя.Форма.ФормаДокумента", ПараметрыФормы);
		
	ИначеЕсли Элемент.ТекущийЭлемент.Имя = "СписокКартРаскрояМодель"
		И Элементы.СписокКартРаскроя.ТекущиеДанные <> Неопределено Тогда
		
		ПараметрыФормы = Новый Структура("Ключ", Элементы.СписокКартРаскроя.ТекущиеДанные.Модель);
		ОткрытьФорму("Справочник.Номенклатура.Форма.ФормаЭлемента", ПараметрыФормы);
		
	ИначеЕсли Элемент.ТекущийЭлемент.Имя = "СписокКартРаскрояАртикул"
		И Элементы.СписокКартРаскроя.ТекущиеДанные <> Неопределено Тогда
		
		ПараметрыФормы = Новый Структура("Ключ", Элементы.СписокКартРаскроя.ТекущиеДанные.Номенклатура);
		ОткрытьФорму("Справочник.Номенклатура.Форма.ФормаЭлемента", ПараметрыФормы);
		
	ИначеЕсли Элемент.ТекущийЭлемент.Имя = "СписокКартРаскрояЭтап" И Элементы.СписокКартРаскроя.ТекущиеДанные <> Неопределено Тогда
		
		ПараметрыФормы = Новый Структура("Ключ", Элементы.СписокКартРаскроя.ТекущиеДанные.ЭтапПроизводства);
		ОткрытьФорму("Документ.ЭтапПроизводства2_2.Форма.ФормаДокумента", ПараметрыФормы);
		
	ИначеЕсли Элемент.ТекущийЭлемент.Имя = "СписокКартРаскрояМаршрутнаяКарта"
		И Элементы.СписокКартРаскроя.ТекущиеДанные <> Неопределено Тогда
		
		ПараметрыФормы = Новый Структура("Ключ", Элементы.СписокКартРаскроя.ТекущиеДанные.МаршрутнаяКарта);
		ОткрытьФорму("Справочник.МаршрутныеКарты.Форма.ФормаЭлемента", ПараметрыФормы);
		
	ИначеЕсли Элемент.ТекущийЭлемент.Имя = "СписокКартРаскрояЗакрытиеКартыРаскроя"
		И Элементы.СписокКартРаскроя.ТекущиеДанные <> Неопределено Тогда
		
		ПараметрыФормы = Новый Структура("Ключ", Элементы.СписокКартРаскроя.ТекущиеДанные.ЗакрытиеКартыРаскроя);
		ОткрытьФорму("Документ.Чд_ЗакрытиеКартыРаскроя.Форма.ФормаДокумента", ПараметрыФормы);
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПроцессОтборПриИзменении(Элемент)
	
	ОбновитьЗаказыНаКлиенте();
	ОбновитьЗаданияНаРаскройНаКлиенте();
	ОбновитьКартыНаКлиенте();
	
КонецПроцедуры

&НаКлиенте
Процедура КотрольныеЛистыОтборПриИзменении(Элемент)
	
	УстановитьОтборКартРаскроя();
	
КонецПроцедуры

&НаКлиенте
Процедура СтатусКартыОтборПриИзменении(Элемент)
	
	УстановитьОтборКартРаскроя();
	
КонецПроцедуры

&НаКлиенте
Процедура СтатусКЛОтборПриИзменении(Элемент)
	
	УстановитьОтборКонтрольныеЛисты();
	
КонецПроцедуры

&НаКлиенте
Процедура СостояниеКЛОтборПриИзменении(Элемент)
	УстановитьОтборКонтрольныеЛисты();
КонецПроцедуры

&НаКлиенте
Процедура ОперацииОтборПриИзменении(Элемент)
	
	УстановитьОтборКонтрольныеЛисты();
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ПечатьМонофилЧашки(Команда)
	
	// 4D: Милавица, АндрейС, 20.03.2020 13:18:55
	// Дополнительные требования по раскройному цеху.Печатные формы «Монофил чашки» И «Мягкие чашки» по документу ""Карта раскроя"" №25008
	// {
	МассивСтрокКартРаскроя = Новый Массив;
	МассивИдентификаторовКарт = Элементы.СписокКартРаскроя.ВыделенныеСтроки;
	Для каждого КартаРаскроя Из МассивИдентификаторовКарт Цикл
		МассивСтрокКартРаскроя.Добавить(Элементы.СписокКартРаскроя.ДанныеСтроки(КартаРаскроя));
	КонецЦикла;
	
	ПараметрыПечати = Новый Структура;
	УправлениеПечатьюКлиент.ВыполнитьКомандуПечати("Документ.Чд_КартаРаскроя", // Менеджер печати
		"МонофилЧашки", // Идентификатор
		СформироватьПечатныеФормыЧашки(МассивСтрокКартРаскроя), // Объекты печати
		ЭтотОбъект);
	// }
	// 4D
	
КонецПроцедуры

&НаКлиенте
Процедура ПечатьМягкиеЧашки(Команда)
	
	// 4D: Милавица, АндрейС, 20.03.2020 13:18:55
	// Дополнительные требования по раскройному цеху.Печатные формы «Монофил чашки» И «Мягкие чашки» по документу ""Карта раскроя"" №25008
	// {
	МассивСтрокКартРаскроя = Новый Массив;
	МассивИдентификаторовКарт = Элементы.СписокКартРаскроя.ВыделенныеСтроки;
	Для каждого КартаРаскроя Из МассивИдентификаторовКарт Цикл
		МассивСтрокКартРаскроя.Добавить(Элементы.СписокКартРаскроя.ДанныеСтроки(КартаРаскроя));
	КонецЦикла;
	
	ПараметрыПечати = Новый Структура;
	УправлениеПечатьюКлиент.ВыполнитьКомандуПечати("Документ.Чд_КартаРаскроя", // Менеджер печати
		"МягкиеЧашки", // Идентификатор
		СформироватьПечатныеФормыЧашки(МассивСтрокКартРаскроя), // Объекты печати
		ЭтотОбъект);
	// }
	// 4D
	
КонецПроцедуры

&НаСервере
Функция СформироватьПечатныеФормыЧашки(МассивСтрокКартРаскроя)
	
	// 4D: Милавица, АндрейС, 20.03.2020 14:16:48
	// Дополнительные требования по раскройному цеху.Печатные формы «Монофил чашки» И «Мягкие чашки» по документу ""Карта раскроя"" №25008
	// {
	МассивОбъектов = Новый Массив;
	Для каждого КартаРаскроя Из МассивСтрокКартРаскроя Цикл
		МассивОбъектов.Добавить(КартаРаскроя.Ссылка);
	КонецЦикла;
	
	Возврат МассивОбъектов;
	// }
	// 4D
	
КонецФункции

&НаКлиенте
Процедура СвязанныеДокументыЗаказа(Команда)
	
	ОткрытьФорму("ОбщаяФорма.СтруктураПодчиненности", Новый Структура("ОбъектОтбора", Элементы.СписокЗаказов.ТекущиеДанные.Ссылка));
	
КонецПроцедуры

&НаКлиенте
Процедура СвязанныеДокументыКЛ(Команда)
	
	Если Элементы.СписокКонтрольныхЛистов.ТекущиеДанные <> Неопределено Тогда
		ОткрытьФорму("ОбщаяФорма.СтруктураПодчиненности",
			Новый Структура("ОбъектОтбора", Элементы.СписокКонтрольныхЛистов.ТекущиеДанные.КонтрольныйЛист));
	КонецЕсли;
		
КонецПроцедуры

&НаКлиенте
Процедура ОбновитьКартыКроя(Команда)
	
	ОбновитьКартыНаКлиенте();
	
КонецПроцедуры

&НаКлиенте
Процедура ОбновитьКонтрольныеЛисты(Команда)
	
	ОбновитьКонтрольныеЛистыНаКлиенте();
	
КонецПроцедуры

&НаКлиенте
Процедура ОбновитьЗаданияНаРаскрой(Команда)
	
	ОбновитьЗаданияНаРаскройНаКлиенте();
	
КонецПроцедуры

&НаКлиенте
Процедура ОбновитьЗаказы(Команда)
	
	ОбновитьЗаказыНаКлиенте();
	
КонецПроцедуры

&НаКлиенте
Процедура ВыбратьПериод(Команда)
	
	ВыбранныеЗаказы.Очистить();
	КоличествоВыбранныхЗаказов = ВыбранныеЗаказы.Количество();
	
	ПараметрыВыбора = Новый Структура("НачалоПериода,КонецПериода", ДатаНачалаОтбор, ДатаОкончанияОтбор);
	ОписаниеОповещения = Новый ОписаниеОповещения("ВыбратьПериодЗавершение", ЭтотОбъект);
	ОткрытьФорму("ОбщаяФорма.ВыборСтандартногоПериода", ПараметрыВыбора, Элементы.ВыбратьПериод, , , , ОписаниеОповещения);
	
КонецПроцедуры

&НаКлиенте
Процедура СформироватьОценкаКомплектностиПолуфабрикатовПоЗаказу(Команда)
	
	Если КоличествоВыбранныхЗаказов = 1 Тогда
		
		ПараметрыФормы = Новый Структура;
		ПараметрыФормы.Вставить("СформироватьПриОткрытии", Истина);
		ПараметрыФормы.Вставить("Отбор", Новый Структура);
		ПараметрыФормы.Отбор.Вставить("ЗаказНаПроизводство", ЗаказНаПроизводствоОтбор);
		
		ОткрытьФорму("Отчет.Чд_ОценкаКомплектностиПолуфабрикатовПоЗаказу.Форма", ПараметрыФормы, ЭтаФорма);
	Иначе
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю("Необходимо выбрать один заказ на производство!");
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ИзменитьМаршрутнуюКарту(Команда)
	
	инПодразделение = ПредопределенноеЗначение("Справочник.СтруктураПредприятия.ПустаяСсылка");
	
	МассивКарт.Очистить();
	Если Элементы.СписокКартРаскроя.ВыделенныеСтроки = Неопределено Тогда
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю("Не выбрано ни одной карты раскроя для изменения маршрутной карты!");
		Возврат;
	КонецЕсли;
	Для каждого СтрокаВыделена Из Элементы.СписокКартРаскроя.ВыделенныеСтроки Цикл
		СтрокаСписка = Элементы.СписокКартРаскроя.ДанныеСтроки(СтрокаВыделена);
		Если СтрокаСписка.КонтрольныеЛистыСформированы Тогда
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю("Для карты " + СтрокаСписка.Номер + " невозможно изменить маршрутную карту!");
		Иначе
			МассивКарт.Добавить(СтрокаСписка.Ссылка);
			инПодразделение = СтрокаСписка.Подразделение;
		КонецЕсли;
	КонецЦикла;
	
	ПараметрыОтбора = Новый Структура("ПометкаУдаления", Ложь);
	
	ПараметрыОтбора.Вставить("Подразделение", инПодразделение);
	ПараметрыОтбора.Вставить("Статус", ПредопределенноеЗначение("Перечисление.СтатусыМаршрутныхКарт.Действует"));
	
	Если ЗначениеЗаполнено(ПроцессОтбор) Тогда
		ПараметрыОтбора.Вставить("Чд_КодПроцесса", ПроцессОтбор);
	КонецЕсли;
	
	Если МассивКарт.Количество() <> 0 Тогда
		ПараметрыФормы = Новый Структура("Отбор", ПараметрыОтбора);
		ОписаниеОповещения = Новый ОписаниеОповещения("ОбработчикЗаполнитьПоМаршрутнойКарте", ЭтотОбъект);
		ОткрытьФорму("Справочник.МаршрутныеКарты.ФормаВыбора",
			ПараметрыФормы, , , , ,
			ОписаниеОповещения,
			РежимОткрытияОкнаФормы.БлокироватьОкноВладельца);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура УстановитьСтатусСформирован(Команда)
	
	Статус = ПредопределенноеЗначение("Перечисление.Чд_СтатусыКЛ.Сформирован");
	УстановитьСтатусНаКлиенте(Статус);
	
КонецПроцедуры

&НаКлиенте
Процедура УстановитьСтатусЗакрыт(Команда)
	
	Статус = ПредопределенноеЗначение("Перечисление.Чд_СтатусыКЛ.Закрыт");
	УстановитьСтатусНаКлиенте(Статус);
	
КонецПроцедуры

&НаКлиенте
Процедура УстановитьСтатусЗакрытаКарта(Команда)
	
	Для каждого СтрокаВыделена Из Элементы.СписокКартРаскроя.ВыделенныеСтроки Цикл
		СтрокаСписка = Элементы.СписокКартРаскроя.ДанныеСтроки(СтрокаВыделена);
		ПеревестиКартуВСтатус(СтрокаСписка.Ссылка, "Закрыта");
	КонецЦикла;
	
	Элементы.СписокКартРаскроя.Обновить();
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьСписокКартРаскроя(Команда)
	
	ОткрытьФорму("Документ.Чд_КартаРаскроя.ФормаСписка");
	
КонецПроцедуры

// СтандартныеПодсистемы.ПодключаемыеКоманды
&НаКлиенте
Процедура Подключаемый_ВыполнитьКоманду(Команда)
	
	ПодключаемыеКомандыКлиент.ВыполнитьКоманду(ЭтотОбъект, Команда, Элементы.СписокКонтрольныхЛистов);
	
КонецПроцедуры

&НаКлиенте
Процедура ПечатьЭтикетокНаЛоток(Команда)
	
	Если Элементы.СписокЗаказов.ТекущиеДанные = Неопределено Тогда
		Возврат;
	КонецЕсли;
	ПараметрыЗапуска = Новый Структура;
	ПараметрыЗапуска.Вставить("ЗаказНаПроизводство", Элементы.СписокЗаказов.ТекущиеДанные.Ссылка);
	ОткрытьФорму("Обработка.Чд_ПечатьЭтикеток.Форма.ФормаПечати", ПараметрыЗапуска, ЭтаФорма);
	
КонецПроцедуры

// 4D:Милавица, ЕленаТ, 07.07.2021 
// Разработка Обработки для контроля корректности заполнения документов раскройного цеха, № 29935  
// { 
&НаКлиенте
Процедура КонтрольДокументов(Команда)
	ОткрытьФорму("Обработка.ЧД_КонтрольДокументовРЦдляЗакрытияМесяца.Форма.Форма", , ЭтотОбъект);
КонецПроцедуры
// }
// 4D	
#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура УсловноеОформлениеФормы()
	
	Если КоличествоВыбранныхЗаказов = 1
		И КоличествоВыбранныхЗаданий = 0 И КоличествоВыбранныхКарт = 0 Тогда
		Элементы.ГруппаСписокЗаданийНаРаскрой.Доступность = Истина;
		Элементы.ГруппаКартРаскроя.Доступность = Ложь;
		Элементы.ГруппаКонтрольныеЛисты.Доступность = Ложь;
		
	ИначеЕсли КоличествоВыбранныхЗаказов = 1
		И КоличествоВыбранныхЗаданий <> 0 И КоличествоВыбранныхКарт = 0 Тогда
		
		Элементы.ГруппаСписокЗаданийНаРаскрой.Доступность = Истина;
		Элементы.ГруппаКартРаскроя.Доступность = Истина;
		Элементы.ГруппаКонтрольныеЛисты.Доступность = Ложь;
		
	ИначеЕсли КоличествоВыбранныхЗаказов = 1
		И КоличествоВыбранныхЗаданий <> 0 И КоличествоВыбранныхКарт > 0 Тогда
		
		Элементы.ГруппаСписокЗаданийНаРаскрой.Доступность = Истина;
		Элементы.ГруппаКартРаскроя.Доступность = Истина;
		Элементы.ГруппаКонтрольныеЛисты.Доступность = Истина;
		
	Иначе
		Элементы.ГруппаКартРаскроя.Доступность = Ложь;
		Элементы.ГруппаКонтрольныеЛисты.Доступность = Ложь;
		Элементы.ГруппаСписокЗаданийНаРаскрой.Доступность = Ложь;
	КонецЕсли;
	
	Элементы.СписокЗаказов.Обновить();
	Элементы.СписокЗаданийНаРаскрой.Обновить();
	Элементы.СписокКартРаскроя.Обновить();
	Элементы.СписокКонтрольныхЛистов.Обновить();
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьИтогоТрудоемкость()
	
	Схема = Элементы.СписокКартРаскроя.ПолучитьИсполняемуюСхемуКомпоновкиДанных();
	Настройки = Элементы.СписокКартРаскроя.ПолучитьИсполняемыеНастройкиКомпоновкиДанных();
	КомпоновщикМакета = Новый КомпоновщикМакетаКомпоновкиДанных;
	МакетКомпоновки = КомпоновщикМакета.Выполнить(Схема, Настройки, , , Тип("ГенераторМакетаКомпоновкиДанныхДляКоллекцииЗначений"));
	ПроцессорКомпоновки = Новый ПроцессорКомпоновкиДанных;
	ПроцессорКомпоновки.Инициализировать(МакетКомпоновки);
	ПроцессорВывода = Новый ПроцессорВыводаРезультатаКомпоновкиДанныхВКоллекциюЗначений;
	Результат = ПроцессорВывода.Вывести(ПроцессорКомпоновки);
	ИтогоТрудоемкость = Результат.Итог("Трудоемкость");
	
КонецПроцедуры

&НаСервере
Процедура НайтиТекущуюСтрокуСписка(ИдентификаторЗаказа)
	
	Элементы.СписокЗаказов.ТекущаяСтрока = ИдентификаторЗаказа;
	
КонецПроцедуры

&НаКлиенте
Процедура УстановитьСтатусНаКлиенте(НовыйСтатус)
	
	Если Элементы.СписокКонтрольныхЛистов.ВыделенныеСтроки = Неопределено Тогда
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю("Не выбрано ни одного Контрольного листа для изменения статуса!");
		Возврат;
	КонецЕсли;
	Для каждого СтрокаВыделена Из Элементы.СписокКонтрольныхЛистов.ВыделенныеСтроки Цикл
		СтрокаСписка = Элементы.СписокКонтрольныхЛистов.ДанныеСтроки(СтрокаВыделена);
		УстановитьСтатусКЛНаСервере(СтрокаСписка.КонтрольныйЛист, НовыйСтатус);
	КонецЦикла;
	
	Элементы.СписокКонтрольныхЛистов.Обновить();
КонецПроцедуры

&НаСервере
Процедура УстановитьСтатусКЛНаСервере(КонтрольныйЛист, НовыйСтатус)
	
	Документы.Чд_КонтрольныйЛист.УстановитьСтатус(КонтрольныйЛист, НовыйСтатус);
	
КонецПроцедуры

&НаКлиенте
Процедура УстановитьОтборКартРаскроя()
	
	СписокКартРаскроя.Отбор.Элементы.Очистить();
	
	Если СтатусКартыОтбор.Количество() <> 0 Тогда
		ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбора(СписокКартРаскроя.Отбор,
			"Статус",
			СтатусКартыОтбор,
			ВидСравненияКомпоновкиДанных.ВСписке);
	КонецЕсли;
	
	Если КонтрольныеЛистыОтбор <> 0 Тогда
		ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбора(СписокКартРаскроя.Отбор,
			"КонтрольныеЛистыСформированы",
			?(КонтрольныеЛистыОтбор = 1, Ложь, Истина),
				ВидСравненияКомпоновкиДанных.Равно);
	КонецЕсли;
	
	Элементы.СписокКартРаскроя.Обновить();
	Элементы.СписокКонтрольныхЛистов.Обновить();
	
КонецПроцедуры

&НаСервере
Процедура ВыбратьПериодЗавершение(РезультатВыбора, ДопПараметры) Экспорт
	
	Если РезультатВыбора = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	ДатаНачалаОтбор = РезультатВыбора.НачалоПериода;
	ДатаОкончанияОтбор = РезультатВыбора.КонецПериода;
	ОбновитьЗаказНаПроизводствоОтбор();
	ОбновитьЗаказыНаСервере();
	УсловноеОформлениеФормы();
	
КонецПроцедуры

&НаСервере
Процедура ОбновитьЗаказНаПроизводствоОтбор(ВыбранныйЗаказ = Неопределено)
	
	Если КоличествоВыбранныхЗаказов = 1 И ВыбранныйЗаказ <> Неопределено Тогда
		ЗаказНаПроизводствоОтбор = ВыбранныйЗаказ;
	Иначе
		ЗаказНаПроизводствоОтбор = Документы.ЗаказНаПроизводство2_2.ПустаяСсылка();
	КонецЕсли;
	
КонецПроцедуры

#Область ПроцедурыИФункцииСпискаЗаказов

&НаКлиенте
Процедура ОбновитьЗаказыНаКлиенте()
	
	ВыбранныеЗаказы.Очистить();
	КоличествоВыбранныхЗаказов = ВыбранныеЗаказы.Количество();
	ОбновитьЗаказНаПроизводствоОтбор();
	СписокЗаказов.Параметры.УстановитьЗначениеПараметра("ВыбранныеЗаказы", ВыбранныеЗаказы);
	ОбновитьЗаказыНаСервере();
	УсловноеОформлениеФормы();
	
КонецПроцедуры

&НаСервере
Процедура ОбновитьЗаказыНаСервере()
	СписокЗаказов.Параметры.УстановитьЗначениеПараметра("ПроцессОтбор", ПроцессОтбор);
	СписокЗаказов.Параметры.УстановитьЗначениеПараметра("ДатаНачалаОтбор", ДатаНачалаОтбор);
	СписокЗаказов.Параметры.УстановитьЗначениеПараметра("ДатаОкончанияОтбор", ДатаОкончанияОтбор);
	СписокЗаказов.Параметры.УстановитьЗначениеПараметра("МодельЦветОтбор", МодельЦветОтбор);
	СписокЗаказов.Параметры.УстановитьЗначениеПараметра("БригадаПошиваОтбор", БригадаПошиваОтбор);
	СписокЗаказов.Параметры.УстановитьЗначениеПараметра("БригадаРаскрояКружеваОтбор", БригадаРаскрояКружеваОтбор);
	Элементы.СписокЗаказов.Обновить();
КонецПроцедуры

#КонецОбласти

#Область ПроцедурыИФункцииСпискаЗаданийНаРаскрой

&НаКлиенте
Процедура ОбновитьЗаданияНаРаскройНаКлиенте()
	
	ВыбранныеЗаданияНаРаскрой.Очистить();
	ЗаданияНаРаскрой.Очистить();
	КоличествоВыбранныхЗаданий = ВыбранныеЗаданияНаРаскрой.Количество();
	СписокЗаданийНаРаскрой.Параметры.УстановитьЗначениеПараметра("ВыбранныеЗаданияНаРаскрой", ВыбранныеЗаданияНаРаскрой);
	ОбновитьЗаказНаПроизводствоОтбор(ЗаказНаПроизводствоОтбор);
	ОбновитьЗаданияНаРаскройНаСервере();
	УсловноеОформлениеФормы();
	
КонецПроцедуры

&НаСервере
Процедура ОбновитьЗаданияНаРаскройНаСервере()
	
	СписокЗаданийНаРаскрой.Параметры.УстановитьЗначениеПараметра("ЗаказНаПроизводствоОтбор", ЗаказНаПроизводствоОтбор);
	СписокЗаданийНаРаскрой.Параметры.УстановитьЗначениеПараметра("ПроцессОтбор", ПроцессОтбор);
	Элементы.СписокЗаданийНаРаскрой.Обновить();
	
КонецПроцедуры

#КонецОбласти

#Область ПроцедурыИФункцииСпискаКартРаскря

&НаКлиенте
Процедура ОбновитьКартыНаКлиенте()
	
	ВыбранныеКарты.Очистить();
	КартыРаскроя.Очистить();
	КоличествоВыбранныхКарт = ВыбранныеКарты.Количество();
	СписокКартРаскроя.Параметры.УстановитьЗначениеПараметра("ВыбранныеКарты", ВыбранныеКарты);
	СписокКартРаскроя.Параметры.УстановитьЗначениеПараметра("ВыбранныеЗаданияНаРаскрой", ВыбранныеЗаданияНаРаскрой);
	ОбновитьЗаказНаПроизводствоОтбор(ЗаказНаПроизводствоОтбор);
	ОбновитьКартыНаСервере();
	УсловноеОформлениеФормы();
	ЗаполнитьИтогоТрудоемкость();
	
КонецПроцедуры

&НаСервере
Процедура ОбновитьКартыНаСервере()
	
	СписокКартРаскроя.Параметры.УстановитьЗначениеПараметра("ЗаказНаПроизводствоОтбор", ЗаказНаПроизводствоОтбор);
	СписокКартРаскроя.Параметры.УстановитьЗначениеПараметра("СвойствоЦвет", 
		ПланыВидовХарактеристик.ДополнительныеРеквизитыИСведения.НайтиПоРеквизиту("Имя", "4д_ЦветМилавица(общий)"));
	СписокКартРаскроя.Параметры.УстановитьЗначениеПараметра("ПроцессОтбор", ПроцессОтбор);
	Элементы.СписокКартРаскроя.Обновить();
	
КонецПроцедуры

#КонецОбласти

#Область ПроцедурыИФункцииСпискаКонтрольныйЛист

&НаКлиенте
Процедура ОбновитьКонтрольныеЛистыНаКлиенте()
	
	ОбновитьКартыНаСервере();
	УсловноеОформлениеФормы();
	
КонецПроцедуры

&НаСервере
Процедура ОбновитьКонтрольныеЛистыНаСервере()
	
	СписокКонтрольныхЛистов.Параметры.УстановитьЗначениеПараметра("КартаРаскрояОтбор", КартыРаскроя);
	Элементы.СписокКонтрольныхЛистов.Обновить();
	
КонецПроцедуры

&НаКлиенте
Процедура УстановитьОтборКонтрольныеЛисты()
	
	СписокКонтрольныхЛистов.Отбор.Элементы.Очистить();
	
	Если СтатусКЛОтбор.Количество() <> 0 Тогда
		ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбора(СписокКонтрольныхЛистов.Отбор,
			"Статус",
			СтатусКЛОтбор,
			ВидСравненияКомпоновкиДанных.ВСписке);
	КонецЕсли;
	
	Если СостояниеКЛОтбор.Количество() <> 0 Тогда
		ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбора(СписокКонтрольныхЛистов.Отбор,
			"СостояниеКЛ",
			СостояниеКЛОтбор,
			ВидСравненияКомпоновкиДанных.ВСписке);
	КонецЕсли;
	
	Если ОперацииОтбор.Количество() <> 0 Тогда
		ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбора(СписокКонтрольныхЛистов.Отбор,
			"ОперацииСостояние",
			ОперацииОтбор,
			ВидСравненияКомпоновкиДанных.ВСписке);
	КонецЕсли;
	
	Элементы.СписокКонтрольныхЛистов.Обновить();
	
КонецПроцедуры

#КонецОбласти

&НаКлиенте
Процедура ОбработчикЗаполнитьПоМаршрутнойКарте(МаршрутнаяКартаИсточник, ДополнительныеПараметры) Экспорт
	
	Если МаршрутнаяКартаИсточник = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	ТекстВопроса = НСтр("ru = 'В ведущих Этапах производства по выбранные картам будут заменены маршрутные карты.
			|Заменить?'");
	СписокКнопок = Новый СписокЗначений;
	СписокКнопок.Добавить(КодВозвратаДиалога.Да, НСтр("ru = 'Заменить'"));
	СписокКнопок.Добавить(КодВозвратаДиалога.Отмена);
	ОписаниеОповещения = Новый ОписаниеОповещения("ВопросЗаполнитьПоМаршрутнойКарте", ЭтаФорма, МаршрутнаяКартаИсточник);
	ПоказатьВопрос(ОписаниеОповещения, ТекстВопроса, СписокКнопок, , , НСтр("ru = 'Замена маршрутной карты'"));
	
КонецПроцедуры

&НаКлиенте
Процедура ВопросЗаполнитьПоМаршрутнойКарте(РезультатВопроса, МаршрутнаяКартаИсточник) Экспорт
	
	Если РезультатВопроса <> КодВозвратаДиалога.Да Тогда
		Возврат;
	КонецЕсли;
	
	ЗаполнитьПоМаршрутнойКарте(МаршрутнаяКартаИсточник);
	
	ОповеститьОбИзменении(Тип("СправочникСсылка.ТехнологическиеОперации"));
	
	Оповестить("Запись_ТехнологическиеОперации");
	
	ОбновитьКартыНаКлиенте();
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьПоМаршрутнойКарте(Источник)
	
	Для каждого КартаСтрока Из МассивКарт Цикл
		КартаСсылка = КартаСтрока.Значение;
		СтатусЭтапаВедущего = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(КартаСсылка.ЭтапВедущий, "Статус");
		Если ЗначениеЗаполнено(КартаСсылка.ЭтапВедущий)
			И ОбщегоНазначения.ЗначениеРеквизитаОбъекта(КартаСсылка.ЭтапВедущий, "МаршрутнаяКарта") <> Источник
			И СтатусЭтапаВедущего <> Перечисления.СтатусыЭтаповПроизводства2_2.Завершен Тогда
			
			НачатьТранзакцию();
			Попытка
				ОбъектЭтап = КартаСсылка.ЭтапВедущий.ПолучитьОбъект();
				ОбъектЭтап.МаршрутнаяКарта = Источник;
				ОбъектЭтап.Записать(РежимЗаписиДокумента.Проведение);
				Обработки.Чд_АРМРабочееМестоМастераРЦ.ЗаполнитьМКПоВедущемуЭтапу(КартаСсылка);
				ЗафиксироватьТранзакцию();
				
			Исключение
				ОтменитьТранзакцию();
				
				ЗаписьЖурналаРегистрации(
					НСтр("ru = 'Изменение маршрутной карты в этапе карты раскроя'", ОбщегоНазначенияКлиентСервер.КодОсновногоЯзыка()),
					УровеньЖурналаРегистрации.Ошибка, , ,
					ПодробноеПредставлениеОшибки(ИнформацияОбОшибке()));
				
				ВызватьИсключение;
			КонецПопытки;
		Иначе
			ТекстСообщения = НСтр("ru='Нельзя изменять маршрутную карту в %1!'");
			ТекстСообщения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ТекстСообщения, КартаСсылка.ЭтапВедущий);
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, КартаСсылка.ЭтапВедущий);
		КонецЕсли;
	КонецЦикла;
	
КонецПроцедуры

&НаКлиенте
Процедура ДвиженияДокумента(Команда)
	
	Если Элементы.СписокКонтрольныхЛистов.ТекущиеДанные <> Неопределено Тогда
		
		ПараметрыФормы = Новый Структура;
		ПараметрыФормы.Вставить("СформироватьПриОткрытии", Истина);
		ПараметрыФормы.Вставить("ДокументВладелец", Элементы.СписокКонтрольныхЛистов.ТекущиеДанные.КонтрольныйЛист);
		ПараметрыФормы.Вставить("ПараметрКоманды", Элементы.СписокКонтрольныхЛистов.ТекущиеДанные.КонтрольныйЛист);
		
		ОткрытьФорму("Отчет.ДвиженияДокумента.Форма", ПараметрыФормы, ЭтаФорма);
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ПеревестиКартуВСтатус(КартаСсылка, Статус)
	
	КартаОбъект = КартаСсылка.ПолучитьОбъект();
	КартаОбъект.УстановитьСтатус(Статус, Неопределено);
	Попытка
		КартаОбъект.Записать(РежимЗаписиДокумента.Проведение);
	Исключение
		ТекстСообщения = НСтр("ru='%1 не удалось перевести в статус ""Закрыта"".'");
		ТекстСообщения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ТекстСообщения, КартаСсылка);
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, КартаСсылка);
		
		ЗаписьЖурналаРегистрации(ТекстСообщения,
			УровеньЖурналаРегистрации.Ошибка
			,
			,
			ПодробноеПредставлениеОшибки(ИнформацияОбОшибке()));
	КонецПопытки;
	
КонецПроцедуры

&НаКлиенте
Процедура СформироватьТрудоемкостьРаскройныхПроцессов(Команда)
	
	ПараметрыФормы = Новый Структура;
	ПараметрыФормы.Вставить("СформироватьПриОткрытии", Истина);
	ПараметрыФормы.Вставить("Отбор", Новый Структура);
	ОткрытьФорму("Отчет.Чд_ТрудоемкостьРаскройныхПроцессов.Форма", ПараметрыФормы, ЭтаФорма);
	
КонецПроцедуры

#КонецОбласти