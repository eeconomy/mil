﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ПрограммныйИнтерфейс

// Записывает данные документы в регистр сведений
//
// Параметры:
//  Ссылка					 - ДокументСсылка - 
//  ДополнительныеСвойства	 - Структура - содержит по ключу ТаблицыДляДвижений структуру
//  									имеющую ключ ТаблицаЧд_ВидыРаботПодготовительногоЦеха (ТаблицаЗначений)
//  Отказ					 - Булево - 
//
Процедура ЗаписатьДанныеДокумента(ДополнительныеСвойства, Движения, Отказ) Экспорт
	
	Таблица = ДополнительныеСвойства.ТаблицыДляДвижений.ТаблицаЧд_ВидыРаботПодготовительногоЦеха;
	
	Если Отказ ИЛИ Таблица.Количество() = 0 Тогда
		Возврат;
	КонецЕсли;

	Движения.Чд_ВидыРаботПодготовительногоЦеха.Записывать = Истина;
	Движения.Чд_ВидыРаботПодготовительногоЦеха.Загрузить(Таблица);	
	
КонецПроцедуры

#КонецОбласти

#КонецЕсли
