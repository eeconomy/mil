﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ОбработчикиСобытий

Процедура ПередЗаписью(Отказ, Замещение)
	
	Если ОбменДанными.Загрузка Тогда
		Возврат;
	КонецЕсли;

	ОбновлениеИнформационнойБазы.ПроверитьОбъектОбработан(ЭтотОбъект);

КонецПроцедуры

Процедура ПриЗаписи(Отказ, Замещение)
	
	Если Отказ ИЛИ ОбменДанными.Загрузка Тогда
		Возврат;
	КонецЕсли;
	
	Запрос = Новый Запрос;
	Запрос.МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	ПорядокОтраженияТМЦВЭксплуатации.Организация,
	|	ПорядокОтраженияТМЦВЭксплуатации.КатегорияЭксплуатации,
	|	ПорядокОтраженияТМЦВЭксплуатации.КатегорияЭксплуатации = ЗНАЧЕНИЕ(Справочник.КатегорииЭксплуатации.ПустаяСсылка)
	|		И ПорядокОтраженияТМЦВЭксплуатации.Организация = ЗНАЧЕНИЕ(Справочник.Организации.ПустаяСсылка) КАК ЭтоОбщаяНастройка,
	|	ПорядокОтраженияТМЦВЭксплуатации.СчетУчета,
	|	ПорядокОтраженияТМЦВЭксплуатации.СчетЗабалансовогоУчета
	|ПОМЕСТИТЬ ТаблицаНовыхДанных
	|ИЗ
	|	&ТаблицаНовыхДанных КАК ПорядокОтраженияТМЦВЭксплуатации
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ТаблицаНовыхДанных.Организация,
	|	НЕОПРЕДЕЛЕНО КАК МестоУчета,
	|	ТаблицаНовыхДанных.КатегорияЭксплуатации КАК АналитикаУчета,
	|	ТаблицаНовыхДанных.ЭтоОбщаяНастройка КАК ЭтоОбщаяНастройка,
	|	ЗНАЧЕНИЕ(Перечисление.ВидыСчетовРеглУчета.ТМЦВЭксплуатации) КАК ВидСчета
	|ПОМЕСТИТЬ ДанныеЗаполненныхСчетовРегистра
	|ИЗ
	|	ТаблицаНовыхДанных КАК ТаблицаНовыхДанных
	|ГДЕ
	|	ТаблицаНовыхДанных.СчетУчета <> ЗНАЧЕНИЕ(ПланСчетов.Хозрасчетный.ПустаяСсылка)
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	ТаблицаНовыхДанных.Организация,
	|	НЕОПРЕДЕЛЕНО КАК МестоУчета,
	|	ТаблицаНовыхДанных.КатегорияЭксплуатации,
	|	ТаблицаНовыхДанных.ЭтоОбщаяНастройка КАК ЭтоОбщаяНастройка,
	|	ЗНАЧЕНИЕ(Перечисление.ВидыСчетовРеглУчета.ТМЦВЭксплуатацииЗаБалансом)
	|ИЗ
	|	ТаблицаНовыхДанных КАК ТаблицаНовыхДанных
	|ГДЕ
	|	ТаблицаНовыхДанных.СчетЗабалансовогоУчета <> ЗНАЧЕНИЕ(ПланСчетов.Хозрасчетный.ПустаяСсылка)";
	Запрос.УстановитьПараметр("ТаблицаНовыхДанных", ЭтотОбъект.Выгрузить());
	Запрос.Выполнить();
	РегистрыСведений.СчетаРеглУчетаТребующиеНастройки.ОчиститьПриЗаписиРегистра(Запрос.МенеджерВременныхТаблиц);
	
КонецПроцедуры

#КонецОбласти

#КонецЕсли
