﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ПрограммныйИнтерфейс

// Рассчитывает и записывает состояния заказов на производство.
//
// Параметры:
//  Заказы - ДокументСсылка.ЗаказНаПроизводство2_2, Массив - набор заказов.
//
Процедура ОтразитьСостояние(Заказы) Экспорт
	
	МассивСсылок = УправлениеПроизводствомКлиентСервер.МассивЗначений(Заказы);
	Если МассивСсылок.Количество() = 0 Тогда
		Возврат;
	КонецЕсли;
	
	Выборка = ВыборкаИзменений(МассивСсылок);
	ЗаписатьСостояния(Выборка);
	
КонецПроцедуры

// Очищает состояния заказов на производство.
//
// Параметры:
//  Заказы - ДокументСсылка.ЗаказНаПроизводство2_2, Массив - набор заказов.
//
Процедура ОчиститьСостояние(Заказы) Экспорт
	
	МассивСсылок = УправлениеПроизводствомКлиентСервер.МассивЗначений(Заказы);
	Если МассивСсылок.Количество() = 0 Тогда
		Возврат;
	КонецЕсли;
	
	Для Каждого Ссылка Из МассивСсылок Цикл
		НаборЗаписей = РегистрыСведений.Чд_СостояниеЗаказовНаПроизводствоПоКартаРаскроя.СоздатьНаборЗаписей();
		НаборЗаписей.Отбор.Заказ.Установить(Ссылка);
		Попытка
			НаборЗаписей.Записать();	
		Исключение
			ЗаписьЖурналаРегистрации("Данные.Запись", УровеньЖурналаРегистрации.Ошибка, , ,
							ОбработкаОшибок.ПодробноеПредставлениеОшибки(ИнформацияОбОшибке()));
			ВызватьИсключение;
		КонецПопытки;
	КонецЦикла;
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область Прочее

Функция ТекстЗапросаВТСостоянияЭтаповПоКартамРаскроя()
	
	ТекстЗапроса =
		"ВЫБРАТЬ
		|	Таблица.Заказ КАК Ссылка,
		|	ВЫБОР
		|		КОГДА Таблица.КоличествоКарт = 0
		|				И Таблица.КоличествоЭтапов <> 0
		|			ТОГДА ЗНАЧЕНИЕ(Перечисление.Чд_СостоянияЗаказаПроизводстаПоКартамРаскроя.НеНачато)
		|		КОГДА Таблица.СтатусВРазработке <> 0
		|				И Таблица.СтатусДействует = 0
		|			ТОГДА ЗНАЧЕНИЕ(Перечисление.Чд_СостоянияЗаказаПроизводстаПоКартамРаскроя.ВРаботе)
		|		КОГДА Таблица.СтатусДействует <> 0
		|			ТОГДА ЗНАЧЕНИЕ(Перечисление.Чд_СостоянияЗаказаПроизводстаПоКартамРаскроя.Действует)
		|		КОГДА Таблица.КоличествоЭтапов = Таблица.СтатусВЗакрыта
		|			ТОГДА ЗНАЧЕНИЕ(Перечисление.Чд_СостоянияЗаказаПроизводстаПоКартамРаскроя.Закрыта)
		|		КОГДА Таблица.КоличествоЭтапов > Таблица.КоличествоКарт
		|				И Таблица.СтатусВЗакрыта <> 0
		|				И Таблица.СтатусДействует = 0
		|				И Таблица.СтатусВРазработке = 0
		|			ТОГДА ЗНАЧЕНИЕ(Перечисление.Чд_СостоянияЗаказаПроизводстаПоКартамРаскроя.ВРаботе)
		|		ИНАЧЕ ЗНАЧЕНИЕ(Перечисление.Чд_СостоянияЗаказаПроизводстаПоКартамРаскроя.ПустаяСсылка)
		|	КОНЕЦ КАК СостояниеКартРаскроя
		|ПОМЕСТИТЬ ВТСостоянияЭтапов
		|ИЗ
		|	(ВЫБРАТЬ
		|		ЭтапПроизводства2_2.Распоряжение КАК Заказ,
		|		СУММА(ВЫБОР
		|				КОГДА ЭтапПроизводства2_2.Чд_КартаРаскроя = ЗНАЧЕНИЕ(Документ.Чд_КартаРаскроя.ПУстаяСсылка)
		|					ТОГДА 0
		|				ИНАЧЕ 1
		|			КОНЕЦ) КАК КоличествоКарт,
		|		СУММА(ВЫБОР
		|				КОГДА ЭтапПроизводства2_2.Чд_КартаРаскроя.Статус = ЗНАЧЕНИЕ(Перечисление.Чд_СтатусыКартРаскроя.Действует)
		|					ТОГДА 1
		|				ИНАЧЕ 0
		|			КОНЕЦ) КАК СтатусДействует,
		|		СУММА(ВЫБОР
		|				КОГДА ЭтапПроизводства2_2.Чд_КартаРаскроя.Статус = ЗНАЧЕНИЕ(Перечисление.Чд_СтатусыКартРаскроя.ВРазработке)
		|					ТОГДА 1
		|				ИНАЧЕ 0
		|			КОНЕЦ) КАК СтатусВРазработке,
		|		СУММА(ВЫБОР
		|				КОГДА ЭтапПроизводства2_2.Чд_КартаРаскроя.Статус = ЗНАЧЕНИЕ(Перечисление.Чд_СтатусыКартРаскроя.Закрыта)
		|					ТОГДА 1
		|				ИНАЧЕ 0
		|			КОНЕЦ) КАК СтатусВЗакрыта,
		|		КОЛИЧЕСТВО(РАЗЛИЧНЫЕ ЭтапПроизводства2_2.Ссылка) КАК КоличествоЭтапов
		|	ИЗ
		|		Справочник.СтруктураПредприятия.Чд_ПроцессыЗаданийНаРаскрой КАК СтруктураПредприятияЧд_ПроцессыЗаданийНаРаскрой
		|			ЛЕВОЕ СОЕДИНЕНИЕ Документ.ЭтапПроизводства2_2 КАК ЭтапПроизводства2_2
		|			ПО СтруктураПредприятияЧд_ПроцессыЗаданийНаРаскрой.Ссылка = ЭтапПроизводства2_2.Подразделение
		|				И СтруктураПредприятияЧд_ПроцессыЗаданийНаРаскрой.КодПроцесса = ЭтапПроизводства2_2.Чд_КодПроцесса
		|	ГДЕ
		|		ЭтапПроизводства2_2.Проведен
		|		И ЭтапПроизводства2_2.Распоряжение В(&МассивСсылок)
		|		И ЭтапПроизводства2_2.Распоряжение.Статус = &СтатусКПроизводству
		|	
		|	СГРУППИРОВАТЬ ПО
		|		ЭтапПроизводства2_2.Распоряжение) КАК Таблица";
	
	Возврат ТекстЗапроса;
	
КонецФункции

Функция ТекстЗапросаИзмененныеСостояния()
	
	ТекстЗапроса =
		"ВЫБРАТЬ
		|	РеквизитыДокумента.Ссылка КАК Заказ,
		|	ВТСостоянияЭтапов.СостояниеКартРаскроя КАК СостояниеЭтаповКартыРаскроя,
		|	ВЫБОР
		|		КОГДА РеквизитыДокумента.Проведен
		|				И РеквизитыДокумента.Статус = &СтатусКПроизводству
		|			ТОГДА ИСТИНА
		|		ИНАЧЕ ЛОЖЬ
		|	КОНЕЦ КАК АктивныйСтатус
		|ИЗ
		|	Документ.ЗаказНаПроизводство2_2 КАК РеквизитыДокумента
		|		ЛЕВОЕ СОЕДИНЕНИЕ ВТСостоянияЭтапов КАК ВТСостоянияЭтапов
		|		ПО (ВТСостоянияЭтапов.Ссылка = РеквизитыДокумента.Ссылка)
		|ГДЕ
		|	РеквизитыДокумента.Ссылка В(&МассивСсылок)";
	
	Возврат ТекстЗапроса;
	
КонецФункции

Функция ВыборкаИзменений(МассивСсылок)
	
	ТекстыЗапроса = Новый Массив();
	
	ТекстЗапроса = ТекстЗапросаВТСостоянияЭтаповПоКартамРаскроя();
	ТекстыЗапроса.Добавить(ТекстЗапроса);
	
	ТекстЗапроса = ТекстЗапросаИзмененныеСостояния();
	ТекстыЗапроса.Добавить(ТекстЗапроса);
	
	ТекстЗапроса = УправлениеПроизводством.ОбъединитьТекстыЗапросаВПакет(ТекстыЗапроса);
	Запрос = Новый Запрос(ТекстЗапроса);
	
	Запрос.УстановитьПараметр("МассивСсылок", МассивСсылок);
	
	Запрос.УстановитьПараметр("СтатусКПроизводству", Перечисления.СтатусыЗаказовНаПроизводство2_2.КПроизводству);
	
	УстановитьПривилегированныйРежим(Истина);
	Возврат Запрос.Выполнить().Выбрать();
	
КонецФункции

Процедура ЗаписатьСостояния(Выборка)
	
	УстановитьПривилегированныйРежим(Истина);
	
	Пока Выборка.Следующий() Цикл
		
		Блокировка = Новый БлокировкаДанных;
		ЭлементБлокировки = Блокировка.Добавить("РегистрСведений.Чд_СостояниеЗаказовНаПроизводствоПоКартаРаскроя");
		ЭлементБлокировки.Режим = РежимБлокировкиДанных.Исключительный;
		ЭлементБлокировки.УстановитьЗначение("Заказ", Выборка.Заказ);
		Блокировка.Заблокировать();
		
		Набор = РегистрыСведений.Чд_СостояниеЗаказовНаПроизводствоПоКартаРаскроя.СоздатьНаборЗаписей();
		Набор.Отбор.Заказ.Установить(Выборка.Заказ);
		
		ДобавитьЗаписьВНабор(Выборка, Набор);
		
		Попытка
			Набор.Записать();
		Исключение
			ЗаписьЖурналаРегистрации("Данные.Изменение", УровеньЖурналаРегистрации.Ошибка, , Выборка.Заказ,
				ОбработкаОшибок.ПодробноеПредставлениеОшибки(ИнформацияОбОшибке()));
			ВызватьИсключение;
		КонецПопытки;
		
		Если Выборка.СостояниеЭтаповКартыРаскроя = Перечисления.Чд_СостоянияЗаказаПроизводстаПоКартамРаскроя.НеНачато Тогда
			
			Блокировка = Новый БлокировкаДанных;
			ЭлементБлокировки = Блокировка.Добавить("РегистрСведений.Чд_РасчетныеДанныеПланГрафика");
			ЭлементБлокировки.Режим = РежимБлокировкиДанных.Исключительный;
			ЭлементБлокировки.УстановитьЗначение("ЗаказНаПроизводство", Выборка.Заказ);
			Блокировка.Заблокировать();
			
			НаборЗаписей = РегистрыСведений.Чд_РасчетныеДанныеПланГрафика.СоздатьНаборЗаписей();
			НаборЗаписей.Отбор.ЗаказНаПроизводство.Установить(Выборка.Заказ);
			НаборЗаписей.Прочитать();
			Для Каждого Запись Из НаборЗаписей Цикл
				Если НЕ ЗначениеЗаполнено(Запись.СтатусКартКрояЗаказа) Тогда
					Запись.СтатусКартКрояЗаказа = "Не начато";
				КонецЕсли;
			КонецЦикла;
			
			Попытка
				НаборЗаписей.Записать();
			Исключение
				ЗаписьЖурналаРегистрации("Данные.Изменение", УровеньЖурналаРегистрации.Ошибка, , Выборка.Заказ,
					ОбработкаОшибок.ПодробноеПредставлениеОшибки(ИнформацияОбОшибке()));
				ВызватьИсключение;
			КонецПопытки;
		КонецЕсли;
		
	КонецЦикла;
	
КонецПроцедуры

Процедура ДобавитьЗаписьВНабор(Выборка, Набор)
	
	Если Выборка.АктивныйСтатус Тогда
		ЗаполнитьЗначенияСвойств(Набор.Добавить(), Выборка);
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#КонецЕсли