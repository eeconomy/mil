﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область СлужебныеПроцедурыИФункции

// Описание:
//  для записи набора движений из модуля объекта документа
Процедура ОтразитьНовыеКоэффициенты(ДополнительныеСвойства, Движения, Отказ) Экспорт
	
	Таблица = ДополнительныеСвойства.ТаблицыДляДвижений.ТаблицаИсторияКоэффициентовПоПаспорту;
	
	Если Отказ ИЛИ Таблица.Количество() = 0 Тогда
		Возврат;
	КонецЕсли;
	
	Движения.Чд_ИсторияКоэффициентовПоПаспорту.Записывать = Истина;
	Движения.Чд_ИсторияКоэффициентовПоПаспорту.Загрузить(Таблица);
	
КонецПроцедуры

// Описание:
// возвращает актуальный коэффициент пересчета по паспорту
// при единице измерения для номенклатуры
//  м2: Площадь(по учету) / Длина (фактическая по паспорту)
//  м : Длина(по учету) / Площадь(расчетная по паспорту)
//
// Параметры
//  - Серия <СправочникСсылка.СерииНоменклатуры>
//  - РегистраторСсылка <ДокументСсылка> - при необходимости получать срезом последних без учета движений регистратора
//
// Возвращаемое значение:
//  - Коэффициент <Число>
Функция ПолучитьКоэффициентПоПаспорту(Серия, РегистраторСсылка = Неопределено) Экспорт
	
	Запрос = Новый Запрос;
	Запрос.Текст =
		"ВЫБРАТЬ
		|	Чд_ИсторияКоэффициентовПоПаспортуСрезПоследних.Коэффициент КАК Коэффициент
		|ИЗ
		|	РегистрСведений.Чд_ИсторияКоэффициентовПоПаспорту.СрезПоследних(&Граница, Серия = &Серия) КАК Чд_ИсторияКоэффициентовПоПаспортуСрезПоследних";
	
	Запрос.УстановитьПараметр("Серия", Серия);
	Если РегистраторСсылка <> Неопределено
		И ЗначениеЗаполнено(РегистраторСсылка) Тогда
		
		Запрос.УстановитьПараметр("Граница", Новый Граница(Новый МоментВремени(РегистраторСсылка.Дата, РегистраторСсылка), ВидГраницы.Исключая));
	Иначе
		Запрос.Текст = СтрЗаменить(Запрос.Текст, "&Граница", "");
	КонецЕсли;
	
	РезультатЗапроса = Запрос.Выполнить();
	
	Выборка = РезультатЗапроса.Выбрать();
	
	Если Выборка.Следующий() Тогда
		Коэффициент = Выборка.Коэффициент;
	Иначе
		Коэффициент = 0;
	КонецЕсли;
	
	Возврат Коэффициент;
	
КонецФункции

#КонецОбласти

#КонецЕсли