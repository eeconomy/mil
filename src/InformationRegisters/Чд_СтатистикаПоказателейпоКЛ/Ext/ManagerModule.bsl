﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда
	
#Область ПрограммныйИнтерфейс

// Описание:
//  для записи набора движений из модуля объекта документа
Процедура ЗаписатьСтатистикуПоказателейПоКЛ(ДополнительныеСвойства, Движения, Отказ) Экспорт
		
	Таблица = ДополнительныеСвойства.ТаблицыДляДвижений.ТаблицаСтатистикаПоказателейПоКЛ;
	
	Если Отказ ИЛИ НЕ Таблица.Количество() Тогда
		Возврат;
	КонецЕсли;
	
	КоллекцияИзделия = Неопределено;
	КоллекцияРодитель = Неопределено; 
	
	Для каждого Строка Из Таблица Цикл
		
		Если КоллекцияИзделия = Неопределено ИЛИ КоллекцияИзделия <> Строка.КоллекцияИзделия Тогда
			КоллекцияИзделия = Строка.КоллекцияИзделия;
			Строка.КоллекцияИзделия = ПолучитьКоллекциюИзделия(Строка.КоллекцияИзделия);
			КоллекцияРодитель = Строка.КоллекцияИзделия;
		Иначе
			Строка.КоллекцияИзделия = КоллекцияРодитель;
		КонецЕсли;
	КонецЦикла;
	
	Движения.Чд_СтатистикаПоказателейпоКЛ.Записывать = Истина;    	
	Движения.Чд_СтатистикаПоказателейпоКЛ.Загрузить(Таблица);
		
КонецПроцедуры
	
Функция ПолучитьКоллекциюИзделия(КоллекцияИзделия) Экспорт
		
	Запрос = Новый Запрос;
	Запрос.Текст = 
		"ВЫБРАТЬ
		|	КоллекцииНоменклатуры.Ссылка КАК КоллекцияИзделия
		|ИЗ
		|	Справочник.КоллекцииНоменклатуры КАК КоллекцииНоменклатуры
		|ГДЕ
		|	КоллекцииНоменклатуры.Родитель = &ПустойРодитель";
	
	Запрос.УстановитьПараметр("ПустойРодитель", Справочники.КоллекцииНоменклатуры.ПустаяСсылка());
	
	Выборка = Запрос.Выполнить().Выбрать();
	Пока Выборка.Следующий() Цикл
		Запрос = Новый Запрос;
		Запрос.Текст = 
		"ВЫБРАТЬ
		|	КоллекцииНоменклатуры.Ссылка КАК КоллекцияИзделия
		|ИЗ
		|	Справочник.КоллекцииНоменклатуры КАК КоллекцииНоменклатуры
		|ГДЕ
		|	КоллекцииНоменклатуры.Ссылка В ИЕРАРХИИ (&Родитель)
		|	И КоллекцииНоменклатуры.Ссылка = &КоллекцииНоменклатуры";
		
		Запрос.УстановитьПараметр("КоллекцииНоменклатуры", КоллекцияИзделия);
		Запрос.УстановитьПараметр("Родитель", Выборка.КоллекцияИзделия);
		
		Выборка1 = Запрос.Выполнить().Выбрать();
		Если Выборка1.Следующий() Тогда
			Возврат Выборка.КоллекцияИзделия;
		КонецЕсли;  			
	КонецЦикла;
	
	Возврат Выборка.КоллекцияИзделия; 		
		
КонецФункции
		
#КонецОбласти
	
#КонецЕсли
