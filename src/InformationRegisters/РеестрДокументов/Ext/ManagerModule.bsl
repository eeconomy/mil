﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ПрограммныйИнтерфейс


// Записывает данные документы в регистр сведений ДанныеВнутреннихДокументов
//
// Параметры:
//  Ссылка					 - ДокументСсылка - 
//  ДополнительныеСвойства	 - Структура - содержит по ключу ТаблицыДляДвижений структуру
//  										имеющую ключ ТаблицаРеестрДокументов (ТаблицаЗначений)
//  Отказ					 - Булево - 
//
Процедура ЗаписатьДанныеДокумента(Ссылка, ДополнительныеСвойства, Отказ, ЭтоОбновлениеИБ = Ложь) Экспорт
	
	Если Отказ Тогда
		Возврат;
	КонецЕсли;
	
	ТаблицаРеестрДокументов = ДополнительныеСвойства.ТаблицыДляДвижений.ТаблицаРеестрДокументов;
	
	УстановитьПривилегированныйРежим(Истина);
	
	Набор = РегистрыСведений.РеестрДокументов.СоздатьНаборЗаписей();
	Набор.Отбор.Ссылка.Установить(Ссылка);
	Набор.ЗагрузитьСОбработкой(ТаблицаРеестрДокументов);
	Если ЭтоОбновлениеИБ Тогда
		ОбновлениеИнформационнойБазы.ЗаписатьДанные(Набор);
	Иначе
		Набор.Записать();
	КонецЕсли;
	
	УстановитьПривилегированныйРежим(Ложь);
	
	
КонецПроцедуры

// Записывает в регистр данные по переданному разделителю записи.
//
// Параметры:
//  РазделительЗаписи		 - ОпределяемыйТип.РазделительЗаписиРеестраДокументов - измерение, по которому необходимо выполнить запись. 
//  ДополнительныеСвойства	 - Структура - содержит по ключу ТаблицыДляДвижений структуру
//  	имеющую ключ ТаблицаРеестрДокументов (ТаблицаЗначений)
// ЗамещатьЗаписи			 - Булево - определяет режим замещения существующих записей разделителя. Истина - перед записью существующие
//		записи будут удалены. Ложь - записи будут дописаны к уже существующим в информационной базе записям.
//
Процедура ЗаписатьДанныеРазделителя(РазделительЗаписи, ДополнительныеСвойства, ЗамещатьЗаписи = Истина) Экспорт
	
	ТаблицаРеестрДокументов = ДополнительныеСвойства.ТаблицыДляДвижений.ТаблицаРеестрДокументов;
	
	УстановитьПривилегированныйРежим(Истина);
	
	Набор = РегистрыСведений.РеестрДокументов.СоздатьНаборЗаписей();
	Набор.Отбор.РазделительЗаписи.Установить(РазделительЗаписи);
	Набор.ЗагрузитьСОбработкой(ТаблицаРеестрДокументов);
	Набор.Записать(ЗамещатьЗаписи);
	
	УстановитьПривилегированныйРежим(Ложь);
	
КонецПроцедуры


// Инициализирует и записывает данные документы в регистр сведений ДанныеВнутреннихДокументов.
//
// Параметры:
//  Ссылка				 - 	 - 
//
Процедура ИнициализироватьИЗаписатьДанныеДокумента(Ссылка, ДополнительныеСвойства, Отказ, ЭтоОбновлениеИБ = Ложь) Экспорт
	
	Если Отказ Тогда
		Возврат;
	КонецЕсли;
	
	УстановитьПривилегированныйРежим(Истина);
	
	ПроведениеСерверУТ.ИнициализироватьДополнительныеСвойстваДляПроведения(Ссылка, ДополнительныеСвойства);
	МенеджерДокумента = ОбщегоНазначения.МенеджерОбъектаПоСсылке(Ссылка);
	МенеджерДокумента.ИнициализироватьДанныеДокумента(Ссылка, ДополнительныеСвойства, "РеестрДокументов");
	РегистрыСведений.РеестрДокументов.ЗаписатьДанныеДокумента(Ссылка, ДополнительныеСвойства, Отказ, ЭтоОбновлениеИБ);
	
КонецПроцедуры

// Возвращает признак, того отражаются ли данные полученного объекта в реестре сведений или нет.
//
// Параметры:
//	ПроверяемыйОбъект - ДокументОбъект, ДокументСсылка, ДанныеФормыСтруктура, ОбъектМетаданных
//
// Возвращаемое значение
//	Булево - Истина, если данные полученного объекта, отражаются в реестре сведений.
//
Функция ОбъектВключенВСоставДанныхРеестра(ПроверяемыйОбъект) Экспорт
	
	ТипЗначения = ТипЗнч(ПроверяемыйОбъект);
	Если ТипЗначения = Тип("ДанныеФормыСтруктура") Тогда
		Если Не ПроверяемыйОбъект.Свойство("Ссылка") Тогда
			Возврат Ложь;
		КонецЕсли;
		ТипСсылки = ТипЗнч(ПроверяемыйОбъект.Ссылка);
	Иначе	
		Если ТипЗначения = Тип("ОбъектМетаданных") Тогда
			МетаданныеЗначения = ПроверяемыйОбъект;
		Иначе
			МетаданныеЗначения = Метаданные.НайтиПоТипу(ТипЗначения);
			
			Если МетаданныеЗначения = Неопределено Тогда
				Возврат Ложь;
			КонецЕсли;
		КонецЕсли;
		
		Если Не ОбщегоНазначения.ЭтоДокумент(МетаданныеЗначения) Тогда
			Возврат Ложь;
		КонецЕсли;
		
		ТипСсылки = ТипЗнч(МетаданныеЗначения.СтандартныеРеквизиты.Ссылка.Тип.ПривестиЗначение());
	КонецЕсли;

	МетаданныеРегистра	= Метаданные.НайтиПоПолномуИмени("РегистрСведений.РеестрДокументов");
	Возврат МетаданныеРегистра.Измерения.Ссылка.Тип.СодержитТип(ТипСсылки);
	
КонецФункции

// Инициализирует и записывает данные документов, полученных объектов метаданных, в регистр сведений.
//
// Параметры:
//	ОбъектыМетаданных - Соответствие - объекты метаданных:
//		* Ключ		- ОбъектМетаданных: Документ	- объект метаданных документа.
//		* Значение	- Неопределено					- пустое значение.
//
Процедура ОтразитьДанныеДокументовВРеестре(ОбъектыМетаданных) Экспорт
	
	ИменаОбъектов		= Новый Массив;
	СсылкиДокументов	= Новый Массив;
	
	Для Каждого ЭлементДанных Из ОбъектыМетаданных Цикл
		ОбъектДанных		= ЭлементДанных.Ключ;
		ПолноеИмяОбъекта	= ОбъектДанных.ПолноеИмя();
		
		ИменаОбъектов.Добавить(ПолноеИмяОбъекта);
	КонецЦикла;
	
	НеиспользуемыеПоля = Новый Массив;
	НеиспользуемыеПоля.Добавить("Дополнительно");
	НеиспользуемыеПоля.Добавить("НомерПервичногоДокумента");
	
	Для Каждого ПолноеИмяОбъекта Из ИменаОбъектов Цикл
		
		ИмяДокумента = СтрРазделить(ПолноеИмяОбъекта, ".")[1];
		РезультатАдаптацииЗапроса = Документы[ИмяДокумента].АдаптированныйТекстЗапросаДвиженийПоРегистру("РеестрДокументов");
		Регистраторы = ОбновлениеИнформационнойБазыУТ.ДанныеНезависимогоРегистраДляПерепроведения(
							РезультатАдаптацииЗапроса,
							"РегистрСведений.РеестрДокументов",
							ПолноеИмяОбъекта,
							НеиспользуемыеПоля);
		
		ОбщегоНазначенияКлиентСервер.ДополнитьМассив(СсылкиДокументов, Регистраторы.ВыгрузитьКолонку("Ссылка"));
		
	КонецЦикла;
	
	Для Каждого Ссылка Из СсылкиДокументов Цикл
		ДополнительныеСвойства = Новый Структура;
		
		РегистрыСведений.РеестрДокументов.ИнициализироватьИЗаписатьДанныеДокумента(Ссылка, ДополнительныеСвойства, Ложь);
	КонецЦикла;
	
КонецПроцедуры

#Область ДляВызоваИзДругихПодсистем

// СтандартныеПодсистемы.УправлениеДоступом

// См. УправлениеДоступомПереопределяемый.ПриЗаполненииСписковСОграничениемДоступа.
Процедура ПриЗаполненииОграниченияДоступа(Ограничение) Экспорт

	Ограничение.Текст =
	"РазрешитьЧтение
	|ГДЕ
	|	ЧтениеОбъектаРазрешено(Ссылка)
	|;
	|РазрешитьИзменениеЕслиРазрешеноЧтение
	|ГДЕ
	|	ИзменениеОбъектаРазрешено(Ссылка)";

КонецПроцедуры

// Конец СтандартныеПодсистемы.УправлениеДоступом

#КонецОбласти

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область ОбновлениеИнформационнойБазы

Процедура ЗарегистрироватьДанныеКОбработкеДляПереходаНаНовуюВерсию(Параметры) Экспорт 

	ПараметрыВыборки = Параметры.ПараметрыВыборки;
	ПараметрыВыборки.ДополнительныеИсточникиДанных.Вставить("Ссылка");
	ПараметрыВыборки.ПолныеИменаРегистров = "РегистрСведений.РеестрДокументов";
	ПараметрыВыборки.ПоляУпорядочиванияПриРаботеПользователей.Добавить("Ссылка.Дата УБЫВ");
	ПараметрыВыборки.ПоляУпорядочиванияПриОбработкеДанных.Добавить("Ссылка");
	ПараметрыВыборки.СпособВыборки = ОбновлениеИнформационнойБазы.СпособВыборкиИзмеренияНезависимогоРегистраСведений();
	
	ДополнительныеПараметры = ОбновлениеИнформационнойБазы.ДополнительныеПараметрыОтметкиОбработки();
	ДополнительныеПараметры.Вставить("ЭтоНезависимыйРегистрСведений", Истина);
	ДополнительныеПараметры.Вставить("ПолноеИмяРегистра", "РегистрСведений.РеестрДокументов");
	
	ОбрабатываемыеТипыДокументов = Новый Массив;
	ОбрабатываемыеТипыДокументов.Добавить("Документ.АвансовыйОтчет");
	//++ НЕ УТ
	ОбрабатываемыеТипыДокументов.Добавить("Документ.ПоступлениеДенежныхДокументов");
	ОбрабатываемыеТипыДокументов.Добавить("Документ.ПриобретениеУслугПоЛизингу");
	ОбрабатываемыеТипыДокументов.Добавить("Документ.ПоступлениеПредметовЛизинга");
	ОбрабатываемыеТипыДокументов.Добавить("Документ.ИнвентаризацияОС");
	//-- НЕ УТ
	
	// Регистрация через АдаптированныйТекстЗапроса
	Для каждого ПолноеИмяДокумента Из ОбрабатываемыеТипыДокументов Цикл
		
		НеиспользуемыеПоля = Новый Массив;
		НеиспользуемыеПоля.Добавить("Дополнительно");
		НеиспользуемыеПоля.Добавить("РазделительЗаписи");
		НеиспользуемыеПоля.Добавить("НомерПервичногоДокумента");
		
		ИмяДокумента = СтрРазделить(ПолноеИмяДокумента, ".")[1];
		РезультатАдаптацииЗапроса = Документы[ИмяДокумента].АдаптированныйТекстЗапросаДвиженийПоРегистру("РеестрДокументов");
		Регистраторы = ОбновлениеИнформационнойБазыУТ.ДанныеНезависимогоРегистраДляПерепроведения(
							РезультатАдаптацииЗапроса, 
							"РегистрСведений.РеестрДокументов", 
							ПолноеИмяДокумента, 
							НеиспользуемыеПоля);
		
		ОбновлениеИнформационнойБазы.ОтметитьКОбработке(Параметры, Регистраторы, ДополнительныеПараметры);
		
	КонецЦикла;
	
	//++ НЕ УТ
	ТекстЗапроса = 
	"ВЫБРАТЬ
	|	РеестрДокументов.Ссылка КАК Ссылка
	|ИЗ
	|	РегистрСведений.РеестрДокументов КАК РеестрДокументов
	|		ЛЕВОЕ СОЕДИНЕНИЕ Документ.ПеремещениеОС2_4 КАК ДанныеДокумента
	|		ПО (ДанныеДокумента.Ссылка = РеестрДокументов.Ссылка)
	|ГДЕ
	|	РеестрДокументов.ТипСсылки = &ПеремещениеОС2_4
	|	И РеестрДокументов.Дополнительно = """"
	|	И НЕ РеестрДокументов.ДополнительнаяЗапись
	|	И (ДанныеДокумента.ХозяйственнаяОперация = ЗНАЧЕНИЕ(Перечисление.ХозяйственныеОперации.ПеремещениеОСвПодразделениеВыделенноеНаБаланс)
	|				И ДанныеДокумента.ПодразделениеПолучатель <> ЗНАЧЕНИЕ(Справочник.СтруктураПредприятия.ПустаяСсылка)
	|				И ДанныеДокумента.ОрганизацияПолучатель <> ЗНАЧЕНИЕ(Справочник.Организации.ПустаяСсылка)
	|			ИЛИ ДанныеДокумента.ХозяйственнаяОперация <> ЗНАЧЕНИЕ(Перечисление.ХозяйственныеОперации.ПеремещениеОСвПодразделениеВыделенноеНаБаланс)
	|				И ДанныеДокумента.ПодразделениеПолучатель <> ЗНАЧЕНИЕ(Справочник.СтруктураПредприятия.ПустаяСсылка))
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	РеестрДокументов.Ссылка
	|ИЗ
	|	РегистрСведений.РеестрДокументов КАК РеестрДокументов
	|		ЛЕВОЕ СОЕДИНЕНИЕ Документ.ПеремещениеНМА2_4 КАК ДанныеДокумента
	|		ПО (ДанныеДокумента.Ссылка = РеестрДокументов.Ссылка)
	|ГДЕ
	|	РеестрДокументов.ТипСсылки = &ПеремещениеНМА2_4
	|	И РеестрДокументов.Дополнительно = """"
	|	И НЕ РеестрДокументов.ДополнительнаяЗапись
	|	И (ДанныеДокумента.ХозяйственнаяОперация = ЗНАЧЕНИЕ(Перечисление.ХозяйственныеОперации.ПеремещениеНМАвПодразделениеВыделенноеНаБаланс)
	|				И ДанныеДокумента.ПодразделениеПолучатель <> ЗНАЧЕНИЕ(Справочник.СтруктураПредприятия.ПустаяСсылка)
	|				И ДанныеДокумента.ОрганизацияПолучатель <> ЗНАЧЕНИЕ(Справочник.Организации.ПустаяСсылка)
	|			ИЛИ ДанныеДокумента.ХозяйственнаяОперация <> ЗНАЧЕНИЕ(Перечисление.ХозяйственныеОперации.ПеремещениеНМАвПодразделениеВыделенноеНаБаланс)
	|				И ДанныеДокумента.ПодразделениеПолучатель <> ЗНАЧЕНИЕ(Справочник.СтруктураПредприятия.ПустаяСсылка))
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	РеестрДокументов.Ссылка
	|ИЗ
	|	РегистрСведений.РеестрДокументов КАК РеестрДокументов
	|		ЛЕВОЕ СОЕДИНЕНИЕ Документ.ПеремещениеОС2_4 КАК ДанныеДокумента
	|		ПО (ДанныеДокумента.Ссылка = РеестрДокументов.Ссылка)
	|ГДЕ
	|	РеестрДокументов.ТипСсылки = &ПеремещениеОС2_4
	|	И РеестрДокументов.Дополнительно = """"
	|	И РеестрДокументов.ДополнительнаяЗапись
	|	И (ДанныеДокумента.ХозяйственнаяОперация = ЗНАЧЕНИЕ(Перечисление.ХозяйственныеОперации.ПеремещениеОСвПодразделениеВыделенноеНаБаланс)
	|				И ДанныеДокумента.Подразделение <> ЗНАЧЕНИЕ(Справочник.СтруктураПредприятия.ПустаяСсылка)
	|				И ДанныеДокумента.Организация <> ЗНАЧЕНИЕ(Справочник.Организации.ПустаяСсылка)
	|			ИЛИ ДанныеДокумента.ХозяйственнаяОперация <> ЗНАЧЕНИЕ(Перечисление.ХозяйственныеОперации.ПеремещениеОСвПодразделениеВыделенноеНаБаланс)
	|				И ДанныеДокумента.Подразделение <> ЗНАЧЕНИЕ(Справочник.СтруктураПредприятия.ПустаяСсылка))
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	РеестрДокументов.Ссылка
	|ИЗ
	|	РегистрСведений.РеестрДокументов КАК РеестрДокументов
	|		ЛЕВОЕ СОЕДИНЕНИЕ Документ.ПеремещениеНМА2_4 КАК ДанныеДокумента
	|		ПО (ДанныеДокумента.Ссылка = РеестрДокументов.Ссылка)
	|ГДЕ
	|	РеестрДокументов.ТипСсылки = &ПеремещениеНМА2_4
	|	И РеестрДокументов.Дополнительно = """"
	|	И РеестрДокументов.ДополнительнаяЗапись
	|	И (ДанныеДокумента.ХозяйственнаяОперация = ЗНАЧЕНИЕ(Перечисление.ХозяйственныеОперации.ПеремещениеНМАвПодразделениеВыделенноеНаБаланс)
	|				И ДанныеДокумента.Подразделение <> ЗНАЧЕНИЕ(Справочник.СтруктураПредприятия.ПустаяСсылка)
	|				И ДанныеДокумента.Организация <> ЗНАЧЕНИЕ(Справочник.Организации.ПустаяСсылка)
	|			ИЛИ ДанныеДокумента.ХозяйственнаяОперация <> ЗНАЧЕНИЕ(Перечисление.ХозяйственныеОперации.ПеремещениеНМАвПодразделениеВыделенноеНаБаланс)
	|				И ДанныеДокумента.Подразделение <> ЗНАЧЕНИЕ(Справочник.СтруктураПредприятия.ПустаяСсылка))";
	
	Запрос = Новый Запрос(ТекстЗапроса);
	Запрос.УстановитьПараметр("ПеремещениеОС2_4", ОбщегоНазначения.ИдентификаторОбъектаМетаданных(Метаданные.Документы.ПеремещениеОС2_4));
	Запрос.УстановитьПараметр("ПеремещениеНМА2_4", ОбщегоНазначения.ИдентификаторОбъектаМетаданных(Метаданные.Документы.ПеремещениеНМА2_4));
	Регистраторы = Запрос.Выполнить().Выгрузить();
	ОбновлениеИнформационнойБазы.ОтметитьКОбработке(Параметры, Регистраторы, ДополнительныеПараметры);
	//-- НЕ УТ
	
КонецПроцедуры

Процедура ЗарегистрироватьДанныеКОбработкеДляПереходаНаНовуюВерсиюСФиВЭД(Параметры) Экспорт 

	ПараметрыВыборки = Параметры.ПараметрыВыборки;
	ПараметрыВыборки.ДополнительныеИсточникиДанных.Вставить("Ссылка");
	ПараметрыВыборки.ПолныеИменаРегистров = "РегистрСведений.РеестрДокументов";
	ПараметрыВыборки.ПоляУпорядочиванияПриРаботеПользователей.Добавить("Ссылка.Дата УБЫВ");
	ПараметрыВыборки.ПоляУпорядочиванияПриОбработкеДанных.Добавить("Ссылка");
	ПараметрыВыборки.СпособВыборки = ОбновлениеИнформационнойБазы.СпособВыборкиИзмеренияНезависимогоРегистраСведений();
	
	ДополнительныеПараметры = ОбновлениеИнформационнойБазы.ДополнительныеПараметрыОтметкиОбработки();
	ДополнительныеПараметры.Вставить("ЭтоНезависимыйРегистрСведений", Истина);
	ДополнительныеПараметры.Вставить("ПолноеИмяРегистра", "РегистрСведений.РеестрДокументов");
	
	ОбрабатываемыеТипыДокументов = Новый Массив;
	ОбрабатываемыеТипыДокументов.Добавить("Документ.СчетФактураКомиссионеру");
	ОбрабатываемыеТипыДокументов.Добавить("Документ.СчетФактураКомитента");
	ОбрабатываемыеТипыДокументов.Добавить("Документ.СчетФактураНалоговыйАгент");
	ОбрабатываемыеТипыДокументов.Добавить("Документ.СчетФактураПолученныйНалоговыйАгент");
	//++ НЕ УТ
	ОбрабатываемыеТипыДокументов.Добавить("Документ.ТаможеннаяДекларацияЭкспорт");
	//-- НЕ УТ
	
	// Регистрация через АдаптированныйТекстЗапроса
	Для каждого ПолноеИмяДокумента Из ОбрабатываемыеТипыДокументов Цикл
		
		НеиспользуемыеПоля = Новый Массив;
		НеиспользуемыеПоля.Добавить("Дополнительно");
		НеиспользуемыеПоля.Добавить("РазделительЗаписи");
		НеиспользуемыеПоля.Добавить("НомерПервичногоДокумента");
		
		ИмяДокумента = СтрРазделить(ПолноеИмяДокумента, ".")[1];
		РезультатАдаптацииЗапроса = Документы[ИмяДокумента].АдаптированныйТекстЗапросаДвиженийПоРегистру("РеестрДокументов");
		Регистраторы = ОбновлениеИнформационнойБазыУТ.ДанныеНезависимогоРегистраДляПерепроведения(
							РезультатАдаптацииЗапроса, 
							"РегистрСведений.РеестрДокументов", 
							ПолноеИмяДокумента, 
							НеиспользуемыеПоля);
		
		ОбновлениеИнформационнойБазы.ОтметитьКОбработке(Параметры, Регистраторы, ДополнительныеПараметры);
		
	КонецЦикла;
	
	// Счета-фактуры полученные и выданные регистрируем отдельно (не через адаптируемый текст запроса),
	// т.к. на момент регистрации разницы в документе и в регистре может не быть.
	
	ТекстЗапроса = 
	"ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	РеестрДокументов.Ссылка КАК Ссылка
	|ИЗ
	|	РегистрСведений.РеестрДокументов КАК РеестрДокументов
	|ГДЕ
	|	РеестрДокументов.ТипСсылки = &СчетФактураПолученный
	|	И НЕ РеестрДокументов.ДополнительнаяЗапись
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	РеестрДокументов.Ссылка
	|ИЗ
	|	РегистрСведений.РеестрДокументов КАК РеестрДокументов
	|ГДЕ
	|	РеестрДокументов.ТипСсылки = &СчетФактураВыданный
	|	И НЕ РеестрДокументов.ДополнительнаяЗапись";
	
	Запрос = Новый Запрос(ТекстЗапроса);
	Запрос.УстановитьПараметр("СчетФактураВыданный", ОбщегоНазначения.ИдентификаторОбъектаМетаданных(Метаданные.Документы.СчетФактураВыданный));
	Запрос.УстановитьПараметр("СчетФактураПолученный", ОбщегоНазначения.ИдентификаторОбъектаМетаданных(Метаданные.Документы.СчетФактураПолученный));
	Регистраторы = Запрос.Выполнить().Выгрузить();
	ОбновлениеИнформационнойБазы.ОтметитьКОбработке(Параметры, Регистраторы, ДополнительныеПараметры);
	
КонецПроцедуры

Процедура ОбработатьДанныеДляПереходаНаНовуюВерсию(Параметры) Экспорт
		
	ВсеКлючиОбработаны = Истина;
	
	ОбъектыМетаданных = Справочники.КлючиРеестраДокументов.ОбъектыМетаданныхЗакешированныеВКлючахРеестра();
	ОбъектыМетаданных.Вставить(Метаданные.Справочники.КлючиРеестраДокументов);
	
	Для Каждого ОбъектМетаданных Из ОбъектыМетаданных Цикл
		ЕстьЗаблокированные = ОбновлениеИнформационнойБазы.ЕстьЗаблокированныеПредыдущимиОчередямиДанные(Параметры.Очередь,
			ОбъектМетаданных.Ключ.ПолноеИмя());
		Если ЕстьЗаблокированные Тогда
			ВсеКлючиОбработаны = Ложь;
			Прервать;
		КонецЕсли;
	КонецЦикла;

	Если Не ВсеКлючиОбработаны Тогда
		Параметры.ОбработкаЗавершена = Ложь;
		Возврат;
	КонецЕсли;
	
	ДополнительныеПараметры = ОбновлениеИнформационнойБазыУТ.ДополнительныеПараметрыПерезаписиДвиженийИзОчереди();
	ДополнительныеПараметры.ЭтоНезависимыйРегистрСведений = Истина;
	ДополнительныеПараметры.ИмяИзмеренияДляОтбора = "Ссылка";
	ДополнительныеПараметры.ЗаписыватьВОднойТранзакции = Ложь;
	ДополнительныеПараметры.НужнаДополнительнаяОбработкаЗаписей = Истина;
	ДополнительныеПараметры.ОбновляемыеДанные = Параметры.ОбновляемыеДанные;
	
	ВсеСделано = ОбновлениеИнформационнойБазыУТ.ПерезаписатьДвиженияИзОчереди(Неопределено,
																			"РегистрСведений.РеестрДокументов",
																			Параметры.Очередь,
																			ДополнительныеПараметры);
	
	Параметры.ОбработкаЗавершена = ВсеСделано;
	
КонецПроцедуры

Процедура ОбработатьДанныеДляПереходаНаНовуюВерсиюСФиВЭД(Параметры) Экспорт
	
	ОбработатьДанныеДляПереходаНаНовуюВерсию(Параметры);
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#КонецЕсли
