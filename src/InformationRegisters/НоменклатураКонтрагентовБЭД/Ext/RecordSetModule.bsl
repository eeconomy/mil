﻿////////////////////////////////////////////////////////////////////////////////
// Модуль набора записей РегистрСведений.НоменклатураКонтрагентовБЭД
//
////////////////////////////////////////////////////////////////////////////////

#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ОбработчикиСобытий

Процедура ОбработкаПроверкиЗаполнения(Отказ, ПроверяемыеРеквизиты)
	
	НеПроверяемыеРеквизиты = Новый Массив;
	НеПроверяемыеРеквизиты.Добавить("Владелец");
	
	МетаданныеСопоставления = ОбменСКонтрагентамиСлужебный.МетаданныеСопоставленияНоменклатуры();
	ПредставлениеВладельца = МетаданныеСопоставления.ВладелецНоменклатурыПредставлениеОбъекта;
	
	ТекстВладелецНеЗаполнен = СтрШаблон(НСтр("ru = 'Поле ""%1"" не заполнено';
											|en = 'The ""%1"" field is not filled in'"), ПредставлениеВладельца);
	
	Для каждого Запись Из ЭтотОбъект Цикл
		Если Не ЗначениеЗаполнено(Запись.Владелец) Тогда
			ОбщегоНазначения.СообщитьПользователю(ТекстВладелецНеЗаполнен, , "Владелец", "Запись", Отказ);
		КонецЕсли;
	КонецЦикла;
	
	ОбщегоНазначения.УдалитьНепроверяемыеРеквизитыИзМассива(ПроверяемыеРеквизиты, НеПроверяемыеРеквизиты);
	
КонецПроцедуры

#КонецОбласти

#Иначе
	
	ВызватьИсключение НСтр("ru = 'Недопустимый вызов объекта на клиенте.';
							|en = 'Invalid object call on the client.'");
	
#КонецЕсли

