﻿
&НаКлиенте
Процедура НеполноеВремяПриИзменении(Элемент)
	
	Если Запись.НеполноеВремя Тогда
		Элементы.НормаВремени.Видимость = Истина;	
	Иначе
		Элементы.НормаВремени.Видимость = Ложь;
		Запись.НормаВремени = 0;
	КонецЕсли;

КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	Если Запись.НеполноеВремя Тогда
		Элементы.НормаВремени.Видимость = Истина;	
	Иначе
		Элементы.НормаВремени.Видимость = Ложь;
	КонецЕсли;

КонецПроцедуры

&НаСервере
Процедура ПередЗаписьюНаСервере(Отказ, ТекущийОбъект, ПараметрыЗаписи)
	
	Если ТекущийОбъект.НеполноеВремя И ТекущийОбъект.НормаВремени = 0 Тогда
		ТекстСообщения = НСтр("ru='Проверьте заполненность обязательных реквизитов!'");
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, , , , Отказ);
	КонецЕсли;
	
КонецПроцедуры

