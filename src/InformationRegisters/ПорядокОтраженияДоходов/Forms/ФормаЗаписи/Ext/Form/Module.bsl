﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Свойство("АвтоТест") Тогда // Возврат при получении формы для анализа.
		Возврат;
	КонецЕсли;

	ОбновлениеИнформационнойБазы.ПроверитьОбъектОбработан(Запись, ЭтотОбъект);
	
	Если Параметры.Свойство("ВозвратЗначенияБезЗаписи") Тогда
		ВозвратЗначенияБезЗаписи = Параметры.ВозвратЗначенияБезЗаписи;
	КонецЕсли;
	
	Если ПолучитьФункциональнуюОпцию("ИспользоватьНесколькоОрганизаций") Тогда
		ДоступныеДляВыбораОрганизации = УправлениеДоступом.РазрешенныеЗначенияДляДинамическогоСписка(
			"РегистрСведений.ПорядокОтраженияДоходов", Тип("СправочникСсылка.Организации"));
		Если ДоступныеДляВыбораОрганизации <> Неопределено Тогда
			Элементы.Организация.СписокВыбора.ЗагрузитьЗначения(ДоступныеДляВыбораОрганизации);
			Элементы.Организация.РежимВыбораИзСписка = Истина;
			Если ДоступныеДляВыбораОрганизации.Количество() = 1 Тогда
				Запись.Организация = ДоступныеДляВыбораОрганизации.Получить(0);
				Элементы.Организация.ТолькоПросмотр = Истина;
			КонецЕсли;
		КонецЕсли;
	КонецЕсли;

	ДоступныеСчетаУчетаПрочихДоходов();
	
КонецПроцедуры

&НаКлиенте
Процедура ПослеЗаписи(ПараметрыЗаписи)
	
	Оповестить("Запись_ПорядокОтраженияДоходов");
	
КонецПроцедуры // ПослеЗаписи()

&НаКлиенте
Процедура ПередЗаписью(Отказ, ПараметрыЗаписи)
	
	Если ВозвратЗначенияБезЗаписи Тогда
		
		Если Не ЗначениеЗаполнено(Запись.Организация) Тогда
			Текст = НСтр("ru = 'Поле ""Организация"" не заполнено';
						|en = 'Company is not filled in'");
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(Текст, Параметры.Ключ, "Запись.Организация", "Запись.Организация", Отказ);
			Возврат;
		КонецЕсли;
		Если Не ЗначениеЗаполнено(Запись.СтатьяДоходов) Тогда
			Текст = НСтр("ru = 'Поле ""Статья доходов"" не заполнено';
						|en = 'The ""Income item"" field is required'");
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(Текст, Параметры.Ключ, "Запись.СтатьяДоходов", "Запись.СтатьяДоходов", Отказ);
			Возврат;
		КонецЕсли;
		
		Если Отказ Тогда
			Возврат;
		КонецЕсли;
		Отказ = Истина;
		Модифицированность = Ложь;
		СтруктураВозврата = Новый Структура("Организация, Подразделение, СтатьяДоходов, СчетУчета");
		ЗаполнитьЗначенияСвойств(СтруктураВозврата, Запись);
		Закрыть(СтруктураВозврата);
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)

	// СтандартныеПодсистемы.УправлениеДоступом
	Если ОбщегоНазначения.ПодсистемаСуществует("СтандартныеПодсистемы.УправлениеДоступом") Тогда
		МодульУправлениеДоступом = ОбщегоНазначения.ОбщийМодуль("УправлениеДоступом");
		МодульУправлениеДоступом.ПриЧтенииНаСервере(ЭтотОбъект, ТекущийОбъект);
	КонецЕсли;
	// Конец СтандартныеПодсистемы.УправлениеДоступом

КонецПроцедуры

&НаСервере
Процедура ПослеЗаписиНаСервере(ТекущийОбъект, ПараметрыЗаписи)

	// СтандартныеПодсистемы.УправлениеДоступом
	УправлениеДоступом.ПослеЗаписиНаСервере(ЭтотОбъект, ТекущийОбъект, ПараметрыЗаписи);
	// Конец СтандартныеПодсистемы.УправлениеДоступом

КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область Прочее

&НаСервере
Процедура ДоступныеСчетаУчетаПрочихДоходов()
	
	СтруктураСчетовУчета = Обработки.НастройкаОтраженияДокументовВРеглУчете.ДоступныеСчетаУчетаПрочихДоходов();
	
	ПараметрыВыбора = Новый Массив;
	ПараметрыВыбора.Добавить(Новый ПараметрВыбора("Отбор.Ссылка", Новый ФиксированныйМассив(СтруктураСчетовУчета.СчетаПрочихДоходов)));
	Элементы.СчетУчета.ПараметрыВыбора = Новый ФиксированныйМассив(ПараметрыВыбора);
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти
