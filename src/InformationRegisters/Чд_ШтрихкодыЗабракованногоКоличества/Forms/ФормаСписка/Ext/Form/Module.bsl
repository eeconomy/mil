﻿
#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ПечатьПФ(Команда)
	
	ВыделенныеСтроки = Элементы.Список.ВыделенныеСтроки;
	Если ВыделенныеСтроки.Количество() Тогда
		
		Штрихкоды = Новый Массив;
		Для Каждого ТекСтр Из ВыделенныеСтроки Цикл
			Штрихкоды.Добавить(Элементы.Список.ДанныеСтроки(ТекСтр).Штрихкод);
		КонецЦикла;
		
		КоллекцияПечатныхФорм = УправлениеПечатьюКлиент.НоваяКоллекцияПечатныхФорм("КоличествоЗабракованного");
		КоллекцияПечатныхФорм[0].ТабличныйДокумент = СформироватьТабДокПФ(Штрихкоды);
		КоллекцияПечатныхФорм[0].Экземпляров = 1;
		КоллекцияПечатныхФорм[0].СинонимМакета = "КоличествоЗабракованного";
		УправлениеПечатьюКлиент.ПечатьДокументов(КоллекцияПечатныхФорм, Неопределено, ЭтотОбъект);
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Функция СформироватьТабДокПФ(Штрихкоды) 
		
	ПараметрыПечати = Новый Структура();
	ТабДокумент = РегистрыСведений.Чд_ШтрихкодыЗабракованногоКоличества.СформироватьТабДокПФ(ПараметрыПечати, Штрихкоды);			
	
	Возврат ТабДокумент;
	
КонецФункции

#КонецОбласти