﻿#Область ОбработчикиСобытийФормы

&НаСервере
Функция ЗаполнитьСписокВыбораРеквизитовАктаЗапуска() 
	
	Массив = Новый Массив();
	Массив.Добавить("Вид акта");
	Массив.Добавить("Номер изменения");
	Массив.Добавить("Причина изменения");
	Массив.Добавить("Модель");
	Массив.Добавить("Дата начала разработки");
	Массив.Добавить("Бригада");
	Массив.Добавить("Изменения конструкции");
	Массив.Добавить("Изменение технологии");
	Массив.Добавить("Изменение привязки");
	Массив.Добавить("Изменение ТО");
	Массив.Добавить("Изменение образца");
	Массив.Добавить("Дизайнер");
	Массив.Добавить("Конструктор");
	Массив.Добавить("Технолог ЭЛ");
	Массив.Добавить("Нормировщик");
	Массив.Добавить("Портной");
	Массив.Добавить("Сырье, СММ");
	Массив.Добавить("Дата проработки");
	Массив.Добавить("Замечания швейному цеху");
	Массив.Добавить("Корректирующие мероприятия");
	Массив.Добавить("Замечания к образцу");
	Массив.Добавить("Дата ТС");
	Массив.Добавить("Решение ТС");
	Массив.Добавить("Дата утверждения образца");
	Массив.Добавить("Конструкция ТС");
	Массив.Добавить("Технология");
	Массив.Добавить("Привязка");
	Массив.Добавить("Пов. дата ТС");
	Массив.Добавить("Повт. решениеТС");
	Массив.Добавить("Готовность трудозатрат швейного цеха");
	Массив.Добавить("Дата готовности трудозатрат");
	Массив.Добавить("Установка методов настилания");
	Массив.Добавить("Положение деталей в раскладке");
	Массив.Добавить("Получение КиТД из ЭЛ");
	Массив.Добавить("Качество сдачи КИиТД");
	Массив.Добавить("Готовность норм расхода МиФ");
	Массив.Добавить("Расчет цены");
	Массив.Добавить("Вопросы при разработке");
	Массив.Добавить("Решения по вопросам");
	Массив.Добавить("Задание");
	Массив.Добавить("КиТД");
	Массив.Добавить("Замечания раскр. цеху");
	Массив.Добавить("Ответы раск. цеха");
	Массив.Добавить("Дата приемки");
	Массив.Добавить("Кач. изд.");
	Массив.Добавить("Коррект. действия"); 	
	Массив.Добавить("Кач. посадки");
	Массив.Добавить("Коррект. действ. цеха");
    Массив.Добавить("Месяц запуска");
	Массив.Добавить("Крой");
	Массив.Добавить("Фурнитура");
	Массив.Добавить("КД");
	Массив.Добавить("ТД");
	Массив.Добавить("СММ");
	Массив.Добавить("Норм. мат"); 
	Массив.Добавить("Норм. вр");	
	Массив.Добавить("Зам. ком. запуска");	
	Массив.Добавить("Корректир. мероприятия");

	Возврат Массив; 
	        
КонецФункции

&НаКлиенте
Процедура ПриОткрытии(Отказ)
		
	Массив = ЗаполнитьСписокВыбораРеквизитовАктаЗапуска();
	
	Элементы.РеквизитыДокумента.СписокВыбора.Очистить();
	
	Для Каждого Строка Из Массив Цикл 
		
		Элементы.РеквизитыДокумента.СписокВыбора.Добавить(Строка); 
		
	КонецЦикла;

КонецПроцедуры

#КонецОбласти
