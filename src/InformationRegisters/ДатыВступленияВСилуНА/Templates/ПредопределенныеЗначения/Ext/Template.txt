﻿<Structure xmlns="http://v8.1c.ru/8.1/data/core" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="Structure">
	<Property name="Версия">
		<Value xsi:type="xs:decimal">6</Value>
	</Property>
	<Property name="ОписаниеВерсии">
		<Value xsi:type="xs:string">Дата вступления в силу постановления Правления ПФР от 27 сентября 2019 № 485п.</Value>
	</Property>
	<Property name="ПолноеИмя">
		<Value xsi:type="xs:string">РегистрСведений.ДатыВступленияВСилуНА</Value>
	</Property>
	<Property name="Идентификатор">
		<Value xsi:type="xs:string">EffectiveDatesOfRegulatoryActs</Value>
	</Property>
	<Property name="Данные">
		<Value xsi:type="ValueTable">
			<column>
				<Name xsi:type="xs:string">ПостановлениеПравленияПФРот27сентября2019Номер485п</Name>
				<ValueType>
					<Type>xs:dateTime</Type>
					<DateQualifiers>
						<DateFractions>Date</DateFractions>
					</DateQualifiers>
				</ValueType>
				<Title>ПостановлениеПравленияПФРот27сентября2019Номер485п</Title>
				<Width xsi:type="xs:decimal">10</Width>
			</column>
			<row>
				<Value>2020-02-11T00:00:00</Value>
			</row>
		</Value>
	</Property>
</Structure>