﻿#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Свойство("Ключ") Тогда 
		ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(
			Список,
			"БригадаПошива",
			Параметры.Ключ,
			ВидСравненияКомпоновкиДанных.Равно);
	КонецЕсли
	
КонецПроцедуры

#КонецОбласти
