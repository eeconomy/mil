﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ПрограммныйИнтерфейс

// Описание:
//  для записи набора движений из модуля объекта документа
Процедура ОтразитьДвижения(ДополнительныеСвойства, Движения, Отказ) Экспорт

	Таблица = ДополнительныеСвойства.ТаблицыДляДвижений.ТаблицаЗначенияПоказателейПоТипуАртикулЦветОперация;
	
	Если Отказ ИЛИ Таблица.Количество() = 0 Тогда
		Возврат;
	КонецЕсли;

	Движения.Чд_ЗначенияПоказателейПоТипуАртикулЦветОперация.Записывать = Истина;
	Движения.Чд_ЗначенияПоказателейПоТипуАртикулЦветОперация.Загрузить(Таблица);

КонецПроцедуры

#КонецОбласти

#КонецЕсли