﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	ВедомостьПрочихДоходовФормы.ПриСозданииНаСервере(ЭтотОбъект, Отказ, СтандартнаяОбработка);
	
	// Обработчик подсистемы "ВерсионированиеОбъектов".
	ВерсионированиеОбъектов.ПриСозданииНаСервере(ЭтотОбъект);
	// СтандартныеПодсистемы.ПодключаемыеКоманды
	ПодключаемыеКоманды.ПриСозданииНаСервере(ЭтотОбъект);
	// Конец СтандартныеПодсистемы.ПодключаемыеКоманды
	
	// СтандартныеПодсистемы.Свойства
	ДополнительныеПараметры = Новый Структура;
	ДополнительныеПараметры.Вставить("ИмяЭлементаДляРазмещения", "ГруппаДополнительныеРеквизиты");
	УправлениеСвойствами.ПриСозданииНаСервере(ЭтотОбъект, ДополнительныеПараметры);
	// Конец СтандартныеПодсистемы.Свойства
	
КонецПроцедуры

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)
	
	// СтандартныеПодсистемы.ДатыЗапретаИзменения
	ДатыЗапретаИзменения.ОбъектПриЧтенииНаСервере(ЭтотОбъект, ТекущийОбъект);
	// Конец СтандартныеПодсистемы.ДатыЗапретаИзменения
	
	ВедомостьПрочихДоходовФормы.ПриЧтенииНаСервере(ЭтотОбъект, ТекущийОбъект);
	
	// СтандартныеПодсистемы.ПодключаемыеКоманды
	ПодключаемыеКомандыКлиентСервер.ОбновитьКоманды(ЭтотОбъект, Объект);
	// Конец СтандартныеПодсистемы.ПодключаемыеКоманды
	
	// СтандартныеПодсистемы.УправлениеДоступом
	УправлениеДоступом.ПриЧтенииНаСервере(ЭтотОбъект, ТекущийОбъект);
	// Конец СтандартныеПодсистемы.УправлениеДоступом
	
	// СтандартныеПодсистемы.Свойства
	УправлениеСвойствами.ПриЧтенииНаСервере(ЭтотОбъект, ТекущийОбъект);
	// Конец СтандартныеПодсистемы.Свойства
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	ВедомостьПрочихДоходовКлиент.ОбработкаОповещения(ЭтотОбъект, ИмяСобытия, Параметр, Источник);
	Если ИмяСобытия = "ЗагруженоПодтверждениеЗачисленияЗарплаты" И Параметр = Объект.Ссылка Тогда
		ГруппаПодтверждениеИзБанкаДополнитьФорму();
	КонецЕсли;
	ОграничениеИспользованияДокументовКлиент.ОбработкаОповещения(ЭтотОбъект, ИмяСобытия, Параметр, Источник);
	
	// СтандартныеПодсистемы.Свойства
	Если УправлениеСвойствамиКлиент.ОбрабатыватьОповещения(ЭтотОбъект, ИмяСобытия, Параметр) Тогда
		ОбновитьЭлементыДополнительныхРеквизитов();
		УправлениеСвойствамиКлиент.ПослеЗагрузкиДополнительныхРеквизитов(ЭтотОбъект);
	КонецЕсли;
	// Конец СтандартныеПодсистемы.Свойства
	
КонецПроцедуры

&НаСервере
Процедура ОбработкаПроверкиЗаполненияНаСервере(Отказ, ПроверяемыеРеквизиты)
	
	ВедомостьПрочихДоходовФормы.ОбработкаПроверкиЗаполненияНаСервере(ЭтотОбъект, Отказ, ПроверяемыеРеквизиты);
	
	// СтандартныеПодсистемы.Свойства
	УправлениеСвойствами.ОбработкаПроверкиЗаполнения(ЭтотОбъект, Отказ, ПроверяемыеРеквизиты);
	// Конец СтандартныеПодсистемы.Свойства
	
КонецПроцедуры

&НаСервере
Процедура ПослеЗаписиНаСервере(ТекущийОбъект, ПараметрыЗаписи)
	
	// СтандартныеПодсистемы.УправлениеДоступом
	УправлениеДоступом.ПослеЗаписиНаСервере(ЭтотОбъект, ТекущийОбъект, ПараметрыЗаписи);
	// Конец СтандартныеПодсистемы.УправлениеДоступом
	
	ВедомостьПрочихДоходовФормы.ПослеЗаписиНаСервере(ЭтотОбъект, ТекущийОбъект, ПараметрыЗаписи); 
	
	СохраняемыеЗначения = Новый Структура;
	СохраняемыеЗначения.Вставить("Бухгалтер", ТекущийОбъект.Бухгалтер);
	
	ЗарплатаКадры.СохранитьЗначенияЗаполненияОтветственныхРаботников(ТекущийОбъект.Организация, СохраняемыеЗначения);
		
КонецПроцедуры

&НаКлиенте
Процедура ПослеЗаписи(ПараметрыЗаписи)
	
	ОповеститьОбИзменении(Тип("РегистрСведенийКлючЗаписи.СостоянияДокументовЗачисленияЗарплаты"));
	Оповестить("Запись_ВедомостьПрочихДоходовВБанк", ПараметрыЗаписи, Объект.Ссылка);
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	//ОбменСБанкамиПоЗарплатнымПроектамКлиент.ОбновитьКомандыОбменаВедомости(ЭтотОбъект, Объект.ЗарплатныйПроект);
	
	// СтандартныеПодсистемы.ПодключаемыеКоманды
	ПодключаемыеКомандыКлиент.НачатьОбновлениеКоманд(ЭтотОбъект);
	// Конец СтандартныеПодсистемы.ПодключаемыеКоманды
	
	// СтандартныеПодсистемы.Свойства
	УправлениеСвойствамиКлиент.ПослеЗагрузкиДополнительныхРеквизитов(ЭтотОбъект);
	// Конец СтандартныеПодсистемы.Свойства
	
КонецПроцедуры

&НаСервере
Процедура ПередЗаписьюНаСервере(Отказ, ТекущийОбъект, ПараметрыЗаписи)
	
	// СтандартныеПодсистемы.Свойства
	УправлениеСвойствами.ПередЗаписьюНаСервере(ЭтотОбъект, ТекущийОбъект);
	// Конец СтандартныеПодсистемы.Свойства
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура ОрганизацияПриИзменении(Элемент)
	
	ЗарплатаКадрыКлиент.КлючевыеРеквизитыЗаполненияФормыОчиститьТаблицы(ЭтотОбъект);
	ОрганизацияПриИзмененииНаСервере();
	//ОбменСБанкамиПоЗарплатнымПроектамКлиент.ОбновитьКомандыОбменаВедомости(ЭтотОбъект, Объект.ЗарплатныйПроект);
	
КонецПроцедуры

&НаКлиенте
Процедура ПредставлениеОснованийНажатие(Элемент, СтандартнаяОбработка)
	ВедомостьПрочихДоходовКлиент.ПредставлениеОснованийНажатие(ЭтотОбъект, Элемент, СтандартнаяОбработка);
КонецПроцедуры

&НаКлиенте
Процедура СпособВыплатыПриИзменении(Элемент)
	
	Если СпособВыплатыПрежнееЗначение <> Объект.СпособВыплаты Тогда
		Если Объект.Состав.Количество() > 0 Тогда
			ВедомостьПрочихДоходовКлиент.Очистить(ЭтотОбъект);
		КонецЕсли;
		ВедомостьПрочихДоходовКлиентСервер.УстановитьПредставлениеОснований(ЭтотОбъект);
	КонецЕсли;
	СпособВыплатыПрежнееЗначение = Объект.СпособВыплаты;
	
КонецПроцедуры

&НаКлиенте
Процедура ЗарплатныйПроектПриИзменении(Элемент)
	ЗарплатныйПроектПриИзмененииНаСервере();
	ОбменСБанкамиПоЗарплатнымПроектамКлиент.ОбновитьКомандыОбменаВедомости(ЭтотОбъект, Объект.ЗарплатныйПроект);
КонецПроцедуры

&НаКлиенте
Процедура РуководительПриИзменении(Элемент)
	ПодписантПриИзмененииНаСервере()
КонецПроцедуры

&НаКлиенте
Процедура ГлавныйБухгалтерПриИзменении(Элемент)
	ПодписантПриИзмененииНаСервере()
КонецПроцедуры

&НаКлиенте
Процедура БухгалтерПриИзменении(Элемент)
	ПодписантПриИзмененииНаСервере()
КонецПроцедуры

&НаКлиенте
Процедура СоставилПриИзменении(Элемент)
	ПодписантПриИзмененииНаСервере()
КонецПроцедуры

&НаКлиенте
Процедура ПроверилПриИзменении(Элемент)
	ПодписантПриИзмененииНаСервере()
КонецПроцедуры

&НаКлиенте
Процедура ДатаПриИзменении(Элемент)
	ВедомостьПрочихДоходовКлиент.ДатаПриИзменении(ЭтотОбъект);
КонецПроцедуры

&НаКлиенте
Процедура ДатаВыплатыПриИзменении(Элемент)
	ВедомостьПрочихДоходовКлиент.ДатаВыплатыПриИзменении(ЭтотОбъект);
КонецПроцедуры


&НаКлиенте
Процедура КомментарийНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	ЗарплатаКадрыКлиент.КомментарийНачалоВыбора(ЭтотОбъект, Элемент, ДанныеВыбора, СтандартнаяОбработка)
КонецПроцедуры

&НаКлиенте
Процедура ПеречислениеНДФЛВыполненоПриИзменении(Элемент)
	ВедомостьПрочихДоходовКлиент.ПеречислениеНДФЛВыполненоПриИзменении(ЭтотОбъект, Элемент);
КонецПроцедуры


#Область РедактированиеМесяцаСтрокой

&НаКлиенте
Процедура ПериодРегистрацииПриИзменении(Элемент)
	ЗарплатаКадрыКлиент.ВводМесяцаПриИзменении(ЭтотОбъект, "Объект.ПериодРегистрации", "ПериодРегистрацииСтрокой", Модифицированность);
	ПодключаемыеКомандыКлиент.НачатьОбновлениеКоманд(ЭтотОбъект);
КонецПроцедуры

&НаКлиенте
Процедура ПериодРегистрацииНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	ОповещениеЗавершения = Новый ОписаниеОповещения("ПериодРегистрацииНачалоВыбораЗавершение", ЭтотОбъект);
	ЗарплатаКадрыКлиент.ВводМесяцаНачалоВыбора(ЭтотОбъект, ЭтотОбъект, "Объект.ПериодРегистрации", "ПериодРегистрацииСтрокой",, ОповещениеЗавершения);
КонецПроцедуры

&НаКлиенте
Процедура ПериодРегистрацииНачалоВыбораЗавершение(ЗначениеВыбрано, ДополнительныеПараметры) Экспорт
	ПодключаемыеКомандыКлиент.НачатьОбновлениеКоманд(ЭтотОбъект);
КонецПроцедуры

&НаКлиенте
Процедура ПериодРегистрацииРегулирование(Элемент, Направление, СтандартнаяОбработка)
	ЗарплатаКадрыКлиент.ВводМесяцаРегулирование(ЭтотОбъект, "Объект.ПериодРегистрации", "ПериодРегистрацииСтрокой", Направление, Модифицированность);
	ПодключаемыеКомандыКлиент.НачатьОбновлениеКоманд(ЭтотОбъект);
КонецПроцедуры

&НаКлиенте
Процедура ПериодРегистрацииАвтоПодбор(Элемент, Текст, ДанныеВыбора, Ожидание, СтандартнаяОбработка)
	ЗарплатаКадрыКлиент.ВводМесяцаАвтоПодборТекста(Текст, ДанныеВыбора, СтандартнаяОбработка);
	ПодключаемыеКомандыКлиент.НачатьОбновлениеКоманд(ЭтотОбъект);
КонецПроцедуры

&НаКлиенте
Процедура ПериодРегистрацииОкончаниеВводаТекста(Элемент, Текст, ДанныеВыбора, СтандартнаяОбработка)
	ЗарплатаКадрыКлиент.ВводМесяцаОкончаниеВводаТекста(Текст, ДанныеВыбора, СтандартнаяОбработка);
	ПодключаемыеКомандыКлиент.НачатьОбновлениеКоманд(ЭтотОбъект);
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыСостав

&НаКлиенте
Процедура СоставВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	ВедомостьПрочихДоходовКлиент.СоставВыбор(ЭтотОбъект, Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)	
КонецПроцедуры

&НаКлиенте
Процедура СоставОбработкаВыбора(Элемент, ВыбранноеЗначение, СтандартнаяОбработка)
	СоставОбработкаВыбораНаСервере(ВыбранноеЗначение, СтандартнаяОбработка);
КонецПроцедуры

&НаКлиенте
Процедура СоставПередНачаломДобавления(Элемент, Отказ, Копирование, Родитель, Группа, Параметр)
	Если Не Копирование Тогда
		ВедомостьНаВыплатуЗарплатыКлиент.Подобрать(ЭтотОбъект);
	КонецЕсли;	
	Отказ = Истина;
КонецПроцедуры

&НаКлиенте
Процедура СоставПередУдалением(Элемент, Отказ)
	ВедомостьНаВыплатуЗарплатыКлиент.СоставПередУдалением(ЭтотОбъект, Элемент, Отказ) 
КонецПроцедуры

&НаКлиенте
Процедура СоставПослеУдаления(Элемент)
	СоставПослеУдаленияНаСервере()
КонецПроцедуры

&НаКлиенте
Процедура СоставКВыплатеПриИзменении(Элемент)
	СоставКВыплатеПриИзмененииНаСервере();
КонецПроцедуры

&НаКлиенте
Процедура СоставКВыплатеОткрытие(Элемент, СтандартнаяОбработка)
	ВедомостьНаВыплатуЗарплатыКлиент.СоставКВыплатеОткрытие(ЭтотОбъект, Элемент, СтандартнаяОбработка);
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

// СтандартныеПодсистемы.Свойства
&НаКлиенте
Процедура Подключаемый_СвойстваВыполнитьКоманду(ЭлементИлиКоманда, НавигационнаяСсылка = Неопределено, СтандартнаяОбработка = Неопределено)
	УправлениеСвойствамиКлиент.ВыполнитьКоманду(ЭтотОбъект, ЭлементИлиКоманда, СтандартнаяОбработка);
КонецПроцедуры
// Конец СтандартныеПодсистемы.Свойства

// СтандартныеПодсистемы.ПодключаемыеКоманды
&НаКлиенте
Процедура Подключаемый_ВыполнитьКоманду(Команда)
	ПодключаемыеКомандыКлиент.ВыполнитьКоманду(ЭтотОбъект, Команда, Объект);
КонецПроцедуры

&НаСервере
Процедура Подключаемый_ВыполнитьКомандуНаСервере(Контекст, Результат) Экспорт
	ПодключаемыеКоманды.ВыполнитьКоманду(ЭтотОбъект, Контекст, Объект, Результат);
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ОбновитьКоманды()
	ПодключаемыеКомандыКлиентСервер.ОбновитьКоманды(ЭтотОбъект, Объект);
КонецПроцедуры
// Конец СтандартныеПодсистемы.ПодключаемыеКоманды

&НаКлиенте
Процедура Заполнить(Команда)
	ВедомостьПрочихДоходовКлиент.Заполнить(ЭтотОбъект)
КонецПроцедуры

&НаКлиенте
Процедура Подобрать(Команда)
	
	АдресСпискаПодобранныхФизическихЛиц = Неопределено;
	Если Не ПодготовитьПодборНаСервере(АдресСпискаПодобранныхФизическихЛиц) Тогда
		Возврат;
	КонецЕсли;
	
	ФизическиеЛицаЗарплатаКадрыРасширенныйКлиент.ОткрытьФормуПодбораФизическихЛицПоРоли(
		Элементы.Состав,
		Объект.Организация,
		ОписаниеПодбораПоРолям[Объект.СпособВыплаты],
		АдресСпискаПодобранныхФизическихЛиц);
	
КонецПроцедуры

&НаСервере
Функция ПодготовитьПодборНаСервере(АдресСпискаПодобранныхФизическихЛиц)

	ТекущийОбъект = РеквизитФормыВЗначение("Объект");
	Если ТекущийОбъект.МожноЗаполнитьВыплаты() Тогда
		АдресСпискаПодобранныхФизическихЛиц = АдресСпискаПодобранныхФизическихЛиц();
		Возврат Истина;
	КонецЕсли;
	
	Возврат Ложь;

КонецФункции

	
&НаСервере
Функция АдресСпискаПодобранныхФизическихЛиц()
	
	Возврат ПоместитьВоВременноеХранилище(Объект.Состав.Выгрузить(,"ФизическоеЛицо").ВыгрузитьКолонку("ФизическоеЛицо"), УникальныйИдентификатор);
	
КонецФункции

&НаКлиенте
Процедура ОбновитьНДФЛ(Команда)
	ВедомостьНаВыплатуЗарплатыКлиент.ОбновитьНДФЛ(ЭтотОбъект);	
КонецПроцедуры

&НаКлиенте
Процедура ОплатыПредставлениеОбработкаНавигационнойСсылки(Элемент, НавигационнаяСсылка, СтандартнаяОбработка)
	ВедомостьНаВыплатуЗарплатыКлиент.ОплатаПоказать(ЭтотОбъект, Элемент, НавигационнаяСсылка, СтандартнаяОбработка);
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ОткрытьДокументПодтверждение(Команда)
	ОбменСБанкамиПоЗарплатнымПроектамКлиент.ОткрытьДокументПодтверждение(ЭтотОбъект);
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ЗагрузитьДокументПодтверждение(Команда)
	ОткрытьФорму("Документ.ПодтверждениеЗачисленияЗарплаты.ФормаОбъекта");
КонецПроцедуры

&НаКлиенте
Процедура ОтправитьВБанк(Команда)
	
	ОбменСБанкамиКлиент.СформироватьПодписатьОтправитьЭД(Объект.Ссылка);
	ОграничениеИспользованияДокументовКлиент.ПодключитьОбработчикОжиданияОкончанияКомандыЗакрытия(ЭтотОбъект, "Подключаемый_ПослеОтправкиВБанк");
	
КонецПроцедуры

&НаКлиенте
Процедура ПоказатьОтправленныйДокумент(Команда)
	
	ОбменСБанкамиКлиент.ОткрытьАктуальныйЭД(Объект.Ссылка, ЭтотОбъект);
	
КонецПроцедуры

#Область ОграничениеДокумента

&НаКлиенте
Процедура Подключаемый_ОграничитьДокумент(Команда)
	
	ОграничитьДокументНаСервере();
	
КонецПроцедуры

#КонецОбласти

&НаКлиенте
Процедура Подключаемый_ПослеОтправкиВБанк()
	
	Если ОграничениеИспользованияДокументовВызовСервера.ДокументОграничен(Объект.Ссылка) Тогда
		ОграничениеИспользованияДокументовКлиентСервер.УстановитьДоступностьДанныхФормы(ЭтотОбъект);
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

// СтандартныеПодсистемы.Свойства 
&НаСервере
Процедура ОбновитьЭлементыДополнительныхРеквизитов()
	УправлениеСвойствами.ОбновитьЭлементыДополнительныхРеквизитов(ЭтотОбъект);
КонецПроцедуры

&НаКлиенте
Процедура ОбновитьЗависимостиДополнительныхРеквизитов()
	УправлениеСвойствамиКлиент.ОбновитьЗависимостиДополнительныхРеквизитов(ЭтотОбъект);
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ПриИзмененииДополнительногоРеквизита(Элемент)
	УправлениеСвойствамиКлиент.ОбновитьЗависимостиДополнительныхРеквизитов(ЭтотОбъект);
КонецПроцедуры
// Конец СтандартныеПодсистемы.Свойства

&НаСервере
Процедура ОграничитьДокументНаСервере()
	
	ОграничениеИспользованияДокументовФормы.ОграничитьДокумент(ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Процедура ОрганизацияПриИзмененииНаСервере()
	ВзаиморасчетыССотрудникамиФормы.ВедомостьОрганизацияПриИзмененииНаСервере(ЭтотОбъект);
	ПодписантПриИзмененииНаСервере();
КонецПроцедуры

&НаСервере
Процедура ПодписантПриИзмененииНаСервере()
	ВзаиморасчетыССотрудникамиФормы.ВедомостьПодписантПриИзмененииНаСервере(ЭтотОбъект)
КонецПроцедуры

&НаСервере
Процедура ЗарплатныйПроектПриИзмененииНаСервере()
	УточнитьВидимостьВХО()
КонецПроцедуры

&НаСервере
Процедура УточнитьВидимостьВХО()
	
	Если ПолучитьФункциональнуюОпцию("ИспользоватьВнешниеХозяйственныеОперацииЗарплатаКадры") Тогда
		
		Если ОбменСБанкамиПоЗарплатнымПроектам.ИспользоватьЭОИСБанком(Объект.ЗарплатныйПроект) Тогда
			
			Запрос = Новый Запрос;
			Запрос.УстановитьПараметр("Ведомость", Объект.Ссылка);
			Запрос.Текст = 
			"ВЫБРАТЬ РАЗРЕШЕННЫЕ ПЕРВЫЕ 1
			|	ПодтверждениеЗачисленияЗарплаты.Ссылка КАК Ссылка
			|ИЗ
			|	Документ.ПодтверждениеЗачисленияЗарплаты КАК ПодтверждениеЗачисленияЗарплаты
			|ГДЕ
			|	ПодтверждениеЗачисленияЗарплаты.ПервичныйДокумент = &Ведомость
			|	И ПодтверждениеЗачисленияЗарплаты.Проведен";
			
			Элементы.ВыплатаЗарплатыГруппа.Видимость = Запрос.Выполнить().Пустой();
			
		Иначе
			Элементы.ВыплатаЗарплатыГруппа.Видимость = Истина;
		КонецЕсли;
		
	КонецЕсли;
	
	ПроектВыбран = ЗначениеЗаполнено(Объект.ЗарплатныйПроект);
	ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "НомерРеестра", "Видимость", ПроектВыбран);
	
КонецПроцедуры

#Область КлючевыеРеквизитыЗаполненияФормы

// Функция возвращает описание таблиц формы подключенных к механизму ключевых реквизитов формы.
&НаСервере
Функция КлючевыеРеквизитыЗаполненияФормыТаблицыОчищаемыеПриИзменении() Экспорт
	Массив = Новый Массив;
	Массив.Добавить("Объект.Состав");
	Массив.Добавить("Объект.Выплаты");
	Массив.Добавить("Объект.Основания");
	Массив.Добавить("Объект.НДФЛ");
	Возврат Массив
КонецФункции 

// Функция возвращает массив реквизитов формы подключенных к механизму ключевых реквизитов формы.
&НаСервере
Функция КлючевыеРеквизитыЗаполненияФормыОписаниеКлючевыхРеквизитов() Экспорт
	Массив = Новый Массив;
	Массив.Добавить(Новый Структура("ЭлементФормы, Представление", "Организация",	Нстр("ru = 'организации'")));
	Массив.Добавить(Новый Структура("ЭлементФормы, Представление", "СпособВыплаты", Нстр("ru = 'порядка выплаты'")));
	Возврат Массив
КонецФункции

&НаСервере
Процедура СоставОбработкаВыбораНаСервере(ВыбранноеЗначение, СтандартнаяОбработка)
	ВедомостьПрочихДоходовФормы.СоставОбработкаВыбораНаСервере(ЭтотОбъект, ВыбранноеЗначение, СтандартнаяОбработка)
КонецПроцедуры

&НаСервере
Процедура СоставПослеУдаленияНаСервере()
	ВедомостьПрочихДоходовФормы.СоставПослеУдаленияНаСервере(ЭтотОбъект)
КонецПроцедуры

&НаСервере
Процедура СоставКВыплатеПриИзмененииНаСервере()
	ВедомостьПрочихДоходовФормы.СоставКВыплатеПриИзмененииНаСервере(ЭтотОбъект)	
КонецПроцедуры

#КонецОбласти

#Область ОбратныеВызовы

&НаСервере
Процедура ПриПолученииДанныхНаСервере(ТекущийОбъект) Экспорт
	
	ВедомостьПрочихДоходовФормы.ПриПолученииДанныхНаСервере(ЭтотОбъект, ТекущийОбъект);
	
	ГруппаПодтверждениеИзБанкаДополнитьФорму();
	//ОбменСБанкамиПоЗарплатнымПроектам.КомандыОбменаДополнитьФормуВедомостиВБанк(ЭтотОбъект);
	УточнитьВидимостьВХО();
	
	ОграничениеИспользованияДокументовФормы.ПриПолученииДанныхНаСервере(ЭтотОбъект);
	
КонецПроцедуры


&НаСервере
Процедура ГруппаПодтверждениеИзБанкаДополнитьФорму()
	ОбменСБанкамиПоЗарплатнымПроектам.ГруппаПодтверждениеИзБанкаДополнитьФорму(ЭтотОбъект);
КонецПроцедуры

&НаСервере
Процедура ПриПолученииДанныхСтрокиСостава(СтрокаСостава) Экспорт
	ВедомостьПрочихДоходовФормы.ПриПолученииДанныхСтрокиСостава(ЭтотОбъект, СтрокаСостава)
КонецПроцедуры


&НаСервере
Процедура НастроитьОтображениеГруппыПодписей() Экспорт
	
	ЗарплатаКадры.НастроитьОтображениеГруппыПодписей(Элементы.ПодписиГруппа, "Объект.Руководитель", "Объект.ГлавныйБухгалтер", "Объект.Бухгалтер");
	
КонецПроцедуры

&НаСервере
Процедура УстановитьПредставлениеОплаты() Экспорт
	ВзаиморасчетыССотрудникамиФормы.ВедомостьУстановитьПредставлениеОплаты(ЭтотОбъект);
КонецПроцедуры

&НаСервере
Процедура УстановитьДоступностьЭлементов() Экспорт
	ВедомостьПрочихДоходовФормы.УстановитьДоступностьЭлементов(ЭтотОбъект);
КонецПроцедуры

&НаСервере
Процедура ОбработатьСообщенияПользователю() Экспорт
	ВзаиморасчетыССотрудникамиФормыРасширенный.ВедомостьОбработатьСообщенияПользователю(ЭтотОбъект);
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьНаСервере() Экспорт
	ВедомостьПрочихДоходовФормы.ЗаполнитьНаСервере(ЭтотОбъект);
КонецПроцедуры

&НаСервере
Процедура ОчиститьНаСервере() Экспорт
	ВедомостьПрочихДоходовФормы.ОчиститьНаСервере(ЭтотОбъект);
КонецПроцедуры

&НаКлиенте
Процедура РедактироватьНДФЛСтроки(ДанныеСтроки) Экспорт
	ВедомостьНаВыплатуЗарплатыКлиент.РедактироватьНДФЛСтроки(ЭтотОбъект, ДанныеСтроки);	
КонецПроцедуры

&НаСервере
Процедура РедактироватьНДФЛСтрокиЗавершениеНаСервере(РезультатыРедактирования) Экспорт
	ВзаиморасчетыССотрудникамиФормыРасширенный.ВедомостьРедактироватьНДФЛСтрокиЗавершениеНаСервере(ЭтотОбъект, РезультатыРедактирования) 
КонецПроцедуры

&НаСервере
Функция АдресВХранилищеНДФЛПоСтроке(ИдентификаторСтроки) Экспорт
	Возврат ВзаиморасчетыССотрудникамиФормыРасширенный.ВедомостьАдресВХранилищеНДФЛПоСтроке(ЭтотОбъект, ИдентификаторСтроки)
КонецФункции

&НаСервере
Процедура ОбновитьНДФЛНаСервере(ИдентификаторыСтрок) Экспорт
	ВедомостьПрочихДоходовФормы.ОбновитьНДФЛНаСервере(ЭтотОбъект, ИдентификаторыСтрок)
КонецПроцедуры

&НаКлиенте
Процедура РедактироватьВыплатуСтроки(ДанныеСтроки) Экспорт
	ВедомостьПрочихДоходовКлиент.РедактироватьВыплатуСтрокиСтроки(ЭтотОбъект, ДанныеСтроки);	
КонецПроцедуры

&НаСервере
Процедура РедактироватьВыплатуСтрокиЗавершениеНаСервере(РезультатыРедактирования) Экспорт
	ВедомостьПрочихДоходовФормы.РедактироватьВыплатуСтрокиЗавершениеНаСервере(ЭтотОбъект, РезультатыРедактирования) 
КонецПроцедуры

&НаСервере
Функция АдресВХранилищеВыплатыПоСтроке(ИдентификаторСтроки) Экспорт
	Возврат ВедомостьПрочихДоходовФормы.АдресВХранилищеВыплатыПоСтроке(ЭтотОбъект, ИдентификаторСтроки)
КонецФункции	


#КонецОбласти

#КонецОбласти
