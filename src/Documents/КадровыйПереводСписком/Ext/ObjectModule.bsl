﻿#Если Не ТолстыйКлиентУправляемоеПриложение Или Сервер Тогда

#Область ПрограммныйИнтерфейс

// Подсистема "Управление доступом".

// Процедура ЗаполнитьНаборыЗначенийДоступа по свойствам объекта заполняет наборы значений доступа
// в таблице с полями:
//    НомерНабора     - Число                                     (необязательно, если набор один),
//    ВидДоступа      - ПланВидовХарактеристикСсылка.ВидыДоступа, (обязательно),
//    ЗначениеДоступа - Неопределено, СправочникСсылка или др.    (обязательно),
//    Чтение          - Булево (необязательно, если набор для всех прав) устанавливается для одной строки набора,
//    Добавление      - Булево (необязательно, если набор для всех прав) устанавливается для одной строки набора,
//    Изменение       - Булево (необязательно, если набор для всех прав) устанавливается для одной строки набора,
//    Удаление        - Булево (необязательно, если набор для всех прав) устанавливается для одной строки набора,
//
//  Вызывается из процедуры УправлениеДоступомСлужебный.ЗаписатьНаборыЗначенийДоступа(),
// если объект зарегистрирован в "ПодпискаНаСобытие.ЗаписатьНаборыЗначенийДоступа" и
// из таких же процедур объектов, у которых наборы значений доступа зависят от наборов этого
// объекта (в этом случае объект зарегистрирован в "ПодпискаНаСобытие.ЗаписатьЗависимыеНаборыЗначенийДоступа").
//
// Параметры:
//  Таблица      - ТабличнаяЧасть,
//                 РегистрСведенийНаборЗаписей.НаборыЗначенийДоступа,
//                 ТаблицаЗначений, возвращаемая УправлениеДоступом.ТаблицаНаборыЗначенийДоступа().
//
Процедура ЗаполнитьНаборыЗначенийДоступа(Таблица) Экспорт
	
	ЗарплатаКадры.ЗаполнитьНаборыПоОрганизацииИФизическимЛицам(ЭтотОбъект, Таблица, "Организация", "Сотрудники.ФизическоеЛицо");
	
КонецПроцедуры

// Подсистема "Управление доступом".

#КонецОбласти


#Область ОбработчикиСобытий

Процедура ОбработкаПроверкиЗаполнения(Отказ, ПроверяемыеРеквизиты)
	
	// Если производится операция бронирования позиции штатного расписания, то никаких действий больше не производится
	ТолькоБронированиеПозиции = БронированиеПозиции И ПолучитьФункциональнуюОпцию("ИспользоватьБронированиеПозиций");
	Если ТолькоБронированиеПозиции Тогда
		Возврат;
	КонецЕсли; 
	
	ИсправлениеДокументовЗарплатаКадры.ПроверитьЗаполнение(ЭтотОбъект, ПроверяемыеРеквизиты, Отказ, "ПериодическиеСведения");
	
КонецПроцедуры

Процедура ОбработкаПроведения(Отказ, РежимПроведения)
	
	Документы.КадровыйПеревод.ОбработкаПроведения(ЭтотОбъект, Отказ, РежимПроведения);
	
КонецПроцедуры

Процедура ОбработкаУдаленияПроведения(Отказ)
	
	Документы.КадровыйПеревод.ОбработкаУдаленияПроведения(ЭтотОбъект, Отказ);
	
КонецПроцедуры

Процедура ОбработкаЗаполнения(ДанныеЗаполнения, СтандартнаяОбработка)
	
	Если ТипЗнч(ДанныеЗаполнения) = Тип("Структура") Тогда
		Если ДанныеЗаполнения.Свойство("Действие") И ДанныеЗаполнения.Действие = "Исправить" Тогда
			
			ИсправлениеДокументовЗарплатаКадры.СкопироватьДокумент(ЭтотОбъект, ДанныеЗаполнения.Ссылка);
			
			ИсправленныйДокумент = ДанныеЗаполнения.Ссылка;
			ЗарплатаКадрыРасширенный.ПриКопированииМногофункциональногоДокумента(ЭтотОбъект);
			
		КонецЕсли;
	КонецЕсли;
	
	ЗарплатаКадрыРасширенный.ОбработкаЗаполненияМногофункциональногоДокумента(ЭтотОбъект, ДанныеЗаполнения, СтандартнаяОбработка);
	
	Если ОбщегоНазначения.ПодсистемаСуществует("ЗарплатаКадрыПриложения.ГосударственнаяСлужба") Тогда
		Модуль = ОбщегоНазначения.ОбщийМодуль("ГосударственнаяСлужба");
		Модуль.ОбработкаЗаполненияДокументаПриемНаРаботу(ЭтотОбъект, ДанныеЗаполнения, СтандартнаяОбработка);
	КонецЕсли;

КонецПроцедуры

Процедура ПередЗаписью(Отказ, РежимЗаписи, РежимПроведения)
	
	Если ЗарплатаКадры.ОтключитьБизнесЛогикуПриЗаписи(ЭтотОбъект) Тогда
		Возврат;
	КонецЕсли;
	
	ДатаСобытия = Дата;
	
	ФОИспользоватьШтатноеРасписание = ПолучитьФункциональнуюОпцию("ИспользоватьШтатноеРасписание");
	Для каждого СтрокаСотрудника Из Сотрудники Цикл
		
		Если ФОИспользоватьШтатноеРасписание Тогда
			
			ДолжностьПозиции = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(СтрокаСотрудника.ДолжностьПоШтатномуРасписанию, "Должность");
			Если СтрокаСотрудника.Должность <> ДолжностьПозиции Тогда
				СтрокаСотрудника.Должность = ДолжностьПозиции;
			КонецЕсли;
			
		Иначе
			
			Если ЗначениеЗаполнено(СтрокаСотрудника.ДолжностьПоШтатномуРасписанию) Тогда
				СтрокаСотрудника.ДолжностьПоШтатномуРасписанию = Справочники.ШтатноеРасписание.ПустаяСсылка();
			КонецЕсли;
			
		КонецЕсли;
		
	КонецЦикла;
	
	Если ЕжегодныеОтпуска.Количество() = 0 Тогда
		
	КонецЕсли;
	
	ЗарплатаКадрыРасширенный.ПередЗаписьюМногофункциональногоДокумента(ЭтотОбъект, Отказ, РежимЗаписи, РежимПроведения);
	
	Если Не ЭтоНовый() Тогда
		
		Запрос = Новый Запрос;
		Запрос.УстановитьПараметр("Сотрудники", Сотрудники.Выгрузить(, "Сотрудник,ДатаОкончания"));
		Запрос.УстановитьПараметр("Ссылка", Ссылка);
		
		Запрос.Текст =
			"ВЫБРАТЬ
			|	КадровыйПереводСпискомСотрудники.Сотрудник,
			|	КадровыйПереводСпискомСотрудники.ДатаОкончания
			|ПОМЕСТИТЬ ВТТекущиеДанные
			|ИЗ
			|	&Сотрудники КАК КадровыйПереводСпискомСотрудники
			|;
			|
			|////////////////////////////////////////////////////////////////////////////////
			|ВЫБРАТЬ РАЗЛИЧНЫЕ ПЕРВЫЕ 1
			|	ЕСТЬNULL(КадровыйПереводСпискомСотрудники.Сотрудник, ТекущиеДанные.Сотрудник) КАК Сотрудник
			|ИЗ
			|	Документ.КадровыйПереводСписком.Сотрудники КАК КадровыйПереводСпискомСотрудники
			|		ПОЛНОЕ СОЕДИНЕНИЕ ВТТекущиеДанные КАК ТекущиеДанные
			|		ПО КадровыйПереводСпискомСотрудники.Сотрудник = ТекущиеДанные.Сотрудник
			|			И (КадровыйПереводСпискомСотрудники.Ссылка = &Ссылка)
			|ГДЕ
			|	(КадровыйПереводСпискомСотрудники.ДатаОкончания = ДАТАВРЕМЯ(1, 1, 1)
			|				И ТекущиеДанные.ДатаОкончания <> ДАТАВРЕМЯ(1, 1, 1)
			|			ИЛИ КадровыйПереводСпискомСотрудники.ДатаОкончания <> ДАТАВРЕМЯ(1, 1, 1)
			|				И ТекущиеДанные.ДатаОкончания = ДАТАВРЕМЯ(1, 1, 1))";
			
		РезультатЗапроса = Запрос.Выполнить();
		Если Не РезультатЗапроса.Пустой() Тогда
			ДополнительныеСвойства.Вставить("ИсключатьНеИзмененные", Истина);
		КонецЕсли;
		
	КонецЕсли; 

КонецПроцедуры

Процедура ПриКопировании(ОбъектКопирования)
	
	Если ОбъектКопирования.Проведен Тогда
		
		Сотрудники.Очистить();
		Начисления.Очистить();
		Показатели.Очистить();
		ЕжегодныеОтпуска.Очистить();
		Льготы.Очистить();
		
	КонецЕсли; 
	
	ЗарплатаКадрыРасширенный.ПриКопированииМногофункциональногоДокумента(ЭтотОбъект);

КонецПроцедуры

Процедура ПриЗаписи(Отказ)
	
	Если ЗарплатаКадры.ОтключитьБизнесЛогикуПриЗаписи(ЭтотОбъект) Тогда
		Возврат;
	КонецЕсли;
	
	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("Ссылка", Ссылка);
	
	Запрос.Текст =
		"ВЫБРАТЬ
		|	ПриемНаРаботу.Сотрудник,
		|	ПриемНаРаботу.ДатаПриема,
		|	ПриемНаРаботу.СпособОтраженияЗарплатыВБухучете,
		|	ПриемНаРаботу.ОтношениеКЕНВД,
		|	ПриемНаРаботу.СтатьяФинансирования
		|ИЗ
		|	Документ.ПриемНаРаботуСписком.Сотрудники КАК ПриемНаРаботу
		|ГДЕ
		|	ПриемНаРаботу.Ссылка = &Ссылка";
		
	ДанныеДокумента = Запрос.Выполнить().Выбрать();
	Пока ДанныеДокумента.Следующий() Цикл
		
		ОтражениеЗарплатыВБухучетеРасширенный.ЗарегистрироватьБухучетЗарплатыСотрудников(
			ДанныеДокумента.ДатаПриема, ЭтотОбъект, ДанныеДокумента);
		
	КонецЦикла; 
	
КонецПроцедуры

#КонецОбласти


#КонецЕсли
