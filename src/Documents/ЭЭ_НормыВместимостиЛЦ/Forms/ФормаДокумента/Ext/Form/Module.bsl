﻿#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	// СтандартныеПодсистемы.ПодключаемыеКоманды
	ПодключаемыеКоманды.ПриСозданииНаСервере(ЭтотОбъект);
	// Конец СтандартныеПодсистемы.ПодключаемыеКоманды
	
	// СтандартныеПодсистемы.ВерсионированиеОбъектов
	ВерсионированиеОбъектов.ПриСозданииНаСервере(ЭтотОбъект);
	// Конец СтандартныеПодсистемы.ВерсионированиеОбъектов
	
	Если ЗначениеЗаполнено(Параметры.ЗначениеКопирования) Тогда
		Объект.ПризнакПроведенияДокумента = Ложь;		
	КонецЕсли;
	
	СобытияФорм.ПриСозданииНаСервере(ЭтотОбъект, Отказ, СтандартнаяОбработка);
	
КонецПроцедуры 

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)
	
	// СтандартныеПодсистемы.ПодключаемыеКоманды
	ПодключаемыеКомандыКлиентСервер.ОбновитьКоманды(ЭтотОбъект, Объект);
	// Конец СтандартныеПодсистемы.ПодключаемыеКоманды 
	
	СобытияФорм.ПриЧтенииНаСервере(ЭтотОбъект, ТекущийОбъект);
	
КонецПроцедуры

&НаСервере
Процедура ПослеЗаписиНаСервере(ТекущийОбъект, ПараметрыЗаписи)
	
	СобытияФорм.ПослеЗаписиНаСервере(ЭтотОбъект, ТекущийОбъект, ПараметрыЗаписи);
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ) 
	
	// СтандартныеПодсистемы.ПодключаемыеКоманды
	ПодключаемыеКомандыКлиент.НачатьОбновлениеКоманд(ЭтотОбъект);
	// Конец СтандартныеПодсистемы.ПодключаемыеКоманды

КонецПроцедуры

&НаКлиенте
Процедура ОбработкаВыбора(ВыбранноеЗначение, ИсточникВыбора)

	Если ЗначениеЗаполнено(ВыбранноеЗначение) Тогда 
		ПрочитатьДанныеНаСервере(ВыбранноеЗначение);	
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыНормыВместимостиЛЦ

&НаКлиенте
Процедура НормыВместимостиЛЦПриАктивизацииСтроки(Элемент)
	
	ТекущаяСтрока = Элементы.НормыВместимостиЛЦ.ТекущиеДанные;
	Если ТекущаяСтрока = Неопределено Тогда 
		Возврат;	
	КонецЕсли; 
	
	УИД = ТекущаяСтрока.УИДСтроки;
	
	ПоискСтрокиВТЧТранспортныеЕдиницы(УИД);
	
КонецПроцедуры

&НаКлиенте
Процедура НормыВместимостиЛЦПередУдалением(Элемент, Отказ)
	
	УИД = Элементы.НормыВместимостиЛЦ.ТекущиеДанные.УИДСтроки;
	УдалениеСтрокиВТЧТранспортныеЕдиницы(УИД);
	
КонецПроцедуры

&НаКлиенте
Процедура НормыВместимостиЛЦНомерХраненияШтПриИзменении(Элемент)
	
	СтарыйУИД = Элементы.НормыВместимостиЛЦ.ТекущиеДанные.УИДСтроки;
	УдалениеСтрокиВТЧТранспортныеЕдиницы(СтарыйУИД);
	УИД = ПолучитьУИДДляСтроки();
	Элементы.НормыВместимостиЛЦ.ТекущиеДанные.УИДСтроки = УИД;	
	НормаХраненияШт = Элементы.НормыВместимостиЛЦ.ТекущиеДанные.НормаХраненияШт;
    	
	Если ЗначениеЗаполнено(НормаХраненияШт) Тогда 
		НормыВместимостиЛЦНомерХраненияШтПриИзмененииНаСервере(УИД, НормаХраненияШт);
		ПоискСтрокиВТЧТранспортныеЕдиницы(УИД);
	КонецЕсли;
		
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы 

&НаКлиенте
Процедура Заполнить(Команда)
	
	ОчиститьСообщения(); 
	Если ЗначениеЗаполнено(Объект.ГруппаВместимости) Тогда 
		ЗаполнитьТЧНаОснованииГруппВместимости();	
	Иначе
		ТекстСообщения = НСтр("ru = 'Не заполнена Группа вместимости'");
		ОбщегоНазначенияКлиент.СообщитьПользователю(ТекстСообщения);	
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ЗаписатьДокумент(Команда)
	
	ОбщегоНазначенияУТКлиент.Записать(ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура ПровестиДокумент(Команда)
	
	Если Не Объект.ПризнакПроведенияДокумента Тогда 
		Оповещение = Новый ОписаниеОповещения("ПослеОтветаНаВопросПровести", ЭтотОбъект);
		ПоказатьВопрос(Оповещение, 
						"Документ будет заблокирован для изменений. Оставить документ доступным для редактирования?",
						 РежимДиалогаВопрос.ДаНет,, КодВозвратаДиалога.Нет); 
	Иначе
		ОбщегоНазначенияКлиент.СообщитьПользователю("Документ не редактируется!"); 	
	КонецЕсли;
					
КонецПроцедуры

&НаКлиенте
Процедура ПровестиИЗакрыть(Команда) 
	
	Если Не Объект.ПризнакПроведенияДокумента Тогда 
		Оповещение = Новый ОписаниеОповещения("ПослеОтветаНаВопросПровестиИЗакрыть", ЭтотОбъект);
		ПоказатьВопрос(Оповещение, 
						"Документ будет заблокирован для изменений. Оставить документ доступным для редактирования?",
						РежимДиалогаВопрос.ДаНет,, КодВозвратаДиалога.Нет); 
	Иначе	
		ОбщегоНазначенияКлиент.СообщитьПользователю("Документ не редактируется!"); 	
	КонецЕсли;
		
КонецПроцедуры

&НаКлиенте
Процедура ЗагрузитьXLS(Команда)
	
	ОткрытьФорму("Документ.ЭЭ_НормыВместимостиЛЦ.Форма.ФормаЗагрузкаДанныхИзФайлаXLS",, ЭтотОбъект,,,,, РежимОткрытияОкнаФормы.БлокироватьОкноВладельца);
	
КонецПроцедуры

#КонецОбласти 

#Область СлужебныеПроцедурыИФункции

&НаКлиенте
Процедура ПослеОтветаНаВопросПровестиИЗакрыть(РезультатВопроса, ДополнительныеПараметры) Экспорт 

	Если РезультатВопроса = КодВозвратаДиалога.Да Тогда 
		ОбщегоНазначенияУТКлиент.Записать(ЭтотОбъект);		
	Иначе 
		ОбщегоНазначенияУТКлиент.ПровестиИЗакрыть(ЭтотОбъект);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПослеОтветаНаВопросПровести(РезультатВопроса, ДополнительныеПараметры) Экспорт 

	Если РезультатВопроса = КодВозвратаДиалога.Да Тогда 
		ОбщегоНазначенияУТКлиент.Записать(ЭтотОбъект);
	Иначе
		ОбщегоНазначенияУТКлиент.Провести(ЭтотОбъект);		
	КонецЕсли;
	
КонецПроцедуры

// СтандартныеПодсистемы.ПодключаемыеКоманды
&НаКлиенте
Процедура Подключаемый_ВыполнитьКоманду(Команда)
	
	ПодключаемыеКомандыКлиент.ВыполнитьКоманду(ЭтотОбъект, Команда, Объект);
	
КонецПроцедуры

&НаСервере
Процедура Подключаемый_ВыполнитьКомандуНаСервере(Контекст, Результат)
	
	ПодключаемыеКоманды.ВыполнитьКоманду(ЭтотОбъект, Контекст, Объект, Результат);
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ОбновитьКоманды()
	
	ПодключаемыеКомандыКлиентСервер.ОбновитьКоманды(ЭтотОбъект, Объект);
	
КонецПроцедуры

// Конец СтандартныеПодсистемы.ПодключаемыеКоманды

&НаКлиенте
Процедура УдалениеСтрокиВТЧТранспортныеЕдиницы(УИД)
	
	Если Не ЗначениеЗаполнено(УИД) Тогда 
		Возврат;	
	КонецЕсли;
	
	СтруктураОтбора = Новый Структура;
	СтруктураОтбора.Вставить("УИДНормыВместимостиЛЦ", УИД);
	МассивСтрок = Объект.ТранспортныеЕдиницы.НайтиСтроки(СтруктураОтбора);
	Если МассивСтрок.Количество() Тогда  
		Для Каждого СтрМассива Из МассивСтрок Цикл
			Объект.ТранспортныеЕдиницы.Удалить(СтрМассива);
		КонецЦикла;
	КонецЕсли;
	
КонецПроцедуры 

&НаКлиенте
Функция ПолучитьУИДДляСтроки()
	
	УИД = 0;
	
	Для Каждого Элемент Из Объект.НормыВместимостиЛЦ Цикл		
		Если Элемент.УИДСтроки >= УИД Тогда 			
			УИД = Элемент.УИДСтроки + 1;		
		КонецЕсли;	
	КонецЦикла;
	
	Возврат УИД; 
	
КонецФункции 

&НаКлиенте
Процедура ПоискСтрокиВТЧТранспортныеЕдиницы(УИД)
	СтруктураПоиска = Новый Структура;
	СтруктураПоиска.Вставить("УИДНормыВместимостиЛЦ", УИД);
	Элементы.ТранспортныеЕдиницы.ОтборСтрок = Новый ФиксированнаяСтруктура(СтруктураПоиска); 
КонецПроцедуры

&НаСервере
Процедура НормыВместимостиЛЦНомерХраненияШтПриИзмененииНаСервере(УИД, НормаХраненияШт) 
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
		"ВЫБРАТЬ
		|	Чд_ТранспортныеЕдиницы.Ссылка КАК ТипТЕ,
		|	ВЫРАЗИТЬ(Чд_ТранспортныеЕдиницы.МножительНормыВместимости * &НормаХраненияШт КАК ЧИСЛО(10, 0)) КАК НормыХраненияВсегоШт,
		|	Чд_ТранспортныеЕдиницы.ПоУмолчанию КАК ТЕПоУмолчанию
		|ПОМЕСТИТЬ ВТ_ТЕ
		|ИЗ
		|	Справочник.Чд_ТранспортныеЕдиницы КАК Чд_ТранспортныеЕдиницы
		|ГДЕ
		|	НЕ Чд_ТранспортныеЕдиницы.ПометкаУдаления
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	ВТ_ТЕ.ТипТЕ КАК ТипТЕ,
		|	ВТ_ТЕ.НормыХраненияВсегоШт КАК НормыХраненияВсегоШт,
		|	ВТ_ТЕ.ТЕПоУмолчанию КАК ТЕПоУмолчанию,
		|	&УИДНормыВместимостиЛЦ КАК УИДНормыВместимостиЛЦ
		|ИЗ
		|	ВТ_ТЕ КАК ВТ_ТЕ
		|ГДЕ
		|	ВТ_ТЕ.НормыХраненияВсегоШт > 0";
	
	Запрос.УстановитьПараметр("УИДНормыВместимостиЛЦ", УИД);
	Запрос.УстановитьПараметр("НормаХраненияШт", НормаХраненияШт);
	
	РезультатЗапроса = Запрос.Выполнить();
	
	Если РезультатЗапроса.Пустой() Тогда 
		Возврат;	
	КонецЕсли;
	
	ВыборкаДетальныеЗаписи = РезультатЗапроса.Выбрать();
	
	Пока ВыборкаДетальныеЗаписи.Следующий() Цикл
		НоваяСтрокаТЧ = Объект.ТранспортныеЕдиницы.Добавить();
		ЗаполнитьЗначенияСвойств(НоваяСтрокаТЧ, ВыборкаДетальныеЗаписи);
	КонецЦикла;
		
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьТЧНаОснованииГруппВместимости()
	
	Запрос = Новый Запрос;
	Запрос.Текст = ТекстЗапросаДляГруппВместимости(); 
	
	СвойствоКонструкция = ПланыВидовХарактеристик.ДополнительныеРеквизитыИСведения.НайтиПоРеквизиту("Имя", "Чд_Конструкция(справочник)"); 
	СвойствоРазмер = ПланыВидовХарактеристик.ДополнительныеРеквизитыИСведения.НайтиПоРеквизиту("Имя", "Чд_РазмерыГотовыхИзделий");

	Запрос.УстановитьПараметр("СвойствоКонструкция", СвойствоКонструкция);
	Запрос.УстановитьПараметр("СвойствоРазмер", СвойствоРазмер);
	Запрос.УстановитьПараметр("ЭЭ_ГруппаВместимости", Объект.ГруппаВместимости);
	
	РезультатЗапроса = Запрос.Выполнить();
	
	Если РезультатЗапроса.Пустой() Тогда 
		ТекстСообщения = НСтр("ru = 'Нет данных для заполнения'");
		ОбщегоНазначения.СообщитьПользователю(ТекстСообщения);
		Возврат;	
	КонецЕсли;
	
	Объект.ТранспортныеЕдиницы.Очистить();
	Объект.НормыВместимостиЛЦ.Загрузить(РезультатЗапроса.Выгрузить());
	ТекстСообщения = НСтр("ru = 'Значения заполнены'");
	ОбщегоНазначения.СообщитьПользователю(ТекстСообщения);

КонецПроцедуры 

&НаСервере
Функция ТекстЗапросаДляГруппВместимости()
	
	Возврат "ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	Чд_Конструкции.Ссылка КАК Ссылка
	|ПОМЕСТИТЬ ВТ_Конструкции
	|ИЗ
	|	Справочник.Чд_Конструкции КАК Чд_Конструкции
	|ГДЕ
	|	НЕ Чд_Конструкции.ПометкаУдаления
	|	И Чд_Конструкции.ЭЭ_ГруппаВместимости = &ЭЭ_ГруппаВместимости
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	НоменклатураДополнительныеРеквизиты.Ссылка КАК Номенклатура,
	|	Номенклатура.ЭЭ_КоллекцияМилавицы КАК ЭЭ_КоллекцияМилавицы
	|ПОМЕСТИТЬ ВТ_Номенклатура
	|ИЗ
	|	Справочник.Номенклатура.ДополнительныеРеквизиты КАК НоменклатураДополнительныеРеквизиты
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ ВТ_Конструкции КАК ВТ_Конструкции
	|		ПО (НоменклатураДополнительныеРеквизиты.Свойство = &СвойствоКонструкция)
	|			И НоменклатураДополнительныеРеквизиты.Значение = ВТ_Конструкции.Ссылка
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Справочник.Номенклатура КАК Номенклатура
	|		ПО НоменклатураДополнительныеРеквизиты.Ссылка = Номенклатура.Ссылка
	|			И (НЕ Номенклатура.ПометкаУдаления)
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	ВТ_Номенклатура.Номенклатура КАК Номенклатура,
	|	ЕСТЬNULL(ХарактеристикиНоменклатуры.Ссылка, ЗНАЧЕНИЕ(Справочник.ХарактеристикиНоменклатуры.ПустаяСсылка)) КАК Характеристика,
	|	ВТ_Номенклатура.ЭЭ_КоллекцияМилавицы КАК ЭЭ_КоллекцияМилавицы
	|ПОМЕСТИТЬ ВТ_НоменклатураИХарактеристики
	|ИЗ
	|	ВТ_Номенклатура КАК ВТ_Номенклатура
	|		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.ХарактеристикиНоменклатуры КАК ХарактеристикиНоменклатуры
	|		ПО ВТ_Номенклатура.Номенклатура = ХарактеристикиНоменклатуры.Владелец
	|			И (НЕ ХарактеристикиНоменклатуры.ПометкаУдаления)
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	ЕСТЬNULL(ХарактеристикиНоменклатурыДополнительныеРеквизиты.Значение, ЗНАЧЕНИЕ(Справочник.Чд_РазмерыГотовыхИзделий.ПустаяСсылка)) КАК Размер,
	|	ВТ_НоменклатураИХарактеристики.ЭЭ_КоллекцияМилавицы КАК Коллекция
	|ИЗ
	|	ВТ_НоменклатураИХарактеристики КАК ВТ_НоменклатураИХарактеристики
	|		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.ХарактеристикиНоменклатуры.ДополнительныеРеквизиты КАК ХарактеристикиНоменклатурыДополнительныеРеквизиты
	|		ПО (ВТ_НоменклатураИХарактеристики.Характеристика = ХарактеристикиНоменклатурыДополнительныеРеквизиты.Ссылка)
	|			И (ХарактеристикиНоменклатурыДополнительныеРеквизиты.Свойство = &СвойствоРазмер)
	|ГДЕ
	|	НЕ ВТ_НоменклатураИХарактеристики.Характеристика = ЗНАЧЕНИЕ(Справочник.ХарактеристикиНоменклатуры.ПустаяСсылка)";
	
КонецФункции	

&НаСервере
Процедура ПрочитатьДанныеНаСервере(АдресФайлаВХранилище)

	ДвоичныеДанные = ПолучитьИзВременногоХранилища(АдресФайлаВХранилище);
	ВременныйФайл = ПолучитьИмяВременногоФайла("xlsx");
	ДвоичныеДанные.Записать(ВременныйФайл);
	УдалитьИзВременногоХранилища(АдресФайлаВХранилище);
	
	ТабДок = Новый ТабличныйДокумент;
	
	Попытка
	 	ТабДок.Прочитать(ВременныйФайл, СпособЧтенияЗначенийТабличногоДокумента.Значение);
	Исключение
	   	ОбщегоНазначения.СообщитьПользователю("Не удалось прочитать файл по причине: " + ОписаниеОшибки()); 
		Возврат;
	КонецПопытки;
	
	УИДСтроки = ПолучитьУИДСтроки(Объект.НормыВместимостиЛЦ);
	
	КоличествоСтрок = ТабДок.ВысотаТаблицы;
	
	Для НомерСтроки = 2 По КоличествоСтрок Цикл
		
		СтруктураЗаполнения = ПолучитьСтруктуруЗаполнения();
		УИДСтроки = УИДСтроки + 1; 
		СтруктураЗаполнения.УИДСтроки = УИДСтроки;	
		СтруктураЗаполнения.КоллекцияСтрока = СокрЛП(ТабДок.ПолучитьОбласть("R" + Формат(НомерСтроки, "ЧГ=0") + "C" + 2).ТекущаяОбласть.Текст);
		СтруктураЗаполнения.РазмерСтрока = СокрЛП(ТабДок.ПолучитьОбласть("R" + Формат(НомерСтроки, "ЧГ=0") + "C" + 3).ТекущаяОбласть.Текст);
		СтруктураЗаполнения.КодПодачи = СокрЛП(ТабДок.ПолучитьОбласть("R" + Формат(НомерСтроки, "ЧГ=0") + "C" + 5).ТекущаяОбласть.Текст);
		
		НормаХраненияСтрока = СокрЛП(ТабДок.ПолучитьОбласть("R" + Формат(НомерСтроки, "ЧГ=0") + "C" + 4).ТекущаяОбласть.Текст);	
		СтруктураЗаполнения.НормаХраненияЧисло = СтроковыеФункцииКлиентСервер.СтрокаВЧисло(НормаХраненияСтрока); 
		Если СтруктураЗаполнения.НормаХраненияЧисло = Неопределено Тогда 
			СтруктураЗаполнения.НормаХраненияЧисло = 0;
 		КонецЕсли;		
		
		ДатаНачалаСтрока = СокрЛП(ТабДок.ПолучитьОбласть("R" + Формат(НомерСтроки, "ЧГ=0") + "C" + 6).ТекущаяОбласть.Текст);
		СтруктураЗаполнения.ДатаНачала = СтроковыеФункцииКлиентСервер.СтрокаВДату(ДатаНачалаСтрока);
		Если СтруктураЗаполнения.ДатаНачала = Неопределено Тогда 
			СтруктураЗаполнения.ДатаНачала = Дата(1,1,1);	
		КонецЕсли;
		
		ЗаполнитьТабличнуюЧастьДокумента(СтруктураЗаполнения);
		
	КонецЦикла;
	
	Попытка
		УдалитьФайлы(ВременныйФайл);
	Исключение
		ЗаписьЖурналаРегистрации(НСтр("ru = 'Действие'"), УровеньЖурналаРегистрации.Ошибка, , , ПодробноеПредставлениеОшибки(ИнформацияОбОшибке()));
	КонецПопытки;
	
КонецПроцедуры

&НаСервере
Функция ПолучитьУИДСтроки(НормыВместимостиЛЦ)
	
	Перем УИД;
	
	ТЗУИДов = НормыВместимостиЛЦ.Выгрузить(, "УИДСтроки");
	Если ТЗУИДов.Количество() = 0 Тогда 
		УИД = 0;
	Иначе 
		ТЗУИДов.Сортировать("УИДСтроки Убыв");
		УИД = ТЗУИДов[0].УИДСтроки;
	КонецЕсли;
	
	Возврат УИД;
	
КонецФункции 

&НаСервере
Функция ПолучитьСтруктуруЗаполнения()
	
	Структура = Новый Структура;
	Структура.Вставить("КоллекцияСтрока");
	Структура.Вставить("РазмерСтрока");
	Структура.Вставить("КодПодачи");
	Структура.Вставить("НормаХраненияЧисло");
	Структура.Вставить("ДатаНачала");
	Структура.Вставить("УИДСтроки");
	Возврат Структура; 
	
КонецФункции 

&НаСервере
Процедура ЗаполнитьТабличнуюЧастьДокумента(СтруктураЗаполнения)
	
	НоваяСтрокаТЧ = Объект.НормыВместимостиЛЦ.Добавить();
	Если ЗначениеЗаполнено(СтруктураЗаполнения.КоллекцияСтрока) Тогда 
		НоваяСтрокаТЧ.Коллекция = Справочники.КоллекцииНоменклатуры.НайтиПоНаименованию(СтруктураЗаполнения.КоллекцияСтрока, Истина);
	Иначе 
		НоваяСтрокаТЧ.Коллекция = Справочники.КоллекцииНоменклатуры.ПустаяСсылка(); 	
	КонецЕсли;
	НоваяСтрокаТЧ.Размер = ПолучитьРазмерыГотовыхИзделий(СтруктураЗаполнения.РазмерСтрока);
	НоваяСтрокаТЧ.НормаХраненияШт = СтруктураЗаполнения.НормаХраненияЧисло;
	НоваяСтрокаТЧ.КодПодачиНаПроизводственнуюУпаковку = Справочники.ЭЭ_ТипыУпаковочныхМашин.НайтиПоНаименованию(СтруктураЗаполнения.КодПодачи, Истина);
	НоваяСтрокаТЧ.ДатаНачала = СтруктураЗаполнения.ДатаНачала;
	НоваяСтрокаТЧ.УИДСтроки = СтруктураЗаполнения.УИДСтроки;
	
	Если ЗначениеЗаполнено(НоваяСтрокаТЧ.НормаХраненияШт) Тогда 
		НормыВместимостиЛЦНомерХраненияШтПриИзмененииНаСервере(НоваяСтрокаТЧ.УИДСтроки, НоваяСтрокаТЧ.НормаХраненияШт);
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Функция  ПолучитьРазмерыГотовыхИзделий(РазмерСтрока)
	
	СсылкаНаРазмер = Справочники.Чд_РазмерыГотовыхИзделий.ПустаяСсылка();
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
		"ВЫБРАТЬ
		|	Чд_РазмерыГотовыхИзделий.Ссылка КАК Ссылка
		|ИЗ
		|	Справочник.Чд_РазмерыГотовыхИзделий КАК Чд_РазмерыГотовыхИзделий
		|ГДЕ
		|	НЕ Чд_РазмерыГотовыхИзделий.ЭтоГруппа
		|	И НЕ Чд_РазмерыГотовыхИзделий.ПометкаУдаления
		|	И Чд_РазмерыГотовыхИзделий.КодРазмера = &КодРазмера";
	
	Запрос.УстановитьПараметр("КодРазмера", РазмерСтрока);
	
	РезультатЗапроса = Запрос.Выполнить();
	
	ВыборкаДетальныеЗаписи = РезультатЗапроса.Выбрать();
	
	Количество = ВыборкаДетальныеЗаписи.Количество();
	
	Если Количество = 1 Тогда 
		
		ВыборкаДетальныеЗаписи.Следующий();
		СсылкаНаРазмер = ВыборкаДетальныеЗаписи.Ссылка;
		
	КонецЕсли;
	
	Возврат СсылкаНаРазмер;
	
КонецФункции

#КонецОбласти
