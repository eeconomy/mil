﻿
#Область ОписаниеПеременных

&НаКлиенте
Перем КэшированныеЗначения; // используется механизмом обработки изменения реквизитов ТЧ

#КонецОбласти

#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Свойство("АвтоТест") Тогда
		Возврат;
	КонецЕсли;
	
	ОбновлениеИнформационнойБазы.ПроверитьОбъектОбработан(Объект, ЭтотОбъект);
	
	Если Параметры.Ключ.Пустая() Тогда
		ПриЧтенииСозданииНаСервере();
	КонецЕсли;

	#Область СтандартныеПодсистемы
	
	// СтандартныеПодсистемы.ВерсионированиеОбъектов
	ВерсионированиеОбъектов.ПриСозданииНаСервере(ЭтаФорма);
	// Конец СтандартныеПодсистемы.ВерсионированиеОбъектов
	
	// ИнтеграцияС1СДокументооборотом
	ИнтеграцияС1СДокументооборот.ПриСозданииНаСервере(ЭтаФорма);
	// Конец ИнтеграцияС1СДокументооборотом

	// СтандартныеПодсистемы.ПодключаемыеКоманды
	ПодключаемыеКоманды.ПриСозданииНаСервере(ЭтотОбъект);
	// Конец СтандартныеПодсистемы.ПодключаемыеКоманды
	
	ОбщегоНазначенияУТ.НастроитьПодключаемоеОборудование(ЭтаФорма);
	
	СобытияФорм.ПриСозданииНаСервере(ЭтотОбъект, Отказ, СтандартнаяОбработка);
	
	#КонецОбласти

КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	МенеджерОборудованияКлиент.НачатьПодключениеОборудованиеПриОткрытииФормы(Неопределено, ЭтаФорма, "СканерШтрихкода");
	
	// СтандартныеПодсистемы.ПодключаемыеКоманды
	ПодключаемыеКомандыКлиент.НачатьОбновлениеКоманд(ЭтотОбъект);
	// Конец СтандартныеПодсистемы.ПодключаемыеКоманды
	
КонецПроцедуры

&НаКлиенте
Процедура ПриЗакрытии(ЗавершениеРаботы)
	
	МенеджерОборудованияКлиент.НачатьОтключениеОборудованиеПриЗакрытииФормы(Неопределено, ЭтаФорма);
	
КонецПроцедуры

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)

	// СтандартныеПодсистемы.УправлениеДоступом
	Если ОбщегоНазначения.ПодсистемаСуществует("СтандартныеПодсистемы.УправлениеДоступом") Тогда
		МодульУправлениеДоступом = ОбщегоНазначения.ОбщийМодуль("УправлениеДоступом");
		МодульУправлениеДоступом.ПриЧтенииНаСервере(ЭтотОбъект, ТекущийОбъект);
	КонецЕсли;
	// Конец СтандартныеПодсистемы.УправлениеДоступом
	
	ДатыЗапретаИзменения.ОбъектПриЧтенииНаСервере(ЭтаФорма, ТекущийОбъект);
	
	ПриЧтенииСозданииНаСервере();
	
	СобытияФорм.ПриЧтенииНаСервере(ЭтотОбъект, ТекущийОбъект);
	
	// СтандартныеПодсистемы.ПодключаемыеКоманды
	ПодключаемыеКомандыКлиентСервер.ОбновитьКоманды(ЭтотОбъект, Объект);
	// Конец СтандартныеПодсистемы.ПодключаемыеКоманды
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаВыбора(ВыбранноеЗначение, ИсточникВыбора)
	
	Если ИсточникВыбора.ИмяФормы = "ОбщаяФорма.РеквизитыПечатиРеализации" Тогда
		
		Если ВыбранноеЗначение <> Неопределено Тогда
			ЗаполнитьЗначенияСвойств(Объект, ВыбранноеЗначение);
		КонецЕсли;
		
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ПослеЗаписиНаСервере(ТекущийОбъект, ПараметрыЗаписи)

	// СтандартныеПодсистемы.УправлениеДоступом
	УправлениеДоступом.ПослеЗаписиНаСервере(ЭтотОбъект, ТекущийОбъект, ПараметрыЗаписи);
	// Конец СтандартныеПодсистемы.УправлениеДоступом
	
	ПриЧтенииСозданииНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ПередЗаписью(Отказ, ПараметрыЗаписи)
	
	СобытияФормКлиент.ПередЗаписью(ЭтотОбъект, Отказ, ПараметрыЗаписи);

КонецПроцедуры

&НаКлиенте
Процедура ПослеЗаписи(ПараметрыЗаписи)
	
	Оповестить("Запись_ПодготовкаКПередачеОС2_4", ПараметрыЗаписи, Объект.Ссылка);

	МодификацияКонфигурацииКлиентПереопределяемый.ПослеЗаписи(ЭтаФорма, ПараметрыЗаписи);

	ОбщегоНазначенияУТКлиент.ВыполнитьДействияПослеЗаписи(ЭтаФорма, Объект, ПараметрыЗаписи);
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	// ПодключаемоеОборудование
	Если Источник = "ПодключаемоеОборудование" И ВводДоступен() Тогда
		Если ИмяСобытия = "ScanData" И МенеджерОборудованияУТКлиент.ЕстьНеобработанноеСобытие() Тогда
			ОбработатьШтрихкоды(МенеджерОборудованияУТКлиент.ПреобразоватьДанныеСоСканераВСтруктуру(Параметр));
		КонецЕсли;
	КонецЕсли;
	// Конец ПодключаемоеОборудование
	
	СобытияФормКлиент.ОбработкаОповещения(ЭтотОбъект, ИмяСобытия, Параметр, Источник);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура ЦенаВключаетНДСПриИзменении(Элемент)
	ЦенаВключаетНДСПриИзмененииСервер(КэшированныеЗначения);
КонецПроцедуры

&НаКлиенте
Процедура ВалютаПриИзменении(Элемент)
	ПересчитатьСуммыПоВалюте();
КонецПроцедуры

&НаКлиенте
Процедура НалогообложениеНДСПриИзменении(Элемент)
	НалогообложениеНДСПриИзмененииСервер();
КонецПроцедуры

&НаКлиенте
Процедура ПартнерПриИзменении(Элемент)
	
	Если Не ЗначениеЗаполнено(Объект.Партнер) Тогда
		Возврат;
	КонецЕсли; 
	
	ПартнерПриИзмененииНаСервере()
	
КонецПроцедуры

&НаСервере
Процедура ПартнерПриИзмененииНаСервере()
	
	ПартнерыИКонтрагенты.ЗаполнитьКонтрагентаПартнераПоУмолчанию(Объект.Партнер, Объект.Контрагент);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийТаблицы

&НаКлиенте
Процедура ОССуммаПриИзменении(Элемент)
	
	Действия = Новый Структура;
	ДобавитьДействияПересчетаНДС(Действия, Объект);
	Действия.Вставить("ПересчитатьЦенуСкидкуПоСуммеВПродажах", Новый Структура("ИмяКоличества", "Количество"));
	
	СтрокаТЧ = СоздатьСтрокуПересчетаСумм(Элементы.ОС.ТекущиеДанные);
	ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТЧ(СтрокаТЧ, Действия, КэшированныеЗначения);
	ЗаполнитьЗначенияСвойств(Элементы.ОС.ТекущиеДанные, СтрокаТЧ);
	
КонецПроцедуры

&НаКлиенте
Процедура ОССтавкаНДСПриИзменении(Элемент)
	
	Действия = Новый Структура;
	ДобавитьДействияПересчетаСумм(Действия, Объект);
	
	СтрокаТЧ = СоздатьСтрокуПересчетаСумм(Элементы.ОС.ТекущиеДанные);
	ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТЧ(СтрокаТЧ, Действия, КэшированныеЗначения);
	ЗаполнитьЗначенияСвойств(Элементы.ОС.ТекущиеДанные, СтрокаТЧ);
	
КонецПроцедуры

&НаКлиенте
Процедура ОСОбработкаВыбора(Элемент, ВыбранноеЗначение, СтандартнаяОбработка)
	
	ВнеоборотныеАктивыКлиентСервер.ОбработкаВыбораЭлемента(Объект.ОС, "ОсновноеСредство", ВыбранноеЗначение);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

// СтандартныеПодсистемы.ПодключаемыеКоманды
&НаКлиенте
Процедура Подключаемый_ВыполнитьКоманду(Команда)
	ПодключаемыеКомандыКлиент.ВыполнитьКоманду(ЭтотОбъект, Команда, Объект);
КонецПроцедуры

&НаСервере
Процедура Подключаемый_ВыполнитьКомандуНаСервере(Контекст, Результат) Экспорт
	ПодключаемыеКоманды.ВыполнитьКоманду(ЭтотОбъект, Контекст, Объект, Результат);
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ОбновитьКоманды()
	ПодключаемыеКомандыКлиентСервер.ОбновитьКоманды(ЭтотОбъект, Объект);
КонецПроцедуры
// Конец СтандартныеПодсистемы.ПодключаемыеКоманды

&НаКлиенте
Процедура Подключаемый_ВыполнитьПереопределяемуюКоманду(Команда)
	
	СобытияФормКлиент.ВыполнитьПереопределяемуюКоманду(ЭтотОбъект, Команда);
	
КонецПроцедуры

&НаКлиенте
Процедура ЗаписатьДокумент(Команда)
	
	ОбщегоНазначенияУТКлиент.Записать(ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура Подбор(Команда)
	
	ПараметрыПодбора = ВнеоборотныеАктивыКлиентСервер.ПараметрыПодбора(Элементы.ОСОсновноеСредство, ЭтаФорма);
	ОткрытьФорму("Справочник.ОбъектыЭксплуатации.ФормаВыбора", 
		ПараметрыПодбора, 
		Элементы.ОС);
	
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьРеквизитыПечати(Команда)
	
	РеквизитыПечати = Новый Структура();
	
	РеквизитыПечати.Вставить("БанковскийСчетГрузоотправителя",  Объект.БанковскийСчетГрузоотправителя);
	РеквизитыПечати.Вставить("БанковскийСчетГрузополучателя",   Объект.БанковскийСчетГрузополучателя);
	РеквизитыПечати.Вставить("БанковскийСчетКонтрагента",       Объект.БанковскийСчетКонтрагента);
	РеквизитыПечати.Вставить("БанковскийСчетОрганизации",       Объект.БанковскийСчетОрганизации);
	РеквизитыПечати.Вставить("ГлавныйБухгалтер",                Объект.ГлавныйБухгалтер);
	РеквизитыПечати.Вставить("Грузоотправитель",                Объект.Грузоотправитель);
	РеквизитыПечати.Вставить("Грузополучатель",                 Объект.Грузополучатель);
	РеквизитыПечати.Вставить("Основание",                       Объект.Основание);
	РеквизитыПечати.Вставить("ОснованиеДата",                   Объект.ОснованиеДата);
	РеквизитыПечати.Вставить("ОснованиеНомер",                  Объект.ОснованиеНомер);
	РеквизитыПечати.Вставить("Отпустил",                        Объект.Отпустил);
	РеквизитыПечати.Вставить("ОтпустилДолжность",               Объект.ОтпустилДолжность);
	РеквизитыПечати.Вставить("Руководитель",                    Объект.Руководитель);
	
	ПараметрыФормы = Новый Структура("РеквизитыПечати", РеквизитыПечати);

	Обработчик = Новый ОписаниеОповещения("УстановитьРеквизитыПечати", ЭтаФорма);
	ОткрытьФорму("Документ.ПодготовкаКПередачеОС2_4.Форма.РеквизитыПечати",
				ПараметрыФормы,
				ЭтаФорма,,,Неопределено,
				Обработчик,
				РежимОткрытияОкнаФормы.БлокироватьОкноВладельца);
	
КонецПроцедуры

&НаКлиенте
Процедура ПровестиИЗакрыть(Команда)
	
	ОбщегоНазначенияУТКлиент.ПровестиИЗакрыть(ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура ПровестиДокумент(Команда)
	
	ОбщегоНазначенияУТКлиент.Провести(ЭтаФорма);
	
КонецПроцедуры

#Область ШтрихкодыИТорговоеОборудование

&НаКлиенте
Процедура ЗагрузитьДанныеИзТСД(Команда)
	
	ВыполнитьЗагрузкуДанныеИзТСД();
	
КонецПроцедуры

#КонецОбласти

#Область ИнтеграцияС1СДокументооборотом

&НаКлиенте
Процедура Подключаемый_ВыполнитьКомандуИнтеграции(Команда)
	
	ИнтеграцияС1СДокументооборотКлиент.ВыполнитьПодключаемуюКомандуИнтеграции(Команда, ЭтаФорма, Объект);
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область ШтрихкодыИТорговоеОборудование

&НаСервере
Процедура ОбработатьШтрихкоды(Знач ДанныеШтрихкодов)
	
	ПараметрыПодбора = ВнеоборотныеАктивыКлиентСервер.ПараметрыПодбора(Элементы.ОСОсновноеСредство, ЭтаФорма);
	МассивОбъектов = ВнеоборотныеАктивы.НайтиОсновныеСредстваПоШтрихкодам(ДанныеШтрихкодов, ПараметрыПодбора);
	ВнеоборотныеАктивыКлиентСервер.ОбработкаВыбораЭлемента(Объект.ОС, "ОсновноеСредство", МассивОбъектов);
	
КонецПроцедуры

&НаКлиенте
Процедура ВыполнитьЗагрузкуДанныеИзТСД()
	
	ОчиститьСообщения();
	
	МенеджерОборудованияКлиент.НачатьЗагрузкуДанныеИзТСД(
		Новый ОписаниеОповещения("ЗагрузитьИзТСДЗавершение", ЭтотОбъект),
		УникальныйИдентификатор);
	
КонецПроцедуры

&НаКлиенте
Процедура ЗагрузитьИзТСДЗавершение(Результат, ДополнительныеПараметры) Экспорт
	
	Если Результат.Результат Тогда
		ОбработатьШтрихкоды(Результат.ТаблицаТоваров);
	Иначе
		МенеджерОборудованияУТКлиент.СообщитьОбОшибке(Результат);
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область Прочее

&НаКлиенте
Процедура Подключаемый_ЗакрытьФорму()
	
	Закрыть();
	
КонецПроцедуры

&НаКлиенте
Процедура УстановитьРеквизитыПечати(Результат, ДопПараметры) Экспорт
	
	Если Результат <> Неопределено И Результат.Количество() > 0 Тогда
		ЗаполнитьЗначенияСвойств(Объект, Результат);
		Модифицированность = Истина;
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ПриЧтенииСозданииНаСервере()
	
	ВалютаДокумента = Объект.Валюта;
	
КонецПроцедуры

&НаСервере
Процедура ВалютаПриИзмененииСервер(ВалютаСтарая, ВалютаНовая)
	
	ДатаДокумента = ?(ЗначениеЗаполнено(Объект.Дата), Объект.Дата, ТекущаяДатаСеанса());
	КурсыСтарые = РаботаСКурсамиВалют.ПолучитьКурсВалюты(ВалютаСтарая, ДатаДокумента);
	КурсыНовые  = РаботаСКурсамиВалют.ПолучитьКурсВалюты(ВалютаНовая, ДатаДокумента);
	Ценообразование.ПересчитатьСуммыТабличнойЧастиВВалюту(
		Объект.ОС, Объект.ЦенаВключаетНДС, ВалютаСтарая, ВалютаНовая, КурсыСтарые, КурсыНовые, , , "Количество");
	
КонецПроцедуры

&НаКлиенте
Процедура ПересчитатьСуммыПоВалюте()
	
	Если ЗначениеЗаполнено(Объект.Валюта) И ВалютаДокумента<>Объект.Валюта И Объект.ОС.Количество()>0 Тогда
		ВалютаПриИзмененииСервер(ВалютаДокумента, Объект.Валюта);
		ЦенообразованиеКлиент.ОповеститьОбОкончанииПересчетаСуммВВалюту(ВалютаДокумента, Объект.Валюта);
	КонецЕсли;
	ВалютаДокумента = Объект.Валюта;
	
КонецПроцедуры

&НаСервере
Процедура ЦенаВключаетНДСПриИзмененииСервер(КэшЗначений)
	
	ДействияПересчета = ОбработкаТабличнойЧастиКлиентСервер.ПараметрыПересчетаСуммыНДСВТЧ(Объект);
	Действия = Новый Структура;
	Действия.Вставить("ПересчитатьСуммуНДС", ДействияПересчета);
	Действия.Вставить("ПересчитатьСуммуСНДС", ДействияПересчета);
	ОбработкаТабличнойЧастиСервер.ОбработатьТЧ(Объект.ОС, Действия, КэшЗначений);
	
КонецПроцедуры

&НаСервере
Процедура НалогообложениеНДСПриИзмененииСервер()
	
	ДействияПересчета = ОбработкаТабличнойЧастиКлиентСервер.ПараметрыПересчетаСуммыНДСВТЧ(Объект);
	Действия = Новый Структура();
	Действия.Вставить("ПересчитатьСуммуНДС", ДействияПересчета);
	Действия.Вставить("ПересчитатьСуммуСНДС", ДействияПересчета);
	Если Перечисления.ТипыНалогообложенияНДС.ПродажаНеОблагаетсяНДС=Объект.НалогообложениеНДС
		Или Перечисления.ТипыНалогообложенияНДС.ПродажаНаЭкспорт=Объект.НалогообложениеНДС
	Тогда
		Действия.Вставить("ЗаполнитьСтавкуНДС",
			Новый Структура("НалогообложениеНДС, Дата", Объект.НалогообложениеНДС, Объект.Дата));
	КонецЕсли;
	ОбработкаТабличнойЧастиСервер.ОбработатьТЧ(Объект.ОС, Действия, Неопределено);
	
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Процедура ДобавитьДействияПересчетаНДС(Действия, Объект)
	
	ДействияПересчета = ОбработкаТабличнойЧастиКлиентСервер.ПараметрыПересчетаСуммыНДСВСтрокеТЧ(Объект);
	Действия.Вставить("ПересчитатьСуммуНДС", ДействияПересчета);
	Действия.Вставить("ПересчитатьСуммуСНДС", ДействияПересчета);
	
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Процедура ДобавитьДействияПересчетаСумм(Действия, Объект)
	
	ДобавитьДействияПересчетаНДС(Действия, Объект);
	Действия.Вставить("ПересчитатьСумму", "Количество");
	
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Функция СоздатьСтрокуПересчетаСумм(ТекущаяСтрокаОС)
	
	Структура = Новый Структура("Количество, КоличествоУпаковок, Цена, Сумма, СуммаНДС, СуммаСНДС, СтавкаНДС");
	ЗаполнитьЗначенияСвойств(Структура, ТекущаяСтрокаОС);
	Структура.Цена = Структура.Сумма;
	Структура.Количество = 1;
	Структура.КоличествоУпаковок = 1;
	
	Возврат Структура;
	
КонецФункции

#КонецОбласти

#КонецОбласти
