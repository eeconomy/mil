﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ПрограммныйИнтерфейс

// Подсистема "Управление доступом".

// Процедура ЗаполнитьНаборыЗначенийДоступа по свойствам объекта заполняет наборы значений доступа
// в таблице с полями:
//    НомерНабора     - Число                                     (необязательно, если набор один),
//    ВидДоступа      - ПланВидовХарактеристикСсылка.ВидыДоступа, (обязательно),
//    ЗначениеДоступа - Неопределено, СправочникСсылка или др.    (обязательно),
//    Чтение          - Булево (необязательно, если набор для всех прав) устанавливается для одной строки набора,
//    Добавление      - Булево (необязательно, если набор для всех прав) устанавливается для одной строки набора,
//    Изменение       - Булево (необязательно, если набор для всех прав) устанавливается для одной строки набора,
//    Удаление        - Булево (необязательно, если набор для всех прав) устанавливается для одной строки набора,
//
//  Вызывается из процедуры УправлениеДоступомСлужебный.ЗаписатьНаборыЗначенийДоступа(),
// если объект зарегистрирован в "ПодпискаНаСобытие.ЗаписатьНаборыЗначенийДоступа" и
// из таких же процедур объектов, у которых наборы значений доступа зависят от наборов этого
// объекта (в этом случае объект зарегистрирован в "ПодпискаНаСобытие.ЗаписатьЗависимыеНаборыЗначенийДоступа").
//
// Параметры:
//  Таблица      - ТабличнаяЧасть,
//                 РегистрСведенийНаборЗаписей.НаборыЗначенийДоступа,
//                 ТаблицаЗначений, возвращаемая УправлениеДоступом.ТаблицаНаборыЗначенийДоступа().
//
Процедура ЗаполнитьНаборыЗначенийДоступа(Таблица) Экспорт
	
	ЗарплатаКадры.ЗаполнитьНаборыПоОрганизацииИФизическимЛицам(ЭтотОбъект, Таблица, "Организация", "ФизическоеЛицо");
	
КонецПроцедуры

// Подсистема "Управление доступом".

#КонецОбласти

#Область ОбработчикиСобытий

Процедура ОбработкаПроверкиЗаполнения(Отказ, ПроверяемыеРеквизиты)
	
	Если ПолучитьФункциональнуюОпцию("ИспользоватьРасчетЗарплатыРасширенная") Тогда 
		
		ЗарплатаКадрыРасширенный.ПроверитьУтверждениеДокумента(ЭтотОбъект, Отказ);
		
		Если Не ДокументЗаполненПравильно(ЭтотОбъект, Истина, ПроверяемыеРеквизиты) Тогда
			Отказ = Истина;
			Возврат;
		КонецЕсли;
		
		Если Не ДокументРассчитан Тогда 
			ОбщегоНазначенияКлиентСервер.УдалитьЗначениеИзМассива(ПроверяемыеРеквизиты, "ВидОплатыПособия");
			ОбщегоНазначенияКлиентСервер.УдалитьЗначениеИзМассива(ПроверяемыеРеквизиты, "ВидОплатыЗаСчетРаботодателя");
			ОбщегоНазначенияКлиентСервер.УдалитьЗначениеИзМассива(ПроверяемыеРеквизиты, "ВидНеоплачиваемогоВремени");
			ОбщегоНазначенияКлиентСервер.УдалитьЗначениеИзМассива(ПроверяемыеРеквизиты, "ПланируемаяДатаВыплаты");
			ОбщегоНазначенияКлиентСервер.УдалитьЗначениеИзМассива(ПроверяемыеРеквизиты, "ДнейОплаты");
		Иначе
			ИсправлениеДокументовЗарплатаКадры.ПроверитьЗаполнение(ЭтотОбъект, ПроверяемыеРеквизиты, Отказ);
			
			ПроверитьПериодДействияНачислений(Отказ);
			
			Если Отказ Тогда
				Возврат;
			КонецЕсли;
			
			РасчетЗарплатыРасширенный.ПроверитьПересечениеФактическогоПериодаДействия(Организация, ПериодРегистрации, Ссылка, Начисления, НачисленияПерерасчет, , , Отказ);
			
			МассивНачисленийДокумента = Новый Массив;
			ОбщегоНазначенияКлиентСервер.ДополнитьМассив(МассивНачисленийДокумента, Начисления.ВыгрузитьКолонку("Начисление"), Истина);
			ОбщегоНазначенияКлиентСервер.ДополнитьМассив(МассивНачисленийДокумента, НачисленияПерерасчет.ВыгрузитьКолонку("Начисление"), Истина);
			Если Не УчетНДФЛРасширенный.ДатаВыплатыОбязательнаКЗаполнению(ПорядокВыплаты, МассивНачисленийДокумента) Тогда
				ОбщегоНазначенияКлиентСервер.УдалитьЗначениеИзМассива(ПроверяемыеРеквизиты, "ПланируемаяДатаВыплаты");
			КонецЕсли;
			
			Если Не НазначитьПособие Тогда
				ОбщегоНазначенияКлиентСервер.УдалитьЗначениеИзМассива(ПроверяемыеРеквизиты, "ДнейОплаты");
			КонецЕсли;
			
			Если Не (ПричинаНетрудоспособности = ПредопределенноеЗначение("Перечисление.ПричиныНетрудоспособности.ОбщееЗаболевание")
				И Не ЗначениеЗаполнено(ДатаНачалаПоловиннойОплаты) 
				И ДатаНачалаСобытия + УчетПособийСоциальногоСтрахованияКлиентСервер.КоличествоДнейЗаСчетРаботодателя(ДатаНачалаСобытия) * УчетПособийСоциальногоСтрахованияКлиентСервер.ДлинаСуток() > ДатаНачалаОплаты) Тогда
				ОбщегоНазначенияКлиентСервер.УдалитьЗначениеИзМассива(ПроверяемыеРеквизиты, "ВидОплатыЗаСчетРаботодателя");
			КонецЕсли;
			
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры

Процедура ПередЗаписью(Отказ, РежимЗаписи, РежимПроведения)
	
	Если ЗарплатаКадры.ОтключитьБизнесЛогикуПриЗаписи(ЭтотОбъект) Тогда
		Возврат;
	КонецЕсли;
	
	Для Каждого СтрокаТабличнойЧасти Из Начисления Цикл
		СтрокаТабличнойЧасти.Сотрудник = Сотрудник;
	КонецЦикла;
	
	Для Каждого СтрокаТабличнойЧасти Из НачисленияПерерасчет Цикл
		СтрокаТабличнойЧасти.Сотрудник = Сотрудник;
	КонецЦикла;
	
	ПредставлениеПериода = ЗарплатаКадрыРасширенный.ПредставлениеПериодаРасчетногоДокумента(ДатаНачала, ДатаОкончания);
	
	УстановитьПривилегированныйРежим(Истина);
	
	ПособиеВыплачиваетсяФСС = УчетПособийСоциальногоСтрахованияРасширенный.ПособиеПлатитУчастникПилотногоПроекта(Организация, ПериодРегистрации)
	И УчетПособийСоциальногоСтрахованияРасширенный.КоллекцияСодержитПособияЗаСчетФСС(Начисления.ВыгрузитьКолонку("Начисление"));
	
	УстановитьПривилегированныйРежим(Ложь);
	
	ЗарплатаКадрыРасширенный.ПередЗаписьюМногофункциональногоДокумента(ЭтотОбъект, Отказ, РежимЗаписи, РежимПроведения);
	
КонецПроцедуры

Процедура ОбработкаПроведения(Отказ, РежимПроведения)
	
	ПроведениеСервер.ПодготовитьНаборыЗаписейКРегистрацииДвижений(ЭтотОбъект);
	ИсправлениеПериодическихСведений.ИсправлениеПериодическихСведений(ЭтотОбъект, Отказ, РежимПроведения);
	
	ДанныеДляПроведения = ДанныеДляПроведения();
	
	Если ДокументРассчитан Тогда 
		
		РасчетЗарплатыРасширенный.СформироватьДвиженияНачислений(
			Движения, Отказ, Организация, КонецМесяца(ПериодРегистрации), ДанныеДляПроведения.Начисления, ДанныеДляПроведения.ПоказателиНачислений);
			
		РасчетЗарплатыРасширенный.СформироватьДвиженияУдержаний(
			Движения, Отказ, Организация, КонецМесяца(ПериодРегистрации), ДанныеДляПроведения.Удержания, ДанныеДляПроведения.ПоказателиУдержаний);
		ИсполнительныеЛисты.СформироватьУдержанияПоИсполнительнымДокументам(Движения, ДанныеДляПроведения.УдержанияПоИсполнительнымДокументам);
		
		УчетНачисленнойЗарплаты.ЗарегистрироватьНачисленияУдержания(
			Движения, Отказ, Организация, ПериодРегистрации, ДанныеДляПроведения.НачисленияПоСотрудникам, ДанныеДляПроведения.УдержанияПоСотрудникам, Неопределено, Неопределено, ПорядокВыплаты);
		
		УчетНачисленнойЗарплаты.ЗарегистрироватьОтработанноеВремя(Движения, Отказ, Организация, ПериодРегистрации, ДанныеДляПроведения.ОтработанноеВремяПоСотрудникам, Истина);	
			
		УчетНДФЛРасширенный.ЗарегистрироватьДоходыИСуммыНДФЛПоВременнойТаблицеНачислений(
			Ссылка, Движения, Отказ, Организация, Дата, ПериодРегистрации, ПорядокВыплаты, ПланируемаяДатаВыплаты, ДанныеДляПроведения, Истина);
			
		// - Регистрация начислений и удержаний.
		ОтражениеЗарплатыВБухучетеРасширенный.СформироватьДвиженияБухучетНачисленияУдержанияПоСотрудникам(
					Движения, Отказ, Организация, ПериодРегистрации,
					ДанныеДляПроведения.НачисленияПоСотрудникам,
					ДанныеДляПроведения.УдержанияПоСотрудникам,
					ДанныеДляПроведения.НДФЛПоСотрудникам,
					РасчетЗарплатыРасширенный.ЭтоМежрасчетнаяВыплата(ПорядокВыплаты));
	
		// - Регистрация начислений в доходах для страховых взносов.
		УчетСтраховыхВзносов.СформироватьСведенияОДоходахСтраховыеВзносы(
			Движения, Отказ, Организация, ПериодРегистрации, ДанныеДляПроведения.МенеджерВременныхТаблиц, Ложь, Истина, Ссылка);
			
			УчетСтраховыхВзносов.СформироватьИсчисленныеВзносы(
			Движения, Отказ, Организация, ПериодРегистрации, ДанныеДляПроведения.СтраховыеВзносы);           //1С-Минск
			
		УчетСтраховыхВзносов.СформироватьСтраховыеВзносыПоФизическимЛицам(
			Движения, Отказ, Организация, ПериодРегистрации, Ссылка, ДанныеДляПроведения.СтраховыеВзносы);   //1С-Минск
			
			//- Регистрация пенсионного фонда
    	УчетНачисленнойЗарплаты.ЗарегистрироватьПенсионныйФонд(Движения, Отказ,Организация, ПериодРегистрации,  ДанныеДляПроведения.СтраховыеВзносы, ДанныеДляПроведения.МенеджерВременныхТаблиц, Перечисления.ХарактерВыплатыЗарплаты.Зарплата); //1С-Минск


		// Корректировки данных для среднего заработка.
		УчетПособийСоциальногоСтрахованияРасширенный.ЗаписатьКорректировкиСреднегоЗаработкаФСС(
			Организация,
			Сотрудник, 
			СреднийЗаработокФСС.Выгрузить(Новый Структура("Корректировка", Истина)), 
			ПериодРасчетаСреднегоЗаработкаНачало,
			ПериодРасчетаСреднегоЗаработкаОкончание,
			ОтработанноеВремяДляСреднегоФСС.Выгрузить(Новый Структура("Корректировка", Истина)), 
			ПереноситьДанныеВДругойУчетСреднегоЗаработка);
		
		// Учет среднего заработка (регистрация начислений).
		УчетСреднегоЗаработка.ЗарегистрироватьДанныеСреднегоЗаработка(Движения, Отказ, ДанныеДляПроведения.НачисленияДляСреднегоЗаработка);
		
		// - регистрация пособий
		УчетСтраховыхВзносов.СформироватьПособия(Движения, Отказ, Организация, ПериодРегистрации, ДанныеДляПроведения.Пособия, Неопределено);
		
		// Займы
		// - взаиморасчеты по займам
		ЗаймыСотрудникам.ЗарегистрироватьВзаиморасчетыПоЗаймам(Движения, ДанныеДляПроведения.ВзаиморасчетыПоЗаймам, Отказ);
		
		// - Регистрация займов в учете заработной платы.
		УчетНачисленнойЗарплатыРасширенный.ЗарегистрироватьПогашениеЗаймов(Движения, Отказ, Организация, ПериодРегистрации, ДанныеДляПроведения.УдержанияЗаймов, ПорядокВыплаты);
		
		//- Регистрация пенсионного фонда
    	УчетНачисленнойЗарплаты.ЗарегистрироватьПенсионныйФонд(Движения, Отказ, Организация, ПериодРегистрации,  ДанныеДляПроведения.СтраховыеВзносы, ДанныеДляПроведения.МенеджерВременныхТаблиц, Перечисления.ХарактерВыплатыЗарплаты.Зарплата);	//1С-Минск

		
		// - Регистрация материальной выгоды в учете НДФЛ.
		ДатаОперацииПоНалогам = НачалоДня(ДатаНачалаСобытия) - 1;
		УчетНДФЛ.СформироватьДоходыНДФЛПоКодамДоходовИзТаблицыЗначений(Движения, Отказ, Организация, ДатаОперацииПоНалогам, ДанныеДляПроведения.МатериальнаяВыгода, Ложь);
		УчетНДФЛ.СформироватьНалогиВычеты(Движения, Отказ, Организация, ДатаОперацииПоНалогам, ДанныеДляПроведения.НалогНаМатериальнуюВыгоду);
		УчетНачисленнойЗарплаты.ЗарегистрироватьНДФЛ(Движения, Отказ, Организация, ПериодРегистрации, ДанныеДляПроведения.НалогНаМатериальнуюВыгоду, ДанныеДляПроведения.МенеджерВременныхТаблиц, ПорядокВыплаты);
		
		Если ОбщегоНазначения.ПодсистемаСуществует("ЗарплатаКадрыПриложения.ГосударственнаяСлужба.РасчетДенежногоСодержания") Тогда
			Модуль = ОбщегоНазначения.ОбщийМодуль("РасчетДенежногоСодержания");
			Модуль.ЗарегистрироватьНачисленияДляРасчетаСохраняемогоДенежногоСодержания(Движения, Отказ, ПериодРегистрации, ДанныеДляПроведения.НачисленияДляРегистрацииДенежногоСодержания);
		КонецЕсли;
		
	КонецЕсли;
	
	ОстаткиОтпусков.СформироватьДвиженияФактическихОтпусков(Движения, Отказ, Дата, ДанныеДляПроведения.Начисления);
	
	СостоянияСотрудников.ЗарегистрироватьСостоянияСотрудников(Движения, Ссылка, ДанныеДляПроведения.ДанныеСостоянийСотрудников);
	
	УчетСтажаФСЗН.ЗарегистрироватьПериодыВУчетеСтажаФСЗН(Движения, ДанныеДляРегистрацииВУчетаСтажаФСЗН());
	
	Если ОсвобождатьСтавку Тогда
	   КадровыйУчетРасширенный.ОсвободитьСтавкуВременно(Движения, ДанныеДляПроведения.ПериодыОсвобожденияСтавки);
	КонецЕсли;

	// Получение признака о том, что нужно удалить перерасчеты текущего периода
	УдалитьПерерасчетыСреднегоЗаработка = Неопределено;
	ДополнительныеСвойства.Свойство("УдалитьПерерасчетыСреднегоЗаработка", УдалитьПерерасчетыСреднегоЗаработка);
	Если УдалитьПерерасчетыСреднегоЗаработка = Истина Тогда
		УчетСреднегоЗаработка.УдалитьПричиныПерерасчетов(Ссылка);
	КонецЕсли;
	
КонецПроцедуры

// В качестве данных заполнения может принимать структуру с полями.
//		Ссылка
//		Действие
Процедура ОбработкаЗаполнения(ДанныеЗаполнения, СтандартнаяОбработка)
	
	Если ТипЗнч(ДанныеЗаполнения) = Тип("СправочникСсылка.Сотрудники") Тогда
		ЗарплатаКадры.ЗаполнитьПоОснованиюСотрудником(ЭтотОбъект, ДанныеЗаполнения);
	ИначеЕсли ТипЗнч(ДанныеЗаполнения) = Тип("Структура") Тогда
		Если ДанныеЗаполнения.Свойство("Действие") И ДанныеЗаполнения.Действие = "Исправить" Тогда
			
			ИсправлениеДокументовЗарплатаКадры.СкопироватьДокумент(ЭтотОбъект, 
												ДанныеЗаполнения.Ссылка, 
												"ДокументРассчитан", 
												"Начисления,НачисленияПерерасчет,НДФЛ,ОтработанноеВремяДляСреднегоФСС,
												|ПогашениеЗаймов,Показатели,ПримененныеВычетыНаДетейИИмущественные,
												|РаспределениеРезультатовНачислений,РаспределениеРезультатовУдержаний,
												|СреднийЗаработокДанныеСтрахователей,СреднийЗаработокФСС,Удержания");
												
			ИсправленныйДокумент = ДанныеЗаполнения.Ссылка;
			
			ЗарплатаКадрыРасширенный.ПриКопированииМногофункциональногоДокумента(ЭтотОбъект);
			
		КонецЕсли;
	ИначеЕсли ТипЗнч(ДанныеЗаполнения) = Тип("ДокументСсылка.БольничныйЛист") Тогда
		ЭтотОбъект.ЯвляетсяПродолжениемБолезни = Истина;
		ЭтотОбъект.ПервичныйБольничныйЛист = ДанныеЗаполнения;
		ЗаполнитьПоПервичномуБольничномуЛисту(ДанныеЗаполнения);	 
	КонецЕсли;
	
	ЗарплатаКадрыРасширенный.ОбработкаЗаполненияМногофункциональногоДокумента(ЭтотОбъект, ДанныеЗаполнения, СтандартнаяОбработка);
	
КонецПроцедуры

Процедура ПриКопировании(ОбъектКопирования)
	
	ЗарплатаКадрыРасширенный.ПриКопированииМногофункциональногоДокумента(ЭтотОбъект);

КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

Функция ДокументЗаполненПравильно(Объект, ВыводитьСообщения = Истина, ПроверяемыеРеквизиты = Неопределено) Экспорт
	
	ТекстСообщения 			= "";
	СтруктураСообщений  	= Новый Соответствие;
	ДокументГотовКРасчету 	= Истина;
		
	// ПериодРегистрации
	Если Не ЗначениеЗаполнено(Объект.ПериодРегистрации) Тогда
		ТекстСообщения = НСтр("ru = 'Не указан период регистрации.'");
		СтруктураСообщений.Вставить("ПериодРегистрации", ТекстСообщения);
	КонецЕсли;

	//  Организация
	Если Не ЗначениеЗаполнено(Объект.Организация) Тогда
		ТекстСообщения = НСтр("ru = 'Не указана организация, по которой выполняется начисление.'");
		СтруктураСообщений.Вставить("Организация", ТекстСообщения);
	КонецЕсли;

	// Сотрудник
	Если Не ЗначениеЗаполнено(Объект.Сотрудник) Тогда
		ТекстСообщения = НСтр("ru = 'Не выбран сотрудник.'");
		СтруктураСообщений.Вставить("Сотрудник", ТекстСообщения);
	КонецЕсли;

	Если Не ЗначениеЗаполнено(Объект.ДатаНачала) И Не ЗначениеЗаполнено(Объект.ДатаОкончания) Тогда
		ТекстСообщения = НСтр("ru = 'Не указаны даты освобождения от работы.'");
		СтруктураСообщений.Вставить("ДатаОкончания",ТекстСообщения);
	Иначе
		Если Не ЗначениеЗаполнено(Объект.ДатаНачала) И ЗначениеЗаполнено(Объект.ДатаОкончания) Тогда
			ТекстСообщения = НСтр("ru = 'Не заполнена дата начала освобождения от работы.'");
			СтруктураСообщений.Вставить("ДатаНачала",ТекстСообщения);
		КонецЕсли;
		
		Если Не ЗначениеЗаполнено(Объект.ДатаОкончания) И ЗначениеЗаполнено(Объект.ДатаНачала) Тогда
			ТекстСообщения = НСтр("ru = 'Не заполнена дата окончания освобождения от работы.'");
			СтруктураСообщений.Вставить("ДатаОкончания",ТекстСообщения);
		КонецЕсли;
		
		Если ЗначениеЗаполнено(Объект.ДатаНачала) И ЗначениеЗаполнено(Объект.ДатаОкончания) И Объект.ДатаНачала > Объект.ДатаОкончания Тогда
			ТекстСообщения = НСтр("ru = 'Дата окончания освобождения от работы не может быть меньше даты начала.'");
			СтруктураСообщений.Вставить("ДатаОкончания",ТекстСообщения);
		КонецЕсли;
		
	КонецЕсли;
	
	Если ДокументРассчитан Тогда	
		// ДатаНачалаСобытия
		Если Не ЗначениеЗаполнено(Объект.ДатаНачалаСобытия) Тогда
			ТекстСообщения = НСтр("ru = 'Не указана дата начала нетрудоспособности.'");
			СтруктураСообщений.Вставить("ДатаНачалаСобытия", ТекстСообщения);
		КонецЕсли;
		
		// Период нетрудоспособности
		Если Объект.НазначитьПособие Тогда
			Если Не ЗначениеЗаполнено(Объект.ДатаНачалаОплаты) И Не ЗначениеЗаполнено(Объект.ДатаОкончанияОплаты) Тогда
				ТекстСообщения = НСтр("ru = 'Не указаны даты оплаты пособия.'");
				СтруктураСообщений.Вставить("ДатаНачалаОплаты", ТекстСообщения);
			Иначе
				Если Не ЗначениеЗаполнено(Объект.ДатаНачалаОплаты) И ЗначениеЗаполнено(Объект.ДатаОкончанияОплаты) Тогда
					ТекстСообщения = НСтр("ru = 'Не заполнена дата начала оплаты пособия.'");
					СтруктураСообщений.Вставить("ДатаНачалаОплаты", ТекстСообщения);
				КонецЕсли;
				
				Если Не ЗначениеЗаполнено(Объект.ДатаОкончанияОплаты) И ЗначениеЗаполнено(Объект.ДатаНачалаОплаты) Тогда
					ТекстСообщения = НСтр("ru = 'Не заполнена дата окончания оплаты пособия.'");
					СтруктураСообщений.Вставить("ДатаОкончанияОплаты", ТекстСообщения);
				КонецЕсли;
				
				Если ЗначениеЗаполнено(Объект.ДатаНачалаОплаты) И ЗначениеЗаполнено(Объект.ДатаНачалаСобытия) И Объект.ДатаНачалаОплаты < Объект.ДатаНачалаСобытия Тогда
					ТекстСообщения = НСтр("ru = 'Дата назначения пособия не должна быть меньше даты начала события.'");
					СтруктураСообщений.Вставить("ДатаНачалаОплаты", ТекстСообщения);
				КонецЕсли;
				
				Если ЗначениеЗаполнено(Объект.ДатаНачалаОплаты) И ЗначениеЗаполнено(Объект.ДатаНачала) И Объект.ДатаНачалаОплаты < Объект.ДатаНачала Тогда
					ТекстСообщения = НСтр("ru = 'Дата назначения пособия не должна быть меньше даты начала периода нетрудоспособности.'");
					СтруктураСообщений.Вставить("ДатаНачалаОплаты", ТекстСообщения);
				КонецЕсли;
				
				Если ЗначениеЗаполнено(Объект.ДатаНачалаОплаты) И ЗначениеЗаполнено(Объект.ДатаОкончанияОплаты) И Объект.ДатаНачалаОплаты > Объект.ДатаОкончанияОплаты Тогда
					ТекстСообщения = НСтр("ru = 'Дата окончания оплаты пособия не должна быть меньше даты начала оплаты пособия.'");
					СтруктураСообщений.Вставить("ДатаОкончанияОплаты", ТекстСообщения);
				КонецЕсли;
				
				Если ЗначениеЗаполнено(Объект.ДатаОкончанияОплаты) И ЗначениеЗаполнено(Объект.ДатаОкончания) И Объект.ДатаОкончания < Объект.ДатаОкончанияОплаты Тогда
					ТекстСообщения = НСтр("ru = 'Дата окончания оплаты пособия не должна быть больше даты окончания периода нетрудоспособности.'");
					СтруктураСообщений.Вставить("ДатаОкончанияОплаты", ТекстСообщения);
				КонецЕсли;
				
				Если ЗначениеЗаполнено(Объект.ДатаНачалаОплаты) И ЗначениеЗаполнено(Объект.ДатаОкончания) И Объект.ДатаОкончания < Объект.ДатаНачалаОплаты Тогда
					ТекстСообщения = НСтр("ru = 'Дата окончания периода отсутствия сотрудника не должна быть меньше даты назначения пособия.'");
					СтруктураСообщений.Вставить("ДатаОкончания", ТекстСообщения);
				КонецЕсли;
				
				Если Не ЗначениеЗаполнено(Объект.ПричинаНетрудоспособности) Тогда
					ТекстСообщения = НСтр("ru = 'Не указана причина нетрудоспособности.'");
					СтруктураСообщений.Вставить("ПричинаНетрудоспособности", ТекстСообщения);
				//ИначеЕсли Объект.ПричинаНетрудоспособности = Перечисления.ПричиныНетрудоспособности.ПоУходуЗаРебенком И Не ЗначениеЗаполнено(Объект.СлучайУходаЗаБольнымРебенком) Тогда
				//	ТекстСообщения = НСтр("ru = 'Не указан случай ухода за ребенком.'");
				//	СтруктураСообщений.Вставить("СлучайУходаЗаБольнымРебенком", ТекстСообщения);
				КонецЕсли;
				
				// Процент оплаты
				Если Не ЗначениеЗаполнено(Объект.ПроцентОплаты) Тогда
					ТекстСообщения = НСтр("ru = 'Не указан процент оплаты.'");
					СтруктураСообщений.Вставить("ПроцентОплаты", ТекстСообщения);
				КонецЕсли;
				
				Если Не ЗначениеЗаполнено(Объект.ОграничениеПособия) Тогда
					ТекстСообщения = НСтр("ru = 'Не указано ограничение пособия.'");
					СтруктураСообщений.Вставить("ОграничениеПособия", ТекстСообщения);
				КонецЕсли;
				
				//Если Объект.ПрименятьЛьготыПриНачисленииПособия И Не ЗначениеЗаполнено(Объект.ФинансированиеФедеральнымБюджетом) Тогда
				//	ТекстСообщения = НСтр("ru = 'Не указан вид льготы используемой при начислении пособия.'");
				//	СтруктураСообщений.Вставить("ФинансированиеФедеральнымБюджетом", ТекстСообщения);
				//КонецЕсли;
				
				Если СтруктураСообщений.Количество() = 0 Тогда
					Если Не ЗначениеЗаполнено(Объект.ВидОплатыПособия) Тогда
						ТекстСообщения = НСтр("ru = 'Не указан вид расчета для начисления пособия.'");
						СтруктураСообщений.Вставить("ВидОплатыПособия", ТекстСообщения);
					КонецЕсли;	
					
					//Если Объект.ПричинаНетрудоспособности = Перечисления.ПричиныНетрудоспособности.ОбщееЗаболевание Тогда
					//	Если Не ЗначениеЗаполнено(Объект.ВидОплатыЗаСчетРаботодателя) Тогда
					//		ТекстСообщения = НСтр("ru = 'Не указан вид расчета для начисления пособия за счет работодателя.'");
					//		СтруктураСообщений.Вставить("ВидОплатыЗаСчетРаботодателя", ТекстСообщения);
					//	КонецЕсли;
					//КонецЕсли;
					
					Если Объект.ДоплачиватьДоСреднегоЗаработка И Не ЗначениеЗаполнено(Объект.ВидРасчетаДоплаты) Тогда
						ТекстСообщения = НСтр("ru = 'Не указан вид расчета для начисления доплаты до полного среднего заработка.'");
						СтруктураСообщений.Вставить("ВидРасчетаДоплаты", ТекстСообщения);
					КонецЕсли;
				КонецЕсли;
				
			КонецЕсли;   
		КонецЕсли;
		
		Если ЗначениеЗаполнено(Объект.ДатаНачала) И ЗначениеЗаполнено(Объект.ДатаНачалаСобытия) И Объект.ДатаНачала < Объект.ДатаНачалаСобытия Тогда
			ТекстСообщения = НСтр("ru = 'Дата начала периода отсутствия сотрудника не должна быть меньше даты начала события.'");
			СтруктураСообщений.Вставить("ДатаНачала", ТекстСообщения);
		КонецЕсли;
		
		ЭтоУчастникПилотногоПроектаБезДатыВступленияВПроект = УчетПособийСоциальногоСтрахованияРасширенный.ЭтоУчастникПилотногоПроектаБезДатыВступленияВПроект(Организация, ДатаНачалаСобытия);
		Если ЭтоУчастникПилотногоПроектаБезДатыВступленияВПроект Тогда
			ТекстСообщения = УчетПособийСоциальногоСтрахованияРасширенный.ТекстСообщенияЭтоУчастникПилотногоПроектаБезДатыВступленияВПроект(Организация);
			СтруктураСообщений.Вставить("Организация", ТекстСообщения);
		КонецЕсли;
		
	КонецЕсли;
	
	ДокументЗаполненПравильно = СтруктураСообщений.Количество() = 0;
	
	Если ВыводитьСообщения И Не ДокументЗаполненПравильно Тогда
		Для Каждого Сообщение Из СтруктураСообщений Цикл
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(Сообщение.Значение, ЭтотОбъект, Сообщение.Ключ);
		КонецЦикла;
	КонецЕсли;
	
	Если ПроверяемыеРеквизиты <> Неопределено Тогда
		ОбщегоНазначенияКлиентСервер.УдалитьЗначениеИзМассива(ПроверяемыеРеквизиты, "Организация");
		ОбщегоНазначенияКлиентСервер.УдалитьЗначениеИзМассива(ПроверяемыеРеквизиты, "Сотрудник");
		ОбщегоНазначенияКлиентСервер.УдалитьЗначениеИзМассива(ПроверяемыеРеквизиты, "ДатаНачалаСобытия");
		ОбщегоНазначенияКлиентСервер.УдалитьЗначениеИзМассива(ПроверяемыеРеквизиты, "ДатаНачалаОплаты");
		ОбщегоНазначенияКлиентСервер.УдалитьЗначениеИзМассива(ПроверяемыеРеквизиты, "ДатаНачала");
		ОбщегоНазначенияКлиентСервер.УдалитьЗначениеИзМассива(ПроверяемыеРеквизиты, "ДатаОкончания");
		ОбщегоНазначенияКлиентСервер.УдалитьЗначениеИзМассива(ПроверяемыеРеквизиты, "ДатаОкончанияОплаты");
		ОбщегоНазначенияКлиентСервер.УдалитьЗначениеИзМассива(ПроверяемыеРеквизиты, "ПричинаНетрудоспособности");
		ОбщегоНазначенияКлиентСервер.УдалитьЗначениеИзМассива(ПроверяемыеРеквизиты, "ПроцентОплаты");
		ОбщегоНазначенияКлиентСервер.УдалитьЗначениеИзМассива(ПроверяемыеРеквизиты, "ОграничениеПособия");
		ОбщегоНазначенияКлиентСервер.УдалитьЗначениеИзМассива(ПроверяемыеРеквизиты, "ФинансированиеФедеральнымБюджетом");
	КонецЕсли;
	
	Возврат ДокументЗаполненПравильно;	
	
КонецФункции

Процедура ПроверитьПериодДействияНачислений(Отказ)
	ПараметрыПроверкиПериодаДействия = РасчетЗарплатыРасширенный.ПараметрыПроверкиПериодаДействия();
	ПараметрыПроверкиПериодаДействия.Ссылка = ЭтотОбъект.Ссылка;
	ПроверяемыеКоллекции = Новый Массив;
	ПроверяемыеКоллекции.Добавить(РасчетЗарплатыРасширенный.ОписаниеКоллекцииДляПроверкиПериодаДействия("НачисленияПерерасчет", "Перерасчет прошлого периода"));
	ПроверяемыеКоллекции.Добавить(РасчетЗарплатыРасширенный.ОписаниеКоллекцииДляПроверкиПериодаДействия("Удержания", "Удержания", "Удержание"));
	РасчетЗарплатыРасширенный.ПроверитьПериодДействияВКоллекцияхНачислений(ЭтотОбъект, ПараметрыПроверкиПериодаДействия, ПроверяемыеКоллекции, Отказ);
КонецПроцедуры

Функция ДанныеДляПроведения()
	
	ДанныеДляПроведения = РасчетЗарплаты.СоздатьДанныеДляПроведенияНачисленияЗарплаты();
	
	РасчетЗарплатыРасширенный.ЗаполнитьНачисления(ДанныеДляПроведения, Ссылка, "Начисления,НачисленияПерерасчет", "Ссылка.ПериодРегистрации");
	РасчетЗарплатыРасширенный.ЗаполнитьУдержания(ДанныеДляПроведения, Ссылка);
	РасчетЗарплатыРасширенный.ЗаполнитьСписокФизическихЛиц(ДанныеДляПроведения, Ссылка);
	РасчетЗарплаты.ЗаполнитьДанныеНДФЛ(ДанныеДляПроведения, Ссылка);
	ЗаполнитьСведенияОПособиях(ДанныеДляПроведения);
	
	ОтражениеЗарплатыВБухучете.ДополнитьНачисленияДаннымиОЕНВД(ДанныеДляПроведения, Ссылка, ПериодРегистрации, Организация);
	
	УчетСреднегоЗаработка.ЗаполнитьТаблицыДляРегистрацииДанныхСреднегоЗаработка(ДанныеДляПроведения, Ссылка, ПериодРегистрации, "Начисления,НачисленияПерерасчет", "ДатаНачала");
	
	ЗаймыСотрудникам.ЗаполнитьДанныеДляПроведенияПоЗаймам(ДанныеДляПроведения, Ссылка, НачалоДня(ДатаНачалаСобытия) - 1, "Ссылка.ПериодРегистрации");
	
	ДругиеСотрудники = КадровыйУчетРасширенный.ДругиеСотрудникиФизическогоЛица(
		ФизическоеЛицо, Организация, Сотрудник, ДатаНачала, ДатаОкончания);
	
	//ДополнитьНачисленияОтсуствиямиСовместителей(ДанныеДляПроведения, ДругиеСотрудники);
	
	ЗаполнитьДанныеСостоянийСотрудника(ДанныеДляПроведения, ДругиеСотрудники);
	
	РасчетЗарплаты.ЗаполнитьДанныеСтраховыхВзносов(ДанныеДляПроведения, Ссылка);

	
	Если ОсвобождатьСтавку Тогда
		МассивСотрудников = ОбщегоНазначенияКлиентСервер.ЗначениеВМассиве(Сотрудник);
		ОбщегоНазначенияКлиентСервер.ДополнитьМассив(МассивСотрудников, ДругиеСотрудники);
		КадровыйУчетРасширенный.ЗаполнитьПериодыОсвобожденияСтавки(ДанныеДляПроведения, МассивСотрудников, ДатаНачала, КонецДня(ДатаОкончания) + 1);
		
		Если ЗначениеЗаполнено(ИсправленныйДокумент) Тогда
			ДанныеИсправленногоДокумента = ОбщегоНазначения.ЗначенияРеквизитовОбъекта(ИсправленныйДокумент, "ФизическоеЛицо,Организация,Сотрудник,ДатаНачала,ДатаОкончания");
			
			ДругиеСотрудникиИсправленногоДокумента = КадровыйУчетРасширенный.ДругиеСотрудникиФизическогоЛица(ДанныеИсправленногоДокумента.ФизическоеЛицо, 
																		ДанныеИсправленногоДокумента.Организация, ДанныеИсправленногоДокумента.Сотрудник, 
																		ДанныеИсправленногоДокумента.ДатаНачала, ДанныеИсправленногоДокумента.ДатаОкончания);

			МассивСотрудниковИсправленногоДокумента = ОбщегоНазначенияКлиентСервер.ЗначениеВМассиве(ДанныеИсправленногоДокумента.Сотрудник);
			ОбщегоНазначенияКлиентСервер.ДополнитьМассив(МассивСотрудниковИсправленногоДокумента, ДругиеСотрудникиИсправленногоДокумента);
			КадровыйУчетРасширенный.ЗаполнитьПериодыОсвобожденияСтавки(ДанныеДляПроведения, МассивСотрудниковИсправленногоДокумента, ДанныеИсправленногоДокумента.ДатаНачала,  КонецДня(ДанныеИсправленногоДокумента.ДатаОкончания) + 1, Истина);
		КонецЕсли;
	КонецЕсли;

	Если ОбщегоНазначения.ПодсистемаСуществует("ЗарплатаКадрыПриложения.ГосударственнаяСлужба.РасчетДенежногоСодержания") Тогда
		Модуль = ОбщегоНазначения.ОбщийМодуль("РасчетДенежногоСодержания");
		НачисленияДляРегистрацииДенежногоСодержания = Модуль.СведенияОНачисленияхДляРегистрацииДенежногоСодержанияДокумента(Ссылка, "Начисления,НачисленияПерерасчет");
		ДанныеДляПроведения.Вставить("НачисленияДляРегистрацииДенежногоСодержания", НачисленияДляРегистрацииДенежногоСодержания);
	КонецЕсли;
	
	Возврат ДанныеДляПроведения;
	
КонецФункции

Процедура ДополнитьНачисленияОтсуствиямиСовместителей(ДанныеДляПроведения, ДругиеСотрудники)
	
	Если Не ДокументРассчитан Тогда
		Возврат;
	КонецЕсли;
	
	МассивВидовРасчета = РасчетЗарплаты.НачисленияПоКатегории(Перечисления.КатегорииНачисленийИНеоплаченногоВремени.БолезньБезОплаты);
	Если МассивВидовРасчета.Количество() = 0 Тогда
		ВызватьИсключение НСтр("ru = 'Не обнаружено ни одного вида расчета с назначением «Болезнь без оплаты», 
                                |невозможно зарегистрировать отсутствие на работе для совместителей.'");
	КонецЕсли;
	
	ВидРасчетаОтсутствия = МассивВидовРасчета[0];
	
	СотрудникиНачисления = РасчетЗарплатыРасширенный.ПустаяТаблицаСотрудниковНачислений();
	Для Каждого ДругойСотрудник Из ДругиеСотрудники Цикл
		СтрокаТаблицы = СотрудникиНачисления.Добавить();
		СтрокаТаблицы.Сотрудник = ДругойСотрудник;
		СтрокаТаблицы.Начисление = ВидРасчетаОтсутствия;
		СтрокаТаблицы.ДатаНачала = ДатаНачала;
		СтрокаТаблицы.ДатаОкончания = ДатаОкончания;
	КонецЦикла;
	
	НачисленияДругимСотрудникам = РасчетЗарплатыРасширенный.ДанныеДляНачисленияЗарплаты(
		Организация, ДатаНачала, ДатаОкончания, ПериодРегистрации, , , СотрудникиНачисления);
	
	ОбщегоНазначенияКлиентСервер.ДополнитьТаблицу(НачисленияДругимСотрудникам, ДанныеДляПроведения.Начисления);
	
КонецПроцедуры

Процедура ЗаполнитьСведенияОПособиях(ДанныеДляПроведения)
	
	Запрос = Новый Запрос;
	Запрос.МенеджерВременныхТаблиц = ДанныеДляПроведения.МенеджерВременныхТаблиц;
	
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	ТаблицаНачислений.Ссылка,
	|	ТаблицаНачислений.Ссылка.ПричинаНетрудоспособности КАК ПричинаНетрудоспособности,
	|	ТаблицаНачислений.Начисление,
	|	ТаблицаНачислений.Сотрудник,
	|	ТаблицаНачислений.ВидЗанятости,
	|	ЛОЖЬ КАК Сторно,
	|	ТаблицаНачислений.ОплаченоДней,
	|	ТаблицаНачислений.Результат,
	|	ТаблицаНачислений.РезультатВТомЧислеЗаСчетФБ,
	|	0 КАК ПроцентОплатыПособия 
	|ПОМЕСТИТЬ ВТНачисленияДляУчетаПособий
	|ИЗ
	|	Документ.БольничныйЛист.Начисления КАК ТаблицаНачислений
	|ГДЕ
	|	ТаблицаНачислений.Ссылка = &Ссылка
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	ВЫБОР
	|		КОГДА ТаблицаНачислений.Сторно
	|			ТОГДА ТаблицаНачислений.СторнируемыйДокумент
	|		ИНАЧЕ ТаблицаНачислений.Ссылка
	|	КОНЕЦ,
	|	ТаблицаНачислений.Ссылка.ПричинаНетрудоспособности КАК ПричинаНетрудоспособности,
	|	ТаблицаНачислений.Начисление,
	|	ТаблицаНачислений.Сотрудник,
	|	ТаблицаНачислений.ВидЗанятости,
	|	ТаблицаНачислений.Сторно,
	|	ТаблицаНачислений.ОплаченоДней,
	|	ТаблицаНачислений.Результат,
	|	ТаблицаНачислений.РезультатВТомЧислеЗаСчетФБ,
	|	0 КАК ПроцентОплатыПособия 
	|ИЗ
	|	Документ.БольничныйЛист.НачисленияПерерасчет КАК ТаблицаНачислений
	|ГДЕ
	|	ТаблицаНачислений.Ссылка = &Ссылка";
	
	Запрос.УстановитьПараметр("Ссылка", Ссылка);
	
	Запрос.Выполнить();
	
	УчетПособийСоциальногоСтрахованияРасширенный.ЗаполнитьСведенияОПособиях(ДанныеДляПроведения);

КонецПроцедуры

Процедура ЗаполнитьПоПервичномуБольничномуЛисту(ДокументСсылка) Экспорт 
	
	ДанныеПервичногоБольничногоЛиста = Документы.БольничныйЛист.ДанныеПервичногоБольничногоЛиста(ДокументСсылка, ОбщегоНазначенияКлиентСервер.ЗначениеВМассиве(ЭтотОбъект.Ссылка));
	
	ЗаполняемыеДанные = "Организация";
	ЗаполняемыеДанные = ЗаполняемыеДанные + "," + "Сотрудник";
	ЗаполняемыеДанные = ЗаполняемыеДанные + "," + "ПричинаНетрудоспособности";
	ЗаполняемыеДанные = ЗаполняемыеДанные + "," + "НазначитьПособие";
	ЗаполняемыеДанные = ЗаполняемыеДанные + "," + "ДатаНачала";
	ЗаполняемыеДанные = ЗаполняемыеДанные + "," + "ДатаНачалаСобытия";
	ЗаполняемыеДанные = ЗаполняемыеДанные + "," + "ДатаНачалаОплаты";
	ЗаполняемыеДанные = ЗаполняемыеДанные + "," + "ДатаНачалаПоловиннойОплаты";
	ЗаполняемыеДанные = ЗаполняемыеДанные + "," + "ДатаНарушенияРежима";
	ЗаполняемыеДанные = ЗаполняемыеДанные + "," + "СлучайУходаЗаБольнымРебенком";
	ЗаполняемыеДанные = ЗаполняемыеДанные + "," + "НомерПервичногоЛисткаНетрудоспособности";
	ЗаполняемыеДанные = ЗаполняемыеДанные + "," + "СтажЛет";
	ЗаполняемыеДанные = ЗаполняемыеДанные + "," + "СтажМесяцев";
	ЗаполняемыеДанные = ЗаполняемыеДанные + "," + "СтажРасширенныйЛет";
	ЗаполняемыеДанные = ЗаполняемыеДанные + "," + "СтажРасширенныйМесяцев";
	ЗаполняемыеДанные = ЗаполняемыеДанные + "," + "ДоплачиватьДоСреднегоЗаработка";
	ЗаполняемыеДанные = ЗаполняемыеДанные + "," + "ПроцентОплаты";
	ЗаполняемыеДанные = ЗаполняемыеДанные + "," + "ПроцентОплатыБезЛьгот";
	ЗаполняемыеДанные = ЗаполняемыеДанные + "," + "ОграничениеПособияБезЛьгот";
	ЗаполняемыеДанные = ЗаполняемыеДанные + "," + "ПрименятьЛьготыПриНачисленииПособия";
	ЗаполняемыеДанные = ЗаполняемыеДанные + "," + "ФинансированиеФедеральнымБюджетом";
	ЗаполняемыеДанные = ЗаполняемыеДанные + "," + "ОграничениеПособия";
	ЗаполняемыеДанные = ЗаполняемыеДанные + "," + "ПериодРасчетаСреднегоЗаработкаНачало";
	ЗаполняемыеДанные = ЗаполняемыеДанные + "," + "ПериодРасчетаСреднегоЗаработкаОкончание";
	ЗаполняемыеДанные = ЗаполняемыеДанные + "," + "ПериодРасчетаСреднегоЗаработкаПервыйГод";
	ЗаполняемыеДанные = ЗаполняемыеДанные + "," + "ПериодРасчетаСреднегоЗаработкаВторойГод";
	ЗаполняемыеДанные = ЗаполняемыеДанные + "," + "УчитыватьЗаработокПредыдущихСтрахователей";
	ЗаполняемыеДанные = ЗаполняемыеДанные + "," + "РасчетПоПравилам2010Года";
	ЗаполняемыеДанные = ЗаполняемыеДанные + "," + "ВидОплатыПособия";
	ЗаполняемыеДанные = ЗаполняемыеДанные + "," + "ВидОплатыЗаСчетРаботодателя";
	ЗаполняемыеДанные = ЗаполняемыеДанные + "," + "ВидНеоплачиваемогоВремени";
	ЗаполняемыеДанные = ЗаполняемыеДанные + "," + "ВидРасчетаДоплаты";
	ЗаполняемыеДанные = ЗаполняемыеДанные + "," + "ПроцентДоплатыЗаДниНетрудоспособности";
	ЗаполняемыеДанные = ЗаполняемыеДанные + "," + "ДоляНеполногоВремени";
	ЗаполняемыеДанные = ЗаполняемыеДанные + "," + "РайонныйКоэффициентРФнаНачалоСобытия";
	
	Если ДанныеПервичногоБольничногоЛиста <> Неопределено Тогда
		ЗаполнитьЗначенияСвойств(ЭтотОбъект, ДанныеПервичногоБольничногоЛиста, ЗаполняемыеДанные);
	КонецЕсли;
	
	Если ПричинаНетрудоспособности = Перечисления.ПричиныНетрудоспособности.ПособиеПриДолечивании
		Или ПричинаНетрудоспособности = Перечисления.ПричиныНетрудоспособности.ПоУходуЗаВзрослым
		Или СлучайУходаЗаБольнымРебенком = Перечисления.СлучаиУходаЗаБольнымиДетьми.ПоУходуДо15тиЛетАмбулаторно 
		Или СлучайУходаЗаБольнымРебенком = Перечисления.СлучаиУходаЗаБольнымиДетьми.ПоУходуДо15тиЛетВСтационаре	Тогда
		
		ДлинаСуток = 24 * 60 * 60;
		
		Если ДатаНачалаСобытия <> ДатаНачалаОплаты Тогда
			ОплаченоРанее = (ДатаНачалаОплаты - ДатаНачалаСобытия) / ДлинаСуток;
		Иначе
			ОплаченоРанее = 0;
		КонецЕсли;
		
		// Учтем ограничение периода оплаты по одному страховому случаю.
		Если ПричинаНетрудоспособности = Перечисления.ПричиныНетрудоспособности.ПособиеПриДолечивании Тогда
			ДнейОплаты = Макс(24 - ОплаченоРанее,0) // П.2 ст.6 Федерального закона от 29 декабря 2006 г. № 255-ФЗ.
		ИначеЕсли ПричинаНетрудоспособности = Перечисления.ПричиныНетрудоспособности.ПоУходуЗаВзрослым Тогда
			ДнейОплаты = Макс(7 - ОплаченоРанее,0) // Пп. 6 п.5 ст.6 Федерального закона от 29 декабря 2006 г. № 255-ФЗ.
		ИначеЕсли СлучайУходаЗаБольнымРебенком = Перечисления.СлучаиУходаЗаБольнымиДетьми.ПоУходуДо15тиЛетАмбулаторно
			Или СлучайУходаЗаБольнымРебенком = Перечисления.СлучаиУходаЗаБольнымиДетьми.ПоУходуДо15тиЛетВСтационаре Тогда
			ДнейОплаты = Макс(15 - ОплаченоРанее,0) // Пп. 2 п.5 ст.6 Федерального закона от 29 декабря 2006 г. № 255-ФЗ.
		КонецЕсли;
		
		Если ДнейОплаты > 0 Тогда
			НазначитьПособие		= Истина; 
			ДатаОкончанияОплаты 	= ДатаНачалаОплаты + (ДнейОплаты - 1) * ДлинаСуток;
		Иначе
			НазначитьПособие		= Ложь; 
			ДатаНачалаОплаты		= Неопределено;
			ДатаОкончанияОплаты   	= Неопределено;
		КонецЕсли;                    
		
	КонецЕсли;
	
КонецПроцедуры

Процедура ЗаполнитьДанныеСостоянийСотрудника(ДанныеДляПроведения, ДругиеСотрудники)
	
	ДанныеСостояний = СостоянияСотрудников.ПустаяТаблицаДанныхСостоянийСотрудника();
	Если ПричинаНетрудоспособности = Перечисления.ПричиныНетрудоспособности.ПоБеременностиИРодам Тогда
		НоваяСтрока = ДанныеСостояний.Добавить();
		НоваяСтрока.Сотрудник = Сотрудник;
		НоваяСтрока.Состояние = Перечисления.СостоянияСотрудника.ОтпускПоБеременностиИРодам;
		НоваяСтрока.Начало = ДатаНачала;
		НоваяСтрока.Окончание = ДатаОкончания;
	Иначе
		Если Не ПолучитьФункциональнуюОпцию("ИспользоватьРасчетЗарплатыРасширенная") Тогда
			// Если расчет зарплаты отключен, то считаем весь период "оплаченной болезнью".
			НоваяСтрока = ДанныеСостояний.Добавить();
			НоваяСтрока.Сотрудник = Сотрудник;
			НоваяСтрока.Состояние = Перечисления.СостоянияСотрудника.Болезнь;
			НоваяСтрока.Начало = ДатаНачала;
			НоваяСтрока.Окончание = ДатаОкончания;
		Иначе
			Если Не НазначитьПособие Тогда
				// Весь период без оплаты
				НоваяСтрока = ДанныеСостояний.Добавить();
				НоваяСтрока.Сотрудник = Сотрудник;
				НоваяСтрока.Состояние = Перечисления.СостоянияСотрудника.БолезньБезОплаты;
				НоваяСтрока.Начало = ДатаНачала;
				НоваяСтрока.Окончание = ДатаОкончания;
			Иначе
				Если ДатаНачала < ДатаНачалаОплаты Тогда
					// Если оплата начинается позже, то интервал перед - это болезнь без оплаты.
					НоваяСтрока = ДанныеСостояний.Добавить();
					НоваяСтрока.Сотрудник = Сотрудник;
					НоваяСтрока.Состояние = Перечисления.СостоянияСотрудника.БолезньБезОплаты;
					НоваяСтрока.Начало = ДатаНачала;
					НоваяСтрока.Окончание = НачалоДня(ДатаНачалаОплаты) - 1;
				КонецЕсли;
				НоваяСтрока = ДанныеСостояний.Добавить();
				НоваяСтрока.Сотрудник = Сотрудник;
				НоваяСтрока.Состояние = Перечисления.СостоянияСотрудника.Болезнь;
				НоваяСтрока.Начало = ДатаНачалаОплаты;
				НоваяСтрока.Окончание = ДатаОкончанияОплаты;
				Если ДатаОкончанияОплаты < ДатаОкончания Тогда
					// Если оплата заканчивается раньше, то интервал - это болезнь без оплаты.
					НоваяСтрока = ДанныеСостояний.Добавить();
					НоваяСтрока.Сотрудник = Сотрудник;
					НоваяСтрока.Состояние = Перечисления.СостоянияСотрудника.БолезньБезОплаты;
					НоваяСтрока.Начало = КонецДня(ДатаОкончанияОплаты) + 1;
					НоваяСтрока.Окончание = ДатаОкончания;
				КонецЕсли;
			КонецЕсли;
		КонецЕсли;
	КонецЕсли;
	
	// По всем другим рабочим местам регистрируем болезнь без оплаты.
	//Для Каждого ДругойСотрудник Из ДругиеСотрудники Цикл
	//	НоваяСтрока = ДанныеСостояний.Добавить();
	//	НоваяСтрока.Сотрудник = ДругойСотрудник;
	//	НоваяСтрока.Состояние = Перечисления.СостоянияСотрудника.Болезнь;
	//	НоваяСтрока.Начало = ДатаНачала;
	//	НоваяСтрока.Окончание = ДатаОкончания;
	//КонецЦикла;
	
	ДанныеДляПроведения.ДанныеСостоянийСотрудников = ДанныеСостояний;
	
КонецПроцедуры

Функция ДанныеДляРегистрацииВУчетаСтажаФСЗН()
	МассивСсылок = Новый Массив;
	МассивСсылок.Добавить(Ссылка);
	
	ДанныеДляРегистрацииВУчете = Документы.БольничныйЛист.ДанныеДляРегистрацииВУчетаСтажаФСЗН(МассивСсылок);
		
	Возврат ДанныеДляРегистрацииВУчете[Ссылка];
														
КонецФункции	

#КонецОбласти

#КонецЕсли
