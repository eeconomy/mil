﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда
	
#Область СлужебныеПроцедурыИФункции
	
#Область Проведение 

Процедура ЗаполнитьПараметрыИнициализации(Запрос, ДокументСсылка)
	
	Запрос.МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
	Запрос.УстановитьПараметр("Ссылка", ДокументСсылка);
	
	Запрос.Текст =
	"ВЫБРАТЬ
	|	Реквизиты.Ссылка КАК Ссылка,
	|	Реквизиты.Дата КАК Период
	|ИЗ
	|	Документ.Мил_ВедомостьВыдачиПродукцииПоАктамУценки КАК Реквизиты
	|ГДЕ
	|	Реквизиты.Ссылка = &Ссылка";
	
	Реквизиты = Запрос.Выполнить().Выбрать();
	Реквизиты.Следующий();
	
	Запрос.УстановитьПараметр("Период", Реквизиты.Период);
	
КонецПроцедуры
	
Процедура ИнициализироватьДанныеДокумента(ДокументСсылка, ДополнительныеСвойства, Регистры = Неопределено) Экспорт
	
	// Создание запроса инициализации движений и заполнение его параметров.
	Запрос = Новый Запрос;
	ЗаполнитьПараметрыИнициализации(Запрос, ДокументСсылка);
	
	// Формирование текста запроса.
	ТекстыЗапроса = Новый СписокЗначений;
	ТекстЗапросаТаблицаРасходПоБронированиюПродукцииПоАктамРеализации(Запрос, ТекстыЗапроса, Регистры);
	// Исполнение запроса и выгрузка полученных таблиц для движений.
	ПроведениеСерверУТ.ИнициализироватьТаблицыДляДвижений(Запрос, ТекстыЗапроса, ДополнительныеСвойства.ТаблицыДляДвижений, Истина);
	
КонецПроцедуры 

Функция ТекстЗапросаТаблицаРасходПоБронированиюПродукцииПоАктамРеализации(Запрос, ТекстыЗапроса, Регистры)
	
	ИмяРегистра = "МИЛ_БронированиеПродукцииПоАктамУценкиСотрудниками";
	
	Если Не ПроведениеСерверУТ.ТребуетсяТаблицаДляДвижений(ИмяРегистра, Регистры) Тогда
		Возврат "";
	КонецЕсли; 
	
	ТекстЗапроса =
		"ВЫБРАТЬ
		|	&Период КАК Период,
		|	ЗНАЧЕНИЕ(ВидДвиженияНакопления.Расход) КАК ВидДвижения,
		|	МИЛ_ВедомостьВыдачиПродукцииПоАктамУценкиПродукция.Номенклатура КАК Номенклатура,
		|	МИЛ_ВедомостьВыдачиПродукцииПоАктамУценкиПродукция.Характеристика КАК Характеристика,
		|	СУММА(МИЛ_ВедомостьВыдачиПродукцииПоАктамУценкиПродукция.Количество) КАК Количество,
		|	МИЛ_ВедомостьВыдачиПродукцииПоАктамУценкиПродукция.Дефект КАК Дефект,
		|	МИЛ_ВедомостьВыдачиПродукцииПоАктамУценкиПродукция.ШтрихКод КАК ШтрихКод,
		|	МИЛ_ВедомостьВыдачиПродукцииПоАктамУценкиПродукция.АктУценки КАК АктУценки,
		|	МИЛ_ВедомостьВыдачиПродукцииПоАктамУценкиПродукция.НоменклатураУценки КАК НоменклатураУценки,
		|	МИЛ_ВедомостьВыдачиПродукцииПоАктамУценкиПродукция.ШтрихКодАктаУценки КАК ШтрихКодАктаУценки,
		|	МИЛ_ВедомостьВыдачиПродукцииПоАктамУценкиПродукция.Цена КАК Цена,
		|	МИЛ_ВедомостьВыдачиПродукцииПоАктамУценкиПродукция.Стоимость КАК Стоимость,
		|	МИЛ_ВедомостьВыдачиПродукцииПоАктамУценкиПродукция.Сотрудник КАК Сотрудник
		|ИЗ
		|	Документ.Мил_ВедомостьВыдачиПродукцииПоАктамУценки.Продукция КАК МИЛ_ВедомостьВыдачиПродукцииПоАктамУценкиПродукция
		|ГДЕ
		|	МИЛ_ВедомостьВыдачиПродукцииПоАктамУценкиПродукция.Ссылка = &Ссылка
		|	И МИЛ_ВедомостьВыдачиПродукцииПоАктамУценкиПродукция.Количество <> 0
		|	И МИЛ_ВедомостьВыдачиПродукцииПоАктамУценкиПродукция.Цена <> 0
		|	И МИЛ_ВедомостьВыдачиПродукцииПоАктамУценкиПродукция.Ссылка.КОплате = ИСТИНА
		|
		|СГРУППИРОВАТЬ ПО
		|	МИЛ_ВедомостьВыдачиПродукцииПоАктамУценкиПродукция.Характеристика,
		|	МИЛ_ВедомостьВыдачиПродукцииПоАктамУценкиПродукция.Номенклатура,
		|	МИЛ_ВедомостьВыдачиПродукцииПоАктамУценкиПродукция.НоменклатураУценки,
		|	МИЛ_ВедомостьВыдачиПродукцииПоАктамУценкиПродукция.АктУценки,
		|	МИЛ_ВедомостьВыдачиПродукцииПоАктамУценкиПродукция.Дефект,
		|	МИЛ_ВедомостьВыдачиПродукцииПоАктамУценкиПродукция.ШтрихКод,
		|	МИЛ_ВедомостьВыдачиПродукцииПоАктамУценкиПродукция.ШтрихКодАктаУценки,
		|	МИЛ_ВедомостьВыдачиПродукцииПоАктамУценкиПродукция.Цена,
		|	МИЛ_ВедомостьВыдачиПродукцииПоАктамУценкиПродукция.Стоимость,
		|	МИЛ_ВедомостьВыдачиПродукцииПоАктамУценкиПродукция.Сотрудник";
	
	ТекстыЗапроса.Добавить(ТекстЗапроса, ИмяРегистра);
	
	Возврат ТекстЗапроса;
	
КонецФункции
#КонецОбласти 
	
#КонецОбласти   

#Область Команды

Процедура ДобавитьКомандыОтчетов(КомандыОтчетов, Параметры) Экспорт
	
	ВариантыОтчетовУТПереопределяемый.ДобавитьКомандуСтруктураПодчиненности(КомандыОтчетов);  
	ВариантыОтчетовУТПереопределяемый.ДобавитьКомандуДвиженияДокумента(КомандыОтчетов);	  
	
КонецПроцедуры

Процедура ДобавитьКомандыПечати(КомандыПечати) Экспорт 

	КомандаПечати = КомандыПечати.Добавить();
	КомандаПечати.Идентификатор = "Этикетки";
	КомандаПечати.Представление = "Печать этикеток";
	КомандаПечати.Обработчик    = "УправлениеПечатьюБПКлиент.ВыполнитьКомандуПечати";
	КомандаПечати.СписокФорм    = "ФормаСписка,ФормаДокумента";          
	
	КомандаПечати = КомандыПечати.Добавить();
	КомандаПечати.Идентификатор = "ВедомостьВыдачи";
	КомандаПечати.Представление = "Ведомость выдачи";
	КомандаПечати.Обработчик    = "УправлениеПечатьюБПКлиент.ВыполнитьКомандуПечати";
	КомандаПечати.СписокФорм    = "ФормаСписка,ФормаДокумента";

КонецПроцедуры 

Процедура Печать(МассивОбъектов, ПараметрыПечати, КоллекцияПечатныхФорм, ОбъектыПечати, ПараметрыВывода) Экспорт 		
	Если УправлениеПечатью.НужноПечататьМакет(КоллекцияПечатныхФорм, "Этикетки") Тогда
		УправлениеПечатью.ВывестиТабличныйДокументВКоллекцию(КоллекцияПечатныхФорм, "Этикетки", "Печать этикеток", 
		СформироватьЭтикеткиПоДокументам(МассивОбъектов),"Документ.Мил_ВедомостьВыдачиПродукцииПоАктамУценки.ПФ_MXL_МИЛ_ЭтикеткиНаПродукциюКАктамУценки");
	КонецЕсли;  
	
	Если УправлениеПечатью.НужноПечататьМакет(КоллекцияПечатныхФорм, "ВедомостьВыдачи") Тогда
		УправлениеПечатью.ВывестиТабличныйДокументВКоллекцию(КоллекцияПечатныхФорм, "ВедомостьВыдачи", "Ведомость выдачи", 
		СформироватьВедомостьПоПодразделениям(МассивОбъектов),"Документ.Мил_ВедомостьВыдачиПродукцииПоАктамУценки.ПФ_MXL_МИЛ_ВедомостьНаВыдачуПродукцииПоАктамУценки");
	КонецЕсли;
КонецПроцедуры 

#КонецОбласти

#Область ОбработкаПечати        

Функция СформироватьЭтикеткиПоДокументам(МассивОбъектов)
	
	ТабДокумент = Новый ТабличныйДокумент;
	Для каждого Ссылка из МассивОбъектов Цикл      
		ТаблицаДляПечати = ПодготовитьТаблицуПечати(Ссылка);
		
		ТабДокументОбъекта = СформироватьЭтикетки(Ссылка, ТаблицаДляПечати); 
		ТабДокумент.Вывести(ТабДокументОбъекта)
	КонецЦикла;

	Возврат ТабДокумент

КонецФункции  


Функция ПодготовитьТаблицуПечати(Док)
	 Запрос = Новый Запрос;
	 Запрос.Текст =  "ВЫБРАТЬ
	                 |	Мил_ВедомостьВыдачиПродукцииПоАктамУценкиПродукция.Сотрудник КАК Сотрудник,
	                 |	Мил_ВедомостьВыдачиПродукцииПоАктамУценкиПродукция.Сотрудник.Подразделение КАК Подразделение,
	                 |	Мил_ВедомостьВыдачиПродукцииПоАктамУценкиПродукция.Сотрудник.ФИО КАК СотрудникФИО,
	                 |	Мил_ВедомостьВыдачиПродукцииПоАктамУценкиПродукция.Номенклатура КАК Номенклатура,
	                 |	Мил_ВедомостьВыдачиПродукцииПоАктамУценкиПродукция.Количество КАК Количество,
	                 |	Мил_ВедомостьВыдачиПродукцииПоАктамУценкиПродукция.АктУценки.Наименование КАК АктУценки,
	                 |	ХарактеристикиНоменклатурыДополнительныеРеквизиты.Значение КАК Цвет,
	                 |	Мил_ВедомостьВыдачиПродукцииПоАктамУценкиПродукция.Номенклатура.ЭЭ_Модель КАК Модель,
	                 |	Мил_ВедомостьВыдачиПродукцииПоАктамУценкиПродукция.Характеристика КАК Характеристика
	                 |ПОМЕСТИТЬ ТаблицаДанных
	                 |ИЗ
	                 |	Документ.Мил_ВедомостьВыдачиПродукцииПоАктамУценки.Продукция КАК Мил_ВедомостьВыдачиПродукцииПоАктамУценкиПродукция
	                 |		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.ХарактеристикиНоменклатуры.ДополнительныеРеквизиты КАК ХарактеристикиНоменклатурыДополнительныеРеквизиты
	                 |		ПО Мил_ВедомостьВыдачиПродукцииПоАктамУценкиПродукция.Характеристика = ХарактеристикиНоменклатурыДополнительныеРеквизиты.Ссылка
	                 |ГДЕ
	                 |	Мил_ВедомостьВыдачиПродукцииПоАктамУценкиПродукция.Ссылка = &Док
	                 |	И ХарактеристикиНоменклатурыДополнительныеРеквизиты.Свойство = &СвойствоЦвет
	                 |;
	                 |
	                 |////////////////////////////////////////////////////////////////////////////////
	                 |ВЫБРАТЬ
	                 |	ТаблицаДанных.Сотрудник КАК Сотрудник,
	                 |	ТаблицаДанных.Подразделение КАК Подразделение,
	                 |	ТаблицаДанных.СотрудникФИО КАК СотрудникФИО,
	                 |	ТаблицаДанных.Номенклатура КАК Номенклатура,
	                 |	ТаблицаДанных.Количество КАК Количество,
	                 |	ТаблицаДанных.АктУценки КАК АктУценки,
	                 |	ТаблицаДанных.Цвет КАК Цвет,
	                 |	ТаблицаДанных.Модель КАК Модель,
	                 |	ХарактеристикиНоменклатурыДополнительныеРеквизиты.Значение.Наименование КАК Размер
	                 |ИЗ
	                 |	ТаблицаДанных КАК ТаблицаДанных
	                 |		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.ХарактеристикиНоменклатуры.ДополнительныеРеквизиты КАК ХарактеристикиНоменклатурыДополнительныеРеквизиты
	                 |		ПО ТаблицаДанных.Характеристика = ХарактеристикиНоменклатурыДополнительныеРеквизиты.Ссылка
	                 |ГДЕ
	                 |	ХарактеристикиНоменклатурыДополнительныеРеквизиты.Свойство = &СвойствоРазмер
	                 |
	                 |УПОРЯДОЧИТЬ ПО
	                 |	Модель,
	                 |	Цвет,
	                 |	Размер";
	 Запрос.УстановитьПараметр("Док",Док); 
	 Запрос.УстановитьПараметр("СвойствоЦвет",ПланыВидовХарактеристик.ДополнительныеРеквизитыИСведения.НайтиПоРеквизиту("Имя", "4д_ЦветМилавица(общий)"));         
	 Запрос.УстановитьПараметр("СвойствоРазмер",ПланыВидовХарактеристик.ДополнительныеРеквизитыИСведения.НайтиПоРеквизиту("Имя", "Чд_РазмерыГотовыхИзделий")); 

     ТаблицаДляПечати = Запрос.Выполнить().Выгрузить();
	Возврат ТаблицаДляПечати

КонецФункции 


Функция  СформироватьЭтикетки(Ссылка, ТаблицаДляПечати)
	ТабличныйДокумент = Новый ТабличныйДокумент;
	ТабличныйДокумент.ИмяПараметровПечати = "ПАРАМЕТРЫ_ПЕЧАТИ_Мил_ВедомостьВыдачиПродукцииПоАктамУценки_Этикетки";
	ТабличныйДокумент.ЧерноБелаяПечать = Истина;
	
	Макет = УправлениеПечатью.МакетПечатнойФормы("Документ.Мил_ВедомостьВыдачиПродукцииПоАктамУценки.ПФ_MXL_МИЛ_ЭтикеткиНаПродукциюКАктамУценки"); 
	ОбластьЭтикетки = Макет.ПолучитьОбласть("Строка|Колонка");
	//ОбластьПустойЭтикетки = Макет.ПолучитьОбласть("СтрокаПустой|Колонка"); 
	
	
	Для каждого Стр из ТаблицаДляПечати Цикл     
		КоличествоШтук = Стр.количество;
		Пока КоличествоШтук > 0 Цикл
			ОбластьЭтикетки.параметры.Модель = Стр.Модель;
			ОбластьЭтикетки.параметры.Цвет = Стр.цвет;
			ОбластьЭтикетки.параметры.Размер = Стр.Размер;
			ОбластьЭтикетки.параметры.Акт = "Акт "+СокрЛП(Стр.АктУценки); 
			
			ОбластьЭтикетки.параметры.Табельный = ?(СокрЛП(Стр.Сотрудник.ОсновнойНомер) = "",Стр.Сотрудник.Код,Стр.Сотрудник.ОсновнойНомер);   
			ФИОФизическогоЛица = ФизическиеЛицаУТ.ФамилияИмяОтчество(Стр.Сотрудник, Ссылка.Дата);
			ФИО = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку("%1 %2.%3.", ФИОФизическогоЛица.Фамилия, Лев(ФИОФизическогоЛица.Имя, 1), Лев(ФИОФизическогоЛица.Отчество, 1));	
			ОбластьЭтикетки.параметры.ФИО = ФИО;
			ОбластьЭтикетки.параметры.Бригада	 = СокрЛП(Стр.Подразделение.код); 
			//ОбластьЭтикетки.параметры.Цех	= СокрЛП(Стр.АктУценки.Наименование);
			ТабличныйДокумент.Вывести(ОбластьЭтикетки);  
			КоличествоШтук = КоличествоШтук - 1;
		КонецЦикла;
	КонецЦикла;	
	
	Возврат ТабличныйДокумент;
КонецФункции    



Функция СформироватьВедомостьПоПодразделениям(МассивОбъектов)
	
	ТабДокумент = Новый ТабличныйДокумент;
	Для каждого Ссылка из МассивОбъектов Цикл      
		ТаблицаДляПечати = ПодготовитьТаблицуПечатиПоПодразделениям(Ссылка);
		
		ТабДокументОбъекта = СформироватьВедомость(Ссылка, ТаблицаДляПечати); 
		ТабДокумент.Вывести(ТабДокументОбъекта)
	КонецЦикла;

	Возврат ТабДокумент

КонецФункции  


Функция ПодготовитьТаблицуПечатиПоПодразделениям(Док)
	 Запрос = Новый Запрос;
	 Запрос.Текст =  "ВЫБРАТЬ
	                 |	Мил_ВедомостьВыдачиПродукцииПоАктамУценкиПродукция.Сотрудник КАК Сотрудник,
	                 |	Мил_ВедомостьВыдачиПродукцииПоАктамУценкиПродукция.Сотрудник.Подразделение КАК Подразделение,
	                 |	Мил_ВедомостьВыдачиПродукцииПоАктамУценкиПродукция.Сотрудник.ФИО КАК СотрудникФИО,
	                 |	Мил_ВедомостьВыдачиПродукцииПоАктамУценкиПродукция.Номенклатура КАК Номенклатура,
	                 |	Мил_ВедомостьВыдачиПродукцииПоАктамУценкиПродукция.Количество КАК Количество,
	                 |	Мил_ВедомостьВыдачиПродукцииПоАктамУценкиПродукция.АктУценки.Наименование КАК АктУценки,
	                 |	ХарактеристикиНоменклатурыДополнительныеРеквизиты.Значение КАК Цвет,
	                 |	Мил_ВедомостьВыдачиПродукцииПоАктамУценкиПродукция.Номенклатура.ЭЭ_Модель КАК Модель,
	                 |	Мил_ВедомостьВыдачиПродукцииПоАктамУценкиПродукция.Стоимость КАК Стоимость,
	                 |	ВЫБОР
	                 |		КОГДА Мил_ВедомостьВыдачиПродукцииПоАктамУценкиПродукция.Сотрудник.Подразделение.Родитель.Родитель.Родитель = ЗНАЧЕНИЕ(Справочник.СтруктураПредприятия.ПустаяСсылка)
	                 |			ТОГДА Мил_ВедомостьВыдачиПродукцииПоАктамУценкиПродукция.Сотрудник.Подразделение.Родитель.Родитель
	                 |		КОГДА Мил_ВедомостьВыдачиПродукцииПоАктамУценкиПродукция.Сотрудник.Подразделение.Родитель.Родитель = ЗНАЧЕНИЕ(Справочник.СтруктураПредприятия.ПустаяСсылка)
	                 |			ТОГДА Мил_ВедомостьВыдачиПродукцииПоАктамУценкиПродукция.Сотрудник.Подразделение.Родитель
	                 |		КОГДА Мил_ВедомостьВыдачиПродукцииПоАктамУценкиПродукция.Сотрудник.Подразделение.Родитель = ЗНАЧЕНИЕ(Справочник.СтруктураПредприятия.ПустаяСсылка)
	                 |			ТОГДА Мил_ВедомостьВыдачиПродукцииПоАктамУценкиПродукция.Сотрудник.Подразделение
	                 |		ИНАЧЕ Мил_ВедомостьВыдачиПродукцииПоАктамУценкиПродукция.Сотрудник.Подразделение
	                 |	КОНЕЦ КАК ВерхнееПодразделение,
	                 |	Мил_ВедомостьВыдачиПродукцииПоАктамУценкиПродукция.Характеристика КАК Характеристика
	                 |ПОМЕСТИТЬ ТаблицаДанных
	                 |ИЗ
	                 |	Документ.Мил_ВедомостьВыдачиПродукцииПоАктамУценки.Продукция КАК Мил_ВедомостьВыдачиПродукцииПоАктамУценкиПродукция
	                 |		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.ХарактеристикиНоменклатуры.ДополнительныеРеквизиты КАК ХарактеристикиНоменклатурыДополнительныеРеквизиты
	                 |		ПО Мил_ВедомостьВыдачиПродукцииПоАктамУценкиПродукция.Характеристика = ХарактеристикиНоменклатурыДополнительныеРеквизиты.Ссылка
	                 |ГДЕ
	                 |	Мил_ВедомостьВыдачиПродукцииПоАктамУценкиПродукция.Ссылка = &Док
	                 |	И ХарактеристикиНоменклатурыДополнительныеРеквизиты.Свойство = &СвойствоЦвет
	                 |;
	                 |
	                 |////////////////////////////////////////////////////////////////////////////////
	                 |ВЫБРАТЬ
	                 |	ТаблицаДанных.Сотрудник КАК Сотрудник,
	                 |	ТаблицаДанных.Подразделение КАК Подразделение,
	                 |	ТаблицаДанных.ВерхнееПодразделение КАК ВерхнееПодразделение,
	                 |	ТаблицаДанных.СотрудникФИО КАК СотрудникФИО,
	                 |	ТаблицаДанных.Номенклатура КАК Номенклатура,
	                 |	ТаблицаДанных.Количество КАК Количество,
	                 |	ТаблицаДанных.АктУценки КАК АктУценки,
	                 |	ТаблицаДанных.Цвет КАК Цвет,
	                 |	ТаблицаДанных.Модель КАК Модель,
	                 |	ТаблицаДанных.Стоимость КАК Стоимость,
	                 |	ХарактеристикиНоменклатурыДополнительныеРеквизиты.Значение.Наименование КАК Размер
	                 |ИЗ
	                 |	ТаблицаДанных КАК ТаблицаДанных
	                 |		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.ХарактеристикиНоменклатуры.ДополнительныеРеквизиты КАК ХарактеристикиНоменклатурыДополнительныеРеквизиты
	                 |		ПО ТаблицаДанных.Характеристика = ХарактеристикиНоменклатурыДополнительныеРеквизиты.Ссылка
	                 |ГДЕ
	                 |	ХарактеристикиНоменклатурыДополнительныеРеквизиты.Свойство = &СвойствоРазмер
	                 |
	                 |УПОРЯДОЧИТЬ ПО
	                 |	Модель,
	                 |	Цвет,
	                 |	Размер
	                 |ИТОГИ
	                 |	СУММА(Количество),
	                 |	СУММА(Стоимость)
	                 |ПО
	                 |	Подразделение,
	                 |	Сотрудник";
	 Запрос.УстановитьПараметр("Док",Док); 
	 Запрос.УстановитьПараметр("СвойствоЦвет",ПланыВидовХарактеристик.ДополнительныеРеквизитыИСведения.НайтиПоРеквизиту("Имя", "4д_ЦветМилавица(общий)"));         
	 Запрос.УстановитьПараметр("СвойствоРазмер",ПланыВидовХарактеристик.ДополнительныеРеквизитыИСведения.НайтиПоРеквизиту("Имя", "Чд_РазмерыГотовыхИзделий")); 

     ТаблицаДляПечати = Запрос.Выполнить();
	Возврат ТаблицаДляПечати

КонецФункции 


Функция  СформироватьВедомость(Ссылка, ТаблицаДляПечати)
	ТабличныйДокумент = Новый ТабличныйДокумент;
	ТабличныйДокумент.ИмяПараметровПечати = "ПАРАМЕТРЫ_ПЕЧАТИ_Мил_ВедомостьВыдачиПродукцииПоАктамУценки_ВедомостьВыдачиПоПодразделениям";
	ТабличныйДокумент.ЧерноБелаяПечать = Истина;   
	ТабличныйДокумент.АвтоМасштаб = Истина;
	
	Макет = УправлениеПечатью.МакетПечатнойФормы("Документ.Мил_ВедомостьВыдачиПродукцииПоАктамУценки.ПФ_MXL_МИЛ_ВедомостьНаВыдачуПродукцииПоАктамУценки");   
	ОбластьШапка = Макет.ПолучитьОбласть("Шапка");      
	ОбластьСтрока = Макет.ПолучитьОбласть("Строка");      
	ОбластьПодвал = Макет.ПолучитьОбласть("Подвал");   
	
	ВыборкаПодразделение = ТаблицаДляПечати.выбрать(ОбходРезультатаЗапроса.ПоГруппировкам,"Подразделение");	
	Пока ВыборкаПодразделение.следующий() Цикл
		ОбластьШапка.параметры.Подразделение = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку("%1 %2 %3", ВыборкаПодразделение.подразделение.код, ВыборкаПодразделение.подразделение, ВыборкаПодразделение.ВерхнееПодразделение);	
		ОбластьШапка.параметры.ДатаДок = Формат(Ссылка.Дата,"ДФ=dd.MM.yyyy"); 
		ТабличныйДокумент.Вывести(ОбластьШапка);  
		
		ВыборкаСотрудник = ВыборкаПодразделение.выбрать(ОбходРезультатаЗапроса.ПоГруппировкам,"Сотрудник");	
		Пока ВыборкаСотрудник.следующий() Цикл        
			//ОбластьСтрока.параметры.Подразделение = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку("%1 %2", ВыборкаПодразделение.подразделение.код, ВыборкаПодразделение.подразделение);	
			//ТабличныйДокумент.Вывести(ОбластьСтрока);  
			
			ВыборкаТовар = ВыборкаСотрудник.выбрать();	
			Пока ВыборкаТовар.следующий() Цикл        
				ОбластьСтрока.параметры.Табельный = ?(СокрЛП(ВыборкаТовар.Сотрудник.ОсновнойНомер) = "",ВыборкаТовар.Сотрудник.Код,ВыборкаТовар.Сотрудник.ОсновнойНомер);
				ОбластьСтрока.параметры.Сотрудник =  ВыборкаТовар.СотрудникФИО;
				ОбластьСтрока.параметры.Модель =  ВыборкаТовар.Модель;
				ОбластьСтрока.параметры.Цвет =  ВыборкаТовар.Цвет;
				ОбластьСтрока.параметры.Размер =  ВыборкаТовар.Размер;
				ОбластьСтрока.параметры.Акт =  ВыборкаТовар.АктУценки;
				ОбластьСтрока.параметры.Количество =  ВыборкаТовар.Количество;
				ОбластьСтрока.параметры.Стоимость =  ВыборкаТовар.Стоимость;
				ТабличныйДокумент.Вывести(ОбластьСтрока);  
			КонецЦикла;	
		КонецЦикла;	
		ОбластьПодвал.параметры.Стоимость = ВыборкаПодразделение.Стоимость;	 
		ОбластьПодвал.параметры.Количество = ВыборкаПодразделение.Количество;	
		ТабличныйДокумент.Вывести(ОбластьПодвал);   
		ТабличныйДокумент.ВывестиГоризонтальныйРазделительСтраниц();
	КонецЦикла;	
	
	Возврат ТабличныйДокумент;
КонецФункции    




#КонецОбласти 


#КонецЕсли

