﻿
#Область ОбработчикиКоманд

&НаКлиенте
Процедура ОбработкаКоманды(ПараметрКоманды, ПараметрыВыполненияКоманды)
	
	ОчиститьСообщения();
	
	КаталогОбменаСOptiPlan = ПолучитьАдресКаталогаОбмена();
	Если КаталогОбменаСOptiPlan = "" Тогда
		
		ТекстСообщения = НСтр("ru='Не задано расположение каталога обмена с Optiplan(настройка находится в подменю ""Настройка"")'");
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения);
		Возврат;
	КонецЕсли;
					
	// проверим наличие каталога "К выгрузке"
	АдресКаталогаВыгрузки = КаталогОбменаСOptiPlan + "\К выгрузке";
	ДопПараметры = Новый Структура();
	ДопПараметры.Вставить("КаталогОбменаСOptiPlan", КаталогОбменаСOptiPlan);
	ДопПараметры.Вставить("ПараметрКоманды", ПараметрКоманды);
	ДопПараметры.Вставить("АдресКаталогаВыгрузки", АдресКаталогаВыгрузки);
	
	ОписаниеОповещения = Новый ОписаниеОповещения("ПроверкаСуществованияКаталогаЗавершение",
													ЭтотОбъект, ДопПараметры);
	КаталогВыгрузки = Новый Файл(АдресКаталогаВыгрузки);
	КаталогВыгрузки.НачатьПроверкуСуществования(ОписаниеОповещения);
		
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаКлиенте
Процедура ПроверкаСуществованияКаталогаЗавершение(Существует, ДопПараметры) Экспорт

	Если Существует Тогда
		ПродолжитьВыгрузкуВОптиплан(ДопПараметры);
	Иначе
		// создаем каталог "К выгрузке"
		ТекстСообщения = НСтр("ru='Создание каталога ""К выгрузке""'");
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения);
		
		ОписаниеОповещения = Новый ОписаниеОповещения("СозданиеКаталогаВыгрузкиЗавершение", ЭтотОбъект, ДопПараметры); 
		НачатьСозданиеКаталога(ОписаниеОповещения, ДопПараметры.АдресКаталогаВыгрузки);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура СозданиеКаталогаВыгрузкиЗавершение(ИмяКаталога, ДопПараметры) Экспорт
	
	Если ИмяКаталога <> "" Тогда
		ТекстСообщения = НСтр("ru='Создан каталог ""К выгрузке"" по адресу %1'");
		ТекстСообщения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ТекстСообщения, ДопПараметры.АдресКаталогаВыгрузки);
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения);
		ПродолжитьВыгрузкуВОптиплан(ДопПараметры);
	Иначе
		ТекстСообщения = НСтр("ru='Не удалось создать каталог ""К выгрузке"" по адресу %1'");
		ТекстСообщения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ТекстСообщения, ДопПараметры.АдресКаталогаВыгрузки);
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения);
	КонецЕсли; 
	
КонецПроцедуры

&НаКлиенте
Процедура ПродолжитьВыгрузкуВОптиплан(ДопПараметры)
	
	РезультатВыгрузки = ВыгрузитьВOptiplan(ДопПараметры);
	Если РезультатВыгрузки = Неопределено
		ИЛИ НЕ РезультатВыгрузки.СоответствиеАдресов.Количество() Тогда
		
		Возврат;
	КонецЕсли;
	
	СоответствиеАдресов = РезультатВыгрузки.СоответствиеАдресов;
	МассивАдресов = Новый Массив;
	Для каждого АдресВыгрузки Из СоответствиеАдресов Цикл
		// переместим созданные файлы в папку "К выгрузке"
		ДвоичныеДанныеФайла = ПолучитьИзВременногоХранилища(АдресВыгрузки.Ключ);
		ДвоичныеДанныеФайла.Записать(АдресВыгрузки.Значение);
		
		ТекстСообщения = НСтр("ru='Файл выгрузки находится по адресу %1'");
		ТекстСообщения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ТекстСообщения, АдресВыгрузки.Значение);
		РезультатВыгрузки.МассивСообщений.Добавить(ТекстСообщения);
		
		УдалитьИзВременногоХранилища(АдресВыгрузки.Ключ);
		МассивАдресов.Добавить(АдресВыгрузки.Значение);
	КонецЦикла;
	
	Для каждого ТекстСообщения Из РезультатВыгрузки.МассивСообщений Цикл
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения);
	КонецЦикла; 
	
	ЗаголовокОповещения = НСтр("ru='Файлы выгрузки'");
	ТекстОповещения = НСтр("ru='нажмите для открытия'");
	Оповещение = Новый ОписаниеОповещения("ЗавершениеВыгрузкиВОптиплан", ЭтотОбъект, Новый Структура("МассивАдресов", МассивАдресов));
	ПоказатьОповещениеПользователя(ЗаголовокОповещения, Оповещение, ТекстОповещения, БиблиотекаКартинок.ВыполненоУспешно,
																					СтатусОповещенияПользователя.Информация);
КонецПроцедуры
 	
&НаКлиенте
Процедура ЗавершениеВыгрузкиВОптиплан(ДопПараметры) Экспорт
	
	Для каждого АдресФайла Из ДопПараметры.МассивАдресов Цикл
		ФайлВыгрузки = Новый Файл(АдресФайла);
		Если ФайлВыгрузки.Существует() Тогда
			ТекстовыйФайл = Новый ТекстовыйДокумент;
			ТекстовыйФайл.Прочитать(АдресФайла);
			ТекстовыйФайл.Показать(АдресФайла);
		КонецЕсли;
	КонецЦикла;
 
КонецПроцедуры

&НаСервере
Функция ВыгрузитьВOptiplan(ДопПараметры)

	МассивКарт = Новый Массив;
	МассивЗаданий = Новый Массив;
	Для Каждого Элемент Из ДопПараметры.ПараметрКоманды Цикл
		Если Элемент.ПометкаУдаления Тогда
			Продолжить;
		КонецЕсли;
		Если ТипЗнч(Элемент) = Тип("ДокументСсылка.Чд_ЗаданиеНаРаскрой") Тогда
			Если МассивЗаданий.Найти(Элемент) = Неопределено Тогда
				МассивЗаданий.Добавить(Элемент);
				Для каждого Строка Из Элемент.КартыРаскроя Цикл
					Если МассивКарт.Найти(Строка.КартаРаскроя) = Неопределено 
						И Не Строка.КартаРаскроя.ПометкаУдаления Тогда
						
						МассивКарт.Добавить(Строка.КартаРаскроя);
					КонецЕсли;
				КонецЦикла;
			КонецЕсли;
		ИначеЕсли ТипЗнч(Элемент) = Тип("ДокументСсылка.Чд_КартаРаскроя") Тогда
			Если МассивКарт.Найти(Элемент) = Неопределено Тогда
				МассивКарт.Добавить(Элемент);
			КонецЕсли;
			Если МассивЗаданий.Найти(Элемент.Распоряжение) = Неопределено
				И НЕ Элемент.Распоряжение.ПометкаУдаления Тогда
				
				МассивЗаданий.Добавить(Элемент.Распоряжение);
			КонецЕсли;
		КонецЕсли;
	КонецЦикла;
	
	Если МассивКарт.Количество() > 0 И МассивЗаданий.Количество() > 0 Тогда
		ТекстСообщения = НСтр("ru='Начало выгрузки данных.'");
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения);
		
		СтруктураДанных = Документы.Чд_КартаРаскроя.ВыгрузитьВOptiplan(МассивКарт, МассивЗаданий, ДопПараметры);
	КонецЕсли;
	
	Если СтруктураДанных <> Неопределено
		И СтруктураДанных.СоответствиеАдресов.Количество() Тогда
		
		ТекстСообщения = "Выгрузка данных: ЗАВЕРШЕНА";
	Иначе
		ТекстСообщения = "Выгрузка данных: ОШИБКА";
	КонецЕсли;
	
	СтруктураДанных.МассивСообщений.Добавить(ТекстСообщения);
	
	Возврат СтруктураДанных;
	
КонецФункции

&НаСервере
Функция ПолучитьАдресКаталогаОбмена()
	
	Возврат Документы.Чд_КартаРаскроя.ПолучитьАдресКаталогаОбмена();
	
КонецФункции

#КонецОбласти 