﻿&НаКлиенте
Перем СтарыеЗначенияКонтролируемыхПолей;

&НаКлиенте
Перем АктивизированныйСотрудник;

&НаКлиенте
Перем СотрудникПередУдалением;

&НаКлиенте
Перем СотрудникиКРасчету Экспорт;

#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	РасчетЗарплатыРасширенныйФормы.ДокументыПриСозданииНаСервере(ЭтаФорма, ОписаниеДокумента(ЭтаФорма));
	РасчетЗарплатыРасширенныйФормы.ИнициализироватьМеханизмПересчетаДокументаПриРедактировании(ЭтаФорма);
	
	Если Параметры.Ключ.Пустая() Тогда
		
		ЗначенияДляЗаполнения = Новый Структура("Организация, Ответственный, Месяц",
		"Объект.Организация", "Объект.Ответственный", "Объект.ПериодРегистрации");
		ЗарплатаКадры.ЗаполнитьПервоначальныеЗначенияВФорме(ЭтаФорма, ЗначенияДляЗаполнения);
		
		Если Не ЗначениеЗаполнено(Объект.ПериодРегистрации) Тогда
			Объект.ПериодРегистрации = ТекущаяДатаСеанса();
		КонецЕсли;
		
		Объект.ДатаПолученияДохода = ТекущаяДатаСеанса();
		
		ЗаполнитьДанныеФормыПоОрганизации();
		ПриПолученииДанныхНаСервере();
		
		УстановитьКодДохода();
		УстановитьКодВычета();
		УстановитьВидДоходаСтраховыхВзносов();
		РассчитатьНДФЛ();
		
	КонецЕсли;
	
	// Обработчик подсистемы "ВерсионированиеОбъектов".
	ВерсионированиеОбъектов.ПриСозданииНаСервере(ЭтаФорма);
	// СтандартныеПодсистемы.ПодключаемыеКоманды
	ПодключаемыеКоманды.ПриСозданииНаСервере(ЭтотОбъект);
	// Конец СтандартныеПодсистемы.ПодключаемыеКоманды
	
	// СтандартныеПодсистемы.Свойства
	ДополнительныеПараметры = Новый Структура;
	ДополнительныеПараметры.Вставить("ИмяЭлементаДляРазмещения", "ГруппаДополнительныеРеквизиты");
	УправлениеСвойствами.ПриСозданииНаСервере(ЭтотОбъект, ДополнительныеПараметры);
	// Конец СтандартныеПодсистемы.Свойства
		
КонецПроцедуры

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)
	
	// СтандартныеПодсистемы.ДатыЗапретаИзменения
	ДатыЗапретаИзменения.ОбъектПриЧтенииНаСервере(ЭтаФорма, ТекущийОбъект);
	// Конец СтандартныеПодсистемы.ДатыЗапретаИзменения
	
	ПриПолученииДанныхНаСервере();
	
	// СтандартныеПодсистемы.ПодключаемыеКоманды
	ПодключаемыеКомандыКлиентСервер.ОбновитьКоманды(ЭтотОбъект, Объект);
	// Конец СтандартныеПодсистемы.ПодключаемыеКоманды
	
	// СтандартныеПодсистемы.УправлениеДоступом
	УправлениеДоступом.ПриЧтенииНаСервере(ЭтотОбъект, ТекущийОбъект);
	// Конец СтандартныеПодсистемы.УправлениеДоступом
	
	// СтандартныеПодсистемы.Свойства
	УправлениеСвойствами.ПриЧтенииНаСервере(ЭтотОбъект, ТекущийОбъект);
	// Конец СтандартныеПодсистемы.Свойства
	
	// ПроцессыОбработкиДокументов
	Если ОбщегоНазначения.ПодсистемаСуществует("ЗарплатаКадрыПриложения.ПроцессыОбработкиДокументовЗарплата") Тогда
		МодульПроцессыОбработкиДокументовЗарплата = ОбщегоНазначения.ОбщийМодуль("ПроцессыОбработкиДокументовЗарплата");
		МодульПроцессыОбработкиДокументовЗарплата.ПриЧтенииНаСервере(ЭтотОбъект, ТекущийОбъект);
	КонецЕсли;	
	// Конец ПроцессыОбработкиДокументов
	
КонецПроцедуры

&НаСервере
Процедура ПослеЗаписиНаСервере(ТекущийОбъект, ПараметрыЗаписи)
	
	// СтандартныеПодсистемы.УправлениеДоступом
	УправлениеДоступом.ПослеЗаписиНаСервере(ЭтотОбъект, ТекущийОбъект, ПараметрыЗаписи);
	// Конец СтандартныеПодсистемы.УправлениеДоступом
	
КонецПроцедуры

&НаКлиенте
Процедура ПередЗакрытием(Отказ, ЗавершениеРаботы, ТекстПредупреждения, СтандартнаяОбработка)
	
	// ПроцессыОбработкиДокументов
	Если ОбщегоНазначенияКлиент.ПодсистемаСуществует("ЗарплатаКадрыПриложения.ПроцессыОбработкиДокументовЗарплата") Тогда
		МодульПроцессыОбработкиДокументовЗарплата = ОбщегоНазначенияКлиент.ОбщийМодуль("ПроцессыОбработкиДокументовЗарплатаКлиент");
		МодульПроцессыОбработкиДокументовЗарплата.ПередЗакрытием(ЭтотОбъект, Объект, Отказ, ЗавершениеРаботы, ТекстПредупреждения, СтандартнаяОбработка)
	КонецЕсли;	
	// Конец ПроцессыОбработкиДокументов
	
КонецПроцедуры

&НаКлиенте
Процедура ПослеЗаписи(ПараметрыЗаписи)
	
	Оповестить("Запись_ПризПодарок", ПараметрыЗаписи, Объект.Ссылка);
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
		
	// СтандартныеПодсистемы.Свойства
	Если УправлениеСвойствамиКлиент.ОбрабатыватьОповещения(ЭтотОбъект, ИмяСобытия, Параметр) Тогда
		ОбновитьЭлементыДополнительныхРеквизитов();
		УправлениеСвойствамиКлиент.ПослеЗагрузкиДополнительныхРеквизитов(ЭтотОбъект);
	КонецЕсли;
	// Конец СтандартныеПодсистемы.Свойства
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	// СтандартныеПодсистемы.ПодключаемыеКоманды
	ПодключаемыеКомандыКлиент.НачатьОбновлениеКоманд(ЭтотОбъект);
	// Конец СтандартныеПодсистемы.ПодключаемыеКоманды
	
	// СтандартныеПодсистемы.Свойства
	УправлениеСвойствамиКлиент.ПослеЗагрузкиДополнительныхРеквизитов(ЭтотОбъект);
	// Конец СтандартныеПодсистемы.Свойства
	
КонецПроцедуры

&НаСервере
Процедура ПередЗаписьюНаСервере(Отказ, ТекущийОбъект, ПараметрыЗаписи)
	
	// СтандартныеПодсистемы.Свойства
	УправлениеСвойствами.ПередЗаписьюНаСервере(ЭтотОбъект, ТекущийОбъект);
	// Конец СтандартныеПодсистемы.Свойства

КонецПроцедуры
	
&НаСервере
Процедура ОбработкаПроверкиЗаполненияНаСервере(Отказ, ПроверяемыеРеквизиты)
	
	// СтандартныеПодсистемы.Свойства
	УправлениеСвойствами.ОбработкаПроверкиЗаполнения(ЭтотОбъект, Отказ, ПроверяемыеРеквизиты);
	// Конец СтандартныеПодсистемы.Свойства
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура ОрганизацияПриИзменении(Элемент)
	
	ЗарплатаКадрыКлиент.КлючевыеРеквизитыЗаполненияФормыОчиститьТаблицы(ЭтаФорма);
	ОрганизацияПриИзмененииНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ВидПризаПодаркаПриИзменении(Элемент)
	
	ВидПризаПодаркаПриИзмененииНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура КодДоходаНДФЛПриИзменении(Элемент)
	
	 КодДоходаНДФЛПриИзмененииНаСервере();
	 
КонецПроцедуры

&НаКлиенте
Процедура КодВычетаНДФЛПриИзменении(Элемент)
	
	КодВычетаНДФЛПриИзмененииНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ДатаПолученияДоходаПриИзменении(Элемент)
	
	ДатаПолученияДоходаПриИзмененииНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ПредусмотреноКолдоговоромПриИзменении(Элемент)
	
	ПредусмотреноКолдоговоромПриИзмененииНаСервере();
	
КонецПроцедуры

#Область РедактированиеМесяцаСтрокой

&НаКлиенте
Процедура МесяцНачисленияСтрокойПриИзменении(Элемент)
	
	ЗарплатаКадрыКлиент.ВводМесяцаПриИзменении(ЭтаФорма, "Объект.ПериодРегистрации", "МесяцНачисленияСтрокой", Модифицированность);
	ПериодРегистрацииПриИзмененииНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура МесяцНачисленияСтрокойНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	Оповещение = Новый ОписаниеОповещения("МесяцНачисленияСтрокойНачалоВыбораЗавершение", ЭтотОбъект);
	ЗарплатаКадрыКлиент.ВводМесяцаНачалоВыбора(ЭтаФорма, ЭтаФорма, "Объект.ПериодРегистрации", "МесяцНачисленияСтрокой", , Оповещение);
	
КонецПроцедуры

&НаКлиенте
Процедура МесяцНачисленияСтрокойНачалоВыбораЗавершение(ЗначениеВыбрано, ДополнительныеПараметры) Экспорт
	
	ПериодРегистрацииПриИзмененииНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура МесяцНачисленияСтрокойРегулирование(Элемент, Направление, СтандартнаяОбработка)
	
	ЗарплатаКадрыКлиент.ВводМесяцаРегулирование(ЭтаФорма, "Объект.ПериодРегистрации", "МесяцНачисленияСтрокой", Направление, Модифицированность);
	ПодключитьОбработчикОжидания("ОбработчикОжиданияМесяцНачисленияПриИзменении", 0.3, Истина);
	
КонецПроцедуры

&НаКлиенте
Процедура МесяцНачисленияСтрокойАвтоПодбор(Элемент, Текст, ДанныеВыбора, Ожидание, СтандартнаяОбработка)
	
	ЗарплатаКадрыКлиент.ВводМесяцаАвтоПодборТекста(Текст, ДанныеВыбора, СтандартнаяОбработка);
	
КонецПроцедуры

&НаКлиенте
Процедура МесяцНачисленияСтрокойОкончаниеВводаТекста(Элемент, Текст, ДанныеВыбора, СтандартнаяОбработка)
	
	ЗарплатаКадрыКлиент.ВводМесяцаОкончаниеВводаТекста(Текст, ДанныеВыбора, СтандартнаяОбработка);
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработчикОжиданияМесяцНачисленияПриИзменении()

	ПериодРегистрацииПриИзмененииНаСервере();
	
КонецПроцедуры

&НаСервере
Процедура ПериодРегистрацииПриИзмененииНаСервере()
	
	УстановитьФункциональныеОпцииФормы();
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыНачисления

&НаКлиенте
Процедура НачисленияПриАктивизацииСтроки(Элемент)
	
	РасчетЗарплатыРасширенныйКлиент.ДокументыВыполненияНачисленийПриАктивацииСтроки(ЭтаФорма, "Начисления", Ложь);
	
	Если Элементы.Начисления.ТекущиеДанные <> Неопределено Тогда
		АктивизированныйСотрудник = Элементы.Начисления.ТекущиеДанные.Сотрудник;
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура НачисленияПередУдалением(Элемент, Отказ)
	
	Если Элементы.Начисления.ТекущиеДанные <> Неопределено Тогда
		СотрудникПередУдалением = Элементы.Начисления.ТекущиеДанные.Сотрудник;
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура НачисленияПриОкончанииРедактирования(Элемент, НоваяСтрока, ОтменаРедактирования)
	
	Если ОтменаРедактирования Тогда
		Возврат;
	КонецЕсли;
	
	РасчетЗарплатыРасширенныйКлиент.СтрокаРасчетаПриОкончанииРедактирования(ЭтаФорма, ОписаниеТаблицыНачислений(), Истина, , ОписаниеДокумента(ЭтаФорма));
	ЗарплатаКадрыКлиентСервер.КлючевыеРеквизитыЗаполненияФормыУстановитьОтображениеПредупреждения(ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура НачисленияОбработкаВыбора(Элемент, ВыбранноеЗначение, СтандартнаяОбработка)
	
	ОбработкаПодбораНаСервере(ВыбранноеЗначение);
	
КонецПроцедуры

&НаКлиенте
Процедура НачисленияПослеУдаления(Элемент)
	
	НачисленияПослеУдаленияНаСервере(СотрудникПередУдалением);
	
КонецПроцедуры

&НаКлиенте
Процедура НачисленияСотрудникПриИзменении(Элемент)
	
	ОбработатьИзменениеСотрудника(Элементы.Начисления.ТекущаяСтрока, АктивизированныйСотрудник);
	
	ДанныеСтроки = Элементы.Начисления.ТекущиеДанные;
	РасчетЗарплатыРасширенныйКлиент.УстановитьЗначенияКонтролируемыхПолей("Начисления", ДанныеСтроки, ЭтаФорма["КонтролируемыеПоляНачисления"], СтарыеЗначенияКонтролируемыхПолей);
	ДанныеСтроки.ФиксЗаполнение = Ложь;
	ДанныеСтроки.ФиксРасчет = Ложь;
	ДанныеСтроки.ФиксСуммаВычета = Ложь;
	
КонецПроцедуры

&НаКлиенте
Процедура НачисленияКодВычетаПриИзменении(Элемент)
	
	ТекущиеДанные = Элементы.Начисления.ТекущиеДанные;
	
	Если ТекущиеДанные <> Неопределено Тогда 
		ТекущиеДанные.ФиксСуммаВычета = Ложь;
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура НачисленияСуммаВычетаПриИзменении(Элемент)
	
	ТекущиеДанные = Элементы.Начисления.ТекущиеДанные;
	
	Если ТекущиеДанные <> Неопределено Тогда 
		ТекущиеДанные.ФиксСуммаВычета = Истина;
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

// СтандартныеПодсистемы.ПодключаемыеКоманды
&НаКлиенте
Процедура Подключаемый_ВыполнитьКоманду(Команда)
	ПодключаемыеКомандыКлиент.ВыполнитьКоманду(ЭтотОбъект, Команда, Объект);
КонецПроцедуры

&НаСервере
Процедура Подключаемый_ВыполнитьКомандуНаСервере(Контекст, Результат) Экспорт
	ПодключаемыеКоманды.ВыполнитьКоманду(ЭтотОбъект, Контекст, Объект, Результат);
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ОбновитьКоманды()
	ПодключаемыеКомандыКлиентСервер.ОбновитьКоманды(ЭтотОбъект, Объект);
КонецПроцедуры
// Конец СтандартныеПодсистемы.ПодключаемыеКоманды

// СтандартныеПодсистемы.Свойства
&НаКлиенте
Процедура Подключаемый_СвойстваВыполнитьКоманду(ЭлементИлиКоманда, НавигационнаяСсылка = Неопределено, СтандартнаяОбработка = Неопределено)
	УправлениеСвойствамиКлиент.ВыполнитьКоманду(ЭтотОбъект, ЭлементИлиКоманда, СтандартнаяОбработка);
КонецПроцедуры
// Конец СтандартныеПодсистемы.Свойства

&НаКлиенте
Процедура ПодборСотрудников(Команда)
	
	НачалоПериодаПримененияОтбора = НачалоМесяца(Объект.ДатаПолученияДохода);
	ОкончаниеПериодаПримененияОтбора = КонецМесяца(Объект.ДатаПолученияДохода);
	
	КадровыйУчетКлиент.ВыбратьСотрудниковРаботающихВПериоде(
		Элементы.Начисления,
		Объект.Организация, ,
		НачалоПериодаПримененияОтбора, ОкончаниеПериодаПримененияОтбора,
		, АдресСпискаПодобранныхСотрудников());
	
КонецПроцедуры

&НаКлиенте
Процедура ПересчитатьДокумент(Команда)
	
	ПересчитатьДокументНаКлиенте();
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

// СтандартныеПодсистемы.Свойства 
&НаСервере
Процедура ОбновитьЭлементыДополнительныхРеквизитов()
	УправлениеСвойствами.ОбновитьЭлементыДополнительныхРеквизитов(ЭтотОбъект);
КонецПроцедуры

&НаКлиенте
Процедура ОбновитьЗависимостиДополнительныхРеквизитов()
	УправлениеСвойствамиКлиент.ОбновитьЗависимостиДополнительныхРеквизитов(ЭтотОбъект);
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ПриИзмененииДополнительногоРеквизита(Элемент)
	УправлениеСвойствамиКлиент.ОбновитьЗависимостиДополнительныхРеквизитов(ЭтотОбъект);
КонецПроцедуры
// Конец СтандартныеПодсистемы.Свойства

&НаСервере
Процедура ПриПолученииДанныхНаСервере()
	
	РасчетЗарплатыРасширенныйФормы.ДокументыВыполненияНачисленийДобавитьКонтрольИсправлений(ЭтотОбъект, ОписаниеТаблицыНачислений(), "Начисления");
	
	ЗарплатаКадры.КлючевыеРеквизитыЗаполненияФормыЗаполнитьПредупреждения(ЭтотОбъект);
	ЗарплатаКадрыКлиентСервер.КлючевыеРеквизитыЗаполненияФормыУстановитьОтображениеПредупреждения(ЭтотОбъект);
	
	ЗарплатаКадрыКлиентСервер.ЗаполнитьМесяцПоДате(ЭтотОбъект, "Объект.ПериодРегистрации", "МесяцНачисленияСтрокой");
	ЗаполнитьСписокВыбораКодДоходаНДФЛ();
	УстановитьФункциональныеОпцииФормы();
	
	УстановитьУсловноеОформление()

КонецПроцедуры

&НаСервере 
Процедура УстановитьУсловноеОформление()
	
	ЭлементУсловногоОформления = УсловноеОформление.Элементы.Добавить();
	
	ЭлементОтбора = ЭлементУсловногоОформления.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	ЭлементОтбора.ВидСравнения = ВидСравненияКомпоновкиДанных.Равно;
	ЭлементОтбора.Использование = Истина;
	ЭлементОтбора.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("Объект.Начисления.ФиксСуммаВычета");
	ЭлементОтбора.ПравоеЗначение = Истина;
	
	ОформляемоеПоле = ЭлементУсловногоОформления.Поля.Элементы.Добавить(); 
	ОформляемоеПоле.Поле = Новый ПолеКомпоновкиДанных("НачисленияСуммаВычета");
	
	ТекущийШрифт = Элементы.Начисления.Шрифт;
	ЖирныйШрифт = Новый Шрифт(ТекущийШрифт, , , Истина);
	
	ЭлементУсловногоОформления.Оформление.УстановитьЗначениеПараметра("Шрифт", ЖирныйШрифт);
	
	РасчетЗарплатыРасширенныйФормы.ДокументыВыполненияНачисленийУстановитьУсловноеОформление(ЭтотОбъект, ОписаниеТаблицыНачислений());

КонецПроцедуры

&НаСервере
Процедура ЗаполнитьСписокВыбораКодДоходаНДФЛ()
	
	Элементы.КодДоходаНДФЛ.СписокВыбора.ЗагрузитьЗначения(УчетНДФЛРасширенный.ДоходыНДФЛПоВидуОсобыхНачислений(Объект.ВидПризаПодарка));
	
КонецПроцедуры

&НаСервере
Процедура УстановитьКодДохода()
	
	//Если Объект.ВидПризаПодарка = Перечисления.ВидыПризовПодарков.Подарок Тогда
	//	Объект.КодДоходаНДФЛ = Справочники.ВидыДоходовНДФЛ;
	//Иначе
	//	Объект.КодДоходаНДФЛ = Справочники.ВидыДоходовНДФЛ.Код12124;
	//КонецЕсли;
	Объект.КодДоходаНДФЛ = Справочники.ВидыДоходовНДФЛ.Код12117;
	
	
КонецПроцедуры

&НаСервере
Процедура УстановитьКодВычета()
	
	Объект.КодВычетаНДФЛ = УчетНДФЛ.КодВычетаПоКодуДоходаНДФЛ(Объект.КодДоходаНДФЛ);
	УстановитьКодВычетаСотрудников();
	
КонецПроцедуры

&НаСервере
Процедура УстановитьКодВычетаСотрудников()
	
	Для Каждого ДанныеСотрудника Из Объект.Начисления Цикл 
		ДанныеСотрудника.КодВычета = Объект.КодВычетаНДФЛ;
	КонецЦикла;
	
КонецПроцедуры

&НаСервере
Процедура РассчитатьНДФЛ(Знач СотрудникиФизическиеЛица = Неопределено, РассчитыватьВычет = Истина)
	
	Если Не ЗначениеЗаполнено(Объект.КодДоходаНДФЛ) Тогда 
		Возврат;
	КонецЕсли;
	
	Если СотрудникиФизическиеЛица = Неопределено Тогда 
		СотрудникиФизическиеЛица = ОбщегоНазначения.ВыгрузитьКолонку(Объект.Начисления, "Сотрудник", Истина);
	КонецЕсли;
	
	Если ТипЗнч(СотрудникиФизическиеЛица) <> Тип("Массив") Тогда
		СотрудникиФизическиеЛица = ОбщегоНазначенияКлиентСервер.ЗначениеВМассиве(СотрудникиФизическиеЛица);
	КонецЕсли;
	
	ОбщегоНазначенияКлиентСервер.УдалитьЗначениеИзМассива(СотрудникиФизическиеЛица, Справочники.Сотрудники.ПустаяСсылка());
	ОбщегоНазначенияКлиентСервер.УдалитьЗначениеИзМассива(СотрудникиФизическиеЛица, Справочники.ФизическиеЛица.ПустаяСсылка());
	
	Если СотрудникиФизическиеЛица.Количество() = 0 Тогда 
		Возврат;
	КонецЕсли;
	
	МассивСотрудников = ОбщегоНазначения.ВыгрузитьКолонку(Объект.Начисления, "Сотрудник", Истина);
	ОбщегоНазначенияКлиентСервер.УдалитьЗначениеИзМассива(МассивСотрудников, Справочники.Сотрудники.ПустаяСсылка());
	ФизическиеЛицаСотрудников = ОбщегоНазначения.ЗначениеРеквизитаОбъектов(МассивСотрудников, "ФизическоеЛицо");
	
	ФизическиеЛицаКРасчету = Новый Соответствие;
	
	Если ТипЗнч(СотрудникиФизическиеЛица[0]) = Тип("СправочникСсылка.Сотрудники") Тогда
		Для Каждого Сотрудник Из СотрудникиФизическиеЛица Цикл 
			ФизическиеЛицаКРасчету.Вставить(ФизическиеЛицаСотрудников[Сотрудник], Истина);
		КонецЦикла;
	Иначе 
		Для Каждого ФизическоеЛицо Из СотрудникиФизическиеЛица Цикл 
			ФизическиеЛицаКРасчету.Вставить(ФизическоеЛицо, Истина);
		КонецЦикла;
	КонецЕсли;
	
	Если РассчитыватьВычет Тогда 
		РассчитатьВычет(ФизическиеЛицаКРасчету, ФизическиеЛицаСотрудников);
	КонецЕсли;
	
	ПризыПодарки = Новый ТаблицаЗначений;
	ПризыПодарки.Колонки.Добавить("НомерСтроки",	Новый ОписаниеТипов("Число"));
	ПризыПодарки.Колонки.Добавить("ФизическоеЛицо",	Новый ОписаниеТипов("СправочникСсылка.ФизическиеЛица"));
	ПризыПодарки.Колонки.Добавить("КодДохода",		Новый ОписаниеТипов("СправочникСсылка.ВидыДоходовНДФЛ"));	
	ПризыПодарки.Колонки.Добавить("СуммаДохода",	Новый ОписаниеТипов("Число"));	
	ПризыПодарки.Колонки.Добавить("СуммаВычета",	Новый ОписаниеТипов("Число"));
	
	Для Каждого ДанныеСотрудника Из Объект.Начисления Цикл
		
		ФизическоеЛицо = ФизическиеЛицаСотрудников[ДанныеСотрудника.Сотрудник];
		Если ФизическиеЛицаКРасчету[ФизическоеЛицо] = Неопределено Тогда 
			Продолжить;
		КонецЕсли;
		
		ДанныеСотрудника.НДФЛ = 0;
		
		СтрокаДохода = ПризыПодарки.Добавить();
		СтрокаДохода.НомерСтроки = ДанныеСотрудника.НомерСтроки;
		СтрокаДохода.ФизическоеЛицо = ФизическоеЛицо;
		СтрокаДохода.КодДохода = Объект.КодДоходаНДФЛ;
		СтрокаДохода.СуммаДохода = ДанныеСотрудника.Результат;
		СтрокаДохода.СуммаВычета = ДанныеСотрудника.СуммаВычета;
		
	КонецЦикла;
	
	УчетНДФЛРасширенный.РассчитатьНалогДляКонкретногоДохода(Объект.ДатаПолученияДохода, ПризыПодарки);
	
	ИдентификаторСтрокиНДФЛ = 0;
	
	Для Каждого СтрокаДохода Из ПризыПодарки Цикл 
		НайденныеСтроки = Объект.Начисления.НайтиСтроки(Новый Структура("НомерСтроки", СтрокаДохода.НомерСтроки));
		Если НайденныеСтроки.Количество() > 0 Тогда
			ДанныеСотрудника = НайденныеСтроки[0];
			ДанныеСотрудника.СуммаНДФЛ = СтрокаДохода.НДФЛ;
		КонецЕсли;
	КонецЦикла;
	
КонецПроцедуры

&НаСервере
Процедура РассчитатьВычет(ФизическиеЛицаКРасчету, ФизическиеЛицаСотрудников)
	
	ИсходныеДанные = Новый ТаблицаЗначений;
	ИсходныеДанные.Колонки.Добавить("ФизическоеЛицо", Новый ОписаниеТипов("СправочникСсылка.ФизическиеЛица"));
	ИсходныеДанные.Колонки.Добавить("КодДохода", Новый ОписаниеТипов("СправочникСсылка.ВидыДоходовНДФЛ"));
	ИсходныеДанные.Колонки.Добавить("Сумма", Новый ОписаниеТипов("Число"));
	ИсходныеДанные.Колонки.Добавить("КодВычета", Новый ОписаниеТипов("СправочникСсылка.ВидыВычетовНДФЛ"));
	ИсходныеДанные.Колонки.Добавить("КоличествоДетей", Новый ОписаниеТипов("Число"));
	ИсходныеДанные.Колонки.Добавить("Регистратор", Новый ОписаниеТипов("ДокументСсылка.ПризПодарок"));
	ИсходныеДанные.Колонки.Добавить("НомерСтроки", Новый ОписаниеТипов("Число"));
	
	Для Каждого ДанныеСотрудника Из Объект.Начисления Цикл 
		
		Если Не ЗначениеЗаполнено(ДанныеСотрудника.Сотрудник)
			Или Не ЗначениеЗаполнено(ДанныеСотрудника.КодВычета) Тогда
			Продолжить;
		КонецЕсли;
		
		ФизическоеЛицо = ФизическиеЛицаСотрудников[ДанныеСотрудника.Сотрудник];
		Если ФизическиеЛицаКРасчету[ФизическоеЛицо] = Неопределено Тогда 
			Продолжить;
		КонецЕсли;
		
		Если Не ДанныеСотрудника.ФиксСуммаВычета Тогда 
			ДанныеСотрудника.СуммаВычета = 0;
		КонецЕсли;
		
		НоваяСтрока = ИсходныеДанные.Добавить();
		НоваяСтрока.ФизическоеЛицо = ФизическоеЛицо;
		НоваяСтрока.КодДохода = Объект.КодДоходаНДФЛ;
		НоваяСтрока.Сумма = ДанныеСотрудника.Результат;
		НоваяСтрока.КодВычета = ДанныеСотрудника.КодВычета;
		НоваяСтрока.Регистратор = Объект.Ссылка;
		НоваяСтрока.НомерСтроки = ДанныеСотрудника.НомерСтроки;
		
	КонецЦикла;
	
	Запрос = Новый Запрос;
	
	Запрос.МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
	
	Запрос.УстановитьПараметр("ИсходныеДанные", ИсходныеДанные);
	
	Запрос.Текст = "ВЫБРАТЬ
	               |	ИсходныеДанные.ФизическоеЛицо,
	               |	ИсходныеДанные.КодДохода,
	               |	ИсходныеДанные.Сумма,
	               |	ИсходныеДанные.КодВычета,
	               |	ИсходныеДанные.КоличествоДетей,
	               |	ИсходныеДанные.Регистратор,
	               |	ИсходныеДанные.НомерСтроки
	               |ПОМЕСТИТЬ ВТНачисления
	               |ИЗ
	               |	&ИсходныеДанные КАК ИсходныеДанные";
				   
	Запрос.Выполнить();			   
	
	УчетНДФЛ.СоздатьВТВычетыКДоходамФизическихЛиц(Объект.Ссылка, Объект.Организация, Объект.ДатаПолученияДохода, Запрос.МенеджерВременныхТаблиц);
	
	Запрос.Текст = "ВЫБРАТЬ
	               |	ВычетыКДоходамФизическихЛиц.НомерСтроки КАК НомерСтроки,
	               |	ВычетыКДоходамФизическихЛиц.СуммаВычета
	               |ИЗ
	               |	ВТВычетыКДоходамФизическихЛиц КАК ВычетыКДоходамФизическихЛиц
	               |
	               |УПОРЯДОЧИТЬ ПО
	               |	НомерСтроки";
				   
	Выборка = Запрос.Выполнить().Выбрать();
	
	Пока Выборка.СледующийПоЗначениюПоля("НомерСтроки") Цикл
		НайденныеСтроки = Объект.Начисления.НайтиСтроки(Новый Структура("НомерСтроки", Выборка.НомерСтроки));
		Если НайденныеСтроки.Количество() > 0 Тогда
			ДанныеСотрудника = НайденныеСтроки[0];
			Пока Выборка.Следующий() Цикл
				Если Не ДанныеСотрудника.ФиксСуммаВычета Тогда 
					ДанныеСотрудника.СуммаВычета = ДанныеСотрудника.СуммаВычета + Выборка.СуммаВычета;
				КонецЕсли;
			КонецЦикла;
			ДанныеСотрудника.СуммаВычета = Мин(ДанныеСотрудника.СуммаВычета, ДанныеСотрудника.Результат);
		КонецЕсли;
	КонецЦикла;
	
КонецПроцедуры 

// {Локализация_ЗарплатаИУправлениеПерсоналомДляБеларуси
&НаСервере
Процедура РассчитатьВзносы(Знач СотрудникиФизическиеЛица = Неопределено, РассчитыватьВычет = Истина)
	
	Если Не ЗначениеЗаполнено(Объект.ВидДоходаСтраховыеВзносы) Тогда 
		Возврат;
	КонецЕсли;
	
	Если СотрудникиФизическиеЛица = Неопределено Тогда 
		СотрудникиФизическиеЛица = ОбщегоНазначения.ВыгрузитьКолонку(Объект.Начисления, "Сотрудник", Истина);
	КонецЕсли;
	
	Если ТипЗнч(СотрудникиФизическиеЛица) <> Тип("Массив") Тогда
		СотрудникиФизическиеЛица = ОбщегоНазначенияКлиентСервер.ЗначениеВМассиве(СотрудникиФизическиеЛица);
	КонецЕсли;
	
	ОбщегоНазначенияКлиентСервер.УдалитьЗначениеИзМассива(СотрудникиФизическиеЛица, Справочники.Сотрудники.ПустаяСсылка());
	ОбщегоНазначенияКлиентСервер.УдалитьЗначениеИзМассива(СотрудникиФизическиеЛица, Справочники.ФизическиеЛица.ПустаяСсылка());
	
	Если СотрудникиФизическиеЛица.Количество() = 0 Тогда 
		Возврат;
	КонецЕсли;
	
	МассивСотрудников = ОбщегоНазначения.ВыгрузитьКолонку(Объект.Начисления, "Сотрудник", Истина);
	ОбщегоНазначенияКлиентСервер.УдалитьЗначениеИзМассива(МассивСотрудников, Справочники.Сотрудники.ПустаяСсылка());
	ФизическиеЛицаСотрудников = ОбщегоНазначения.ЗначениеРеквизитаОбъектов(МассивСотрудников, "ФизическоеЛицо");
	
	ФизическиеЛицаКРасчету = Новый Соответствие;
	
	Если ТипЗнч(СотрудникиФизическиеЛица[0]) = Тип("СправочникСсылка.Сотрудники") Тогда
		Для Каждого Сотрудник Из СотрудникиФизическиеЛица Цикл 
			ФизическиеЛицаКРасчету.Вставить(ФизическиеЛицаСотрудников[Сотрудник], Истина);
		КонецЦикла;
	Иначе 
		Для Каждого ФизическоеЛицо Из СотрудникиФизическиеЛица Цикл 
			ФизическиеЛицаКРасчету.Вставить(ФизическоеЛицо, Истина);
		КонецЦикла;
	КонецЕсли;
	
	Если РассчитыватьВычет Тогда 
		РассчитатьВычетВзносы(ФизическиеЛицаКРасчету, ФизическиеЛицаСотрудников);
	КонецЕсли;
	
КонецПроцедуры
	
&НаСервере
Процедура РассчитатьВычетВзносы(ФизическиеЛицаКРасчету, ФизическиеЛицаСотрудников)
	
	ИсходныеДанные = Новый ТаблицаЗначений;
	ИсходныеДанные.Колонки.Добавить("ФизическоеЛицо", Новый ОписаниеТипов("СправочникСсылка.ФизическиеЛица"));
	ИсходныеДанные.Колонки.Добавить("КодДоходаВзносы", Новый ОписаниеТипов("СправочникСсылка.ВидыДоходовПоСтраховымВзносам"));
	ИсходныеДанные.Колонки.Добавить("Сумма", Новый ОписаниеТипов("Число"));
	ИсходныеДанные.Колонки.Добавить("Регистратор", Новый ОписаниеТипов("ДокументСсылка.ПризПодарок"));
	ИсходныеДанные.Колонки.Добавить("НомерСтроки", Новый ОписаниеТипов("Число"));
	
	Для Каждого ДанныеСотрудника Из Объект.Начисления Цикл 
		
		Если Не ЗначениеЗаполнено(ДанныеСотрудника.Сотрудник)
			Или Не ЗначениеЗаполнено(ДанныеСотрудника.КодВычета) Тогда
			Продолжить;
		КонецЕсли;
		
		ФизическоеЛицо = ФизическиеЛицаСотрудников[ДанныеСотрудника.Сотрудник];
		Если ФизическиеЛицаКРасчету[ФизическоеЛицо] = Неопределено Тогда 
			Продолжить;
		КонецЕсли;
		
		Если Не ДанныеСотрудника.ФиксСуммаВычета Тогда 
			ДанныеСотрудника.СуммаВычетаВзносы = 0;
		КонецЕсли;
		
		НоваяСтрока = ИсходныеДанные.Добавить();
		НоваяСтрока.ФизическоеЛицо = ФизическоеЛицо;
		НоваяСтрока.КодДоходаВзносы = Объект.ВидДоходаСтраховыеВзносы;
		НоваяСтрока.Сумма = ДанныеСотрудника.Результат;
		НоваяСтрока.Регистратор = Объект.Ссылка;
		НоваяСтрока.НомерСтроки = ДанныеСотрудника.НомерСтроки;
		
	КонецЦикла;
	
	Запрос = Новый Запрос;
	
	Запрос.МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
	
	Запрос.УстановитьПараметр("ИсходныеДанные", ИсходныеДанные);
	
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	ИсходныеДанные.ФизическоеЛицо,
	|	ИсходныеДанные.КодДоходаВзносы,
	|	ИсходныеДанные.Сумма,
	|	ИсходныеДанные.Регистратор,
	|	ИсходныеДанные.НомерСтроки
	|ПОМЕСТИТЬ ВТНачисления
	|ИЗ
	|	&ИсходныеДанные КАК ИсходныеДанные";
	
	Запрос.Выполнить();	
	
	УчетСтраховыхВзносов.СоздатьВТВычетыПоВзносамФизическихЛиц(Объект.Ссылка, Объект.Организация, Объект.ДатаПолученияДохода, Запрос.МенеджерВременныхТаблиц);

	Запрос.Текст = 
	"ВЫБРАТЬ
	|	ВычетыКДоходамФизическихЛиц.ФизическоеЛицо,
	|	ВычетыКДоходамФизическихЛиц.ВидДохода КАК КодДоходаВзносы,
	|	ВычетыКДоходамФизическихЛиц.СуммаВычета КАК СуммаВычетаВзносы,
	|	ВычетыКДоходамФизическихЛиц.НомерСтроки
	|ИЗ
	|	ВТВычетыКДоходамФизическихЛицВзносы КАК ВычетыКДоходамФизическихЛиц";
				   
	РезультатЗапроса = Запрос.Выполнить();
		
	Выборка = РезультатЗапроса.Выбрать();
	Пока Выборка.СледующийПоЗначениюПоля("НомерСтроки") Цикл
		НайденныеСтроки = Объект.Начисления.НайтиСтроки(Новый Структура("НомерСтроки", Выборка.НомерСтроки));
		Если НайденныеСтроки.Количество() > 0 Тогда
			ДанныеСотрудника = НайденныеСтроки[0];
			Пока Выборка.Следующий() Цикл
				Если Не ДанныеСотрудника.ФиксСуммаВычета Тогда 
					ДанныеСотрудника.СуммаВычетаВзносы = ДанныеСотрудника.СуммаВычетаВзносы + Выборка.СуммаВычетаВзносы;
				КонецЕсли;
			КонецЦикла;
			ДанныеСотрудника.СуммаВычетаВзносы = Мин(ДанныеСотрудника.СуммаВычетаВзносы, ДанныеСотрудника.Результат);
			ДанныеСотрудника.КодДоходаВзносы = Выборка.КодДоходаВзносы;
		КонецЕсли;
	КонецЦикла;
	
КонецПроцедуры

&НаКлиенте
Процедура ВидДоходаСтраховыеВзносыПриИзменении(Элемент)
		
	РасчетЗарплатыРасширенныйКлиент.СтрокаРасчетаПриОкончанииРедактирования(ЭтаФорма, ОписаниеТаблицыНачислений(), Истина, , ОписаниеДокумента(ЭтаФорма));
	ЗарплатаКадрыКлиентСервер.КлючевыеРеквизитыЗаполненияФормыУстановитьОтображениеПредупреждения(ЭтаФорма);  
	
КонецПроцедуры
 
// Локализация_ЗарплатаИУправлениеПерсоналомДляБеларуси}

&НаКлиентеНаСервереБезКонтекста
Функция ОписаниеДокумента(Форма)
	
	Описание = РасчетЗарплатыРасширенныйКлиентСервер.ОписаниеРасчетногоДокумента();
	Описание.МесяцНачисленияИмя		= "ПериодРегистрации";
	Описание.ПериодДействияВШапке	= Истина;
	Описание.ИменаПолейНачисления	= "ВидПризаПодарка";
	Описание.ВидНачисленияВШапке	= Истина;
	Описание.ВидНачисленияИмя		= "ВидПризаПодарка";
	
	Описание.НачисленияИмя			= "Начисления";
	Описание.БухучетПервичногоДокументаИмяМетода = "Документы.ПризПодарок.ДанныеДляБухучетаЗарплатыПервичныхДокументов";
	
	Описание.ПорядокВыплатыИмя = Неопределено;
	Описание.ПланируемаяДатаВыплатыИмя = "ДатаПолученияДохода";
	
	Описание.Вставить("УстанавливатьФиксРасчет", Ложь);
	
	Возврат Описание;
	
КонецФункции

&НаКлиентеНаСервереБезКонтекста
Функция ОписаниеТаблицыНачислений()
	
	ОписаниеТаблицы = РасчетЗарплатыРасширенныйКлиентСервер.ОписаниеТаблицыРасчета();
	ОписаниеТаблицы.СодержитПолеСотрудник						= Истина;
	ОписаниеТаблицы.ИмяРеквизитаСотрудник						= "Сотрудник";
	ОписаниеТаблицы.ИмяРеквизитаВидРасчета						= "ВидПризаПодарка";
	ОписаниеТаблицы.СодержитПолеВидРасчета						= Ложь;
	ОписаниеТаблицы.ИмяРеквизитаДатаНачала 						= Неопределено;
	ОписаниеТаблицы.ИмяРеквизитаДатаОкончания 					= Неопределено;
	ОписаниеТаблицы.ИмяРеквизитаПериод 							= "ДатаПолученияДохода";
	ОписаниеТаблицы.СодержитПолеКодВычета 						= Истина;
	
	ОписаниеТаблицы.ОтменятьВсеИсправления 						= Истина;
	
	ОписаниеТаблицы.ИмяПоляДляВставкиРаспределенияРезультатов 	= "НачисленияРезультат";
	ОписаниеТаблицы.ВставлятьПослеПоля 							= Истина;
	ОписаниеТаблицы.ОтображатьПоляРаспределенияРезультатов 		= Истина;
	ОписаниеТаблицы.РаспределениеРезультатовЗависимыеТаблицы    = "НДФЛ";
	
	Возврат ОписаниеТаблицы;
	
КонецФункции

&НаСервере
Процедура НачисленияПослеУдаленияНаСервере(Сотрудник)
	
	РасчетЗарплатыРасширенный.ОчиститьДанныеФормыПоСотруднику(ЭтаФорма, ОписаниеДокумента(ЭтотОбъект), Сотрудник, Объект.Организация);
	ЗарплатаКадрыКлиентСервер.КлючевыеРеквизитыЗаполненияФормыУстановитьОтображениеПредупреждения(ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Процедура ОбработатьИзменениеСотрудника(ИдентификаторСтроки, ПрежнийСотрудник)
	
	УстановитьПривилегированныйРежим(Истина);
	
	Сотрудник = Объект.Начисления.НайтиПоИдентификатору(ИдентификаторСтроки).Сотрудник;
	
	ОписаниеТаблицы = ОписаниеТаблицыНачислений();
	
	ДополнитьСтрокуНаСервере(ИдентификаторСтроки);
	
	РасчетЗарплатыРасширенный.ОбработатьИзменениеСотрудникаВедущейТаблицыФормы(
		ЭтаФорма, ОписаниеДокумента(ЭтаФорма), Сотрудник, ПрежнийСотрудник);
	
КонецПроцедуры

&НаСервере
Процедура ДополнитьСтрокуНаСервере(ИдентификаторСтроки)
	
	ДополнитьСтрокиНаСервере(ОбщегоНазначенияКлиентСервер.ЗначениеВМассиве(ИдентификаторСтроки));
	
КонецПроцедуры

&НаСервере
Процедура ДополнитьСтрокиНаСервере(ИдентификаторыСтрок)
	
	УстановитьПривилегированныйРежим(Истина);
	
	СписокСотрудников = Новый Массив;
	
	Для Каждого ИдентификаторСтроки Из ИдентификаторыСтрок Цикл 
		ДанныеСотрудника = Объект.Начисления.НайтиПоИдентификатору(ИдентификаторСтроки);
		Если ЗначениеЗаполнено(ДанныеСотрудника.Сотрудник) Тогда 
			СписокСотрудников.Добавить(ДанныеСотрудника.Сотрудник);
		КонецЕсли;
	КонецЦикла;
	
	КадровыеДанные = КадровыйУчет.КадровыеДанныеСотрудников(Истина, СписокСотрудников, "Подразделение", Объект.ДатаПолученияДохода);
	
	Для Каждого КадровыеДанныеСотрудника Из КадровыеДанные Цикл 
		НайденныеСтроки = Объект.Начисления.НайтиСтроки(Новый Структура("Сотрудник", КадровыеДанныеСотрудника.Сотрудник));
		Для Каждого НайденнаяСтрока Из НайденныеСтроки Цикл
			НайденнаяСтрока.Подразделение = КадровыеДанныеСотрудника.Подразделение;
			НайденнаяСтрока.КодВычета = Объект.КодВычетаНДФЛ;
		КонецЦикла;
	КонецЦикла;
	
КонецПроцедуры

&НаКлиенте
Процедура РассчитатьСотрудника(Сотрудник, ОписаниеТаблицы) Экспорт
	
	Если Не РасчетЗарплатыРасширенныйКлиент.ДобавитьСотрудникаКРасчету(ЭтаФорма, Сотрудник, ОписаниеТаблицы) Тогда
		РассчитатьСотрудникаНаСервере(Сотрудник, ОписаниеТаблицы);
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура РассчитатьСотрудникаНаСервере(Сотрудник, ОписаниеТаблицы) 
	
	РассчитатьНДФЛ(ОбщегоНазначенияКлиентСервер.ЗначениеВМассиве(Сотрудник));
	//
	РассчитатьВзносы(ОбщегоНазначенияКлиентСервер.ЗначениеВМассиве(Сотрудник));
	//
	
КонецПроцедуры

&НаСервере
Процедура ОрганизацияПриИзмененииНаСервере()
	
	ЗаполнитьДанныеФормыПоОрганизации();
	УстановитьФункциональныеОпцииФормы();
	
КонецПроцедуры

&НаСервере
Процедура ВидПризаПодаркаПриИзмененииНаСервере()
	
	УстановитьКодДохода();
	ЗаполнитьСписокВыбораКодДоходаНДФЛ();
	УстановитьКодВычета();
	РассчитатьНДФЛ();
	
КонецПроцедуры

&НаСервере
Процедура КодДоходаНДФЛПриИзмененииНаСервере()
	
	УстановитьКодВычета();
	РассчитатьНДФЛ();
	
КонецПроцедуры

&НаСервере
Процедура КодВычетаНДФЛПриИзмененииНаСервере()
	
	УстановитьКодВычетаСотрудников();
	РассчитатьНДФЛ();
	
КонецПроцедуры

&НаСервере
Процедура ДатаПолученияДоходаПриИзмененииНаСервере()
	
	УстановитьКодВычета();
	РассчитатьНДФЛ();
	
КонецПроцедуры

&НаСервере
Процедура ПредусмотреноКолдоговоромПриИзмененииНаСервере()
	
	УстановитьВидДоходаСтраховыхВзносов();
	
КонецПроцедуры

&НаСервере
Процедура УстановитьВидДоходаСтраховыхВзносов()
	
	Если Объект.ПредусмотреноКолдоговором Тогда
		Объект.ВидДоходаСтраховыеВзносы  = Справочники.ВидыДоходовПоСтраховымВзносам.ОблагаетсяЦеликом;
	Иначе
		Объект.ВидДоходаСтраховыеВзносы  = Справочники.ВидыДоходовПоСтраховымВзносам.НеОблагаетсяПолностью;
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура УстановитьФункциональныеОпцииФормы()
	
	ПараметрыФО = Новый Структура("Организация, Период", Объект.Организация, НачалоДня(Объект.ПериодРегистрации));
	УстановитьПараметрыФункциональныхОпцийФормы(ПараметрыФО);
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьДанныеФормыПоОрганизации()
	
	Если НЕ ЗначениеЗаполнено(Объект.Организация) Тогда
		Возврат;
	КонецЕсли; 
	
	ЗапрашиваемыеЗначения = Новый Структура;
	ЗапрашиваемыеЗначения.Вставить("Организация", "Объект.Организация");
	
	ЗапрашиваемыеЗначения.Вставить("Руководитель", "Объект.Руководитель");
	ЗапрашиваемыеЗначения.Вставить("ДолжностьРуководителя", "Объект.ДолжностьРуководителя");
	
	ЗарплатаКадры.ЗаполнитьЗначенияВФорме(ЭтаФорма, ЗапрашиваемыеЗначения, ОбщегоНазначенияКлиентСервер.ЗначениеВМассиве("Организация"));	
	
КонецПроцедуры

&НаСервере
Функция АдресСпискаПодобранныхСотрудников()
	
	Возврат ПоместитьВоВременноеХранилище(ОбщегоНазначения.ВыгрузитьКолонку(Объект.Начисления, "Сотрудник"), УникальныйИдентификатор);
	
КонецФункции

&НаСервере
Процедура ОбработкаПодбораНаСервере(Знач Сотрудники)
	
	ИдентификаторыСтрок = Новый Массив;
	
	Если ТипЗнч(Сотрудники) <> Тип("Массив") Тогда
		Сотрудники = ОбщегоНазначенияКлиентСервер.ЗначениеВМассиве(Сотрудники);
	КонецЕсли;
	
	Для Каждого Сотрудник Из Сотрудники Цикл
		СтрокиНачислений = Объект.Начисления.НайтиСтроки(Новый Структура("Сотрудник", Сотрудник));
		
		Если СтрокиНачислений.Количество() = 0 Тогда
			СтрокаНачисления = Объект.Начисления.Добавить();
			СтрокаНачисления.Сотрудник = Сотрудник;
		Иначе
			СтрокаНачисления = СтрокиНачислений[0];
		КонецЕсли;
		
		ИдентификаторыСтрок.Добавить(СтрокаНачисления.ПолучитьИдентификатор());
	КонецЦикла;
	
	ОбработатьДобавлениеСотрудников(ИдентификаторыСтрок, Сотрудники);
	
	ЗарплатаКадрыКлиентСервер.КлючевыеРеквизитыЗаполненияФормыУстановитьОтображениеПредупреждения(ЭтаФорма);
	
КонецПроцедуры

&НаСервере
Процедура ОбработатьДобавлениеСотрудников(ИдентификаторыСтрок, Сотрудники)
	
	УстановитьПривилегированныйРежим(Истина);
	
	ДополнитьСтрокиНаСервере(ИдентификаторыСтрок);
	
	// Заполняем поля по итогам заполнения коллекций.
	ФизическиеЛицаСотрудников = ОбщегоНазначения.ЗначениеРеквизитаОбъектов(Сотрудники, "ФизическоеЛицо");

	Для Каждого ИдентификаторСтроки Из ИдентификаторыСтрок Цикл
		СтрокаТаблицыНачислений = Объект.Начисления.НайтиПоИдентификатору(ИдентификаторСтроки);
		ФизическоеЛицоСтроки = ФизическиеЛицаСотрудников[СтрокаТаблицыНачислений.Сотрудник];
	КонецЦикла;
	
КонецПроцедуры

&НаКлиенте
Процедура ПересчитатьДокументНаКлиенте()
	
	ПересчитатьДокументНаСервере(СотрудникиКРасчету);
	СотрудникиКРасчету.Очистить();
	РасчетЗарплатыКлиент.УстановитьОтображениеКнопкиПересчитать(ЭтаФорма, Ложь);
	
КонецПроцедуры

&НаСервере
Процедура ПересчитатьДокументНаСервере(СотрудникиКРасчету)
	
	Для Каждого ОписаниеСотрудников Из СотрудникиКРасчету Цикл
		Сотрудники = ОбщегоНазначения.ВыгрузитьКолонку(ОписаниеСотрудников.Значение, "Ключ");
		РассчитатьНДФЛ(Сотрудники);
		//
		РассчитатьВзносы(Сотрудники);
		//
	КонецЦикла;
	
КонецПроцедуры

#Область КонтролируемыеПоля

&НаСервере
Функция ПолучитьКонтролируемыеПоля() Экспорт
	
	НачисленияФиксРасчет = Новый Массив;
	НачисленияФиксРасчет.Добавить("Сотрудник");
	НачисленияФиксРасчет.Добавить("Результат");
	
	НачисленияФиксЗаполнение = Новый Массив;
	
	Возврат Новый Структура("Начисления",
		Новый Структура("ФиксРасчет, ФиксЗаполнение", НачисленияФиксРасчет, НачисленияФиксЗаполнение));
	
КонецФункции

&НаКлиенте
Функция ПолучитьСтарыеЗначенияКонтролируемыхПолей() Экспорт
	
	Возврат СтарыеЗначенияКонтролируемыхПолей;
	
КонецФункции

#КонецОбласти

#Область КлючевыеРеквизитыЗаполненияФормы

&НаСервере
// Функция возвращает описание таблиц формы подключенных к механизму ключевых реквизитов формы.
Функция КлючевыеРеквизитыЗаполненияФормыТаблицыОчищаемыеПриИзменении() Экспорт
	
	Массив = Новый Массив;
	Массив.Добавить("Объект.Начисления");
	Массив.Добавить("Объект.ФизическиеЛица");
	
	Возврат Массив;
	
КонецФункции

&НаСервере
// Функция возвращает массив реквизитов формы подключенных к механизму ключевых реквизитов формы.
Функция КлючевыеРеквизитыЗаполненияФормыОписаниеКлючевыхРеквизитов() Экспорт
	
	Массив = Новый Массив;
	Массив.Добавить(Новый Структура("ЭлементФормы, Представление", "Организация", НСтр("ru = 'организации'")));
	
	Возврат Массив;
	
КонецФункции

#КонецОбласти

СтарыеЗначенияКонтролируемыхПолей = Новый Соответствие;
СотрудникиКРасчету = Новый Соответствие;

#КонецОбласти
