﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ОбработчикиСобытий

Процедура ПередЗаписью(Отказ, РежимЗаписи, РежимПроведения)
	
	Если ОбменДанными.Загрузка Тогда
		Возврат;
	КонецЕсли;

	ОбновлениеИнформационнойБазы.ПроверитьОбъектОбработан(ЭтотОбъект);

КонецПроцедуры

Процедура ПриЗаписи(Отказ)
	
	Если ОбменДанными.Загрузка Тогда
		Возврат;
	КонецЕсли;
	
	Если Не Отказ Тогда
		ПроведениеСерверУТ.ИнициализироватьДополнительныеСвойстваДляПроведения(Ссылка, ДополнительныеСвойства);
		Документы.ИнвентаризацияНМА.ИнициализироватьДанныеДокумента(Ссылка, ДополнительныеСвойства, "РеестрДокументов,ДокументыПоНМА");
		РегистрыСведений.РеестрДокументов.ЗаписатьДанныеДокумента(Ссылка, ДополнительныеСвойства, Отказ);
		РегистрыСведений.ДокументыПоНМА.ЗаписатьДанныеДокумента(Ссылка, ДополнительныеСвойства, Отказ);
	КонецЕсли;
	
КонецПроцедуры

Процедура ПриКопировании(ОбъектКопирования)
	
	Ответственный = Пользователи.ТекущийПользователь();
	
	Документы.ИнвентаризацияНМА.ЗаполнитьДанныеУчета(ЭтотОбъект, НМА);
	
КонецПроцедуры

Процедура ОбработкаЗаполнения(ДанныеЗаполнения, СтандартнаяОбработка)
	
	Организация = ЗначениеНастроекПовтИсп.ПолучитьОрганизациюПоУмолчанию(Организация);
	
КонецПроцедуры

Процедура ОбработкаПроверкиЗаполнения(Отказ, ПроверяемыеРеквизиты)
	
	НепроверяемыеРеквизиты = Новый Массив;
	НепроверяемыеРеквизиты.Добавить("НМА.СтоимостьФактическая");
	
	ВнеоборотныеАктивы.ПроверитьСоответствиеДатыВерсииУчета(ЭтотОбъект, Истина, Отказ);
	
	ВнеоборотныеАктивы.ПроверитьОтсутствиеДублейВТабличнойЧасти(ЭтотОбъект, "НМА", "НематериальныйАктив", Отказ);
		
	ПроверитьТабличнуюЧасть(Отказ);
	
	ОбщегоНазначения.УдалитьНепроверяемыеРеквизитыИзМассива(ПроверяемыеРеквизиты, НепроверяемыеРеквизиты);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

Процедура ПроверитьТабличнуюЧасть(Отказ)

	Для каждого ДанныеСтроки Из НМА Цикл
		Если ДанныеСтроки.НаличиеФактическое И ДанныеСтроки.СтоимостьФактическая = 0 Тогда
			Путь = ОбщегоНазначенияКлиентСервер.ПутьКТабличнойЧасти("НМА", ДанныеСтроки.НомерСтроки, "СтоимостьФактическая");
			ТекстСообщения = НСтр("ru = 'Не заполнено поле ""Фактическая стоимость"" в строке ""%1"" табличной части ""Нематериальные активы"".';
									|en = 'The ""Actual cost"" field is empty in row ""%1"" of tabular section ""Intangible assets"".'");
			ТекстСообщения = СтрШаблон(ТекстСообщения, ДанныеСтроки.НомерСтроки);
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, ЭтотОбъект, Путь,, Отказ);
		КонецЕсли;
	КонецЦикла; 
	
КонецПроцедуры
 
#КонецОбласти

#КонецЕсли
