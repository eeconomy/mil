﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ОбработчикиСобытий

Процедура ПриКопировании(ОбъектКопирования)
	
	ИнициализироватьДокумент();
	
КонецПроцедуры

Процедура ОбработкаЗаполнения(ДанныеЗаполнения, СтандартнаяОбработка)
	
	ИнициализироватьДокумент();
	
КонецПроцедуры

Процедура ПередЗаписью(Отказ, РежимЗаписи, РежимПроведения)
	
	Если ОбменДанными.Загрузка Тогда
		Возврат;
	КонецЕсли;
	
	ОбновлениеИнформационнойБазы.ПроверитьОбъектОбработан(ЭтотОбъект);
	
	ДополнительныеСвойства.Вставить("ЭтоНовый", ЭтоНовый());
	ДополнительныеСвойства.Вставить("РежимЗаписи", РежимЗаписи);
	
КонецПроцедуры

Процедура ОбработкаПроверкиЗаполнения(Отказ, ПроверяемыеРеквизиты)
	
	НепроверяемыеРеквизиты = Новый Массив;
	
	// Проверка характеристик в т.ч. товары.
	НоменклатураСервер.ПроверитьЗаполнениеХарактеристик(ЭтотОбъект, НепроверяемыеРеквизиты, Отказ);
	
	Для каждого СтрТабл Из Товары Цикл
		// если не заполнено ни одно из полей количество
		Если СтрТабл.Количество = 0
			И СтрТабл.КоличествоНаСкладе = 0 Тогда
			
			АдресСообщения = НСтр("ru='в строке %НомерСтроки% списка ""Товары""'");
			АдресСообщения = СтрЗаменить(АдресСообщения, "%НомерСтроки%", СтрТабл.НомерСтроки);
			Поле = ОбщегоНазначенияКлиентСервер.ПутьКТабличнойЧасти("Товары", СтрТабл.НомерСтроки, "НомерСтроки");
			ТекстСообщения = НСтр("ru='Не заполнено количество материала для передачи или перемещения'");
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения + " " + АдресСообщения, ЭтотОбъект, Поле, , Отказ);
			
		КонецЕсли;
		
		// проверим обязательность заполнения полей для документа "Перемещение в эксплуатации"
		Если СтрТабл.Количество > 0 И (НЕ ЗначениеЗаполнено(СтрТабл.ПартияТМЦВЭксплуатации)
				ИЛИ НЕ ЗначениеЗаполнено(СтрТабл.ФизическоеЛицо)) Тогда
			
			АдресСообщения = НСтр("ru='в строке %НомерСтроки% списка ""Товары""'");
			АдресСообщения = СтрЗаменить(АдресСообщения, "%НомерСтроки%", СтрТабл.НомерСтроки);
			Поле = ОбщегоНазначенияКлиентСервер.ПутьКТабличнойЧасти("Товары", СтрТабл.НомерСтроки, "НомерСтроки");
			ТекстСообщения = НСтр("ru='Не полностью заполнены исходные данные'");
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения + " " + АдресСообщения, ЭтотОбъект, Поле, , Отказ);
			
		КонецЕсли;
		
		// проверим обязательность заполнения полей для документа "Внутреннее потребление товаров"
		Если СтрТабл.КоличествоНаСкладе > 0 И (НЕ ЗначениеЗаполнено(СтрТабл.СтатьяРасходов)
				ИЛИ НЕ ЗначениеЗаполнено(СтрТабл.АналитикаРасходов) ИЛИ НЕ ЗначениеЗаполнено(СтрТабл.КатегорияЭксплуатации)) Тогда
			
			АдресСообщения = НСтр("ru='в строке %НомерСтроки% списка ""Товары""'");
			АдресСообщения = СтрЗаменить(АдресСообщения, "%НомерСтроки%", СтрТабл.НомерСтроки);
			Поле = ОбщегоНазначенияКлиентСервер.ПутьКТабличнойЧасти("Товары", СтрТабл.НомерСтроки, "НомерСтроки");
			ТекстСообщения = НСтр("ru='Не полностью заполнены исходные данные'");
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения + " " + АдресСообщения, ЭтотОбъект, Поле, , Отказ);
			
		КонецЕсли;
	КонецЦикла;
	
	Если Товары.Итог("КоличествоНаСкладе") = 0 Тогда
		НепроверяемыеРеквизиты.Добавить("Склад");
	КонецЕсли;
	
	Если Товары.Итог("Количество") = 0 Тогда
		НепроверяемыеРеквизиты.Добавить("Подразделение");
	КонецЕсли;
	
	ОбщегоНазначения.УдалитьНепроверяемыеРеквизитыИзМассива(ПроверяемыеРеквизиты, НепроверяемыеРеквизиты);
	
КонецПроцедуры

Процедура ОбработкаПроведения(Отказ, РежимПроведения)
	
	Если Товары.Итог("Количество") > 0 Тогда
		СоздатьДокументПеремещениеВЭксплуатации(Отказ);
	КонецЕсли;
	
	Если Товары.Итог("КоличествоНаСкладе") > 0 Тогда
		СоздатьДокументПередачаВЭксплуатацию(Отказ);
	КонецЕсли;
	
КонецПроцедуры

Процедура ОбработкаУдаленияПроведения(Отказ)
	
	ДокументПередачаВЭксплуатацию = Документы.ВнутреннееПотреблениеТоваров.НайтиПоРеквизиту("ДокументОснование", Ссылка);
	Если ЗначениеЗаполнено(ДокументПередачаВЭксплуатацию) 
		И НЕ ОбщегоНазначения.ЗначениеРеквизитаОбъекта(ДокументПередачаВЭксплуатацию, "ПометкаУдаления") Тогда
		
		ДокументВнутреннегоПотребленияОбъект = ДокументПередачаВЭксплуатацию.ПолучитьОбъект();
		ДокументВнутреннегоПотребленияОбъект.УстановитьПометкуУдаления(Истина);
	КонецЕсли;
	
	ДокументПеремещениеВЭксплуатацию = Документы.ПеремещениеВЭксплуатации.НайтиПоРеквизиту("ДокументОснование", Ссылка);
	Если ЗначениеЗаполнено(ДокументПеремещениеВЭксплуатацию)
		И НЕ ОбщегоНазначения.ЗначениеРеквизитаОбъекта(ДокументПеремещениеВЭксплуатацию, "ПометкаУдаления")  Тогда
		
		ДокументПеремещениеВЭксплОбъект = ДокументПеремещениеВЭксплуатацию.ПолучитьОбъект();
		ДокументПеремещениеВЭксплОбъект.УстановитьПометкуУдаления(Истина);
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

Процедура СоздатьДокументПередачаВЭксплуатацию(Отказ)
	
	Если Отказ Тогда
		Возврат;
	КонецЕсли;
	
	ДокументПередачаВЭксплуатацию = Документы.ВнутреннееПотреблениеТоваров.НайтиПоРеквизиту("ДокументОснование", Ссылка);
	
	Если ЗначениеЗаполнено(ДокументПередачаВЭксплуатацию) Тогда
		ДокументВнутреннегоПотребленияОбъект = ДокументПередачаВЭксплуатацию.ПолучитьОбъект();
		Если ДокументВнутреннегоПотребленияОбъект.ПометкаУдаления Тогда
			ДокументВнутреннегоПотребленияОбъект.УстановитьПометкуУдаления(Ложь);
		КонецЕсли;
		// 4D:Милваица, Антон, 01.10.19
		// остается старое движение по себестоимости со старыми идентификаторами строки, №23291
		// {
		Попытка
			ДокументВнутреннегоПотребленияОбъект.Записать(?(ДокументВнутреннегоПотребленияОбъект.Проведен,
										РежимЗаписиДокумента.ОтменаПроведения, РежимЗаписиДокумента.Запись));
		Исключение
			Отказ = Истина;
		КонецПопытки;
		// } 4D
	Иначе
		ДокументВнутреннегоПотребленияОбъект = Документы.ВнутреннееПотреблениеТоваров.СоздатьДокумент();
	КонецЕсли;
	
	ДокументВнутреннегоПотребленияОбъект.ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ПередачаВЭксплуатацию;
	ДокументВнутреннегоПотребленияОбъект.ДокументОснование = ЭтотОбъект.Ссылка;
	ДокументВнутреннегоПотребленияОбъект.Подразделение = ПодразделениеПолучатель;
	ДокументВнутреннегоПотребленияОбъект.Грузополучатель = Грузополучатель;
	ДокументВнутреннегоПотребленияОбъект.ВидЦены = ВидЦены;
	ДокументВнутреннегоПотребленияОбъект.Склад = Склад;
	ДокументВнутреннегоПотребленияОбъект.Организация = Организация;
	ДокументВнутреннегоПотребленияОбъект.Ответственный = ПараметрыСеанса.ТекущийПользователь;
	ДокументВнутреннегоПотребленияОбъект.Дата = Дата;
	// 4D:Милавица, Михаил, 12.06.2019 15:54:49
	// Печать ТТН документа Движение материалов в эксплуатации, №21248
	// {
	ЗаполнитьЗначенияСвойств(ДокументВнутреннегоПотребленияОбъект, ЭтотОбъект, "СерияБланкаСтрогойОтчетности, НомерБланкаСтрогойОтчетности");
	// }
	// 4D
	ДокументВнутреннегоПотребленияОбъект.Товары.Очистить();
	
	Для каждого ТекущаяСтрокаТоваров Из Товары Цикл
		
		Если ТекущаяСтрокаТоваров.КоличествоНаСкладе > 0 Тогда
			НоваяСтрокаТоваров = ДокументВнутреннегоПотребленияОбъект.Товары.Добавить();
			ЗаполнитьЗначенияСвойств(НоваяСтрокаТоваров, ТекущаяСтрокаТоваров);
			НоваяСтрокаТоваров.Количество = ТекущаяСтрокаТоваров.КоличествоНаСкладе;
			НоваяСтрокаТоваров.КоличествоУпаковок = ТекущаяСтрокаТоваров.КоличествоНаСкладе;
			НоваяСтрокаТоваров.ФизическоеЛицо = ТекущаяСтрокаТоваров.ФизическоеЛицоПолучатель;
		КонецЕсли;
		
	КонецЦикла;
	
	ДокументВнутреннегоПотребленияПараметрыУказанияСерий = НоменклатураСервер.ПараметрыУказанияСерий(ДокументВнутреннегоПотребленияОбъект,
			Документы.ВнутреннееПотреблениеТоваров);
	НоменклатураСервер.ЗаполнитьСерииПоFEFO(ДокументВнутреннегоПотребленияОбъект, ДокументВнутреннегоПотребленияПараметрыУказанияСерий, Ложь);
	
	ДокументВнутреннегоПотребленияОбъект.Комментарий = "Документ создан автоматически через документ " + Ссылка + ".";
	
	Попытка
		ДокументВнутреннегоПотребленияОбъект.Записать(РежимЗаписиДокумента.Проведение);
		ТекстСообщения = НСтр("ru='Создан и проведен документ Внутреннего потребления товаров %1.'");
		ТекстСообщения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ТекстСообщения, ДокументВнутреннегоПотребленияОбъект);
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(
			ТекстСообщения,
			ДокументВнутреннегоПотребленияОбъект);
	Исключение
		Отказ = Истина;
	КонецПопытки;
	
КонецПроцедуры

Процедура СоздатьДокументПеремещениеВЭксплуатации(Отказ)
	
	Если Отказ Тогда
		Возврат;
	КонецЕсли;
	
	ДокументПеремещениеВЭксплуатацию = Документы.ПеремещениеВЭксплуатации.НайтиПоРеквизиту("ДокументОснование", Ссылка);
	
	Если ЗначениеЗаполнено(ДокументПеремещениеВЭксплуатацию) Тогда
		ДокументПеремещениеВЭксплОбъект = ДокументПеремещениеВЭксплуатацию.ПолучитьОбъект();
		Если ДокументПеремещениеВЭксплОбъект.ПометкаУдаления Тогда
			ДокументПеремещениеВЭксплОбъект.УстановитьПометкуУдаления(Ложь);
		КонецЕсли;
		// 4D:Милваица, Антон, 01.10.19
		// остается старое движение по себестоимости со старыми идентификаторами строки, №23291
		// {
		Попытка
			ДокументПеремещениеВЭксплОбъект.Записать(?(ДокументПеремещениеВЭксплОбъект.Проведен,
							РежимЗаписиДокумента.ОтменаПроведения, РежимЗаписиДокумента.Запись));
		Исключение
			Отказ = Истина;
		КонецПопытки;
		// } 4D
	Иначе
		ДокументПеремещениеВЭксплОбъект = Документы.ПеремещениеВЭксплуатации.СоздатьДокумент();
	КонецЕсли;
	
	ДокументПеремещениеВЭксплОбъект.ДокументОснование       = ЭтотОбъект.Ссылка;
	ДокументПеремещениеВЭксплОбъект.Организация             = Организация;
	ДокументПеремещениеВЭксплОбъект.Подразделение           = Подразделение;
	ДокументПеремещениеВЭксплОбъект.ПодразделениеПолучатель = ПодразделениеПолучатель;
	ДокументПеремещениеВЭксплОбъект.Грузополучатель         = Грузополучатель;
	ДокументПеремещениеВЭксплОбъект.ВидЦены                 = ВидЦены;
	ДокументПеремещениеВЭксплОбъект.Ответственный           = ПараметрыСеанса.ТекущийПользователь;
	ДокументПеремещениеВЭксплОбъект.Дата                    = Дата + 1;
	// 4D:Милавица, Михаил, 12.06.2019
	// Печать ТТН документа Движение материалов в эксплуатации, №21248
	// {
	ЗаполнитьЗначенияСвойств(ДокументПеремещениеВЭксплОбъект, ЭтотОбъект, "СерияБланкаСтрогойОтчетности, НомерБланкаСтрогойОтчетности");
	// }
	// 4D
	ДокументПеремещениеВЭксплОбъект.Товары.Очистить();
	
	Для каждого ТекущаяСтрокаТоваров Из ЭтотОбъект.Товары Цикл
		Если ТекущаяСтрокаТоваров.Количество > 0 Тогда
			НоваяСтрокаТоваров = ДокументПеремещениеВЭксплОбъект.Товары.Добавить();
			ЗаполнитьЗначенияСвойств(НоваяСтрокаТоваров, ТекущаяСтрокаТоваров);
		КонецЕсли;
	КонецЦикла;
	
	ДокументПеремещениеВЭксплОбъект.Комментарий = "Документ создан программно через документ " + Ссылка + ".";
	
	Попытка
		ДокументПеремещениеВЭксплОбъект.Записать(РежимЗаписиДокумента.Проведение);
		ТекстСообщения = НСтр("ru='Создан и проведен документ %1.'");
		ТекстСообщения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ТекстСообщения, ДокументПеремещениеВЭксплОбъект);
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(
			ТекстСообщения,
			ДокументПеремещениеВЭксплОбъект);
	Исключение
		Отказ = Истина;
	КонецПопытки;
	
КонецПроцедуры

Процедура ИнициализироватьДокумент(ДанныеЗаполнения = Неопределено)
	
	Ответственный = Пользователи.ТекущийПользователь();
	Организация   = ЗначениеНастроекПовтИсп.ПолучитьОрганизациюПоУмолчанию(Организация);
	Подразделение = ЗначениеНастроекПовтИсп.ПодразделениеПользователя(Ответственный, Подразделение);
	
КонецПроцедуры

#КонецОбласти

#КонецЕсли