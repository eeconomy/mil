﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ПрограммныйИнтерфейс

// СтандартныеПодсистемы.ВерсионированиеОбъектов

// Определяет настройки объекта для подсистемы ВерсионированиеОбъектов.
//
// Параметры:
//  Настройки - Структура - настройки подсистемы.
Процедура ПриОпределенииНастроекВерсионированияОбъектов(Настройки) Экспорт
	
КонецПроцедуры

// Конец СтандартныеПодсистемы.ВерсионированиеОбъектов

// Определяет список команд создания на основании.
//
// Параметры:
//   КомандыСозданияНаОсновании - ТаблицаЗначений - Таблица с командами создания на основании. Для изменения.
//       См. описание 1 параметра процедуры СозданиеНаОснованииПереопределяемый.ПередДобавлениемКомандСозданияНаОсновании().
//   Параметры - Структура - Вспомогательные параметры. Для чтения.
//       См. описание 2 параметра процедуры СозданиеНаОснованииПереопределяемый.ПередДобавлениемКомандСозданияНаОсновании().
//
Процедура ДобавитьКомандыСозданияНаОсновании(КомандыСозданияНаОсновании, Параметры) Экспорт
	
КонецПроцедуры

// Добавляет команду создания документа "Перемещение в эксплуатации".
//
// Параметры:
//   КомандыСозданияНаОсновании - ТаблицаЗначений - Таблица с командами создания на основании. Для изменения.
//       См. описание 1 параметра процедуры СозданиеНаОснованииПереопределяемый.ПередДобавлениемКомандСозданияНаОсновании().
//
Функция ДобавитьКомандуСоздатьНаОсновании(КомандыСозданияНаОсновании) Экспорт
	Если ПравоДоступа("Добавление", Метаданные.Документы.Чд_ДвижениеМатериаловВЭксплуатации) Тогда
		КомандаСоздатьНаОсновании = КомандыСозданияНаОсновании.Добавить();
		КомандаСоздатьНаОсновании.Менеджер = Метаданные.Документы.Чд_ДвижениеМатериаловВЭксплуатации.ПолноеИмя();
		КомандаСоздатьНаОсновании.Представление = ОбщегоНазначенияУТ.ПредставлениеОбъекта(Метаданные.Документы.Чд_ДвижениеМатериаловВЭксплуатации);
		КомандаСоздатьНаОсновании.РежимЗаписи = "Проводить";
		КомандаСоздатьНаОсновании.ФункциональныеОпции = "ИспользоватьВнутреннееПотребление";
		Возврат КомандаСоздатьНаОсновании;
	КонецЕсли;
	
	Возврат Неопределено;
КонецФункции

// Определяет список команд отчетов.
//
// Параметры:
//   КомандыОтчетов - ТаблицаЗначений - Таблица с командами отчетов. Для изменения.
//       См. описание 1 параметра процедуры ВариантыОтчетовПереопределяемый.ПередДобавлениемКомандОтчетов().
//   Параметры - Структура - Вспомогательные параметры. Для чтения.
//       См. описание 2 параметра процедуры ВариантыОтчетовПереопределяемый.ПередДобавлениемКомандОтчетов().
//
Процедура ДобавитьКомандыОтчетов(КомандыОтчетов, Параметры) Экспорт
	
	ВариантыОтчетовУТПереопределяемый.ДобавитьКомандуСтруктураПодчиненности(КомандыОтчетов);
	
	ВариантыОтчетовУТПереопределяемый.ДобавитьКомандуДвижениеМатериаловВЭксплуатации(КомандыОтчетов);
	
КонецПроцедуры

// 4D:Милавица, Михаил, 11.06.2019
// Печать ТТН документа Движение материалов в эксплуатации, №21248
// {
#Область ОснованиеДляПечати

// Возвращает таблицу значений по умолчанию для реквизита "Основание"
//
// Параметры:
//	Объект - ДанныеФормыСтруктура, ДокументОбъект.ВыкупВозвратнойТарыКлиентом - Объект документа,
//													по которому необходимо получить список выбора
//
// Возвращаемое значение:
//	ТаблицаОснований - Список вариантов текстов основания.
//
Функция ТаблицаОснованийДляПечати(Объект) Экспорт
	
	ТаблицаОснований = Новый ТаблицаЗначений;
	ТаблицаОснований.Колонки.Добавить("Основание", Новый ОписаниеТипов("Строка", , , , Новый КвалификаторыСтроки(300)));
	ТаблицаОснований.Колонки.Добавить("ОснованиеДата", Новый ОписаниеТипов("Дата", , , , , Новый КвалификаторыДаты(ЧастиДаты.Дата)));
	ТаблицаОснований.Колонки.Добавить("ОснованиеНомер", Новый ОписаниеТипов("Строка", , , , Новый КвалификаторыСтроки(128)));
	
	Если ЗначениеЗаполнено(Объект.ПодразделениеПолучатель) Тогда
		
		Запрос = Новый Запрос;
		Запрос.Текст =
			"ВЫБРАТЬ РАЗРЕШЕННЫЕ 
			|	ДоговорыКонтрагентов.НаименованиеДляПечати КАК Основание,
			|	ДоговорыКонтрагентов.Дата КАК ОснованиеДата,
			|	ДоговорыКонтрагентов.Номер КАК ОснованиеНомер
			|ИЗ
			|	Справочник.ДоговорыКонтрагентов КАК ДоговорыКонтрагентов
			|		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.СтруктураПредприятия КАК СтруктураПредприятия
			|		ПО (ДоговорыКонтрагентов.Партнер = (ВЫРАЗИТЬ(СтруктураПредприятия.Чд_ЮридическоеЛицо КАК Справочник.Партнеры)))
			|ГДЕ
			|	СтруктураПредприятия.Ссылка = &Ссылка
			|	И ДоговорыКонтрагентов.Контрагент = &Контрагент
			|	И ДоговорыКонтрагентов.Статус = &Статус
			|
			|УПОРЯДОЧИТЬ ПО
			|	ДоговорыКонтрагентов.Дата УБЫВ";
		
		Запрос.УстановитьПараметр("Ссылка", Объект.ПодразделениеПолучатель);
		Запрос.УстановитьПараметр("Контрагент", Объект.Грузополучатель);
		Запрос.УстановитьПараметр("ТекДата", ТекущаяДатаСеанса());
		Запрос.УстановитьПараметр("Статус", Перечисления.СтатусыДоговоровКонтрагентов.Действует);
		
		Выборка = Запрос.Выполнить().Выбрать();
		Пока Выборка.Следующий() Цикл
			Стр = ТаблицаОснований.Добавить();
			ЗаполнитьЗначенияСвойств(Стр, Выборка);
		КонецЦикла;
	КонецЕсли;
	
	Возврат ТаблицаОснований;
	
КонецФункции

#КонецОбласти

// Возвращает выборку контрагентов по подразделению
Функция ПолучитьКонтрагентов(Ссылка) Экспорт
	
	Запрос = Новый Запрос;
	Запрос.Текст =
		"ВЫБРАТЬ РАЗРЕШЕННЫЕ
		|	Контрагенты.Ссылка КАК Ссылка
		|ИЗ
		|	Справочник.СтруктураПредприятия КАК СтруктураПредприятия
		|		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.Контрагенты КАК Контрагенты
		|		ПО ((ВЫРАЗИТЬ(СтруктураПредприятия.Чд_ЮридическоеЛицо КАК Справочник.Партнеры)) = Контрагенты.Партнер)
		|ГДЕ
		|	СтруктураПредприятия.Ссылка = &Ссылка";
	
	Запрос.УстановитьПараметр("Ссылка", Ссылка);
	
	Возврат Запрос.Выполнить().Выбрать();
	
КонецФункции

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область Проведение

Функция ДополнительныеИсточникиДанныхДляДвижений(ИмяРегистра) Экспорт
	
	ИсточникиДанных = Новый Соответствие;
	Возврат ИсточникиДанных;
	
КонецФункции

Процедура ИнициализироватьДанныеДокумента(ДокументСсылка, ДополнительныеСвойства, Регистры = Неопределено) Экспорт 
	
	// 4D:Милавица, СветланаА, 08.09.2021
	// {
	// Добавление возможности испортить БСО и отображение таблицы с испорченными БСО
	Запрос = Новый Запрос;
	
	// Формирование текста запроса.
	ТекстыЗапроса = Новый СписокЗначений;
	
	// 4D: ERP для Беларуси 
	// Учет бланков строгой отчетности
	// {
	ТекстЗапросаТаблицаИспользованныеБСО(Запрос, ТекстыЗапроса, Регистры);
	// }
	// 4D

КонецПроцедуры

#КонецОбласти

#Область Печать

// Заполняет список команд печати.
//
// Параметры:
//   КомандыПечати - ТаблицаЗначений - состав полей см. в функции УправлениеПечатью.СоздатьКоллекциюКомандПечати.
//
 Процедура ДобавитьКомандыПечати(КомандыПечати) Экспорт
	
	// 4D:Милавица, Михаил, 11.06.2019
	// Печать ТТН документа Движение материалов в эксплуатации, БСО, №21248
	// {
	УправлениеПечатьюУПВызовСервера_Локализация.ДобавитьКомандыПечатиТН2(КомандыПечати);
	Документы.ТранспортнаяНакладная.ДобавитьКомандыПечати(КомандыПечати);
	
	КомандаПечати = КомандыПечати.Добавить();
	КомандаПечати.Идентификатор = "НакладнаяБСО";
	КомандаПечати.Представление = НСтр("ru = 'Приходно-расходная накладная БСО'");
	КомандаПечати.ПроверкаПроведенияПередПечатью = Истина;
	// }4D
	
	// 4D:Милавица, ВикторК, 17.03.2021
	// Задача № 28415 Печатная форма к документу "Движение материалов в эксплуатации" 
	КомандаПечати = КомандыПечати.Добавить();
	КомандаПечати.Идентификатор = "М11";
	КомандаПечати.МенеджерПечати = "Обработка.ПечатьМ11.Печать";
	КомандаПечати.Представление = НСтр("ru = 'М-11'");
	КомандаПечати.ПроверкаПроведенияПередПечатью = Истина; 
	// 
	// }4D
	
	// 4D:Милавица, ЕленаТ, 15.07.2021
	// Тестирование функционала конфигурации 2.4.11, Задача № 29781 
	
	//МассивДокументов = Новый Массив;
	//
	//МассивДокументов.Добавить("Перемещения");
	//МассивДокументов.Добавить("Передачи");
	//
	//ПорядокКоманд = 4;
	//Для каждого НаименованиеДокумента Из МассивДокументов Цикл
	//	                                                                        
	//	НайденнаяСтрока = КомандыПечати.Найти("ТТН_Вертикальная_" + НаименованиеДокумента, "Идентификатор");
	//	Если НайденнаяСтрока = Неопределено Тогда
	//		КомандаПечати = КомандыПечати.Добавить();
	//		КомандаПечати.Обработчик = "УправлениеПечатьюУПКлиент_Локализация.ПечатьТТН";
	//		КомандаПечати.Идентификатор = "ТТН_Вертикальная" + "_Для_" + НаименованиеДокумента;
	//		КомандаПечати.Представление = НСтр("ru = 'ТТН-1 (вертикальная)'") + " для " + НаименованиеДокумента;
	//		КомандаПечати.ПроверкаПроведенияПередПечатью = Истина;
	//		ПорядокКоманд = ПорядокКоманд + 1;
	//		КомандаПечати.Порядок = ПорядокКоманд;
	//	КонецЕсли;
	//	
	//	НайденнаяСтрока = КомандыПечати.Найти("ТТН_ВертикальнаяСПриложением_" + НаименованиеДокумента, "Идентификатор");
	//	Если НайденнаяСтрока = Неопределено Тогда
	//		КомандаПечати = КомандыПечати.Добавить();
	//		КомандаПечати.Обработчик = "УправлениеПечатьюУПКлиент_Локализация.ПечатьТТН";
	//		КомандаПечати.Идентификатор = "ТТН_ВертикальнаяСПриложением" + "_Для_" + НаименованиеДокумента;
	//		КомандаПечати.Представление = НСтр("ru = 'ТТН-1 (вертикальная) с приложением'") + " для " + НаименованиеДокумента;
	//		КомандаПечати.ПроверкаПроведенияПередПечатью = Истина;
	//		ПорядокКоманд = ПорядокКоманд + 1;
	//		КомандаПечати.Порядок = ПорядокКоманд;
	//	КонецЕсли;
	//	
	//	НайденнаяСтрока = КомандыПечати.Найти("ТТН_Горизонтальная_" + НаименованиеДокумента, "Идентификатор");
	//	Если НайденнаяСтрока = Неопределено Тогда
	//		КомандаПечати = КомандыПечати.Добавить();
	//		КомандаПечати.Обработчик = "УправлениеПечатьюУПКлиент_Локализация.ПечатьТТН";
	//		КомандаПечати.Идентификатор = "ТТН_Горизонтальная" + "_Для_" + НаименованиеДокумента;
	//		КомандаПечати.Представление = НСтр("ru = 'ТТН-1 (горизонтальная)'") + " для " + НаименованиеДокумента;
	//		КомандаПечати.ПроверкаПроведенияПередПечатью = Истина;
	//		ПорядокКоманд = ПорядокКоманд + 1;
	//		КомандаПечати.Порядок = ПорядокКоманд;
	//	КонецЕсли;
	//	
	//	НайденнаяСтрока = КомандыПечати.Найти("ТТН_ГоризонтальнаяСПриложением_" + НаименованиеДокумента, "Идентификатор");
	//	Если НайденнаяСтрока = Неопределено Тогда
	//		КомандаПечати = КомандыПечати.Добавить();
	//		КомандаПечати.Обработчик = "УправлениеПечатьюУПКлиент_Локализация.ПечатьТТН";
	//		КомандаПечати.Идентификатор = "ТТН_ГоризонтальнаяСПриложением" + "_Для_" + НаименованиеДокумента;
	//		КомандаПечати.Представление = НСтр("ru = 'ТТН-1 (горизонтальная) с приложением'") + " для " + НаименованиеДокумента;
	//		КомандаПечати.ПроверкаПроведенияПередПечатью = Истина;
	//		ПорядокКоманд = ПорядокКоманд + 1;
	//		КомандаПечати.Порядок = ПорядокКоманд;
	//	КонецЕсли;
	//	
	//	// 4D:Милавица, АнастасияМ, 25.02.2021 
	//	// Доработка. Печатная форма ТН-2 для документов ТМЦ в эксплуатации, №28335
	//	// { Требуется добавить печатную форму ТН-2 для документов
	//	НайденнаяСтрока = КомандыПечати.Найти("ТН_Вертикальная_" + НаименованиеДокумента, "Идентификатор");
	//	Если НайденнаяСтрока = Неопределено Тогда
	//		КомандаПечати = КомандыПечати.Добавить();
	//		КомандаПечати.Обработчик = "УправлениеПечатьюУПКлиент_Локализация.ПечатьТранспортнойНакладной";
	//		КомандаПечати.Идентификатор = "ТН_Вертикальная";
	//		КомандаПечати.Представление = НСтр("ru = 'ТН-2 (вертикальная)'") + " для " + НаименованиеДокумента;
	//		КомандаПечати.ПроверкаПроведенияПередПечатью = Истина;
	//		ПорядокКоманд = ПорядокКоманд + 1;
	//		КомандаПечати.Порядок = ПорядокКоманд;
	//	КонецЕсли;
	//	
	//	НайденнаяСтрока = КомандыПечати.Найти("ТН_ВертикальнаяСПриложением_" + НаименованиеДокумента, "Идентификатор");
	//	Если НайденнаяСтрока = Неопределено Тогда
	//		КомандаПечати = КомандыПечати.Добавить();
	//		КомандаПечати.Обработчик = "УправлениеПечатьюУПКлиент_Локализация.ПечатьТранспортнойНакладной";
	//		КомандаПечати.Идентификатор = "ТН_ВертикальнаяСПриложением";
	//		КомандаПечати.Представление = НСтр("ru = 'ТН-2 (вертикальная) с приложением'") + " для " + НаименованиеДокумента;
	//		КомандаПечати.ПроверкаПроведенияПередПечатью = Истина;
	//		ПорядокКоманд = ПорядокКоманд + 1;
	//		КомандаПечати.Порядок = ПорядокКоманд;
	//	КонецЕсли;
	//	
	//	НайденнаяСтрока = КомандыПечати.Найти("ТН_Горизонтальная_" + НаименованиеДокумента, "Идентификатор");
	//	Если НайденнаяСтрока = Неопределено Тогда
	//		КомандаПечати = КомандыПечати.Добавить();
	//		КомандаПечати.Обработчик = "УправлениеПечатьюУПКлиент_Локализация.ПечатьТранспортнойНакладной";
	//		КомандаПечати.Идентификатор = "ТН_Горизонтальная";
	//		КомандаПечати.Представление = НСтр("ru = 'ТН-2 (горизонтальная)'") + " для " + НаименованиеДокумента;
	//		КомандаПечати.ПроверкаПроведенияПередПечатью = Истина;
	//		ПорядокКоманд = ПорядокКоманд + 1;
	//		КомандаПечати.Порядок = ПорядокКоманд;
	//	КонецЕсли;
	//	
	//	НайденнаяСтрока = КомандыПечати.Найти("ТН_ГоризонтальнаяСПриложением_" + НаименованиеДокумента, "Идентификатор");
	//	Если НайденнаяСтрока = Неопределено Тогда
	//		КомандаПечати = КомандыПечати.Добавить();
	//		КомандаПечати.Обработчик = "УправлениеПечатьюУПКлиент_Локализация.ПечатьТранспортнойНакладной";
	//		КомандаПечати.Идентификатор = "ТН_ГоризонтальнаяСПриложением";
	//		КомандаПечати.Представление = НСтр("ru = 'ТН-2 (горизонтальная) с приложением'") + " для " + НаименованиеДокумента;
	//		КомандаПечати.ПроверкаПроведенияПередПечатью = Истина;
	//		ПорядокКоманд = ПорядокКоманд + 1;
	//		КомандаПечати.Порядок = ПорядокКоманд;
	//	КонецЕсли;
	//	// }4D
	//КонецЦикла;
	
КонецПроцедуры

// 4D:Милавица, Михаил, 10.06.2019
// Печать ТТН документа Движение материалов в эксплуатации, БСО, №21248
// {
// Формирует временную таблицу, содержащую табличную часть по таблице данных документов.
//
// Параметры:
//	МенеджерВременныхТаблиц - МенеджерВременныхТаблиц - Менеджер временных таблиц, содержащий таблицу ТаблицаДанныхДокументов с полями:
//		Ссылка,
//		Валюта.
//
//	ПараметрыЗаполнения - Структура - структура, возвращаемая функцией ПродажиСервер.ПараметрыЗаполненияВременнойТаблицыТоваров.
//
Процедура ПоместитьВременнуюТаблицуТоваров(МенеджерВременныхТаблиц, ПараметрыЗаполнения = Неопределено) Экспорт
	
	Запрос = Новый Запрос;
	Запрос.МенеджерВременныхТаблиц = МенеджерВременныхТаблиц;
	
	Запрос.Текст =
		"ВЫБРАТЬ
		|	ТаблицаДокумента.Ссылка КАК Ссылка,
		|	ТаблицаДокумента.НомерСтроки КАК НомерСтроки,
		|	ТаблицаДокумента.Номенклатура КАК Номенклатура,
		|	ТаблицаДокумента.Характеристика КАК Характеристика,
		|	ВЫБОР
		|		КОГДА ТаблицаДокумента.Количество > 0
		|			ТОГДА ТаблицаДокумента.Количество
		|		КОГДА ТаблицаДокумента.КоличествоНаСкладе > 0
		|			ТОГДА ТаблицаДокумента.КоличествоНаСкладе
		|		ИНАЧЕ 0
		|	КОНЕЦ КАК Количество,
		|	ВЫБОР
		|		КОГДА ТаблицаДокумента.Количество > 0
		|			ТОГДА ТаблицаДокумента.Количество
		|		КОГДА ТаблицаДокумента.КоличествоНаСкладе > 0
		|			ТОГДА ТаблицаДокумента.КоличествоНаСкладе
		|		ИНАЧЕ 0
		|	КОНЕЦ КАК КоличествоУпаковок,
		|	0 КАК СуммаБезНДС,
		|	0 КАК СтавкаНДС,
		|	0 КАК СуммаНДС,
		|	ЛОЖЬ КАК ЭтоТовар,
		|	ЛОЖЬ КАК ЭтоНеВозвратнаяТара,
		|	ЗНАЧЕНИЕ(Справочник.УпаковкиЕдиницыИзмерения.ПустаяСсылка) КАК Упаковка,
		|	ВЫБОР
		|		КОГДА ТаблицаДокумента.Количество > 0
		|			ТОГДА ""02""
		|		КОГДА ТаблицаДокумента.КоличествоНаСкладе > 0
		|			ТОГДА ""01""
		|		ИНАЧЕ 0
		|	КОНЕЦ КАК Примечание
		|ПОМЕСТИТЬ ДвижениеМатериаловВЭксплуатацииТаблицаТоваров
		|ИЗ
		|	Документ.Чд_ДвижениеМатериаловВЭксплуатации.Товары КАК ТаблицаДокумента
		|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ ТаблицаДанныхДокументов КАК ДанныеДокументов
		|		ПО ТаблицаДокумента.Ссылка = ДанныеДокументов.Ссылка
		|ГДЕ
		|	ТаблицаДокумента.Номенклатура.ТипНоменклатуры В (ЗНАЧЕНИЕ(Перечисление.ТипыНоменклатуры.Услуга), ЗНАЧЕНИЕ(Перечисление.ТипыНоменклатуры.Работа))
		|
		|ОБЪЕДИНИТЬ ВСЕ
		|
		|ВЫБРАТЬ
		|	ТаблицаТоваров.Ссылка,
		|	ТаблицаТоваров.НомерСтроки,
		|	ТаблицаТоваров.Номенклатура,
		|	ТаблицаТоваров.Характеристика,
		|	ВЫБОР
		|		КОГДА ТаблицаТоваров.Количество > 0
		|			ТОГДА ТаблицаТоваров.Количество
		|		КОГДА ТаблицаТоваров.КоличествоНаСкладе > 0
		|			ТОГДА ТаблицаТоваров.КоличествоНаСкладе
		|		ИНАЧЕ 0
		|	КОНЕЦ,
		|	ВЫБОР
		|		КОГДА ТаблицаТоваров.Количество > 0
		|			ТОГДА ТаблицаТоваров.Количество
		|		КОГДА ТаблицаТоваров.КоличествоНаСкладе > 0
		|			ТОГДА ТаблицаТоваров.КоличествоНаСкладе
		|		ИНАЧЕ 1
		|	КОНЕЦ,
		|	0,
		|	0,
		|	0,
		|	ИСТИНА,
		|	ВЫБОР
		|		КОГДА ТаблицаТоваров.Номенклатура.ТипНоменклатуры = ЗНАЧЕНИЕ(Перечисление.ТипыНоменклатуры.МногооборотнаяТара)
		|			ТОГДА ЛОЖЬ
		|		ИНАЧЕ ИСТИНА
		|	КОНЕЦ,
		|	ЗНАЧЕНИЕ(Справочник.УпаковкиЕдиницыИзмерения.ПустаяСсылка),
		|	ВЫБОР
		|		КОГДА ТаблицаТоваров.Количество > 0
		|			ТОГДА ""02""
		|		КОГДА ТаблицаТоваров.КоличествоНаСкладе > 0
		|			ТОГДА ""01""
		|		ИНАЧЕ 0
		|	КОНЕЦ
		|ИЗ
		|	Документ.Чд_ДвижениеМатериаловВЭксплуатации.Товары КАК ТаблицаТоваров
		|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ ТаблицаДанныхДокументов КАК ДанныеДокументов
		|		ПО ТаблицаТоваров.Ссылка = ДанныеДокументов.Ссылка
		|ГДЕ
		|	НЕ ТаблицаТоваров.Номенклатура.ТипНоменклатуры В (ЗНАЧЕНИЕ(Перечисление.ТипыНоменклатуры.Услуга), ЗНАЧЕНИЕ(Перечисление.ТипыНоменклатуры.Работа))
		|
		|ИНДЕКСИРОВАТЬ ПО
		|	Ссылка,
		|	НомерСтроки";
	
	Запрос.Выполнить();
	
КонецПроцедуры

Процедура Печать(МассивОбъектов, ПараметрыПечати, КоллекцияПечатныхФорм, ОбъектыПечати, ПараметрыВывода) Экспорт
	
	Если УправлениеПечатью.НужноПечататьМакет(КоллекцияПечатныхФорм, "НакладнаяБСО") Тогда
		УправлениеПечатью.ВывестиТабличныйДокументВКоллекцию(КоллекцияПечатныхФорм, "НакладнаяБСО",
				НСтр("ru = 'Приходно-расходная накладная БСО'"),
				СформироватьПечатнуюФормуНакладнаяБСО(МассивОбъектов, ОбъектыПечати, Неопределено));
	КонецЕсли;
	
КонецПроцедуры

Функция СформироватьПечатнуюФормуНакладнаяБСО(МассивОбъектов, ОбъектыПечати, КомплектыПечати)
	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("МассивОбъектов", МассивОбъектов);
	Запрос.Текст =
		"ВЫБРАТЬ
		|	ПеремещениеТоваров.Ссылка КАК Ссылка,
		|	ПеремещениеТоваров.Дата КАК Дата,
		|	ПеремещениеТоваров.Номер КАК Номер,
		|	ПеремещениеТоваров.Организация КАК Организация,
		|	ПРЕДСТАВЛЕНИЕ(ПеремещениеТоваров.Организация.НаименованиеПолное) КАК ОрганизацияНаименованиеПолное,
		|	ПеремещениеТоваров.Организация КАК ОрганизацияПолучатель,
		|	ПРЕДСТАВЛЕНИЕ(ПеремещениеТоваров.Организация.НаименованиеПолное) КАК ОрганизацияПолучательНаименованиеПолное,
		|	НЕОПРЕДЕЛЕНО КАК СкладПолучатель,
		|	НЕОПРЕДЕЛЕНО КАК СкладПолучательПодразделение,
		|	НЕОПРЕДЕЛЕНО КАК СкладПолучательДолжность,
		|	НЕОПРЕДЕЛЕНО КАК СкладПолучательОтветственный,
		|	НЕОПРЕДЕЛЕНО КАК СкладОтправитель,
		|	НЕОПРЕДЕЛЕНО КАК СкладОтправительПодразделение,
		|	НЕОПРЕДЕЛЕНО КАК СкладОтправительДолжность,
		|	НЕОПРЕДЕЛЕНО КАК СкладОтправительОтветственный
		|ИЗ
		|	Документ.Чд_ДвижениеМатериаловВЭксплуатации КАК ПеремещениеТоваров
		|ГДЕ
		|	ПеремещениеТоваров.Ссылка В(&МассивОбъектов)
		|
		|УПОРЯДОЧИТЬ ПО
		|	Ссылка
		|АВТОУПОРЯДОЧИВАНИЕ
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	ДвижениеМатериаловВЭксплуатацииТовары.Ссылка КАК Ссылка,
		|	ДвижениеМатериаловВЭксплуатацииТовары.Номенклатура.НаименованиеПолное КАК Номенклатура,
		|	1 КАК КоличествоКниг,
		|	ДвижениеМатериаловВЭксплуатацииТовары.Количество КАК Количество,
		|	ДвижениеМатериаловВЭксплуатацииТовары.СерияБланкаСтрогойОтчетности КАК Серия,
		|	ДвижениеМатериаловВЭксплуатацииТовары.НачальныйНомерБланкаСтрогойОтчетности КАК НачальныйНомер,
		|	ДвижениеМатериаловВЭксплуатацииТовары.КонечныйНомерБланкаСтрогойОтчетности КАК КонечныйНомер
		|ИЗ
		|	Документ.Чд_ДвижениеМатериаловВЭксплуатации.Товары КАК ДвижениеМатериаловВЭксплуатацииТовары
		|ГДЕ
		|	ДвижениеМатериаловВЭксплуатацииТовары.Ссылка В(&МассивОбъектов)
		|
		|УПОРЯДОЧИТЬ ПО
		|	Ссылка,
		|	ДвижениеМатериаловВЭксплуатацииТовары.НомерСтроки
		|АВТОУПОРЯДОЧИВАНИЕ";
	
	Результат = Запрос.ВыполнитьПакет();
	Если Результат[0].Пустой() Или Результат[1].Пустой() Тогда
		Возврат Неопределено;
	КонецЕсли;
	
	ТабДокумент = Новый ТабличныйДокумент;
	ТабДокумент.ИмяПараметровПечати = "ПАРАМЕТРЫ_ПЕЧАТИ_ДвижениеМатериаловВЭксплуатации_НакладнаяБСО";
	ТабДокумент.АвтоМасштаб = Истина;
	ТабДокумент.ОриентацияСтраницы = ОриентацияСтраницы.Портрет;
	ТабДокумент.ПолеСверху = 5;
	ТабДокумент.ПолеСлева = 10;
	ТабДокумент.ПолеСнизу = 5;
	ТабДокумент.ПолеСправа = 5;
	
	Макет = УправлениеПечатью.МакетПечатнойФормы("Документ.Чд_ДвижениеМатериаловВЭксплуатации.ПФ_MXL_НакладнаяБСО");
	
	ПервыйДокумент = Истина;
	
	Шапка = Результат[0].Выбрать();
	ВыборкаНоменклатура = Результат[1].Выбрать();
	
	Пока Шапка.Следующий() Цикл
		Если Не ПервыйДокумент Тогда
			ТабДокумент.ВывестиГоризонтальныйРазделительСтраниц();
		КонецЕсли;
		
		ПервыйДокумент = Ложь;
		НомерСтрокиНачало = ТабДокумент.ВысотаТаблицы + 1;
		
		ОбластьМакетаШапка = Макет.ПолучитьОбласть("Шапка");
		ОбластьМакетаСтрока = Макет.ПолучитьОбласть("Строка");
		ОбластьМакетаПодвал = Макет.ПолучитьОбласть("Подвал");
		
		ОбластьМакетаШапка.Параметры.ФиоПолучил = ФизическиеЛицаУТ.ФамилияИнициалыФизЛица(Шапка.СкладПолучательОтветственный);
		
		ОбластьМакетаШапка.Параметры.ОрганизацияОтправитель = Шапка.ОрганизацияНаименованиеПолное + ", " + Шапка.СкладОтправительПодразделение;
		Если ЗначениеЗаполнено(Шапка.ОрганизацияПолучательНаименованиеПолное) Тогда
			ОбластьМакетаШапка.Параметры.ОрганизацияПолучатель = Шапка.ОрганизацияПолучательНаименованиеПолное + ", " + Шапка.СкладПолучательПодразделение;
		Иначе
			ОбластьМакетаШапка.Параметры.ОрганизацияПолучатель = Шапка.ОрганизацияНаименованиеПолное + ", " + Шапка.СкладПолучательПодразделение;
		КонецЕсли;
		
		ОбластьМакетаШапка.Параметры.НомерДок = ПрефиксацияОбъектовКлиентСервер.ПолучитьНомерНаПечать(Шапка.Номер, Истина, Истина);
		ОбластьМакетаШапка.Параметры.ДатаДок = Шапка.Дата;
		ТабДокумент.Вывести(ОбластьМакетаШапка);
		
		НомерСтроки = 1;
		
		ВыборкаНоменклатура.Сбросить();
		СтруктураПоиска = Новый Структура("Ссылка", Шапка.Ссылка);
		Пока ВыборкаНоменклатура.НайтиСледующий(СтруктураПоиска) Цикл
			ОбластьМакетаСтрока.Параметры.Заполнить(ВыборкаНоменклатура);
			ОбластьМакетаСтрока.Параметры.Серия = СокрЛП(ВыборкаНоменклатура.Серия);
			ОбластьМакетаСтрока.Параметры.НомерСтроки = НомерСтроки;
			ТабДокумент.Вывести(ОбластьМакетаСтрока);
			
			НомерСтроки = НомерСтроки + 1;
		КонецЦикла;
		
		ОбластьМакетаПодвал.Параметры.ОтправительМОЛДолжность = Шапка.СкладОтправительДолжность;
		ОбластьМакетаПодвал.Параметры.ОтправительМОЛ = ФизическиеЛицаУТ.ФамилияИнициалыФизЛица(Шапка.СкладОтправительОтветственный);
		
		ОбластьМакетаПодвал.Параметры.ПолучательМОЛДолжность = Шапка.СкладПолучательДолжность;
		ОбластьМакетаПодвал.Параметры.ПолучательМОЛ = ФизическиеЛицаУТ.ФамилияИнициалыФизЛица(Шапка.СкладПолучательОтветственный);
		
		ОтветственныеЛица = ОтветственныеЛицаБП.ОтветственныеЛица(Шапка.Организация, Шапка.Дата);
		ОбластьМакетаПодвал.Параметры.ГлБухгалтерДолжность = ОтветственныеЛица.ГлавныйБухгалтерДолжностьПредставление;
		ОбластьМакетаПодвал.Параметры.ГлБухгалтерМОЛ = ОтветственныеЛица.ГлавныйБухгалтерПредставление;
		ТабДокумент.Вывести(ОбластьМакетаПодвал);
		
		УправлениеПечатью.ЗадатьОбластьПечатиДокумента(ТабДокумент, НомерСтрокиНачало, ОбъектыПечати, Шапка.Ссылка);
	КонецЦикла;
	
	Возврат ТабДокумент;
КонецФункции
// }4D

// 4D:Милавица, АнастасияМ, 27.04.2021 
// Доработка. Печатная форма ТН-2 для документов ТМЦ в эксплуатации, №28335
// {
Функция ПолучитьДанныеДляПечатнойФормыТОРГ12(ПараметрыПечати, МассивОбъектов) Экспорт
	МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
	
	Запрос = Новый Запрос;
	Запрос.МенеджерВременныхТаблиц = МенеджерВременныхТаблиц;
	Запрос.Текст =
		"ВЫБРАТЬ
		|	ДанныеДокументов.Ссылка КАК Ссылка,
		|	&ВалютаРеглУчета КАК Валюта
		|ПОМЕСТИТЬ ТаблицаДанныхДокументов
		|ИЗ
		|	Документ.Чд_ДвижениеМатериаловВЭксплуатации КАК ДанныеДокументов
		|ГДЕ
		|	ДанныеДокументов.Ссылка В(&МассивОбъектов)
		|
		|ИНДЕКСИРОВАТЬ ПО
		|	Ссылка";
	Запрос.УстановитьПараметр("МассивОбъектов", МассивОбъектов);
	Запрос.УстановитьПараметр("ВалютаРеглУчета", Константы.ВалютаРегламентированногоУчета.Получить());
	Запрос.Выполнить();
	
	ПоместитьВременнуюТаблицуТоваров(МенеджерВременныхТаблиц);
	ОтветственныеЛицаСервер.СформироватьВременнуюТаблицуОтветственныхЛицДокументов(МассивОбъектов, МенеджерВременныхТаблиц);
	
	Запрос.Текст =
		"ВЫБРАТЬ
		|	ДвижениеМатериаловВЭксплуатации.Ссылка КАК Ссылка,
		|	ДвижениеМатериаловВЭксплуатации.НомерБланкаСтрогойОтчетности КАК Номер,
		|	ДвижениеМатериаловВЭксплуатации.Дата КАК Дата,
		|	ДвижениеМатериаловВЭксплуатации.НомерБланкаСтрогойОтчетности КАК НомерБланкаСтрогойОтчетности,
		|	ДвижениеМатериаловВЭксплуатации.СерияБланкаСтрогойОтчетности КАК СерияБланкаСтрогойОтчетности,
		|	ДвижениеМатериаловВЭксплуатации.Отпустил КАК Отпустил,
		|	ДвижениеМатериаловВЭксплуатации.ОтпустилДолжность КАК ОтпустилДолжность,
		|	ДвижениеМатериаловВЭксплуатации.Руководитель.Наименование КАК Разрешил,
		|	ДвижениеМатериаловВЭксплуатации.Руководитель.Должность КАК РазрешилДолжность,
		|	ДвижениеМатериаловВЭксплуатации.Принял КАК Принял,
		|	ДвижениеМатериаловВЭксплуатации.ПереданыДокументы КАК ПереданыДокументы,
		|	ДвижениеМатериаловВЭксплуатации.Организация КАК Организация,
		|	ДвижениеМатериаловВЭксплуатации.Грузополучатель КАК Грузополучатель,
		|	ДвижениеМатериаловВЭксплуатации.Организация КАК Грузоотправитель,
		|	ТаблицаОтветственныеЛица.РуководительНаименование КАК Руководитель,
		|	ТаблицаОтветственныеЛица.РуководительДолжность КАК ДолжностьРуководителя,
		|	ТаблицаОтветственныеЛица.ГлавныйБухгалтерНаименование КАК ГлавныйБухгалтер,
		|	ДвижениеМатериаловВЭксплуатации.Организация.Префикс КАК Префикс,
		|	ДвижениеМатериаловВЭксплуатации.Основание КАК Основание,
		|	ДвижениеМатериаловВЭксплуатации.ОснованиеДата КАК ОснованиеДата,
		|	ДвижениеМатериаловВЭксплуатации.ОснованиеНомер КАК ОснованиеНомер,
		|	ДвижениеМатериаловВЭксплуатации.АдресДоставки КАК АдресДоставки,
		|	&ВалютаРеглУчета КАК Валюта,
		|	ДвижениеМатериаловВЭксплуатации.ДоверенностьНомер КАК ДоверенностьНомер,
		|	ДвижениеМатериаловВЭксплуатации.ДоверенностьДата КАК ДоверенностьДата,
		|	ДвижениеМатериаловВЭксплуатации.ДоверенностьВыдана КАК ДоверенностьВыдана,
		|	ДвижениеМатериаловВЭксплуатации.ДоверенностьЛицо КАК ДоверенностьЛицо,
		|	&ЕдиницаИзмеренияВеса КАК ЕдиницаИзмеренияВеса,
		|	ДвижениеМатериаловВЭксплуатации.Подразделение КАК СкладОтправитель,
		|	ДвижениеМатериаловВЭксплуатации.ПодразделениеПолучатель КАК СкладПолучатель
		|ИЗ
		|	Документ.Чд_ДвижениеМатериаловВЭксплуатации КАК ДвижениеМатериаловВЭксплуатации
		|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ ТаблицаДанныхДокументов КАК ДанныеДокументов
		|		ПО ДвижениеМатериаловВЭксплуатации.Ссылка = ДанныеДокументов.Ссылка
		|		ЛЕВОЕ СОЕДИНЕНИЕ ТаблицаОтветственныеЛица КАК ТаблицаОтветственныеЛица
		|		ПО ДвижениеМатериаловВЭксплуатации.Ссылка = ТаблицаОтветственныеЛица.Ссылка
		|
		|УПОРЯДОЧИТЬ ПО
		|	Ссылка
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	ДвижениеМатериаловВЭксплуатацииТаблицаТоваров.Ссылка КАК Ссылка,
		|	ДвижениеМатериаловВЭксплуатацииТаблицаТоваров.Номенклатура КАК Номенклатура,
		|	ДвижениеМатериаловВЭксплуатацииТаблицаТоваров.Характеристика КАК Характеристика,
		|	ЦеныНоменклатуры.ВидЦены КАК ВидЦены,
		|	ЦеныНоменклатуры.Упаковка КАК Упаковка,
		|	МАКСИМУМ(ЦеныНоменклатуры.Период) КАК Период
		|ПОМЕСТИТЬ ТаблицаПериодовЦен
		|ИЗ
		|	ДвижениеМатериаловВЭксплуатацииТаблицаТоваров КАК ДвижениеМатериаловВЭксплуатацииТаблицаТоваров
		|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ РегистрСведений.ЦеныНоменклатуры КАК ЦеныНоменклатуры
		|		ПО ДвижениеМатериаловВЭксплуатацииТаблицаТоваров.Ссылка.ВидЦены = ЦеныНоменклатуры.ВидЦены
		|			И ДвижениеМатериаловВЭксплуатацииТаблицаТоваров.Номенклатура = ЦеныНоменклатуры.Номенклатура
		|			И ДвижениеМатериаловВЭксплуатацииТаблицаТоваров.Характеристика = ЦеныНоменклатуры.Характеристика
		|			И ДвижениеМатериаловВЭксплуатацииТаблицаТоваров.Ссылка.Дата >= ЦеныНоменклатуры.Период
		|
		|СГРУППИРОВАТЬ ПО
		|	ДвижениеМатериаловВЭксплуатацииТаблицаТоваров.Ссылка,
		|	ДвижениеМатериаловВЭксплуатацииТаблицаТоваров.Номенклатура,
		|	ДвижениеМатериаловВЭксплуатацииТаблицаТоваров.Характеристика,
		|	ЦеныНоменклатуры.ВидЦены,
		|	ЦеныНоменклатуры.Упаковка
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	ДвижениеМатериаловВЭксплуатации.Ссылка КАК Ссылка,
		|	ДвижениеМатериаловВЭксплуатации.Номенклатура КАК Номенклатура,
		|	ДвижениеМатериаловВЭксплуатации.Характеристика.НаименованиеПолное КАК Характеристика,
		|	""шт."" КАК ЕдиницаИзмеренияНаименование,
		|	ДвижениеМатериаловВЭксплуатации.КоличествоУпаковок КАК Количество,
		|	ЗНАЧЕНИЕ(Перечисление.СтавкиНДС.БезНДС) КАК СтавкаНДС,
		|	ВЫРАЗИТЬ(ЕСТЬNULL(ЦеныНоменклатуры.Цена, 0) КАК ЧИСЛО(15, 2)) КАК Цена,
		|	ЕСТЬNULL(ЦеныНоменклатуры.Цена, 0)  КАК СуммаБезНДС,
		|	ЕСТЬNULL(ЦеныНоменклатуры.Цена, 0) КАК СуммаСНДС,
		|	ДвижениеМатериаловВЭксплуатации.Примечание КАК Примечание,
		|	0 КАК СуммаНДС
		|ИЗ
		|	ДвижениеМатериаловВЭксплуатацииТаблицаТоваров КАК ДвижениеМатериаловВЭксплуатации
		|		ЛЕВОЕ СОЕДИНЕНИЕ ТаблицаПериодовЦен КАК ТаблицаПериодовЦен
		|			ВНУТРЕННЕЕ СОЕДИНЕНИЕ РегистрСведений.ЦеныНоменклатуры КАК ЦеныНоменклатуры
		|			ПО ТаблицаПериодовЦен.ВидЦены = ЦеныНоменклатуры.ВидЦены
		|				И ТаблицаПериодовЦен.Номенклатура = ЦеныНоменклатуры.Номенклатура
		|				И ТаблицаПериодовЦен.Характеристика = ЦеныНоменклатуры.Характеристика
		|				И ТаблицаПериодовЦен.Период = ЦеныНоменклатуры.Период
		|		ПО ДвижениеМатериаловВЭксплуатации.Ссылка = ТаблицаПериодовЦен.Ссылка
		|			И ДвижениеМатериаловВЭксплуатации.Номенклатура = ТаблицаПериодовЦен.Номенклатура
		|			И ДвижениеМатериаловВЭксплуатации.Характеристика = ТаблицаПериодовЦен.Характеристика
		|
		|УПОРЯДОЧИТЬ ПО
		|	ДвижениеМатериаловВЭксплуатации.Ссылка,
		|	ДвижениеМатериаловВЭксплуатации.НомерСтроки
		|ИТОГИ ПО
		|	Ссылка";
	
	Запрос.УстановитьПараметр("ВыводитьБазовыеЕдиницыИзмерения", Константы.ВыводитьБазовыеЕдиницыИзмерения.Получить());
	Запрос.УстановитьПараметр("ЕдиницаИзмеренияВеса", Константы.ЕдиницаИзмеренияВеса.Получить());
	
	РезультатПакетаЗапросов = Запрос.ВыполнитьПакет();
	РезультатПоШапке = РезультатПакетаЗапросов[0];
	РезультатПоТабличнойЧасти = РезультатПакетаЗапросов[2];
	СтруктураДанныхДляПечати = Новый Структура("РезультатПоШапке, РезультатПоТабличнойЧасти", РезультатПоШапке, РезультатПоТабличнойЧасти);
	
	Возврат СтруктураДанныхДляПечати;
	
КонецФункции
// }4D

// 4D:Милавица, ВикторК, 29.03.2021
// Задача № 28415 Печатная форма к документу "Движение материалов в эксплуатации" 
Функция ПолучитьДанныеДляПечатнойФормыМ11(ПараметрыПечати, МассивДокументов) Экспорт
	
	Запрос = Новый Запрос(
	"ВЫБРАТЬ
	|	МАКСИМУМ(Товары.ФизическоеЛицо) КАК Отправитель,
	|	Товары.Ссылка КАК Ссылка
	|ПОМЕСТИТЬ ФизическиеЛица
	|ИЗ
	|	Документ.Чд_ДвижениеМатериаловВЭксплуатации.Товары КАК Товары
	|ГДЕ
	|	Товары.Ссылка В(&МассивДокументов)
	|
	|СГРУППИРОВАТЬ ПО
	|	Товары.Ссылка
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	КОНЕЦПЕРИОДА(Товары.Ссылка.Дата, ДЕНЬ) КАК ДатаПолученияЦены,
	|	Товары.Ссылка КАК Ссылка,
	|	Товары.НомерСтроки КАК НомерСтроки,
	|	Товары.Номенклатура КАК Номенклатура,
	|	Товары.Характеристика КАК Характеристика,
	|	Товары.Номенклатура.ЕдиницаИзмерения КАК Упаковка,
	|	Товары.Количество + Товары.КоличествоНаСкладе КАК КоличествоУпаковок,
	|	Товары.ФизическоеЛицоПолучатель КАК Получатель,
	|	ВЫБОР
	|		КОГДА Товары.ФизическоеЛицо = ЗНАЧЕНИЕ(Справочник.ФизическиеЛица.ПустаяСсылка)
	|			ТОГДА ФизическиеЛица.Отправитель
	|		ИНАЧЕ Товары.ФизическоеЛицо
	|	КОНЕЦ КАК Отправитель
	|ПОМЕСТИТЬ Товары
	|ИЗ
	|	Документ.Чд_ДвижениеМатериаловВЭксплуатации.Товары КАК Товары
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ ФизическиеЛица КАК ФизическиеЛица
	|		ПО Товары.Ссылка = ФизическиеЛица.Ссылка
	|ГДЕ
	|	Товары.Ссылка В(&МассивДокументов)
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	Документы.Ссылка КАК Ссылка,
	|	Документы.Номер КАК Номер,
	|	Документы.Дата КАК ДатаДокумента,
	|	Документы.Дата КАК ДатаСоставления,
	|	Документы.Организация КАК Организация,
	|	Документы.Организация.Префикс КАК Префикс,
	|	Документы.ПодразделениеПолучатель КАК Подразделение,
	|	Документы.Подразделение КАК ПодразделениеОтправитель,
	|	НЕОПРЕДЕЛЕНО КАК УчетныйВидЦены,
	|	""01"" КАК Код,
	|	НЕОПРЕДЕЛЕНО КАК УчетныйВидЦеныВалютаЦены
	|ИЗ
	|	Документ.Чд_ДвижениеМатериаловВЭксплуатации КАК Документы
	|ГДЕ
	|	Документы.Ссылка В(&МассивДокументов)
	|
	|УПОРЯДОЧИТЬ ПО
	|	ДатаДокумента,
	|	Ссылка
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	Товары.Ссылка КАК Ссылка,
	|	Товары.Ссылка.Подразделение.Чд_Кладовая КАК Склад,
	|	Товары.Отправитель КАК КладовщикОтправитель,
	|	НЕОПРЕДЕЛЕНО КАК ДолжностьКладовщикаОтправителя,
	|	Товары.Номенклатура КАК Номенклатура,
	|	Товары.Номенклатура.НаименованиеПолное КАК НоменклатураНаименование,
	|	Товары.Номенклатура.Код КАК НоменклатурныйНомерКод,
	|	Товары.Номенклатура.Артикул КАК НоменклатурныйНомерАртикул,
	|	Товары.Получатель КАК Получатель,
	|	Товары.Характеристика.НаименованиеПолное КАК Характеристика,
	|	ВЫБОР
	|		КОГДА ЕСТЬNULL(&ТекстЗапросаКоэффициентУпаковки1, 1) = 1
	|			ТОГДА НЕОПРЕДЕЛЕНО
	|		ИНАЧЕ Товары.Упаковка.Наименование
	|	КОНЕЦ КАК Упаковка,
	|	&ТекстЗапросаНаименованиеЕдиницыИзмерения КАК ЕдиницаИзмеренияНаименование,
	|	&ТекстЗапросаКодЕдиницыИзмерения КАК ЕдиницаИзмеренияКод,
	|	Товары.КоличествоУпаковок КАК Количество,
	|	0 КАК Сумма,
	|	0 КАК Цена,
	|	Товары.НомерСтроки КАК НомерСтроки
	|ИЗ
	|	Товары КАК Товары
	|
	|УПОРЯДОЧИТЬ ПО
	|	Ссылка,
	|	НомерСтроки
	|ИТОГИ
	|	МАКСИМУМ(Получатель),
	|	СУММА(Количество)
	|ПО
	|	Ссылка,
	|	КладовщикОтправитель");
	
	Запрос.Текст = СтрЗаменить(Запрос.Текст, "&ТекстЗапросаКоэффициентУпаковки1",
		Справочники.УпаковкиЕдиницыИзмерения.ТекстЗапросаКоэффициентаУпаковки(
			"Товары.Упаковка",
			"Товары.Номенклатура"));
	Запрос.Текст = СтрЗаменить(Запрос.Текст, "&ТекстЗапросаНаименованиеЕдиницыИзмерения",
		Справочники.УпаковкиЕдиницыИзмерения.ТекстЗапросаЗначениеРеквизитаЕдиницыИзмерения(
			"Наименование",
			"Товары.Упаковка",
			"Товары.Номенклатура"));
	Запрос.Текст = СтрЗаменить(Запрос.Текст, "&ТекстЗапросаКодЕдиницыИзмерения",
		Справочники.УпаковкиЕдиницыИзмерения.ТекстЗапросаЗначениеРеквизитаЕдиницыИзмерения(
			"Код",
			"Товары.Упаковка",
			"Товары.Номенклатура"));
			
	Запрос.УстановитьПараметр("МассивДокументов", МассивДокументов);
	
	РезультатПакетаЗапросов = Запрос.ВыполнитьПакет();
	// Пакет запросов:
	// 		[0] - Временная таблица по табличной части документа
	// 		[1] - Выборка по шапкам документов
	// 		[2] - Выборка по табличным частям документов.
	
	Возврат Новый Структура(
		"РезультатПоШапке, РезультатПоТабличнойЧасти",
		РезультатПакетаЗапросов[2],
		РезультатПакетаЗапросов[3]);

КонецФункции
// }4D

#КонецОбласти

#КонецОбласти

#КонецЕсли

#Область Локализация

// 4D:Милавица, СветланаА, 08.09.2021
	// {
	// Добавление возможности испортить БСО и отображение таблицы с испорченными БСО
	// 4D:ERP для Беларуси
	// Учет бланков строгой отчетности
	// {
Функция ТекстЗапросаТаблицаИспользованныеБСО(Запрос, ТекстыЗапроса, Регистры)
	
	ИмяРегистра = "ИспользованныеБСО";
	
	Если НЕ ПроведениеСерверУТ.ТребуетсяТаблицаДляДвижений(ИмяРегистра, Регистры) Тогда
		Возврат "";
	КонецЕсли; 	

	ТекстЗапроса = 
	"ВЫБРАТЬ
	|	&СерияПервичногоДокумента КАК СерияБланкаСтрогойОтчетности,
	|	&НомерПервичногоДокумента КАК НомерБланкаСтрогойОтчетности,
	|	&ТипБланкаСтрогойОтчетности КАК ТипБланкаСтрогойОтчетности,
	|	&Ссылка КАК ДокументДвижения,
	|	&Период КАК ДатаЗаписи,
	|	ЗНАЧЕНИЕ(Перечисление.ВидыСписанияБСО.Использованы) КАК ТипСписания
	|ИЗ
	|	Документ.ПеремещениеТоваров КАК ДанныеДокумента
	|ГДЕ
	|	ДанныеДокумента.Ссылка = &Ссылка
	|	И ДанныеДокумента.НомерБланкаСтрогойОтчетности <> """"
	|	И ДанныеДокумента.СерияБланкаСтрогойОтчетности <> """"
	|	И ДанныеДокумента.ТипБланкаСтрогойОтчетности <> ЗНАЧЕНИЕ(Справочник.БланкиСтрогойОтчетности.ПустаяСсылка)
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	ИспорченныеНомераБСО.СерияБланкаСтрогойОтчетности,
	|	ИспорченныеНомераБСО.НомерБланкаСтрогойОтчетности,
	|	ИспорченныеНомераБСО.ТипБланкаСтрогойОтчетности,
	|	&Ссылка,
	|	&Период,
	|	ЗНАЧЕНИЕ(Перечисление.ВидыСписанияБСО.Испорчены)
	|ИЗ
	|	Документ.ПеремещениеТоваров.ИспорченныеНомераБСО КАК ИспорченныеНомераБСО
	|ГДЕ
	|	ИспорченныеНомераБСО.Ссылка = &Ссылка
	|	И ИспорченныеНомераБСО.НомерБланкаСтрогойОтчетности <> """"
	|	И ИспорченныеНомераБСО.СерияБланкаСтрогойОтчетности <> """"
	|	И ИспорченныеНомераБСО.ТипБланкаСтрогойОтчетности <> ЗНАЧЕНИЕ(Справочник.БланкиСтрогойОтчетности.ПустаяСсылка)";
	
	ТекстыЗапроса.Добавить(ТекстЗапроса, ИмяРегистра);
	
	Возврат ТекстЗапроса;
	
КонецФункции
// }
// 4D
 
 #КонецОбласти