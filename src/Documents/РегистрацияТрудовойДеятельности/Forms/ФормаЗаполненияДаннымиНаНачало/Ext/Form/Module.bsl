﻿#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	ЗаполнитьЗначенияСвойств(ЭтотОбъект, Параметры, "СсылкаНаОбъект,Организация");
	
	Если Не ЗначениеЗаполнено(Организация) Тогда
		Отказ = Истина;
		Возврат;
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	Если РазмерПорции = 0 Тогда
		РазмерПорции = 100;
	КонецЕсли;
	
	УстановитьДоступностьЭлементовФормы(ЭтотОбъект);
	
КонецПроцедуры

#КонецОбласти


#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура ЗаполнятьПорциямиПриИзменении(Элемент)
	
	УстановитьДоступностьЭлементовФормы(ЭтотОбъект);
	
КонецПроцедуры

#КонецОбласти


#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура Заполнить(Команда)
	
	Закрыть(АдресДанныхЗаполнения());
	
КонецПроцедуры

#КонецОбласти


#Область СлужебныеПроцедурыИФункции

&НаКлиентеНаСервереБезКонтекста
Процедура УстановитьДоступностьЭлементовФормы(УправляемаяФорма)
	
	ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(
		УправляемаяФорма.Элементы,
		"РазмерПорции",
		"Доступность",
		УправляемаяФорма.ЗаполнятьПорциями);
	
КонецПроцедуры

&НаСервере
Функция АдресДанныхЗаполнения()
	
	Запрос = Новый Запрос;
	Запрос.МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
	
	Запрос.УстановитьПараметр("Организация", Организация);
	Запрос.УстановитьПараметр("ДатаНачалаУчета", ЭлектронныеТрудовыеКнижки.ДатаНачалаУчета());
	Запрос.УстановитьПараметр("ИсключаемыйРегистратор", СсылкаНаОбъект);
	
	Запрос.Текст =
		"ВЫБРАТЬ РАЗРЕШЕННЫЕ
		|	МероприятияТрудовойДеятельности.Сотрудник КАК Сотрудник
		|ПОМЕСТИТЬ ВТСотрудникиСМероприятиями
		|ИЗ
		|	РегистрСведений.МероприятияТрудовойДеятельности КАК МероприятияТрудовойДеятельности
		|ГДЕ
		|	МероприятияТрудовойДеятельности.Организация = &Организация
		|	И МероприятияТрудовойДеятельности.ДатаМероприятия < &ДатаНачалаУчета
		|	И НЕ МероприятияТрудовойДеятельности.Регистратор В (&ИсключаемыйРегистратор)
		|
		|ОБЪЕДИНИТЬ ВСЕ
		|
		|ВЫБРАТЬ
		|	МероприятияТрудовойДеятельностиПрочие.Сотрудник
		|ИЗ
		|	РегистрСведений.МероприятияТрудовойДеятельностиПрочие КАК МероприятияТрудовойДеятельностиПрочие
		|ГДЕ
		|	МероприятияТрудовойДеятельностиПрочие.Организация = &Организация
		|	И МероприятияТрудовойДеятельностиПрочие.ДатаМероприятия = &ДатаНачалаУчета
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ РАЗЛИЧНЫЕ
		|	СотрудникиСМероприятиями.Сотрудник КАК Сотрудник
		|ИЗ
		|	ВТСотрудникиСМероприятиями КАК СотрудникиСМероприятиями";
	
	РанееПодобранные = Запрос.Выполнить().Выгрузить().ВыгрузитьКолонку("Сотрудник");
	
	ПараметрыПолучения = КадровыйУчет.ПараметрыПолученияСотрудниковОрганизацийПоСпискуФизическихЛиц();
	ПараметрыПолучения.Организация = Организация;
	ПараметрыПолучения.НачалоПериода = ЭлектронныеТрудовыеКнижки.ДатаНачалаУчета() - 1;
	ПараметрыПолучения.ОкончаниеПериода = ПараметрыПолучения.НачалоПериода;
	ПараметрыПолучения.КадровыеДанные = ЭлектронныеТрудовыеКнижки.ИменаКадровыхДанныхСотрудниковДляНачалаУчета();
	
	ЭлектронныеТрудовыеКнижки.УстановитьОтборПараметровПолученияСотрудниковОрганизации(ПараметрыПолучения);
	
	КадровыйУчет.СоздатьВТСотрудникиОрганизации(Запрос.МенеджерВременныхТаблиц, Истина, ПараметрыПолучения);
	
	Запрос.УстановитьПараметр("РанееПодобранные", РанееПодобранные);
	Запрос.Текст =
		"ВЫБРАТЬ ПЕРВЫЕ 1
		|	СотрудникиОрганизации.Сотрудник КАК СотрудникЗаписи,
		|	СотрудникиОрганизации.ФизическоеЛицо КАК ФизическоеЛицо,
		|	СотрудникиОрганизации.Подразделение КАК Подразделение,
		|	СотрудникиОрганизации.Должность КАК Должность,
		|	ВЫБОР
		|		КОГДА СотрудникиОрганизации.ВидЗанятости = ЗНАЧЕНИЕ(Перечисление.ВидыЗанятости.ОсновноеМестоРаботы)
		|			ТОГДА ЛОЖЬ
		|		ИНАЧЕ ИСТИНА
		|	КОНЕЦ КАК ЯвляетсяСовместителем,
		|	ВЫБОР
		|		КОГДА СотрудникиОрганизации.ВидСобытия = ЗНАЧЕНИЕ(Перечисление.ВидыКадровыхСобытий.Прием)
		|			ТОГДА ЗНАЧЕНИЕ(Перечисление.ВидыМероприятийТрудовойДеятельности.Прием)
		|		КОГДА СотрудникиОрганизации.ВидСобытия = ЗНАЧЕНИЕ(Перечисление.ВидыКадровыхСобытий.Перемещение)
		|			ТОГДА ЗНАЧЕНИЕ(Перечисление.ВидыМероприятийТрудовойДеятельности.Перевод)
		|		ИНАЧЕ ЗНАЧЕНИЕ(Перечисление.ВидыМероприятийТрудовойДеятельности.ПустаяСсылка)
		|	КОНЕЦ КАК ВидМероприятия,
		|	СотрудникиОрганизации.РабочееМестоПериодРегистрации КАК ДатаМероприятия,
		|	ВЫБОР
		|		КОГДА СотрудникиОрганизации.ВидСобытия В (ЗНАЧЕНИЕ(Перечисление.ВидыКадровыхСобытий.Прием), ЗНАЧЕНИЕ(Перечисление.ВидыКадровыхСобытий.Перемещение))
		|			ТОГДА СотрудникиОрганизации.РабочееМестоРегистратор.Дата
		|		ИНАЧЕ ДАТАВРЕМЯ(1, 1, 1)
		|	КОНЕЦ КАК ДатаДокументаОснования,
		|	ВЫБОР
		|		КОГДА СотрудникиОрганизации.ВидСобытия В (ЗНАЧЕНИЕ(Перечисление.ВидыКадровыхСобытий.Прием), ЗНАЧЕНИЕ(Перечисление.ВидыКадровыхСобытий.Перемещение))
		|			ТОГДА СотрудникиОрганизации.РабочееМестоРегистратор.Номер
		|		ИНАЧЕ """"
		|	КОНЕЦ КАК НомерДокументаОснования,
		|	СотрудникиОрганизации.РабочееМестоРегистратор.НомерПриказа КАК НомерПриказа,
		|	&РазрядКатегория КАК РазрядКатегория
		|ИЗ
		|	ВТСотрудникиОрганизации КАК СотрудникиОрганизации
		|ГДЕ
		|	НЕ СотрудникиОрганизации.Сотрудник В (&РанееПодобранные)
		|	И СотрудникиОрганизации.ВидСобытия <> ЗНАЧЕНИЕ(Перечисление.ВидыКадровыхСобытий.Увольнение)
		|	И (СотрудникиОрганизации.ДатаУвольнения = ДАТАВРЕМЯ(1, 1, 1)
		|			ИЛИ СотрудникиОрганизации.ДатаУвольнения >= &ДатаНачалаУчета)
		|
		|УПОРЯДОЧИТЬ ПО
		|	СотрудникиОрганизации.ФИОПолные,
		|	ЯвляетсяСовместителем";
	
	Если ЗаполнятьПорциями Тогда
		Запрос.Текст = СтрЗаменить(Запрос.Текст, "ПЕРВЫЕ 1", "ПЕРВЫЕ " + Формат(РазмерПорции, "ЧГ="));
	Иначе
		Запрос.Текст = СтрЗаменить(Запрос.Текст, "ПЕРВЫЕ 1", "");
	КонецЕсли;
	
	ЭлектронныеТрудовыеКнижки.УточнитьЗапросПолученияДанныхНаНачалоУчета(Запрос);
	
	ДанныеЗаполнения = Новый Массив;
	ДанныеСотрудниковБезМероприятий = Новый Массив;
	
	УстановитьПривилегированныйРежим(Истина);
	Выборка = Запрос.Выполнить().Выбрать();
	УстановитьПривилегированныйРежим(Ложь);
	
	Пока Выборка.Следующий() Цикл
		
		ДанныеСотрудника = ЭлектронныеТрудовыеКнижки.ПустаяСтруктураЗаписиОТрудовойДеятельности();
		ЗаполнитьЗначенияСвойств(ДанныеСотрудника, Выборка);
		ДанныеСотрудника.НомерДокументаОснования = ЗарплатаКадрыОтчеты.НомерНаПечать(
			Выборка.НомерДокументаОснования, Выборка.НомерПриказа);
		
		Если ДанныеСотрудника.ВидМероприятия = Перечисления.ВидыМероприятийТрудовойДеятельности.Прием Тогда
			ДанныеСотрудника.НаименованиеДокументаОснования = ЭлектронныеТрудовыеКнижки.НаименованиеДокументаПоВидуДокументаСобытия(Организация, "ПриемНаРаботу");
		ИначеЕсли ДанныеСотрудника.ВидМероприятия = Перечисления.ВидыМероприятийТрудовойДеятельности.Перевод Тогда
			ДанныеСотрудника.НаименованиеДокументаОснования = ЭлектронныеТрудовыеКнижки.НаименованиеДокументаПоВидуДокументаСобытия(Организация, "КадровыйПеревод");
		КонецЕсли;
		
		ДанныеЗаполнения.Добавить(ДанныеСотрудника);
		
		Если Не ЗначениеЗаполнено(ДанныеСотрудника.ВидМероприятия) Тогда
			ДанныеСотрудниковБезМероприятий.Добавить(ДанныеСотрудника);
		КонецЕсли;
		
	КонецЦикла;
	
	Если ДанныеЗаполнения.Количество() = 0 Тогда
		Возврат "";
	КонецЕсли;
	
	Если ДанныеСотрудниковБезМероприятий.Количество() > 0 Тогда
		ЭлектронныеТрудовыеКнижки.ДополнитьМероприятияЭТКДаннымиРеестраКадровыхПриказов(ДанныеСотрудниковБезМероприятий);
	КонецЕсли;
	
	Возврат ПоместитьВоВременноеХранилище(ДанныеЗаполнения, Новый УникальныйИдентификатор);
	
КонецФункции

#КонецОбласти
