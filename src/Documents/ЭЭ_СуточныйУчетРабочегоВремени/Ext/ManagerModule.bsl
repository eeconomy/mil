﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ПрограммныйИнтерфейс

// СтандартныеПодсистемы.ВерсионированиеОбъектов
// Определяет настройки объекта для подсистемы ВерсионированиеОбъектов.
//
// Параметры:
// Настройки - Структура - настройки подсистемы.
Процедура ПриОпределенииНастроекВерсионированияОбъектов(Настройки) Экспорт
	
КонецПроцедуры

#КонецОбласти

#Область Команды

Процедура ДобавитьКомандыОтчетов(КомандыОтчетов, Параметры) Экспорт
	
	ВариантыОтчетовУТПереопределяемый.ДобавитьКомандуСтруктураПодчиненности(КомандыОтчетов);  
	
КонецПроцедуры

Процедура ДобавитьКомандыПечати(КомандыПечати) Экспорт
	
	КомандаПечати = КомандыПечати.Добавить();
	КомандаПечати.Идентификатор = "ПечатьШтрихкодов";
	КомандаПечати.МенеджерПечати = "Документ.ЭЭ_СуточныйУчетРабочегоВремени";
	КомандаПечати.Представление = НСтр("ru = 'Печать управляющих штрихкодов'");
	КомандаПечати.Картинка = БиблиотекаКартинок.Печать;
	КомандаПечати.ПроверкаПроведенияПередПечатью = Ложь;
	
	КомандаПечати = КомандыПечати.Добавить();
	КомандаПечати.Идентификатор = "СуточныйУчетВыработкиСотрудника";
	КомандаПечати.МенеджерПечати = "Документ.ЭЭ_СуточныйУчетРабочегоВремени";
	КомандаПечати.Представление = НСтр("ru = 'Суточный учет выработки сотрудника'");
	КомандаПечати.Картинка = БиблиотекаКартинок.Печать;
	КомандаПечати.ПроверкаПроведенияПередПечатью = Истина;
	
КонецПроцедуры

Процедура Печать(МассивОбъектов, ПараметрыПечати, КоллекцияПечатныхФорм, ОбъектыПечати, ПараметрыВывода) Экспорт
	
	Если УправлениеПечатью.НужноПечататьМакет(КоллекцияПечатныхФорм, "ПечатьШтрихкодов") Тогда
		УправлениеПечатью.ВывестиТабличныйДокументВКоллекцию(
			КоллекцияПечатныхФорм,
			"ПечатьШтрихкодов",
			НСтр("ru = 'Печать управляющих штрихкодов'"),
			ПечатьШКНаСервере(МассивОбъектов, ОбъектыПечати, ПараметрыПечати));
	КонецЕсли;
	
	Если УправлениеПечатью.НужноПечататьМакет(КоллекцияПечатныхФорм, "СуточныйУчетВыработкиСотрудника") Тогда
		УправлениеПечатью.ВывестиТабличныйДокументВКоллекцию(
			КоллекцияПечатныхФорм,
			"СуточныйУчетВыработкиСотрудника",
			НСтр("ru = 'Суточный учет выработки сотрудника'"),
			ПечатьВыработкиНаСервере(МассивОбъектов, ОбъектыПечати, ПараметрыПечати));
	КонецЕсли;
	
КонецПроцедуры

Функция ПечатьШКНаСервере(МассивСсылок, ОбъектыПечати, ПараметрыПечати)
	
	ТабДок = Новый ТабличныйДокумент;
	ТабДок.ИмяПараметровПечати = "ПАРАМЕТРЫ_ПЕЧАТИ_ЭЭ_СуточныйУчетРабочегоВремени_ПечатьШтрихкодов";
	Макет = УправлениеПечатью.МакетПечатнойФормы("Документ.ЭЭ_СуточныйУчетРабочегоВремени.ПФ_MXL_ПечатьШтрихкодов");
	ТабДок.ЧерноБелаяПечать = Истина;
	
	ОбластьСоздания 	= Макет.ПолучитьОбласть("Создание|Создание1");
	ОбластьЗакрытия 	= Макет.ПолучитьОбласть("Создание|Закрытие2");
	ОбластьДатаВперед 	= Макет.ПолучитьОбласть("Вперед1|Создание1");
	ОбластьДатаНазад 	= Макет.ПолучитьОбласть("Вперед1|Закрытие2");
	
	Эталон = Обработки.ПечатьЭтикетокИЦенников.ПолучитьМакет("Эталон");
	КоличествоМиллиметровВПикселе = Эталон.Рисунки.Квадрат100Пикселей.Высота / 100;
	ШтрихкодСоздания 	= "0011111";
	ШтрихкодЗакрытия 	= "0000011";
	ШтрихкодДатаВперед 	= "0000111";
	ШтрихкодДатаНазад 	= "0001111";
	
	ПараметрыШтрихкода = ГенерацияШтрихкода.ПараметрыГенерацииШтрихкода();
	ПараметрыШтрихкода.Ширина 	= Окр(ОбластьСоздания.Рисунки.КартинкаШтрихкода.Ширина / КоличествоМиллиметровВПикселе);
	ПараметрыШтрихкода.Высота 	= Окр(ОбластьСоздания.Рисунки.КартинкаШтрихкода.Высота / КоличествоМиллиметровВПикселе);
	ПараметрыШтрихкода.Штрихкод = ШтрихкодСоздания;
	ПараметрыШтрихкода.ТипКода 	= 16; // QR
	ПараметрыШтрихкода.ОтображатьТекст = Ложь;
	ПараметрыШтрихкода.Масштабировать = Истина;
	
	РезультатОперации = ГенерацияШтрихкода.ИзображениеШтрихкода(ПараметрыШтрихкода);
	Если  РезультатОперации.Результат Тогда
		ОбластьСоздания.Рисунки.КартинкаШтрихкода.Картинка = РезультатОперации.Картинка;
	КонецЕсли;
	ТабДок.Вывести(ОбластьСоздания);
	
	ПараметрыШтрихкода.Штрихкод = ШтрихкодЗакрытия;
	РезультатОперации = ГенерацияШтрихкода.ИзображениеШтрихкода(ПараметрыШтрихкода);
	Если РезультатОперации.Результат Тогда
		ОбластьЗакрытия.Рисунки.КартинкаШтрихкода1.Картинка = РезультатОперации.Картинка;
	КонецЕсли;
	
	Если ТабДок.ПроверитьПрисоединение(ОбластьЗакрытия) Тогда
		ТабДок.Присоединить(ОбластьЗакрытия);
	Иначе
		ТабДок.Вывести(ОбластьЗакрытия);
	КонецЕсли;
	
	ПараметрыШтрихкода.Штрихкод = ШтрихкодДатаВперед;
	РезультатОперации = ГенерацияШтрихкода.ИзображениеШтрихкода(ПараметрыШтрихкода);
	Если РезультатОперации.Результат Тогда
		ОбластьДатаВперед.Рисунки.КартинкаШтрихкода4.Картинка = РезультатОперации.Картинка;
	КонецЕсли;
	
	Если ТабДок.ПроверитьПрисоединение(ОбластьДатаВперед) Тогда
		ТабДок.Присоединить(ОбластьДатаВперед);
	Иначе
		ТабДок.Вывести(ОбластьДатаВперед);
	КонецЕсли;
	
	ПараметрыШтрихкода.Штрихкод = ШтрихкодДатаНазад;
	РезультатОперации = ГенерацияШтрихкода.ИзображениеШтрихкода(ПараметрыШтрихкода);
	Если РезультатОперации.Результат Тогда
		ОбластьДатаНазад.Рисунки.КартинкаШтрихкода5.Картинка = РезультатОперации.Картинка;
	КонецЕсли;
	
	Если ТабДок.ПроверитьПрисоединение(ОбластьДатаНазад) Тогда
		ТабДок.Присоединить(ОбластьДатаНазад);
	Иначе
		ТабДок.Вывести(ОбластьДатаНазад);
	КонецЕсли;
	
	Возврат ТабДок;
	
КонецФункции

Функция ПечатьВыработкиНаСервере(МассивСсылок, ОбъектыПечати, ПараметрыПечати)
	
	ТабДок = Новый ТабличныйДокумент;
	ТабДок.ИмяПараметровПечати = "ПАРАМЕТРЫ_ПЕЧАТИ_ЭЭ_СуточныйУчетРабочегоВремени_СуточныйУчетВыработкиСотрудника";
	Макет = УправлениеПечатью.МакетПечатнойФормы("Документ.ЭЭ_СуточныйУчетРабочегоВремени.ПФ_MXL_СуточныйУчетВыработкиСотрудника");
	
	ОбластьСоздания = Макет.ПолучитьОбласть("Шапка");
	ТабДок.Вывести(ОбластьСоздания);
	
	Возврат ТабДок;
	
КонецФункции

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область Проведение

Процедура ЗаполнитьПараметрыИнициализации(Запрос, ДокументСсылка)
	
	Запрос.МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
	Запрос.УстановитьПараметр("Ссылка", ДокументСсылка);
	
	Запрос.Текст =
		"ВЫБРАТЬ
		|	Реквизиты.Ссылка КАК Ссылка,
		|	Реквизиты.Дата КАК Период
		|ИЗ
		|	Документ.ЭЭ_СуточныйУчетРабочегоВремени КАК Реквизиты
		|ГДЕ
		|	Реквизиты.Ссылка = &Ссылка";
	
	Реквизиты = Запрос.Выполнить().Выбрать();
	Реквизиты.Следующий();
	
	Запрос.УстановитьПараметр("Период", Реквизиты.Период);
	
КонецПроцедуры

Процедура ИнициализироватьДанныеДокумента(ДокументСсылка, ДополнительныеСвойства, Регистры = Неопределено) Экспорт
	
	Запрос = Новый Запрос;
	ЗаполнитьПараметрыИнициализации(Запрос, ДокументСсылка);
	
	ТекстыЗапроса = Новый СписокЗначений;
	
	ТекстЗапросаТаблицаТрудозатратыКОформлению(Запрос, ТекстыЗапроса, Регистры);
	ТекстЗапросаПлановыеТрудозатраты36Процесса(Запрос, ТекстыЗапроса, Регистры);
	ПроведениеСерверУТ.ИнициализироватьТаблицыДляДвижений(Запрос, ТекстыЗапроса, ДополнительныеСвойства.ТаблицыДляДвижений, Истина);
	
КонецПроцедуры

Функция ТекстЗапросаТаблицаТрудозатратыКОформлению(Запрос, ТекстыЗапроса, Регистры)
	
	ИмяРегистра = "ТрудозатратыКОформлению";
	
	Если НЕ ПроведениеСерверУТ.ТребуетсяТаблицаДляДвижений(ИмяРегистра, Регистры) Тогда
		Возврат "";
	КонецЕсли;
	
	ТекстЗапроса =
		"ВЫБРАТЬ
		|	&Период КАК Период,
		|	ЭЭ_СуточныйУчетРабочегоВремениСуточныйУчет.Этап.Организация КАК Организация,
		|	ЗНАЧЕНИЕ(ВидДвиженияНакопления.Приход) КАК ВидДвижения,
		|	ЭЭ_СуточныйУчетРабочегоВремениСуточныйУчет.Этап.ПартияПроизводства КАК ПартияПроизводства,
		|	ЭЭ_СуточныйУчетРабочегоВремениСуточныйУчет.Этап КАК Распоряжение,
		|	ЭЭ_СуточныйУчетРабочегоВремениСуточныйУчет.КодСтроки КАК КодСтрокиРаспоряжения,
		|	ЭЭ_СуточныйУчетРабочегоВремениСуточныйУчет.Этап.Подразделение КАК Подразделение,
		|	ЭЭ_СуточныйУчетРабочегоВремениСуточныйУчет.Операция.Чд_КодОперации.ВидРабот КАК ВидРабот,
		|	ЭЭ_СуточныйУчетРабочегоВремениСуточныйУчет.Ссылка.ФизЛицо КАК Исполнитель,
		|	СУММА(ЭЭ_СуточныйУчетРабочегоВремениСуточныйУчет.КоличествоСекПоОперации * ЭЭ_СуточныйУчетРабочегоВремениСуточныйУчет.КоличествоВПачке) КАК Количество
		|ИЗ
		|	Документ.ЭЭ_СуточныйУчетРабочегоВремени.СуточныйУчет КАК ЭЭ_СуточныйУчетРабочегоВремениСуточныйУчет
		|ГДЕ
		|	ЭЭ_СуточныйУчетРабочегоВремениСуточныйУчет.Ссылка = &Ссылка
		|
		|СГРУППИРОВАТЬ ПО
		|	ЭЭ_СуточныйУчетРабочегоВремениСуточныйУчет.Этап,
		|	ЭЭ_СуточныйУчетРабочегоВремениСуточныйУчет.Операция.Чд_КодОперации.ВидРабот,
		|	ЭЭ_СуточныйУчетРабочегоВремениСуточныйУчет.Ссылка.ФизЛицо,
		|	ЭЭ_СуточныйУчетРабочегоВремениСуточныйУчет.Этап.Организация,
		|	ЭЭ_СуточныйУчетРабочегоВремениСуточныйУчет.Этап.ПартияПроизводства,
		|	ЭЭ_СуточныйУчетРабочегоВремениСуточныйУчет.КодСтроки,
		|	ЭЭ_СуточныйУчетРабочегоВремениСуточныйУчет.Этап.Подразделение";
	
	ТекстыЗапроса.Добавить(ТекстЗапроса, ИмяРегистра);
	
	Возврат ТекстЗапроса;
	
КонецФункции

Функция ТекстЗапросаПлановыеТрудозатраты36Процесса(Запрос, ТекстыЗапроса, Регистры)
	
	ИмяРегистра = "ПлановыеТрудозатраты36Процесса";
	
	Если НЕ ПроведениеСерверУТ.ТребуетсяТаблицаДляДвижений(ИмяРегистра, Регистры) Тогда
		Возврат "";
	КонецЕсли;
	
	ТекстЗапроса =
		"ВЫБРАТЬ
		|	&Период КАК Период,
		|	ЗНАЧЕНИЕ(ВидДвиженияНакопления.Расход) КАК ВидДвижения,
		|	ЭЭ_СуточныйУчетРабочегоВремениСуточныйУчет.Штрихкод КАК Штрихкод,
		|	ЭЭ_СуточныйУчетРабочегоВремениСуточныйУчет.НомерПачки КАК НомерПачки,
		|	ЭЭ_СуточныйУчетРабочегоВремениСуточныйУчет.Операция КАК Операция,
		|	ЭЭ_СуточныйУчетРабочегоВремениСуточныйУчет.Этап КАК ЭтапПроизводства,
		|	ЭЭ_СуточныйУчетРабочегоВремениСуточныйУчет.ОперацияРодитель КАК ОперацияРодитель,
		|	ЭЭ_СуточныйУчетРабочегоВремениСуточныйУчет.ПачкаРодитель КАК ПачкаРодитель,
		|	СУММА(ЭЭ_СуточныйУчетРабочегоВремениСуточныйУчет.КоличествоВПачке) КАК КоличествоВПачке,
		|	СУММА(ЭЭ_СуточныйУчетРабочегоВремениСуточныйУчет.КоличествоСекПоОперации) КАК ВремяНаЕдиницу
		|ИЗ
		|	Документ.ЭЭ_СуточныйУчетРабочегоВремени.СуточныйУчет КАК ЭЭ_СуточныйУчетРабочегоВремениСуточныйУчет
		|ГДЕ
		|	ЭЭ_СуточныйУчетРабочегоВремениСуточныйУчет.Ссылка = &Ссылка
		|
		|СГРУППИРОВАТЬ ПО
		|	ЭЭ_СуточныйУчетРабочегоВремениСуточныйУчет.Этап,
		|	ЭЭ_СуточныйУчетРабочегоВремениСуточныйУчет.ОперацияРодитель,
		|	ЭЭ_СуточныйУчетРабочегоВремениСуточныйУчет.Операция,
		|	ЭЭ_СуточныйУчетРабочегоВремениСуточныйУчет.Штрихкод,
		|	ЭЭ_СуточныйУчетРабочегоВремениСуточныйУчет.НомерПачки,
		|	ЭЭ_СуточныйУчетРабочегоВремениСуточныйУчет.ПачкаРодитель";
	
	ТекстыЗапроса.Добавить(ТекстЗапроса, ИмяРегистра);
	
	Возврат ТекстЗапроса;
	
КонецФункции

#КонецОбласти

#КонецОбласти

#КонецЕсли
