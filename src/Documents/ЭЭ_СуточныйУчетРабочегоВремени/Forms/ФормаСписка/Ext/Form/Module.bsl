﻿#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	ОбщегоНазначенияУТ.НастроитьПодключаемоеОборудование(ЭтотОбъект);

	// СтандартныеПодсистемы.ПодключаемыеКоманды
	ПодключаемыеКоманды.ПриСозданииНаСервере(ЭтотОбъект);
	// Конец СтандартныеПодсистемы.ПодключаемыеКоманды
	
	СобытияФорм.ПриСозданииНаСервере(ЭтотОбъект, Отказ, СтандартнаяОбработка);
	ДатаПоУмолчанию = ТекущаяДатаСеанса();	
	СтандартныеПодсистемыСервер.УстановитьУсловноеОформлениеПоляДата(ЭтотОбъект, "Список.Дата", "Дата");
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	// МеханизмВнешнегоОборудования
	ИспользоватьПодключаемоеОборудование = Истина;
	ОповещениеПриПодключении = Новый ОписаниеОповещения("ПодключитьОборудованиеЗавершениеПоУмолчанию", МенеджерОборудованияКлиент);
	МенеджерОборудованияКлиент.НачатьПодключениеОборудованиеПриОткрытииФормы(
						ОповещениеПриПодключении, ЭтотОбъект, "СканерШтрихкода");
	// Конец МеханизмВнешнегоОборудования

    // СтандартныеПодсистемы.ПодключаемыеКоманды
    ПодключаемыеКомандыКлиент.НачатьОбновлениеКоманд(ЭтотОбъект);
    // Конец СтандартныеПодсистемы.ПодключаемыеКоманды
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	Если Источник = "ПодключаемоеОборудование" И ВводДоступен()
		И ИмяСобытия = "ScanData" И МенеджерОборудованияУТКлиент.ЕстьНеобработанноеСобытие() Тогда
		
		Если Параметр[1] = Неопределено Тогда
			ЗначениеСканера = Параметр[0];
		Иначе
			ЗначениеСканера = Параметр[1][1];
		КонецЕсли;
		
		Если ЗначениеСканера = "0011111" Тогда
			Если КодПроцессаПоУмолчанию = ПредопределенноеЗначение("Справочник.Чд_Процессы.Пошив60") Тогда
				ПараметрыОткрытия = Новый Структура;
				ПараметрыОткрытия.Вставить("Дата", ДатаПоУмолчанию);
				ПараметрыОткрытия.Вставить("КодПроцесса", КодПроцессаПоУмолчанию);
				ПараметрыФормы = Новый Структура("ЗначенияЗаполнения", ПараметрыОткрытия);
				ОткрытьФорму("Документ.ЭЭ_СуточныйУчетРабочегоВремени.Форма.ФормаДокумента", ПараметрыФормы, ЭтотОбъект);		
			Иначе
				ОбщегоНазначенияКлиент.СообщитьПользователю(
					НСтр("ru = 'Создание документа сканером доступно только для процесса с кодом 60!'"));
			КонецЕсли;
		КонецЕсли;
	КонецЕсли;

КонецПроцедуры

&НаКлиенте
Процедура ПриЗакрытии(ЗавершениеРаботы)
	
	Если НЕ ЗавершениеРаботы Тогда
		МенеджерОборудованияКлиент.НачатьОтключениеОборудованиеПриЗакрытииФормы(Неопределено, ЭтотОбъект);
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура Создать(Команда)
	
	ОчиститьСообщения();
	Если ПроверитьЗаполнение() Тогда
		ПараметрыОткрытия = Новый Структура;
		ПараметрыОткрытия.Вставить("Дата", ДатаПоУмолчанию);
		ПараметрыОткрытия.Вставить("КодПроцесса", КодПроцессаПоУмолчанию);
		ПараметрыФормы = Новый Структура("ЗначенияЗаполнения", ПараметрыОткрытия);
		ОткрытьФорму("Документ.ЭЭ_СуточныйУчетРабочегоВремени.Форма.ФормаДокумента", ПараметрыФормы, ЭтотОбъект);		
	Иначе
		ОбщегоНазначенияКлиент.СообщитьПользователю("Заполните дату по умолчанию и/или код процесса по умолчанию!");
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ОтразитьВыработку(Команда)
	
	ОчиститьСообщения();
	ОтразитьВыработкуНаСервере();
	Элементы.Список.Обновить();
	
КонецПроцедуры

// СтандартныеПодсистемы.ПодключаемыеКоманды
&НаКлиенте
Процедура Подключаемый_ВыполнитьКоманду(Команда)
    ПодключаемыеКомандыКлиент.ВыполнитьКоманду(ЭтотОбъект, Команда, Элементы.Список);
КонецПроцедуры

&НаСервере
Процедура Подключаемый_ВыполнитьКомандуНаСервере(Контекст, Результат)
    ПодключаемыеКоманды.ВыполнитьКоманду(ЭтотОбъект, Контекст, Элементы.Список, Результат);
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ОбновитьКоманды()
    ПодключаемыеКомандыКлиентСервер.ОбновитьКоманды(ЭтотОбъект, Элементы.Список);
КонецПроцедуры
// Конец СтандартныеПодсистемы.ПодключаемыеКоманды

#КонецОбласти

#Область ОбработчикиСобытийЭлементовФормы

&НаКлиенте
Процедура СписокПриАктивизацииСтроки(Элемент)
	
    // СтандартныеПодсистемы.ПодключаемыеКоманды
    ПодключаемыеКомандыКлиент.НачатьОбновлениеКоманд(ЭтотОбъект);
    // Конец СтандартныеПодсистемы.ПодключаемыеКоманды
	
КонецПроцедуры

&НаКлиенте
Процедура ОтветственныйПриИзменении(Элемент)
	ОтветственныйПриИзмененииСервер();
КонецПроцедуры

&НаСервере
Процедура ОтветственныйПриИзмененииСервер()
	
	Если ЗначениеЗаполнено(Ответственный) Тогда 	
		ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(Список, 
		"Ответственный", Ответственный, ВидСравненияКомпоновкиДанных.Равно, , Истина); 
	Иначе
		ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(Список, 
		"Ответственный", Ответственный, ВидСравненияКомпоновкиДанных.Равно, , Ложь); 		
	КонецЕсли;
		
КонецПроцедуры

&НаКлиенте
Процедура ПодразделениеПриИзменении(Элемент)
	ПодразделениеПриИзмененииСервер();
КонецПроцедуры

&НаСервере
Процедура ПодразделениеПриИзмененииСервер()
		
	Если ЗначениеЗаполнено(Подразделение) Тогда 	
		ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(Список, 
		"ПодразделениеРегистра", Подразделение, ВидСравненияКомпоновкиДанных.Равно, , Истина);
	Иначе
		ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(Список, 
		"ПодразделениеРегистра", Подразделение, ВидСравненияКомпоновкиДанных.Равно, , Ложь);	
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ИсполнительПриИзмененииСервер()
	
	Если ЗначениеЗаполнено(Исполнитель) Тогда 	
		ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(Список, 
						"ФИО", Исполнитель, ВидСравненияКомпоновкиДанных.Равно, , Истина);	
	Иначе
		ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(Список, 
						"ФИО", Исполнитель, ВидСравненияКомпоновкиДанных.Равно, , Ложь);	
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ИсполнительПриИзменении(Элемент)
	
	ИсполнительПриИзмененииСервер();

КонецПроцедуры

&НаКлиенте
Процедура КодПроцессаПриИзменении(Элемент)
	
	КодПроцессаПриИзмененииНаСервере();
	
КонецПроцедуры

&НаСервере
Процедура КодПроцессаПриИзмененииНаСервере()
	
	Если ЗначениеЗаполнено(КодПроцесса) Тогда 	
		ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(Список, 
		"КодПроцесса", КодПроцесса, ВидСравненияКомпоновкиДанных.Равно, , Истина);
	Иначе
		ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(Список, 
		"КодПроцесса", КодПроцесса, ВидСравненияКомпоновкиДанных.Равно, , Ложь);	
	КонецЕсли;

КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура ОтразитьВыработкуНаСервере()
	
	Чд_УчетТрудозатрат.СоздатьДокументСУРВ();
	
КонецПроцедуры

//++EE:IVS 05.04.2023 MLVC-1067
&НаКлиенте
Процедура ОткрытьОбработкуКорректировкуСУРВ(Команда)
	Форма = ОткрытьФорму("Обработка.ЭЭ_КорректировкаСУРВ.Форма.Форма");
КонецПроцедуры
//--EE:IVS 05.04.2023 MLVC-1067

/// Мил. 2023.10.12 Евлампиева   ERP-307
&НаКлиенте
Процедура ОткрытьПериод(Команда)
	ОткрытьПериодПоШвейномуЦехуСервер();
КонецПроцедуры    

&НаСервере
Процедура ОткрытьПериодПоШвейномуЦехуСервер()     
	ЗапросПоДатеЗапрета = Новый Запрос;
	ЗапросПоДатеЗапрета.Текст = "ВЫБРАТЬ РАЗРЕШЕННЫЕ
	|	МАКСИМУМ(ДатыЗапретаИзменения.ДатаЗапрета) КАК ДатаЗапрета
	|ИЗ
	|	РегистрСведений.ДатыЗапретаИзменения КАК ДатыЗапретаИзменения";
	РезульатПоДате = ЗапросПоДатеЗапрета.Выполнить().Выгрузить();   
	Если РезульатПоДате.Количество() > 0 Тогда
		ДатаЗапретаРедактирования = РезульатПоДате[0].ДатаЗапрета;
	Иначе
		ДатаЗапретаРедактирования = НачалоМесяца(ТекущаяДата());
	КонецЕсли;
	
	
	
	Запрос =  Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
	               |	ВыработкаСотрудников.Ссылка КАК Док
	               |ИЗ
	               |	Документ.ВыработкаСотрудников КАК ВыработкаСотрудников
	               |ГДЕ
	               |	ВыработкаСотрудников.Проведен = ИСТИНА
	               |	И ВыработкаСотрудников.Дата МЕЖДУ &ДатаНач И &ДатаКон
	               |	И ВыработкаСотрудников.Подразделение = &Подразделение5Цех";
	Запрос.УстановитьПараметр("Подразделение5Цех",Справочники.ЭЭ_ПредопределенныеЗначения.ШвейныйЦехНомерПять.Значение);
	Запрос.УстановитьПараметр("ДатаКон",КонецМесяца(ТекущаяДата())); 
	Запрос.УстановитьПараметр("ДатаНач",КонецДня(Макс(Константы.ЭЭ_ДатаЗапретаРедактированияТрудозатрат.получить(),ДатаЗапретаРедактирования))+1); 
	
	Рез = Запрос.Выполнить().Выбрать();
	Пока Рез.Следующий() Цикл
		Док = Рез.Док.получитьОбъект();
		Док.УстановитьПометкуУдаления(Истина);
		Док.записать();
	КонецЦикла;	
КонецПроцедуры    
///кон  Мил. 2023.10.12 Евлампиева   ERP-307    

#КонецОбласти
