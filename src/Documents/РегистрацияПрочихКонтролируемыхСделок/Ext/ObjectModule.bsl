﻿
#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ОписаниеПеременных

Перем мУдалятьДвижения;

#КонецОбласти

#Область ОбработчикиСобытий

Процедура ОбработкаЗаполнения(ДанныеЗаполнения, СтандартнаяОбработка)

	Если ЗначениеЗаполнено(ДанныеЗаполнения) Тогда
		ЗаполнитьЗначенияСвойств(ЭтотОбъект, ДанныеЗаполнения);
	КонецЕсли;
	
	Если ЗначениеЗаполнено(УведомлениеОКонтролируемойСделке) Тогда
		Организация = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(УведомлениеОКонтролируемойСделке, "Организация");
	КонецЕсли;
	
	Если НЕ ЗначениеЗаполнено(ВалютаДокумента) Тогда
		ВалютаДокумента = Константы.ВалютаРегламентированногоУчета.Получить();
	КонецЕсли;
	
	Если Не ЗначениеЗаполнено(Организация) Тогда
		Организация = ЗначениеНастроекПовтИсп.ПолучитьОрганизациюПоУмолчанию(Организация);
	КонецЕсли;

КонецПроцедуры

Процедура ОбработкаПроверкиЗаполнения(Отказ, ПроверяемыеРеквизиты)
	
	Если Не ЗначениеЗаполнено(УведомлениеОКонтролируемойСделке) Тогда
		Возврат;
	КонецЕсли;
	
	РеквизитыУведомления = ОбщегоНазначения.ЗначенияРеквизитовОбъекта(УведомлениеОКонтролируемойСделке, "ОтчетныйГод");
	НачалоГода = НачалоГода(РеквизитыУведомления.ОтчетныйГод);
	ОкончаниеГода = КонецГода(РеквизитыУведомления.ОтчетныйГод);
	
	Для Каждого Сделка Из Сделки Цикл
		
		Префикс = "Сделки[" + Формат(Сделка.НомерСтроки - 1, "ЧН=0; ЧГ=") + "].";
		
		Если ЗначениеЗаполнено(Сделка.ДатаСовершенияСделки)
			И (Сделка.ДатаСовершенияСделки > ОкончаниеГода Или Сделка.ДатаСовершенияСделки < НачалоГода) Тогда
			ТекстСообщения = НСтр("ru = 'Дата совершения сделки в строке %1 не соответствует отчетному году уведомления. Сделка должна попадать в %2 год.';
									|en = 'Transaction date in line %1 does not conform to the notification''s reporting year. The transaction must fall within the year of %2.  '");
			ТекстСообщения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ТекстСообщения, Сделка.НомерСтроки, Формат(НачалоГода, "ДФ=yyyy"));
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, ЭтотОбъект, Префикс+"ДатаСовершенияСделки",, Отказ);
		КонецЕсли;
		
		Если Сделка.ТипПредметаСделки = Перечисления.ТипыПредметовКонтролируемыхСделок.ДолговоеОбязательство Тогда
			Если НЕ ЗначениеЗаполнено(Сделка.ДатаПроцентнойСтавки) Тогда
				ТекстСообщения = ОбщегоНазначенияКлиентСервер.ТекстОшибкиЗаполнения("Колонка", , НСтр("ru = 'Дата изменения процентной ставки';
																										|en = 'Interest rate change date'"),
					Сделка.НомерСтроки, "Сделки");
				Поле = Префикс + "ДатаПроцентнойСтавки";
				ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, ЭтотОбъект, Поле, "Объект", Отказ);
			КонецЕсли;
		КонецЕсли;
		
		Если Не ЗначениеЗаполнено(Сделка.СуммаБезНДСВРублях) Тогда
			ТекстСообщения = ОбщегоНазначенияКлиентСервер.ТекстОшибкиЗаполнения("Колонка", , НСтр("ru = 'Сумма (руб)';
																									|en = 'Amount (rub)'"),
				Сделка.НомерСтроки, "Сделки");
			Поле = Префикс + "СуммаБезНДСВРублях";
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, ЭтотОбъект, Поле, "Объект", Отказ);
		КонецЕсли;
		
		Если Не ЗначениеЗаполнено(Сделка.СтавкаНДС) Тогда
			ТекстСообщения = ОбщегоНазначенияКлиентСервер.ТекстОшибкиЗаполнения("Колонка", , НСтр("ru = 'Ставка НДС';
																									|en = 'VAT rate'"),
				Сделка.НомерСтроки, "Сделки");
			Поле = Префикс + "СтавкаНДС";
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, ЭтотОбъект, Поле, "Объект", Отказ);
		КонецЕсли;
		
	КонецЦикла;
	
	МассивНепроверяемыхРеквизитов = Новый Массив();
	МассивНепроверяемыхРеквизитов.Добавить("Сделки.ДатаПроцентнойСтавки");
	
	Если Не СделкаСовершенаЧерезКомиссионера Тогда
		МассивНепроверяемыхРеквизитов.Добавить("Комиссионер");
	КонецЕсли;
	
	ОбщегоНазначения.УдалитьНепроверяемыеРеквизитыИзМассива(ПроверяемыеРеквизиты, МассивНепроверяемыхРеквизитов);
	
КонецПроцедуры

Процедура ПередЗаписью(Отказ, РежимЗаписи, РежимПроведения)
	
	Если ОбменДанными.Загрузка  Тогда
		Возврат;
	КонецЕсли;

	ОбновлениеИнформационнойБазы.ПроверитьОбъектОбработан(ЭтотОбъект);
	
	мУдалятьДвижения = НЕ ЭтоНовый();
	
	СуммаДокумента = ЭтотОбъект.Сделки.Итог("СуммаБезНДСВРублях") + ЭтотОбъект.Сделки.Итог("СуммаНДСВРублях");
	
	Для Каждого Сделка Из Сделки Цикл
		
		Если Сделка.ТипПредметаСделки <> Перечисления.ТипыПредметовКонтролируемыхСделок.Товар Тогда
			Сделка.Грузоотправитель = Неопределено;
		КонецЕсли;
		
		Если Сделка.ТипПредметаСделки = Перечисления.ТипыПредметовКонтролируемыхСделок.РаботаУслуга Тогда
			Сделка.СтранаПроисхожденияПредметаСделки = Справочники.СтраныМира.ПустаяСсылка();
		КонецЕсли;
		
	КонецЦикла;
	
	Если ЗначениеЗаполнено(УведомлениеОКонтролируемойСделке) Тогда
		ВерсияУведомления = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(УведомлениеОКонтролируемойСделке, "ВерсияУведомления");
		Если ВерсияУведомления < КонтролируемыеСделкиКлиентСервер.ВерсияУведомления_2018() Тогда
			СделкаСовершенаЧерезКомиссионера = Ложь;
			Комиссионер = Справочники.Контрагенты.ПустаяСсылка();
		КонецЕсли;
	КонецЕсли;
	
	ДополнительныеСвойства.Вставить("ЭтоНовый", ЭтоНовый());
	ДополнительныеСвойства.Вставить("РежимЗаписи", РежимЗаписи);
	
КонецПроцедуры

Процедура ОбработкаПроведения(Отказ, РежимПроведения)
	
	ПроведениеСерверУТ.ИнициализироватьДополнительныеСвойстваДляПроведения(Ссылка, ДополнительныеСвойства, РежимПроведения);
	
	ПроведениеСерверУТ.ПодготовитьНаборыЗаписейКПроведению(ЭтотОбъект);
	
	Документы.РегистрацияПрочихКонтролируемыхСделок.ИнициализироватьДанныеДокумента(Ссылка, ДополнительныеСвойства);
	Если Отказ Тогда
		Возврат;
	КонецЕсли;
	
	// Зачет аванса
	КонтролируемыеСделки.СформироватьДвиженияКонтролируемыхСделокОрганизаций(
		ДополнительныеСвойства.ТаблицыДляДвижений.ТаблицаКонтролируемыеСделкиОрганизаций, Движения, Отказ);
	
КонецПроцедуры

Процедура ОбработкаУдаленияПроведения(Отказ)
	ПроведениеСерверУТ.ИнициализироватьДополнительныеСвойстваДляПроведения(Ссылка, ДополнительныеСвойства);
	ПроведениеСерверУТ.ПодготовитьНаборыЗаписейКРегистрацииДвижений(ЭтотОбъект);
	ПроведениеСерверУТ.ЗаписатьНаборыЗаписей(ЭтотОбъект);
	ПроведениеСерверУТ.ОчиститьДополнительныеСвойстваДляПроведения(ДополнительныеСвойства);
КонецПроцедуры

#КонецОбласти

#КонецЕсли