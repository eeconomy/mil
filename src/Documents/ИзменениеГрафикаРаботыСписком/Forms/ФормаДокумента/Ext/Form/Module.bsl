﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	// СтандартныеПодсистемы.ВерсионированиеОбъектов
	ВерсионированиеОбъектов.ПриСозданииНаСервере(ЭтаФорма);
	// Конец СтандартныеПодсистемы.ВерсионированиеОбъектов
	
	// СтандартныеПодсистемы.ПодключаемыеКоманды
	ПодключаемыеКоманды.ПриСозданииНаСервере(ЭтотОбъект);
	// Конец СтандартныеПодсистемы.ПодключаемыеКоманды
	
	КадровыйУчетФормы.ФормаКадровогоДокументаПриСозданииНаСервере(ЭтаФорма);
	РасчетЗарплатыРасширенныйФормы.ДокументыПриСозданииНаСервере(ЭтаФорма); 
	
	Если Параметры.Ключ.Пустая() Тогда
		
		ЗначенияДляЗаполнения = Новый Структура("Организация, Ответственный, ДатаСобытия",
			"Объект.Организация",
			"Объект.Ответственный",
			"Объект.ДатаИзменения");
		ЗарплатаКадры.ЗаполнитьПервоначальныеЗначенияВФорме(ЭтаФорма, ЗначенияДляЗаполнения);
		
		ПриПолученииДанныхНаСервере();
		ОрганизацияПриИзмененииНаСервере();
		
	КонецЕсли;
	
	// СтандартныеПодсистемы.Свойства
	ДополнительныеПараметры = Новый Структура;
	ДополнительныеПараметры.Вставить("ИмяЭлементаДляРазмещения", "ГруппаДополнительныеРеквизиты");
	УправлениеСвойствами.ПриСозданииНаСервере(ЭтотОбъект, ДополнительныеПараметры);
	// Конец СтандартныеПодсистемы.Свойства
	
КонецПроцедуры

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)
	
	// СтандартныеПодсистемы.ДатыЗапретаИзменения
	ДатыЗапретаИзменения.ОбъектПриЧтенииНаСервере(ЭтаФорма, ТекущийОбъект);
	// Конец СтандартныеПодсистемы.ДатыЗапретаИзменения
	
	ПриПолученииДанныхНаСервере();
	
	// СтандартныеПодсистемы.ПодключаемыеКоманды
	ПодключаемыеКомандыКлиентСервер.ОбновитьКоманды(ЭтотОбъект, Объект);
	// Конец СтандартныеПодсистемы.ПодключаемыеКоманды
	
	// СтандартныеПодсистемы.УправлениеДоступом
	УправлениеДоступом.ПриЧтенииНаСервере(ЭтотОбъект, ТекущийОбъект);
	// Конец СтандартныеПодсистемы.УправлениеДоступом
	
	// СтандартныеПодсистемы.Свойства
	УправлениеСвойствами.ПриЧтенииНаСервере(ЭтотОбъект, ТекущийОбъект);
	// Конец СтандартныеПодсистемы.Свойства
	
	// ПроцессыОбработкиДокументов
	Если ОбщегоНазначения.ПодсистемаСуществует("ЗарплатаКадрыПриложения.ПроцессыОбработкиДокументовЗарплата") Тогда
		МодульПроцессыОбработкиДокументовЗарплата = ОбщегоНазначения.ОбщийМодуль("ПроцессыОбработкиДокументовЗарплата");
		МодульПроцессыОбработкиДокументовЗарплата.ПриЧтенииНаСервере(ЭтотОбъект, ТекущийОбъект);
	КонецЕсли;	
	// Конец ПроцессыОбработкиДокументов
	
КонецПроцедуры

&НаКлиенте
Процедура ПередЗаписью(Отказ, ПараметрыЗаписи)
	
	Если ПараметрыЗаписи.РежимЗаписи <> РежимЗаписиДокумента.ОтменаПроведения
		И Не ПараметрыЗаписи.Свойство("ПроверкаПередЗаписьюВыполнена") Тогда
		
		Отказ = Истина;
		ЗаписатьНаКлиенте(Ложь, ПараметрыЗаписи);
		
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ПередЗаписьюНаСервере(Отказ, ТекущийОбъект, ПараметрыЗаписи)
	
	РеквизитВДанные(ТекущийОбъект);
	
	// СтандартныеПодсистемы.Свойства
	УправлениеСвойствами.ПередЗаписьюНаСервере(ЭтотОбъект, ТекущийОбъект);
	// Конец СтандартныеПодсистемы.Свойства
	
КонецПроцедуры

&НаКлиенте
Процедура ПослеЗаписи(ПараметрыЗаписи)
	
	Оповестить("Запись_ИзменениеГрафикаРаботыСписком", ПараметрыЗаписи, Объект.Ссылка);
	
КонецПроцедуры

&НаСервере
Процедура ПослеЗаписиНаСервере(ТекущийОбъект, ПараметрыЗаписи)
	
	// СтандартныеПодсистемы.УправлениеДоступом
	УправлениеДоступом.ПослеЗаписиНаСервере(ЭтотОбъект, ТекущийОбъект, ПараметрыЗаписи);
	// Конец СтандартныеПодсистемы.УправлениеДоступом
	
	ПрочитатьВремяРегистрации();
	ДанныеВРеквизит();
	УстановитьОтображениеНадписей();
	
	// ПроцессыОбработкиДокументов
	Если ОбщегоНазначения.ПодсистемаСуществует("ЗарплатаКадрыПриложения.ПроцессыОбработкиДокументовЗарплата") Тогда
		МодульПроцессыОбработкиДокументовЗарплата = ОбщегоНазначения.ОбщийМодуль("ПроцессыОбработкиДокументовЗарплата");
		МодульПроцессыОбработкиДокументовЗарплата.ПослеЗаписиНаСервере(ЭтотОбъект, ТекущийОбъект, ПараметрыЗаписи);
	КонецЕсли;	
	// Конец ПроцессыОбработкиДокументов
	
КонецПроцедуры

&НаКлиенте
Процедура ПередЗакрытием(Отказ, ЗавершениеРаботы, ТекстПредупреждения, СтандартнаяОбработка)
	
	Оповещение = Новый ОписаниеОповещения("ЗаписатьИЗакрытьНаКлиенте", ЭтотОбъект);
	ОбщегоНазначенияКлиент.ПоказатьПодтверждениеЗакрытияФормы(Оповещение, Отказ, ЗавершениеРаботы);
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	Если Источник = ЭтаФорма Тогда
		Если ИмяСобытия = "ИзмененыНачисления" И Источник = ЭтаФорма Тогда
			ЗаполнитьНачисленияИзВременногоХранилища(Параметр.АдресВХранилище);
		КонецЕсли;
	КонецЕсли;

	// СтандартныеПодсистемы.Свойства
	Если УправлениеСвойствамиКлиент.ОбрабатыватьОповещения(ЭтотОбъект, ИмяСобытия, Параметр) Тогда
		ОбновитьЭлементыДополнительныхРеквизитов();
		УправлениеСвойствамиКлиент.ПослеЗагрузкиДополнительныхРеквизитов(ЭтотОбъект);
	КонецЕсли;
	// Конец СтандартныеПодсистемы.Свойства	
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	// СтандартныеПодсистемы.ПодключаемыеКоманды
	ПодключаемыеКомандыКлиент.НачатьОбновлениеКоманд(ЭтотОбъект);
	// Конец СтандартныеПодсистемы.ПодключаемыеКоманды
	
	// СтандартныеПодсистемы.Свойства
	УправлениеСвойствамиКлиент.ПослеЗагрузкиДополнительныхРеквизитов(ЭтотОбъект);
	// Конец СтандартныеПодсистемы.Свойства
	
КонецПроцедуры

&НаСервере
Процедура ОбработкаПроверкиЗаполненияНаСервере(Отказ, ПроверяемыеРеквизиты)
	
	// СтандартныеПодсистемы.Свойства
	УправлениеСвойствами.ОбработкаПроверкиЗаполнения(ЭтотОбъект, Отказ, ПроверяемыеРеквизиты);
	// Конец СтандартныеПодсистемы.Свойства
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура Подключаемый_ОткрытьДокументыВведенныеПозже(Элемент, НавигационнаяСсылка, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	ЗарплатаКадрыРасширенныйКлиент.ОткрытьВведенныеНаДатуДокументы(ЭтотОбъект.ДокументыВведенныеПозже);
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ОткрытьРанееВведенныеДокументы(Элемент, НавигационнаяСсылка, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	ЗарплатаКадрыРасширенныйКлиент.ОткрытьВведенныеНаДатуДокументы(ЭтотОбъект.РанееВведенныеДокументы);
	
КонецПроцедуры

&НаКлиенте
Процедура ОрганизацияПриИзменении(Элемент)
	ОрганизацияПриИзмененииНаСервере();
КонецПроцедуры

&НаКлиенте
Процедура ГрафикРаботыПриИзменении(Элемент)
	ГрафикРаботыПриИзмененииНаСервере();
КонецПроцедуры

&НаКлиенте
Процедура РуководительПриИзменении(Элемент)
	
	НастроитьОтображениеГруппыПодписантов();
	
КонецПроцедуры

&НаКлиенте
Процедура ГлавныйБухгалтерПриИзменении(Элемент)
	
	НастроитьОтображениеГруппыПодписантов();
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыПоказателиСотрудников

&НаКлиенте
Процедура СотрудникиВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	
	ТекущиеДанные = Элемент.ТекущиеДанные;
	Если ЗначениеЗаполнено(ТекущиеДанные.Сотрудник) Тогда
		
		Если Поле.Имя = "ПоказателиСотрудниковФОТ" Тогда
			
			ПараметрыОткрытия = Новый Структура;
			ПараметрыОткрытия.Вставить("АдресВХранилище", АдресВХранилищеНачисленийИУдержаний(ТекущиеДанные.Сотрудник));
			ПараметрыОткрытия.Вставить("ТолькоПросмотр", ТолькоПросмотр);
			
			ЗарплатаКадрыРасширенныйКлиент.ОткрытьФормуРедактированияСоставаНачисленийИУдержаний(ПараметрыОткрытия, ЭтаФорма);
			
		КонецЕсли;
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПоказателиСотрудниковПриАктивизацииСтроки(Элемент)
	
	Если НЕ Элементы.ПоказателиСотрудников.ТекущиеДанные = Неопределено Тогда
		ТекущийСотрудник = Элементы.ПоказателиСотрудников.ТекущиеДанные.Сотрудник;
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПоказателиСотрудниковОбработкаВыбора(Элемент, ВыбранноеЗначение, СтандартнаяОбработка)
	ДополнитьПоказателиСотрудников(ВыбранноеЗначение);
КонецПроцедуры

&НаКлиенте
Процедура ПоказателиСотрудниковСотрудникПриИзменении(Элемент)
	ПоказателиСотрудниковСотрудникПриИзмененииНаСервере(Элементы.ПоказателиСотрудников.ТекущаяСтрока);
КонецПроцедуры

&НаКлиенте
Процедура ПоказателиСотрудниковПриОкончанииРедактирования(Элемент, НоваяСтрока, ОтменаРедактирования)
	
	Если ОтменаРедактирования Тогда
		Возврат;
	КонецЕсли;
	
	Если НЕ Элементы.ПоказателиСотрудников.ТекущиеДанные = Неопределено Тогда
		ТекущийСотрудник = Элементы.ПоказателиСотрудников.ТекущиеДанные.Сотрудник;
	КонецЕсли;
	
	КлючевыеРеквизитыЗаполненияФормыУстановитьОтображениеПредупреждения();
	
КонецПроцедуры

&НаКлиенте
Процедура ПоказателиСотрудниковПередУдалением(Элемент, Отказ)
	
	ТекущиеДанные = ПоказателиСотрудников.НайтиПоИдентификатору(Элементы.ПоказателиСотрудников.ТекущаяСтрока);
	Если ТекущиеДанные.Сотрудник = ТекущийСотрудник Тогда
		УдалитьНачисленияСотрудников(ТекущийСотрудник);
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

// СтандартныеПодсистемы.Свойства
&НаКлиенте
Процедура Подключаемый_СвойстваВыполнитьКоманду(ЭлементИлиКоманда, НавигационнаяСсылка = Неопределено, СтандартнаяОбработка = Неопределено)
	УправлениеСвойствамиКлиент.ВыполнитьКоманду(ЭтотОбъект, ЭлементИлиКоманда, СтандартнаяОбработка);
КонецПроцедуры

// СтандартныеПодсистемы.ПодключаемыеКоманды
&НаКлиенте
Процедура Подключаемый_ВыполнитьКоманду(Команда)
	ОписаниеОповещения = Новый ОписаниеОповещения("ВыполнитьПодключаемуюКомандуПослеПодтвержденияЗаписи", ЭтотОбъект, Команда);
	ЗарплатаКадрыРасширенныйКлиент.ПоказатьДиалогЗаписиОбъектаДляВыполненияПодключаемойКоманды(ЭтотОбъект, Команда, ОписаниеОповещения);
КонецПроцедуры

&НаСервере
Процедура Подключаемый_ВыполнитьКомандуНаСервере(Контекст, Результат) Экспорт
	ПодключаемыеКоманды.ВыполнитьКоманду(ЭтотОбъект, Контекст, Объект, Результат);
КонецПроцедуры
// Конец СтандартныеПодсистемы.ПодключаемыеКоманды

&НаКлиенте
Процедура Подключаемый_ОбновитьКоманды()
	ПодключаемыеКомандыКлиентСервер.ОбновитьКоманды(ЭтотОбъект, Объект);
КонецПроцедуры
// Конец СтандартныеПодсистемы.ПодключаемыеКоманды

// ИсправлениеДокументов
&НаКлиенте
Процедура Подключаемый_Исправить(Команда)
	ИсправлениеДокументовЗарплатаКадрыКлиент.Исправить(Объект.Ссылка, "ИзменениеГрафикаРаботыСписком");
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ПерейтиКИсправлению(Команда)
	ИсправлениеДокументовЗарплатаКадрыКлиент.ПерейтиКИсправлению(ЭтаФорма.ДокументИсправление, "ИзменениеГрафикаРаботыСписком");
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ПерейтиКИсправленному(Команда)
	ИсправлениеДокументовЗарплатаКадрыКлиент.ПерейтиКИсправленному(Объект.ИсправленныйДокумент, "ИзменениеГрафикаРаботыСписком");
КонецПроцедуры
// Конец ИсправлениеДокументов

&НаКлиенте
Процедура ЗаполнитьПодходящихСотрудников(Команда)
	
	Если ПоказателиСотрудников.Количество() > 0 Тогда
		Оповещение = Новый ОписаниеОповещения("ЗаполнитьПодходящихСотрудниковЗавершение", ЭтотОбъект);
		ПоказатьВопрос(Оповещение, НСтр("ru = 'Табличная часть будет очищена, продолжить?'"), РежимДиалогаВопрос.ДаНет);
	Иначе
		ЗаполнитьПодходящихСотрудниковЗавершение(КодВозвратаДиалога.Да, Неопределено);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура КомандаПровестиИЗакрыть(Команда)
	
	ПараметрыЗаписи = Новый Структура("РежимЗаписи", РежимЗаписиДокумента.Проведение);
	ЗаписатьНаКлиенте(Истина, ПараметрыЗаписи);
	
КонецПроцедуры

&НаКлиенте
Процедура КомандаПровести(Команда)
	
	ПараметрыЗаписи = Новый Структура("РежимЗаписи", РежимЗаписиДокумента.Проведение);
	ЗаписатьНаКлиенте(Ложь, ПараметрыЗаписи);
	
КонецПроцедуры

&НаКлиенте
Процедура КомандаЗаписать(Команда)
	
	ПараметрыЗаписи = Новый Структура("РежимЗаписи", ?(Объект.Проведен, РежимЗаписиДокумента.Проведение, РежимЗаписиДокумента.Запись));
	ЗаписатьНаКлиенте(Ложь, ПараметрыЗаписи);
	
КонецПроцедуры

&НаКлиенте
Процедура ПодборСотрудников(Команда)
	
	КадровыйУчетКлиент.ВыбратьСотрудниковРаботающихВПериоде(
		Элементы.ПоказателиСотрудников,
		Объект.Организация,
		Объект.Подразделение,
		Объект.ДатаИзменения,
		КонецМесяца(Объект.ДатаИзменения),
		, АдресСпискаПодобранныхСотрудников());
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

// СтандартныеПодсистемы.Свойства 
&НаСервере
Процедура ОбновитьЭлементыДополнительныхРеквизитов()
	УправлениеСвойствами.ОбновитьЭлементыДополнительныхРеквизитов(ЭтотОбъект);
КонецПроцедуры

&НаКлиенте
Процедура ОбновитьЗависимостиДополнительныхРеквизитов()
	УправлениеСвойствамиКлиент.ОбновитьЗависимостиДополнительныхРеквизитов(ЭтотОбъект);
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ПриИзмененииДополнительногоРеквизита(Элемент)
	УправлениеСвойствамиКлиент.ОбновитьЗависимостиДополнительныхРеквизитов(ЭтотОбъект);
КонецПроцедуры
// Конец СтандартныеПодсистемы.Свойства

&НаСервере
Процедура ПриПолученииДанныхНаСервере()
	
	ПрочитатьВремяРегистрации();
	ДанныеВРеквизит();
	
	ЗарплатаКадры.КлючевыеРеквизитыЗаполненияФормыЗаполнитьПредупреждения(ЭтаФорма, Неопределено, Ложь);
	КлючевыеРеквизитыЗаполненияФормыУстановитьОтображениеПредупреждения();
	НастроитьОтображениеГруппыПодписантов();
	
КонецПроцедуры

&НаСервере
Процедура ДанныеВРеквизит()
	
	МассивСотрудников = Объект.Сотрудники.Выгрузить(,"Сотрудник").ВыгрузитьКолонку("Сотрудник");
	
	ДополнитьФорму();
	ПоказателиСотрудников.Очистить();
	ПоказателиСотрудниковВРеквизитФормы(Объект.Сотрудники);
	ЗаполнитьФОТПоСотрудникам();
	
	ТекущиеЗначенияСовокупныхТарифныхСтавок = Документы.ИзменениеГрафикаРаботыСписком.ТекущиеЗначенияСовокупныхТарифныхСтавокСотрудников(
		Объект.Ссылка, ВремяРегистрации, МассивСотрудников);
	
	ЗначенияСовокупныхТарифныхСтавокВРеквизитФормы(Объект.ПересчетТарифныхСтавок, ТекущиеЗначенияСовокупныхТарифныхСтавок);
	
КонецПроцедуры

// Процедура заполняет таблицу формы в которой редактируются список сотрудников.
// Данные для заполнения берутся из Объект.Сотрудники
&НаСервере
Процедура ПоказателиСотрудниковВРеквизитФормы(Сотрудники)
	
	Если ТипЗнч(Сотрудники) = Тип("ТаблицаЗначений") Тогда
		МассивСотрудников = Сотрудники.Скопировать(, "Сотрудник").ВыгрузитьКолонку("Сотрудник");
	Иначе
		МассивСотрудников = Сотрудники.Выгрузить().ВыгрузитьКолонку("Сотрудник");
	КонецЕсли;
	
	Отбор = Новый Структура;
	КадровыеДанные = КадровыйУчет.КадровыеДанныеСотрудников(Истина, МассивСотрудников, "ДолжностьПоШтатномуРасписанию", ВремяРегистрации, , Ложь);
	Для каждого СтрокаСотрудника Из Сотрудники Цикл
		
		Отбор.Очистить();
		Отбор.Вставить("Сотрудник", СтрокаСотрудника.Сотрудник);
		Строки = ЭтаФорма["ПоказателиСотрудников"].НайтиСтроки(Отбор);
		Если Строки.Количество() > 0 Тогда
			Строка = Строки[0];
		Иначе
			Строка = ЭтаФорма["ПоказателиСотрудников"].Добавить();
			Строка["Сотрудник"] = СтрокаСотрудника.Сотрудник;
			Строка["ФиксСтрока"] = СтрокаСотрудника.ФиксСтрока;
		КонецЕсли;
		
		СтрокиДанных = КадровыеДанные.НайтиСтроки(Новый Структура("Сотрудник", СтрокаСотрудника.Сотрудник));
		Если СтрокиДанных.Количество() > 0 Тогда
			Строка.ДолжностьПоШтатномуРасписанию = СтрокиДанных[0].ДолжностьПоШтатномуРасписанию;
		КонецЕсли;
		
	КонецЦикла;
	
КонецПроцедуры

&НаСервере
Процедура ЗначенияСовокупныхТарифныхСтавокВРеквизитФормы(ЗначенияСовокупныхТарифныхСтавок, ТекущиеЗначенияСовокупныхТарифныхСтавок);
	
	Отбор = Новый Структура;
	
	Для Каждого ДанныеСовокупныхТарифныхСтавок Из ЗначенияСовокупныхТарифныхСтавок Цикл 
		
		Отбор.Вставить("Сотрудник", ДанныеСовокупныхТарифныхСтавок.Сотрудник);
		ДанныеСотрудника = ЭтаФорма["ПоказателиСотрудников"].НайтиСтроки(Отбор);
		
		Если ДанныеСотрудника.Количество() > 0 Тогда 
			
			ДанныеСотрудника[0].СовокупнаяТарифнаяСтавка = ДанныеСовокупныхТарифныхСтавок.СовокупнаяТарифнаяСтавка;
			ДанныеСотрудника[0].ВидТарифнойСтавки = ДанныеСовокупныхТарифныхСтавок.ВидТарифнойСтавки;
			
			ТекущиеДанныеСовокупныхТарифныхСтавок = ТекущиеЗначенияСовокупныхТарифныхСтавок.НайтиСтроки(Отбор);
			
			Если ТекущиеДанныеСовокупныхТарифныхСтавок.Количество() > 0 Тогда 
				ДанныеСотрудника[0].СовокупнаяТарифнаяСтавкаТекущееЗначение = ТекущиеДанныеСовокупныхТарифныхСтавок[0].СовокупнаяТарифнаяСтавка;
				
				СуммаПодстановки = Строка(Формат(ТекущиеДанныеСовокупныхТарифныхСтавок[0].СовокупнаяТарифнаяСтавка, "ЧДЦ=2; ЧРГ="));
				СуммаПодстановки = СтроковыеФункцииКлиентСервер.ДополнитьСтроку(СуммаПодстановки, 10, " ");
				
				ПредставлениеТекущего = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(НСтр("ru = 'было: %1'"), СуммаПодстановки);
				ДанныеСотрудника[0].СовокупнаяТарифнаяСтавкаТекущееЗначениеПредставление = ПредставлениеТекущего;
			КонецЕсли;
		
		КонецЕсли;
		
	КонецЦикла;
	
	ЗначенияСовокупныхТарифныхСтавок.Очистить();
	
КонецПроцедуры

// Процедура переносит отредактированный пользователем список сотрудников в Объект.Сотрудники.
&НаСервере
Процедура РеквизитВДанные(ТекущийОбъект)
	
	ТекущийОбъект.Сотрудники.Очистить();
	ТекущийОбъект.ПересчетТарифныхСтавок.Очистить();
	
	Для каждого ПоказателиСотрудника Из ЭтаФорма["ПоказателиСотрудников"] Цикл
		Строка = ТекущийОбъект.Сотрудники.Добавить();
		Строка.Сотрудник = ПоказателиСотрудника.Сотрудник;
		Строка.ФиксСтрока = ПоказателиСотрудника.ФиксСтрока;
		
		// И значения совокупных тарифных ставок.
		Строка = ТекущийОбъект.ПересчетТарифныхСтавок.Добавить();
		Строка.Сотрудник = ПоказателиСотрудника.Сотрудник;
		Строка.СовокупнаяТарифнаяСтавка = ПоказателиСотрудника.СовокупнаяТарифнаяСтавка;
		Строка.ВидТарифнойСтавки = ПоказателиСотрудника.ВидТарифнойСтавки;
	КонецЦикла;
	
КонецПроцедуры

&НаКлиенте
Процедура ЗаполнитьПодходящихСотрудниковЗавершение(Ответ, ДополнительныеПараметры) Экспорт 
	
	Если Ответ <> КодВозвратаДиалога.Да Тогда
		Возврат;
	КонецЕсли;
	
	ЗаполнитьПодходящихСотрудниковНаСервере();
	
КонецПроцедуры

// Процедура заполняет таблицы документа сотрудниками
// Также выполняются все сопутствующие действия: расчет ФОТ и т.п.
&НаСервере
Процедура ЗаполнитьПодходящихСотрудниковНаСервере(ВыводитьСообщения = Истина)
	
	Если НЕ ПроверитьЗаполнение() Тогда
		ПоказателиСотрудников.Очистить();
		ПолучитьСообщенияПользователю(?(ВыводитьСообщения, Ложь, Истина));
		Возврат
	КонецЕсли;
	
	ДокументОбъект = РеквизитФормыВЗначение("Объект");
	ДокументОбъект.ЗаполнитьДокумент(ВремяРегистрации);
	ЗначениеВРеквизитФормы(ДокументОбъект, "Объект");
	
	ДанныеВРеквизит();
	
	КлючевыеРеквизитыЗаполненияФормыУстановитьОтображениеПредупреждения();
	
КонецПроцедуры

#Область ФОТ

&НаСервере
Процедура ЗаполнитьФОТПоСотрудникам()
	
	Для каждого СтрокаСотрудника Из ЭтаФорма["ПоказателиСотрудников"] Цикл
		СтрокаСотрудника.ФОТ = ФОТСотрудника(СтрокаСотрудника.Сотрудник);
	КонецЦикла;
	
КонецПроцедуры

&НаСервере
Функция ФОТСотрудника(Сотрудник)
	
	НачисленияСотрудника = Объект.НачисленияСотрудников.Выгрузить(Новый Структура("Сотрудник", Сотрудник));
	
	// 4D:ERP для Беларуси Екатерина, 17.09.2021 10:54:00 
	// №29496 
	// {
	ФОТСотрудника = 0;
	Для Каждого СтрокаНачисления Из НачисленияСотрудника Цикл 
		Если СтрокаНачисления.Начисление.ВключатьВФОТ Тогда 
			ФОТСотрудника = ФОТСотрудника + СтрокаНачисления.Размер;	
		КонецЕсли;
	КонецЦикла;	
	
	Возврат ФОТСотрудника;
	// }
	// 4D
		
КонецФункции

&НаСервере
Процедура ПересчитатьФОТИСовокупныеТарифныеСтавки()
	
	Менеджер = Документы.ИзменениеГрафикаРаботыСписком;
	
	РеквизитВДанные(Объект);
	
	ПересчитываемыеНачисления = Объект.НачисленияСотрудников;
	
	Менеджер.РассчитатьФОТ(Объект.Ссылка, Объект.Организация, ВремяРегистрации, Объект.ГрафикРаботы, ПересчитываемыеНачисления);
	
	ЗаполнитьФОТПоСотрудникам();
	
	ЗначенияСовокупныхТарифныхСтавок = Документы.ИзменениеГрафикаРаботыСписком.ЗначенияСовокупныхТарифныхСтавокСотрудников(
		Объект.НачисленияСотрудников, ВремяРегистрации, Объект.ГрафикРаботы);
	
	Для Каждого ПоказателиСотрудника Из ЭтаФорма["ПоказателиСотрудников"] Цикл
		ДанныеСовокупныхТарифныхСтавок = ЗначенияСовокупныхТарифныхСтавок.Найти(ПоказателиСотрудника.Сотрудник, "Сотрудник");
		ПоказателиСотрудника.СовокупнаяТарифнаяСтавка = ?(ДанныеСовокупныхТарифныхСтавок <> Неопределено, ДанныеСовокупныхТарифныхСтавок.СовокупнаяТарифнаяСтавка, 0);
		ПоказателиСотрудника.ВидТарифнойСтавки = ?(ДанныеСовокупныхТарифныхСтавок <> Неопределено, ДанныеСовокупныхТарифныхСтавок.ВидТарифнойСтавки, Неопределено);
	КонецЦикла;
	
КонецПроцедуры

&НаСервере
Функция АдресВХранилищеНачисленийИУдержаний(Сотрудник)
	
	ПараметрыОткрытия = ЗарплатаКадрыРасширенныйКлиентСервер.ПараметрыРедактированияСоставаНачисленийИУдержаний();
	
	ПараметрыОткрытия.ВладелецНачисленийИУдержаний = Сотрудник;
	ПараметрыОткрытия.ДатаРедактирования = ВремяРегистрации;
	ПараметрыОткрытия.Организация = Объект.Организация;
	ПараметрыОткрытия.ГрафикРаботы = Объект.ГрафикРаботы;
	ПараметрыОткрытия.РежимРаботы = 3;
	ПараметрыОткрытия.ДополнитьНедостающиеЗначенияПоказателей = Истина;
	
	КадровыеДанные = КадровыйУчет.КадровыеДанныеСотрудников(Истина, Сотрудник, "Подразделение", ВремяРегистрации);
	Если КадровыеДанные.Количество() > 0 Тогда
		ПараметрыОткрытия.Подразделение = КадровыеДанные[0].Подразделение;
	КонецЕсли;
	
	МассивНачислений = Новый Массив;
	
	ТаблицаСотрудников = Новый ТаблицаЗначений;
	ТаблицаСотрудников.Колонки.Добавить("Период", Новый ОписаниеТипов("Дата"));
	ТаблицаСотрудников.Колонки.Добавить("Организация", Новый ОписаниеТипов("СправочникСсылка.Организации"));
	ТаблицаСотрудников.Колонки.Добавить("Сотрудник", Новый ОписаниеТипов("СправочникСсылка.Сотрудники"));
	
	СтрокаТаблицыСотрудников = ТаблицаСотрудников.Добавить();
	СтрокаТаблицыСотрудников.Период = ВремяРегистрации;
	СтрокаТаблицыСотрудников.Организация = Объект.Организация;
	СтрокаТаблицыСотрудников.Сотрудник = Сотрудник;
	
	СтрокиНачислений = Объект.НачисленияСотрудников.НайтиСтроки(Новый Структура("Сотрудник", Сотрудник));
	Для каждого СтрокаНачислений Из СтрокиНачислений Цикл
		
		СтруктураНачисления = Новый Структура("Начисление, Размер");
		ЗаполнитьЗначенияСвойств(СтруктураНачисления, СтрокаНачислений);
		МассивНачислений.Добавить(СтруктураНачисления);
		
	КонецЦикла;
	
	ПараметрыОткрытия.ОписаниеТаблицыНачислений.Используется = Истина;
	ПараметрыОткрытия.ОписаниеТаблицыНачислений.Таблица = МассивНачислений;
	ПараметрыОткрытия.ОписаниеТаблицыНачислений.ИзменятьСоставВидовРасчета = Ложь;
	ПараметрыОткрытия.ОписаниеТаблицыНачислений.ИзменятьЗначенияПоказателей = Ложь;
	ПараметрыОткрытия.ОписаниеТаблицыНачислений.НомерТаблицы = 1;
	ПараметрыОткрытия.ОписаниеТаблицыНачислений.ПоказатьФОТ = Истина;
	
	Возврат ПоместитьВоВременноеХранилище(ПараметрыОткрытия, УникальныйИдентификатор);
	
КонецФункции

&НаСервере
Процедура ЗаполнитьНачисленияИзВРеменногоХранилища(АдресВХранилище);
	
	ДанныеИзХранилища = ПолучитьИзВременногоХранилища(АдресВХранилище);
	Сотрудник = ДанныеИзХранилища.ВладелецНачисленийИУдержаний;
	
	УдалитьНачисленияСотрудников(Сотрудник, Ложь, Ложь);
	ФОТИзменен = Ложь;
	НачисленияСотрудников = Новый Массив;
	Если ДанныеИзХранилища <> Неопределено Тогда
		Для каждого НачислениеСотрудника Из ДанныеИзХранилища.Начисления Цикл
			НоваяСтрокаНачислений = Объект.НачисленияСотрудников.Добавить();
			ЗаполнитьЗначенияСвойств(НоваяСтрокаНачислений, НачислениеСотрудника);
			НоваяСтрокаНачислений.Сотрудник = Сотрудник;
			НачисленияСотрудников.Добавить(НоваяСтрокаНачислений);
		КонецЦикла;
		ФОТИзменен = ДанныеИзХранилища.Модифицированность;
	КонецЕсли;
	
	НайденныеСтроки = ЭтаФорма["ПоказателиСотрудников"].НайтиСтроки(Новый Структура("Сотрудник", Сотрудник));
	Если НайденныеСтроки.Количество() > 0 Тогда
		НайденныеСтроки[0].ФОТ = ФОТСотрудника(Сотрудник);
		Если ФОТИзменен Тогда
			НайденныеСтроки[0].ФиксСтрока = Истина;
		КонецЕсли;
		КлючевыеРеквизитыЗаполненияФормыУстановитьОтображениеПредупреждения();
	КонецЕсли;
	
	ЗначенияСовокупныхТарифныхСтавок = Документы.ИзменениеГрафикаРаботыСписком.ЗначенияСовокупныхТарифныхСтавокСотрудников(
		НачисленияСотрудников, ВремяРегистрации, Объект.ГрафикРаботы);
	КоличествоСтрок = ЗначенияСовокупныхТарифныхСтавок.Количество();
	Если НайденныеСтроки.Количество() > 0 Тогда
		НайденныеСтроки[0].СовокупнаяТарифнаяСтавка = ?(КоличествоСтрок > 0, ЗначенияСовокупныхТарифныхСтавок[0].СовокупнаяТарифнаяСтавка, 0);
		НайденныеСтроки[0].ВидТарифнойСтавки = ?(КоличествоСтрок > 0, ЗначенияСовокупныхТарифныхСтавок[0].ВидТарифнойСтавки, Неопределено);
	КонецЕсли;
	
	Модифицированность = ДанныеИзХранилища.Модифицированность;
	
КонецПроцедуры

#КонецОбласти

#Область Отрисовка_формы

&НаСервере
Процедура ДополнитьФорму()
	
	Если ПолучитьФункциональнуюОпцию("ИспользоватьШтатноеРасписание") Тогда
		ГруппаСотрудник = ЭтаФорма.Элементы.Найти("ГруппаСотрудник");
		ГруппаСотрудник.Заголовок = "Сотрудник / Должность";
	КонецЕсли;
	
	ЗарплатаКадрыРасширенный.ОформлениеНесколькихДокументовНаОднуДатуДополнитьФорму(ЭтотОбъект);
	
	ИсправлениеДокументовЗарплатаКадры.ГруппаИсправлениеДополнитьФорму(ЭтаФорма, Истина, Ложь);
	Если Не ЭтаФорма.Параметры.Ключ.Пустая() Тогда
		ИсправлениеДокументовЗарплатаКадры.ПрочитатьРеквизитыИсправления(ЭтаФорма, "ПериодическиеСведения");
	КонецЕсли;
	ИсправлениеДокументовЗарплатаКадры.УстановитьПоляИсправления(ЭтаФорма, "ПериодическиеСведения");
	
КонецПроцедуры

#КонецОбласти

#Область Серверные_обработчики_событий_формы

&НаСервере
Процедура ОрганизацияПриИзмененииНаСервере()
	
	Если НЕ ЗначениеЗаполнено(Объект.Организация) Тогда
		Возврат;
	КонецЕсли;
	
	ЗапрашиваемыеЗначения = Новый Структура;
	ЗапрашиваемыеЗначения.Вставить("Организация", "Объект.Организация");
	
	ЗапрашиваемыеЗначения.Вставить("Руководитель", "Объект.Руководитель");
	ЗапрашиваемыеЗначения.Вставить("ДолжностьРуководителя", "Объект.ДолжностьРуководителя");
	
	ЗапрашиваемыеЗначения.Вставить("ГлавныйБухгалтер", "Объект.ГлавныйБухгалтер");
	
	ЗарплатаКадры.ЗаполнитьЗначенияВФорме(ЭтаФорма, ЗапрашиваемыеЗначения, ОбщегоНазначенияКлиентСервер.ЗначениеВМассиве("Организация"));
	НастроитьОтображениеГруппыПодписантов();
	
	ЗаполнитьПодходящихСотрудниковНаСервере(Ложь);
	
КонецПроцедуры

&НаСервере
Процедура НастроитьОтображениеГруппыПодписантов()
	ЗарплатаКадры.НастроитьОтображениеГруппыПодписей(Элементы.ПодписиГруппа, "Объект.Руководитель", "Объект.ГлавныйБухгалтер");
КонецПроцедуры

&НаКлиенте
Процедура ДатаНачалаПриИзменении()
	ДатаНачалаПриИзмененииНаСервере();
КонецПроцедуры

&НаСервере
Процедура ДатаНачалаПриИзмененииНаСервере()
	
	ПрочитатьВремяРегистрации();
	УстановитьОтображениеНадписей();
	
КонецПроцедуры

&НаСервере
Процедура ГрафикРаботыПриИзмененииНаСервере()
	ПересчитатьФОТИСовокупныеТарифныеСтавки();
КонецПроцедуры

&НаСервере
Процедура УдалитьНачисленияСотрудников(Сотрудник, УдалитьСотрудников = Истина, УдалитьТарифныеСтавки = Истина)
	
	СтрокиНачислений = Объект.НачисленияСотрудников.НайтиСтроки(Новый Структура("Сотрудник", Сотрудник));
	Для каждого СтрокаНачислений Из СтрокиНачислений Цикл
		Объект.НачисленияСотрудников.Удалить(СтрокаНачислений);
	КонецЦикла;
	
	Если УдалитьСотрудников Тогда
		СтрокиНачислений = Объект.Сотрудники.НайтиСтроки(Новый Структура("Сотрудник", Сотрудник));
		Для каждого СтрокаНачислений Из СтрокиНачислений Цикл
			Объект.Сотрудники.Удалить(СтрокаНачислений);
		КонецЦикла;
	КонецЕсли;
	
	Если УдалитьТарифныеСтавки Тогда
		СтрокиПересчета = Объект.ПересчетТарифныхСтавок.НайтиСтроки(Новый Структура("Сотрудник", Сотрудник));
		Для каждого СтрокаПересчета Из СтрокиПересчета Цикл
			Объект.ПересчетТарифныхСтавок.Удалить(СтрокаПересчета);
		КонецЦикла;
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ПоказателиСотрудниковСотрудникПриИзмененииНаСервере(Идентификатор)
	
	ТекущиеДанные = ПоказателиСотрудников.НайтиПоИдентификатору(Идентификатор);
	НовыйСотрудник = ТекущиеДанные.Сотрудник;
	Если НовыйСотрудник <> ТекущийСотрудник Тогда
		УдалитьНачисленияСотрудников(ТекущийСотрудник);
	КонецЕсли;
	
	Если Не ЗначениеЗаполнено(НовыйСотрудник) Тогда
		Возврат;
	КонецЕсли;
	
	ДополнитьПоказателиСотрудников(ОбщегоНазначенияКлиентСервер.ЗначениеВМассиве(НовыйСотрудник));
	
КонецПроцедуры

#КонецОбласти

#Область КлючевыеРеквизитыЗаполненияФормы

// Функция возвращает описание таблиц формы подключенных к механизму ключевых реквизитов формы.
&НаСервере
Функция КлючевыеРеквизитыЗаполненияФормыТаблицыОчищаемыеПриИзменении() Экспорт
	Возврат Новый Массив;
КонецФункции

// Функция возвращает массив реквизитов формы подключенных к механизму ключевых реквизитов формы.
&НаСервере
Функция КлючевыеРеквизитыЗаполненияФормыОписаниеКлючевыхРеквизитов() Экспорт
	
	Массив = Новый Массив;
	
	Массив.Добавить(Новый Структура("ЭлементФормы", "Организация"));
	Массив.Добавить(Новый Структура("ЭлементФормы", "Подразделение"));
	Массив.Добавить(Новый Структура("ЭлементФормы", "ГрафикРаботы"));
	Массив.Добавить(Новый Структура("ЭлементФормы", "ДатаИзменения"));
	
	Для каждого ОписаниеЭлемента Из Массив Цикл
		ОписаниеЭлемента.Вставить("ПредупреждениеПриРедактировании", ЗарплатаКадрыРасширенный.КлючевыеРеквизитыЗаполненияФормыТекстПредупрежденияДокументовСАвтоматическимРасчетом());
	КонецЦикла;
	
	Возврат Массив;
	
КонецФункции

&НаСервере
Функция КлючевыеРеквизитыЗаполненияФормыУстановитьОтображениеПредупреждения()
	
	ЗарплатаКадрыКлиентСервер.КлючевыеРеквизитыЗаполненияФормыУстановитьОтображениеПредупреждения(ЭтотОбъект, ?(ЕстьФиксированныеДанные(), ОтображениеПредупрежденияПриРедактировании.Отображать, ОтображениеПредупрежденияПриРедактировании.Авто));
	УстановитьОтображениеНадписей();
	
КонецФункции

&НаСервере
Функция ЕстьФиксированныеДанные()
	Возврат РасчетЗарплатыРасширенныйКлиентСервер.ЕстьФиксированныеДанныеВТаблице(ЭтотОбъект, ОписаниеТаблицыПоказателиСотрудников());
КонецФункции

// Контролируемые поля
&НаСервере
Функция ПолучитьКонтролируемыеПоля() Экспорт
	
	КонтролируемыеПоля = Новый Структура("ПоказателиСотрудников", Новый Структура("ФиксСтрока"));
	Возврат КонтролируемыеПоля;
	
КонецФункции

// Контролируемые поля
&НаСервере
Функция ОписаниеТаблицыПоказателиСотрудников() Экспорт
	
	ОписаниеТаблицыПоказателиСотрудников = Новый Структура;	
	ОписаниеТаблицыПоказателиСотрудников.Вставить("ИмяТаблицы", 	"ПоказателиСотрудников");
	ОписаниеТаблицыПоказателиСотрудников.Вставить("ПутьКДанным", 	"ПоказателиСотрудников");
	ОписаниеТаблицыПоказателиСотрудников.Вставить("ЭтоПерерасчеты", Ложь);
	
	Возврат ОписаниеТаблицыПоказателиСотрудников;
	
КонецФункции

#КонецОбласти

&НаСервере
Процедура ПрочитатьВремяРегистрации()
	ВремяРегистрации = ЗарплатаКадрыРасширенный.ВремяРегистрацииДокумента(Объект.Ссылка, Объект.ДатаИзменения);
КонецПроцедуры

&НаСервере
Процедура УстановитьОтображениеНадписей()
	
	УстановитьПривилегированныйРежим(Истина);
	МассивСотрудников = ОбщегоНазначения.ВыгрузитьКолонку(ЭтотОбъект.ПоказателиСотрудников, "Сотрудник", Истина);
	ЗарплатаКадрыРасширенный.УстановитьТекстНадписиОДокументахВведенныхНаДату(ЭтотОбъект, ВремяРегистрации, 
		МассивСотрудников, Объект.Ссылка);
	
КонецПроцедуры

#Область ЗаписьДокумента

&НаКлиенте
Процедура ЗаписатьИЗакрытьНаКлиенте(Результат, ДополнительныеПараметры) Экспорт 
	
	ПараметрыЗаписи = Новый Структура("РежимЗаписи", ?(Объект.Проведен, РежимЗаписиДокумента.Проведение, РежимЗаписиДокумента.Запись));
	ЗаписатьНаКлиенте(Истина, ПараметрыЗаписи);
	
КонецПроцедуры

&НаКлиенте
Процедура ЗаписатьНаКлиенте(ЗакрытьПослеЗаписи, ПараметрыЗаписи, ОповещениеЗавершения = Неопределено) Экспорт
	
	ПараметрыЗаписи.Вставить("ПроверкаПередЗаписьюВыполнена", Истина);
	
	Если ОповещениеЗавершения <> Неопределено Тогда
		ВыполнитьОбработкуОповещения(ОповещениеЗавершения, ПараметрыЗаписи);
	ИначеЕсли Записать(ПараметрыЗаписи) И ЗакрытьПослеЗаписи Тогда 
		Закрыть();
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

&НаСервере
Функция АдресСпискаПодобранныхСотрудников()
	Возврат ПоместитьВоВременноеХранилище(ПоказателиСотрудников.Выгрузить(,"Сотрудник").ВыгрузитьКолонку("Сотрудник"), УникальныйИдентификатор);
КонецФункции

&НаСервере
Процедура ДополнитьПоказателиСотрудников(МассивСотрудников)
	
	ТаблицаНачисленийСотрудников = Документы.ИзменениеГрафикаРаботыСписком.НачисленияСотрудников(
		Объект.Ссылка, ВремяРегистрации, МассивСотрудников);
	
	Менеджер = Документы.ИзменениеГрафикаРаботыСписком;
	Менеджер.РассчитатьФОТ(Объект.Ссылка, Объект.Организация, ВремяРегистрации, Объект.ГрафикРаботы, ТаблицаНачисленийСотрудников);
	
	Для каждого СтрокаНачисленияСотрудников Из ТаблицаНачисленийСотрудников Цикл
		НоваяСтрока = Объект.НачисленияСотрудников.Добавить();
		ЗаполнитьЗначенияСвойств(НоваяСтрока, СтрокаНачисленияСотрудников);
	КонецЦикла;
	
	ТаблицаНачисленийСотрудниковВрем = ТаблицаНачисленийСотрудников.Скопировать();
	ТаблицаНачисленийСотрудниковВрем.Свернуть("Сотрудник, ФиксСтрока");
	Для каждого СтрокаНачисленияСотрудников Из ТаблицаНачисленийСотрудниковВрем Цикл
		НоваяСтрока = Объект.Сотрудники.Добавить();
		ЗаполнитьЗначенияСвойств(НоваяСтрока, СтрокаНачисленияСотрудников);
	КонецЦикла;
	
	ЗначенияСовокупныхТарифныхСтавок = Документы.ИзменениеГрафикаРаботыСписком.ЗначенияСовокупныхТарифныхСтавокСотрудников(
		ТаблицаНачисленийСотрудников, ВремяРегистрации, Объект.ГрафикРаботы);
	
	ТекущиеЗначенияСовокупныхТарифныхСтавок = Документы.ИзменениеГрафикаРаботыСписком.ТекущиеЗначенияСовокупныхТарифныхСтавокСотрудников(
		Объект.Ссылка, ВремяРегистрации, МассивСотрудников);
	
	ПоказателиСотрудниковВРеквизитФормы(ТаблицаНачисленийСотрудников);
	ЗаполнитьФОТПоСотрудникам();
	
	ЗначенияСовокупныхТарифныхСтавокВРеквизитФормы(ЗначенияСовокупныхТарифныхСтавок, ТекущиеЗначенияСовокупныхТарифныхСтавок);
	
КонецПроцедуры

#КонецОбласти
