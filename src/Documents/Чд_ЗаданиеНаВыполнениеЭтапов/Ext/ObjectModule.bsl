﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ОбработчикиСобытий

Процедура ПередЗаписью(Отказ, РежимЗаписи, РежимПроведения)
	
	Если ОбменДанными.Загрузка Тогда
		Возврат;
	КонецЕсли;
	
	ИзменилисьКлючевыеРеквизиты = Ложь;
	Если РежимЗаписи = РежимЗаписиДокумента.Проведение Тогда
		Если Проведен Тогда
			ИзменилисьКлючевыеРеквизиты =
				ЭлектронноеВзаимодействиеБЗК.ИзменилисьРеквизитыОбъекта(ЭтотОбъект, "ПереданоВШвейноеПодразделение")
				ИЛИ ЭлектронноеВзаимодействиеБЗК.ИзмениласьТабличнаяЧастьОбъекта(ЭтотОбъект, "МаршрутныеКартыИПачки", "НомерПачки, Штрихкод, КоличествоВПачке");
		ИначеЕсли НЕ Ссылка.Проведен Тогда
			ИзменилисьКлючевыеРеквизиты = Истина;
		КонецЕсли;
	КонецЕсли;
	
	ОбновлениеИнформационнойБазы.ПроверитьОбъектОбработан(ЭтотОбъект);
	
	ДополнительныеСвойства.Вставить("ЭтоНовый", ЭтоНовый());
	ДополнительныеСвойства.Вставить("РежимЗаписи", РежимЗаписи);
	ДополнительныеСвойства.Вставить("ИзменилисьКлючевыеРеквизиты", ИзменилисьКлючевыеРеквизиты);
	
#Область Проверки
	Если НЕ Проведен И РежимЗаписи = РежимЗаписиДокумента.Проведение Тогда
		Запрос = Новый Запрос;
		Запрос.Текст =
			"ВЫБРАТЬ ПЕРВЫЕ 1
			|	Чд_ЗаданиеНаВыполнениеЭтапов.Представление КАК СсылкаПредставление,
			|	Чд_ЗаданиеНаВыполнениеЭтапов.ЗаказНаПроизводство.Представление КАК ЗаказНаПроизводствоПредставление
			|ИЗ
			|	Документ.Чд_ЗаданиеНаВыполнениеЭтапов КАК Чд_ЗаданиеНаВыполнениеЭтапов
			|ГДЕ
			|	Чд_ЗаданиеНаВыполнениеЭтапов.ЗаказНаПроизводство = &ЗаказНаПроизводство
			|	И Чд_ЗаданиеНаВыполнениеЭтапов.Проведен
			|	И Чд_ЗаданиеНаВыполнениеЭтапов.Ссылка <> &Ссылка";
		
		Запрос.УстановитьПараметр("ЗаказНаПроизводство", ЗаказНаПроизводство);
		Запрос.УстановитьПараметр("Ссылка", Ссылка);
		
		Выборка = Запрос.Выполнить().Выбрать();
		Если Выборка.Следующий() Тогда
			ТекстСообщения = НСтр("ru='%1 уже существует в проведенном документе ""%2""'");
			ТекстСообщения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ТекстСообщения,
								Выборка.ЗаказНаПроизводствоПредставление, Выборка.СсылкаПредставление);
			ОбщегоНазначения.СообщитьПользователю(ТекстСообщения, , , , Отказ);
			Возврат;
		КонецЕсли;
	КонецЕсли;
	
	Если Проведен
		И (ПометкаУдаления ИЛИ РежимЗаписи = РежимЗаписиДокумента.ОтменаПроведения) Тогда
		
		ЭтапыПроизводстваВИсходноеСостояние(Отказ);
		Если Отказ Тогда
			Возврат;
		КонецЕсли;
		
		Запрос = Новый Запрос;
		Запрос.Текст =
			"ВЫБРАТЬ
			|	ЭтапПроизводства2_2ВыходныеИзделия.Ссылка.Представление КАК ЭтапПредставление,
			|	ЭтапПроизводства2_2ВыходныеИзделия.Чд_НомерПачки КАК НомерПачки
			|ИЗ
			|	Документ.Чд_ЗаданиеНаВыполнениеЭтапов.МаршрутныеКартыИПачки КАК Чд_ЗаданиеНаВыполнениеЭтаповМаршрутныеКартыИПачки
			|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Документ.ЭтапПроизводства2_2.ВыходныеИзделия КАК ЭтапПроизводства2_2ВыходныеИзделия
			|		ПО Чд_ЗаданиеНаВыполнениеЭтаповМаршрутныеКартыИПачки.ЭтапПроизводства = ЭтапПроизводства2_2ВыходныеИзделия.Ссылка
			|			И Чд_ЗаданиеНаВыполнениеЭтаповМаршрутныеКартыИПачки.НомерПачки = ЭтапПроизводства2_2ВыходныеИзделия.Чд_НомерПачки
			|			И (ЭтапПроизводства2_2ВыходныеИзделия.Произведено)
			|ГДЕ
			|	Чд_ЗаданиеНаВыполнениеЭтаповМаршрутныеКартыИПачки.Ссылка = &Ссылка
			|;
			|
			|////////////////////////////////////////////////////////////////////////////////
			|ВЫБРАТЬ
			|	ПРЕДСТАВЛЕНИЕ(ЭЭ_Трудозатраты36ПроцессаСрезПоследних.Исполнитель) КАК ИсполнительПредставление,
			|	ПРЕДСТАВЛЕНИЕ(ЭЭ_Трудозатраты36ПроцессаСрезПоследних.Операция) КАК ОперацияПредставление
			|ИЗ
			|	РегистрСведений.ЭЭ_Трудозатраты36Процесса.СрезПоследних(, Задание = &Ссылка) КАК ЭЭ_Трудозатраты36ПроцессаСрезПоследних";
		
		Запрос.УстановитьПараметр("Ссылка", Ссылка);
		
		Результат = Запрос.ВыполнитьПакет();
		ВыборкаПроизведеноПоЭтапу = Результат[0].Выбрать();
		Пока ВыборкаПроизведеноПоЭтапу.Следующий() Цикл
			ТекстСообщения = НСтр("ru='Изменение %1 не доступно, т.к. в связанном %2 есть отметка о выпуске продукции по пачке №%3!'");
			ТекстСообщения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ТекстСообщения, Ссылка,
				ВыборкаПроизведеноПоЭтапу.ЭтапПредставление, ВыборкаПроизведеноПоЭтапу.НомерПачки);
			ОбщегоНазначения.СообщитьПользователю(ТекстСообщения, , , , Отказ);
		КонецЦикла;
		
		ВыборкаОтметкаИсполнителем = Результат[1].Выбрать();
		Пока ВыборкаОтметкаИсполнителем.Следующий() Цикл
			ТекстСообщения = НСтр("ru='Изменение %1 не доступно, т.к. исполнителем %2 уже выполнена операция ""%3""!'");
			ТекстСообщения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ТекстСообщения, Ссылка,
				ВыборкаОтметкаИсполнителем.ИсполнительПредставление, ВыборкаОтметкаИсполнителем.ОперацияПредставление);
			ОбщегоНазначения.СообщитьПользователю(ТекстСообщения, , , , Отказ);
		КонецЦикла;
	КонецЕсли;
#КонецОбласти
	
КонецПроцедуры

Процедура ПриЗаписи(Отказ)
	
	Если ОбменДанными.Загрузка Тогда
		Возврат;
	КонецЕсли;
	
	Если Проведен И ЕстьНезаполненныеПачки() Тогда
		ТекстСообщения = НСтр("ru='Проведение документа без разбития на пачки запрещено!'");
		ОбщегоНазначения.СообщитьПользователю(ТекстСообщения, , , , Отказ);
		Возврат;
	КонецЕсли;
	
	Если ДополнительныеСвойства.ИзменилисьКлючевыеРеквизиты Тогда
		ЗаполнитьДокумент_Этап(Отказ);
	КонецЕсли;
	
	// ++EE:KMV 14.06.2022 MLVC-583
	Если НЕ Отказ
		И ЗначениеЗаполнено(ПереданоВШвейноеПодразделение) Тогда
		
		ЭЭ_ЗапускБизнесПроцессаПередачаС36На60Процесс();
	КонецЕсли;
	// --EE:KMV 14.06.2022 MLVC-583
	
КонецПроцедуры

Процедура ОбработкаПроведения(Отказ, РежимПроведения)
	
	ПроведениеСерверУТ.ИнициализироватьДополнительныеСвойстваДляПроведения(Ссылка, ДополнительныеСвойства, РежимПроведения);
	
	Документы.Чд_ЗаданиеНаВыполнениеЭтапов.ИнициализироватьДанныеДокумента(Ссылка, ДополнительныеСвойства);
	ПроведениеСерверУТ.ПодготовитьНаборыЗаписейКРегистрацииДвижений(ЭтотОбъект);
	
	РегистрыСведений.Чд_ЗаданиеНаВыполнениеЭтаповКВыгрузке.ОтразитьДвижения(ДополнительныеСвойства, Движения, Отказ);
	РегистрыСведений.Чд_ПередачаНаКооперациюКВыгрузке.ОтразитьДвижения(ДополнительныеСвойства, Движения, Отказ);
	РегистрыСведений.ЭЭ_ШКОпераций36Процесса.ОтразитьДвижения(ДополнительныеСвойства, Движения, Отказ);
	РегистрыНакопления.ЭЭ_ПлановыеТрудозатраты36Процесса.ОтразитьДвижения(ДополнительныеСвойства, Движения, Отказ);
	
	СформироватьСписокРегистровДляКонтроля();
	
	ПроведениеСерверУТ.ЗаписатьНаборыЗаписей(ЭтотОбъект);
	ПроведениеСерверУТ.ВыполнитьКонтрольРезультатовПроведения(ЭтотОбъект, Отказ);
	ПроведениеСерверУТ.ОчиститьДополнительныеСвойстваДляПроведения(ДополнительныеСвойства);
	
	// ++EE:YDS 05.03.2023 MLVC-1042
	Если Не Отказ И ЗначениеЗаполнено(ПереданоВШвейноеПодразделение) Тогда
		ЮрЛицоБригады = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Ссылка.БригадаПошива, "Чд_ЮридическоеЛицо");
		Если Не ЮрЛицоБригады = Справочники.ЭЭ_ПредопределенныеЗначения.ОрганизацияМилавица.Значение Тогда
			Документы.ЭЭ_ДополнительнаяОперацияПК.СоздатьДопОперацииПК36Процесс(Ссылка);		
		КонецЕсли;		
	КонецЕсли;
	// --EE:YDS 05.03.2023 MLVC-1042
	
КонецПроцедуры

Процедура ОбработкаУдаленияПроведения(Отказ)
	
	ПроведениеСерверУТ.ИнициализироватьДополнительныеСвойстваДляПроведения(Ссылка, ДополнительныеСвойства);
	ПроведениеСерверУТ.ПодготовитьНаборыЗаписейКРегистрацииДвижений(ЭтотОбъект);
	СформироватьСписокРегистровДляКонтроля();
	ПроведениеСерверУТ.ЗаписатьНаборыЗаписей(ЭтотОбъект);
	ПроведениеСерверУТ.ВыполнитьКонтрольРезультатовПроведения(ЭтотОбъект, Отказ);
	ПроведениеСерверУТ.ОчиститьДополнительныеСвойстваДляПроведения(ДополнительныеСвойства);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

Процедура СформироватьСписокРегистровДляКонтроля()
	
	Массив = Новый Массив;
	// ++EE:BAN 23.05.2023 SDEE-2153
	Если Не ДополнительныеСвойства.Свойство("ПропуститьПроверкуТрудозатрат") Тогда
		Массив.Добавить(Движения.ЭЭ_ПлановыеТрудозатраты36Процесса);
	КонецЕсли;
	// --EE:BAN 23.05.2023 SDEE-2153
	
	ДополнительныеСвойства.ДляПроведения.Вставить("РегистрыДляКонтроля", Массив);
	
КонецПроцедуры

Функция ЕстьНезаполненныеПачки()
	
	Запрос = Новый Запрос;
	Запрос.Текст =
		"ВЫБРАТЬ ПЕРВЫЕ 1
		|	Чд_ЗаданиеНаВыполнениеЭтаповМаршрутныеКартыИПачки.НомерПачки КАК НомерПачки
		|ИЗ
		|	Документ.Чд_ЗаданиеНаВыполнениеЭтапов.МаршрутныеКартыИПачки КАК Чд_ЗаданиеНаВыполнениеЭтаповМаршрутныеКартыИПачки
		|ГДЕ
		|	Чд_ЗаданиеНаВыполнениеЭтаповМаршрутныеКартыИПачки.Ссылка = &Ссылка
		|	И Чд_ЗаданиеНаВыполнениеЭтаповМаршрутныеКартыИПачки.НомерПачки = 0";
	
	Запрос.УстановитьПараметр("Ссылка", Ссылка);
	
	Выборка = Запрос.Выполнить().Выбрать();
	
	Если Выборка.Следующий() Тогда
		Возврат Истина;
	КонецЕсли;
	
	Возврат Ложь;
	
КонецФункции

Процедура ЗаполнитьДокумент_Этап(Отказ)
	
	Запрос = Новый Запрос;
	Запрос.Текст =
		"ВЫБРАТЬ
		|	МаршрутныеКартыИПачки.ЭтапПроизводства КАК ЭтапПроизводства,
		|	МаршрутныеКартыИПачки.КоличествоВТП КАК КоличествоВТП,
		|	МаршрутныеКартыИПачки.КоличествоВПачке КАК КоличествоВПачке,
		|	МаршрутныеКартыИПачки.Номенклатура КАК Номенклатура,
		|	МаршрутныеКартыИПачки.НомерПачки КАК НомерПачки
		|ПОМЕСТИТЬ ВТ_ЭтапПроизводства
		|ИЗ
		|	&МаршрутныеКартыИПачки КАК МаршрутныеКартыИПачки
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	ВТ_ЭтапПроизводства.ЭтапПроизводства КАК ЭтапПроизводства,
		|	ВТ_ЭтапПроизводства.КоличествоВТП КАК КоличествоВТП,
		|	ВТ_ЭтапПроизводства.КоличествоВПачке КАК КоличествоВПачке,
		|	ВТ_ЭтапПроизводства.Номенклатура КАК Номенклатура,
		|	ВТ_ЭтапПроизводства.НомерПачки КАК НомерПачки
		|ИЗ
		|	ВТ_ЭтапПроизводства КАК ВТ_ЭтапПроизводства
		|
		|УПОРЯДОЧИТЬ ПО
		|	НомерПачки
		|ИТОГИ ПО
		|	ЭтапПроизводства";
	
	Запрос.УстановитьПараметр("МаршрутныеКартыИПачки", МаршрутныеКартыИПачки.Выгрузить());
	
	Результат = Запрос.Выполнить();
	ВыборкаПоЭтапамПроизводства = Результат.Выбрать(ОбходРезультатаЗапроса.ПоГруппировкам);
	Пока ВыборкаПоЭтапамПроизводства.Следующий() Цикл
		Если Отказ Тогда
			Прервать;
		КонецЕсли;
		
		ЭтапОбъект = ВыборкаПоЭтапамПроизводства.ЭтапПроизводства.ПолучитьОбъект();
		Если ЭтапОбъект.ВыходныеИзделия.Количество() = 1 Тогда
			ЭтапОбъект.ВыходныеИзделия[0].Чд_НомерПачки = 0;
			РазбитьСтрокиЭтапа(ЭтапОбъект, ВыборкаПоЭтапамПроизводства, Отказ);
		Иначе
			КлючевыеРеквизиты = Новый Массив;
			КлючевыеРеквизиты.Добавить("Номенклатура");
			КлючевыеРеквизиты.Добавить("Характеристика");
			КлючевыеРеквизиты.Добавить("Назначение");
			КлючевыеРеквизиты.Добавить("Серия");
			КлючевыеРеквизиты.Добавить("Произведено");
			КлючевыеРеквизиты.Добавить("ДатаПроизводства");
			КлючевыеРеквизиты.Добавить("Упаковка");
			КлючевыеРеквизиты.Добавить("Получатель");
			КлючевыеРеквизиты.Добавить("Отменено");
			КлючевыеРеквизиты.Добавить("Чд_НомерПачки");
			
			ЕстьДублиСтрок = Документы.Чд_ЗаданиеНаВыполнениеЭтапов.ПроверитьНаличиеДублейСтрокТЧ(ЭтапОбъект, "ВыходныеИзделия", КлючевыеРеквизиты);
			Если ЕстьДублиСтрок Тогда
				Отказ = Истина;
				Продолжить;
			КонецЕсли;
			
			ИсходнаяСтрока = ЭтапОбъект.ВыходныеИзделия[0];
			ИсходнаяСтрока.КоличествоУпаковок = ЭтапОбъект.ВыходныеИзделия.Итог("КоличествоУпаковок");
			ИсходнаяСтрока.Количество = ИсходнаяСтрока.КоличествоУпаковок;
			ИсходнаяСтрока.ДоляСтоимости = ИсходнаяСтрока.КоличествоУпаковок;
			ИсходнаяСтрока.Чд_НомерПачки = 0;
			
			МассивСтрокКУдалению = Новый Массив;
			Для Каждого СтрокаЭтапа Из ЭтапОбъект.ВыходныеИзделия Цикл
				Если СтрокаЭтапа.НомерСтроки <> 1 Тогда
					МассивСтрокКУдалению.Добавить(СтрокаЭтапа);
				КонецЕсли;
			КонецЦикла;
			
			Для каждого СтрКУдалению Из МассивСтрокКУдалению Цикл
				ЭтапОбъект.ВыходныеИзделия.Удалить(СтрКУдалению);
			КонецЦикла;
			
			РазбитьСтрокиЭтапа(ЭтапОбъект, ВыборкаПоЭтапамПроизводства, Отказ);
		КонецЕсли;
	КонецЦикла;
	
КонецПроцедуры

Процедура РазбитьСтрокиЭтапа(ЭтапОбъект, ВыборкаПоЭтапамПроизводства, Отказ)
	
	ЭтапОбъект.Чд_ПереданоВШвейноеПодразделение = ПереданоВШвейноеПодразделение;

	ВыборкаДанные = ВыборкаПоЭтапамПроизводства.Выбрать();
	Пока ВыборкаДанные.Следующий() Цикл
		КоличествоСтрок = ЭтапОбъект.ВыходныеИзделия.Количество();
		КоличествоВТП   = ВыборкаДанные.КоличествоВПачке;
		
		СтруктураСтроки = Новый Структура("Номенклатура, Чд_НомерПачки", ВыборкаДанные.Номенклатура, 0);
		
		МассивСтрок = ЭтапОбъект.ВыходныеИзделия.НайтиСтроки(СтруктураСтроки);
		Для Каждого ТекСтрока Из МассивСтрок Цикл
			Если ТекСтрока.Количество > КоличествоВТП Тогда
				НоваяСтрока = ЭтапОбъект.ВыходныеИзделия.Добавить();
				ЗаполнитьЗначенияСвойств(НоваяСтрока, ТекСтрока);
				СтароеКоличество = ТекСтрока.Количество;
				НовоеКоличество = ТекСтрока.Количество - КоличествоВТП;
				ТекСтрока.Количество = КоличествоВТП;
				ТекСтрока.Чд_НомерПачки = ВыборкаДанные.НомерПачки;
				
				НоваяСтрока.Количество = НовоеКоличество;
				
				КоличествоВТП = НовоеКоличество;
				Если НЕ ЗначениеЗаполнено(НоваяСтрока.ИдентификаторСтроки) Тогда
					НоваяСтрока.ИдентификаторСтроки = Новый УникальныйИдентификатор;
				КонецЕсли;
				
				// пересчет количества упаковок
				Если ЗначениеЗаполнено(ТекСтрока.КоличествоУпаковок) Тогда
					НовоеКоличествоУпаковок = Формат((ТекСтрока.КоличествоУпаковок * НовоеКоличество / СтароеКоличество), "ЧДЦ=3");
					НоваяСтрока.КоличествоУпаковок = НовоеКоличествоУпаковок;
					КоличествоУпаковокВТП = ТекСтрока.КоличествоУпаковок - НовоеКоличествоУпаковок;
					ТекСтрока.КоличествоУпаковок = КоличествоУпаковокВТП;
				КонецЕсли;
				
				// пересчет ДоляСтоимости
				НовоеДоляСтоимости = Формат((ТекСтрока.ДоляСтоимости * НовоеКоличество / СтароеКоличество), "ЧДЦ=3");
				НоваяСтрока.ДоляСтоимости = НовоеДоляСтоимости;
				ДоляСтоимостиВТП = ТекСтрока.ДоляСтоимости - НовоеДоляСтоимости;
				ТекСтрока.ДоляСтоимости = ДоляСтоимостиВТП;
				ТекСтрока.КодСтроки = КоличествоСтрок + 1;
				ТекСтрока.ДатаПроизводства = ТекущаяДатаСеанса();
				КоличествоСтрок = КоличествоСтрок + 1;
			Иначе
				ТекСтрока.Чд_НомерПачки = ВыборкаДанные.НомерПачки;
			КонецЕсли;
		КонецЦикла;
	КонецЦикла;
	
	Если ЭтапОбъект.ПроверитьЗаполнение() Тогда
		ЭтапОбъект.ОтключитьЗаполнениеНормативногоГрафикаПриЗаписи();
		ЭтапОбъект.ОтключитьКонтрольПараметровРазмещенияВГрафике();
		ЭтапОбъект.ОтключитьПроверкуРеквизитовЦепочкиЭтапов();
		ЭтапОбъект.ВыходныеИзделия.Сортировать("Чд_НомерПачки Возр");
		
		Попытка
			ЭтапОбъект.Записать(РежимЗаписиДокумента.Проведение);
		Исключение
			ТекстОшибки = НСтр("ru='Не удалось провести документ %1.'");
			ТекстОшибки = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ТекстОшибки, ЭтапОбъект.Ссылка);
			ОбщегоНазначения.СообщитьПользователю(ТекстОшибки, , , , Отказ);
		КонецПопытки;
	КонецЕсли;
	
КонецПроцедуры

Процедура ЭтапыПроизводстваВИсходноеСостояние(Отказ)
	
	Запрос = Новый Запрос;
	Запрос.Текст =
		"ВЫБРАТЬ РАЗЛИЧНЫЕ
		|	Чд_ЗаданиеНаВыполнениеЭтаповМаршрутныеКартыИПачки.ЭтапПроизводства КАК Ссылка,
		|	Чд_ЗаданиеНаВыполнениеЭтаповМаршрутныеКартыИПачки.КоличествоВЭтапе КАК КоличествоВЭтапе
		|ИЗ
		|	Документ.Чд_ЗаданиеНаВыполнениеЭтапов.МаршрутныеКартыИПачки КАК Чд_ЗаданиеНаВыполнениеЭтаповМаршрутныеКартыИПачки
		|ГДЕ
		|	Чд_ЗаданиеНаВыполнениеЭтаповМаршрутныеКартыИПачки.Ссылка = &Ссылка";
	
	Запрос.УстановитьПараметр("Ссылка", Ссылка);
	
	Выборка = Запрос.Выполнить().Выбрать();
	
	Пока Выборка.Следующий() Цикл
		МассивУдаляемыхСтрок = Новый Массив;
		ДокументОбъект = Выборка.Ссылка.ПолучитьОбъект();
		СтрокаИсходная = ДокументОбъект.ВыходныеИзделия[0];
		СтрокаИсходная.КоличествоУпаковок = Выборка.КоличествоВЭтапе;
		СтрокаИсходная.Количество = Выборка.КоличествоВЭтапе;
		СтрокаИсходная.ДоляСтоимости = Выборка.КоличествоВЭтапе;
		СтрокаИсходная.Чд_НомерПачки = 0;
		СтрокаИсходная.Произведено = Ложь;
		
		Для Каждого СтрокаЭтапа Из ДокументОбъект.ВыходныеИзделия Цикл
			Если СтрокаЭтапа.Чд_НомерПачки <> 0 Тогда
				МассивУдаляемыхСтрок.Добавить(СтрокаЭтапа);
			КонецЕсли;
		КонецЦикла;
		
		Для Каждого СтрокаМассива Из МассивУдаляемыхСтрок Цикл
			ДокументОбъект.ВыходныеИзделия.Удалить(СтрокаМассива);
		КонецЦикла;
		
		Если ДокументОбъект.ПроверитьЗаполнение() Тогда
			ДокументОбъект.ОтключитьЗаполнениеНормативногоГрафикаПриЗаписи();
			ДокументОбъект.ОтключитьКонтрольПараметровРазмещенияВГрафике();
			ДокументОбъект.ОтключитьПроверкуРеквизитовЦепочкиЭтапов();
			Попытка
				ДокументОбъект.Записать(РежимЗаписиДокумента.Проведение);
			Исключение
				ТекстОшибки = НСтр("ru='Не удалось провести документ %1.'");
				ТекстОшибки = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ТекстОшибки, Выборка.Ссылка);
				ОбщегоНазначения.СообщитьПользователю(ТекстОшибки, , , , Отказ);
				ВызватьИсключение;
			КонецПопытки;
		Иначе
			Отказ = Истина;
			Прервать;
		КонецЕсли;
	КонецЦикла;
	
КонецПроцедуры

// ++EE:KMV 14.06.2022 MLVC-583
Процедура ЭЭ_ЗапускБизнесПроцессаПередачаС36На60Процесс()
	
	НовыйБП = БизнесПроцессы.ЭЭ_ПередачаС36На60Процесс.СоздатьБизнесПроцесс();
	НовыйБП.ДатаНачала = ТекущаяДатаСеанса();
	НовыйБП.ДатаЗавершения = ТекущаяДатаСеанса() + 86400; // + один день
	НовыйБП.СрокИсполнения = ТекущаяДатаСеанса() + 86400; // + один день
	НовыйБП.Дата = ТекущаяДатаСеанса();
	НовыйБП.Важность = Перечисления.ВариантыВажностиЗадачи.Обычная;
	НовыйБП.Автор = Пользователи.ТекущийПользователь();
	НовыйБП.Предмет = Ссылка;
	НовыйБП.Исполнитель = Справочники.РолиИсполнителей.ЭЭ_КоординаторДопОпераций;
	НовыйБП.Наименование = "Задача на создание документа 'Дополнительные производственные операции'";
	СодержаниеБП = " В документе %1 было отмечено поле 'передано в швейное подразделение' и/или внесены изменения. 
		|Необходимо создать документ 'Дополнительные производственные операции' для 60 процесса";
	НовыйБП.Содержание = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(СодержаниеБП, Ссылка);
	НовыйБП.Записать();
	
	НовыйБП.Старт();
	
КонецПроцедуры
// --EE:KMV 14.06.2022 MLVC-583

#КонецОбласти

#КонецЕсли