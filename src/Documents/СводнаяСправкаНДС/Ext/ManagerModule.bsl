﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ПрограммныйИнтерфейс

// Определяет список команд отчетов.
//
// Параметры:
//   КомандыОтчетов - ТаблицаЗначений - Таблица с командами отчетов. Для изменения.
//       См. описание 1 параметра процедуры ВариантыОтчетовПереопределяемый.ПередДобавлениемКомандОтчетов().
//   Параметры - Структура - Вспомогательные параметры. Для чтения.
//       См. описание 2 параметра процедуры ВариантыОтчетовПереопределяемый.ПередДобавлениемКомандОтчетов().
//
Процедура ДобавитьКомандыОтчетов(КомандыОтчетов, Параметры) Экспорт
	
	ВариантыОтчетовУТПереопределяемый.ДобавитьКомандуСтруктураПодчиненности(КомандыОтчетов);
	
КонецПроцедуры

// Функция формирует представление счет-фактуры.
//
// Параметры:
//  Номер - Строка - Номер счета-фактуры;
//  Дата - Дата - Дата счета-фактуры;
//  Проведен - Булево - признак проведения
//
// Возвращаемое значение:
//	Строка - Представление счета-фактуры.
//
Функция ПредставлениеСчетаФактуры(Номер, Дата, Проведен = Истина) Экспорт
	
	СтруктураШапки = Новый Структура;
	СтруктураШапки.Вставить("Дата", Дата);
	СтруктураШапки.Вставить("Номер", Номер);
	
	МассивПодстрок = Новый Массив;
	МассивПодстрок.Добавить(ОбщегоНазначенияУТКлиентСервер.СформироватьЗаголовокДокумента(СтруктураШапки, НСтр("ru = 'Счет-фактура';
																												|en = 'Tax invoice'")));
	Если Не Проведен Тогда
		МассивПодстрок.Добавить(НСтр("ru = '(не проведен)';
									|en = '(not posted)'"));
	КонецЕсли;
	Представление = СтрСоединить(МассивПодстрок, " ");
	
	Возврат Представление;
	
КонецФункции

//Возвращает количество счетов-фактур к оформлению.
//
// Параметры:
//	Параметры - структура со следующими свойствами:
//	* МассивОрганизаций - Массив - массив организаций.
//	* КонецПериода - Дата - ограничение сверху на дату документа-основания.
//	* БезОграниченияПериода - Булево - не ограничивать по дате документа-основания 
//
// Возвращаемое значение:
//	Число - количество счетов-фактур к оформлению.
//
Функция ЕстьСчетаФактурыКОформлению(Параметры) Экспорт
	
	Запрос = Новый Запрос;
	Запрос.Параметры.Вставить("ТипСчетаФактуры", ОбщегоНазначения.ИдентификаторОбъектаМетаданных("Документ.СводнаяСправкаНДС"));
	Запрос.Параметры.Вставить("МассивОрганизаций", Параметры.МассивОрганизаций);
	Если Параметры.Свойство("БезОграниченияПериода") Тогда
		Запрос.Текст = 
		"ВЫБРАТЬ
		|	СУММА(1) КАК КоличествоДокументовКОформлению
		|ИЗ
		|	РегистрСведений.ТребуетсяОформлениеСчетаФактуры КАК ТребуетсяОформлениеСчетаФактуры
		|ГДЕ
		|	ТребуетсяОформлениеСчетаФактуры.ТипСчетаФактуры = &ТипСчетаФактуры
		|	И &УсловиеОтбора";
	Иначе
		Запрос.Параметры.Вставить("КонецПериода", КонецДня(Параметры.КонецПериода));
		Запрос.Текст = 
		"ВЫБРАТЬ
		|	СУММА(1) КАК КоличествоДокументовКОформлению
		|ИЗ
		|	РегистрСведений.ТребуетсяОформлениеСчетаФактуры КАК ТребуетсяОформлениеСчетаФактуры
		|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.ДанныеПервичныхДокументов КАК ДанныеОснования
		|		ПО ТребуетсяОформлениеСчетаФактуры.Организация = ДанныеОснования.Организация
		|			И ТребуетсяОформлениеСчетаФактуры.Основание = ДанныеОснования.Документ
		|ГДЕ
		|	ТребуетсяОформлениеСчетаФактуры.ТипСчетаФактуры = &ТипСчетаФактуры
		|	И ВЫБОР
		|			КОГДА ЕСТЬNULL(ДанныеОснования.Дата, ДАТАВРЕМЯ(1, 1, 1)) = ДАТАВРЕМЯ(1, 1, 1)
		|				ТОГДА ЕСТЬNULL(ДанныеОснования.ДатаРегистратора, ДАТАВРЕМЯ(1, 1, 1))
		|			ИНАЧЕ ДанныеОснования.Дата
		|		КОНЕЦ <= &КонецПериода
		|	И &УсловиеОтбора";
	КонецЕсли;
	
	Если ЗначениеЗаполнено(Параметры.МассивОрганизаций) Тогда
		УсловиеОтбора = "ТребуетсяОформлениеСчетаФактуры.Организация В(&МассивОрганизаций)";
		Запрос.Текст = СтрЗаменить(Запрос.Текст,"&УсловиеОтбора", УсловиеОтбора);
	Иначе
		Запрос.Текст = СтрЗаменить(Запрос.Текст,"И &УсловиеОтбора", "");
	КонецЕсли;
	
	Выборка = Запрос.Выполнить().Выбрать();
	Если Выборка.Следующий() И НЕ Выборка.КоличествоДокументовКОформлению = NULL Тогда
		Возврат Выборка.КоличествоДокументовКОформлению;
	Иначе
		Возврат 0;
	КонецЕсли;
	
КонецФункции

#Область ДляВызоваИзДругихПодсистем

// СтандартныеПодсистемы.УправлениеДоступом

// См. УправлениеДоступомПереопределяемый.ПриЗаполненииСписковСОграничениемДоступа.
Процедура ПриЗаполненииОграниченияДоступа(Ограничение) Экспорт

	Ограничение.Текст =
	"РазрешитьЧтениеИзменение
	|ГДЕ
	|	ЗначениеРазрешено(Организация)";

КонецПроцедуры

// Конец СтандартныеПодсистемы.УправлениеДоступом

#КонецОбласти

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область Проведение

Функция ДополнительныеИсточникиДанныхДляДвижений(ИмяРегистра) Экспорт

	ИсточникиДанных = Новый Соответствие;

	Возврат ИсточникиДанных; 

КонецФункции

Процедура ИнициализироватьДанныеДокумента(ДокументСсылка, ДополнительныеСвойства, Регистры = Неопределено) Экспорт
	
	////////////////////////////////////////////////////////////////////////////
	// Создадим запрос инициализации движений
	
	Запрос = Новый Запрос;
	ЗаполнитьПараметрыИнициализации(Запрос, ДокументСсылка);
	
	////////////////////////////////////////////////////////////////////////////
	// Сформируем текст запроса
	ТекстыЗапроса = Новый СписокЗначений;
	ТекстЗапросаТаблицаЖурналУчетаСчетовФактур(Запрос, ТекстыЗапроса, Регистры);
	ТекстЗапросаТаблицаНДСЗаписиКнигиПокупок(Запрос, ТекстыЗапроса, Регистры);
	ТекстЗапросаТаблицаНДСЗаписиКнигиПродаж(Запрос, ТекстыЗапроса, Регистры);
	ТекстЗапросаТаблицаРеестрДокументов(Запрос, ТекстыЗапроса, Регистры);
	
	ПроведениеСерверУТ.ИнициализироватьТаблицыДляДвижений(Запрос, ТекстыЗапроса, ДополнительныеСвойства.ТаблицыДляДвижений, Истина);
	
КонецПроцедуры

Процедура ЗаполнитьПараметрыИнициализации(Запрос, ДокументСсылка)
	
	Запрос.МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
	Запрос.УстановитьПараметр("Ссылка", ДокументСсылка);
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	ДанныеДокумента.Дата                                                              КАК Период,
	|	ДанныеДокумента.Номер                                                             КАК Номер,
	|	ДанныеДокумента.Организация                                                       КАК Организация,
	|	ЕСТЬNULL(ДанныеДокумента.Организация.ОбособленноеПодразделение, ЛОЖЬ)             КАК ОбособленноеПодразделение,
	|	ЕСТЬNULL(ДанныеДокумента.Организация.ЦифровойИндексОбособленногоПодразделения, 0) КАК ЦифровойИндексОбособленногоПодразделения,
	|	ДанныеДокумента.СводнаяСправкаОснование                                           КАК СводнаяСправкаОснование,
	|	ЕСТЬNULL(ДанныеДокумента.СводнаяСправкаОснование.Дата, ДАТАВРЕМЯ(1,1,1))          КАК ДатаСводнойСправкиОснования,
	|	ДанныеДокумента.СводнаяСправкаОснование.Номер                                     КАК НомерСводнойСправкиОснования,
	|	ДанныеДокумента.Исправление                                                       КАК Исправление,
	|	ДанныеДокумента.НомерИсправления                                                  КАК НомерИсправления,
	|	ДанныеДокумента.Корректировочная                                                  КАК Корректировочная,
	|	ДанныеДокумента.ПервичныйДокумент                                                 КАК ПервичныйДокумент,
	|	ДанныеДокумента.ДатаПервичногоДокумента                                           КАК ДатаПервичногоДокумента,
	|	ДанныеДокумента.НомерПервичногоДокумента                                          КАК НомерПервичногоДокумента,
	|	ДанныеДокумента.РозничныйПокупатель                                               КАК РозничныйПокупатель,
	|	ДанныеДокумента.Проведен                                                          КАК Проведен,
	|	ДанныеДокумента.ПометкаУдаления                                                   КАК ПометкаУдаления
	|ИЗ
	|	Документ.СводнаяСправкаНДС КАК ДанныеДокумента
	|ГДЕ
	|	ДанныеДокумента.Ссылка = &Ссылка";
	
	Реквизиты = Запрос.Выполнить().Выбрать();
	Реквизиты.Следующий();
	
	Запрос.УстановитьПараметр("Период",						Реквизиты.Период);
	Запрос.УстановитьПараметр("ДатаВыставления",			Реквизиты.Период);
	Запрос.УстановитьПараметр("Номер",						Реквизиты.Номер);
	Запрос.УстановитьПараметр("Организация",				Реквизиты.Организация);
	Запрос.УстановитьПараметр("ДатаСводнойСправкиОснования",Реквизиты.ДатаСводнойСправкиОснования);
	Запрос.УстановитьПараметр("Исправление",				Реквизиты.Исправление);
	Запрос.УстановитьПараметр("НомерИсправления",			Реквизиты.НомерИсправления);
	Запрос.УстановитьПараметр("Корректировочная",			Реквизиты.Корректировочная);
	Запрос.УстановитьПараметр("СводнаяСправкаОснование",	Реквизиты.СводнаяСправкаОснование);
	Запрос.УстановитьПараметр("ПервичныйДокумент",			Реквизиты.ПервичныйДокумент);
	Запрос.УстановитьПараметр("ДатаПервичногоДокумента",	Реквизиты.ДатаПервичногоДокумента);
	Запрос.УстановитьПараметр("НомерПервичногоДокумента",	Реквизиты.НомерПервичногоДокумента);
	Запрос.УстановитьПараметр("Проведен",					Реквизиты.Проведен);
	Запрос.УстановитьПараметр("ПометкаУдаления",			Реквизиты.ПометкаУдаления);
	Запрос.УстановитьПараметр("ИдентификаторМетаданных",	ОбщегоНазначения.ИдентификаторОбъектаМетаданных(ТипЗнч(ДокументСсылка)));
	Запрос.УстановитьПараметр("КодВидаОперации",			"26");
	Запрос.УстановитьПараметр("КодВидаОперацииНаУменьшение",?(Реквизиты.РозничныйПокупатель, "17", "16"));
	Запрос.УстановитьПараметр("ОбособленноеПодразделение",	Реквизиты.ОбособленноеПодразделение);
	Запрос.УстановитьПараметр("ЦифровойИндексОбособленногоПодразделения", Реквизиты.ЦифровойИндексОбособленногоПодразделения);
	
	НомерНаПечать = УчетНДСПереопределяемый.НомерСчетаФактурыНаПечать(
				?(Реквизиты.Исправление, Реквизиты.НомерСводнойСправкиОснования, Реквизиты.Номер), 
				Ложь,
				Реквизиты.ОбособленноеПодразделение, 
				Реквизиты.ЦифровойИндексОбособленногоПодразделения);
	Запрос.УстановитьПараметр("НомерНаПечать",				НомерНаПечать);
	
	
	ИнформацияПоИсправлению = "";
	Если ЗначениеЗаполнено(Реквизиты.НомерИсправления) Тогда
		Если Реквизиты.Исправление Тогда
			ИнформацияПоИсправлению = НСтр("ru = 'Исправление %НомерИсправления% от %ДатаИсправления%';
											|en = 'Correction %НомерИсправления% dated %ДатаИсправления%'", ОбщегоНазначения.КодОсновногоЯзыка());
		КонецЕсли;
		ИнформацияПоИсправлению = СтрЗаменить(ИнформацияПоИсправлению, "%НомерИсправления%", СокрЛП(Реквизиты.НомерИсправления));
		ИнформацияПоИсправлению = СтрЗаменить(ИнформацияПоИсправлению, "%ДатаИсправления%", Формат(Реквизиты.Период,"ДЛФ=D"));
	КонецЕсли;
	Запрос.УстановитьПараметр("ИнформацияПоИсправлению", ИнформацияПоИсправлению);
	
	ЗначенияПараметровПроведения = ЗначенияПараметровПроведения(Реквизиты);
	Для каждого КлючИЗначение Из ЗначенияПараметровПроведения Цикл
		Запрос.УстановитьПараметр(КлючИЗначение.Ключ, КлючИЗначение.Значение);
	КонецЦикла; 
	
КонецПроцедуры

Функция ЗначенияПараметровПроведения(Реквизиты = Неопределено)

	ЗначенияПараметровПроведения = Новый Структура;
	ЗначенияПараметровПроведения.Вставить("ИдентификаторМетаданных", ОбщегоНазначения.ИдентификаторОбъектаМетаданных("Документ.СводнаяСправкаНДС"));
	ЗначенияПараметровПроведения.Вставить("Валюта", 				 Константы.ВалютаРегламентированногоУчета.Получить());

	Возврат ЗначенияПараметровПроведения;
	
КонецФункции

Процедура УстановитьПараметрЗапросаСводнаяСправкаПредыдущееИсправление(Запрос)
	
	Если Запрос.Параметры.Свойство("СводнаяСправкаПредыдущееИсправление") Тогда
		Возврат;
	КонецЕсли;
	
	Запрос.УстановитьПараметр("СводнаяСправкаПредыдущееИсправление", ?(Запрос.Параметры.Исправление,
		СводнаяСправкаПредыдущееИсправление(Запрос.Параметры.СводнаяСправкаОснование, Запрос.Параметры.НомерИсправления),
		Неопределено));
	
КонецПроцедуры

Функция ТекстЗапросаТаблицаЖурналУчетаСчетовФактур(Запрос, ТекстыЗапроса, Регистры)
	
	ИмяРегистра = "ЖурналУчетаСчетовФактур";
	
	Если НЕ ПроведениеСерверУТ.ТребуетсяТаблицаДляДвижений(ИмяРегистра, Регистры) Тогда
		Возврат "";
	КонецЕсли; 
	
	УстановитьПараметрЗапросаСводнаяСправкаПредыдущееИсправление(Запрос);
	
#Область ТекстСторноИсправление
	ТекстЗапросаСторноИсправление = 
	"ВЫБРАТЬ
	|	&Период КАК Период,
	|	ЖурналУчетаСчетовФактур.Организация КАК Организация,
	|	ЖурналУчетаСчетовФактур.Контрагент КАК Контрагент,
	|	ЖурналУчетаСчетовФактур.СчетФактура КАК СчетФактура,
	|	ЖурналУчетаСчетовФактур.ЧастьЖурнала КАК ЧастьЖурнала,
	|	ЖурналУчетаСчетовФактур.ДатаВыставленияПолучения КАК ДатаВыставленияПолучения,
	|	ЖурналУчетаСчетовФактур.КодСпособаВыставленияПолучения КАК КодСпособаВыставленияПолучения,
	|	ЖурналУчетаСчетовФактур.КодВидаОперации КАК КодВидаОперации,
	|	ЖурналУчетаСчетовФактур.НомерСчетаФактуры КАК НомерСчетаФактуры,
	|	ЖурналУчетаСчетовФактур.ДатаСчетаФактуры КАК ДатаСчетаФактуры,
	|	ЖурналУчетаСчетовФактур.НомерКорректировочногоСчетаФактуры КАК НомерКорректировочногоСчетаФактуры,
	|	ЖурналУчетаСчетовФактур.ДатаКорректировочногоСчетаФактуры КАК ДатаКорректировочногоСчетаФактуры,
	|	ЖурналУчетаСчетовФактур.НомерИсправления КАК НомерИсправления,
	|	ЖурналУчетаСчетовФактур.ДатаИсправления КАК ДатаИсправления,
	|	ЖурналУчетаСчетовФактур.НомерИсправленияКорректировочногоСчетаФактуры КАК НомерИсправленияКорректировочногоСчетаФактуры,
	|	ЖурналУчетаСчетовФактур.ДатаИсправленияКорректировочногоСчетаФактуры КАК ДатаИсправленияКорректировочногоСчетаФактуры,
	|	ЖурналУчетаСчетовФактур.ИндексСтроки КАК ИндексСтроки,
	|	ЖурналУчетаСчетовФактур.Валюта КАК Валюта,
	|	-ЖурналУчетаСчетовФактур.СуммаПоСчетуФактуре КАК СуммаПоСчетуФактуре,
	|	-ЖурналУчетаСчетовФактур.СуммаНДС КАК СуммаНДС,
	|	-ЖурналУчетаСчетовФактур.СуммаПоСчетуФактуреРазницаУменьшение КАК СуммаПоСчетуФактуреРазницаУменьшение,
	|	-ЖурналУчетаСчетовФактур.СуммаПоСчетуФактуреРазницаУвеличение КАК СуммаПоСчетуФактуреРазницаУвеличение,
	|	-ЖурналУчетаСчетовФактур.СуммаНДСРазницаУменьшение КАК СуммаНДСРазницаУменьшение,
	|	-ЖурналУчетаСчетовФактур.СуммаНДСРазницаУвеличение КАК СуммаНДСРазницаУвеличение,
	|	ЖурналУчетаСчетовФактур.ПоСтавкеБезНДС КАК ПоСтавкеБезНДС,
	|	ЖурналУчетаСчетовФактур.СчетФактураНеВыставляется КАК СчетФактураНеВыставляется,
	|	ЖурналУчетаСчетовФактур.КППКонтрагента КАК КППКонтрагента,
	|	ЖурналУчетаСчетовФактур.ИННКонтрагента КАК ИННКонтрагента,
	|	ЖурналУчетаСчетовФактур.Посредник КАК Посредник,
	|	ЖурналУчетаСчетовФактур.СчетФактураВыданныйПокупателю КАК СчетФактураВыданныйПокупателю,
	|	-ЖурналУчетаСчетовФактур.СуммаПоСчетуФактуреКомиссия КАК СуммаПоСчетуФактуреКомиссия,
	|	-ЖурналУчетаСчетовФактур.СуммаНДСКомиссия КАК СуммаНДСКомиссия,
	|	-ЖурналУчетаСчетовФактур.СуммаПоСчетуФактуреРазницаУменьшениеКомиссия КАК СуммаПоСчетуФактуреРазницаУменьшениеКомиссия,
	|	-ЖурналУчетаСчетовФактур.СуммаПоСчетуФактуреРазницаУвеличениеКомиссия КАК СуммаПоСчетуФактуреРазницаУвеличениеКомиссия,
	|	-ЖурналУчетаСчетовФактур.СуммаНДСРазницаУменьшениеКомиссия КАК СуммаНДСРазницаУменьшениеКомиссия,
	|	-ЖурналУчетаСчетовФактур.СуммаНДСРазницаУвеличениеКомиссия КАК СуммаНДСРазницаУвеличениеКомиссия,
	|	ЖурналУчетаСчетовФактур.КодВидаОперацииКомиссия КАК КодВидаОперацииКомиссия,
	|	ЖурналУчетаСчетовФактур.Субкомиссионер КАК Субкомиссионер,
	|	ЖурналУчетаСчетовФактур.КодВидаСделки КАК КодВидаСделки,
	|	ЖурналУчетаСчетовФактур.НомерСчетаФактурыПродавца КАК НомерСчетаФактурыПродавца,
	|	ЖурналУчетаСчетовФактур.ДатаСчетаФактурыПродавца КАК ДатаСчетаФактурыПродавца,
	|	ЖурналУчетаСчетовФактур.ИННПродавца КАК ИННПродавца,
	|	ЖурналУчетаСчетовФактур.КПППродавца КАК КПППродавца,
	|	ЖурналУчетаСчетовФактур.ИННСубкомиссионера КАК ИННСубкомиссионера,
	|	ЖурналУчетаСчетовФактур.КППСубкомиссионера КАК КППСубкомиссионера,
	|	ИСТИНА КАК Сторно,
	|	ЖурналУчетаСчетовФактур.ИсправленныйСчетФактура КАК ИсправленныйСчетФактура,
	|	ЖурналУчетаСчетовФактур.СчетФактураПолученныйОтПродавца КАК СчетФактураПолученныйОтПродавца,
	|	ЖурналУчетаСчетовФактур.ИсправлениеСобственнойОшибки КАК ИсправлениеСобственнойОшибки,
	|	ЖурналУчетаСчетовФактур.Продавец КАК Продавец
	|ИЗ
	|	РегистрСведений.ЖурналУчетаСчетовФактур.СрезПоследних(
	|		&Период,
	|		НЕ Сторно
	|		И Регистратор <> &Ссылка
	|		И СчетФактура = &СводнаяСправкаПредыдущееИсправление) КАК ЖурналУчетаСчетовФактур
	|
	|ГДЕ
	|	&Исправление
	|	И (ЖурналУчетаСчетовФактур.СуммаПоСчетуФактуреКомиссия > 0
	|		ИЛИ ЖурналУчетаСчетовФактур.СуммаНДСКомиссия > 0
	|		ИЛИ ЖурналУчетаСчетовФактур.СуммаПоСчетуФактуреРазницаУменьшениеКомиссия > 0
	|		ИЛИ ЖурналУчетаСчетовФактур.СуммаПоСчетуФактуреРазницаУвеличениеКомиссия > 0
	|		ИЛИ ЖурналУчетаСчетовФактур.СуммаНДСРазницаУменьшениеКомиссия > 0
	|		ИЛИ ЖурналУчетаСчетовФактур.СуммаНДСРазницаУвеличениеКомиссия > 0)
	|";
#КонецОбласти
	
#Область ТекстВыставленныеСчетаФактурыНаРеализацию
	ТекстЗапросаВыставленныеСчетаФактурыНаРеализацию = "
	|ВЫБРАТЬ
	|	&Период КАК Период,
	|	&Организация КАК Организация,
	|	NULL КАК Контрагент,
	|	&Ссылка КАК СчетФактура,
	|	ЗНАЧЕНИЕ(Перечисление.ЧастиЖурналаУчетаСчетовФактур.ВыставленныеСчетаФактуры) КАК ЧастьЖурнала,
	|	&ДатаВыставления КАК ДатаВыставленияПолучения,
	|	ЛОЖЬ КАК КодСпособаВыставленияПолучения,
	|	&КодВидаОперации КАК КодВидаОперации,
	|	&НомерНаПечать КАК НомерСчетаФактуры,
	|	&Период КАК ДатаСчетаФактуры,
	|	NULL КАК НомерКорректировочногоСчетаФактуры,
	|	NULL КАК ДатаКорректировочногоСчетаФактуры,
	|	NULL КАК НомерИсправления,
	|	NULL КАК ДатаИсправления,
	|	NULL КАК НомерИсправленияКорректировочногоСчетаФактуры,
	|	NULL КАК ДатаИсправленияКорректировочногоСчетаФактуры,
	|	1 КАК ИндексСтроки,
	|	&Валюта КАК Валюта,
	|	СУММА(ТаблицаДанных.СуммаБезНДС + ТаблицаДанных.НДС) КАК СуммаПоСчетуФактуре,
	|	СУММА(ТаблицаДанных.НДС) КАК СуммаНДС,
	|	NULL КАК СуммаПоСчетуФактуреРазницаУменьшение,
	|	NULL КАК СуммаПоСчетуФактуреРазницаУвеличение,
	|	NULL КАК СуммаНДСРазницаУменьшение,
	|	NULL КАК СуммаНДСРазницаУвеличение,
	|	ЛОЖЬ КАК ПоСтавкеБезНДС,
	|	ЛОЖЬ КАК СчетФактураНеВыставляется,
	|	NULL КАК КППКонтрагента,
	|	NULL КАК ИННКонтрагента,
	|	NULL КАК Посредник,
	|	NULL КАК СчетФактураВыданныйПокупателю,
	|	
	|	0 КАК СуммаПоСчетуФактуреКомиссия,
	|	0 КАК СуммаНДСКомиссия,
	|	0 КАК СуммаПоСчетуФактуреРазницаУменьшениеКомиссия,
	|	0 КАК СуммаПоСчетуФактуреРазницаУвеличениеКомиссия,
	|	0 КАК СуммаНДСРазницаУменьшениеКомиссия,
	|	0 КАК СуммаНДСРазницаУвеличениеКомиссия,
	|	"""" КАК КодВидаОперацииКомиссия,
	|	NULL КАК Субкомиссионер,
	|	NULL КАК КодВидаСделки,
	|	NULL КАК НомерСчетаФактурыПродавца,
	|	NULL КАК ДатаСчетаФактурыПродавца,
	|	NULL КАК ИННПродавца,
	|	NULL КАК КПППродавца,
	|	NULL КАК ИННСубкомиссионера,
	|	NULL КАК КППСубкомиссионера,
	|	ЛОЖЬ КАК Сторно,
	|	NULL КАК ИсправленныйСчетФактура,
	|	NULL КАК СчетФактураПолученныйОтПродавца,
	|	NULL КАК ИсправлениеСобственнойОшибки,
	|	NULL КАК Продавец
	|ИЗ
	|	Документ.СводнаяСправкаНДС.Продажи КАК ТаблицаДанных
	|
	|ГДЕ
	|	ТаблицаДанных.Ссылка = &Ссылка
	|	И НЕ &Корректировочная
	|	И НЕ &Исправление
	|	И &Организация <> ЗНАЧЕНИЕ(Справочник.Организации.УправленческаяОрганизация)
	|	
	|ИМЕЮЩИЕ
	|	СУММА(ТаблицаДанных.СуммаБезНДС) <> 0 
	|		ИЛИ СУММА(ТаблицаДанных.НДС) <> 0
	|";
#КонецОбласти
	
#Область ТекстВыставленныеСчетаФактурыНаРеализациюИсправление
	ТекстЗапросаВыставленныеСчетаФактурыНаРеализациюИсправление = "
	|ВЫБРАТЬ
	|	&Период,
	|	&Организация,
	|	NULL,
	|	&Ссылка,
	|	ЗНАЧЕНИЕ(Перечисление.ЧастиЖурналаУчетаСчетовФактур.ВыставленныеСчетаФактуры),
	|	&ДатаВыставления,
	|	ЛОЖЬ,
	|	&КодВидаОперации,
	|	&НомерНаПечать,
	|	&ДатаСводнойСправкиОснования,
	|	NULL,
	|	NULL,
	|	&НомерИсправления,
	|	&Период,
	|	NULL,
	|	NULL,
	|	2,
	|	&Валюта,
	|	СУММА(ТаблицаДанных.СуммаБезНДС + ТаблицаДанных.НДС) КАК СуммаПоСчетуФактуре,
	|	СУММА(ТаблицаДанных.НДС) КАК СуммаНДС,
	|	NULL,
	|	NULL,
	|	NULL,
	|	NULL,
	|	ЛОЖЬ,
	|	ЛОЖЬ,
	|	NULL,
	|	NULL,
	|	NULL,
	|	NULL,
	|	
	|	0 КАК СуммаПоСчетуФактуреКомиссия,
	|	0 КАК СуммаНДСКомиссия,
	|	0 КАК СуммаПоСчетуФактуреРазницаУменьшениеКомиссия,
	|	0 КАК СуммаПоСчетуФактуреРазницаУвеличениеКомиссия,
	|	0 КАК СуммаНДСРазницаУменьшениеКомиссия,
	|	0 КАК СуммаНДСРазницаУвеличениеКомиссия,
	|	"""" КАК КодВидаОперацииКомиссия,
	|	NULL КАК Субкомиссионер,
	|	NULL КАК КодВидаСделки,
	|	NULL КАК НомерСчетаФактурыПродавца,
	|	NULL КАК ДатаСчетаФактурыПродавца,
	|	NULL КАК ИННПродавца,
	|	NULL КАК КПППродавца,
	|	NULL КАК ИННСубкомиссионера,
	|	NULL КАК КППСубкомиссионера,
	|	ЛОЖЬ КАК Сторно,
	|	NULL КАК ИсправленныйСчетФактура,
	|	NULL КАК СчетФактураПолученныйОтПродавца,
	|	NULL КАК ИсправлениеСобственнойОшибки,
	|	NULL КАК Продавец
	|ИЗ
	|	Документ.СводнаяСправкаНДС.Продажи КАК ТаблицаДанных
	|
	|ГДЕ
	|	ТаблицаДанных.Ссылка = &Ссылка
	|	И НЕ &Корректировочная
	|	И &Исправление
	|	И &Организация <> ЗНАЧЕНИЕ(Справочник.Организации.УправленческаяОрганизация)
	|	
	|ИМЕЮЩИЕ
	|	СУММА(ТаблицаДанных.СуммаБезНДС) <> 0 
	|		ИЛИ СУММА(ТаблицаДанных.НДС) <> 0
	|";
#КонецОбласти
	
#Область ТекстВыставленныеСчетаФактурыНаРеализациюКорректировочный
	ТекстЗапросаВыставленныеСчетаФактурыНаРеализациюКорректировочный = "
	|ВЫБРАТЬ
	|	&Период,
	|	&Организация,
	|	NULL,
	|	&Ссылка,
	|	ЗНАЧЕНИЕ(Перечисление.ЧастиЖурналаУчетаСчетовФактур.ВыставленныеСчетаФактуры),
	|	&ДатаВыставления,
	|	ЛОЖЬ,
	|	&КодВидаОперацииНаУменьшение,
	|	&НомерПервичногоДокумента,
	|	&ДатаПервичногоДокумента,
	|	&НомерНаПечать,
	|	&Период,
	|	NULL,
	|	NULL,
	|	NULL,
	|	NULL,
	|	0,
	|	&Валюта,
	|	NULL,
	|	NULL,
	|	СУММА(ТаблицаДанных.СуммаБезНДС + ТаблицаДанных.НДС) КАК СуммаПоСчетуФактуреУменьшение,
	|	0,
	|	СУММА(ТаблицаДанных.НДС) КАК СуммаНДСУменьшение,
	|	0,
	|	ЛОЖЬ,
	|	ЛОЖЬ,
	|	NULL,
	|	NULL,
	|	NULL,
	|	NULL,
	|	
	|	0 КАК СуммаПоСчетуФактуреКомиссия,
	|	0 КАК СуммаНДСКомиссия,
	|	0,
	|	0,
	|	0,
	|	0,
	|	"""" КАК КодВидаОперацииКомиссия,
	|	NULL КАК Субкомиссионер,
	|	NULL КАК КодВидаСделки,
	|	NULL КАК НомерСчетаФактурыПродавца,
	|	NULL КАК ДатаСчетаФактурыПродавца,
	|	NULL КАК ИННПродавца,
	|	NULL КАК КПППродавца,
	|	NULL КАК ИННСубкомиссионера,
	|	NULL КАК КППСубкомиссионера,
	|	ЛОЖЬ КАК Сторно,
	|	NULL КАК ИсправленныйСчетФактура,
	|	NULL КАК СчетФактураПолученныйОтПродавца,
	|	NULL КАК ИсправлениеСобственнойОшибки,
	|	NULL КАК Продавец
	|
	|ИЗ
	|	Документ.СводнаяСправкаНДС.Продажи КАК ТаблицаДанных
	|
	|ГДЕ
	|	ТаблицаДанных.Ссылка = &Ссылка
	|	И &Корректировочная
	|	И НЕ &Исправление
	|	И &Организация <> ЗНАЧЕНИЕ(Справочник.Организации.УправленческаяОрганизация)
	|	
	|ИМЕЮЩИЕ
	|	СУММА(ТаблицаДанных.СуммаБезНДС) <> 0 
	|		ИЛИ СУММА(ТаблицаДанных.НДС) <> 0
	|";
#КонецОбласти

	МассивТекстов = Новый Массив;
	МассивТекстов.Добавить(ТекстЗапросаСторноИсправление);
	МассивТекстов.Добавить(ТекстЗапросаВыставленныеСчетаФактурыНаРеализацию);
	МассивТекстов.Добавить(ТекстЗапросаВыставленныеСчетаФактурыНаРеализациюИсправление);
	МассивТекстов.Добавить(ТекстЗапросаВыставленныеСчетаФактурыНаРеализациюКорректировочный);
	ТекстЗапроса = СтрСоединить(МассивТекстов, ОбщегоНазначенияУТ.РазделительЗапросовВОбъединении());
	
	ТекстыЗапроса.Добавить(ТекстЗапроса, ИмяРегистра);
	
	Возврат ТекстЗапроса
	
КонецФункции

Функция ТекстЗапросаТаблицаНДСЗаписиКнигиПокупок(Запрос, ТекстыЗапроса, Регистры)
	
	ИмяРегистра = "НДСЗаписиКнигиПокупок";
	
	Если НЕ ПроведениеСерверУТ.ТребуетсяТаблицаДляДвижений(ИмяРегистра, Регистры) Тогда
		Возврат "";
	КонецЕсли; 
	
	ТекстЗапросаКорректировка = "
	|
	|ВЫБРАТЬ
	|	&Период                           КАК Период,
	|	&Организация                      КАК Организация,
	|	&Организация                      КАК Поставщик,
	|	&Ссылка                           КАК СчетФактура,
	|	ТаблицаДанных.ВидЦенности         КАК ВидЦенности,
	|	ТаблицаДанных.СтавкаНДС           КАК СтавкаНДС,
	|	NULL                              КАК СчетУчетаНДС,
	|	ЗНАЧЕНИЕ(Перечисление.СобытияПоНДСПокупки.ПредъявленНДСКВычету) КАК Событие,
	|	&Период                           КАК ДатаСобытия,
	|	ЛОЖЬ                              КАК ЗаписьДополнительногоЛиста,
	|	NULL                              КАК КорректируемыйПериод,
	|	ЛОЖЬ                              КАК Исправление,
	|	NULL                              КАК ИсправленныйСчетФактура,
	|	СУММА(ТаблицаДанных.СуммаБезНДС)  КАК СуммаБезНДС,
	|	СУММА(ТаблицаДанных.НДС)          КАК НДС,
	|	&КодВидаОперацииНаУменьшение      КАК КодВидаОперации
	|ИЗ
	|	Документ.СводнаяСправкаНДС.Продажи КАК ТаблицаДанных
	|
	|ГДЕ
	|	ТаблицаДанных.Ссылка = &Ссылка
	|	И &Корректировочная
	|	И НЕ &Исправление
	|	И &Организация <> ЗНАЧЕНИЕ(Справочник.Организации.УправленческаяОрганизация)
	|
	|СГРУППИРОВАТЬ ПО
	|	ТаблицаДанных.ВидЦенности,
	|	ТаблицаДанных.СтавкаНДС
	|";
	
	ТекстыЗапроса.Добавить(ТекстЗапросаКорректировка, ИмяРегистра);
	Возврат ТекстЗапросаКорректировка
	
КонецФункции

Функция ТекстЗапросаТаблицаНДСЗаписиКнигиПродаж(Запрос, ТекстыЗапроса, Регистры)
	
	ИмяРегистра = "НДСЗаписиКнигиПродаж";
	
	Если НЕ ПроведениеСерверУТ.ТребуетсяТаблицаДляДвижений(ИмяРегистра, Регистры) Тогда
		Возврат "";
	КонецЕсли; 
	
	УстановитьПараметрЗапросаСводнаяСправкаПредыдущееИсправление(Запрос);
	
#Область ТекстСторноИсправление
	ТекстЗапросаСторноИсправление = "
	|
	|ВЫБРАТЬ
	|	&Период КАК Период,
	|	&Организация КАК Организация,
	|	НДСЗаписиКнигиПродаж.Покупатель КАК Покупатель,
	|	НДСЗаписиКнигиПродаж.СчетФактура КАК СчетФактура,
	|	НДСЗаписиКнигиПродаж.ВидЦенности КАК ВидЦенности,
	|	НДСЗаписиКнигиПродаж.СтавкаНДС КАК СтавкаНДС,
	|	НДСЗаписиКнигиПродаж.ДатаОплаты КАК ДатаОплаты,
	|	НДСЗаписиКнигиПродаж.ДокументОплаты КАК ДокументОплаты,
	|	НДСЗаписиКнигиПродаж.Событие КАК Событие,
	|	&Период КАК ДатаСобытия,
	|	ВЫБОР
	|		КОГДА НДСЗаписиКнигиПродаж.ЗаписьДополнительногоЛиста
	|			ТОГДА ИСТИНА
	|		КОГДА НАЧАЛОПЕРИОДА(НДСЗаписиКнигиПродаж.Период,КВАРТАЛ) < НАЧАЛОПЕРИОДА(&Период,КВАРТАЛ)
	|			ТОГДА ИСТИНА
	|	КОНЕЦ КАК ЗаписьДополнительногоЛиста,
	|	ВЫБОР
	|		КОГДА НДСЗаписиКнигиПродаж.КорректируемыйПериод <> ДАТАВРЕМЯ(1,1,1)
	|			ТОГДА НДСЗаписиКнигиПродаж.КорректируемыйПериод
	|		КОГДА НАЧАЛОПЕРИОДА(НДСЗаписиКнигиПродаж.Период,КВАРТАЛ) < НАЧАЛОПЕРИОДА(&Период,КВАРТАЛ)
	|			ТОГДА НДСЗаписиКнигиПродаж.Период
	|	КОНЕЦ КАК КорректируемыйПериод,
	|	ВЫБОР
	|		КОГДА НДСЗаписиКнигиПродаж.ЗаписьДополнительногоЛиста
	|			ТОГДА ИСТИНА
	|		КОГДА НАЧАЛОПЕРИОДА(НДСЗаписиКнигиПродаж.Период,КВАРТАЛ) < НАЧАЛОПЕРИОДА(&Период,КВАРТАЛ)
	|			ТОГДА ИСТИНА
	|	КОНЕЦ КАК СторнирующаяЗаписьДопЛиста,
	|	НДСЗаписиКнигиПродаж.ДоговорКонтрагента КАК ДоговорКонтрагента,
	|	НДСЗаписиКнигиПродаж.ИсправленныйСчетФактура КАК ИсправленныйСчетФактура,
	|	&Исправление КАК Исправление,
	|	НДСЗаписиКнигиПродаж.ДатаСчетаФактурыКомиссионера КАК ДатаСчетаФактурыКомиссионера,
	|	-НДСЗаписиКнигиПродаж.СуммаБезНДСОборот КАК СуммаБезНДС,
	|	-НДСЗаписиКнигиПродаж.НДСОборот КАК НДС,
	|	NULL КАК НомерДокументаОплаты,
	|	NULL КАК ДатаДокументаОплаты
	|ИЗ
	|	РегистрНакопления.НДСЗаписиКнигиПродаж.Обороты(
	|		,
	|		,
	|		Регистратор,
	|		Организация = &Организация
	|			И СчетФактура = &СводнаяСправкаОснование
	|			И ИсправленныйСчетФактура = &СводнаяСправкаПредыдущееИсправление) КАК НДСЗаписиКнигиПродаж
	|ГДЕ
	|	&Исправление
	|	И НДСЗаписиКнигиПродаж.Регистратор <> &Ссылка
	|	И (НДСЗаписиКнигиПродаж.СуммаБезНДСОборот > 0
	|		ИЛИ НДСЗаписиКнигиПродаж.НДСОборот > 0)
	|";
#КонецОбласти
	
#Область ТекстПродажи
	ТекстЗапросаПродажи = "
	|ВЫБРАТЬ
	|	&Период,
	|	&Организация,
	|	NULL,
	|	&Ссылка,
	|	ТаблицаДанных.ВидЦенности,
	|	ТаблицаДанных.СтавкаНДС,
	|	NULL,
	|	NULL,
	|	ЗНАЧЕНИЕ(Перечисление.СобытияПоНДСПродажи.Реализация) КАК Событие,
	|	&Период,
	|	ЛОЖЬ,
	|	NULL,
	|	ЛОЖЬ,
	|	NULL,
	|	NULL,
	|	ЛОЖЬ,
	|	NULL,
	|	СУММА(ТаблицаДанных.СуммаБезНДС) КАК СуммаБезНДС,
	|	СУММА(ТаблицаДанных.НДС) КАК НДС,
	|	NULL,
	|	NULL
	|ИЗ
	|	Документ.СводнаяСправкаНДС.Продажи КАК ТаблицаДанных
	|
	|ГДЕ
	|	ТаблицаДанных.Ссылка = &Ссылка
	|	И НЕ &Корректировочная
	|	И НЕ &Исправление
	|	И &Организация <> ЗНАЧЕНИЕ(Справочник.Организации.УправленческаяОрганизация)
	|
	|СГРУППИРОВАТЬ ПО
	|	ТаблицаДанных.ВидЦенности,
	|	ТаблицаДанных.СтавкаНДС
	|	
	|ИМЕЮЩИЕ
	|	СУММА(ТаблицаДанных.СуммаБезНДС) <> 0 
	|		ИЛИ СУММА(ТаблицаДанных.НДС) <> 0
	|";
#КонецОбласти

#Область ТекстПродажиИсправление
	ТекстПродажиИсправление = "
	|ВЫБРАТЬ
	|	&Период,
	|	&Организация,
	|	NULL,
	|	ТаблицаДанных.Ссылка.СводнаяСправкаОснование,
	|	ТаблицаДанных.ВидЦенности,
	|	ТаблицаДанных.СтавкаНДС,
	|	NULL,
	|	NULL,
	|	ЗНАЧЕНИЕ(Перечисление.СобытияПоНДСПродажи.Реализация) КАК Событие,
	|	&Период,
	|	ВЫБОР
	|		КОГДА НАЧАЛОПЕРИОДА(&ДатаСводнойСправкиОснования, КВАРТАЛ) < НАЧАЛОПЕРИОДА(&Период, КВАРТАЛ) ТОГДА
	|			ИСТИНА
	|		ИНАЧЕ
	|			ЛОЖЬ
	|	КОНЕЦ,
	|	ВЫБОР
	|		КОГДА НАЧАЛОПЕРИОДА(&ДатаСводнойСправкиОснования, КВАРТАЛ) < НАЧАЛОПЕРИОДА(&Период, КВАРТАЛ) ТОГДА
	|			&ДатаСводнойСправкиОснования
	|		ИНАЧЕ
	|			НЕОПРЕДЕЛЕНО
	|	КОНЕЦ,
	|	ЛОЖЬ,
	|	NULL,
	|	&Ссылка,
	|	ИСТИНА,
	|	NULL,
	|	СУММА(ТаблицаДанных.СуммаБезНДС) КАК СуммаБезНДС,
	|	СУММА(ТаблицаДанных.НДС) КАК НДС,
	|	NULL,
	|	NULL
	|ИЗ
	|	Документ.СводнаяСправкаНДС.Продажи КАК ТаблицаДанных
	|
	|ГДЕ
	|	ТаблицаДанных.Ссылка = &Ссылка
	|	И НЕ &Корректировочная
	|	И &Исправление
	|	И &Организация <> ЗНАЧЕНИЕ(Справочник.Организации.УправленческаяОрганизация)
	|
	|СГРУППИРОВАТЬ ПО
	|	ТаблицаДанных.ВидЦенности,
	|	ТаблицаДанных.СтавкаНДС,
	|	ТаблицаДанных.Ссылка.СводнаяСправкаОснование
	|	
	|ИМЕЮЩИЕ
	|	СУММА(ТаблицаДанных.СуммаБезНДС) <> 0 
	|		ИЛИ СУММА(ТаблицаДанных.НДС) <> 0
	|";
#КонецОбласти

	ТекстЗапроса = ТекстЗапросаСторноИсправление
	+ " ОБЪЕДИНИТЬ ВСЕ " + ТекстЗапросаПродажи
	+ " ОБЪЕДИНИТЬ ВСЕ " + ТекстПродажиИсправление;
	
	ТекстыЗапроса.Добавить(ТекстЗапроса, ИмяРегистра);
	Возврат ТекстЗапроса
	
КонецФункции

Функция СводнаяСправкаПредыдущееИсправление(СводнаяСправкаОснование, НомерИсправления) Экспорт
	
	Запрос = Новый Запрос;
	Запрос.Текст =
	"ВЫБРАТЬ
	|	СводнаяСправкаНДС.Ссылка КАК ИсправленнаяСводнаяСправка,
	|	СводнаяСправкаНДС.НомерИсправления КАК НомерИсправления
	|ИЗ
	|	Документ.СводнаяСправкаНДС КАК СводнаяСправкаНДС
	|ГДЕ
	|	СводнаяСправкаНДС.СводнаяСправкаОснование = &СводнаяСправкаОснование
	|	И СводнаяСправкаНДС.Проведен
	|	И СводнаяСправкаНДС.Исправление
	|	И СводнаяСправкаНДС.НомерИсправления <> &НомерИсправления
	|";
	
	Запрос.УстановитьПараметр("СводнаяСправкаОснование", СводнаяСправкаОснование);
	Запрос.УстановитьПараметр("НомерИсправления", НомерИсправления);
	
	ПредыдущийНомер = 0;
	ИсправленнаяСводнаяСправка = Неопределено;
	НомерИсправленияЧислом = ?(ЗначениеЗаполнено(НомерИсправления),Число(НомерИсправления),1);
	
	Выборка = Запрос.Выполнить().Выбрать();
	Пока Выборка.Следующий() Цикл
		НомерИзВыборки = Число(Выборка.НомерИсправления);
		Если НомерИзВыборки < НомерИсправленияЧислом
			И НомерИзВыборки > ПредыдущийНомер Тогда
			ПредыдущийНомер = НомерИзВыборки;
			ИсправленнаяСводнаяСправка = Выборка.ИсправленнаяСводнаяСправка;
		КонецЕсли;
	КонецЦикла;
	
	Возврат ИсправленнаяСводнаяСправка
	
КонецФункции

Функция АдаптированныйТекстЗапросаДвиженийПоРегистру(ИмяРегистра) Экспорт
	
	Запрос = Новый Запрос;
	ТекстыЗапроса = Новый СписокЗначений;
	
	ПолноеИмяДокумента      = "Документ.СводнаяСправкаНДС";
	СинонимТаблицыДокумента = "";
	ВЗапросеЕстьИсточник    = Истина;
	
	ПереопределениеРасчетаПараметров = Новый Структура;
	ПереопределениеРасчетаПараметров.Вставить("НомерНаПечать",           """""");
	ПереопределениеРасчетаПараметров.Вставить("ИнформацияПоИсправлению", """""");
	
	ЗначенияПараметров = ЗначенияПараметровПроведения();
	
	Если ИмяРегистра = "РеестрДокументов" Тогда
		Запрос.Параметры.Вставить("НомерНаПечать","""""");
		ТекстЗапроса = ТекстЗапросаТаблицаРеестрДокументов(Запрос, ТекстыЗапроса, ИмяРегистра);
		СинонимТаблицыДокумента = "ДанныеДокумента";
	Иначе
		ТекстИсключения = НСтр("ru = 'В документе %ПолноеИмяДокумента% не реализована адаптация текста запроса формирования движений по регистру %ИмяРегистра%.';
								|en = 'In document %ПолноеИмяДокумента%, adaptation of request for generating records of register %ИмяРегистра% is not implemented.'");
		ТекстИсключения = СтрЗаменить(ТекстИсключения, "%ПолноеИмяДокумента%", ПолноеИмяДокумента);
		ТекстИсключения = СтрЗаменить(ТекстИсключения, "%ИмяРегистра%", ИмяРегистра);
		
		ВызватьИсключение ТекстИсключения;
	КонецЕсли;
	
	Если ИмяРегистра = "РеестрДокументов" Тогда
		
		ТекстЗапроса = ОбновлениеИнформационнойБазыУТ.АдаптироватьЗапросПроведенияПоНезависимомуРегистру(
			ТекстЗапроса,
			ПолноеИмяДокумента,
			СинонимТаблицыДокумента,
			ВЗапросеЕстьИсточник,
			ПереопределениеРасчетаПараметров);
		
	Иначе
		
		ТекстЗапроса = ОбновлениеИнформационнойБазыУТ.АдаптироватьЗапросМеханизмаПроведения(
			ТекстЗапроса,
			ПолноеИмяДокумента,
			СинонимТаблицыДокумента,
			ПереопределениеРасчетаПараметров);
		
	КонецЕсли;
	
	Результат = ОбновлениеИнформационнойБазыУТ.РезультатАдаптацииЗапроса();
	Результат.ЗначенияПараметров = ЗначенияПараметров;
	Результат.ТекстЗапроса = ТекстЗапроса;
	
	Возврат Результат;
	
КонецФункции

#КонецОбласти

#Область Прочее

Функция ТекстЗапросаТаблицаРеестрДокументов(Запрос, ТекстыЗапроса, Регистры)
	
	ИмяРегистра = "РеестрДокументов";
	
	Если НЕ ПроведениеСерверУТ.ТребуетсяТаблицаДляДвижений(ИмяРегистра, Регистры) Тогда
		Возврат "";
	КонецЕсли;
	
	ТекстЗапроса = 
	"ВЫБРАТЬ
	|	&Ссылка                  КАК Ссылка,
	|	&Период                  КАК ДатаДокументаИБ,
	|	&Номер                   КАК НомерДокументаИБ,
	|	&ИдентификаторМетаданных КАК ТипСсылки,
	|	&Организация             КАК Организация,
	|	НЕОПРЕДЕЛЕНО             КАК Контрагент,
	|	НЕОПРЕДЕЛЕНО             КАК Подразделение,
	|	НЕОПРЕДЕЛЕНО             КАК Ответственный,
	|	""""                     КАК Комментарий,
	|	НЕОПРЕДЕЛЕНО             КАК Валюта,
	|	0                        КАК Сумма,
	|	НЕОПРЕДЕЛЕНО             КАК Статус,
	|	&Проведен                КАК Проведен,
	|	&ПометкаУдаления         КАК ПометкаУдаления,
	|	&ИнформацияПоИсправлению КАК Дополнительно,
	|	&Период                  КАК ДатаПервичногоДокумента,
	|	&НомерНаПечать           КАК НомерПервичногоДокумента,
	|	НЕОПРЕДЕЛЕНО             КАК МестоХранения,
	|	НЕОПРЕДЕЛЕНО             КАК НаправлениеДеятельности,
	|	НЕОПРЕДЕЛЕНО             КАК Партнер,
	|	НЕОПРЕДЕЛЕНО             КАК Договор,
	|	&Период                  КАК ДатаОтраженияВУчете,
	|	ЛОЖЬ                     КАК ДополнительнаяЗапись,
	|	ЗНАЧЕНИЕ(Перечисление.ХозяйственныеОперации.РеализацияВРозницу) КАК ХозяйственнаяОперация
	|ИЗ
	|	Документ.СводнаяСправкаНДС КАК ДанныеДокумента
	|ГДЕ
	|	ДанныеДокумента.Ссылка = &Ссылка
	|";
	
	ТекстыЗапроса.Добавить(ТекстЗапроса, ИмяРегистра);
	
	Возврат ТекстЗапроса;
	
КонецФункции

#КонецОбласти

#КонецОбласти

#КонецЕсли
