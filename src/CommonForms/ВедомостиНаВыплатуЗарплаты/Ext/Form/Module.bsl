﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Свойство("АвтоТест") Тогда // Возврат при получении формы для анализа.
		Возврат;
	КонецЕсли;
	
	Если Параметры.Свойство("ВедомостиДляПеречисленияНалога") Тогда
		ВедомостиДляПеречисленияНалога = Параметры.ВедомостиДляПеречисленияНалога;
		
		Элементы.СписокСумма.Видимость = Не ВедомостиДляПеречисленияНалога;
	КонецЕсли;
	
	Если Параметры.Свойство("ОтборВедомостей") Тогда
		УстановитьОтбор(Параметры.ОтборВедомостей);
		
		Если Параметры.ОтборВедомостей.Свойство("ХозяйственнаяОперация") Тогда
		    Если Параметры.ОтборВедомостей.ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ВыплатаЗарплатыПоЗарплатномуПроекту Тогда
			    Элементы.ЗарплатныйПроект.Видимость = Истина;
			КонецЕсли; 
		КонецЕсли; 
	КонецЕсли;
	
	СобытияФорм.ПриСозданииНаСервере(ЭтаФорма, Отказ, СтандартнаяОбработка);

	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыСписок

&НаКлиенте
Процедура СписокВыборЗначения(Элемент, Значение, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	
	ТекущиеДанные = Элементы.Список.ТекущиеДанные;
	
	Если ТекущиеДанные <> Неопределено Тогда
		Если ВедомостиДляПеречисленияНалога Тогда
			ЗначениеВыбора = Новый Структура("Ведомость, Сумма, Получатель",
				ТекущиеДанные.Ссылка, ТекущиеДанные.СуммаНДФЛ, ТекущиеДанные.Получатель);
		Иначе	
			ЗначениеВыбора = Новый Структура("Ведомость, Сумма, Получатель",
				ТекущиеДанные.Ссылка, ТекущиеДанные.Сумма, ТекущиеДанные.Получатель);
		КонецЕсли; 
		ОповеститьОВыборе(ЗначениеВыбора);
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура УстановитьОтбор(ОтборВедомостей)
	
	МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
	
	УстановитьПривилегированныйРежим(Истина);
	ДенежныеСредстваСервер.ДанныеОбОплатеВедомостей(МенеджерВременныхТаблиц, ОтборВедомостей);
	
	Запрос = Новый Запрос();
	Запрос.МенеджерВременныхТаблиц = МенеджерВременныхТаблиц;
	Запрос.Текст = 
	"ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	ВТВедомости.Ведомость КАК Ведомость
	|ИЗ
	|	ДанныеВедомостейНаОплату КАК ВТВедомости";
	
	Выборка = Запрос.Выполнить().Выбрать();
	
	МассивВедомостей = Новый Массив;
	Пока Выборка.Следующий() Цикл
		МассивВедомостей.Добавить(Выборка.Ведомость);
	КонецЦикла;
	
	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(
		Список,
		"Ссылка",
		МассивВедомостей,
		ВидСравненияКомпоновкиДанных.ВСписке,,
		Истина);
	
КонецПроцедуры

#КонецОбласти