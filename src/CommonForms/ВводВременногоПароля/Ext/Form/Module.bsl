﻿&НаКлиенте
Перем ИдентификаторСессии;

#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	// Пропускаем инициализацию, чтобы гарантировать получение формы при передаче параметра "АвтоТест".
	Если Параметры.Свойство("АвтоТест") Тогда
		Возврат;
	КонецЕсли;
	
	Сертификат = Параметры.Сертификат;
	
	Если Не ЗначениеЗаполнено(Сертификат) Тогда
		Отказ = Истина;
		Возврат;
	КонецЕсли;
	
	СпособДоставкиПаролей = "phone";
	
	ПараметрыПроцедуры = Новый Структура("ИдентификаторСертификата", Сертификат.Идентификатор);
	
	ДлительнаяОперация = СервисКриптографииСлужебный.ВыполнитьВФоне(
		"СервисКриптографииСлужебный.ПолучитьНастройкиПолученияВременныхПаролей", ПараметрыПроцедуры);
	
	ИспользоватьТокен = ЭлектроннаяПодписьВМоделиСервиса.ДолговременныйТокенИспользованиеВозможно();
	ПарольОтправляется = Истина;
	Элементы.ПолучитьВременныйПарольДругимСпособом.Видимость = Ложь;	
	Телефон = "...";
	
	Элементы.НастроитьПодтверждение.Заголовок = ПолучитьЗаголовокНастроек(ИспользоватьТокен, НСтр("ru = 'Изменить получателя';
																									|en = 'Change the recipient '"));
	Элементы.НастроитьПодтверждение.РасширеннаяПодсказка.Заголовок = ?(ИспользоватьТокен, 
				НСтр("ru = 'Изменить настройки подтверждения операций с ключом';
					|en = 'Change settings of confirming operations with the key'"), "");
	УправлениеФормой(ЭтаФорма);

КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	ИдентификаторСессии = Неопределено;
	Оповещение = Новый ОписаниеОповещения("ПолучитьНастройкиПолученияВременныхПаролейПослеВыполнения", ЭтотОбъект);
	СервисКриптографииСлужебныйКлиент.ОжидатьЗавершенияВыполненияВФоне(Оповещение, ДлительнаяОперация);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура ПарольПриИзменении(Элемент)
	
	Если Элементы.ГруппаПароль.Доступность Тогда 
		ПарольИзменениеТекстаРедактирования(Элемент, Элемент.ТекстРедактирования, Истина);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПарольИзменениеТекстаРедактирования(Элемент, Текст, СтандартнаяОбработка)
	
	Пароль = СокрЛП(Текст);
	Если ЗначениеЗаполнено(Пароль) И СтрДлина(Пароль) = 6 Тогда
		ОтключитьОбработчикОжидания("Подключаемый_ПроверитьПароль");
		ПодключитьОбработчикОжидания("Подключаемый_ПроверитьПароль", 0.5, Истина);
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ЗакрытьОткрытую(ПараметрыФормы)
	
	ОтключитьОбработчикОжидания("Подключаемый_ПроверитьПароль");
	Если Открыта() Тогда 
		Закрыть(ПараметрыФормы);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ОтправитьПарольПовторно(Команда)

	Пароль = Неопределено;
	Оповещение = Новый ОписаниеОповещения("ПолучитьВременныйПарольПослеВыполнения", ЭтотОбъект);
		СервисКриптографииСлужебныйКлиент.ОжидатьЗавершенияВыполненияВФоне(Оповещение, ПолучитьПарольНаСервере(Истина, ИдентификаторСессии));
		
	УправлениеФормой(ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура Подтвердить(Команда)
	
	ОтключитьОбработчикОжидания("Подключаемый_ПроверитьПароль");
	ОчиститьСообщения();
	ТекстОшибки = "";
	Пароль = СокрЛП(Пароль);
	Если ЗначениеЗаполнено(Пароль) И СтрДлина(Пароль) = 6 Тогда
		Оповещение = Новый ОписаниеОповещения("ПодтвердитьПарольПослеВыполнения", ЭтотОбъект);
		СервисКриптографииСлужебныйКлиент.ОжидатьЗавершенияВыполненияВФоне(Оповещение, ПодтвердитьНаСервере(ИдентификаторСессии));
		УправлениеФормой(ЭтаФорма);
	ИначеЕсли ЗначениеЗаполнено(Пароль) И СтрДлина(Пароль) <> 6 Тогда
		ТекстОшибки = НСтр("ru = 'Пароль должен состоять из 6 цифр';
							|en = 'The password must contain 6 characters'");
	Иначе
		ТекстОшибки = НСтр("ru = 'Пароль не указан';
							|en = 'Password is not specified'");
	КонецЕсли;

КонецПроцедуры

&НаКлиенте
Процедура ПолучитьВременныйПарольДругимСпособом(Команда)
	
	Если СпособДоставкиПаролей = "phone" Тогда
		СпособДоставкиПаролей = "email";
	Иначе
		СпособДоставкиПаролей = "phone";
	КонецЕсли;
	
	ОтправитьПарольПовторно(Неопределено);
	
	УправлениеФормой(ЭтаФорма);

КонецПроцедуры

&НаКлиенте
Процедура НастроитьПодтверждение(Команда)
	
	ПараметрыФормы = Новый Структура;
	ПараметрыФормы.Вставить("Сертификат", Сертификат);
	Если ИспользоватьТокен Тогда
		ПараметрыФормы.Вставить("Состояние", "ИзменениеСпособаПодтвержденияКриптооперации");
	Иначе
		ПараметрыФормы.Вставить("Состояние", "ИзменениеНастроекПолученияВременныхПаролей");
	КонецЕсли;	
	
	ЗакрытьОткрытую(ПараметрыФормы);

КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаКлиенте
Процедура ПодтвердитьПарольПослеВыполнения(Результат, ВходящийКонтекст) Экспорт

	Результат = СервисКриптографииСлужебныйКлиент.ПолучитьРезультатВыполненияВФоне(Результат);
	
	Если Результат.Выполнено Тогда
		СохранитьМаркерыБезопасности(Результат.РезультатВыполнения);
		ЗакрытьОткрытую(Новый Структура("Состояние", "ПарольПринят"));
	Иначе
		ПарольПроверяется = Ложь;
		УправлениеФормой(ЭтаФорма);
		
		ТекстИсключения = ПодробноеПредставлениеОшибки(Результат.ИнформацияОбОшибке);
		Если СтрНайти(ТекстИсключения, "УказанНеверныйПароль") Тогда
			ТекстОшибки = НСтр("ru = 'Указан неверный пароль';
								|en = 'The password is incorrect'");
		ИначеЕсли СтрНайти(ТекстИсключения, "ПревышенЛимитПопытокВводаПароля") Тогда
			ЗакрытьОткрытую(Новый Структура("Состояние, ОписаниеОшибки", "ПарольНеПринят", НСтр("ru = 'Превышен лимит попыток ввода пароля';
																								|en = 'Exceeded number of attempts to enter password'"))); 
		ИначеЕсли СтрНайти(ТекстИсключения, "СрокДействияПароляИстек") Тогда
			ЗакрытьОткрытую(Новый Структура("Состояние, ОписаниеОшибки", "ПарольНеПринят", НСтр("ru = 'Срок действия пароля истек';
																								|en = 'Password expired'")));
		Иначе 
			ЗакрытьОткрытую(Новый Структура("Состояние, ОписаниеОшибки", "ПарольНеПринят", НСтр("ru = 'Выполнение операции временно невозможно';
																								|en = 'Operation temporarily cannot be executed'")));
		КонецЕсли;		
	КонецЕсли;
	
КонецПроцедуры

&НаСервереБезКонтекста
Процедура СохранитьМаркерыБезопасности(Знач МаркерыБезопасности)
	
	УстановитьПривилегированныйРежим(Истина);
	СервисКриптографииСлужебный.СохранитьМаркерыБезопасности(МаркерыБезопасности);
	УстановитьПривилегированныйРежим(Ложь);
	
КонецПроцедуры

&НаКлиенте
Процедура ПолучитьНастройкиПолученияВременныхПаролейПослеВыполнения(Результат, ВходящийКонтекст) Экспорт
	
	Результат = СервисКриптографииСлужебныйКлиент.ПолучитьРезультатВыполненияВФоне(Результат);
	
	Если Результат.Выполнено Тогда
		Телефон          = Результат.РезультатВыполнения.Телефон;
		ЭлектроннаяПочта = Результат.РезультатВыполнения.ЭлектроннаяПочта;
		
		Получатель = Телефон;
		
		Оповещение = Новый ОписаниеОповещения("ПолучитьВременныйПарольПослеВыполнения", ЭтотОбъект);
		СервисКриптографииСлужебныйКлиент.ОжидатьЗавершенияВыполненияВФоне(Оповещение, ПолучитьПарольНаСервере());
	Иначе
		Оповещение = Новый ОписаниеОповещения("ПослеПоказаПредупреждения", ЭтотОбъект);
		ПоказатьПредупреждение(Оповещение, НСтр("ru = 'Сервис отправки SMS-сообщений временно недоступен. Повторите попытку позже.';
												|en = 'SMS service is temporarily unavailable. Try again later.'"));		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПослеПоказаПредупреждения(ВходящийКонтекст) Экспорт
	
	ЗакрытьОткрытую(Неопределено);
	
КонецПроцедуры

&НаКлиенте
Процедура ПолучитьВременныйПарольПослеВыполнения(Результат, ВходящийКонтекст) Экспорт
	
	Результат = СервисКриптографииСлужебныйКлиент.ПолучитьРезультатВыполненияВФоне(Результат);
	
	Если Результат.Выполнено Тогда
		Таймер = Результат.РезультатВыполнения.ЗадержкаПередПовторнойОтправкой;
		ПарольОтправлен = Истина;
		ПарольОтправляется = Ложь;
		Если Результат.РезультатВыполнения.Свойство("ИдентификаторСессии") Тогда 
			ИдентификаторСессии = Результат.РезультатВыполнения.ИдентификаторСессии;
		КонецЕсли;
		ЗапуститьОбратныйОтсчет();
		УправлениеФормой(ЭтаФорма);
		ПодключитьОбработчикОжидания("Подключаемый_АктивироватьПолеДляВводаПароля", 0.1, Истина);
	Иначе
		Оповещение = Новый ОписаниеОповещения("ПослеПоказаПредупреждения", ЭтотОбъект);
		ПоказатьПредупреждение(Оповещение, НСтр("ru = 'Сервис отправки SMS-сообщений временно недоступен. Повторите попытку позже.';
												|en = 'SMS service is temporarily unavailable. Try again later.'"));
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ПроверитьПароль()
	
	Подтвердить(Неопределено);
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_АктивироватьПолеДляВводаПароля()
	
	Пароль = Неопределено;
	Элементы.Пароль.ОбновитьТекстРедактирования();
	ТекущийЭлемент = Элементы.Пароль;
	
КонецПроцедуры

&НаСервере
Функция ПолучитьПарольНаСервере(Повторно = Ложь, ИдентификаторСессии = Неопределено)
	
	ПараметрыПроцедуры = Новый Структура;
	ПараметрыПроцедуры.Вставить("ИдентификаторСертификата", Сертификат.Идентификатор);
	ПараметрыПроцедуры.Вставить("ПовторнаяОтправка", Повторно);
	ПараметрыПроцедуры.Вставить("Тип", СпособДоставкиПаролей);
	ПараметрыПроцедуры.Вставить("ИдентификаторСессии", ИдентификаторСессии);
	
	Возврат СервисКриптографииСлужебный.ВыполнитьВФоне(
		"СервисКриптографииСлужебный.ПолучитьВременныйПароль", ПараметрыПроцедуры);
	
КонецФункции

&НаКлиенте
Процедура ЗапуститьОбратныйОтсчет()
	
	ПодключитьОбработчикОжидания("Подключаемый_ОбработчикОбратногоОтсчета", 1, Истина);
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ОбработчикОбратногоОтсчета()
	
	Таймер = Таймер - 1;
	Если Таймер >= 0 Тогда
		НадписьОбратногоОтсчета = СтрШаблон(НСтр("ru = 'Запросить пароль повторно можно будет через %1 сек.';
												|en = 'You can request the password again in %1 sec.'"), Таймер);
		ПодключитьОбработчикОжидания("Подключаемый_ОбработчикОбратногоОтсчета", 1, Истина);		
	Иначе
		НадписьОбратногоОтсчета = "";
		ПарольОтправлен = Ложь;		
		НовыйЗаголовок	= НСтр("ru = 'Изменить адрес';
								|en = 'Change the address '");
		Элементы.ПолучитьВременныйПарольДругимСпособом.Видимость = ЗначениеЗаполнено(ЭлектроннаяПочта);
		Элементы.НастроитьПодтверждение.Заголовок = ПолучитьЗаголовокНастроек(ИспользоватьТокен, НовыйЗаголовок);
		
		УправлениеФормой(ЭтаФорма);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Функция ПолучитьЗаголовокНастроек(ТекущийРежим, НовыйЗаголовок)
	
	Результат = "Настройки";
	Если НЕ ТекущийРежим Тогда
		Результат = НовыйЗаголовок;
	КонецЕсли;
	
	Возврат Результат;
	
КонецФункции	

&НаКлиентеНаСервереБезКонтекста
Процедура УправлениеФормой(Форма)
	
	Элементы = Форма.Элементы;
	
	Элементы.НадписьОбратногоОтсчета.Видимость = Форма.ПарольОтправлен;	
	Элементы.ОтправитьПарольПовторно.Видимость = Не Форма.ПарольОтправлен И Не Форма.ПарольОтправляется;
	Элементы.ДекорацияОтступ.Видимость = Элементы.ПолучитьВременныйПарольДругимСпособом.Видимость;
	
	ТекстКоманды = НСтр("ru = 'Отправить пароль на %1';
						|en = 'Send password to %1'");
	Если Форма.СпособДоставкиПаролей = "phone" Тогда
		Если Форма.ПарольОтправляется Тогда
			Элементы.Пояснение.Заголовок = НСтр("ru = 'Выполняется отправка пароля в SMS-сообщении на номер';
												|en = 'Sending SMS message with password to the number'");
		Иначе
			Элементы.Пояснение.Заголовок = НСтр("ru = 'Пароль отправлен в SMS-сообщении на номер';
												|en = 'SMS message with password is sent to the number'");
		КонецЕсли;
		НовыйЗаголовок = НСтр("ru = 'Изменить номер';
								|en = 'Change the number'");
		Элементы.НастроитьПодтверждение.Заголовок = ПолучитьЗаголовокНастроек(Форма.ИспользоватьТокен, НовыйЗаголовок);
		Элементы.ПолучитьВременныйПарольДругимСпособом.Заголовок = СтрШаблон(ТекстКоманды, Форма.ЭлектроннаяПочта);
		Форма.Получатель = Форма.Телефон;
		Элементы.Пароль.ПодсказкаВвода = НСтр("ru = 'Введите пароль из SMS';
												|en = 'Enter password from SMS message'");
	ИначеЕсли Форма.СпособДоставкиПаролей = "email" Тогда
		Если Форма.ПарольОтправляется Тогда
			Элементы.Пояснение.Заголовок = НСтр("ru = 'Выполняется отправка пароля в письме на адрес';
												|en = 'Sending email with password to the address'");
		Иначе
			Элементы.Пояснение.Заголовок = НСтр("ru = 'Пароль отправлен в письме на адрес';
												|en = 'Email with password is sent to the address'");
		КонецЕсли;	
		НовыйЗаголовок = НСтр("ru = 'Изменить адрес';
								|en = 'Change the address '");
		Элементы.НастроитьПодтверждение.Заголовок = ПолучитьЗаголовокНастроек(Форма.ИспользоватьТокен, НовыйЗаголовок);
		Элементы.ПолучитьВременныйПарольДругимСпособом.Заголовок = СтрШаблон(ТекстКоманды, Форма.Телефон);
		Форма.Получатель = Форма.ЭлектроннаяПочта;
		Элементы.Пароль.ПодсказкаВвода = НСтр("ru = 'Введите пароль из письма';
												|en = 'Enter a password from the email'");
	КонецЕсли;
	
	Элементы.ИндикаторДлительнойОперации.Видимость = Форма.ПарольОтправляется;
	Элементы.ГруппаПароль.Доступность = Не Форма.ПарольОтправляется И Не Форма.ПарольПроверяется;		
	Элементы.ГруппаДополнительно.Доступность = Не Форма.ПарольОтправляется И Не Форма.ПарольПроверяется;
		
	Элементы.ИндикаторДлительнойОперации2.Видимость = Форма.ПарольПроверяется;
	Элементы.НадписьПроверкаПароля.Видимость = Форма.ПарольПроверяется;
	Элементы.ТекстОшибки.Видимость = Не Форма.ПарольПроверяется;
	
КонецПроцедуры

&НаСервере
Функция ПодтвердитьНаСервере(ИдентификаторСессии = Неопределено)
	
	ПарольПроверяется = Истина;

	ПараметрыПроцедуры = Новый Структура;
	ПараметрыПроцедуры.Вставить("ИдентификаторСертификата", Сертификат.Идентификатор);
	ПараметрыПроцедуры.Вставить("ВременныйПароль", Пароль);
	ПараметрыПроцедуры.Вставить("ИдентификаторСессии", ИдентификаторСессии);
	
	Возврат СервисКриптографииСлужебный.ВыполнитьВФоне(
		"СервисКриптографииСлужебный.ПолучитьСеансовыйКлюч", ПараметрыПроцедуры);
	
КонецФункции

#КонецОбласти