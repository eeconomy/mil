﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ОбработчикиСобытий

Процедура ПередЗаписью(Отказ)
	
	Если ЗарплатаКадры.ОтключитьБизнесЛогикуПриЗаписи(ЭтотОбъект) Тогда
		Возврат;
	КонецЕсли; 
	
	Если НЕ ЗначениеЗаполнено(ГоловнойСотрудник) Тогда
		
		СсылкаОбъекта = Неопределено;
		Если ДополнительныеСвойства.Свойство("СсылкаНового") Тогда
			СсылкаОбъекта = ДополнительныеСвойства.СсылкаНового;
			УстановитьСсылкуНового(СсылкаОбъекта);
		ИначеЕсли ЗначениеЗаполнено(Ссылка) Тогда
			СсылкаОбъекта = Ссылка;
		Иначе
			СсылкаОбъекта = Справочники.Сотрудники.ПолучитьСсылку();
			УстановитьСсылкуНового(СсылкаОбъекта);
		КонецЕсли; 
		
		ГоловнойСотрудник = СсылкаОбъекта;
		
	КонецЕсли; 
	
	ПрежниеЗначенияРеквизитов = ОбщегоНазначения.ЗначенияРеквизитовОбъекта(Ссылка, "ГоловнаяОрганизация,Код");
	
	Если ЭтоНовый() Тогда
		ОбновитьЗначенияДоступаПоГоловнойОрганизации(СсылкаОбъекта, ГоловнаяОрганизация, ПрежниеЗначенияРеквизитов.ГоловнаяОрганизация);
	Иначе
		ДополнительныеСвойства.Вставить("ГоловнаяОрганизацияПрежняя", ПрежниеЗначенияРеквизитов.ГоловнаяОрганизация);
	КонецЕсли;

	
	КодПредыдущий = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Ссылка, "Код");
	Если КодПредыдущий <> Код Тогда
		
		// Проверка уникальности кода справочника в пределах головного сотрудника.
		Запрос = Новый Запрос;
		Запрос.УстановитьПараметр("Код", Код);
		Запрос.УстановитьПараметр("Ссылка", Ссылка);
		Запрос.УстановитьПараметр("ГоловнойСотрудник", ГоловнойСотрудник);
		
		Запрос.Текст =
			"ВЫБРАТЬ ПЕРВЫЕ 1
			|	Сотрудники.Ссылка
			|ИЗ
			|	Справочник.Сотрудники КАК Сотрудники
			|ГДЕ
			|	Сотрудники.Код = &Код
			|	И Сотрудники.Ссылка <> &Ссылка
			|	И Сотрудники.ГоловнойСотрудник <> &ГоловнойСотрудник";
			
		РезультатЗапроса = Запрос.Выполнить();
		Если НЕ РезультатЗапроса.Пустой() Тогда
			
			ТекстИсключения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
				НСтр("ru='Значение ""%1"" поля ""Таб. номер"" не уникально'"),
				Код);
				
			ВызватьИсключение ТекстИсключения;
			
		КонецЕсли;
		
		// Синхронизация кодов всех сотрудников в пределах головного сотрудника.
		Запрос.Текст =
			"ВЫБРАТЬ
			|	Сотрудники.Ссылка
			|ИЗ
			|	Справочник.Сотрудники КАК Сотрудники
			|ГДЕ
			|	Сотрудники.ГоловнойСотрудник = &ГоловнойСотрудник
			|	И Сотрудники.Ссылка <> &Ссылка
			|	И Сотрудники.Код <> &Код";
		
		РезультатЗапроса = Запрос.Выполнить();
		Если НЕ РезультатЗапроса.Пустой() Тогда
			
			Выборка = РезультатЗапроса.Выбрать();
			Пока Выборка.Следующий() Цикл
				
				СотрудникОбъект = Выборка.Ссылка.ПолучитьОбъект();
			
				Попытка 
					СотрудникОбъект.Заблокировать();
				Исключение
					
					ТекстИсключенияЗаписи = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
						НСтр("ru = 'Не удалось изменить Таб. номер ""%1"".
						|Возможно, данные редактируется другим пользователем'"),
						СотрудникОбъект.Наименование);
						
					ВызватьИсключение ТекстИсключенияЗаписи;
					
				КонецПопытки;
				
				СотрудникОбъект.Код = Код;
				СотрудникОбъект.ОбменДанными.Загрузка = Истина;
				СотрудникОбъект.Записать();
			
			КонецЦикла;
			
		КонецЕсли;
		
	КонецЕсли;
	
КонецПроцедуры

Процедура ПриКопировании(ОбъектКопирования)
	
	ГоловнойСотрудник = Ссылка;
	Наименование = "";
	ФизическоеЛицо = Справочники.ФизическиеЛица.ПустаяСсылка();
	ТекущийПроцентСевернойНадбавки = 0;
	УточнениеНаименования = "";
	ВАрхиве = Ложь;
	
КонецПроцедуры

Процедура ОбработкаПроверкиЗаполнения(Отказ, ПроверяемыеРеквизиты)
	
	Если НЕ ПолучитьФункциональнуюОпцию("ИспользоватьКадровыйУчет") Тогда
		
		ОбщегоНазначения.УдалитьНепроверяемыеРеквизитыИзМассива(
			ПроверяемыеРеквизиты,
			ОбщегоНазначенияКлиентСервер.ЗначениеВМассиве("ГоловнаяОрганизация"));
		
	КонецЕсли; 

КонецПроцедуры

Процедура ПриЗаписи(Отказ)
	
	Если ЗарплатаКадры.ОтключитьБизнесЛогикуПриЗаписи(ЭтотОбъект) Тогда
		Возврат;
	КонецЕсли;
	
	Если ДополнительныеСвойства.Свойство("ГоловнаяОрганизацияПрежняя") Тогда
		ОбновитьЗначенияДоступаПоГоловнойОрганизации(Ссылка, ГоловнаяОрганизация, ДополнительныеСвойства.ГоловнаяОрганизацияПрежняя);
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти


#Область СлужебныеПроцедурыИФункции

Процедура ОбновитьЗначенияДоступаПоГоловнойОрганизации(Сотрудник, ГоловнаяОрганизация, ГоловнаяОрганизацияПредыдущая)
	
		ТаблицаАнализаИзменений = КадровыйУчет.ТаблицаАнализаИзменений();
		
		Если ЗначениеЗаполнено(ГоловнаяОрганизацияПредыдущая) Тогда
			
			СтрокаТаблицы = ТаблицаАнализаИзменений.Добавить();
			СтрокаТаблицы.Сотрудник = Сотрудник;
			СтрокаТаблицы.Организация = ГоловнаяОрганизацияПредыдущая;
			СтрокаТаблицы.ФлагИзменений = -1;
			
		КонецЕсли;
		
		СтрокаТаблицы = ТаблицаАнализаИзменений.Добавить();
		СтрокаТаблицы.Сотрудник = Сотрудник;
		СтрокаТаблицы.Организация = ГоловнаяОрганизация;
		СтрокаТаблицы.ФлагИзменений = 1;
		
		КадровыйУчет.ОбработатьИзменениеОрганизацийВНабореПоТаблицеИзменений(ТаблицаАнализаИзменений);
		
КонецПроцедуры


// СтандартныеПодсистемы.УправлениеДоступом

// Процедура ЗаполнитьНаборыЗначенийДоступа по свойствам объекта заполняет наборы значений доступа
// в таблице с полями:
//    НомерНабора     - Число                                     (необязательно, если набор один),
//    ВидДоступа      - ПланВидовХарактеристикСсылка.ВидыДоступа, (обязательно),
//    ЗначениеДоступа - Неопределено, СправочникСсылка или др.    (обязательно),
//    Чтение          - Булево (необязательно, если набор для всех прав) устанавливается для одной строки набора,
//    Добавление      - Булево (необязательно, если набор для всех прав) устанавливается для одной строки набора,
//    Изменение       - Булево (необязательно, если набор для всех прав) устанавливается для одной строки набора,
//    Удаление        - Булево (необязательно, если набор для всех прав) устанавливается для одной строки набора,
//
//  Вызывается из процедуры УправлениеДоступомСлужебный.ЗаписатьНаборыЗначенийДоступа(),
// если объект зарегистрирован в "ПодпискаНаСобытие.ЗаписатьНаборыЗначенийДоступа" и
// из таких же процедур объектов, у которых наборы значений доступа зависят от наборов этого
// объекта (в этом случае объект зарегистрирован в "ПодпискаНаСобытие.ЗаписатьЗависимыеНаборыЗначенийДоступа").
//
// Параметры:
//  Таблица      - ТабличнаяЧасть,
//                 РегистрСведенийНаборЗаписей.НаборыЗначенийДоступа,
//                 ТаблицаЗначений, возвращаемая УправлениеДоступом.ТаблицаНаборыЗначенийДоступа().
//
Процедура ЗаполнитьНаборыЗначенийДоступа(Таблица) Экспорт
	
	УстановитьПривилегированныйРежим(Истина);
	
	Запрос = КадровыйУчет.ЗапросВТВсеЗначенияДоступаСотрудников(ОбщегоНазначенияКлиентСервер.ЗначениеВМассиве(Ссылка));
	Запрос.МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
	Запрос.Выполнить();
	
	Запрос.УстановитьПараметр("ГоловнаяОрганизация", ГоловнаяОрганизация);
	
	Если ДополнительныеСвойства.Свойство("ТекущаяОрганизация") 
		И ЗначениеЗаполнено(ДополнительныеСвойства.ТекущаяОрганизация) Тогда
		
		ТекущаяОрганизация = ДополнительныеСвойства.ТекущаяОрганизация;
		
	Иначе
		ТекущаяОрганизация = Справочники.Организации.ПустаяСсылка();
	КонецЕсли;
	
	Запрос.УстановитьПараметр("ТекущаяОрганизация", ТекущаяОрганизация);
	
	Запрос.Текст =
		"ВЫБРАТЬ РАЗЛИЧНЫЕ
		|	ВсеЗначенияДоступаСотрудников.ЗначениеДоступа
		|ПОМЕСТИТЬ ВТЗначенияДоступаСотрудника
		|ИЗ
		|	ВТВсеЗначенияДоступаСотрудников КАК ВсеЗначенияДоступаСотрудников
		|
		|ОБЪЕДИНИТЬ ВСЕ
		|
		|ВЫБРАТЬ
		|	&ГоловнаяОрганизация
		|ГДЕ
		|	&ГоловнаяОрганизация <> ЗНАЧЕНИЕ(Справочник.Организации.ПустаяСсылка)
		|
		|ОБЪЕДИНИТЬ ВСЕ
		|
		|ВЫБРАТЬ
		|	&ТекущаяОрганизация
		|ГДЕ
		|	&ТекущаяОрганизация <> ЗНАЧЕНИЕ(Справочник.Организации.ПустаяСсылка)
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ РАЗЛИЧНЫЕ
		|	ЗначенияДоступаСотрудника.ЗначениеДоступа
		|ИЗ
		|	ВТЗначенияДоступаСотрудника КАК ЗначенияДоступаСотрудника";
		
	НомерНабора = 0;
	
	РезультатЗапроса = Запрос.Выполнить();
	Если НЕ РезультатЗапроса.Пустой() Тогда
			
		Выборка = РезультатЗапроса.Выбрать();
		Пока Выборка.Следующий() Цикл
			
			СтрокаТаб = Таблица.Добавить();
			СтрокаТаб.НомерНабора		= НомерНабора;
			СтрокаТаб.ЗначениеДоступа	= Выборка.ЗначениеДоступа;
			
			СтрокаТаб = Таблица.Добавить();
			СтрокаТаб.НомерНабора		= НомерНабора;
			СтрокаТаб.ЗначениеДоступа	= ФизическоеЛицо;
			
			НомерНабора = НомерНабора + 1;
			
		КонецЦикла;
			
	Иначе
	
		СтрокаТаб = Таблица.Добавить();
		СтрокаТаб.НомерНабора		= НомерНабора;
		СтрокаТаб.ЗначениеДоступа	= ФизическоеЛицо;
		
	КонецЕсли;
	
	УстановитьПривилегированныйРежим(Ложь);
	
КонецПроцедуры

// Конец СтандартныеПодсистемы.УправлениеДоступом

#КонецОбласти

#КонецЕсли

