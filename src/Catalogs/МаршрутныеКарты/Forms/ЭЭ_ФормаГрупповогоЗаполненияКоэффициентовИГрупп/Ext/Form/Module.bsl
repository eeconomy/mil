﻿#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	Если Не Параметры.Свойство("Команда") Тогда 
		ТекстСообщения = НСтр("ru='Форма предназначена только для программного открытия!'");
		ОбщегоНазначения.СообщитьПользователю(ТекстСообщения, , , , Отказ);
		Возврат;		
	КонецЕсли;
	
	НастроитьЭлементыФормы();
	
	УстановитьВидимостьЭлемента(Параметры.Команда);
КонецПроцедуры

&НаСервере
Процедура НастроитьЭлементыФормы()

	ВидимостьКоэффициент = Ложь;
	УстановитьВидимостьГруппы(Элементы.Коэффициент, 
											ВидимостьКоэффициент);
											
	ВидимостьГруппаЗаполнения = Ложь;
	УстановитьВидимостьГруппы(Элементы.ГруппаЗаполнения, 
											ВидимостьГруппаЗаполнения);										
																															
КонецПроцедуры

&НаСервере
Процедура УстановитьВидимостьЭлемента(ЭлементФормы)

	ЭтотОбъект["Видимость" + ЭлементФормы] = Не ЭтотОбъект["Видимость" + ЭлементФормы];	
	
	УстановитьВидимостьГруппы(Элементы[ЭлементФормы], ЭтотОбъект["Видимость" + ЭлементФормы]); 

КонецПроцедуры

&НаСервере
Процедура УстановитьВидимостьГруппы(ЭлементГруппа, ГруппаВидимость)

	ЭлементГруппа.Видимость = ГруппаВидимость;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура Применить(Команда)
	
	ЗаполнитьСтруктуруДляОтправки(); 
		
КонецПроцедуры 

&НаКлиенте
Процедура ЗаполнитьСтруктуруДляОтправки()

	Если ВидимостьКоэффициент Тогда
		РезультатСтруктура = Новый Структура;
		РезультатСтруктура.Вставить("Коэффициент", Коэффициент);	
	Иначе
		РезультатСтруктура = Новый Структура;
		РезультатСтруктура.Вставить("ГруппаЗаполнения", ГруппаЗаполнения);	
	КонецЕсли;
	
	Закрыть(РезультатСтруктура);
	
КонецПроцедуры

#КонецОбласти
