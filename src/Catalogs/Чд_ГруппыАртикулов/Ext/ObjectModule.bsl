﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ОбработчикиСобытий

Процедура ОбработкаПроверкиЗаполнения(Отказ, ПроверяемыеРеквизиты)
	
	ВыполнитьПроверкуУникальностиНаименования(Отказ);
	
	// ++EE:BAN 07.02.2023 MLVC-991
	Если Не Отказ И СоставнаяГруппа Тогда
		ПроверяемыеРеквизиты.Добавить("Группа31");
		ПроверяемыеРеквизиты.Добавить("Группа33");
	КонецЕсли;
	// --EE:BAN 07.02.2023 MLVC-991
	
КонецПроцедуры

Процедура ПередЗаписью(Отказ)
	
	Если ОбменДанными.Загрузка Тогда
		Возврат;
	КонецЕсли;
	
	Если НЕ ПометкаУдаления И Ссылка.ПометкаУдаления Тогда
		ВыполнитьПроверкуУникальностиНаименования(Отказ);
	КонецЕсли; 
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

Процедура ВыполнитьПроверкуУникальностиНаименования(Отказ)
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
		"ВЫБРАТЬ
		|	Чд_ГруппыАртикулов.Ссылка КАК Ссылка
		|ИЗ
		|	Справочник.Чд_ГруппыАртикулов КАК Чд_ГруппыАртикулов
		|ГДЕ
		|	Чд_ГруппыАртикулов.Наименование = &Наименование
		|	И НЕ Чд_ГруппыАртикулов.ПометкаУдаления";
	
	Запрос.УстановитьПараметр("Наименование", СокрЛП(Наименование));
	
	Выборка = Запрос.Выполнить().Выбрать();
	Пока Выборка.Следующий() Цикл
		Если Выборка.Ссылка <> Ссылка Тогда
			ТекстСообщения = "Группа артикула " + Наименование + " уже существует!";
			ОбщегоНазначения.СообщитьПользователю(ТекстСообщения, Выборка.Ссылка, , , Отказ);
		КонецЕсли; 	
	КонецЦикла; 
	
КонецПроцедуры

#КонецОбласти 

#КонецЕсли
