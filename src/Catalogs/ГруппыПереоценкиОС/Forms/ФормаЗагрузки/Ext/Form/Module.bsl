﻿
#Область ОбработчикиСобытийЭлементовФормы

&НаКлиенте
Процедура ФайлДанныхНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
    ПараметрыЗагрузки = ФайловаяСистемаКлиент.ПараметрыЗагрузкиФайла();
    ПараметрыЗагрузки.ИдентификаторФормы = УникальныйИдентификатор;
    
    ДиалогВыбораФайла = Новый ДиалогВыбораФайла(РежимДиалогаВыбораФайла.Открытие);
    ДиалогВыбораФайла.Заголовок = НСтр("ru = 'Выберите файл КОС.dbf'");
	ДиалогВыбораФайла.Фильтр	  = "Файл данных КОС|*.dbf";
	ДиалогВыбораФайла.Расширение= "dbf";
	ДиалогВыбораФайла.ПроверятьСуществованиеФайла = Истина;
	ДиалогВыбораФайла.МножественныйВыбор = Ложь;
	
    ПараметрыЗагрузки.Диалог = ДиалогВыбораФайла;
    
    ОповещениеЗавершенияВыбораФайла = Новый ОписаниеОповещения("ОбработкаВыбораФайла", ЭтотОбъект);
    
    ФайловаяСистемаКлиент.ЗагрузитьФайлы(ОповещениеЗавершенияВыбораФайла, ПараметрыЗагрузки);
    
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаВыбораФайла(Результат, ДополнительныеПараметры) Экспорт
    
    Если Результат = Неопределено Тогда
        Возврат;
    КонецЕсли;
    
	Для Каждого Файл Из Результат Цикл
		ФайлДанных = Файл.ПолноеИмя;
	КонецЦикла;
    
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура СнятьПометки(Команда)
	
	УстановитьЗначениеПометкиВДереве(0);
	
КонецПроцедуры

&НаКлиенте
Процедура УстановитьПометки(Команда)
	
	УстановитьЗначениеПометкиВДереве(1);
	
КонецПроцедуры

&НаКлиенте
Процедура ЗагрузитьГруппы(Команда)
	
	Файл = Новый ДвоичныеДанные(ФайлДанных);
	Адрес = ПоместитьВоВременноеХранилище(Файл, ЭтаФорма.УникальныйИдентификатор);
	
	ДеревоГрупп.ПолучитьЭлементы().Очистить();
	ЗагрузитьГруппыНаСервере(Адрес);

КонецПроцедуры

&НаКлиенте
Процедура ЗагрузитьКоэффициенты(Команда)
	
	Файл = Новый ДвоичныеДанные(ФайлДанных);
	Адрес = ПоместитьВоВременноеХранилище(Файл, ЭтаФорма.УникальныйИдентификатор);
	ЗагрузитьСтрокиНаСервере(Адрес);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыДеревоГрупп

&НаКлиенте
Процедура ДеревоГруппПометкаПриИзменении(Элемент)
	
	ТекущаяСтрокаДерева = Элементы.ДеревоГрупп.ТекущиеДанные;
	Если ТекущаяСтрокаДерева.Пометка = 2 Тогда
		ТекущаяСтрокаДерева.Пометка = 0;
	КонецЕсли;
	УстановитьПометкиВДереве(Элементы.ДеревоГрупп.ТекущиеДанные);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура УстановитьЗначениеПометкиВДереве(Знач ЗначениеПометки)
	
	УстановитьЗначениеПометкиВСтрокахДерева(ДеревоГрупп.ПолучитьЭлементы(), ЗначениеПометки);
	
КонецПроцедуры

&НаСервере
Процедура УстановитьЗначениеПометкиВСтрокахДерева(ЭлементыДерева, ЗначениеПометки)
	
	Для Каждого ЭлементДерева Из ЭлементыДерева Цикл
		ЭлементДерева.Пометка = ЗначениеПометки;
		ПодчиненныеЭлементыДерева = ЭлементДерева.ПолучитьЭлементы();
		Если ПодчиненныеЭлементыДерева.Количество() > 0 Тогда
			УстановитьЗначениеПометкиВСтрокахДерева(ПодчиненныеЭлементыДерева, ЗначениеПометки);
		КонецЕсли;
	КонецЦикла;
	
КонецПроцедуры

&НаСервере
Процедура ЗагрузитьГруппыНаСервере(Адрес)
	
	КлассификаторГрупп = Справочники.ГруппыПереоценкиОС.ПолучитьМакет("ГруппыПоПереоценке");
	
	Для Сч = 3 По КлассификаторГрупп.ВысотаТаблицы Цикл
		
		КодГруппы     = СокрЛП(КлассификаторГрупп.Область(Сч, 5, Сч, 5).Текст);
		КодПодГруппы  = СокрЛП(КлассификаторГрупп.Область(Сч, 6, Сч, 6).Текст);
		НаимГруппы    = СокрЛП(КлассификаторГрупп.Область(Сч, 7, Сч, 7).Текст);
		НаимПодГруппы = СокрЛП(КлассификаторГрупп.Область(Сч, 8, Сч, 8).Текст);
		
		Если СтрДлина(КодПодГруппы)=0 Тогда
			ДобавитьВДерево(КодГруппы,НаимГруппы,КодПодГруппы);
		иначе
			ДобавитьВДерево(КодГруппы,наимПодГруппы,КодПодГруппы);
		КонецЕсли;
		
	КонецЦикла;	
	
	СтараяСсылка = "";
	
	Запрос = Новый Запрос;
	Запрос.Текст = "
	| ВЫБРАТЬ
	|	ГруппыПереоценкиОСШифрыГруппыНС.Ссылка,
	|	ГруппыПереоценкиОСШифрыГруппыНС.НомерСтроки,
	|	ГруппыПереоценкиОСШифрыГруппыНС.НомерГруппы,
	|	ГруппыПереоценкиОСШифрыГруппыНС.ШифрНачала1,
	|	ГруппыПереоценкиОСШифрыГруппыНС.ШифрОкончания1
	|ИЗ
	|	Справочник.ГруппыПереоценкиОС.ШифрыГруппыНС КАК ГруппыПереоценкиОСШифрыГруппыНС";
	РезультатЗапроса = Запрос.Выполнить().Выгрузить();
	Если  РезультатЗапроса.Количество() = 0 Тогда
		НССОСПустой = Истина;
	Иначе
		НССОСПустой = Ложь;
	КонецЕсли;	
	
	Для Сч = 3 По КлассификаторГрупп.ВысотаТаблицы Цикл
		
		НачШифр1      = ?(СокрЛП(КлассификаторГрупп.Область(Сч, 1, Сч, 1).Текст) = "", "", Число(СокрЛП(КлассификаторГрупп.Область(Сч, 1, Сч, 1).Текст)));
		КонШифр1      = ?(СокрЛП(КлассификаторГрупп.Область(Сч, 2, Сч, 2).Текст) = "", "", Число(СокрЛП(КлассификаторГрупп.Область(Сч, 2, Сч, 2).Текст)));
		НачШифр       = ?(СокрЛП(КлассификаторГрупп.Область(Сч, 3, Сч, 3).Текст) = "", "", Число(СокрЛП(КлассификаторГрупп.Область(Сч, 3, Сч, 3).Текст)));
		КонШифр       = ?(СокрЛП(КлассификаторГрупп.Область(Сч, 4, Сч, 4).Текст) = "", "", Число(СокрЛП(КлассификаторГрупп.Область(Сч, 4, Сч, 4).Текст)));
		КодГруппы     = СокрЛП(КлассификаторГрупп.Область(Сч, 5, Сч, 5).Текст);
		КодПодГруппы  = СокрЛП(КлассификаторГрупп.Область(Сч, 6, Сч, 6).Текст);
		НаимГруппы    = СокрЛП(КлассификаторГрупп.Область(Сч, 7, Сч, 7).Текст);
		НаимПодГруппы = СокрЛП(КлассификаторГрупп.Область(Сч, 8, Сч, 8).Текст);
		
		Если СтрДлина(КодПодГруппы)=0 Тогда
			ДобавитьВДерево(КодГруппы,НаимГруппы,КодПодГруппы);
		иначе
			ДобавитьВДерево(КодГруппы,наимПодГруппы,КодПодГруппы);
		КонецЕсли;
		
		//Найдем группу(родителя) или добавим ее
		Родитель=Неопределено;
		Ссылка = Справочники.ГруппыПереоценкиОС.НайтиПоНаименованию(СокрП(КодГруппы));
		Если Ссылка.Пустая() Тогда
			Родитель = Справочники.ГруппыПереоценкиОС.СоздатьГруппу();
			Родитель.Наименование = СокрЛП(КодГруппы);
		Иначе
			Родитель = Ссылка.ПолучитьОбъект();
			Если ПереписыватьОбъекты Тогда
				Родитель.Наименование = СокрЛП(КодГруппы); 
			КонецЕсли;
		КонецЕсли;
		
		Попытка 
			Родитель.Записать();
		Исключение
			Сообщить("Ошибка при записи элемента: " + СокрЛП(КодГруппы)+ " !!!");
		КонецПопытки;
		
		КодГруппы2 = СокрЛП(КодГруппы) + "_" + СокрЛП(КодПодГруппы);
		
		Объект=Неопределено;
		Ссылка = Справочники.ГруппыПереоценкиОС.НайтиПоНаименованию(КодГруппы2);
		Если Ссылка.Пустая() Тогда
			Объект = Справочники.ГруппыПереоценкиОС.СоздатьЭлемент();
			Объект.Родитель = Родитель.Ссылка;
			Объект.Наименование = КодГруппы2;
			Объект.РасшифровкаГруппы = ?(СтрДлина(СокрЛП(НаимПодГруппы))=0, СокрЛП(НаимГруппы), СокрЛП(НаимПодГруппы));
		Иначе
			Объект = Ссылка.ПолучитьОбъект();
			
			Если ПереписыватьОбъекты ИЛИ НССОСПустой Тогда
				Объект.Родитель = Родитель.Ссылка;
				Объект.РасшифровкаГруппы = ?(СтрДлина(СокрЛП(НаимПодГруппы))=0, СокрЛП(НаимГруппы), СокрЛП(НаимПодГруппы));
				Если СтараяСсылка <> Ссылка Тогда
					Объект.шифрыгруппы.Очистить();
					СтараяСсылка = Ссылка;
				КонецЕсли;					
			КонецЕсли;
		КонецЕсли;	
		
		Попытка 
			Объект.Записать();
		Исключение
			Сообщить("Ошибка при записи элемента: " + СокрЛП(КодГруппы2)+ " !!!");
		КонецПопытки;
		
		Если  НачШифр <> "" И КонШифр <> "" ТОгда
			
			таб = Объект.ШифрыГруппы.Выгрузить();
			Если таб.Количество()>0 тогда
				ужеЕсть = Таб.НайтиСтроки(Новый Структура("ШифрНачала,ШифрОкончания", НачШифр,КонШифр));
				Если УжеЕсть.Количество() =0 тогда
					НоваяСтрока = Объект.ШифрыГруппы.Добавить();
					НоваяСтрока.НомерГруппы = КодГруппы2;
					НоваяСтрока.ШифрНачала = НачШифр;
					НоваяСтрока.ШифрОкончания = КонШифр;
				КонецЕсли;
			Иначе
				НоваяСтрока = Объект.ШифрыГруппы.Добавить();
				НоваяСтрока.НомерГруппы = КодГруппы2;
				НоваяСтрока.ШифрНачала = НачШифр;
				НоваяСтрока.ШифрОкончания = КонШифр;
			КонецЕсли;
		КонецЕсли;
		
		//Для НССОС
		Если  НачШифр1 <> "" И КонШифр1 <> "" ТОгда
			
			таб = Объект.ШифрыГруппыНС.Выгрузить();
			Если таб.Количество()>0 тогда
				ужеЕсть = Таб.НайтиСтроки(Новый Структура("ШифрНачала1,ШифрОкончания1", НачШифр1,КонШифр1));
				Если УжеЕсть.Количество() = 0 тогда
					НоваяСтрока = Объект.ШифрыГруппыНС.Добавить();
					НоваяСтрока.НомерГруппы     = КодГруппы2;
					НоваяСтрока.ШифрНачала1     = НачШифр1;
					НоваяСтрока.ШифрОкончания1  = КонШифр1;
				КонецЕсли;
			Иначе
				НоваяСтрока = Объект.ШифрыГруппыНС.Добавить();
				НоваяСтрока.НомерГруппы     = КодГруппы2;
				НоваяСтрока.ШифрНачала1     = НачШифр1;
				НоваяСтрока.ШифрОкончания1  = КонШифр1;
			КонецЕсли;
			
		КонецЕсли;
		
		
		Попытка 
			Объект.Записать();
		Исключение
			Сообщить("Ошибка при записи элемента: " + СокрЛП(КодГруппы2)+ " !!!");
		КонецПопытки;
		
	КонецЦикла;
	
КонецПроцедуры 

&НаСервере
Процедура ДобавитьВДерево(Группа,РасшифровкаГруппы,ПодГруппа)
	
	ДеревоГруппОбъект = РеквизитФормыВЗначение("ДеревоГрупп");
	ТекСтрокаГруппа = ДеревоГруппОбъект.Строки.Найти(Группа,"Группа");
	Если ТекСтрокаГруппа=Неопределено Тогда
		ТекСтрока = ДеревоГруппОбъект.Строки.Добавить();
		ТекСтрока.Представление = Группа;
		ТекСтрока.Группа = Группа; 
		ТекСтрока.РасшифровкаГруппы = РасшифровкаГруппы;
		ТекСтрока.Подгруппа = Подгруппа;
		ТекСтрока.Пометка = Ложь;
	Иначе
		ТекСтрокаГруппа2 =ТекСтрокаГруппа.Строки.Найти(РасшифровкаГруппы,"Группа");
		Если ТекСтрокаГруппа2=Неопределено Тогда
			ТекСтрока = ТекСтрокаГруппа.Строки.Добавить();
			ТекСтрока.Представление = РасшифровкаГруппы;
			ТекСтрока.Группа = РасшифровкаГруппы; 
			ТекСтрока.РасшифровкаГруппы = РасшифровкаГруппы;
			ТекСтрока.Подгруппа = Подгруппа;
			ТекСтрока.Пометка = Ложь;
		КонецЕсли;
	КонецЕсли;
	ЗначениеВРеквизитФормы(ДеревоГруппОбъект, "ДеревоГрупп");

КонецПроцедуры

// Определяет значение флага по составу пометок коллекции строк.
//
// Параметры:
//  СтрокаРодитель - КоллекцияСтрокДереваЗначений - строки для определения значения флага.
//
// Возвращаемое значение:
//  Число - значение флага.
// 
&НаКлиенте
Функция ОпределитьЗначениеФлага(СтрокаРодитель)

	НайденыИстина = Ложь;
	НайденыЛожь = Ложь;

	Для Каждого Строка Из СтрокаРодитель.ПолучитьЭлементы() Цикл
		Если Строка.Пометка = 2 Тогда
			Возврат 2;
		КонецЕсли;
		
		Если НЕ НайденыИстина И Строка.Пометка Тогда
			НайденыИстина = Истина;
		КонецЕсли;
		
		Если НЕ НайденыЛожь И НЕ Строка.Пометка Тогда
			НайденыЛожь = Истина;
		КонецЕсли;
	КонецЦикла;

	Если НайденыИстина И НайденыЛожь Тогда
		Возврат 2;
	ИначеЕсли НайденыИстина И НЕ НайденыЛожь Тогда
		Возврат 1;
	ИначеЕсли НЕ НайденыИстина И НайденыЛожь Тогда
		Возврат 0;
	КонецЕсли;

КонецФункции // ОпределитьЗначениеФлага()

// Устанавливает пометку в вышестоящих элементах дерева.
//
&НаКлиенте
Процедура ОбходВерхнихУровней(ТекСтрока)

	ТекСтрока.Пометка = ОпределитьЗначениеФлага(ТекСтрока);
	
	Родитель = ТекСтрока.ПолучитьРодителя();
	Если Родитель <> Неопределено Тогда
		ОбходВерхнихУровней(Родитель);
	КонецЕсли;
	
КонецПроцедуры // ОбходВерхнихУровней()

// Устанавливает пометку в подчиненных элементах дерева.
//
&НаКлиенте
Процедура ОбходНижнихУровней(ТекСтрока)
	
	Для Каждого Строка Из ТекСтрока.ПолучитьЭлементы() Цикл
		Строка.Пометка = ТекСтрока.Пометка;
		ОбходНижнихУровней(Строка);
	КонецЦикла;
	
КонецПроцедуры // ОбходНижнихУровней()

// Устанавливает пометки в подчиненных строках и устанавливает пометку
// в текущей строке в зависимости от состава пометок в подчиненных строках.
// При значении параметра ИнтерактивнаяУстановкаПометок равным Истина
// возможна установка флага для строки с незаполненным источником.
//
// Параметры:
//  ТекСтрока - СтрокаДереваЗначений.
// 
&НаКлиенте
Процедура УстановитьПометкиВДереве(ТекСтрока)

	ОбходНижнихУровней(ТекСтрока);
	Родитель = ТекСтрока.ПолучитьРодителя();
	Если Родитель <> Неопределено Тогда
		ОбходВерхнихУровней(Родитель);
	КонецЕсли;
	
КонецПроцедуры // УстановитьПометкиВДереве()

&НаСервере
Процедура ЗагрузитьСтрокиНаСервере(Адрес)
	
	ДеревоГруппОбъект = РеквизитФормыВЗначение("ДеревоГрупп");
	Строки = ДеревоГруппОбъект.Строки;
	
	ДвоичныеДанные = ПолучитьИзВременногоХранилища(Адрес);
	
	ПутьФайла = КаталогВременныхФайлов() + "КОС.dbf"; 
	ДвоичныеДанные.Записать(ПутьФайла);	
	
	Файл = Новый XBase;
	Файл.ОткрытьФайл(ПутьФайла,,Ложь);
	
	Если НЕ Файл.Открыта() тогда
		Сообщить("Невозможно открыть файл " + СокрЛП(ПутьФайла));
		Возврат;
	КонецЕсли;
	Файл.индексы.Добавить("IND1","LTRIM(TRIM(N1))+LTRIM(TRIM(N0))",Ложь);
	Файл.СоздатьИндексныйФайл(КаталогВременныхФайлов() + "\ind.cdx");
	Файл.ТекущийИндекс=Файл.индексы.Найти("IND1");
	Файл.Переиндексировать();
	
	Для НомСтроки=0 По Строки.Количество()-1 Цикл
		ТекСтрока = Строки.Получить(НомСтроки);
		Если ТекСтрока.Пометка=Неопределено Тогда
			Продолжить;
		КонецЕсли;
		Если ТекСтрока.Пометка>0 Тогда
			Для Каждого Стр Из ТекСтрока.Строки Цикл
				Если Стр.Пометка=Ложь Тогда
					Продолжить;
				КонецЕсли;
				
				НашаГруппа = ТекСтрока.Группа;
				НашаПодГруппа = Стр.ПодГруппа;
				КодПоСправочнику = СокрЛП(НашаГруппа)+"_"+СокрЛП(НашаПодгруппа);
				СсылкаСпр = Справочники.ГруппыПереоценкиОС.НайтиПоНаименованию(КодПоСправочнику);
				Если НЕ СсылкаСпр.Пустая() тогда
					Файл.Первая();
					НаборЗаписей = РегистрыСведений.КоэффициентыПереоценкиОС.СоздатьНаборЗаписей();
					Если Файл.Найти(СокрЛП(НашаГруппа)+СокрЛП(НашаПодгруппа),"=") тогда
						НаборЗаписей.Отбор.Сбросить();
						НаборЗаписей.Отбор.ГруппаПереоценкиОС.Использование = Истина;
						НаборЗаписей.Отбор.Период.Использование = Истина;
						НаборЗаписей.Отбор.ГруппаПереоценкиОС.Значение = СсылкаСпр;
						НаборЗаписей.Отбор.Период.Значение = началоГода(ДатаПереоценки);
						Пока не Файл.ВКонце() и (СокрЛП(Файл.N1) = НашаГруппа и СокрЛП(Файл.N0)= нашаПодГруппа) цикл 
							НоваяЗапись = НаборЗаписей.Добавить();
							НоваяЗапись.Период = началоГода(ДатаПереоценки);
							НоваяЗапись.ГруппаПереоценкиОС = СсылкаСпр;
							НоваяЗапись.ПериодВвода = Число(Файл.Year);
							НоваяЗапись.Коэф13	 	  = Файл.N2;
							НоваяЗапись.Коэф1 	  	  = Файл.N3;
							НоваяЗапись.Коэф2 	      = Файл.N4;
							НоваяЗапись.Коэф3 	      = Файл.N5;
							НоваяЗапись.Коэф4 	      = Файл.N6;
							НоваяЗапись.Коэф5 	      = Файл.N7;
							НоваяЗапись.Коэф6 	      = Файл.N8;
							НоваяЗапись.Коэф7 	      = Файл.N9;
							НоваяЗапись.Коэф8 	      = Файл.N10;
							НоваяЗапись.Коэф9 	      = Файл.N11;
							НоваяЗапись.Коэф10 	      = Файл.N12;
							НоваяЗапись.Коэф11 	      = Файл.N13;
							НоваяЗапись.Коэф12 	      = Файл.N14;
							Файл.Следующая();
						КонецЦикла;
						НаборЗаписей.Записать();
					КонецЕсли;
				КонецЕсли;
			КонецЦикла;
			
		КонецЕсли;
	КонецЦикла;
	
	ЗначениеВРеквизитФормы(ДеревоГруппОбъект, "ДеревоГрупп");
	
КонецПроцедуры

#КонецОбласти


