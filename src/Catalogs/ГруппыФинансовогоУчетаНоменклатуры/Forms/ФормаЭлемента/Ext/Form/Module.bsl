﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Свойство("АвтоТест") Тогда
		Возврат;
	КонецЕсли;

	ОбновлениеИнформационнойБазы.ПроверитьОбъектОбработан(Объект, ЭтотОбъект);
	
	Если Не ЗначениеЗаполнено(Объект.Ссылка) Тогда
		ПриЧтенииСозданииНаСервере();
	КонецЕсли;
	
	ОтображатьНастройкиСчетовУчета = ПолучитьФункциональнуюОпцию("ИспользоватьРеглУчет");
	//++ НЕ УТ
	ОтображатьНастройкиСчетовУчета = ОтображатьНастройкиСчетовУчета И ПравоДоступа("Просмотр",  Метаданные.ПланыСчетов.Хозрасчетный);
	//-- НЕ УТ
	Элементы.ГруппаОтражениеВРеглУчете.Видимость = ОтображатьНастройкиСчетовУчета;
	//++ НЕ УТ
	ДоступныеСчетаУчетаНоменклатуры();
	//-- НЕ УТ
	
	Элементы.ГруппаОтражениеВМеждународномУчете.Видимость = Ложь;
	//++ НЕ УТКА
	ПолучитьСостояниеУточненияСчетовУчета();
	//-- НЕ УТКА
	
	// СтандартныеПодсистемы.ПодключаемыеКоманды
	ПодключаемыеКоманды.ПриСозданииНаСервере(ЭтотОбъект);
	// Конец СтандартныеПодсистемы.ПодключаемыеКоманды
	
	
	СобытияФорм.ПриСозданииНаСервере(ЭтаФорма, Отказ, СтандартнаяОбработка);

КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	//++ НЕ УТКА
	Если ИмяСобытия = "ИзмененыНастройкиУточненияСчетовУчета" Тогда
		ПолучитьСостояниеУточненияСчетовУчета();
	КонецЕсли;
	//-- НЕ УТКА
	Возврат;// в УТ11 обработчик пустой
	
КонецПроцедуры

&НаКлиенте
Процедура ПослеЗаписи(ПараметрыЗаписи)

	МодификацияКонфигурацииКлиентПереопределяемый.ПослеЗаписи(ЭтаФорма, ПараметрыЗаписи);

КонецПроцедуры

&НаСервере
Процедура ПередЗаписьюНаСервере(Отказ, ТекущийОбъект, ПараметрыЗаписи)

	МодификацияКонфигурацииПереопределяемый.ПередЗаписьюНаСервере(ЭтаФорма, Отказ, ТекущийОбъект, ПараметрыЗаписи);

КонецПроцедуры

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)
	
	ПриЧтенииСозданииНаСервере();
	
	//++ НЕ УТ
	ПолучитьСостояниеНастройкиСчетовРеглУчетаПоОрганизациямИСкладам();
	//-- НЕ УТ
	
	МодификацияКонфигурацииПереопределяемый.ПриЧтенииНаСервере(ЭтаФорма, ТекущийОбъект);
	
	// СтандартныеПодсистемы.ПодключаемыеКоманды
	ПодключаемыеКомандыКлиентСервер.ОбновитьКоманды(ЭтотОбъект, Объект);
	// Конец СтандартныеПодсистемы.ПодключаемыеКоманды
КонецПроцедуры

&НаСервере
Процедура ПослеЗаписиНаСервере(ТекущийОбъект, ПараметрыЗаписи)

	МодификацияКонфигурацииПереопределяемый.ПослеЗаписиНаСервере(ЭтаФорма, ТекущийОбъект, ПараметрыЗаписи);

КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	// СтандартныеПодсистемы.ПодключаемыеКоманды
	ПодключаемыеКомандыКлиент.НачатьОбновлениеКоманд(ЭтотОбъект);
	// Конец СтандартныеПодсистемы.ПодключаемыеКоманды
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиЭлементовШапкиФормы

&НаКлиенте
Процедура СчетУчетаНаСкладеПриИзменении(Элемент)
	
	//++ НЕ УТ
	Если Не ЗначениеЗаполнено(Объект.СчетУчетаНаСкладе) Тогда
		Возврат;
	КонецЕсли;
	
	Если Объект.СчетУчетаНаСкладе = ПредопределенноеЗначение("ПланСчетов.Хозрасчетный.ОборудованиеКУстановке")
		ИЛИ Объект.СчетУчетаНаСкладе = ПредопределенноеЗначение("ПланСчетов.Хозрасчетный.КомпонентыОсновныхСредств") Тогда
		Объект.СчетУчетаНДСПриЗакупке = ПредопределенноеЗначение("ПланСчетов.Хозрасчетный.НДСприПриобретенииОсновныхСредств");
	Иначе
		Объект.СчетУчетаНДСПриЗакупке = ПредопределенноеЗначение("ПланСчетов.Хозрасчетный.НДСпоПриобретеннымМПЗ");
	КонецЕсли;
	//-- НЕ УТ
	
	Возврат; // Чтобы в УТ был не пустой обработчик
	
КонецПроцедуры

&НаКлиенте
Процедура СчетУчетаВыручкиОтПродажПриИзменении(Элемент)
	
	//++ НЕ УТ
	СчетУчетаВыручки91 = СчетУчетаВыручки91(Объект.СчетУчетаВыручкиОтПродаж);
	Если Не СчетУчетаВыручки91 И ЗначениеЗаполнено(Объект.СтатьяДоходовОСНО) Тогда
		Объект.СтатьяДоходовОСНО = Неопределено;
	КонецЕсли;
	Элементы.СтатьяДоходовОСНО.Доступность = СчетУчетаВыручки91;
	//-- НЕ УТ
	
	Возврат; // Чтобы в УТ был не пустой обработчик
	
КонецПроцедуры

&НаКлиенте
Процедура СчетУчетаВыручкиОтПродажЕНВДПриИзменении(Элемент)
	
	//++ НЕ УТ
	СчетУчетаВыручки91 = СчетУчетаВыручки91(Объект.СчетУчетаВыручкиОтПродажЕНВД);
	Если Не СчетУчетаВыручки91 И ЗначениеЗаполнено(Объект.СтатьяДоходовЕНВД) Тогда
		Объект.СтатьяДоходовЕНВД = Неопределено;
	КонецЕсли;
	Элементы.СтатьяДоходовЕНВД.Доступность = СчетУчетаВыручки91;
	//-- НЕ УТ
	
	Возврат; // Чтобы в УТ был не пустой обработчик
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура НастроитьСчетаРеглУчетаПоОрганизациямИСкладам(Команда)
	
	//++ НЕ УТ
	Если Не ЗначениеЗаполнено(Объект.Ссылка) Тогда
		ОписаниеОповещения = Новый ОписаниеОповещения("ОбработкаВопросЗаписиОбъекта", ЭтотОбъект);
		ТекстВопроса = НСтр("ru = 'Для продолжения необходимо записать объект. Записать?';
							|en = 'To continue, write the object. Write?'");
		Кнопки = Новый СписокЗначений;
		Кнопки.Добавить(КодВозвратаДиалога.Да, НСтр("ru = 'Записать';
													|en = 'Write'"));
		Кнопки.Добавить(КодВозвратаДиалога.Отмена);
		ПоказатьВопрос(ОписаниеОповещения, ТекстВопроса, Кнопки);
		Возврат;
	КонецЕсли;
	ОткрытьФормуНастройкиСчетовРеглУчетаПоОрганизациямИСкладам();
	//-- НЕ УТ
	
	Возврат; // Чтобы в УТ был не пустой обработчик
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ВыполнитьПереопределяемуюКоманду(Команда)
	
	СобытияФормКлиент.ВыполнитьПереопределяемуюКоманду(ЭтаФорма, Команда);
	
КонецПроцедуры

// СтандартныеПодсистемы.ПодключаемыеКоманды
&НаКлиенте
Процедура Подключаемый_ВыполнитьКоманду(Команда)
	ПодключаемыеКомандыКлиент.ВыполнитьКоманду(ЭтотОбъект, Команда, Объект);
КонецПроцедуры

&НаСервере
Процедура Подключаемый_ВыполнитьКомандуНаСервере(Контекст, Результат) Экспорт
	ПодключаемыеКоманды.ВыполнитьКоманду(ЭтотОбъект, Контекст, Объект, Результат);
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ОбновитьКоманды()
	ПодключаемыеКомандыКлиентСервер.ОбновитьКоманды(ЭтотОбъект, Объект);
КонецПроцедуры
// Конец СтандартныеПодсистемы.ПодключаемыеКоманды

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура ПриЧтенииСозданииНаСервере()
	
	//++ НЕ УТ
	СчетУчетаВыручкиОСНО91 = Ложь;
	СчетУчетаВыручкиЕНВД91 = Ложь;
	Если ПравоДоступа("Просмотр",  Метаданные.ПланыСчетов.Хозрасчетный) Тогда
		СчетУчетаВыручкиОСНО91 = СчетУчетаВыручки91(Объект.СчетУчетаВыручкиОтПродаж);
		СчетУчетаВыручкиЕНВД91 = СчетУчетаВыручки91(Объект.СчетУчетаВыручкиОтПродажЕНВД);
	КонецЕсли;
	Элементы.СтатьяДоходовОСНО.Доступность = СчетУчетаВыручкиОСНО91;
	Элементы.СтатьяДоходовЕНВД.Доступность = СчетУчетаВыручкиЕНВД91;
	//-- НЕ УТ
	
	Возврат; // Чтобы в УТ был не пустой обработчик
	
КонецПроцедуры

//++ НЕ УТ

&НаСервереБезКонтекста
Функция СчетУчетаВыручки91(СчетУчета)
	
	// 4D:ERP для Беларуси, Анастасия, 19.02.2020
	// Статья доходов для отражения себестоимости при прочей реализации, № 24758 
	// {
	Возврат БухгалтерскийУчетПовтИсп.СчетВИерархии(СчетУчета, ПланыСчетов.Хозрасчетный.ПрочиеДоходыИРасходы)
			или БухгалтерскийУчетПовтИсп.СчетВИерархии(СчетУчета, ПланыСчетов.Хозрасчетный.ПрочиеДоходыПоТекущейДеятельности);
	// }
	// 4D
		
КонецФункции

&НаКлиенте
Процедура ОбработкаВопросЗаписиОбъекта(Результат, ДополнительныеПараметры) Экспорт
	
	Если Результат = КодВозвратаДиалога.Да Тогда
		Если Не Записать() Тогда
			Возврат;
		КонецЕсли;
		ОткрытьФормуНастройкиСчетовРеглУчетаПоОрганизациямИСкладам();
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьФормуНастройкиСчетовРеглУчетаПоОрганизациямИСкладам()
	
	ПараметрыФормы = Новый Структура;
	ПараметрыФормы.Вставить("ГруппаФинансовогоУчета", Объект.Ссылка);
	
	ОписаниеОповещения = Новый ОписаниеОповещения("ПослеНастройкиСчетовРеглУчетаПоОрганизациямИСкладам", ЭтотОбъект);
	
	ОткрытьФорму("РегистрСведений.ПорядокОтраженияНоменклатуры.Форма.ФормаНастройки", 
		ПараметрыФормы, ЭтаФорма, , , , ОписаниеОповещения);
	
КонецПроцедуры

&НаКлиенте
Процедура ПослеНастройкиСчетовРеглУчетаПоОрганизациямИСкладам(Результат, ДополнительныеПараметры) Экспорт
	
	ПолучитьСостояниеНастройкиСчетовРеглУчетаПоОрганизациямИСкладам();
	
КонецПроцедуры

&НаСервере
Процедура ПолучитьСостояниеНастройкиСчетовРеглУчетаПоОрганизациямИСкладам()
	
	ЗаголовокКоманды  = НСтр("ru = 'Посмотреть настройки счетов учета по организациям и складам';
							|en = 'View GL account settings of companies and warehouses'");
	
	Если ПравоДоступа("Редактирование", Метаданные.РегистрыСведений.ПорядокОтраженияНоменклатуры)
		 И ПравоДоступа("Редактирование", Метаданные.РегистрыСведений.ПорядокОтраженияНоменклатурыПереданной) Тогда
	
		Запрос = Новый Запрос;
		Запрос.Текст =
		"ВЫБРАТЬ ПЕРВЫЕ 1
		|	1 КАК Количество
		|ИЗ
		|	РегистрСведений.ПорядокОтраженияНоменклатуры КАК ПорядокОтражения
		|ГДЕ
		|	ПорядокОтражения.ГруппаФинансовогоУчета = &ГруппаФинансовогоУчета
		|
		|ОБЪЕДИНИТЬ ВСЕ
		|
		|ВЫБРАТЬ
		|	1
		|ИЗ
		|	РегистрСведений.ПорядокОтраженияНоменклатурыПереданной КАК ПорядокОтражения
		|ГДЕ
		|	ПорядокОтражения.ГруппаФинансовогоУчета = &ГруппаФинансовогоУчета";
		
		Запрос.УстановитьПараметр("ГруппаФинансовогоУчета", Объект.Ссылка);
		
		РезультатЗапроса = Запрос.Выполнить();
		Если РезультатЗапроса.Пустой() Тогда
			ЗаголовокКоманды  = НСтр("ru = 'Настроить счета учета по организациям и складам';
									|en = 'Configure GL accounts by companies and warehouses'");
		Иначе
			ЗаголовокКоманды  = НСтр("ru = 'Изменить настройки счетов учета по организациям и складам';
									|en = 'Change GL account settings by companies and warehouses'");
		КонецЕсли;
		
	КонецЕсли;
	
	Элементы.НастроитьСчетаРеглУчетаПоОрганизациямИСкладам.Заголовок = ЗаголовокКоманды; 
	
КонецПроцедуры

&НаСервере
Процедура ДоступныеСчетаУчетаНоменклатуры()
	
	Если Не ПравоДоступа("Просмотр",  Метаданные.ПланыСчетов.Хозрасчетный) Тогда
		Возврат;
	КонецЕсли;
	
	СтруктураСчетовУчета = Обработки.НастройкаОтраженияДокументовВРеглУчете.ДоступныеСчетаУчетаНоменклатуры();
	
	// Счета учета товаров на складе.
	МассивПараметров = Новый Массив;
	МассивПараметров.Добавить(Новый ПараметрВыбора("Отбор.Ссылка", Новый ФиксированныйМассив(СтруктураСчетовУчета.СчетаУчетаНаСкладе)));
	Элементы.СчетУчетаНаСкладе.ПараметрыВыбора = Новый ФиксированныйМассив(МассивПараметров);
	
	// Счета учета товаров в пути и передачи на комиссию.
	МассивПараметров.Очистить();
	МассивПараметров.Добавить(Новый ПараметрВыбора("Отбор.Ссылка", Новый ФиксированныйМассив(СтруктураСчетовУчета.СчетаУчетаПередачиНаКомиссию)));
	Элементы.СчетУчетаПередачиНаКомиссию.ПараметрыВыбора = Новый ФиксированныйМассив(МассивПараметров);
	Элементы.СчетУчетаВПути.ПараметрыВыбора = Новый ФиксированныйМассив(МассивПараметров);
	
	// Счета учета товаров передачи в переработку.
	МассивПараметров.Очистить();
	МассивПараметров.Добавить(Новый ПараметрВыбора("Отбор.Ссылка", Новый ФиксированныйМассив(СтруктураСчетовУчета.СчетаУчетаПередачиВПереработку)));
	Элементы.СчетУчетаПередачиВПереработку.ПараметрыВыбора = Новый ФиксированныйМассив(МассивПараметров);
	
	// Счета учета выручки от продаж.
	МассивПараметров.Очистить();
	МассивПараметров.Добавить(Новый ПараметрВыбора("Отбор.Ссылка", Новый ФиксированныйМассив(СтруктураСчетовУчета.СчетаУчетаВыручкиОтПродаж)));
	Элементы.СчетУчетаВыручкиОтПродаж.ПараметрыВыбора = Новый ФиксированныйМассив(МассивПараметров);
	Элементы.СчетУчетаВыручкиОтПродажЕНВД.ПараметрыВыбора = Новый ФиксированныйМассив(МассивПараметров);
	
	// Счета учета себестоимости от продаж.
	МассивПараметров.Очистить();
	МассивПараметров.Добавить(Новый ПараметрВыбора("Отбор.Ссылка", Новый ФиксированныйМассив(СтруктураСчетовУчета.СчетаУчетаСебестоимостиПродаж)));
	Элементы.СчетУчетаСебестоимостиПродаж.ПараметрыВыбора = Новый ФиксированныйМассив(МассивПараметров);
	Элементы.СчетУчетаСебестоимостиПродажЕНВД.ПараметрыВыбора = Новый ФиксированныйМассив(МассивПараметров);
	
	// Счета учета НДС при продаже.
	МассивПараметров.Очистить();
	МассивПараметров.Добавить(Новый ПараметрВыбора("Отбор.Ссылка", Новый ФиксированныйМассив(СтруктураСчетовУчета.СчетаУчетаНДСПриПродаже)));
	Элементы.СчетУчетаНДСПриПродаже.ПараметрыВыбора = Новый ФиксированныйМассив(МассивПараметров);
	
	// Счета учета НДС при отгрузке без перехода права собственности
	МассивПараметров = Новый Массив;
	МассивПараметров.Добавить(Новый ПараметрВыбора("Отбор.Ссылка", Новый ФиксированныйМассив(СтруктураСчетовУчета.СчетаУчетаНДСВПути)));
	Элементы.СчетУчетаНДСВПути.ПараметрыВыбора = Новый ФиксированныйМассив(МассивПараметров);
	
	// Счета учета затрат на приобретение товаров
	МассивПараметров = Новый Массив;
	МассивПараметров.Добавить(Новый ПараметрВыбора("Отбор.Ссылка", Новый ФиксированныйМассив(СтруктураСчетовУчета.СчетаУчетаЗатратыНаПриобретение)));
	Элементы.СчетУчетаЗатратыНаПриобретение.ПараметрыВыбора = Новый ФиксированныйМассив(МассивПараметров);
	
КонецПроцедуры
//-- НЕ УТ

//++ НЕ УТКА
&НаСервере
Процедура ПолучитьСостояниеУточненияСчетовУчета()
	
	МеждународныйУчетОбщегоНазначения.УстановитьВидимостьНастроекМФУ(
		Объект.Ссылка,
		Элементы.ГруппаОтражениеВМеждународномУчете,
		Элементы.Найти("НастроитьУточнениеСчетов"));// у ограниченных пользователей элемент вырезается с формы
	
КонецПроцедуры

//-- НЕ УТКА

#КонецОбласти
