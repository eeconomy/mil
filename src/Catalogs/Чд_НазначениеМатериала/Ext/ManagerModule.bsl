﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда
#Область СлужебныеПроцедурыИФункции
Процедура ОбработкаПолученияПолейПредставления(Поля, СтандартнаяОбработка)  	
	СтандартнаяОбработка = Ложь;
	Поля.Добавить("Наименование");
	Поля.Добавить("Код");   	
КонецПроцедуры 

Процедура ОбработкаПолученияПредставления(Данные, Представление, СтандартнаяОбработка) 	
	СтандартнаяОбработка = Ложь;
	Представление = Данные.Код + " " + Данные.Наименование;  	
КонецПроцедуры
#КонецОбласти

#КонецЕсли