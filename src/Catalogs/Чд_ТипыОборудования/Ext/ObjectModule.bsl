﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область СлужебныеПроцедурыИФункции

Процедура ОбработкаПроверкиЗаполнения(Отказ, ПроверяемыеРеквизиты)
	
	Если НЕ ЗначениеЗаполнено(Код) Тогда
		УстановитьНовыйКод();
	КонецЕсли;

КонецПроцедуры

#КонецОбласти

#КонецЕсли