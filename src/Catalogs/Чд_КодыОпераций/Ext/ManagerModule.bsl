﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ПрограммныйИнтерфейс

#Область СтандартныеПодсистемы

#Область ДляВызоваИзДругихПодсистем

Процедура ДобавитьКомандыОтчетов(КомандыОтчетов, Параметры) Экспорт
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#КонецОбласти
	
#КонецЕсли
