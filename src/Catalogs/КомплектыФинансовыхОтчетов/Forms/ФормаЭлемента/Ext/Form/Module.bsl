﻿
#Область ОписаниеПеременных

&НаКлиенте
Перем БуферВидаПоказателей;

#КонецОбласти

#Область ОбработчикиСобытийФормы

&НаКлиенте
Процедура  ПослеЗаписи(ПараметрыЗаписи)

	МодификацияКонфигурацииКлиентПереопределяемый.ПослеЗаписи(ЭтаФорма, ПараметрыЗаписи);

КонецПроцедуры

&НаСервере
Процедура ПередЗаписьюНаСервере(Отказ, ТекущийОбъект, ПараметрыЗаписи)

	МодификацияКонфигурацииПереопределяемый.ПередЗаписьюНаСервере(ЭтаФорма, Отказ, ТекущийОбъект, ПараметрыЗаписи);

КонецПроцедуры

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)

	МодификацияКонфигурацииПереопределяемый.ПриЧтенииНаСервере(ЭтаФорма, ТекущийОбъект);

КонецПроцедуры

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)

	Если Параметры.Свойство("АвтоТест") Тогда // Возврат при получении формы для анализа.
		Возврат;
	КонецЕсли;

	ОбновлениеИнформационнойБазы.ПроверитьОбъектОбработан(Объект, ЭтотОбъект);

	СобытияФорм.ПриСозданииНаСервере(ЭтаФорма, Отказ, СтандартнаяОбработка);

КонецПроцедуры

&НаСервере
Процедура ПослеЗаписиНаСервере(ТекущийОбъект, ПараметрыЗаписи)

	МодификацияКонфигурацииПереопределяемый.ПослеЗаписиНаСервере(ЭтаФорма, ТекущийОбъект, ПараметрыЗаписи);

КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура ВидыОтчетовВидФинансовогоОтчетаПриИзменении(Элемент)
	Элементы.ВидыОтчетов.ТекущиеДанные.НастройкиТекст = НСтр("ru = 'Настроить отборы';
															|en = 'Set filters'");
КонецПроцедуры

&НаКлиенте
Процедура ОписаниеНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	ОбщегоНазначенияКлиент.ПоказатьФормуРедактированияКомментария(
		Элемент.ТекстРедактирования, 
		ЭтотОбъект, 
		"Объект.Описание");
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура Подключаемый_ВыполнитьПереопределяемуюКоманду(Команда)
	
	СобытияФормКлиент.ВыполнитьПереопределяемуюКоманду(ЭтаФорма, Команда);
	
КонецПроцедуры

&НаКлиенте
Процедура ВидПоказателейНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	БуферВидаПоказателей = Объект.ВидПоказателей;
	
КонецПроцедуры

&НаКлиенте
Процедура ВидПоказателейПриИзменении(Элемент)
	
	Если Объект.ВидыОтчетов.Количество() > 0 Тогда
		ТекстВопроса = НСтр("ru = 'Список видов отчета будет очищен. Продолжить?';
							|en = 'Report kind list will be cleared. Continue?'");
		ОбработчикОтвета = Новый ОписаниеОповещения("ОбработатьВопросОчисткиВидовОтчета", ЭтотОбъект);
		ПоказатьВопрос(ОбработчикОтвета, ТекстВопроса, РежимДиалогаВопрос.ДаНет);
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаКлиенте
Процедура ОбработатьВопросОчисткиВидовОтчета(РезультатВопроса, ДополнительныеПараметры) Экспорт

	Если РезультатВопроса = КодВозвратаДиалога.Да Тогда
		Объект.ВидыОтчетов.Очистить();
	Иначе
		Объект.ВидПоказателей = БуферВидаПоказателей;
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти