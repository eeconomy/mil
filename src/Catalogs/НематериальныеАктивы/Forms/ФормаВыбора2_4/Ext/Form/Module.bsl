﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Свойство("АвтоТест") Тогда
		Возврат;
	КонецЕсли;
	
	НастроитьФормуПоПараметрамФормы();
	
	Если НЕ ПолучитьФункциональнуюОпцию("ИспользоватьРеглУчет") Тогда
		Элементы.СостояниеУУ.Заголовок = НСтр("ru = 'Состояние';
												|en = 'State'");
		Элементы.СостояниеБУ.Видимость = Ложь;
		Элементы.ДатаПринятияКУчетуУпр.Заголовок = НСтр("ru = 'Принят к учету';
														|en = 'Recognized'");
		Элементы.ДатаПринятияКУчетуРегл.Видимость = Ложь;
	КонецЕсли; 
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ОтборСостояниеПриИзменении(Элемент)
	
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(
		Список, "Состояние", ОтборСостояние);
	
КонецПроцедуры

&НаКлиенте
Процедура ОтборОрганизацияПриИзменении(Элемент)
	
	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(
		Список, "Организация", ОтборОрганизация,,, ЗначениеЗаполнено(ОтборОрганизация));
	
КонецПроцедуры

&НаКлиенте
Процедура ОтборПодразделениеПриИзменении(Элемент)
	
	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(
		Список, "Подразделение", ОтборПодразделение,,, ЗначениеЗаполнено(ОтборПодразделение));
	
КонецПроцедуры

&НаКлиенте
Процедура ОтборВидОбъектаУчетаПриИзменении(Элемент)
	
	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(
		Список, "ВидОбъектаУчета", ОтборВидОбъектаУчета,,, ЗначениеЗаполнено(ОтборВидОбъектаУчета));
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура НастроитьФормуПоПараметрамФормы()
	
	ОтборСписка = ОбщегоНазначенияКлиентСервер.СвойствоСтруктуры(Параметры, "Отбор", Новый Структура);

	ЗаполнитьБыстрыйОтбор("ВидОбъектаУчета", ОтборСписка, "ВидОбъектаУчета");
	
	Если ПравоДоступа("Чтение", Метаданные.РегистрыСведений.МестоУчетаНМА) Тогда
		ЗаполнитьБыстрыйОтбор("Организация",   ОтборСписка, "Организация");
		ЗаполнитьБыстрыйОтбор("Подразделение", ОтборСписка, "Подразделение");
	Иначе
		Элементы.ОтборОрганизация.Видимость = Ложь;
		Элементы.ОтборПодразделение.Видимость = Ложь;
		Элементы.Подразделение.Видимость = Ложь;
	КонецЕсли;
	
	Если ВнеоборотныеАктивыСлужебный.ЕстьПраваНаЧтениеСостоянияНМА() Тогда
		
		ЗаполнитьБыстрыйОтбор("Состояние", ОтборСписка, "СостояниеБУ,СостояниеУУ");
		
		ОтборНеПринятыхКУчету = ОтборСостояниеСписок.НайтиПоЗначению(Перечисления.ВидыСостоянийНМА.НеПринятКУчету) <> Неопределено
								ИЛИ ОтборСостояниеСписок.НайтиПоЗначению(Перечисления.ВидыСостоянийНМА.Списан) <> Неопределено
								ИЛИ ОтборСостояние = Перечисления.ВидыСостоянийНМА.НеПринятКУчету
								ИЛИ ОтборСостояние = Перечисления.ВидыСостоянийНМА.Списан;
		
		Если ОтборНеПринятыхКУчету Тогда
			Элементы.ОтборОрганизация.ТолькоПросмотр = Истина;
			Элементы.ОтборПодразделение.ТолькоПросмотр = Истина;
			Элементы.Организация.Видимость = Ложь;
			Элементы.Подразделение.Видимость = Ложь;
		КонецЕсли;
		
		Если Элементы.ОтборСостояние.Видимость Тогда
			Элементы.ОтборСостояниеСписок.Видимость = Ложь;
		КонецЕсли;
	
	Иначе
		Элементы.ОтборСостояние.Видимость = Ложь;
		Элементы.ОтборСостояниеСписок.Видимость = Ложь;
		Элементы.СостояниеБУ.Видимость = Ложь;
		Элементы.СостояниеУУ.Видимость = Ложь;
	КонецЕсли;
	
	ОписаниеЗапросаДляВыбора = Справочники.НематериальныеАктивы.ОписаниеЗапросаДляВыбора(Параметры);
	
	СвойстваСписка = ОбщегоНазначения.СтруктураСвойствДинамическогоСписка();
	СвойстваСписка.ТекстЗапроса = ОписаниеЗапросаДляВыбора.ТекстЗапроса;
	ОбщегоНазначения.УстановитьСвойстваДинамическогоСписка(Элементы.Список, СвойстваСписка);
	
	Для каждого ОписаниеПараметра Из ОписаниеЗапросаДляВыбора.ПараметрыЗапроса Цикл
		Список.Параметры.УстановитьЗначениеПараметра(ОписаниеПараметра.Ключ, ОписаниеПараметра.Значение);
	КонецЦикла; 
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьБыстрыйОтбор(ИмяОтбора, ОтборСписка, ЭлементыСписка = Неопределено)

	// Заполняет быстрые отборы и скрывает колонки в списке
	
	ЗначениеОтбора = ОбщегоНазначенияКлиентСервер.СвойствоСтруктуры(ОтборСписка, ИмяОтбора, Неопределено);
	
	Суффикс = "";
	Если ТипЗнч(ЗначениеОтбора) = Тип("ФиксированныйМассив") 
		ИЛИ ТипЗнч(ЗначениеОтбора) = Тип("Массив") Тогда
		
		Если ЗначениеОтбора.Количество() > 1 Тогда
			Суффикс = "Список";
			Элементы["Отбор" + ИмяОтбора].Видимость = Ложь;
			ЗначениеОтбора = ВнеоборотныеАктивыСлужебный.ФиксированныйМассивВСписок(ЗначениеОтбора);
		Иначе
			ЗначениеОтбора = ЗначениеОтбора.Получить(0);
			Элементы["Отбор" + ИмяОтбора + "Список"].Видимость = Ложь;
		КонецЕсли;
	КонецЕсли;
	
	ЭтаФорма["Отбор" + ИмяОтбора + Суффикс] = ЗначениеОтбора;
	Если ЗначениеЗаполнено(ЗначениеОтбора) Тогда
		Элементы["Отбор" + ИмяОтбора + Суффикс].ТолькоПросмотр = Истина;
		Если ЭлементыСписка <> Неопределено Тогда
			Для каждого ИмяЭлементаСписка Из СтрРазделить(ЭлементыСписка, ",") Цикл
				Элементы[ИмяЭлементаСписка].Видимость = Ложь;
			КонецЦикла; 
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти
