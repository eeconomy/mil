﻿
#Область ПрограммныйИнтерфейс

// Определяет список объектов конфигурации, в модулях менеджеров которых предусмотрена процедура 
// ДобавитьКомандыСозданияНаОсновании, формирующая команды создания на основании объектов.
// Синтаксис процедуры ДобавитьКомандыСозданияНаОсновании см. в документации.
//
// см. ОбщийМодуль.СозданиеНаОснованииПереопределяемый.ПриОпределенииОбъектовСКомандамиСозданияНаОсновании()
//   
Процедура ПриОпределенииОбъектовСКомандамиСозданияНаОсновании(Объекты) Экспорт

	// 4D:ERP для Беларуси 
	// Учет бланков строгой отчетности
	// {
	Объекты.Добавить(Метаданные.Документы.ИнвентаризационнаяОписьБСО);
	// }
	// 4D
	// 4D:ERP для Беларуси МаксимК, 14.05.2021
	// Учет бланков строгой отчетности
	// {
	Объекты.Добавить(Метаданные.Документы.ИнвентаризационнаяОписьТМЦ);
	// }
	// 4D
	
	// 4D:ERP для Беларуси, ВладимирР, 26.05.2022 11:40:45
	// Расчет НДС к вычету, №33515
	// {
	Объекты.Добавить(Метаданные.Документы.РасчетСуммНДСПодлежащихВычету);
	// }
	// 4D
	
КонецПроцедуры

// Вызывается для формирования списка команд создания на основании КомандыСозданияНаОсновании, однократно для при первой
// необходимости, а затем результат кэшируется с помощью модуля с повторным использованием возвращаемых значений.
// Здесь можно определить команды создания на основании, общие для большинства объектов конфигурации.
//
// см. ОбщийМодуль.СозданиеНаОснованииПереопределяемый.ПриОпределенииОбъектовСКомандамиСозданияНаОсновании()
//
Процедура ПередДобавлениемКомандСозданияНаОсновании(КомандыСозданияНаОсновании, Параметры, СтандартнаяОбработка) Экспорт
	
	
КонецПроцедуры

#КонецОбласти
