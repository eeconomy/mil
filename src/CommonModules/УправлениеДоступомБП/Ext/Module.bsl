﻿#Область ПрограммныйИнтерфейс

// Позволяет указать списки, у которых объекты метаданных содержат описание логики ограничения
// доступа в модулях менеджеров или переопределяемом модуле.
//
// В модулях менеджеров указанных списков должна быть размещена процедура обработчика,
// в которую передаются следующие параметры.
// 
//  Ограничение - Структура - со свойствами:
//    * Текст                             - Строка - ограничение доступа для пользователей.
//                                            Если пустая строка, значит доступ разрешен.
//    * ТекстДляВнешнихПользователей      - Строка - ограничение доступа для внешних пользователей.
//                                            Если пустая строка, значит доступ запрещен.
//    * ПоВладельцуБезЗаписиКлючейДоступа - Неопределено - определить автоматически.
//                                        - Булево - если Ложь, то всегда записывать ключи доступа,
//                                            если Истина, тогда не записывать ключи доступа,
//                                            а использовать ключи доступа владельца (требуется,
//                                            чтобы ограничение было строго по объекту-владельцу).
///   * ПоВладельцуБезЗаписиКлючейДоступаДляВнешнихПользователей - Неопределено, Булево - см.
//                                            описание предыдущего параметра.
//
// Далее пример процедуры для модуля менеджера.
//
//// См. УправлениеДоступомПереопределяемый.ПриЗаполненииСписковСОграничениемДоступа.
//Процедура ПриЗаполненииОграниченияДоступа(Ограничение) Экспорт
//	
//	Ограничение.Текст =
//	"РазрешитьЧтениеИзменение
//	|ГДЕ
//	|	ЗначениеРазрешено(Организация)
//	|	И ЗначениеРазрешено(Контрагент)";
//	
//КонецПроцедуры
//
// Параметры:
//  Списки - Соответствие - списки с ограничением доступа:
//             * Ключ     - ОбъектМетаданных - список с ограничением доступа.
//             * Значение - Булево - Истина - текст ограничения в модуле менеджера.
//                                 - Ложь   - текст ограничения в этом переопределяемом
//                модуле в процедуре ПриЗаполненииОграниченияДоступа.
//
Процедура ПриЗаполненииСписковСОграничениемДоступа(Списки) Экспорт

	//++ НЕ УТ
	Списки.Вставить(Метаданные.Справочники.ТорговыеТочки, Истина);
	Списки.Вставить(Метаданные.Документы.ВосстановлениеНДСПоОбъектамНедвижимости, Истина);
	Списки.Вставить(Метаданные.Документы.ДопЛистКнигиПокупокДляПередачиВЭлектронномВиде, Истина);
	Списки.Вставить(Метаданные.Документы.ДопЛистКнигиПродажДляПередачиВЭлектронномВиде, Истина);
	Списки.Вставить(Метаданные.Документы.ЖурналУчетаСчетовФактурДляПередачиВЭлектронномВиде, Истина);
	Списки.Вставить(Метаданные.Документы.КнигаПокупокДляПередачиВЭлектронномВиде, Истина);
	Списки.Вставить(Метаданные.Документы.КнигаПродажДляПередачиВЭлектронномВиде, Истина);
	Списки.Вставить(Метаданные.Документы.РегистрУчета, Истина);
	Списки.Вставить(Метаданные.Документы.РеестрСчетовФактурПоставщика, Истина);
	Списки.Вставить(Метаданные.ЖурналыДокументов.ДокументыПоУчетуНДСДляПередачиВЭлектронномВиде, Истина);
	//-- НЕ УТ

	Списки.Вставить(Метаданные.РегистрыСведений.ЖурналУчетаСчетовФактур, Истина);

	//++ НЕ УТ
	Списки.Вставить(Метаданные.РегистрыСведений.НастройкаЗаполненияСвободныхСтрокФормСтатистики, Истина);
	Списки.Вставить(Метаданные.РегистрыСведений.НастройкаЗаполненияФормСтатистики, Истина);
	//-- НЕ УТ

	Списки.Вставить(Метаданные.РегистрыСведений.ОшибочныеРеквизитыКонтрагентов, Истина);

	//++ НЕ УТ
	Списки.Вставить(Метаданные.РегистрыСведений.ПараметрыТорговыхТочек, Истина);
	Списки.Вставить(Метаданные.РегистрыСведений.РасчетЗемельногоНалога, Истина);
	Списки.Вставить(Метаданные.РегистрыСведений.РасчетНалогаНаИмущество, Истина);
	Списки.Вставить(Метаданные.РегистрыСведений.РасчетТранспортногоНалога, Истина);
	Списки.Вставить(Метаданные.РегистрыСведений.СведенияТаможенныхДекларацийЭкспорт, Истина);
	Списки.Вставить(Метаданные.РегистрыСведений.СтатусыЗапросовРеестровСчетовФактур, Истина);
	//-- НЕ УТ

	Списки.Вставить(Метаданные.РегистрыНакопления.НДСЗаписиКнигиПокупок, Истина);
	Списки.Вставить(Метаданные.РегистрыНакопления.НДСЗаписиКнигиПродаж, Истина);

	//++ НЕ УТ
	Списки.Вставить(Метаданные.РегистрыНакопления.НДСЗаписиРаздела7Декларации, Истина);
	Списки.Вставить(Метаданные.РегистрыБухгалтерии.Хозрасчетный, Истина);
	//-- НЕ УТ
	
КонецПроцедуры

#КонецОбласти
