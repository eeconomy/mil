﻿
////////////////////////////////////////////////////////////////////////////////
// Модуль содержит переопределяемые процедуры и функции.
//
////////////////////////////////////////////////////////////////////////////////

#Область ПрограммныйИнтерфейс

////////////////////////////////////////////////////////////////////////////////
// Процедуры и функции механизма расшифровки.

// Процедура выводит форму расшифровки.
//
// Параметры:
//	 ИДОтчета - Строка - идентификатор отчета (совпадает с именем объекта метаданных).
// 	 ИДРедакцииОтчета - Строка - идентификатор редакции формы отчета (совпадает с именем формы объекта метаданных).
//   ИДИменПоказателей - Массив - массив идентификаторов имен показателей, по которым формируется расшифровка.
//   ПараметрыОтчета - Структура - структура параметров отчета, необходимых для формирования расшифровки.
// 
// Пример:
// 	 Если ИДОтчета = "РегламентированныйОтчетБухОтчетность" Тогда
//	 	 Если ИДРедакцииОтчета = "ФормаОтчета2011Кв1" Тогда
//		 	 ОткрытьРасшифровкуБухОтчетностьФормаОтчета2011Кв1(ИДИменПоказателей, ПараметрыОтчета);
//		 КонецЕсли;
// 	 КонецЕсли;
//
Процедура ОткрытьРасшифровкуОтчета(ИДОтчета, ИДРедакцииОтчета, ИДИменПоказателей, ПараметрыОтчета) Экспорт
	
	Если ИДОтчета = "РегламентированныйОтчетБаланс_Локализация" 
		 ИЛИ ИДОтчета = "РегламентированныйОтчетПриложение2_Локализация" 
		 ИЛИ ИДОтчета = "РегламентированныйОтчетПриложение3_Локализация"
		 ИЛИ ИДОтчета = "РегламентированныйОтчетПриложение4_Локализация" Тогда
		
		ОткрытьРасшифровкуБухОтчетностьБаланс_Локализация(ИДИменПоказателей, ПараметрыОтчета);
		
	Иначе
		
		ЗарплатаКадрыКлиент.ОткрытьРасшифровкуРегламентированногоОтчета(ИДОтчета, ИДРедакцииОтчета, ИДИменПоказателей, ПараметрыОтчета)	
		
	КонецЕсли;
	
КонецПроцедуры

Процедура ОткрытьРасшифровкуБухОтчетностьБаланс_Локализация(ИДИменПоказателей, ПараметрыОтчета)

	ПараметрыФормы = КопияСтруктуры(ПараметрыОтчета);
	ПараметрыФормы.Вставить("ИДИменПоказателей", ИДИменПоказателей);
	
	ЗаголовокРасшифровкиДляВсегоРаздела = НаименованиеРасшифровкиБухОтчетности("Бухгалтерский баланс",
		ПараметрыОтчета.мДатаНачалаПериодаОтчета, ПараметрыОтчета.мДатаКонцаПериодаОтчета);
	
	Если НЕ ЗаполнениеБухгалтерскойОтчетностиВызовСервера.ЕстьРасшифровкаПоПоказателям(ПараметрыОтчета.АдресВременногоХранилищаРасшифровки, ИДИменПоказателей) Тогда
		ТекстВопроса = НСтр("ru = 'Для выбранной ячейки расшифровка не существует." + Символы.ПС
			+ "Показать расшифровку для всех показателей формы?'");
		ДополнительныеПараметры = Новый Структура("ЗаголовокРасшифровкиДляВсегоРаздела, ПараметрыФормы", ЗаголовокРасшифровкиДляВсегоРаздела, ПараметрыФормы);
		ОписаниеОповещения = Новый ОписаниеОповещения("ОткрытьРасшифровкуЗавершение", ЭтотОбъект, ДополнительныеПараметры);
		ПоказатьВопрос(ОписаниеОповещения, ТекстВопроса, РежимДиалогаВопрос.ДаНет);
	Иначе
		ПоказатьРасшифровкуВсехПоказателейФормы(ЗаголовокРасшифровкиДляВсегоРаздела, ПараметрыФормы);
	КонецЕсли;

КонецПроцедуры

// Открывает форму помощника по учету НДС.
//
Процедура ОткрытьФормуПомощникаПоУчетуНДС(ПараметрыОткрытияПомощника) Экспорт
	
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// Процедуры и функции календаря бухгалтера.

// Функция возвращает имя формы, используемой для уплаты налога.
// 
// Возвращаемое значение:
//   Строка - имя формы, используемой для уплаты налога.
//
// Пример:
//  Возврат "Документ.ПлатежноеПоручение.ФормаОбъекта";
//
Функция ИмяФормыДляУплатыНалога() Экспорт
	Возврат "Документ.СписаниеБезналичныхДенежныхСредств.ФормаОбъекта";
КонецФункции

////////////////////////////////////////////////////////////////////////////////
// Процедуры и функции списка задач бухгалтера.

// Процедура открывает форму списка задач бухгалтера.
//
// Параметры:
//   Владелец - Форма - владелец формы.
//   Организация - СправочникСсылка.Организации - ссылка на элемент справочника организаций.
//   СтандартнаяОбработка - Булево - признак стандартной обработки.
//
Процедура ОткрытьКалендарь(Владелец, Организация, СтандартнаяОбработка) Экспорт
			
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// Процедуры и функции интерфейса взаимодействия с конфигурациями (библиотеками)
// - потребителями.

// Процедура переопределяет создание формы РСВ-1 из списка регламентированных отчетов.
//
// Параметры:
//   ОписаниеРСВ_1 - Структура - структура с полями:
//     * Организация             - СправочникСсылка.Организации - организация;
//     * ДатаНачалаПериодаОтчета - Дата - дата начала периода отчета;
//     * ДатаКонцаПериодаОтчета  - Дата - дата конца периода отчета;
//     * КорректирующаяФорма     - Булево - признак корректирующей формы.
//   СтандартнаяОбработка - Булево - признак стандартной обработки.
//
Процедура ПриСозданииРСВ_1ИзСпискаРеглОтчетов(ОписаниеРСВ_1, СтандартнаяОбработка) Экспорт
	
	ПерсонифицированныйУчетКлиент.ПриСозданииРСВ_1ИзСпискаРеглОтчетов(ОписаниеРСВ_1, СтандартнаяОбработка);
	
КонецПроцедуры

// Процедура переопределяет открытие формы РСВ-1 из журнала регламентированных отчетов.
//
// Параметры:
//   Ссылка - ДокументСсылка.РегламентированныйОтчет - ссылка на регламентированный отчет.
//   ОписаниеРСВ_1 - Структура - структура с полями:
//     * Организация             - СправочникСсылка.Организации - организация;
//     * ДатаНачалаПериодаОтчета - Дата - дата начала периода отчета;
//     * ДатаКонцаПериодаОтчета  - Дата - дата конца периода отчета;
//     * КорректирующаяФорма     - Булево - признак корректирующей формы.
//   СтандартнаяОбработка - Булево - признак стандартной обработки.
//
Процедура ПриОткрытииРСВ_1ИзЖурналаРеглОтчетов(Ссылка, ОписаниеРСВ_1, СтандартнаяОбработка) Экспорт
	
	ПерсонифицированныйУчетКлиент.ПриОткрытииРСВ_1ИзСпискаРеглОтчетов(Ссылка, ОписаниеРСВ_1, СтандартнаяОбработка);
	
КонецПроцедуры

// Процедура переопределяет поведение при печати формы РСВ-1 из журнала регламентированных отчетов.
//
// Параметры:
//   Ссылка - ДокументСсылка.РегламентированныйОтчет - ссылка на регламентированный отчет.
//   ОписаниеРСВ_1 - Структура - структура с полями:
//     * Организация             - СправочникСсылка.Организации - организация;
//     * ДатаНачалаПериодаОтчета - Дата - дата начала периода отчета;
//     * ДатаКонцаПериодаОтчета  - Дата - дата конца периода отчета;
//     * КорректирующаяФорма     - Булево - признак корректирующей формы.
//   СтандартнаяОбработка - Булево - признак стандартной обработки.
//
Процедура ПриПечатиРСВ_1ИзЖурналаРеглОтчетов(Ссылка, ОписаниеРСВ_1, СтандартнаяОбработка) Экспорт
	
	ПерсонифицированныйУчетКлиент.ПриПечатиРСВ_1ИзЖурналаРеглОтчетов(Ссылка, ОписаниеРСВ_1, СтандартнаяОбработка);
	
КонецПроцедуры

// Процедура переопределяет поведение при выгрузке формы РСВ-1 из журнала регламентированных отчетов.
//
// Параметры:
//   Ссылка - ДокументСсылка.РегламентированныйОтчет - ссылка на регламентированный отчет.
//   ОписаниеРСВ_1 - Структура - структура с полями:
//     * Организация             - СправочникСсылка.Организации - организация;
//     * ДатаНачалаПериодаОтчета - Дата - дата начала периода отчета;
//     * ДатаКонцаПериодаОтчета  - Дата - дата конца периода отчета;
//     * КорректирующаяФорма     - Булево - признак корректирующей формы.
//   СтандартнаяОбработка - Булево - признак стандартной обработки.
//
Процедура ПриВыгрузкеРСВ_1ИзЖурналаРеглОтчетов(Ссылка, ОписаниеРСВ_1, СтандартнаяОбработка) Экспорт
	
	ПерсонифицированныйУчетКлиент.ПриВыгрузкеРСВ_1ИзЖурналаРеглОтчетов(Ссылка, ОписаниеРСВ_1, СтандартнаяОбработка);
	
КонецПроцедуры

// Процедура переопределяет поведение при проверке выгрузки формы РСВ-1 из журнала регламентированных отчетов.
//
// Параметры:
//   Ссылка        - ДокументСсылка.РегламентированныйОтчет - ссылка на регламентированный отчет.
//   ОписаниеРСВ_1 - Структура - структура с полями:
//     * Организация             - СправочникСсылка.Организации - организация;
//     * ДатаНачалаПериодаОтчета - Дата - дата начала периода отчета;
//     * ДатаКонцаПериодаОтчета  - Дата - дата конца периода отчета;
//     * КорректирующаяФорма     - Булево - признак корректирующей формы.
//   СтандартнаяОбработка - Булево - признак стандартной обработки.
//
Процедура ПриПроверкеВыгрузкиРСВ_1ИзЖурналаРеглОтчетов(Ссылка, ОписаниеРСВ_1, СтандартнаяОбработка) Экспорт
	
	ПерсонифицированныйУчетКлиент.ПриПроверкеВыгрузкиРСВ_1ИзЖурналаРеглОтчетов(Ссылка, ОписаниеРСВ_1, СтандартнаяОбработка)
	
КонецПроцедуры

// Процедура открывает форму с информацией об изменениях.
//
// Параметры:
//  ИмяОтчета - Строка - имя отчета в дереве объектов метаданных;
//  ИмяФормы  - Строка - имя формы отчета;
//  Форма     - УправляемаФорма - форма, из которой вызывается процедура.
//
Процедура ПредупредитьОбИзменениях(ИмяОтчета, ИмяФормы, Форма) Экспорт
	
КонецПроцедуры

// Процедура изменяет признак вывода предупреждения об изменениях.
//
// Параметры:
//  ИмяОтчета - Строка - имя отчета в дереве объектов метаданных;
//  ИмяФормы  - Строка - имя формы отчета;
//  ТребуетсяПредупредитьОбИзменениях - Булево - признак вывода предупреждения об изменениях;
//  Форма     - УправляемаФорма - форма, из которой вызывается процедура.
//
Процедура ИзменитьПризнакВыводаПредупрежденияОбИзменениях(ИмяОтчета, ИмяФормы, ТребуетсяПредупредитьОбИзменениях, Форма) Экспорт
	
КонецПроцедуры

// Если требуются какие-то особые действия при открытии уведомления надо выставить СтандартнаяОбработка = Ложь
// В этом случае уведомление не создается, а требуемые действия можно проделать в процедуре ОбработчикСозданияУведомления(...)
//
// Параметры:
//   Организация          - СправочникСсылка.Организации - ссылка на элемент справочника организаций.
//   ВидУведомления       - Перечисление.ВидыУведомленийОСпецрежимахНалогообложения - вид уведомления.
//   СтандартнаяОбработка - Булево - признак стандартной обработки.
//
Процедура ПередОткрытиемФормыУведомления(Организация, ВидУведомления, СтандартнаяОбработка) Экспорт
	
КонецПроцедуры

// При переопределенном действии при создании уведомления здесь прописывается что именно надо сделать
//
// Параметры:
//   Форма - Форма - форма 1с-отчетности.
//   Параметр - Структура - "Организация", "ВидУведомления".
//
Процедура ОбработчикСозданияУведомления(Форма, Параметр) Экспорт
	
КонецПроцедуры

// Процедура уточняет данные для автозаполнения уведомления
//
// Параметры:
//   ИДОтчета             - Строка - имя отчета в дереве объектов метаданных.
//   ПараметрыОтчета      - Структура - структура обязательных параметров.
//   ФормаОтчета          - УправляемаФорма - ссылка на заполняемую форму уведомления.
//   ОписаниеОповещения   - ОписаниеОповещения - процедура для продолжения процедуры автозаполнения.
//   СтандартнаяОбработка - Булево - если Истина то никакой новой информации не добавляется для автозаполнения,
//                                   необходимо продолжить автозаполнение в самой форме.
//
Процедура ПередЗаполнениемОтчета(ИДОтчета, ПараметрыОтчета, ФормаОтчета, ОписаниеОповещения, СтандартнаяОбработка = Неопределено) Экспорт
	
КонецПроцедуры

// Процедура переопределяет вывод дополнительной информации о регламентированном отчете
//
// Параметры:
//   Ссылка                    - ДокументСсылка.РегламентированныйОтчет - ссылка на регламентированный отчет.
//   ИмяФормыПодробнееОбОтчете - Строка - имя формы, которая будет открыта с параметром "Ссылка" для предоставления дополнительной информации об отчете.
//   СтандартнаяОбработка      - Булево - если Истина - будет открыта форма по имени: "Обработка.ОбщиеОбъектыРеглОтчетности.Форма." + ИмяФормыПодробнееОбОтчете;
//                                        иначе - можно выполнить свой алгоритм обработки ПодробнееОбОчете().
//
Процедура ПодробнееОбОчете(Ссылка, ИмяФормыПодробнееОбОтчете, СтандартнаяОбработка) Экспорт
	
КонецПроцедуры

// Дополняет функционал обработчика "ОбработкаОповещения" общей формы "Регламентированная отчетность",
// здесь можно добавить новую функциональность к существующей обработке оповещения.
//
// Параметры:
//  ИмяСобытия - Строка - Имя события может быть использовано для идентификации сообщений принимающими их формами.
//  Параметр   - Параметр сообщения. Могут быть переданы любые необходимые данные.
//  Источник   - Источник события. Например, в качестве источника может быть указана другая форма.
//  Форма      - УправляемаяФорма - Общая форма "Регламентированная отчетность".
//
// Пример:
//  Если ИмяСобытия = "Получены новые сообщения 1С-Отчетности" И Источник <> Форма Тогда
//      Оповестить("Закрыть форму новых сообщений 1С-Отчетности",, Форма);
//  КонецЕсли;
//
Процедура ФормаРегламентированнойОтчетности_ОбработкаОповещения(ИмяСобытия, Параметр, Источник, Форма) Экспорт
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// Служебные процедуры и функции.

// Процедура вызывается в формах отчета "РегламентированныйОтчетРСВ1"
// при нажатии кнопки перехода в специализированное рабочее место подготовки
// отчетности в ПФР.
//
// Параметры:
//   Организация - СправочникСсылка.Организации - ссылка на элемент справочника организаций.
//   ДатаНачалаПериодаОтчета - Дата - дата начала периода отчета.
//   ДатаКонцаПериодаОтчета - Дата - дата конца периода отчета.
//
Процедура ПерейтиВРабочееМестоПФР(Организация, ДатаНачалаПериодаОтчета, ДатаКонцаПериодаОтчета) Экспорт
	ЗарплатаКадрыКлиент.ПерейтиВРабочееМестоПодготовкиКвартальнойОтчетностиПФР(Организация, ДатаНачалаПериодаОтчета);	
КонецПроцедуры

// Процедура выполняет назначаемую команду формы.
//
// Параметры:
//   Форма - Форма - форма, из которой вызвана команда.
//
Процедура ВыполнитьНазначаемуюКомандуНаКлиенте(Форма) Экспорт
	
КонецПроцедуры

// Процедура выполняет назначаемую команду формы "РегламентированнаяОтчетность".
//
// Параметры:
//   Форма   - Форма - форма, из которой вызвана команда.
//   Команда - КомандаФормы - назначенная команда формы.
//
// Пример:
//   Если Команда.Имя = "ОтчетыПоказатьДополнительнуюИнформацию" Тогда
//   	ПоказатьПредупреждение(, "Заполните обработчик команды """ + Команда.Имя + """");
//   КонецЕсли;
//
Процедура ФормаРегламентированнойОтчетности_ВыполнитьНазначаемуюКомандуНаКлиенте(Форма, Команда) Экспорт
	
КонецПроцедуры

// Процедура переопределяет имя формы выбора периода для установки отбора в форме "1С Отчетность".
//
// Параметры:
//  ПолноеИмяФормыВыбораПериода  - Строка - Полный путь к форме выбора периода.
//
// Пример:
//  ПолноеИмяФормыВыбораПериода = "ОбщаяФорма.ВыборПроизвольногоПериода";
//
Процедура ФормаРегламентированнойОтчетности_ИмяФормыВыбораПериода(ПолноеИмяФормыВыбораПериода) Экспорт
	
КонецПроцедуры

// Процедура проверяет, выполнялась ли ранее настройка автозаполнения.
//
// Параметры:
//   ПараметрыОтчета       - Структура - параметры регл. отчета.
//   ВыполняемоеОповещение - ОписаниеОповещения - Описание оповещения, которое будет вызвано после выполнения данной операции.
//                                                В качестве результата описания оповещения должно передаваться булево значение,
//												  от которого зависит будет ли выполнятся дальнейший код в процедуре,
//												  которая вызвала этот метод.
//
Процедура ПроверитьНастройкиЗаполненияОтчета(ПараметрыОтчета, ВыполняемоеОповещение) Экспорт
	
	ИмяОтчета   = ПараметрыОтчета.ИДОтчета;
	ИмяФормы    = ПараметрыОтчета.ИДРедакцииОтчета;
	Организация = ПараметрыОтчета.Организация;
	
	РезультатПроверки = ЗаполнениеФормСтатистикиВызовСервера.ПроверитьНастройкиЗаполненияОтчета(ИмяОтчета, ИмяФормы, Организация);
		
	Если РезультатПроверки = Неопределено Тогда
		// У пользователя нет прав на редактирование настроек.
		// Поэтому предложить ему ничего не можем.
		// Либо настройка формы не предусмотрена.
		ВыполнитьОбработкуОповещения(ВыполняемоеОповещение, Истина);
		Возврат;
	КонецЕсли;
	
	ФормаСтатистики 			= РезультатПроверки.ФормаСтатистики;
	ВыполнялисьНастройкиФормы 	= РезультатПроверки.ВыполнялисьНастройкиФормы;
	
	Если Не ВыполнялисьНастройкиФормы Тогда
		
		Кнопки = Новый СписокЗначений();
		
		Кнопки.Добавить(КодВозвратаДиалога.Да, 		"Открыть настройки");
		Кнопки.Добавить(КодВозвратаДиалога.Нет, 	"Заполнить");
		Кнопки.Добавить(КодВозвратаДиалога.Отмена, 	"Отмена");
		ДополнительныеПараметры = Новый Структура;
		ДополнительныеПараметры.Вставить("ВыполняемоеОповещение", ВыполняемоеОповещение);
		ДополнительныеПараметры.Вставить("Организация", Организация);
		ДополнительныеПараметры.Вставить("ФормаСтатистики", ФормаСтатистики);
		
		ОписаниеОповещения = Новый ОписаниеОповещения("ПроверитьНастройкиЗаполненияОтчетаЗавершение", ЭтотОбъект, ДополнительныеПараметры);
		ТекстВопроса = "Перед заполнением этой формы рекомендуем проверить настройки заполнения
			|";
		ПоказатьВопрос(ОписаниеОповещения, ТекстВопроса, Кнопки);
		
	Иначе
		
		ВыполнитьОбработкуОповещения(ВыполняемоеОповещение, Истина);
		
	КонецЕсли;
	
КонецПроцедуры	

// Процедура открывает форму настройки автозаполнения.
//
// Параметры:
//   ПараметрыФормы - Структура - параметры настройки автозаполнения формы.
//
Процедура ОткрытьФормуНастройкиАвтозаполнения(ПараметрыФормы) Экспорт
	
	Перем ИмяОтчета, ИмяФормы, ИмяПоля, Организация;
	
	ПараметрыФормы.Свойство("ИДОтчета",             ИмяОтчета);
	ПараметрыФормы.Свойство("ИДРедакцииОтчета",     ИмяФормы);
	ПараметрыФормы.Свойство("ИДТекущегоПоказателя", ИмяПоля); // Может не передаваться или передаваться Неопределено, если пользователь хочет выполнить настройку для всей формы
	ПараметрыФормы.Свойство("Организация",          Организация);
	
	ОписаниеФормы = ЗаполнениеФормСтатистикиВызовСервера.ОписаниеФормыНастройкиПоля(ИмяОтчета, ИмяФормы, ИмяПоля, Организация);
	
	Если ТипЗнч(ОписаниеФормы) = Тип("Строка") Тогда
		
		ПоказатьПредупреждение(,ОписаниеФормы);
		
	Иначе
		
		ОткрытьФорму(ОписаниеФормы.ИмяФормы, ОписаниеФормы.ПараметрыФормы);
		
	КонецЕсли;
	
КонецПроцедуры

// Процедура реализует печать объектов, отображаемых на закладке Отчеты и Уведомления формы Отчетность.
//
// Параметры:
//	 Ссылка - СправочникСсылка, ДокументСсылка - ссылка на объект, который необходимо напечатать.
//                                        		 Если для данного объекта печать невозможна,
//                                               нужно выдавать соотвествующее предупрежедние.
//	 ИмяМакетаДляПечати - Строка - имя макета для печати, при использовании которого необходимо распечатать объект.
//		                           Если ИмяМакетаДляПечати пустое, то для печати использовать основной макет.
// 	 СтандартнаяОбработка - Булево - если СтандартнаяОбработка = Истина, то будет выполнена печать с помощью подсистемы печати из БСП.
//
Процедура Печать(Ссылка, ИмяМакетаДляПечати, СтандартнаяОбработка) Экспорт
	РегламентированнаяОтчетностьУПКлиент.ПечатьРегламентированногоОтчета(Ссылка, ИмяМакетаДляПечати, СтандартнаяОбработка);
	ЗарплатаКадрыКлиент.ПечатьДокументаОтчетности(Ссылка, ИмяМакетаДляПечати, СтандартнаяОбработка);		
КонецПроцедуры

// Процедура реализует выгрузку объектов, отображаемых на закладке Отчеты и Уведомления формы Отчетность.
// Параметры:
//	 Ссылка - СправочникСсылка, ДокументСсылка - ссылка на объект, который необходимо выгрузить.
//                                               Если для данного объекта выгрузка невозможна,
//												 нужно выдавать соответствующее предупреждение.
//
Процедура Выгрузить(Ссылка) Экспорт
	РегламентированнаяОтчетностьУПКлиент.ВыгрузкаРегламентированногоОтчета(Ссылка);
	ЗарплатаКадрыКлиент.ВыгрузитьДокументОтчетности(Ссылка);	
КонецПроцедуры

// Процедура реализует создание объектов, отображаемых на закладке Уведомления и Отчетность формы Отчетность, не входящие в состав БРО.
//
// Параметры:
//	Организация - СправочникСсылка.Организации - организация, по которой нужно создать объект.
//  Тип - Тип - тип объекта, который необходимо создать.
//  СтандартнаяОбработка - Булево - если СтандартнаяОбработка = Истина, то будет выполнено создание объекта стандартным образом.
//
Процедура СоздатьНовыйОбъект(Организация, Тип, СтандартнаяОбработка) Экспорт
	РегламентированнаяОтчетностьУПКлиент.СозданиеРегламентированногоОтчета(Организация, Тип, СтандартнаяОбработка);
	ЗарплатаКадрыКлиент.СоздатьНовыйДокументОтчетности(Организация, Тип, СтандартнаяОбработка);	
КонецПроцедуры

// Процедура открывает окно выбора обособленных подразделений в случае, когда стандартный механизм 
// по каким-либо причинам не может быть использован.
// Вызывается из форм статистики (ЗП-*, П-4 и некоторых других).
// В качестве callback-процедуры следует использовать РегламентированнаяОтчетностьКлиент.ОбработкаВыбораОбособленногоПодразделения.
//
// Параметры:
//   Форма - Форма - форма отчета откуда открывается выбор подразделения.
//   СтандартнаяОбработка - Булево - если отработано необходимо выставить СтандартнаяОбработка = Ложь.
//
// Пример для ЗУП 3.0 КОРП:
//   Там форма выбора это форма списка с параметром открытия РежимВыбора = Истина
//   СтандартнаяОбработка = Ложь;
//   Отбор = Новый Структура("Владелец, ИмеетНомерТерриториальногоОрганаРосстата", Форма.СтруктураРеквизитовФормы.Организация, Истина);
//   Параметры = Новый Структура("Отбор, РежимВыбора", Отбор, Истина);
//   ОповещениеОЗакрытии = Новый ОписаниеОповещения("ОбработкаВыбораОбособленногоПодразделения", РегламентированнаяОтчетностьКлиент, Новый Структура("Форма", Форма));
//   ОткрытьФорму("Справочник.ПодразделенияОрганизаций.ФормаВыбора",Параметры,Форма,,,,ОповещениеОЗакрытии,РежимОткрытияОкнаФормы.БлокироватьОкноВладельца);
//
Процедура ОбработкаВыбораПоляОбособленныхПодразделений(Форма, СтандартнаяОбработка) Экспорт
	
КонецПроцедуры

// Процедура определяет, открывать или нет из формы "1С-Отчетность" файлы, присоединенные к объекту.
//
// Параметры:
//  ПараметрыПроверки - Структура:
//   "Владелец"             - СправочникСсылка, ДокументСсылка - ссылка на объект;
//   "ТекстПредупреждения"  - Строка - если заполнен и СтандартнаяОбработка = Ложь, то будет выведено предупреждение;
//   "СтандартнаяОбработка" - Булево - если Ложь, открытие присоединенных файлов не выполняется.
//
// Пример:
//  Если ТипЗнч(ПараметрыПроверки.Владелец) = Тип("СправочникСсылка.МакетыПенсионныхДел") Тогда
//  	
//  	ПараметрыПроверки.СтандартнаяОбработка = Ложь;
//  	
//  	ПараметрыПроверки.ТекстПредупреждения = НСтр(
//  	"ru='Присоединение файлов к ""Макетам пенсионных дел"" из списка отчетов не предусмотрено'");
//  	
//  КонецЕсли;
//
Процедура ПроверитьВладельцаПриОткрытииПрисоединенныхФайловИзСпискаОтчетов(ПараметрыПроверки) Экспорт
	
КонецПроцедуры

// Процедура завершения проверки настройки заполнения статистических отчетов
// Вызывается если задан вопрос, про проверку настроек заполнения.
//
Процедура ПроверитьНастройкиЗаполненияОтчетаЗавершение(Ответ, ДополнительныеПараметры) Экспорт
	
	Организация = ДополнительныеПараметры.Организация;
	ФормаСтатистики = ДополнительныеПараметры.ФормаСтатистики;
	ВыполняемоеОповещение = ДополнительныеПараметры.ВыполняемоеОповещение;
	
	Если Ответ = КодВозвратаДиалога.Да Тогда
		ПараметрыФормы = Новый Структура;
		ПараметрыФормы.Вставить("Организация", Организация);
		ПараметрыФормы.Вставить("ФормаСтатистики", ФормаСтатистики);
		ОткрытьФорму("РегистрСведений.НастройкаЗаполненияФормСтатистики.Форма.НастройкаЗаполненияФормСтатистики", ПараметрыФормы);
		Результат = Ложь;
	ИначеЕсли Ответ = КодВозвратаДиалога.Нет Тогда
		ЗаполнениеФормСтатистикиВызовСервера.ЗаписатьНастройкиВыполнены(ФормаСтатистики, Организация);
		Результат = Истина;
	Иначе
		Результат = Ложь;
	КонецЕсли;
	
	ВыполнитьОбработкуОповещения(ВыполняемоеОповещение, Результат);
	
КонецПроцедуры

Процедура ОткрытьРасшифровкуЗавершение(Результат, ДополнительныеПараметры) Экспорт
	
	ЗаголовокРасшифровкиДляВсегоРаздела = ДополнительныеПараметры.ЗаголовокРасшифровкиДляВсегоРаздела;
	ПараметрыФормы = ДополнительныеПараметры.ПараметрыФормы;
	Если Результат = КодВозвратаДиалога.Да Тогда
		ПоказатьРасшифровкуВсехПоказателейФормы(ЗаголовокРасшифровкиДляВсегоРаздела, ПараметрыФормы);
	КонецЕсли;
	
КонецПроцедуры

Процедура ПоказатьРасшифровкуВсехПоказателейФормы(ЗаголовокРасшифровкиДляВсегоРаздела, ПараметрыФормы)
	
	ПараметрыФормы.Вставить("ЗаголовокРасшифровки", ЗаголовокРасшифровкиДляВсегоРаздела);
	
	ФормаРасшифровки = ПолучитьФорму("ОбщаяФорма.РасшифровкаПоказателейРегламентированныхОтчетов", ПараметрыФормы, , Истина);
	ФормаРасшифровки.Открыть();
	
КонецПроцедуры

Функция НаименованиеРасшифровкиБухОтчетности(НаименованиеОтчета, НачалоПериода, КонецПериода)
	
	Если НачалоГода(НачалоПериода) <> НачалоДня(НачалоПериода) Тогда
		ПериодОтчета = Формат(НачалоПериода, "ДФ=dd.MM.yyyy") + " - " + Формат(КонецПериода, "ДФ='ММММ гггг'") + " г." ;
	Иначе
		Если Месяц(КонецПериода) = 1 Тогда 
			ПериодОтчета = Формат(КонецПериода, "ДФ='ММММ гггг'") + " г." ;
		Иначе
			ПериодОтчета = "Январь - " + Формат(КонецПериода, "ДФ='ММММ гггг'") + " г." ;
		КонецЕсли;
	КонецЕсли;
	
	Возврат НаименованиеОтчета + " за " + ПериодОтчета + " - расшифровка значений показателей";
	
КонецФункции

Функция КопияСтруктуры(ИсходнаяСтруктура)
	
	Результат = Новый Структура;
	
	Для Каждого Элемент Из ИсходнаяСтруктура Цикл
		Результат.Вставить(Элемент.Ключ, Элемент.Значение);
	КонецЦикла;
	
	Возврат Результат;
	
КонецФункции

Процедура ОткрытьРасшифровкуОтчетНДС(ИДИменПоказателей, ПараметрыОтчета)
	
	Если ИДИменПоказателей.Количество() > 0 Тогда
		
		ПользовательскиеНастройки = Неопределено;
		
		ИдПоказателя = ИДИменПоказателей[0];
		
		ИдентификаторМакета = УчетНДСВызовСервера.ПолучитьИдентификаторМакетаРасшифровкиДекларацииПоНДС(ПараметрыОтчета, ИдПоказателя, ПользовательскиеНастройки);
		
		Если ЗначениеЗаполнено(ИдентификаторМакета) Тогда
			
			Если ТипЗнч(ИдентификаторМакета) = Тип("СправочникСсылка.ОбъектыЭксплуатации") Тогда
				
				ПараметрыФормы = Новый Структура();
				ПараметрыФормы.Вставить("Ключ", ИдентификаторМакета);
				ОткрытьФорму("Справочник.ОбъектыЭксплуатации.ФормаОбъекта", ПараметрыФормы,, Новый УникальныйИдентификатор);
				
			Иначе
				
				Если ПользовательскиеНастройки = Неопределено Тогда
					ПользовательскиеНастройки = Новый ПользовательскиеНастройкиКомпоновкиДанных;
				КонецЕсли;
				
				ЕстьОтбор = ПользовательскиеНастройки.Элементы.Найти("Отбор") <> Неопределено;
				ЗаполняемыеНастройки = Новый Структура("Показатели, Группировка, Отбор, ВыводимыеДанные", Ложь, Ложь, ЕстьОтбор, Ложь);
				
				ДополнительныеСвойства = ПользовательскиеНастройки.ДополнительныеСвойства;
				ДополнительныеСвойства.Вставить("РежимРасшифровки",  Истина);
				ДополнительныеСвойства.Вставить("Организация",       ПараметрыОтчета.Организация);
				ДополнительныеСвойства.Вставить("НачалоПериода",     ПараметрыОтчета.ДатаНачалаПериодаОтчета);
				ДополнительныеСвойства.Вставить("КонецПериода",      ПараметрыОтчета.ДатаКонцаПериодаОтчета);
				ДополнительныеСвойства.Вставить("ВыводитьЗаголовок", Истина);
				ДополнительныеСвойства.Вставить("КлючВарианта",      ИдентификаторМакета);
				
				ГруппаОрганизаций = Новый СписокЗначений;
				Если ПараметрыОтчета.Свойство("ГруппаОрганизаций", ГруппаОрганизаций)
					И ТипЗнч(ГруппаОрганизаций) = Тип("СписокЗначений")
					И ГруппаОрганизаций.Количество() > 0 Тогда
					
					ДополнительныеСвойства.Вставить("ВключатьОбособленныеПодразделения", Истина);
					
				КонецЕсли;
				
				ПараметрыФормы = Новый Структура();
				ПараметрыФормы.Вставить("ВидРасшифровки", 2);
				ПараметрыФормы.Вставить("СформироватьПриОткрытии", Истина);
				ПараметрыФормы.Вставить("ИДРасшифровки", ИдентификаторМакета);
				ПараметрыФормы.Вставить("ЗаполняемыеНастройки", ЗаполняемыеНастройки);
				ПараметрыФормы.Вставить("ПользовательскиеНастройки", ПользовательскиеНастройки);
				
				ОткрытьФорму("Отчет.РасшифровкаПоказателейНДС.Форма.ФормаОтчета", ПараметрыФормы,, Новый УникальныйИдентификатор);
			КонецЕсли;
		Иначе
			ТекстПредупреждения = НСтр("ru = 'Для выбранной ячейки расшифровка не существует.'");
			ПоказатьПредупреждение(,ТекстПредупреждения);
		КонецЕсли;
		
	КонецЕсли;
	
КонецПроцедуры

#Область МеханизмТарификации

// Проверяет возможность автоматического заполнения регламентированного отчета по данным ИБ конфигурации.
//
// Параметры:
//  ИмяФормы - Строка - имя формы регламентированного отчета;
//  Отказ - Булево - признак отказа от автозаполнения регл. отчета.
//
// Пример реализации:
//  Если НЕ ТарификацияБПВызовСервераПовтИсп.РазрешенУчетРегулярнойДеятельности() Тогда
//  	ТарификацияБПКлиент.ОповеститьОбОграниченииТарифа(ИмяФормы + ".ЗаполнитьАвто");
//      Отказ = Истина;
//  КонецЕсли;
//
Процедура ПроверитьВозможностьАвтоЗаполненияРеглОтчета(ИмяФормы, Отказ) Экспорт
 		 	
КонецПроцедуры

#КонецОбласти

#КонецОбласти