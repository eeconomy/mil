﻿
#Область СлужебныйПрограммныйИнтерфейс

Функция ПараметрыРедактированияСреднегоЗаработка(ОписаниеОбъекта) Экспорт
	
	Возврат РезервОтпусковРасширенный.ПараметрыРедактированияСреднегоЗаработка(ОписаниеОбъекта);
	
КонецФункции

#КонецОбласти
