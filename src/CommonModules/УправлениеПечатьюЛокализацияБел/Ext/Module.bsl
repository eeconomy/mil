﻿///////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2019, ООО 1С-Софт
// Все права защищены. Эта программа и сопроводительные материалы предоставляются 
// в соответствии с условиями лицензии Attribution 4.0 International (CC BY 4.0)
// Текст лицензии доступен по ссылке:
// https://creativecommons.org/licenses/by/4.0/legalcode
///////////////////////////////////////////////////////////////////////////////////////////////////////

#Область ПрограммныйИнтерфейс

// Определяет объекты конфигурации, в модулях менеджеров которых размещена процедура ДобавитьКомандыПечати,
// формирующая список команд печати, предоставляемых этим объектом.
// Синтаксис процедуры ДобавитьКомандыПечати см. в документации к подсистеме.
//
// Параметры:
//  СписокОбъектов - Массив - менеджеры объектов с процедурой ДобавитьКомандыПечати.
//
Процедура ПриОпределенииОбъектовСКомандамиПечати(СписокОбъектов) Экспорт
	
	// 4D:ERP для Беларуси 
	// Учет бланков строгой отчетности
	// {
	СписокОбъектов.Добавить(Документы.ИнвентаризационнаяОписьБСО);
	// }
	// 4D
	
	// 4D:ERP для Беларуси, Антон,  
	// печатная форма ИнвентаризационнаяОписьТМЦ, №24325 
	// {
	СписокОбъектов.Добавить(Документы.ИнвентаризационнаяОписьТМЦ);
	// }
	// 4D
	
	// 4D:ERP для Беларуси
	// Учет драгоценных материалов
	// {
	СписокОбъектов.Добавить(Документы.КорректировкаДрагоценныхМатериалов);
	// }
	// 4D   
	
	//++ ЛокализацияБел
	СписокОбъектов.Добавить(Документы.ПачкаДокументовПУ_1);
	СписокОбъектов.Добавить(Документы.ПачкаДокументовПУ_2);
	СписокОбъектов.Добавить(Документы.ПачкаДокументовПУ_3);
	СписокОбъектов.Добавить(Документы.ВыдачаЗаймаСотруднику);
	СписокОбъектов.Добавить(Документы.ПогашениеЗаймаСотруднику);
	СписокОбъектов.Добавить(Документы.ЗаявленияОПредоставленииСведенийОТрудовойДеятельности);
	СписокОбъектов.Добавить(Документы.ОтзывЗаявленийОПредоставленииСведенийОТрудовойДеятельности);
	СписокОбъектов.Добавить(Документы.РегистрацияТрудовойДеятельности);
	СписокОбъектов.Добавить(Документы.УведомлениеОПрекращенииОтпускаПоУходуЗаРебенком);
	СписокОбъектов.Добавить(Документы.НачислениеПособий);
	СписокОбъектов.Добавить(Обработки.ГрупповоеРедактированиеНачислений);
	СписокОбъектов.Добавить(Обработки.ПлатежиПоРезультатамРасчетаЗарплаты);
	СписокОбъектов.Добавить(Обработки.ПособияВыплачиваемыеФСС);
	//-- ЛокализацияБел  
	
	// 4D:ERP для Беларуси, ЕленаТ,  
	// печатная форма ИнвентаризацияНЗП, №31380 
	// {
	СписокОбъектов.Добавить(Документы.ИнвентаризацияНЗП);
	// } 4D
	
	// 4D:ERP для Беларуси, ВладимирР, 26.05.2022 11:40:45
	// Расчет НДС к вычету, №33515
	// {
	СписокОбъектов.Добавить(Документы.РасчетСуммНДСПодлежащихВычету);
	// }
	// 4D
	
КонецПроцедуры

// Позволяет переопределить список команд печати в произвольной форме.
// Может использоваться для общих форм, у которых нет модуля менеджера для размещения в нем процедуры ДобавитьКомандыПечати,
// для случаев, когда штатных средств добавления команд в такие формы недостаточно. 
// Например, если в общих формах нужны специфические команды печати.
// Вызывается из функции УправлениеПечатью.КомандыПечатиФормы.
// 
// Параметры:
//  ИмяФормы             - Строка - полное имя формы, в которой добавляются команды печати;
//  КомандыПечати        - ТаблицаЗначений - см. УправлениеПечатью.СоздатьКоллекциюКомандПечати;
//  СтандартнаяОбработка - Булево - при установке значения Ложь не будет автоматически заполняться коллекция КомандыПечати.
//
// Пример:
//  Если ИмяФормы = "ОбщаяФорма.ЖурналДокументов" Тогда
//    Если Пользователи.РолиДоступны("ПечатьСчетаНаОплатуНаПринтер") Тогда
//      КомандаПечати = КомандыПечати.Добавить();
//      КомандаПечати.Идентификатор = "Счет";
//      КомандаПечати.Представление = НСтр("ru = 'Счет на оплату (на принтер)'");
//      КомандаПечати.Картинка = БиблиотекаКартинок.ПечатьСразу;
//      КомандаПечати.ПроверкаПроведенияПередПечатью = Истина;
//      КомандаПечати.СразуНаПринтер = Истина;
//    КонецЕсли;
//  КонецЕсли;
//
Процедура ПередДобавлениемКомандПечати(ИмяФормы, КомандыПечати, СтандартнаяОбработка) Экспорт
	
// TODO	
	
//	// {{ Локализация_УправлениеТорговлейДляБеларуси
//	УправлениеПечатьюПереопределяемый_Локализация.ПередДобавлениемКомандПечати_СписокОбъектов(СписокОбъектов, КомандыПечати);
//	// Локализация_УправлениеТорговлейДляБеларуси }}
//			
//	// {{ Локализация_КомплекснаяАвтоматизацияДляБеларуси
//	УправлениеПечатьюПереопределяемый_ЛокализацияКА.ПередДобавлениемКомандПечати_СписокОбъектов(СписокОбъектов, КомандыПечати);
//	// Локализация_КомплекснаяАвтоматизацияДляБеларуси }}
	
	
	
	// {{ Локализация_УправлениеТорговлейДляБеларуси
	УправлениеПечатьюПереопределяемый_Локализация.ПередДобавлениемКомандПечати_Локализация(ИмяФормы, КомандыПечати, СтандартнаяОбработка);
	// Локализация_УправлениеТорговлейДляБеларуси }}
	// {{ Локализация_КомплекснаяАвтоматизацияДляБеларуси
	УправлениеПечатьюПереопределяемый_ЛокализацияКА.ПередДобавлениемКомандПечати_Локализация(ИмяФормы, КомандыПечати, СтандартнаяОбработка);
	// Локализация_КомплекснаяАвтоматизацияДляБеларуси }}
	
КонецПроцедуры

// Позволяет задать дополнительные настройки команд печати в журналах документов.
//
// Параметры:
//  НастройкиСписка - Структура - модификаторы списка команд печати.
//   * МенеджерКомандПечати     - МенеджерОбъекта - менеджер объекта, в котором формируется список команд печати;
//   * АвтоматическоеЗаполнение - Булево - заполнять команды печати из объектов, входящих в состав журнала.
//                                         Если установлено значение Ложь, то список команд печати журнала будет
//                                         заполнен вызовом метода ДобавитьКомандыПечати из модуля менеджера журнала.
//                                         Значение по умолчанию - Истина - метод ДобавитьКомандыПечати будет вызван из
//                                         модулей менеджеров документов, входящих в состав журнала.
//
// Пример:
//   Если НастройкиСписка.МенеджерКомандПечати = "ЖурналДокументов.СкладскиеДокументы" Тогда
//     НастройкиСписка.АвтоматическоеЗаполнение = Ложь;
//   КонецЕсли;
//
Процедура ПриПолученииНастроекСпискаКомандПечати(НастройкиСписка) Экспорт


КонецПроцедуры

// Вызывается после завершения вызова процедуры Печать менеджера печати объекта, имеет те же параметры.
// Может использоваться для постобработки всех печатных форм при их формировании.
// Например, можно вставить в колонтитул дату формирования печатной формы.
//
// Параметры:
//  МассивОбъектов - Массив - список объектов, для которых была выполнена процедура Печать;
//  ПараметрыПечати - Структура - произвольные параметры, переданные при вызове команды печати;
//  КоллекцияПечатныхФорм - ТаблицаЗначений - содержит табличные документы и дополнительную информацию;
//  ОбъектыПечати - СписокЗначений - соответствие между объектами и именами областей в табличных документах, где
//                                   значение - Объект, представление - имя области с объектом в табличных документах;
//  ПараметрыВывода - Структура - параметры, связанные с выводом табличных документов:
//   * ПараметрыОтправки - Структура - информация для заполнения письма при отправке печатной формы по электронной почте.
//                                     Содержит следующие поля (описание см. в общем модуле конфигурации
//                                     РаботаСПочтовымиСообщениямиКлиент в процедуре СоздатьНовоеПисьмо):
//    ** Получатель;
//    ** Тема,
//    ** Текст.
Процедура ПриПечати(МассивОбъектов, ПараметрыПечати, КоллекцияПечатныхФорм, ОбъектыПечати, ПараметрыВывода) Экспорт

	
КонецПроцедуры

// Вызывается из обработчика ПриСозданииНаСервере формы печати документов (ОбщаяФорма.ПечатьДокументов).
// Позволяет изменить внешний вид и поведение формы, например, разместить на ней дополнительные элементы:
// информационные надписи, кнопки, гиперссылки, различные настройки и т.п.
//
// При добавлении команд (кнопок) в качестве обработчика следует указывать имя "Подключаемый_ВыполнитьКоманду",
// а его реализацию размещать в УправлениеПечатьюПереопределяемый.ПечатьДокументовПриВыполненииКоманды (серверная часть),
// либо в УправлениеПечатьюКлиентПереопределяемый.ПечатьДокументовВыполнитьКоманду (клиентская часть).
//
// Для того, чтобы добавить свою команду на форму, необходимо сделать следующее.
// 1. Создать команду и кнопку в УправлениеПечатьюПереопределяемый.ПечатьДокументовПриСозданииНаСервере.
// 2. Реализовать клиентский обработчик команды в УправлениеПечатьюКлиентПереопределяемый.ПечатьДокументовВыполнитьКоманду.
// 3. (Опционально) Реализовать серверный обработчик команды в УправлениеПечатьюПереопределяемый.ПечатьДокументовПриВыполненииКоманды.
//
// При добавлении гиперссылок в качестве обработчика нажатия следует указывать имя "Подключаемый_ОбработкаНавигационнойСсылки",
// а его реализацию размещать в УправлениеПечатьюКлиентПереопределяемый.ПечатьДокументовОбработкаНавигационнойСсылки.
//
// При размещении элементов, значение которых должны запоминаться между открытиями формы печати,
// следует воспользоваться процедурами ПечатьДокументовПриЗагрузкеДанныхИзНастроекНаСервере и
// ПечатьДокументовПриСохраненииДанныхВНастройкахНаСервере.
//
// Параметры:
//  Форма                - ФормаКлиентскогоПриложения - форма ОбщаяФорма.ПечатьДокументов.
//  Отказ                - Булево - признак отказа от создания формы. Если установить
//                                  данному параметру значение Истина, то форма создана не будет.
//  СтандартнаяОбработка - Булево - в данный параметр передается признак выполнения стандартной (системной) обработки
//                                  события. Если установить данному параметру значение Ложь, 
//                                  стандартная обработка события производиться не будет.
// 
// Пример:
//  КомандаФормы = Форма.Команды.Добавить("МояКоманда");
//  КомандаФормы.Действие = "Подключаемый_ВыполнитьКоманду";
//  КомандаФормы.Заголовок = НСтр("ru = 'Моя команда'");
//  
//  КнопкаФормы = Форма.Элементы.Добавить(КомандаФормы.Имя, Тип("КнопкаФормы"), Форма.Элементы.КоманднаяПанельПраваяЧасть);
//  КнопкаФормы.Вид = ВидКнопкиФормы.КнопкаКоманднойПанели;
//  КнопкаФормы.ИмяКоманды = КомандаФормы.Имя;
//
Процедура ПечатьДокументовПриСозданииНаСервере(Форма, Отказ, СтандартнаяОбработка) Экспорт
	
	
КонецПроцедуры

#КонецОбласти
