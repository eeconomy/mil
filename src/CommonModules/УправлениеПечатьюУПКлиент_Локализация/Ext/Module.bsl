﻿
#Область ПрограммныйИнтерфейс

Функция ПечатьТТН(ОписаниеКоманды) Экспорт
	МассивОбъектов = ОписаниеКоманды.ОбъектыПечати;
	ОбъектыПечати = ОписаниеКоманды.ОбъектыПечати;
	ТипДокументов = ТипЗнч(МассивОбъектов[0]);
	
	КоличествоРаспоряженийБезТранспортныхНакладных = 0;
	Если ТипДокументов = Тип("ДокументСсылка.ЗаданиеНаПеревозку") Тогда
		КоличествоРаспоряженийБезТранспортныхНакладных = УправлениеПечатьюУТВызовСервераЛокализация.ПроверитьНаличиеТранспортныхНакладныхДляРаспоряженийИзЗаданийНаПеревозку(МассивОбъектов);
	ИначеЕсли ТипДокументов <> Тип("ДокументСсылка.ТранспортнаяНакладная") Тогда
		СтруктураВозврата = УправлениеПечатьюУТВызовСервераЛокализация.ПроверитьНаличиеТранспортныхНакладныхДляРаспоряжений(МассивОбъектов);
		ОбъектыПечати = СтруктураВозврата.МассивОбъектов;
		КоличествоРаспоряженийБезТранспортныхНакладных = СтруктураВозврата.КоличествоРаспоряженийБезТранспортныхНакладных;
	КонецЕсли;
	
	Если КоличествоРаспоряженийБезТранспортныхНакладных <> 0 Тогда
		СписокЗначенийКнопок = Новый СписокЗначений;
		СписокЗначенийКнопок.Добавить(КодВозвратаДиалога.ОК, НСтр("ru = 'Создать и вывести на печать'"));
		СписокЗначенийКнопок.Добавить(КодВозвратаДиалога.Отмена, НСтр("ru = 'Отмена'"));
		
		ДополнительныеПараметры = Новый Структура;
		ДополнительныеПараметры.Вставить("ТипДокументов", ТипДокументов);
		ДополнительныеПараметры.Вставить("МассивОбъектов", МассивОбъектов);
		ДополнительныеПараметры.Вставить("ОписаниеКоманды", ОписаниеКоманды);
		
		ОповещениеОЗавершении = Новый ОписаниеОповещения("НеСозданыТранспортныеНакладныеЗавершение", УправлениеПечатьюУТКлиентЛокализация, ДополнительныеПараметры);
		ПоказатьВопрос(ОповещениеОЗавершении, НСтр("ru = 'Для некоторых распоряжений не созданы транспортные накладные.'"), СписокЗначенийКнопок);
	Иначе
		Если ОписаниеКоманды.Идентификатор = "ТТН_ВертикальнаяСПриложением" Тогда
			МенеджерПечати = "Обработка.ПечатьТТН";
			ИменаМакетов = "ТТН_ВертикальнаяСПриложением";
		ИначеЕсли ОписаниеКоманды.Идентификатор = "ТТН_Вертикальная" Тогда
			МенеджерПечати = "Обработка.ПечатьТТН";
			ИменаМакетов = "ТТН_Вертикальная";
		ИначеЕсли ОписаниеКоманды.Идентификатор = "ТТН_Горизонтальная" Тогда
			МенеджерПечати = "Обработка.ПечатьТТН";
			ИменаМакетов = "ТТН_Горизонтальная";
		ИначеЕсли ОписаниеКоманды.Идентификатор = "ТТН_ГоризонтальнаяСПриложением" Тогда
			МенеджерПечати = "Обработка.ПечатьТТН";
			ИменаМакетов = "ТТН_ГоризонтальнаяСПриложением";
		ИначеЕсли ОписаниеКоманды.Идентификатор = "ТН_Вертикальная" Тогда
			МенеджерПечати = "Обработка.ПечатьТТН";
			ИменаМакетов = "ТН_Вертикальная";
		ИначеЕсли ОписаниеКоманды.Идентификатор = "ТН_ВертикальнаяСПриложением" Тогда
			МенеджерПечати = "Обработка.ПечатьТТН";
			ИменаМакетов = "ТН_ВертикальнаяСПриложением";
		ИначеЕсли ОписаниеКоманды.Идентификатор = "ТН_Горизонтальная" Тогда
			МенеджерПечати = "Обработка.ПечатьТТН";
			ИменаМакетов = "ТН_Горизонтальная";
		ИначеЕсли ОписаниеКоманды.Идентификатор = "ТН_ГоризонтальнаяСПриложением" Тогда
			МенеджерПечати = "Обработка.ПечатьТТН";
			ИменаМакетов = "ТН_ГоризонтальнаяСПриложением";
		КонецЕсли;
		
		УправлениеПечатьюКлиент.ВыполнитьКомандуПечати(МенеджерПечати, ИменаМакетов, ОбъектыПечати, ОписаниеКоманды.Форма, ОписаниеКоманды.ДополнительныеПараметры);
	КонецЕсли;
КонецФункции

Функция ПечатьТранспортнойНакладной(ОписаниеКоманды) Экспорт
	МассивОбъектов = ОписаниеКоманды.ОбъектыПечати;
	ОбъектыПечати = ОписаниеКоманды.ОбъектыПечати;
	ТипДокументов = ТипЗнч(МассивОбъектов[0]);
	
	Если ОписаниеКоманды.Идентификатор = "ТН_Вертикальная" Тогда
		МенеджерПечати = "Обработка.ПечатьТранспортнойНакладной";
		ИменаМакетов = "ТН_Вертикальная";
	ИначеЕсли ОписаниеКоманды.Идентификатор = "ТН_ВертикальнаяСПриложением" Тогда
		МенеджерПечати = "Обработка.ПечатьТранспортнойНакладной";
		ИменаМакетов = "ТН_ВертикальнаяСПриложением";
	ИначеЕсли ОписаниеКоманды.Идентификатор = "ТН_Горизонтальная" Тогда
		МенеджерПечати = "Обработка.ПечатьТранспортнойНакладной";
		ИменаМакетов = "ТН_Горизонтальная";
	ИначеЕсли ОписаниеКоманды.Идентификатор = "ТН_ГоризонтальнаяСПриложением" Тогда
		МенеджерПечати = "Обработка.ПечатьТранспортнойНакладной";
		ИменаМакетов = "ТН_ГоризонтальнаяСПриложением";
	КонецЕсли;
	
	УправлениеПечатьюКлиент.ВыполнитьКомандуПечати(МенеджерПечати, ИменаМакетов, ОбъектыПечати, ОписаниеКоманды.Форма, ОписаниеКоманды.ДополнительныеПараметры);
КонецФункции

Функция ПечатьCMR(ОписаниеКоманды) Экспорт
	МассивОбъектов = ОписаниеКоманды.ОбъектыПечати;
	ОбъектыПечати = ОписаниеКоманды.ОбъектыПечати;
	ТипДокументов = ТипЗнч(МассивОбъектов[0]);
	
	КоличествоРаспоряженийБезТранспортныхНакладных = 0;
	Если ТипДокументов <> Тип("ДокументСсылка.ТранспортнаяНакладная") Тогда
		СтруктураВозврата = УправлениеПечатьюУТВызовСервераЛокализация.ПроверитьНаличиеТранспортныхНакладныхДляРаспоряжений(МассивОбъектов);
		ОбъектыПечати = СтруктураВозврата.МассивОбъектов;
		КоличествоРаспоряженийБезТранспортныхНакладных = СтруктураВозврата.КоличествоРаспоряженийБезТранспортныхНакладных;
	КонецЕсли;
	
	Если КоличествоРаспоряженийБезТранспортныхНакладных <> 0 Тогда
		СписокЗначенийКнопок = Новый СписокЗначений;
		СписокЗначенийКнопок.Добавить(КодВозвратаДиалога.ОК, НСтр("ru = 'Создать и вывести на печать'"));
		СписокЗначенийКнопок.Добавить(КодВозвратаДиалога.Отмена, НСтр("ru = 'Отмена'"));
		
		ДополнительныеПараметры = Новый Структура;
		ДополнительныеПараметры.Вставить("ТипДокументов", ТипДокументов);
		ДополнительныеПараметры.Вставить("МассивОбъектов", МассивОбъектов);
		ДополнительныеПараметры.Вставить("ОписаниеКоманды", ОписаниеКоманды);
		
		ОповещениеОЗавершении = Новый ОписаниеОповещения("НеСозданыТранспортныеНакладныеЗавершение", УправлениеПечатьюУТКлиентЛокализация, ДополнительныеПараметры);
		ПоказатьВопрос(ОповещениеОЗавершении, НСтр("ru = 'Для некоторых распоряжений не созданы транспортные накладные.'"), СписокЗначенийКнопок);
	Иначе
		МенеджерПечати = "Обработка.ПечатьCMR";
		ИменаМакетов = "ПФ_MXL_CMR";
		
		УправлениеПечатьюКлиент.ВыполнитьКомандуПечати(МенеджерПечати, ИменаМакетов, ОбъектыПечати, ОписаниеКоманды.Форма, ОписаниеКоманды.ДополнительныеПараметры);
	КонецЕсли;
КонецФункции

#КонецОбласти

#Область СлужебныеПроцедурыИФункции


#КонецОбласти