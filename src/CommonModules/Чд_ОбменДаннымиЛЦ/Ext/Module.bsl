﻿#Область ПрограммныйИнтерфейс

#Область РекламнаяПродукция

// Выгрузка в телеграммы для ЛЦ данных по рекламной продукции
Процедура ВыгрузкаРекламнойПродукции() Экспорт
	
	ОбщегоНазначения.ПриНачалеВыполненияРегламентногоЗадания(Метаданные.РегламентныеЗадания.Чд_ВыгрузкаРекламнойПродукцииЛЦ);
	
	СтруктураПараметров = СтруктураПараметровОбменаЛЦПоУмолчанию();
	СтруктураПараметров.НазначениеУзла = Перечисления.Чд_НазначенияОбменаЛЦ.РекламнаяПродукция;
	
	Обработки.Чд_ОбменДаннымиЛЦ.ВыгрузкаСообщения(СтруктураПараметров);
	
КонецПроцедуры

// Выгрузка в телеграммы для ЛЦ данных по артикулам
Процедура ВыгрузкаАртикулов() Экспорт
	
	ОбщегоНазначения.ПриНачалеВыполненияРегламентногоЗадания(Метаданные.РегламентныеЗадания.Чд_ВыгрузкаАртикуловЛЦ);
	
	СтруктураПараметров = СтруктураПараметровОбменаЛЦПоУмолчанию();
	СтруктураПараметров.НазначениеУзла = Перечисления.Чд_НазначенияОбменаЛЦ.Артикула;
	
	Обработки.Чд_ОбменДаннымиЛЦ.ВыгрузкаСообщения(СтруктураПараметров);
	
КонецПроцедуры

// Выгрузка в телеграммы для ЛЦ данных по деловым партнерам
Процедура ВыгрузкаПартнеров() Экспорт
	
	ОбщегоНазначения.ПриНачалеВыполненияРегламентногоЗадания(Метаданные.РегламентныеЗадания.Чд_ВыгрузкаПартнеровЛЦ);
	
	СтруктураПараметров = СтруктураПараметровОбменаЛЦПоУмолчанию();
	СтруктураПараметров.НазначениеУзла = Перечисления.Чд_НазначенияОбменаЛЦ.Партнеры;
	
	Обработки.Чд_ОбменДаннымиЛЦ.ВыгрузкаСообщения(СтруктураПараметров);
	
КонецПроцедуры

// Загрузка данных из телеграмм от ЛЦ
Процедура ЗагрузкаРекламнойПродукции() Экспорт
	
	ОбщегоНазначения.ПриНачалеВыполненияРегламентногоЗадания(Метаданные.РегламентныеЗадания.Чд_ЗагрузкаРекламнойПродукции);
	
	СтруктураПараметров = СтруктураПараметровОбменаЛЦПоУмолчанию();
	Обработки.Чд_ОбменДаннымиЛЦ.ЗагрузкаСообщения(СтруктураПараметров);
	
КонецПроцедуры

#КонецОбласти 

#Область ГотоваяПродукция

// ++EE:YDS 01.07.2022 MLVC-658
Процедура ЗагрузкаФайлаt302qtyРегламент() Экспорт
	
	ОбщегоНазначения.ПриНачалеВыполненияРегламентногоЗадания(Метаданные.РегламентныеЗадания.ЭЭ_ЗагрузкаФайлаt302qty);
	Обработки.Чд_ОбменДаннымиЛЦ.ЗагрузкаФайлаt302qty();
	
КонецПроцедуры
// --EE:YDS 01.07.2022 MLVC-658

// ++EE:TEN 13.12.2022 MLVC-948
Процедура ЗагрузкаФайлаSEMAFORРегламент() Экспорт
	
	ОбщегоНазначения.ПриНачалеВыполненияРегламентногоЗадания(Метаданные.РегламентныеЗадания.ЭЭ_ВыгрузкаФайловDBFДляЛЦ);
	Обработки.Чд_ОбменДаннымиЛЦ.ЗагрузкаФайлаSEMAFOR();
	
КонецПроцедуры
// --EE:TEN 13.12.2022 MLVC-948

// ++EE:TEN 16.01.2023 MLVC-925
Процедура ПереводВНестандартнуюПродукциюНаЛЦРегламент() Экспорт
	
	ОбщегоНазначения.ПриНачалеВыполненияРегламентногоЗадания(Метаданные.РегламентныеЗадания.ЭЭ_ПереводВНестандартнуюПродукциюНаЛЦ);
	// ++EE:TEN 10.07.2023 MLVC-1238, MLVC-1306 
	Обработки.ЭЭ_АРМПоступлениеЛЦ.СозданиеДокументовУценка();
	// --EE:TEN 10.07.2023 MLVC-1238, MLVC-1306 
		
КонецПроцедуры
// --EE:TEN 16.01.2023 MLVC-925

// ++EE:TEN 13.12.2022 MLVC-928
Процедура ЗагрузкаФайлаZarplataРегламент() Экспорт
	
	ОбщегоНазначения.ПриНачалеВыполненияРегламентногоЗадания(Метаданные.РегламентныеЗадания.ЭЭ_ЗагрузкаФайлаZarplata);
	Обработки.Чд_ОбменДаннымиЛЦ.ЗагрузкаДанныхZarplata();
	
КонецПроцедуры
// --EE:TEN 13.12.2022 MLVC-928

// ++EE:TEN 18.10.2023 MLVC-1306
Процедура ЗагрузкаФайлаPrNakl_VOZVRATРегламент() Экспорт
	
	ОбщегоНазначения.ПриНачалеВыполненияРегламентногоЗадания(Метаданные.РегламентныеЗадания.ЭЭ_ЗагрузкаФайлаFX_PrNakl_VOZVRAT);
	Обработки.Чд_ОбменДаннымиЛЦ.ЗагрузкаДанныхFX_PrNakl_VOZVRAT();
	
КонецПроцедуры
// --EE:TEN 18.10.2023 MLVC-1306

#КонецОбласти 

// Структура параметоров для запуска обмена с ЛЦ
//
// Возвращаемое значение:
//  СтруктураПараметров - <Структура> - структура параметров
Функция СтруктураПараметровОбменаЛЦПоУмолчанию() Экспорт
	
	СтруктураПараметров = Новый Структура;
	СтруктураНастроек = Обработки.Чд_ОбменДаннымиЛЦ.СтруктураНастроекОбменаЛЦ();
	СтруктураПараметров.Вставить("КаталогиОбменаЛЦ", СтруктураНастроек);
	СтруктураПараметров.Вставить("ЭтоФоновоеЗадание", Истина);
	СтруктураПараметров.Вставить("НазначениеУзла", Перечисления.Чд_НазначенияОбменаЛЦ.ПустаяСсылка());
	
	Возврат СтруктураПараметров;
	
КонецФункции

// Параметры:
//  СсылкаНаДокумент - ДокументСсылка - ссылка документа для проверки
// Возвращаемое значение:
//  РезультатПроверки - <Булево> - если документ уже выгружался
Функция ВыгрузкаЛЦВыполнена(СсылкаНаДокумент) Экспорт
	
	Запрос = Новый Запрос;
	Запрос.Текст =
		"ВЫБРАТЬ
		|	Чд_СостоянияДанныхОбменаЛЦ.ОбъектОбмена КАК ОбъектОбмена
		|ИЗ
		|	РегистрСведений.Чд_СостоянияДанныхОбменаЛЦ КАК Чд_СостоянияДанныхОбменаЛЦ
		|ГДЕ
		|	Чд_СостоянияДанныхОбменаЛЦ.ОбъектОбмена = &СсылкаДокумент";
	
	Запрос.УстановитьПараметр("СсылкаДокумент", СсылкаНаДокумент);
	
	РезультатЗапроса = Запрос.Выполнить();
	Если Не РезультатЗапроса.Пустой() Тогда
		Возврат Истина;
	КонецЕсли;
	
	Возврат Ложь;
	
КонецФункции

#Область ПодпискиНаСобытия

// 4D:Милавица, Анастасия, 29.11.2019
// ДС №34.1 Разработка обменов с WMS ЛЦ по рекламной продукции, №23862
// {
Процедура Чд_ОбменДаннымиЛЦПередЗаписьюДокументовРекламнаяПродукция(Источник, Отказ, РежимЗаписи, РежимПроведения) Экспорт
	
	Если РежимЗаписи = РежимЗаписиДокумента.Запись Тогда
		Возврат;
	КонецЕсли;
	
	Если ТипЗнч(Источник) = Тип("ДокументОбъект.ЗаказНаПеремещение") Тогда
		СкладТранзит = Источник.СкладПолучатель;
		СкладОтвХранения = Константы.ЭЭ_СкладЛЦ.Получить();
		Если СкладОтвХранения <> Источник.СкладОтправитель
			ИЛИ Источник.ДополнительныеСвойства.Свойство("ИзмененияПоДаннымТелеграммыЛЦ") Тогда
			
			Возврат;
		КонецЕсли;
	ИначеЕсли ТипЗнч(Источник) = Тип("ДокументОбъект.ПриобретениеТоваровУслуг") Тогда
		СкладТранзит = Источник.Склад;
	КонецЕсли;
	
	УзелОбмена = Обработки.Чд_ОбменДаннымиЛЦ.УзелОбменаСЛогистическийЦентром(Перечисления.Чд_НазначенияОбменаЛЦ.РекламнаяПродукция);
	Если УзелОбмена = ПланыОбмена.Чд_ОбменСЛогистическийЦентром.ПустаяСсылка()
		ИЛИ СкладТранзит <> УзелОбмена.СкладЛЦ Тогда
		
		Возврат;
	КонецЕсли;
	
	Если Не Обработки.Чд_ОбменДаннымиЛЦ.ЕстьРекламнаяПродукция(Источник.Товары.ВыгрузитьКолонку("Номенклатура")) Тогда
		Возврат;
	КонецЕсли;
	
	Если ТипЗнч(Источник) = Тип("ДокументОбъект.ПриобретениеТоваровУслуг") Тогда
		ПроверитьПовторнуюВыгрузкуЛЦДокументовПередЗаписью(Источник, Отказ, РежимЗаписи);
		Возврат;
	КонецЕсли;
	
	Если РежимЗаписи = РежимЗаписиДокумента.ОтменаПроведения Тогда
		// если документ не выгружался на ЛЦ, то снимаем регистрацию
		ПроверитьПовторнуюВыгрузкуЛЦДокументовПередЗаписью(Источник, Отказ, РежимЗаписи);
		Если Отказ Тогда
			Возврат;
		КонецЕсли;
		
		ПланыОбмена.УдалитьРегистрациюИзменений(УзелОбмена, Источник);
		Возврат;
	КонецЕсли;
	
	ВсеСтрокиОтгружаются = Истина;
	Если ТипЗнч(Источник) = Тип("ДокументОбъект.ЗаказНаПеремещение") Тогда
		Для Каждого Стр Из Источник.Товары Цикл
			Если НЕ Стр.Отменено
				И Стр.ВариантОбеспечения <> Перечисления.ВариантыОбеспечения.Отгрузить
				И Стр.ВариантОбеспечения <> Перечисления.ВариантыОбеспечения.ОтгрузитьОбособленно Тогда
				
				ВсеСтрокиОтгружаются = Ложь;
			КонецЕсли;
		КонецЦикла;
	КонецЕсли;
	
	ТребуетсяВыгрузка = Ложь;
	ПроверитьПовторнуюВыгрузкуЛЦДокументовПередЗаписью(Источник, Отказ, РежимЗаписи, ТребуетсяВыгрузка);
	Если Отказ ИЛИ НЕ ВсеСтрокиОтгружаются Тогда
		Возврат;
	КонецЕсли;
	
	Если ТребуетсяВыгрузка Тогда
		ПланыОбмена.ЗарегистрироватьИзменения(УзелОбмена, Источник);
	КонецЕсли;
	
КонецПроцедуры

Процедура Чд_ОбменДаннымиЛЦПередЗаписьюСправочников(Источник, Отказ) Экспорт
	
	ОбъектКРегистрации = Неопределено;
	Если ТипЗнч(Источник) = Тип("СправочникОбъект.Партнеры") Тогда
		Если Источник.ПометкаУдаления Тогда
			ОбъектКРегистрации = Источник;
		Иначе
			КлючевыеРеквизиты = "ПометкаУдаления,Наименование,НаименованиеПолное,Родитель,Клиент,Поставщик,Перевозчик";
			ИзменилисьРеквизиты = ЭлектронноеВзаимодействиеБЗК.ИзменилисьРеквизитыОбъекта(Источник, КлючевыеРеквизиты);
			Если Не ИзменилисьРеквизиты Тогда
				ИзменилисьДопРеквизиты = ЭлектронноеВзаимодействиеБЗК.ИзмениласьТабличнаяЧастьОбъекта(Источник, "ДополнительныеРеквизиты");
				Если Не ИзменилисьДопРеквизиты Тогда
					ИзменилисьДопРеквизиты = ЭлектронноеВзаимодействиеБЗК.ИзмениласьТабличнаяЧастьОбъекта(Источник, "КонтактнаяИнформация");
				КонецЕсли;
			КонецЕсли;
			
			Если ИзменилисьРеквизиты ИЛИ ИзменилисьДопРеквизиты Тогда
				ОбъектКРегистрации = Источник;
			КонецЕсли;
		КонецЕсли;
	ИначеЕсли ТипЗнч(Источник) = Тип("СправочникОбъект.Контрагенты") Тогда
		КлючевыеРеквизиты = "СтранаРегистрации";
		ИзменилисьРеквизиты = ЭлектронноеВзаимодействиеБЗК.ИзменилисьРеквизитыОбъекта(Источник, КлючевыеРеквизиты);
		Если ИзменилисьРеквизиты
			ИЛИ Источник.Ссылка.Партнер <> Источник.Партнер Тогда
			
			УзелОбмена = Обработки.Чд_ОбменДаннымиЛЦ.УзелОбменаСЛогистическийЦентром(Перечисления.Чд_НазначенияОбменаЛЦ.Партнеры);
			// регистрируем всех подчиненных партнеров
			Запрос = Новый Запрос;
			Запрос.Текст =
				"ВЫБРАТЬ
				|	Партнеры.Ссылка КАК Ссылка
				|ИЗ
				|	Справочник.Партнеры КАК Партнеры
				|ГДЕ
				|	Партнеры.Родитель В (&МассивПартнеров)
				|	И НЕ Партнеры.ПометкаУдаления";
			
			МассивПартнеров = Новый Массив;
			МассивПартнеров.Добавить(Источник.Партнер);
			Если Источник.Ссылка.Партнер <> Источник.Партнер Тогда
				МассивПартнеров.Добавить(Источник.Ссылка.Партнер);
			КонецЕсли;
			Запрос.УстановитьПараметр("МассивПартнеров", Источник.Партнер);
			
			Выборка = Запрос.Выполнить().Выбрать();
			Пока Выборка.Следующий() Цикл
				ПартнерОбъект = Выборка.Ссылка.ПолучитьОбъект();
				ПланыОбмена.ЗарегистрироватьИзменения(УзелОбмена, ПартнерОбъект);
			КонецЦикла;
			
			Возврат;
		КонецЕсли;
	ИначеЕсли ТипЗнч(Источник) = Тип("СправочникОбъект.Номенклатура") Тогда
		КлючевыеРеквизиты = "ПометкаУдаления,Наименование,Артикул,ЕдиницаИзмерения,Марка,ВидНоменклатуры,
			|Чд_КлассификацияНоменклатуры,ЭЭ_КоллекцияМилавицы,ЭЭ_ПризнакУпаковкиПриОтгрузке";
		ИзменилисьРеквизиты = ЭлектронноеВзаимодействиеБЗК.ИзменилисьРеквизитыОбъекта(Источник, КлючевыеРеквизиты);
		// ++EE:KMV 14.03.2023 SDEE-1907
		КлассификацияНоменклатуры = Истина;
		ПроверитьКлассификациюНоменклатуры(Источник.Чд_КлассификацияНоменклатуры, КлассификацияНоменклатуры);
		// --EE:KMV 14.03.2023 SDEE-1907
		Если НЕ ИзменилисьРеквизиты Тогда
			ИзменилисьДопРеквизиты = ЭлектронноеВзаимодействиеБЗК.ИзмениласьТабличнаяЧастьОбъекта(Источник, "ДополнительныеРеквизиты");
		КонецЕсли;
		
		// ++EE:KMV 14.03.2023 SDEE-1907
		Если КлассификацияНоменклатуры Тогда
		// --EE:KMV 14.03.2023 SDEE-1907
			Если ИзменилисьРеквизиты ИЛИ ИзменилисьДопРеквизиты Тогда
				УзелОбмена = Обработки.Чд_ОбменДаннымиЛЦ.УзелОбменаСЛогистическийЦентром(Перечисления.Чд_НазначенияОбменаЛЦ.Артикула);
				Запрос = Новый Запрос;
				Запрос.Текст =
				"ВЫБРАТЬ
				|	ШтрихкодыНоменклатуры.Штрихкод КАК Штрихкод
				|ИЗ
				|	РегистрСведений.ШтрихкодыНоменклатуры КАК ШтрихкодыНоменклатуры
				|ГДЕ
				|	ШтрихкодыНоменклатуры.Номенклатура = &Номенклатура";
				
				Запрос.УстановитьПараметр("Номенклатура", Источник.Ссылка);
				Выборка = Запрос.Выполнить().Выбрать();
				Пока Выборка.Следующий() Цикл
					НаборЗаписей = РегистрыСведений.ШтрихкодыНоменклатуры.СоздатьНаборЗаписей();
					НаборЗаписей.Отбор.Штрихкод.Установить(Выборка.Штрихкод);
					НаборЗаписей.Прочитать();
					Если Источник.ПометкаУдаления Тогда
						ПланыОбмена.УдалитьРегистрациюИзменений(УзелОбмена, НаборЗаписей);
					Иначе
						// регистрируем все ШК по измененной номенклатуре
						ПланыОбмена.ЗарегистрироватьИзменения(УзелОбмена, НаборЗаписей);
					КонецЕсли;
				КонецЦикла;
			КонецЕсли;
		// ++EE:KMV 14.03.2023 SDEE-1907	
		КонецЕсли;
		// --EE:KMV 14.03.2023 SDEE-1907
		
		Возврат;
	ИначеЕсли ТипЗнч(Источник) = Тип("СправочникОбъект.НоменклатураПоставщиков") Тогда
		КлючевыеРеквизиты = "ПометкаУдаления,Владелец,Артикул,Номенклатура,Чд_КодFoxPro,Характеристика";
		ИзменилисьРеквизиты = ЭлектронноеВзаимодействиеБЗК.ИзменилисьРеквизитыОбъекта(Источник, КлючевыеРеквизиты);
		// ++EE:KMV 14.03.2023 SDEE-1907
		КлассификацияНоменклатуры = Истина;	
		ЗначениеКлассификацииНоменклатуры =	ОбщегоНазначения.ЗначенияРеквизитовОбъекта(Источник.Номенклатура, "Чд_КлассификацияНоменклатуры");
		ПроверитьКлассификациюНоменклатуры(ЗначениеКлассификацииНоменклатуры.Чд_КлассификацияНоменклатуры, КлассификацияНоменклатуры);
		// --EE:KMV 14.03.2023 SDEE-1907
		Если ИзменилисьРеквизиты
			// ++EE:KMV 14.03.2023 SDEE-1907
			И КлассификацияНоменклатуры
			// --EE:KMV 14.03.2023 SDEE-1907
			И (Источник.Владелец = Справочники.Партнеры.НашеПредприятие
				ИЛИ Источник.Ссылка.Владелец = Справочники.Партнеры.НашеПредприятие) Тогда
			
			УзелОбмена = Обработки.Чд_ОбменДаннымиЛЦ.УзелОбменаСЛогистическийЦентром(Перечисления.Чд_НазначенияОбменаЛЦ.Артикула);
			Запрос = Новый Запрос;
			Запрос.Текст =
				"ВЫБРАТЬ
				|	ШтрихкодыНоменклатуры.Штрихкод КАК Штрихкод
				|ИЗ
				|	РегистрСведений.ШтрихкодыНоменклатуры КАК ШтрихкодыНоменклатуры
				|ГДЕ
				|	ШтрихкодыНоменклатуры.Номенклатура = &Номенклатура
				|	И ШтрихкодыНоменклатуры.Характеристика = &Характеристика";
			
			Запрос.УстановитьПараметр("Номенклатура", Источник.Номенклатура);
			Запрос.УстановитьПараметр("Характеристика", Источник.Характеристика);
			Выборка = Запрос.Выполнить().Выбрать();
			Пока Выборка.Следующий() Цикл
				НаборЗаписей = РегистрыСведений.ШтрихкодыНоменклатуры.СоздатьНаборЗаписей();
				НаборЗаписей.Отбор.Штрихкод.Установить(Выборка.Штрихкод);
				НаборЗаписей.Прочитать();
				НаборЗаписей.Записать();
			КонецЦикла;
		КонецЕсли;
		
		Возврат;
	КонецЕсли;
	
	УзелОбмена = Обработки.Чд_ОбменДаннымиЛЦ.УзелОбменаСЛогистическийЦентром(Перечисления.Чд_НазначенияОбменаЛЦ.Партнеры);
	Если ОбъектКРегистрации <> Неопределено
		И ЗначениеЗаполнено(УзелОбмена) Тогда
		
		Если ОбъектКРегистрации.ПометкаУдаления Тогда
			ПланыОбмена.УдалитьРегистрациюИзменений(УзелОбмена, ОбъектКРегистрации);
		Иначе
			ПланыОбмена.ЗарегистрироватьИзменения(УзелОбмена, ОбъектКРегистрации);
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры

Процедура Чд_ОбменДаннымиЛЦПередЗаписьюШтрихкодовНоменклатуры(Источник, Отказ, Замещение) Экспорт
	
	УзелОбмена = Обработки.Чд_ОбменДаннымиЛЦ.УзелОбменаСЛогистическийЦентром(Перечисления.Чд_НазначенияОбменаЛЦ.Артикула);
	
	Если УзелОбмена <> ПланыОбмена.Чд_ОбменСЛогистическийЦентром.ПустаяСсылка() Тогда
		Если Источник.Количество()
			И ЗначениеЗаполнено(Источник[0].Номенклатура) Тогда
			
			КлассификацияНоменклатуры = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Источник[0].Номенклатура, "Чд_КлассификацияНоменклатуры");
			Если КлассификацияНоменклатуры = Перечисления.Чд_КлассификацияНоменклатуры.ГотоваяПродукция
				ИЛИ КлассификацияНоменклатуры = Перечисления.Чд_КлассификацияНоменклатуры.ПокупныеТовары
				ИЛИ КлассификацияНоменклатуры = Перечисления.Чд_КлассификацияНоменклатуры.РекламнаяПродукция
				ИЛИ КлассификацияНоменклатуры = Перечисления.Чд_КлассификацияНоменклатуры.НестандартнаяПродукция Тогда
				
				ПланыОбмена.ЗарегистрироватьИзменения(УзелОбмена, Источник);
			КонецЕсли;
		Иначе
			ПланыОбмена.УдалитьРегистрациюИзменений(УзелОбмена, Источник);
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры
// }
// 4D Анастасия

// ++EE:BAN 29.12.2021 MLVC-4
Процедура ЭЭ_ПроверитьБлокировкуТовараЛЦПередЗаписью(Источник, Отказ, РежимЗаписи, РежимПроведения) Экспорт
	
	Если Отказ ИЛИ РежимЗаписи <> РежимЗаписиДокумента.Проведение Тогда
		Возврат;
	КонецЕсли;
	// ++EE:TEN 12.01.2023 MLVC-977
	Если ТипЗнч(Источник) = Тип("ДокументОбъект.ПеремещениеТоваров") И Источник.ЭЭ_СоздаетсяПоПроверкаКачества Тогда
		Возврат;
	КонецЕсли;
	// --EE:TEN 12.01.2023 MLVC-977
	
	СкладЛЦ = Константы.ЭЭ_СкладЛЦ.Получить();
	Если ТипЗнч(Источник) = Тип("ДокументОбъект.ЗаказНаПеремещение")
		ИЛИ ТипЗнч(Источник) = Тип("ДокументОбъект.ПеремещениеТоваров") Тогда
		
		СкладОтправитель = Источник.СкладОтправитель;
	КонецЕсли;
	
	Если СкладОтправитель <> СкладЛЦ Тогда
		Возврат;
	КонецЕсли;
	
	Если ТипЗнч(Источник) = Тип("ДокументОбъект.ПеремещениеТоваров")
		И (НЕ ЗначениеЗаполнено(Источник.ЗаказНаПеремещение)
			ИЛИ НЕ Источник.ЭЭ_СоздаетсяПоТелеграммеЛЦ) Тогда
		
		ТекстСообщения = НСтр("ru='Создание документа реализовано программно на основании полученной от WMS телеграммы (при перемещении со склада %1)!'");
		ТекстСообщения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ТекстСообщения, СкладОтправитель);
// ++EE:TEN 17.04.2023 MLVC-1106
// закомментировано Временно для возможности создания документов пока нет работы по телеграммам от WMS 
//		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, , , , Отказ);
		Возврат;
	ИначеЕсли ТипЗнч(Источник) = Тип("ДокументОбъект.ПеремещениеТоваров") Тогда
		Возврат;
	КонецЕсли;
	
	ТЗТовары = Источник.Товары.Выгрузить();
	ПроверитьОстаткиСУчетомБлокировки(ТЗТовары, СкладЛЦ, Отказ);
	
КонецПроцедуры
// --EE:BAN 29.12.2021 MLVC-4

// ++EE:TEN 21.02.2022 MLVC-45
// Выгрузка файлов DBF для готовой продукции при перемещении на ЛЦ
Процедура ЭЭ_ОбменДаннымиЛЦПередЗаписьюДокументаПеремещениеПередЗаписью(Источник, Отказ, РежимЗаписи, РежимПроведения) Экспорт
	
	// ++EE:TEN 23.03.2022 MLVC-257
	// требование выгружать документы по команде. В перспективе на удаление Подписки на события "ЭЭ_ОбменДаннымиЛЦПередЗаписьюДокументаПеремещение"
	// и Регламентное задание ЭЭ_ВыгрузкаФайловDBFДляЛЦ
	// добавлена в Обработка.Чд_ОбменДаннымиЛЦ
	Возврат;
	
	СкладПолучатель = ПредопределенноеЗначение("Справочник.ЭЭ_ПредопределенныеЗначения.СкладУчастокЛЦ");
	
	Если ТипЗнч(Источник.Ссылка) = Тип("ДокументСсылка.ПеремещениеТоваров") Тогда
		
		Запрос = Новый Запрос;
		Запрос.Текст =
			"ВЫБРАТЬ
			|	ПеремещениеТоваров.Ссылка КАК Ссылка,
			|	Чд_СостоянияДанныхОбменаЛЦ.ИмяФайла КАК ИмяФайла
			|ПОМЕСТИТЬ ВТ_ДокументПеремещение
			|ИЗ
			|	Документ.ПеремещениеТоваров КАК ПеремещениеТоваров
			|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.Чд_СостоянияДанныхОбменаЛЦ КАК Чд_СостоянияДанныхОбменаЛЦ
			|		ПО (Чд_СостоянияДанныхОбменаЛЦ.ОбъектОбмена = ПеремещениеТоваров.Ссылка)
			|ГДЕ
			|	ПеремещениеТоваров.СкладПолучатель = &СкладПолучатель
			|	И ПеремещениеТоваров.Ссылка = &Ссылка
			|;
			|
			|////////////////////////////////////////////////////////////////////////////////
			|ВЫБРАТЬ
			|	ВТ_ДокументПеремещение.Ссылка КАК Ссылка,
			|	ВТ_ДокументПеремещение.ИмяФайла КАК ИмяФайла
			|ИЗ
			|	ВТ_ДокументПеремещение КАК ВТ_ДокументПеремещение
			|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Документ.ТранспортнаяНакладная.ДокументыОснования КАК ТранспортнаяНакладнаяДокументыОснования
			|		ПО ВТ_ДокументПеремещение.Ссылка = ТранспортнаяНакладнаяДокументыОснования.ДокументОснование
			|ГДЕ
			|	ТранспортнаяНакладнаяДокументыОснования.Ссылка.Проведен";
		
		Запрос.УстановитьПараметр("СкладПолучатель", СкладПолучатель);
		Запрос.УстановитьПараметр("Ссылка", Источник.Ссылка);
		
		РезультатЗапроса = Запрос.Выполнить();
		
		Выборка = РезультатЗапроса.Выбрать();
		Если Не Отказ Тогда
			Если Выборка.Следующий() И НЕ ЗначениеЗаполнено(Выборка.ИмяФайла) Тогда
				УзелОбмена = Обработки.Чд_ОбменДаннымиЛЦ.УзелОбменаСЛогистическийЦентром(Перечисления.Чд_НазначенияОбменаЛЦ.ГотоваяПродукция);
				Если РежимЗаписи = РежимЗаписиДокумента.ОтменаПроведения Тогда
					ПланыОбмена.УдалитьРегистрациюИзменений(УзелОбмена, Источник);
				ИначеЕсли РежимЗаписи = РежимЗаписиДокумента.Проведение Тогда
					ПланыОбмена.ЗарегистрироватьИзменения(УзелОбмена, Источник);
				КонецЕсли;
			Иначе
				Возврат;
			КонецЕсли;
		КонецЕсли;
		
	КонецЕсли;
	// --EE:TEN 18.03.2022 MLVC-45, MLVC-257
	Если ТипЗнч(Источник.Ссылка) = Тип("ДокументСсылка.ТранспортнаяНакладная") Тогда
		
		Запрос = Новый Запрос;
		Запрос.Текст =
			"ВЫБРАТЬ
			|	ТранспортнаяНакладнаяДокументыОснования.Ссылка КАК Ссылка,
			|	ТранспортнаяНакладнаяДокументыОснования.ДокументОснование КАК ДокументОснование,
			|	Чд_СостоянияДанныхОбменаЛЦ.ИмяФайла КАК ИмяФайла
			|ИЗ
			|	Документ.ТранспортнаяНакладная.ДокументыОснования КАК ТранспортнаяНакладнаяДокументыОснования
			|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.Чд_СостоянияДанныхОбменаЛЦ КАК Чд_СостоянияДанныхОбменаЛЦ
			|		ПО ТранспортнаяНакладнаяДокументыОснования.ДокументОснование = Чд_СостоянияДанныхОбменаЛЦ.ОбъектОбмена
			|ГДЕ
			|	ТранспортнаяНакладнаяДокументыОснования.ДокументОснование.Проведен
			|	И ТранспортнаяНакладнаяДокументыОснования.Ссылка = &Ссылка
			|	И ТранспортнаяНакладнаяДокументыОснования.ДокументОснование.СкладПолучатель = &СкладПолучатель";
		
		Запрос.УстановитьПараметр("Ссылка", Источник.Ссылка);
		Запрос.УстановитьПараметр("СкладПолучатель", СкладПолучатель);
		
		РезультатЗапроса = Запрос.Выполнить();
		
		Выборка = РезультатЗапроса.Выбрать();
		Пока Выборка.Следующий() И НЕ ЗначениеЗаполнено(Выборка.ИмяФайла) Цикл
			УзелОбмена = Обработки.Чд_ОбменДаннымиЛЦ.УзелОбменаСЛогистическийЦентром(Перечисления.Чд_НазначенияОбменаЛЦ.ГотоваяПродукция);
			ПеремещениеОбъект = Выборка.ДокументОснование.ПолучитьОбъект();
			Если РежимЗаписи = РежимЗаписиДокумента.ОтменаПроведения Тогда
				ПланыОбмена.УдалитьРегистрациюИзменений(УзелОбмена, ПеремещениеОбъект);
			ИначеЕсли РежимЗаписи = РежимЗаписиДокумента.Проведение Тогда
				ПланыОбмена.ЗарегистрироватьИзменения(УзелОбмена, ПеремещениеОбъект);
			КонецЕсли;
		КонецЦикла;
		
	КонецЕсли;
	// --EE:TEN 18.03.2022 MLVC-45, MLVC-257
	
КонецПроцедуры
// --EE:TEN 21.02.2022 MLVC-45

#КонецОбласти

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

// ++EE:BAN 15.12.2021 MLVC-4
Процедура ПроверитьПовторнуюВыгрузкуЛЦДокументовПередЗаписью(Источник, Отказ, РежимЗаписи, ТребуетсяВыгрузка = Ложь)
	
	Если Источник.ЭтоНовый()
		ИЛИ НЕ Источник.Проведен Тогда
		
		ТребуетсяВыгрузка = Истина;
		Возврат;
	КонецЕсли;
	
	Если ВыгрузкаЛЦВыполнена(Источник.Ссылка) Тогда
		ТекстСообщения = НСтр("ru='Документ выгружен в WMS. Изменения не доступны!'");
		Если РежимЗаписи = РежимЗаписиДокумента.ОтменаПроведения Тогда
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, , , , Отказ);
			Возврат;
		КонецЕсли;
		
		РеквизитыОсновныеДляПроверки = "Дата, Номер, Организация, ПометкаУдаления";
		РеквизитыТЧДляПроверки = "Номенклатура, Характеристика, Серия, Количество, Упаковка";
		ИмяТЧ = "Товары";
		
		Если ТипЗнч(Источник) = Тип("ДокументОбъект.ПриобретениеТоваровУслуг") Тогда
			РеквизитыОсновныеДляПроверки = РеквизитыОсновныеДляПроверки + ", Чд_Статус, Склад, Партнер, СерияВходящегоДокумента,
																				|ДатаВходящегоДокумента, НомерВходящегоДокумента";
			РеквизитыТЧДляПроверки = РеквизитыТЧДляПроверки + ", НомерСтроки, Цена";
		ИначеЕсли ТипЗнч(Источник) = Тип("ДокументОбъект.ЗаказНаПеремещение") Тогда
			РеквизитыОсновныеДляПроверки = РеквизитыОсновныеДляПроверки + ", ДокументОснование, ЖелаемаяДатаПоступления";
			РеквизитыТЧДляПроверки = РеквизитыТЧДляПроверки + ", КодСтроки, НачалоОтгрузки, ВариантОбеспечения";
		КонецЕсли;
		
		Если ЭлектронноеВзаимодействиеБЗК.ИзменилисьРеквизитыОбъекта(Источник, РеквизитыОсновныеДляПроверки)
			ИЛИ ЭлектронноеВзаимодействиеБЗК.ИзмениласьТабличнаяЧастьОбъекта(Источник, ИмяТЧ, РеквизитыТЧДляПроверки) Тогда
			
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, , , , Отказ);
		КонецЕсли;
		
		Если Отказ Тогда
			Возврат;
		КонецЕсли;
		
		Если ТипЗнч(Источник) = Тип("ДокументОбъект.ЗаказНаПеремещение") Тогда
			// отметка "Отменена" не может быть снята, если заказ на перемещение уже отправлен на ЛЦ
			ТоварыДоЗаписи = Источник.Ссылка.Товары;
			ТоварыИзмененные = Источник.Товары;
			Для каждого СтрТовары Из ТоварыДоЗаписи Цикл
				СтрокаДляЗаписи = ТоварыИзмененные[СтрТовары.НомерСтроки - 1];
				Если СтрТовары.Отменено
					И НЕ СтрокаДляЗаписи.Отменено Тогда
					
					ТекстСообщения = НСтр("ru='Документ выгружен в WMS. Снятие отметки ""Отменено"" не доступно для строк.'");
					ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, , , , Отказ);
					Возврат;
				ИначеЕсли (НЕ СтрТовары.Отменено
						И СтрокаДляЗаписи.Отменено) Тогда
					
					ТребуетсяВыгрузка = Истина;
				КонецЕсли;
			КонецЦикла;
		КонецЕсли;
	Иначе
		ТребуетсяВыгрузка = Истина;
	КонецЕсли;
	
КонецПроцедуры
// --EE:BAN 15.12.2021 MLVC-4

// ++EE:BAN 29.12.2021 MLVC-4
Процедура ПроверитьОстаткиСУчетомБлокировки(ТЗТовары, СкладЛЦ, Отказ)
	
	Запрос = Новый Запрос;
	Запрос.Текст =
		"ВЫБРАТЬ
		|	ВЫРАЗИТЬ(ТоварыДокумента.Номенклатура КАК Справочник.Номенклатура) КАК Номенклатура,
		|	ВЫРАЗИТЬ(ТоварыДокумента.Характеристика КАК Справочник.ХарактеристикиНоменклатуры) КАК Характеристика,
		|	ВЫРАЗИТЬ(ТоварыДокумента.Серия КАК Справочник.СерииНоменклатуры) КАК Серия,
		|	ТоварыДокумента.Количество КАК Количество
		|ПОМЕСТИТЬ ВТ_Товары
		|ИЗ
		|	&ТоварыДокумента КАК ТоварыДокумента
		|ГДЕ
		|	ТоварыДокумента.ВариантОбеспечения В(&ВариантОбеспеченияОтгрузить)
		|	И ТоварыДокумента.Отменено = ЛОЖЬ
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	ВТ_Товары.Номенклатура КАК Номенклатура,
		|	ВТ_Товары.Характеристика КАК Характеристика,
		|	&СкладЛЦ КАК Склад,
		|	ВТ_Товары.Серия КАК Серия,
		|	СУММА(ВТ_Товары.Количество) КАК Количество
		|ПОМЕСТИТЬ ВТ_ТоварыСгруппированные
		|ИЗ
		|	ВТ_Товары КАК ВТ_Товары
		|
		|СГРУППИРОВАТЬ ПО
		|	ВТ_Товары.Номенклатура,
		|	ВТ_Товары.Характеристика,
		|	ВТ_Товары.Серия
		|
		|ИНДЕКСИРОВАТЬ ПО
		|	Номенклатура,
		|	Характеристика,
		|	Склад,
		|	Серия
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	ТоварыНаСкладахОстатки.Номенклатура КАК Номенклатура,
		|	ТоварыНаСкладахОстатки.Характеристика КАК Характеристика,
		|	ТоварыНаСкладахОстатки.Серия КАК Серия,
		|	ТоварыНаСкладахОстатки.ВНаличииОстаток КАК ВНаличииОстаток
		|ПОМЕСТИТЬ ВТ_ОстаткиНаСкладе
		|ИЗ
		|	РегистрНакопления.ТоварыНаСкладах.Остатки(
		|			,
		|			(Номенклатура, Характеристика, Склад, Серия) В
		|				(ВЫБРАТЬ
		|					ВТ_ТоварыСгруппированные.Номенклатура КАК Номенклатура,
		|					ВТ_ТоварыСгруппированные.Характеристика КАК Характеристика,
		|					ВТ_ТоварыСгруппированные.Склад КАК Склад,
		|					ВТ_ТоварыСгруппированные.Серия КАК Серия
		|				ИЗ
		|					ВТ_ТоварыСгруппированные КАК ВТ_ТоварыСгруппированные)) КАК ТоварыНаСкладахОстатки
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	ЭЭ_БлокировкаТоваровЛЦОстатки.Номенклатура КАК Номенклатура,
		|	ЭЭ_БлокировкаТоваровЛЦОстатки.Характеристика КАК Характеристика,
		|	ЭЭ_БлокировкаТоваровЛЦОстатки.Серия КАК Серия,
		|	ЭЭ_БлокировкаТоваровЛЦОстатки.КоличествоОстаток КАК КоличествоОстаток
		|ПОМЕСТИТЬ ВТ_БлокированныеТовары
		|ИЗ
		|	РегистрНакопления.ЭЭ_БлокировкаТоваровЛЦ.Остатки(
		|			,
		|			(Номенклатура, Характеристика, Серия, Склад) В
		|				(ВЫБРАТЬ
		|					ВТ_ТоварыСгруппированные.Номенклатура КАК Номенклатура,
		|					ВТ_ТоварыСгруппированные.Характеристика КАК Характеристика,
		|					ВТ_ТоварыСгруппированные.Серия КАК Серия,
		|					ВТ_ТоварыСгруппированные.Склад КАК Склад
		|				ИЗ
		|					ВТ_ТоварыСгруппированные КАК ВТ_ТоварыСгруппированные)) КАК ЭЭ_БлокировкаТоваровЛЦОстатки
		|ГДЕ
		|	ЭЭ_БлокировкаТоваровЛЦОстатки.КоличествоОстаток > 0
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	ВТ_ТоварыСгруппированные.Количество + ЕСТЬNULL(ВТ_БлокированныеТовары.КоличествоОстаток, 0) - ВТ_ОстаткиНаСкладе.ВНаличииОстаток КАК НедостаточноТовара,
		|	ВТ_ТоварыСгруппированные.Номенклатура.Представление КАК НоменклатураПредставление,
		|	ВТ_ТоварыСгруппированные.Характеристика.Представление КАК ХарактеристикаПредставление,
		|	ВТ_ТоварыСгруппированные.Серия.Представление КАК СерияПредставление,
		|	ВТ_ТоварыСгруппированные.Номенклатура.ЕдиницаИзмерения.Представление КАК ЕдИзмПредставление
		|ИЗ
		|	ВТ_ТоварыСгруппированные КАК ВТ_ТоварыСгруппированные
		|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ ВТ_ОстаткиНаСкладе КАК ВТ_ОстаткиНаСкладе
		|			ЛЕВОЕ СОЕДИНЕНИЕ ВТ_БлокированныеТовары КАК ВТ_БлокированныеТовары
		|			ПО ВТ_ОстаткиНаСкладе.Номенклатура = ВТ_БлокированныеТовары.Номенклатура
		|				И ВТ_ОстаткиНаСкладе.Характеристика = ВТ_БлокированныеТовары.Характеристика
		|				И ВТ_ОстаткиНаСкладе.Серия = ВТ_БлокированныеТовары.Серия
		|		ПО ВТ_ТоварыСгруппированные.Номенклатура = ВТ_ОстаткиНаСкладе.Номенклатура
		|			И ВТ_ТоварыСгруппированные.Характеристика = ВТ_ОстаткиНаСкладе.Характеристика
		|			И ВТ_ТоварыСгруппированные.Серия = ВТ_ОстаткиНаСкладе.Серия
		|ГДЕ
		|	ВТ_ОстаткиНаСкладе.ВНаличииОстаток >= ВТ_ТоварыСгруппированные.Количество
		|	И (ВТ_ОстаткиНаСкладе.ВНаличииОстаток - ЕСТЬNULL(ВТ_БлокированныеТовары.КоличествоОстаток, 0)) < ВТ_ТоварыСгруппированные.Количество";
	
	Запрос.УстановитьПараметр("ТоварыДокумента", ТЗТовары);
	Запрос.УстановитьПараметр("СкладЛЦ", СкладЛЦ);
	МассивВариантовОбеспечения = Новый Массив;
	МассивВариантовОбеспечения.Добавить(Перечисления.ВариантыОбеспечения.Отгрузить);
	МассивВариантовОбеспечения.Добавить(Перечисления.ВариантыОбеспечения.ОтгрузитьОбособленно);
	Запрос.УстановитьПараметр("ВариантОбеспеченияОтгрузить", МассивВариантовОбеспечения);
	
	Результат = Запрос.Выполнить();
	Выборка = Результат.Выбрать();
	
	Пока Выборка.Следующий() Цикл
		ТекстСообщения = НСтр("ru='Для отгрузки номенклатуры %1 недостаточно %2 %3 товара на складе (находится на контроле качества ЛЦ)!'");
		ТекстСообщения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ТекстСообщения, Выборка.НоменклатураПредставление
				+ ?(ЗначениеЗаполнено(Выборка.ХарактеристикаПредставление), Выборка.ХарактеристикаПредставление, "")
				+ ?(ЗначениеЗаполнено(Выборка.СерияПредставление), "(серия " + Выборка.СерияПредставление + ")", ""),
				Выборка.НедостаточноТовара, Выборка.ЕдИзмПредставление);
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, , , , Отказ);
	КонецЦикла;
	
КонецПроцедуры
// --EE:BAN 29.12.2021 MLVC-4   

// ++EE:KMV 14.03.2023 SDEE-1907
Процедура ПроверитьКлассификациюНоменклатуры(КлассификацияНоменклатуры, ИзменилисьРеквизиты)

	МассивЗначений = ПолучитьМассивКлассификацииНоменклатуры();
	
	Если МассивЗначений.Найти(КлассификацияНоменклатуры) = Неопределено Тогда 
		ИзменилисьРеквизиты = Ложь;	
	КонецЕсли;
		
КонецПроцедуры 

Функция ПолучитьМассивКлассификацииНоменклатуры()
	
	Массив = Новый Массив;
	Массив.Добавить(Перечисления.Чд_КлассификацияНоменклатуры.ГотоваяПродукция);
	Массив.Добавить(Перечисления.Чд_КлассификацияНоменклатуры.РекламнаяПродукция);
	Массив.Добавить(Перечисления.Чд_КлассификацияНоменклатуры.ПокупныеТовары); 
	Возврат Массив;
	
КонецФункции 
// --EE:KMV 14.03.2023 SDEE-1907 

#КонецОбласти