﻿
#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

Процедура ПередЗаписью(Отказ)
	
	Если ОбменДанными.Загрузка Тогда
		Возврат;
	КонецЕсли;
	
	Если Не ДополнительныеСвойства.Свойство("КонстантаВычисленаПослеБлокировки") Тогда
		
		ВызватьИсключение НСтр("ru = 'Значение константы ИспользоватьУправлениеПеремещениемОбособленныхТоваров должно вычисляться после установки исключительной блокировки';
								|en = 'Value of the ИспользоватьУправлениеПеремещениемОбособленныхТоваров constant should be calculated after exclusive lock is set'");
		
	КонецЕсли;
	
КонецПроцедуры

#КонецЕсли
