﻿<package xmlns="http://v8.1c.ru/8.1/xdto" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" targetNamespace="http://www.w3schoolsa.com" elementFormQualified="true" attributeFormQualified="false">
	<property name="issuance">
		<typeDef xsi:type="ObjectType">
			<property name="sender" type="xs:string" lowerBound="1" form="Attribute"/>
			<property xmlns:d4p1="http://www.w3schoolsa.com" name="system" type="d4p1:system" lowerBound="0"/>
			<property xmlns:d4p1="http://www.w3schoolsa.com" name="general" type="d4p1:general"/>
			<property xmlns:d4p1="http://www.w3schoolsa.com" name="recipient" type="d4p1:recipient" lowerBound="0"/>
			<property xmlns:d4p1="http://www.w3schoolsa.com" name="deliveryCondition" type="d4p1:deliveryCondition" lowerBound="0"/>
			<property xmlns:d4p1="http://www.w3schoolsa.com" name="roster" type="d4p1:rosterList"/>
		</typeDef>
	</property>
	<valueType name="belongingToString" base="xs:string" variety="Atomic">
		<enumeration>1.1</enumeration>
		<enumeration>1.2</enumeration>
		<enumeration>1.3</enumeration>
	</valueType>
	<valueType name="descriptionType" base="xs:string" variety="Atomic">
		<enumeration>DEDUCTION_IN_FULL</enumeration>
		<enumeration>VAT_EXEMPTION</enumeration>
		<enumeration>OUTSIDE_RB</enumeration>
		<enumeration>IMPORT_VAT</enumeration>
		<enumeration>EXCISE_MARK</enumeration>
		<enumeration>CONTROL_MARK</enumeration>
		<enumeration>CUSTOMS_EXCISE</enumeration>
		<enumeration>IDENTIFIED_MARK</enumeration>
		<enumeration>ANALYZED_DEAL</enumeration>
	</valueType>
	<valueType name="invoiceDocType" base="xs:string" variety="Atomic">
		<enumeration>ORIGINAL</enumeration>
		<enumeration>ADDITIONAL</enumeration>
		<enumeration>FIXED</enumeration>
		<enumeration>ADD_NO_REFERENCE</enumeration>
	</valueType>
	<valueType name="invoiceFormat" base="xs:string" variety="Atomic">
		<pattern>\d{9}\-\d{4}\-\d{10}</pattern>
	</valueType>
	<valueType name="rateType" base="xs:string" variety="Atomic">
		<enumeration>DECIMAL</enumeration>
		<enumeration>ZERO</enumeration>
		<enumeration>NO_VAT</enumeration>
		<enumeration>CALCULATED</enumeration>
	</valueType>
	<valueType name="version" base="xs:string" variety="Atomic">
		<enumeration>1.0.0</enumeration>
		<enumeration>1.1.0</enumeration>
	</valueType>
	<objectType name="contract">
		<property name="number" type="xs:string" lowerBound="0"/>
		<property name="date" type="xs:date" lowerBound="0"/>
		<property xmlns:d3p1="http://www.w3schoolsa.com" name="documents" type="d3p1:documentList" lowerBound="0"/>
	</objectType>
	<objectType name="deliveryCondition">
		<property xmlns:d3p1="http://www.w3schoolsa.com" name="contract" type="d3p1:contract" lowerBound="0"/>
		<property name="description" type="xs:string" lowerBound="0"/>
		<property xmlns:d3p1="http://www.w3schoolsa.com" name="belongToString" type="d3p1:belongingToString" lowerBound="0" fixed="false" default="1.1"/>
	</objectType>
	<objectType name="descriptionList">
		<property xmlns:d3p1="http://www.w3schoolsa.com" name="description" type="d3p1:descriptionType" upperBound="-1"/>
	</objectType>
	<objectType name="docType">
		<property name="code" type="xs:integer"/>
		<property name="value" type="xs:string" lowerBound="0"/>
	</objectType>
	<objectType name="document">
		<property xmlns:d3p1="http://www.w3schoolsa.com" name="docType" type="d3p1:docType"/>
		<property name="date" type="xs:date" lowerBound="0"/>
		<property name="blankCode" type="xs:string" lowerBound="0"/>
		<property name="seria" type="xs:string" lowerBound="0"/>
		<property name="number" type="xs:string"/>
		<property name="refund" type="xs:boolean" lowerBound="0"/>
	</objectType>
	<objectType name="documentList">
		<property xmlns:d3p1="http://www.w3schoolsa.com" name="document" type="d3p1:document" upperBound="-1"/>
	</objectType>
	<objectType name="general">
		<property xmlns:d3p1="http://www.w3schoolsa.com" name="number" type="d3p1:invoiceFormat"/>
		<property name="dateIssuance" type="xs:date" lowerBound="0"/>
		<property name="dateTransaction" type="xs:date"/>
		<property xmlns:d3p1="http://www.w3schoolsa.com" name="documentType" type="d3p1:invoiceDocType"/>
		<property xmlns:d3p1="http://www.w3schoolsa.com" name="invoice" type="d3p1:invoiceFormat"/>
		<property name="sendToRecipient" type="xs:boolean" fixed="false" default="false"/>
	</objectType>
	<objectType name="recipient">
		<property xmlns:d3p1="http://www.w3schoolsa.com" name="taxes" type="d3p1:taxesType" lowerBound="0"/>
	</objectType>
	<objectType name="rosterItem">
		<property name="number" type="xs:integer" lowerBound="0"/>
		<property name="name" type="xs:string"/>
		<property name="code" type="xs:string" lowerBound="0"/>
		<property name="code_oced" lowerBound="0">
			<typeDef xsi:type="ValueType" variety="Union">
				<typeDef base="xs:string" variety="Atomic" length="0"/>
				<typeDef base="xs:integer" variety="Atomic"/>
			</typeDef>
		</property>
		<property name="units" type="xs:integer" lowerBound="0"/>
		<property name="count" type="xs:decimal" lowerBound="0"/>
		<property name="price" type="xs:decimal" lowerBound="0"/>
		<property name="cost" type="xs:decimal"/>
		<property name="summaExcise" type="xs:decimal" lowerBound="0"/>
		<property xmlns:d3p1="http://www.w3schoolsa.com" name="vat" type="d3p1:vat"/>
		<property name="costVat" type="xs:decimal"/>
		<property xmlns:d3p1="http://www.w3schoolsa.com" name="descriptions" type="d3p1:descriptionList" lowerBound="0"/>
		<property name="skipDeduction" type="xs:date" lowerBound="0"/>
	</objectType>
	<objectType name="rosterList">
		<property name="totalCostVat" type="xs:decimal" lowerBound="1" form="Attribute"/>
		<property name="totalExcise" type="xs:decimal" lowerBound="1" form="Attribute"/>
		<property name="totalVat" type="xs:decimal" lowerBound="1" form="Attribute"/>
		<property name="totalCost" type="xs:decimal" lowerBound="1" form="Attribute"/>
		<property xmlns:d3p1="http://www.w3schoolsa.com" name="rosterItem" type="d3p1:rosterItem" upperBound="-1"/>
	</objectType>
	<objectType name="system">
		<property xmlns:d3p1="http://www.w3schoolsa.com" name="modelVersion" type="d3p1:version" fixed="false" default="1.0.0"/>
	</objectType>
	<objectType name="taxesType">
		<property name="number" type="xs:string"/>
		<property name="date" type="xs:date"/>
	</objectType>
	<objectType name="vat">
		<property name="rate" type="xs:decimal"/>
		<property xmlns:d3p1="http://www.w3schoolsa.com" name="rateType" type="d3p1:rateType"/>
		<property name="summaVat" type="xs:decimal"/>
	</objectType>
</package>