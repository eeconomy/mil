﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ОбработчикиСобытий

Процедура ПередЗаписью(Отказ, Замещение)

	Если ОбменДанными.Загрузка Или Не ПроведениеСерверУТ.РассчитыватьИзменения(ДополнительныеСвойства) Тогда
		Возврат;
	КонецЕсли;

	ОбновлениеИнформационнойБазы.ПроверитьОбъектОбработан(ЭтотОбъект);
	
	СтруктураВременныеТаблицы = ДополнительныеСвойства.ДляПроведения.СтруктураВременныеТаблицы;
	БлокироватьДляИзменения = Истина;

	// Текущее состояние набора помещается во временную таблицу "ДвиженияТоварыВЯчейкахЗаписью",
	// чтобы при записи получить изменение нового набора относительно текущего.
	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("Регистратор", Отбор.Регистратор.Значение);
	Запрос.УстановитьПараметр("ЭтоНовый",    ДополнительныеСвойства.ЭтоНовый);
	Запрос.МенеджерВременныхТаблиц = СтруктураВременныеТаблицы.МенеджерВременныхТаблиц;
	Запрос.Текст =
		"ВЫБРАТЬ
		|	Таблица.Номенклатура КАК Номенклатура,
		|	Таблица.Характеристика КАК Характеристика,
		|	Таблица.Склад КАК Склад,
		|	Таблица.Серия КАК Серия,
		|	Таблица.SNOMTTN КАК SNOMTTN,
		|	ВЫБОР
		|		КОГДА Таблица.ВидДвижения = ЗНАЧЕНИЕ(ВидДвиженияНакопления.Приход)
		|			ТОГДА Таблица.КВозврату
		|		ИНАЧЕ -Таблица.КВозврату
		|	КОНЕЦ КАК КВозврату,
		|	ВЫБОР
		|		КОГДА Таблица.ВидДвижения = ЗНАЧЕНИЕ(ВидДвиженияНакопления.Приход)
		|			ТОГДА Таблица.КУтилизации
		|		ИНАЧЕ -Таблица.КУтилизации
		|	КОНЕЦ КАК КУтилизации,
		|	ВЫБОР
		|		КОГДА Таблица.ВидДвижения = ЗНАЧЕНИЕ(ВидДвиженияНакопления.Приход)
		|			ТОГДА Таблица.КУценке
		|		ИНАЧЕ -Таблица.КУценке
		|	КОНЕЦ КАК КУценке,
		|	ВЫБОР
		|		КОГДА Таблица.ВидДвижения = ЗНАЧЕНИЕ(ВидДвиженияНакопления.Приход)
		|			ТОГДА Таблица.КРазмещению
		|		ИНАЧЕ -Таблица.КРазмещению
		|	КОНЕЦ КАК КРазмещению,
		|	ВЫБОР
		|		КОГДА Таблица.ВидДвижения = ЗНАЧЕНИЕ(ВидДвиженияНакопления.Приход)
		|			ТОГДА Таблица.КХранению
		|		ИНАЧЕ -Таблица.КХранению
		|	КОНЕЦ КАК КХранению
		|ПОМЕСТИТЬ ДвиженияОтражениеКачестваЛЦПередЗаписью
		|ИЗ
		|	РегистрНакопления.ЭЭ_ОтражениеКачестваЛЦ КАК Таблица
		|ГДЕ
		|	НЕ &ЭтоНовый
		|	И Таблица.Регистратор = &Регистратор";
	
	Запрос.Выполнить();

КонецПроцедуры

Процедура ПриЗаписи(Отказ, Замещение)

	Если ОбменДанными.Загрузка Или Не ПроведениеСерверУТ.РассчитыватьИзменения(ДополнительныеСвойства) Тогда
		Возврат;
	КонецЕсли;

	СтруктураВременныеТаблицы = ДополнительныеСвойства.ДляПроведения.СтруктураВременныеТаблицы;

	// Рассчитывается изменение нового набора относительно текущего с учетом накопленных изменений
	// и помещается во временную таблицу.
	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("Регистратор", Отбор.Регистратор.Значение);
	Запрос.МенеджерВременныхТаблиц = СтруктураВременныеТаблицы.МенеджерВременныхТаблиц;
	Запрос.Текст =	
	"ВЫБРАТЬ
	|	ТаблицаИзменений.Номенклатура КАК Номенклатура,
	|	ТаблицаИзменений.Характеристика КАК Характеристика,
	|	ТаблицаИзменений.Склад КАК Склад,
	|	ТаблицаИзменений.Серия КАК Серия,
	|	ТаблицаИзменений.SNOMTTN КАК SNOMTTN,
	|	СУММА(ТаблицаИзменений.КВозврату) КАК КВозврату,
	|	СУММА(ТаблицаИзменений.КУтилизации) КАК КУтилизации,
	|	СУММА(ТаблицаИзменений.КУценке) КАК КУценке,
	|	СУММА(ТаблицаИзменений.КРазмещению) КАК КРазмещению,
	|	СУММА(ТаблицаИзменений.КХранению) КАК КХранению
	|ПОМЕСТИТЬ ДвиженияОтражениеКачестваЛЦИзменение
	|ИЗ
	|	(ВЫБРАТЬ
	|		Таблица.Номенклатура КАК Номенклатура,
	|		Таблица.Характеристика КАК Характеристика,
	|		Таблица.Серия КАК Серия,
	|		Таблица.Склад КАК Склад,
	|		Таблица.SNOMTTN КАК SNOMTTN,
	|		Таблица.КВозврату КАК КВозврату,
	|		Таблица.КУтилизации КАК КУтилизации,
	|		Таблица.КУценке КАК КУценке,
	|		Таблица.КРазмещению КАК КРазмещению,
	|		Таблица.КРазмещению КАК КХранению
	|	ИЗ
	|		ДвиженияОтражениеКачестваЛЦПередЗаписью КАК Таблица
	|	
	|	ОБЪЕДИНИТЬ ВСЕ
	|	
	|	ВЫБРАТЬ
	|		Таблица.Номенклатура,
	|		Таблица.Характеристика,
	|		Таблица.Склад,
	|		Таблица.Серия,
	|		Таблица.SNOMTTN,
	|		ВЫБОР
	|			КОГДА Таблица.ВидДвижения = ЗНАЧЕНИЕ(ВидДвиженияНакопления.Приход)
	|				ТОГДА -Таблица.КВозврату
	|			ИНАЧЕ Таблица.КВозврату
	|		КОНЕЦ,
	|		ВЫБОР
	|			КОГДА Таблица.ВидДвижения = ЗНАЧЕНИЕ(ВидДвиженияНакопления.Приход)
	|				ТОГДА -Таблица.КУтилизации
	|			ИНАЧЕ Таблица.КУтилизации
	|		КОНЕЦ,
	|		ВЫБОР
	|			КОГДА Таблица.ВидДвижения = ЗНАЧЕНИЕ(ВидДвиженияНакопления.Приход)
	|				ТОГДА -Таблица.КУценке
	|			ИНАЧЕ Таблица.КУценке
	|		КОНЕЦ,
	|		ВЫБОР
	|			КОГДА Таблица.ВидДвижения = ЗНАЧЕНИЕ(ВидДвиженияНакопления.Приход)
	|				ТОГДА -Таблица.КРазмещению
	|			ИНАЧЕ Таблица.КРазмещению
	|		КОНЕЦ,
	|		ВЫБОР
	|			КОГДА Таблица.ВидДвижения = ЗНАЧЕНИЕ(ВидДвиженияНакопления.Приход)
	|				ТОГДА -Таблица.КХранению
	|			ИНАЧЕ Таблица.КХранению
	|		КОНЕЦ
	|	ИЗ
	|		РегистрНакопления.ЭЭ_ОтражениеКачестваЛЦ КАК Таблица
	|	ГДЕ
	|		Таблица.Регистратор = &Регистратор) КАК ТаблицаИзменений
	|ГДЕ
	|	ТаблицаИзменений.КРазмещению > 0
	|
	|СГРУППИРОВАТЬ ПО
	|	ТаблицаИзменений.Номенклатура,
	|	ТаблицаИзменений.Характеристика,
	|	ТаблицаИзменений.Склад,
	|	ТаблицаИзменений.Серия,
	|	ТаблицаИзменений.SNOMTTN
	|
	|ИМЕЮЩИЕ
	|	(СУММА(ТаблицаИзменений.КУценке) > 0
	|		ИЛИ СУММА(ТаблицаИзменений.КВозврату) > 0
	|		ИЛИ СУММА(ТаблицаИзменений.КУтилизации) > 0
	|		ИЛИ СУММА(ТаблицаИзменений.КРазмещению) > 0
	|		ИЛИ СУММА(ТаблицаИзменений.КХранению) > 0)
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|УНИЧТОЖИТЬ ДвиженияОтражениеКачестваЛЦПередЗаписью";
	
	Выборка = Запрос.ВыполнитьПакет()[0].Выбрать();
	Выборка.Следующий();
	
	// Добавляется информация о ее существовании и наличии в ней записей об изменении.
	СтруктураВременныеТаблицы.Вставить("ДвижениеОтражениеКачестваЛЦ", Выборка.Количество > 0);

КонецПроцедуры

#КонецОбласти

#КонецЕсли