﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда
	
#Область СлужебныеПроцедурыИФункции

// Описание:
//  для записи набора движений из модуля объекта документа
Процедура ОтразитьПлановыеВозвратыОсновныхМатериалов(ДополнительныеСвойства, Движения, Отказ) Экспорт

	Таблица = ДополнительныеСвойства.ТаблицыДляДвижений.ТаблицаПлановыеВозвратыОсновныхМатериалов;
	
	Если Отказ ИЛИ Таблица.Количество() = 0 Тогда
		Возврат;
	КонецЕсли;

	Движения.Чд_ПлановыеВозвратыОсновныхМатериалов.Записывать = Истина;
	Движения.Чд_ПлановыеВозвратыОсновныхМатериалов.Загрузить(Таблица);

КонецПроцедуры

#КонецОбласти

#КонецЕсли
