﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда
	
#Область СлужебныеПроцедурыИФункции

// Описание:
//  для записи набора движений из модуля объекта документа
Процедура ОтразитьТоварыПереданныеПодКарту(ДополнительныеСвойства, Движения, Отказ) Экспорт

	Таблица = ДополнительныеСвойства.ТаблицыДляДвижений.ТаблицаТоварыПереданныеПодКарту;
	
	Если Отказ ИЛИ Таблица.Количество() = 0 Тогда
		Возврат;
	КонецЕсли;

	Движения.Чд_ТоварыПереданныеПодКарту.Записывать = Истина;
	Движения.Чд_ТоварыПереданныеПодКарту.Загрузить(Таблица);

КонецПроцедуры

#КонецОбласти

#КонецЕсли
