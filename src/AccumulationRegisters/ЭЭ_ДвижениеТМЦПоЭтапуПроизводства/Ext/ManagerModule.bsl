﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда
	
#Область СлужебныеПроцедурыИФункции

// ++EE:BOP 31.03.2022 MLVC-372
// ДС 10 Разработка Отчета Движение ТМЦ 36 процесса
// Описание:
// для записи набора движений из модуля объекта документа
// Параметры:
// ДополнительныеСвойства - Структура - Структура дополнительных свойств для проведения.
// Движения - Структура - Структура наборов движений документа.
// Отказ - Булево - Признак отказа от проведения документа.
Процедура ОтразитьДвижениеТМЦПоЭтапуПроизводства(ДополнительныеСвойства, Движения, Отказ) Экспорт

	Таблица = ДополнительныеСвойства.ТаблицыДляДвижений.ТаблицаДвижениеТМЦПоЭтапуПроизводства;
	
	Если Отказ ИЛИ НЕ Таблица.Количество() Тогда
		Возврат;
	КонецЕсли;

	Движения.ЭЭ_ДвижениеТМЦПоЭтапуПроизводства.Записывать = Истина;
	Движения.ЭЭ_ДвижениеТМЦПоЭтапуПроизводства.Загрузить(Таблица);

КонецПроцедуры
// --EE:BOP 31.03.2022 MLVC-372

#КонецОбласти

#КонецЕсли
