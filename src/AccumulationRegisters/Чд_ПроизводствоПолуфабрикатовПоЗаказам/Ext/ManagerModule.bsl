﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ПрограммныйИнтерфейс

// Записывает данные документы в регистр накопленя Чд_ПроизводствоПолуфабрикатовПоЗаказам
//
// Параметры:
//  ДополнительныеСвойства	 - Структура - содержит по ключу ТаблицыДляДвижений структуру
//  										имеющую ключ Чд_ПроизводствоПолуфабрикатовПоЗаказам (ТаблицаЗначений)
//  Движения				 - Структура - движения документа
//  Отказ					 - Булево - флаг отказа от записи 
//
Процедура ОтразитьДвижения(ДополнительныеСвойства, Движения, Отказ) Экспорт
	
	Если ТипЗнч(ДополнительныеСвойства.ДляПроведения.Ссылка) = Тип("ДокументСсылка.ЗаказНаПроизводство2_2") Тогда  // заказ на производство
		Таблица = СформироватьДанныеПоСпецификации(ДополнительныеСвойства.ДляПроведения.Ссылка);
	Иначе   // этап производства
		Таблица = ДополнительныеСвойства.ТаблицыДляДвижений.ТаблицаЧд_ПроизводствоПолуфабрикатовПоЗаказам;
	КонецЕсли;
	
	Если Отказ ИЛИ Таблица = Неопределено ИЛИ Таблица.Количество() = 0 Тогда
		Возврат;
	КонецЕсли;
	
	СписокПустыхХарактеристик = Таблица.НайтиСтроки(Новый Структура("Характеристика", Справочники.ХарактеристикиНоменклатуры.ПустаяСсылка()));
	Если СписокПустыхХарактеристик.Количество() <> 0 Тогда
		ТекстСообщение = "Нет характеристик по следующим позициям:";
		Для каждого Строка Из СписокПустыхХарактеристик Цикл
			ТекстСообщение = ТекстСообщение + Символы.ПС + Строка(Строка.Полуфабрикат) 
				+ " по РРШ " +  Строка(Строка.ЗаказНаПроизводство.ДокументОснование.МодельЦвет) 
				+ "(РРШ " +  Строка(Строка.ЗаказНаПроизводство.ДокументОснование) + ")";   
		КонецЦикла; 
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщение);
	КонецЕсли;
	
	СписокПустыхПроцессов = Таблица.НайтиСтроки(Новый Структура("Процесс", Справочники.Чд_Процессы.ПустаяСсылка()));
	Если СписокПустыхПроцессов.Количество() <> 0 Тогда
		ТекстСообщение = "Нет процессов по следующим позициям (проверьте спецификации):";
		Для каждого Строка Из СписокПустыхПроцессов Цикл
			ТекстСообщение = ТекстСообщение + Символы.ПС + Строка(Строка.Полуфабрикат) 
				+ " по РРШ " +  Строка(Строка.ЗаказНаПроизводство.ДокументОснование.МодельЦвет) 
				+ "(РРШ " +  Строка(Строка.ЗаказНаПроизводство.ДокументОснование) + ")";   
		КонецЦикла; 
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщение);
	КонецЕсли;
	
	Если СписокПустыхХарактеристик.Количество() > 0 ИЛИ СписокПустыхПроцессов.Количество() > 0 Тогда
		Отказ = Истина;
		Возврат;
	КонецЕсли; 
	
	Движения.Чд_ПроизводствоПолуфабрикатовПоЗаказам.Записывать = Истина;
	Движения.Чд_ПроизводствоПолуфабрикатовПоЗаказам.Загрузить(Таблица);
	
КонецПроцедуры

// Описание:
// 	формирует записи в РН "Чд_ПроизводствоПолуфабрикатовПоЗаказам"
//	и РС "Чд_ЗаказыНаПроизводствоВРазрезеМатериалов" при проведении документа "ЗаказНаПроизводство2_2" из 
//  обработки "Чд_АРМФормированиеЗаказовПоРРШ"
// Параметры:
//  ЗаказыКОбработке 	- Структура - структура объектов к обработке
//		* ОбъектРасчета - ДокументСсылка.ЗаказНаПроизводство2_2, ДокументСсылка.ЭтапПроизводства2_2 - документ к обработке
//  ПропуститьЗадание	- Булево- признак, требуется пропустить выполнение задания (возвращаемое значение).
//
Процедура СформироватьЗаписиВПлановыеРегистрыОбеспечения(ЗаказыКОбработке, ПропуститьЗадание = Ложь) Экспорт
	
	ЗаказСсылка = ЗаказыКОбработке.ОбъектРасчета;
	Если ТипЗнч(ЗаказСсылка) <> Тип("ДокументСсылка.ЗаказНаПроизводство2_2") Тогда
		Возврат;
	КонецЕсли;
	
#Область РН_ПроизводствоПолуфабрикатовПоЗаказам

	ТаблицаПФПоЗаказам = СформироватьДанныеПоСпецификации(ЗаказСсылка);
	
	Если ТаблицаПФПоЗаказам <> Неопределено И ТаблицаПФПоЗаказам.Количество() Тогда
		
		Отказ = Ложь;
		ФрагментФормированияДанныхДляЗаписи(ТаблицаПФПоЗаказам, ЗаказСсылка, Отказ);
		
		Если Отказ Тогда
			Возврат;
		КонецЕсли; 
		
		НаборЗаписей = РегистрыНакопления.Чд_ПроизводствоПолуфабрикатовПоЗаказам.СоздатьНаборЗаписей();
		НаборЗаписей.Отбор.Регистратор.Установить(ЗаказСсылка);
		НаборЗаписей.Загрузить(ТаблицаПФПоЗаказам);
		НаборЗаписей.Записать();
	КонецЕсли;
	
#КонецОбласти 	
	
#Область РС_ЗаказыНаПроизводствоВРазрезеМатериалов

	РегистрыСведений.Чд_ЗаказыНаПроизводствоВРазрезеМатериалов.ЗаписатьВРегистрЧд_ЗаказыНаПроизводствоВРазрезеМатериалов(ЗаказСсылка);

#КонецОбласти 
	
КонецПроцедуры

// Процедура "Записи" данных в регистр
//
Процедура ЗаписатьВРегистрЧд_ПроизводствоПолуфабрикатовПоЗаказам(ЗаказСсылка) Экспорт
	
	Если ТипЗнч(ЗаказСсылка) = Тип("ДокументСсылка.ЗаказНаПроизводство2_2") Тогда  // заказ на производство
		Таблица = СформироватьДанныеПоСпецификации(ЗаказСсылка);
	Иначе   // этап производства
		Возврат;
	КонецЕсли;
	
	Если Таблица = Неопределено ИЛИ Таблица.Количество() = 0 Тогда
		Возврат;
	КонецЕсли;
	
	Отказ = Ложь;
	ФрагментФормированияДанныхДляЗаписи(Таблица, ЗаказСсылка, Отказ);
	
	Если Отказ Тогда
		Возврат;
	КонецЕсли; 
	
	НаборЗаписей = РегистрыНакопления.Чд_ПроизводствоПолуфабрикатовПоЗаказам.СоздатьНаборЗаписей();
	НаборЗаписей.Отбор.Регистратор.Установить(ЗаказСсылка);
	НаборЗаписей.Загрузить(Таблица);
	НаборЗаписей.Записать();
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

Функция СформироватьДанныеПоСпецификации(ЗаказСсылка)
	
	Изделия = Новый Массив;
	
	СписокТребуемыхМатериалов = Новый ТаблицаЗначений;
	СписокТребуемыхМатериалов.Колонки.Добавить("ЗаказНаПроизводство", Новый ОписаниеТипов("ДокументСсылка.ЗаказНаПроизводство2_2"));
	СписокТребуемыхМатериалов.Колонки.Добавить("Приоритет", Новый ОписаниеТипов("Число", Новый КвалификаторыЧисла(30, 0)));	
	СписокТребуемыхМатериалов.Колонки.Добавить("ВидНоменклатуры", Новый ОписаниеТипов("СправочникСсылка.ВидыНоменклатуры"));
	СписокТребуемыхМатериалов.Колонки.Добавить("Полуфабрикат", Новый ОписаниеТипов("СправочникСсылка.Номенклатура"));
	СписокТребуемыхМатериалов.Колонки.Добавить("Характеристика", Новый ОписаниеТипов("СправочникСсылка.ХарактеристикиНоменклатуры"));
	СписокТребуемыхМатериалов.Колонки.Добавить("Упаковка", Новый ОписаниеТипов("СправочникСсылка.УпаковкиЕдиницыИзмерения"));
	СписокТребуемыхМатериалов.Колонки.Добавить("Количество", Новый ОписаниеТипов("Число", Новый КвалификаторыЧисла(15, 3)));
	СписокТребуемыхМатериалов.Колонки.Добавить("Процесс", Новый ОписаниеТипов("СправочникСсылка.Чд_Процессы"));
	СписокТребуемыхМатериалов.Колонки.Добавить("Операция", Новый ОписаниеТипов("Строка", Новый КвалификаторыСтроки(50)));	
	СписокТребуемыхМатериалов.Колонки.Добавить("Период", Новый ОписаниеТипов("Дата", Новый КвалификаторыДаты()));	
	СписокТребуемыхМатериалов.Колонки.Добавить("ВидДвижения", Новый ОписаниеТипов("ВидДвиженияНакопления"));	
	
	ЗаказНаПроизводство = ЗаказСсылка;
	Изделия = ЗаказНаПроизводство.Продукция.НайтиСтроки(Новый Структура("Отменено", Ложь));
	
	ДанныеСпецификации = Неопределено;
	ПараметрыСпецификации = Неопределено;
	
	Если Изделия.Количество() = 0 Тогда
		Возврат Неопределено;
	КонецЕсли;
	
	Обработки.Чд_АРМРасчетОбеспеченности.РазузловатьПересчитатьСпецификацию(Изделия, 
																	ДанныеСпецификации, ПараметрыСпецификации);
	
	ЗначенияЗаполнения = Обработки.Чд_АРМРасчетОбеспеченности.ЗначенияЗаполненияРегистровПотребностей(Изделия, ПараметрыСпецификации);
	
	Для каждого Данные Из ДанныеСпецификации.ДанныеСпецификаций Цикл
		
		// Планы потребления материалов
		ЗначенияЗаполнения.Вставить("Спецификация", ЗначенияЗаполнения.СпецификацияПродукции);
		
		ДобавитьДвиженияМатериалыВСписокТребуемыхМатериалов(ЗначенияЗаполнения, 
					Данные.МатериалыИУслуги, ЗаказНаПроизводство, СписокТребуемыхМатериалов);
		
		Если Данные.Спецификация = ЗначенияЗаполнения.СпецификацияПродукции Тогда
			
			ДобавитьДвиженияВходящиеИзделияВСписокТребуемыхМатериалов(ЗначенияЗаполнения, Данные.ВходящиеИзделия, 
				ЗаказНаПроизводство, СписокТребуемыхМатериалов);
			
		КонецЕсли;
		
	КонецЦикла;
	
	СписокТребуемыхМатериалов.Свернуть("ЗаказНаПроизводство, Полуфабрикат, Характеристика, Упаковка, Процесс, Операция, Период, ВидДвижения", "Количество");
	СписокТребуемыхМатериалов.ЗаполнитьЗначения(ВидДвиженияНакопления.Приход, "ВидДвижения");
	Возврат СписокТребуемыхМатериалов;
	
КонецФункции

Процедура ДобавитьДвиженияМатериалыВСписокТребуемыхМатериалов(ЗначенияЗаполнения, ТаблицаИсточник, 
																ЗаказНаПроизводство, СписокТребуемыхМатериалов) 
	
	Для каждого Строка Из ТаблицаИсточник Цикл
		
		Если Строка.СпособПолученияМатериала = Перечисления.СпособыПолученияМатериаловВСпецификации.ПроизводитсяНаЭтапе 
			И ЗначениеЗаполнено(Строка.ИсточникПолученияПолуфабриката) Тогда
			Чд_Этап = Строка.ИсточникПолученияПолуфабриката;
		ИначеЕсли Строка.СпособПолученияМатериала = Перечисления.СпособыПолученияМатериаловВСпецификации.ПроизвестиПоСпецификации 
			И ЗначениеЗаполнено(Строка.ИсточникПолученияПолуфабриката) Тогда
			Чд_Этап = ЭтапРС(Строка.ИсточникПолученияПолуфабриката);
		Иначе
			Продолжить;
		КонецЕсли;
		
		Отбор = Новый Структура;
		Отбор.Вставить("ЗаказНаПроизводство", ЗаказНаПроизводство);
		Отбор.Вставить("Полуфабрикат", Строка.Номенклатура);
		Отбор.Вставить("Характеристика", Строка.Характеристика);
		Отбор.Вставить("Процесс", Чд_Этап.Чд_КодПроцесса);
		
		Если СписокТребуемыхМатериалов.НайтиСтроки(Отбор).Количество() = 0 Тогда
			НоваяСтрока = СписокТребуемыхМатериалов.Добавить();
			НоваяСтрока.ВидНоменклатуры = Строка.Номенклатура.ВидНоменклатуры;
			НоваяСтрока.Полуфабрикат = Строка.Номенклатура;
			ЗаполнитьЗначенияСвойств(НоваяСтрока, Строка);
			ЗаполнитьЗначенияСвойств(НоваяСтрока, ЗначенияЗаполнения);
			НоваяСтрока.Упаковка = Строка.ЕдиницаИзмерения;
			НоваяСтрока.Процесс = Чд_Этап.Чд_КодПроцесса;
			НоваяСтрока.Количество = Строка.Количество;
			НоваяСтрока.ЗаказНаПроизводство = ЗаказНаПроизводство;
			НоваяСтрока.Период = ЗаказНаПроизводство.Дата;
			НоваяСтрока.Операция = "требуется";
		Иначе
			СтрокаСписка = СписокТребуемыхМатериалов.НайтиСтроки(Отбор)[0];
			СтрокаСписка.Количество = СтрокаСписка.Количество + Строка.Количество;
		КонецЕсли;
		
	КонецЦикла;
	
КонецПроцедуры

Процедура ДобавитьДвиженияВходящиеИзделияВСписокТребуемыхМатериалов(ЗначенияЗаполнения, ТаблицаИсточник, 
																	ЗаказНаПроизводство, СписокТребуемыхМатериалов)  
	
	Для каждого Строка Из ТаблицаИсточник Цикл
		
		Отбор = Новый Структура;
		Отбор.Вставить("ЗаказНаПроизводство", ЗаказНаПроизводство);
		Отбор.Вставить("Полуфабрикат", Строка.Номенклатура);
		Отбор.Вставить("Характеристика", Строка.Характеристика);
		Отбор.Вставить("Процесс", Строка.Этап.Чд_КодПроцесса);
		
		Если СписокТребуемыхМатериалов.НайтиСтроки(Отбор).Количество() = 0 Тогда
			НоваяСтрока = СписокТребуемыхМатериалов.Добавить();
			НоваяСтрока.ВидНоменклатуры = Строка.Номенклатура.ВидНоменклатуры;
			НоваяСтрока.Полуфабрикат = Строка.Номенклатура;
			ЗаполнитьЗначенияСвойств(НоваяСтрока, Строка);
			ЗаполнитьЗначенияСвойств(НоваяСтрока, ЗначенияЗаполнения);
			НоваяСтрока.Упаковка = Строка.ЕдиницаИзмерения;
			НоваяСтрока.Процесс = Строка.Этап.Чд_КодПроцесса;
			НоваяСтрока.Количество = Строка.Количество;
			НоваяСтрока.ЗаказНаПроизводство = ЗаказНаПроизводство;
			НоваяСтрока.Период = ЗаказНаПроизводство.Дата;
			НоваяСтрока.Операция = "требуется";
		Иначе
			СтрокаСписка = СписокТребуемыхМатериалов.НайтиСтроки(Отбор)[0];
			СтрокаСписка.Количество = СтрокаСписка.Количество + Строка.Количество;
		КонецЕсли;
		
	КонецЦикла;
	
КонецПроцедуры

Функция ЭтапРС(Спецификация)
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	ЭтапыПроизводства.Ссылка КАК Ссылка
	|ИЗ
	|	Справочник.ЭтапыПроизводства КАК ЭтапыПроизводства
	|ГДЕ
	|	НЕ ЭтапыПроизводства.ПометкаУдаления
	|	И ЭтапыПроизводства.Владелец = &Спецификация
	|	И ЭтапыПроизводства.НомерСледующегоЭтапа = 0";
	
	Запрос.УстановитьПараметр("Спецификация", Спецификация);
	
	Выборка = Запрос.Выполнить().Выбрать();
	
	Пока Выборка.Следующий() Цикл
		Возврат Выборка.Ссылка;
	КонецЦикла;
	
	Возврат Справочники.ЭтапыПроизводства.ПустаяСсылка();
	
КонецФункции

Процедура ФрагментФормированияДанныхДляЗаписи(Таблица, ЗаказСсылка, Отказ)

	СписокПустыхХарактеристик = Таблица.НайтиСтроки(Новый Структура("Характеристика", Справочники.ХарактеристикиНоменклатуры.ПустаяСсылка()));
	Если СписокПустыхХарактеристик.Количество() <> 0 Тогда
		ТекстСообщение = "Нет характеристик по следующим позициям:";
		Для каждого Строка Из СписокПустыхХарактеристик Цикл
			ТекстСообщение = ТекстСообщение + Символы.ПС + Строка(Строка.Полуфабрикат) 
				+ " по РРШ " +  Строка(Строка.ЗаказНаПроизводство.ДокументОснование.МодельЦвет) 
				+ "(РРШ " +  Строка(Строка.ЗаказНаПроизводство.ДокументОснование) + ")";   
		КонецЦикла; 
		// 4D:Милавица, Михаил, 28.11.2019 9:06:51 
		// Формирование регистров ""Производство полуфабрикатов по заказам"" 
		// и ""Заказы на производство в разрезе материалов"" регламентным фоновым заданием, №23579
		// {
		ТекстСообщения = НСтр("ru = 'Не удалось завершить задание по формированию записей регистра накопления Производство полуфабрикатов по заказам: %Причина%'");
		ТекстСообщения = СтрЗаменить(ТекстСообщения, "%Причина%", ТекстСообщение);
		СобытиеЖурналаРегистрации = НСтр("ru = 'Заказ на производство (фоновое задание)'",
										ОбщегоНазначенияКлиентСервер.КодОсновногоЯзыка());
										ЗаписьЖурналаРегистрации(СобытиеЖурналаРегистрации,
											УровеньЖурналаРегистрации.Ошибка,
											Метаданные.Документы.ЗаказНаПроизводство2_2,
											ЗаказСсылка,
											ТекстСообщения);
		// }
		// 4D
	КонецЕсли;
	
	СписокПустыхПроцессов = Таблица.НайтиСтроки(Новый Структура("Процесс", Справочники.Чд_Процессы.ПустаяСсылка()));
	Если СписокПустыхПроцессов.Количество() <> 0 Тогда
		ТекстСообщение = "Нет процессов по следующим позициям (проверьте спецификации):";
		Для каждого Строка Из СписокПустыхПроцессов Цикл
			ТекстСообщение = ТекстСообщение + Символы.ПС + Строка(Строка.Полуфабрикат) 
				+ " по РРШ " +  Строка(Строка.ЗаказНаПроизводство.ДокументОснование.МодельЦвет) 
				+ "(РРШ " +  Строка(Строка.ЗаказНаПроизводство.ДокументОснование) + ")";   
		КонецЦикла; 
		// 4D:Милавица, Михаил, 28.11.2019 9:06:51 
		// Формирование регистров ""Производство полуфабрикатов по заказам"" 
		// и ""Заказы на производство в разрезе материалов"" регламентным фоновым заданием, №23579
		// {
		ТекстСообщения = НСтр("ru = 'Не удалось завершить задание по формированию записей регистра накопления Производство полуфабрикатов по заказам: %Причина%'");
		ТекстСообщения = СтрЗаменить(ТекстСообщения, "%Причина%", ТекстСообщение);
		СобытиеЖурналаРегистрации = НСтр("ru = 'Заказ на производство (фоновое задание)'",
										ОбщегоНазначенияКлиентСервер.КодОсновногоЯзыка());
										ЗаписьЖурналаРегистрации(СобытиеЖурналаРегистрации,
											УровеньЖурналаРегистрации.Ошибка,
											Метаданные.Документы.ЗаказНаПроизводство2_2,
											ЗаказСсылка,
											ТекстСообщения);
		// }
		// 4D
	КонецЕсли;
	
	Если СписокПустыхХарактеристик.Количество() > 0 ИЛИ СписокПустыхПроцессов.Количество() > 0 Тогда
		// 4D:Милавица, Михаил, 28.11.2019 9:09:46 
		// Формирование регистров ""Производство полуфабрикатов по заказам"" 
		// и ""Заказы на производство в разрезе материалов"" регламентным фоновым заданием, №23579
		// {
		ДокОбъект = ЗаказСсылка.ПолучитьОбъект(); 
		ДокОбъект.Прочитать();
		ДокОбъект.Записать(РежимЗаписиДокумента.ОтменаПроведения);
		Отказ = Истина;
		// }
		// 4D
	КонецЕсли; 
	
КонецПроцедуры

#КонецОбласти

#КонецЕсли